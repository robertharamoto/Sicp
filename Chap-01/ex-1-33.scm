#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.33                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 5, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (filtered-accumulate-rec
         combiner null-value filter term aa next bb tolerance)
  (begin
    (if (and (> aa bb)
             (> (abs (- aa bb)) tolerance))
        (begin
          null-value)
        (begin
          (let ((next-aa (next aa))
                (this-term (term aa)))
            (begin
              (if (filter aa)
                  (begin
                    (combiner
                     (term aa)
                     (filtered-accumulate-rec
                      combiner null-value filter
                      term next-aa next bb tolerance)))
                  (begin
                    (combiner
                     null-value
                     (filtered-accumulate-rec
                      combiner null-value filter
                      term next-aa next bb tolerance))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (filtered-accumulate-iter
         combiner null-value filter
         term aa next bb tolerance)
  (define (local-iter aa acc)
    (begin
      (if (and (> aa bb)
               (> (abs (- aa bb)) tolerance))
          (begin
            acc)
          (begin
            (let ((next-acc
                   (combiner acc (term aa)))
                  (next-aa (next aa)))
              (begin
                (if (filter aa)
                    (begin
                      (local-iter next-aa next-acc))
                    (begin
                      (local-iter next-aa acc)
                      ))
                ))
            ))
      ))
  (begin
    (local-iter aa null-value)
    ))

;;;#############################################################
;;;#############################################################
(define (fac-rec nn tolerance)
  (define (fac-term xx)
    (begin
      xx
      ))
  (define (fac-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((result
           (filtered-accumulate-rec
            * 1
            (lambda (anum) #t)
            fac-term 1 fac-next nn tolerance)
           ))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fac-rec-1 result-hash-table)
 (begin
   (let ((sub-name "test-fac-rec-1")
         (test-list
          (list
           (list 2 2) (list 3 6) (list 4 24)
           (list 5 120) (list 6 720)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fac-rec nn tolerance)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (fac-iter nn tolerance)
  (define (fac-term xx)
    (begin
      xx
      ))
  (define (fac-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((result
           (filtered-accumulate-iter
            * 1
            (lambda (anum) #t)
            fac-term 1 fac-next nn tolerance)))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fac-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-fac-iter-1")
         (test-list
          (list
           (list 2 2) (list 3 6) (list 4 24)
           (list 5 120) (list 6 720)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fac-iter nn tolerance)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pi-term kk)
  (begin
    (cond
     ((even? kk)
      (begin
        (/ (+ kk 2) (+ kk 1))
        ))
     (else
      (begin
        (/ (+ kk 1) (+ kk 2))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pi-term-1 result-hash-table)
 (begin
   (let ((sub-name "test-pi-term-1")
         (test-list
          (list
           (list 1 (/ 2 3)) (list 2 (/ 4 3)) (list 3 (/ 4 5))
           (list 4 (/ 6 5)) (list 5 (/ 6 7)) (list 6 (/ 8 7))
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((kk (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (pi-term kk)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : kk=~a, "
                        sub-name test-label-index kk))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pi-rec nn tolerance)
  (define (pi-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((result
           (* 4
              (filtered-accumulate-rec
               * 1
               (lambda (anum) #t)
               pi-term 1 pi-next nn tolerance)
              )))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (pi-iter nn tolerance)
  (define (pi-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((result
           (* 4
              (filtered-accumulate-iter
               * 1
               (lambda (anum) #t)
               pi-term 1 pi-next nn tolerance)
              )))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (pi-sum-iter aa bb tolerance)
  (define (pi-term xx)
    (begin
      (/ 1.0 (* xx (+ xx 2)))
      ))
  (define (pi-next xx)
    (begin
      (+ xx 4)
      ))
  (begin
    (let ((result
           (* 8
              (filtered-accumulate-iter
               + 0
               (lambda (anum) #t)
               pi-term aa pi-next bb tolerance))))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pi-sum-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-pi-sum-iter-1")
         (test-list
          (list
           (list 1 1000 3.139592655589783)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa (list-ref this-list 0))
                  (bb (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (pi-sum-iter aa bb tolerance)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : aa=~a, bb=~a, "
                        sub-name test-label-index aa bb))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (smallest-divisor nn)
  (define (local-find-divisor nn test-divisor max-divisor)
    (begin
      (cond
       ((> test-divisor max-divisor)
        (begin
          nn
          ))
       ((zero? (remainder nn test-divisor))
        (begin
          test-divisor
          ))
       (else
        (begin
          (local-find-divisor
           nn (+ test-divisor 2) max-divisor)
          )))
      ))
  (begin
    (cond
     ((<= nn 1)
      (begin
        -1
        ))
     ((zero? (remainder nn 2))
      (begin
        2
        ))
     ((zero? (remainder nn 3))
      (begin
        3
        ))
     (else
      (begin
        (let ((max-divisor (1+ (sqrt nn))))
          (begin
            (local-find-divisor nn 5 max-divisor)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-smallest-divisor-1 result-hash-table)
 (begin
   (let ((sub-name "test-smallest-divisor-1")
         (test-list
          (list
           (list 2 2) (list 3 3) (list 4 2) (list 5 5)
           (list 6 2) (list 7 7) (list 8 2) (list 9 3)
           (list 10 2) (list 11 11) (list 12 2) (list 13 13)
           (list 14 2) (list 15 3) (list 16 2) (list 17 17)
           (list 18 2) (list 19 19) (list 20 2) (list 21 3)
           (list 22 2) (list 23 23) (list 24 2) (list 25 5)
           (list 26 2) (list 27 3) (list 28 2) (list 29 29)
           (list 30 2) (list 31 31) (list 32 2) (list 33 3)
           (list 34 2) (list 35 5) (list 36 2) (list 37 37)
           (list 38 2) (list 39 3) (list 40 2) (list 41 41)
           (list 42 2) (list 43 43) (list 44 2) (list 45 3)
           (list 46 2) (list 47 47) (list 48 2) (list 49 7)
           (list 50 2) (list 51 3) (list 52 2) (list 53 53)
           (list 54 2) (list 55 5) (list 56 2) (list 57 3)
           (list 58 2) (list 59 59) (list 60 2) (list 61 61)
           (list 62 2) (list 63 3) (list 64 2) (list 65 5)
           (list 66 2) (list 67 67) (list 68 2) (list 69 3)
           (list 70 2) (list 71 71) (list 72 2) (list 73 73)
           (list 74 2) (list 75 3) (list 76 2) (list 77 7)
           (list 78 2) (list 79 79) (list 80 2) (list 81 3)
           (list 82 2) (list 83 83) (list 84 2) (list 85 5)
           (list 86 2) (list 87 3) (list 88 2) (list 89 89)
           (list 90 2) (list 91 7) (list 92 2) (list 93 3)
           (list 94 2) (list 95 5) (list 96 2) (list 97 97)
           (list 98 2) (list 99 3) (list 100 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (smallest-divisor nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime? nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     (else
      (begin
        (= (smallest-divisor nn) nn)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-prime-1")
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 87 #f) (list 88 #f) (list 89 #t)
           (list 90 #f) (list 91 #f) (list 92 #f) (list 93 #f)
           (list 94 #f) (list 95 #f) (list 96 #f) (list 97 #t)
           (list 98 #f) (list 99 #f) (list 100 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (prime? nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; uses iterative version of filtered-accumulate-iter
(define (sum-squares-primes aa bb tolerance)
  (begin
    (let ((result
           (filtered-accumulate-iter
            + 0
            prime?
            (lambda (anum) (* anum anum))
            aa
            (lambda (anum) (1+ anum))
            bb tolerance)))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-squares-primes-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-squares-primes-1")
         (test-list
          (list
           (list 1 3 13) (list 1 5 38)
           (list 1 6 38) (list 1 7 87)
           (list 1 8 87) (list 1 9 87)
           (list 1 10 87) (list 1 11 208)
           (list 1 12 208)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa (list-ref this-list 0))
                  (bb (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (sum-squares-primes aa bb tolerance)))
                (let ((diff (abs (- shouldbe result))))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : aa=~a, bb=~a, "
                          sub-name test-label-index aa bb))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (< diff tolerance)
                       sub-name
                       (string-append err-msg-1 err-msg-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; uses iterative version of filtered-accumulate-iter
(define (relatively-prime-product nn tolerance)
  (begin
    (let ((result
           (filtered-accumulate-iter
            * 1
            (lambda (anum)
              (begin
                (if (equal? (gcd anum nn) 1)
                    (begin
                      #t)
                    (begin
                      #f
                      ))
                ))
            (lambda (anum)
              (begin
                anum
                ))
            1
            (lambda (anum)
              (begin
                (1+ anum)
                ))
            nn tolerance)))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-relatively-prime-product-1 result-hash-table)
 (begin
   (let ((sub-name "test-relatively-prime-product-1")
         (test-list
          (list
           (list 2 1) (list 3 2)
           (list 4 3) (list 5 24)
           (list 6 5) (list 7 720)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (relatively-prime-product
                      nn tolerance)))
                (let ((diff (abs (- shouldbe result))))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : nn=~a, "
                          sub-name test-label-index nn))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (< diff tolerance)
                       sub-name
                       (string-append err-msg-1 err-msg-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (run-main-loop-32)
  (begin
    (let ((sub-name "run-main-loop-32")
          (test-fac-list
           (list
            (list 2) (list 3) (list 4) (list 5) (list 6)
            (list 7) (list 8) (list 9) (list 10)))
          (test-pi-list
           (list
            (list 100) (list 1000)))
          (tolerance 1e-12))
      (begin
        ;;; first display factorial calculations
        (for-each
         (lambda (tlist)
           (begin
             (let ((nn (list-ref tlist 0)))
               (let ((frec (fac-rec nn tolerance))
                     (fiter (fac-iter nn tolerance)))
                 (let ((diff (abs (- frec fiter))))
                   (begin
                     (if (> diff tolerance)
                         (begin
                           (display
                            (ice-9-format:format
                             #f "factorial-recursive(~a) = ~:d~%"
                             nn frec))
                           (display
                            (ice-9-format:format
                             #f "factorial-iterative(~a) = ~:d~%"
                             nn fiter))
                           (force-output))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "factorial-recursive(~a) = ~:d = "
                             nn frec))
                           (display
                            (format
                             #f "factorial_iterative(~a)~%" nn))
                           (force-output)
                           ))
                     ))
                 ))
             )) test-fac-list)

        ;;; next display pi calculations
        (newline)
        (force-output)

        (for-each
         (lambda (tlist)
           (begin
             (let ((nn (list-ref tlist 0)))
               (let ((pi-rec (pi-rec nn tolerance))
                     (pi-iter (pi-iter nn tolerance)))
                 (let ((diff (abs (- pi-rec pi-iter))))
                   (begin
                     (if (> diff tolerance)
                         (begin
                           (display
                            (ice-9-format:format
                             #f "pi-recursive(~:d) = ~a~%"
                             nn (exact->inexact pi-rec)))
                           (display
                            (ice-9-format:format
                             #f "pi-iterative(~:d) = ~a~%"
                             nn (exact->inexact pi-iter)))
                           (force-output))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "pi-recursive(~:d) = ~a = "
                             nn (exact->inexact pi-rec)))
                           (display
                            (ice-9-format:format
                             #f "pi_iterative(~:d)~%" nn))
                           (force-output)
                           ))
                     ))
                 ))
             )) test-pi-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-pi)
  (begin
    (let ((n-list (list 10000 100000))
          (tolerance 1e-12))
      (begin
        (for-each
         (lambda (num)
           (begin
             (newline)
             (let ((pi-iter (pi-iter num tolerance)))
               (begin
                 (display
                  (ice-9-format:format
                   #f "pi-iterative(~:d) = ~a~%"
                   num (exact->inexact pi-iter)))
                 (force-output)
                 ))
             )) n-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-a)
  (begin
    (let ((tolerance 1e-12))
      (begin
        (display (format #f "(a)~%"))
        (force-output)
        (do ((ii 2 (1+ ii)))
            ((> ii 10))
          (begin
            (let ((sumsq
                   (sum-squares-primes 1 ii tolerance)))
              (begin
                (display
                 (ice-9-format:format
                  #f "sum of squares of primes(~:d, ~:d] = ~:d~%"
                  1 ii sumsq))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-b)
  (begin
    (let ((tolerance 1e-12))
      (begin
        (display (format #f "(b)~%"))
        (force-output)
        (do ((ii 2 (1+ ii)))
            ((> ii 10))
          (begin
            (let ((rpp
                   (relatively-prime-product ii tolerance)))
              (begin
                (display
                 (ice-9-format:format
                  #f "relatively prime product(1, ~:d) = ~:d~%"
                  ii rpp))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "a. Show that sum and product (exercise "))
    (display
     (format #f "1.31) are both special~%"))
    (display
     (format #f "cases of a still more general notion "))
    (display
     (format #f "called accumulate that~%"))
    (display
     (format #f "combines a collection of terms, using "))
    (display
     (format #f "some general accumulation~%"))
    (display
     (format #f "function:~%"))
    (display
     (format #f "(accumulate combiner null-value term a next b)~%"))
    (newline)
    (display
     (format #f "Accumulate takes as arguments the same "))
    (display
     (format #f "term and range~%"))
    (display
     (format #f "specifications as sum and product, "))
    (display
     (format #f "together with a~%"))
    (display
     (format #f "combiner procedure (of two arguments) "))
    (display
     (format #f "that specifies how~%"))
    (display
     (format #f "the current term is to be combined "))
    (display
     (format #f "with the accumulation~%"))
    (display
     (format #f "of the preceding terms and a null-value "))
    (display
     (format #f "that specifies what~%"))
    (display
     (format #f "base value to use when the terms run out. "))
    (display
     (format #f "Write accumulate and~%"))
    (display
     (format #f "show how sum and product can both be "))
    (display
     (format #f "defined as simple calls~%"))
    (display
     (format #f "to accumulate.~%"))
    (newline)
    (display
     (format #f "b. If your accumulate procedure generates "))
    (display
     (format #f "a recursive process,~%"))
    (display
     (format #f "write one that generates an iterative "))
    (display
     (format #f "process. If it~%"))
    (display
     (format #f "generates an iterative process, write one "))
    (display
     (format #f "that generates a~%"))
    (display
     (format #f "recursive process.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.33 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (run-main-loop-32)

             (newline)
             (main-loop-pi)

             (newline)
             (main-loop-a)
             (newline)
             (main-loop-b)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
