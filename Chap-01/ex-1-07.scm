#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.7                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for testing functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (square x)
  (begin
    (* x x)
    ))

;;;#############################################################
;;;#############################################################
(define (improve guess x)
  (begin
    (average guess (/ x guess))
    ))

;;;#############################################################
;;;#############################################################
(define (average x y)
  (begin
    (/ (+ x y) 2)
    ))

;;;#############################################################
;;;#############################################################
(define (good-enough? guess x tol)
  (begin
    (let ((g-sqr (square guess)))
      (begin
        (if (not (zero? g-sqr))
            (begin
              (let ((abs-pcnt-change
                     (abs
                      (/ (- g-sqr x) g-sqr))))
                (begin
                  (< abs-pcnt-change tol)
                  )))
            (begin
              (< (abs (- g-sqr x)) tol)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-iter guess x tol)
  (begin
    (if (good-enough? guess x tol)
        (begin
          (exact->inexact guess))
        (begin
          (sqrt-iter
           (improve guess x)
           x tol)
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sqrt-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-sqrt-iter-1")
         (test-list
          (list
           (list 1 2 1e-6 1.414214)
           (list 1 3 1e-6 1.732051)
           (list 1 4 1e-6 2.000000)
           (list 1 5 1e-6 2.236068)
           (list 1 6 1e-6 2.449490)
           (list 1 7 1e-6 2.645751)
           (list 1 8 1e-6 2.828427)
           (list 1 9 1e-6 3.000000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((guess (list-ref this-list 0))
                  (xx (list-ref this-list 1))
                  (tol (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (sqrt-iter guess xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : guess=~a, x=~a, tol=~a, "
                        sub-name test-label-index guess xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sicp-sqrt x tol)
  (begin
    (sqrt-iter 1.0 x tol)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sicp-sqrt-1 result-hash-table)
 (begin
   (let ((sub-name "test-sicp-sqrt-1")
         (test-list
          (list
           (list 1e-6 1e-6 0.001000)
           (list 2 1e-6 1.414214)
           (list 3 1e-6 1.732051)
           (list 4 1e-6 2.000000)
           (list 5 1e-6 2.236068)
           (list 6 1e-6 2.449490)
           (list 7 1e-6 2.645751)
           (list 8 1e-6 2.828427)
           (list 9 1e-6 3.000000)
           (list 100000000 1e-6 10000.000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (tol (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (sicp-sqrt xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : x=~a, tol=~a, "
                        sub-name test-label-index xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((sub-name "main-loop")
          (test-list
           (list
            (list 1e-12 1e-6 1e-6)
            (list 1e-8 1e-6 1e-4)
            (list 1e-6 1e-6 1e-3)
            (list 1 1e-6 1.000)
            (list 2 1e-6 1.414214)
            (list 3 1e-6 1.732051)
            (list 4 1e-6 2.000000)
            (list 5 1e-6 2.236068)
            (list 6 1e-6 2.449490)
            (list 7 1e-6 2.645751)
            (list 8 1e-6 2.828427)
            (list 9 1e-6 3.000000)
            (list 100000000 1e-6 10000.000)
            ))
          (test-label-index 0))
      (begin
        (for-each
         (lambda (this-list)
           (begin
             (let ((xx (list-ref this-list 0))
                   (tol (list-ref this-list 1))
                   (shouldbe (list-ref this-list 2)))
               (let ((result (sicp-sqrt xx tol)))
                 (begin
                   (if (> (abs (- shouldbe result)) tol)
                       (begin
                         (display
                          (format
                           #f "~a : error (~a) : x=~a, tol=~a, "
                           sub-name test-label-index xx tol))
                         (display
                          (format
                           #f "shouldbe=~a, result=~a~%"
                           shouldbe result)))
                       (begin
                         (display
                          (ice-9-format:format
                           #f "sqrt(~a) = ~6,4f : sqrt(~a)^2 = ~6,4f"
                           xx result xx (* result result)))
                         (display
                          (ice-9-format:format
                           #f " : (tol = ~a)~%" tol))
                         ))
                   (force-output)
                   )))
             (set! test-label-index (1+ test-label-index))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Design a square-root procedure that "))
    (display
     (format #f "uses this kind~%"))
    (display
     (format #f "of end test (percentage tolerance).~%"))
    (newline)
    (display
     (format #f "Good enough used a tolerance that "))
    (display
     (format #f "was fixed, so~%"))
    (display
     (format #f "if you are trying to compute the "))
    (display
     (format #f "square root of a~%"))
    (display
     (format #f "small number like 0.001, you may not "))
    (display
     (format #f "get the right~%"))
    (display
     (format #f "answer if your tolerance is 0.010.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.7 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (force-output)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
