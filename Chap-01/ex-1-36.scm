#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.36                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 5, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (fixed-point func first-guess tolerance)
  (define (local-try guess max-iter nstep tol)
    (begin
      (let ((next (func guess)))
        (let ((abs-diff (abs (- next guess))))
          (begin
            (if (> nstep max-iter)
                (begin
                  (display
                   (ice-9-format:format
                    #f "max iterations ~:d exceeded, "
                    max-iter))
                  (display
                   (ice-9-format:format
                    #f "stopping program...~%"))
                  (force-output)
                  (quit))
                (begin
                  (display
                   (format #f "(~a) guess=~12,8f, next=~12,8f, "
                           nstep guess next))
                  (display
                   (format #f "abs-diff=~12,8f, tol=~a~%"
                           abs-diff tolerance))
                  (force-output)
                  (if (< abs-diff tol)
                      (begin
                        next)
                      (begin
                        (local-try
                         next max-iter (1+ nstep) tol)
                        ))
                  ))
            )))
      ))
  (begin
    (let ((max-iterations 1000))
      (begin
        (local-try
         first-guess max-iterations 0 tolerance)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (x-to-x-is-1000 tolerance)
  (define (local-func xx)
    (begin
      (/ (log 1000) (log xx))
      ))
  (begin
    (fixed-point local-func 3.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (x-to-x-avg-damp tolerance)
  (define (local-func xx)
    (begin
      (/
       (+ xx
          (/ (log 1000.0) (log xx)))
       2.0)
      ))
  (begin
    (fixed-point local-func 3.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Modify fixed-point so that it prints "))
    (display
     (format #f "the sequence of~%"))
    (display
     (format #f "approximations it generates, using "))
    (display
     (format #f "the newline and display~%"))
    (display
     (format #f "primatives shown in exercise 1.22. "))
    (display
     (format #f "Then find a solution to~%"))
    (display
     (format #f "x^x = 1000 by finding a fixed "))
    (display
     (format #f "point of~%"))
    (display
     (format #f "x -> log(1000)/log(x).~%"))
    (display
     (format #f "(Use Scheme's primitive log procedure, "))
    (display
     (format #f "which computes natural~%"))
    (display
     (format #f "logarithms.) Compare the number "))
    (display
     (format #f "of steps this takes with~%"))
    (display
     (format #f "and without average damping. (Note "))
    (display
     (format #f "that you cannot start~%"))
    (display
     (format #f "fixed-point with a guess of 1, "))
    (display
     (format #f "as this would cause division~%"))
    (display
     (format #f "by log(1) = 0.)~%"))
    (newline)

    (display
     (format #f "Took 37 steps to find the fixed point "))
    (display
     (format #f "without average damping,~%"))
    (display
     (format #f "and just 8 steps to find it with "))
    (display
     (format #f "average damping.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.36 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)
          (force-output)

          (timer-module:time-code-macro
           (begin
             (display
              (format #f "find the fixed point of x^x = 1000.~%"))
             (let ((tol 1e-6))
               (begin
                 (x-to-x-is-1000 tol)
                 ))
             (newline)
             ))

          (timer-module:time-code-macro
           (begin
             (display
              (format #f "find the fixed point of x^x = 1000~%"))
             (display
              (format #f "with average damping.~%"))
             (let ((tol 1e-6))
               (begin
                 (x-to-x-avg-damp tol)
                 ))
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
