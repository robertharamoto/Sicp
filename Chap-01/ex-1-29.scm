#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.29                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 4, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (sum-iter term aa next bb acc tolerance)
  (begin
    (if (and (> aa bb)
             (> (abs (- aa bb)) tolerance))
        (begin
          acc)
        (begin
          (let ((next-acc
                 (+ acc (term aa))))
            (begin
              (sum-iter
               term (next aa) next bb next-acc tolerance)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pi-sum aa bb tolerance)
  (define (pi-term xx)
    (begin
      (/ 1.0 (* xx (+ xx 2)))
      ))
  (define (pi-next xx)
    (begin
      (+ xx 4)
      ))
  (begin
    (let ((result
           (* 8
              (sum-iter
               pi-term aa pi-next bb 0 tolerance))))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pi-sum-1 result-hash-table)
 (begin
   (let ((sub-name "test-pi-sum-1")
         (test-list
          (list
           (list 1 1000 3.139592655589783)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa (list-ref this-list 0))
                  (bb (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (pi-sum aa bb tolerance)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : aa=~a, bb=~a, "
                        sub-name test-label-index aa bb))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (cube xx)
  (begin
    (* xx xx xx)
    ))

;;;#############################################################
;;;#############################################################
(define (integral f a b dx tolerance)
  (define (add-dx x)
    (begin
      (+ x dx)
      ))
  (begin
    (let ((result
           (* (sum-iter f (+ a (/ dx 2.0)) add-dx b 0 tolerance)
              dx)))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integral-1 result-hash-table)
 (begin
   (let ((sub-name "test-integral-1")
         (test-list
          (list
           (list cube 0 1 0.01 .24998750000000042)
           (list cube 0 1 0.001 .249999875000001)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((func (list-ref this-list 0))
                  (aa (list-ref this-list 1))
                  (bb (list-ref this-list 2))
                  (dx (list-ref this-list 3))
                  (shouldbe (list-ref this-list 4)))
              (let ((result (integral func aa bb dx tolerance)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : aa=~a, bb=~a, "
                        sub-name test-label-index aa bb))
                      (err-msg-2
                       (format
                        #f "dx=~a, shouldbe=~a, result=~a"
                        dx shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (simpsons-rule func aa bb nn tolerance)
  (define (local-term-0 func xx kk nn)
    (begin
      (let ((yy (func xx)))
        (begin
          (cond
           ((= kk 0) yy)
           ((= kk nn) yy)
           ((even? kk)
            (begin
              (* 2 yy)
              ))
           (else
            (begin
              (* 4 yy)
              )))
          ))
      ))
  (define (local-next aa hh)
    (begin
      (+ aa hh)
      ))
  (begin
    (let ((hh (exact->inexact (/ (- bb aa) nn)))
          (kk 0))
      (let ((final-factor (/ hh 3.0))
            (sum-result
             (sum-iter
              (lambda (xx)
                (begin
                  (let ((result
                         (local-term-0 func xx kk nn)))
                    (begin
                      (set! kk (1+ kk))
                      result))
                  ))
              aa
              (lambda (anum) (local-next anum hh))
              bb 0.0 tolerance)))
        (begin
          (* final-factor sum-result)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-simpsons-rule-1 result-hash-table)
 (begin
   (let ((sub-name "test-simpsons-rule-1")
         (test-list
          (list
           (list cube 0 1 100 0.25)
           (list cube 0 1 1000 0.25)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((func (list-ref this-list 0))
                  (aa (list-ref this-list 1))
                  (bb (list-ref this-list 2))
                  (nn (list-ref this-list 3))
                  (shouldbe (list-ref this-list 4)))
              (let ((result
                     (simpsons-rule func aa bb nn tolerance)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : aa=~a, bb=~a, "
                        sub-name test-label-index aa bb))
                      (err-msg-2
                       (format
                        #f "nn=~a, shouldbe=~a, result=~a~%"
                        nn shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list 0 1 0.01 100)
            (list 0 1 0.001 1000)))
          (tolerance 1e-12)
          (test-label-index 0))
      (begin
        (for-each
         (lambda (tlist)
           (begin
             (let ((aa (list-ref tlist 0))
                   (bb (list-ref tlist 1))
                   (dx (list-ref tlist 2))
                   (nn (list-ref tlist 3)))
               (let ((int-sum
                      (integral
                       cube aa bb dx tolerance))
                     (simp-sum
                      (simpsons-rule
                       cube aa bb nn tolerance)))
                 (let ((diff (abs (- simp-sum int-sum))))
                   (begin
                     (if (> simp-sum 0)
                         (begin
                           (let ((pcnt-diff (/ diff simp-sum))
                                 (text1
                                  (format
                                   #f "integral(cube, ~a, ~a, dx=~a)"
                                   aa bb dx))
                                 (text2
                                  (ice-9-format:format
                                   #f "simpsons-rule(cube, ~a, ~a, n=~:d)"
                                   aa bb nn)))
                             (begin
                               (display
                                (ice-9-format:format
                                 #f "~a = ~12,8f~%" text1 int-sum))
                               (display
                                (ice-9-format:format
                                 #f "~a = ~12,8f~%" text2 simp-sum))
                               (display
                                (ice-9-format:format
                                 #f "absolute difference = ~8,4g~%"
                                 diff))
                               (display
                                (ice-9-format:format
                                 #f "percent difference = ~8,4f%~%"
                                 (* 100.0 pcnt-diff)))
                               (force-output)
                               )))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "integral(cube, ~a, ~a, "
                             aa bb))
                           (display
                            (ice-9-format:format
                             #f "dx=~a) = ~a~%"
                             dx int-sum))
                           (display
                            (ice-9-format:format
                             #f "simpsons-rule(cube, ~a, ~a, "
                             aa bb))
                           (display
                            (ice-9-format:format
                             #f "n=~a) = ~a~%"
                             nn simp-sum))
                           (force-output)
                           ))

                     (newline)
                     ))
                 ))
             (set! test-label-index (1+ test-label-index))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Simpson's Rule is a more accurate method "))
    (display
     (format #f "of numerical~%"))
    (display
     (format #f "integration than the method illustrated "))
    (display
     (format #f "above. Using~%"))
    (display
     (format #f "Simpson's Rule, the integral of a "))
    (display
     (format #f "function f between~%"))
    (display
     (format #f "a and b is approximated as:~%"))
    (newline)
    (display
     (format #f "(h/3)*[y_0+4y_1+2y_2+4y_3+2y_4+"))
    (display
     (format #f "...+2y_(n-2)+4y_(n-1)+y_n]~%"))
    (newline)
    (display
     (format #f "where h = (b - a)/n, for some even "))
    (display
     (format #f "integer n, and~%"))
    (display
     (format #f "y_k = f(a + kh). (Increasing n "))
    (display
     (format #f "increases the accuracy~%"))
    (display
     (format #f "of the approximation.) Define a "))
    (display
     (format #f "a procedure that takes~%"))
    (display
     (format #f "as arguments f, a, b, and n, and returns "))
    (display
     (format #f "the value of the~%"))
    (display
     (format #f "integral, computed using Simpson's "))
    (display
     (format #f "Rule. Use your~%"))
    (display
     (format #f "procedure to integrate cube between 0 "))
    (display
     (format #f "and 1 (with~%"))
    (display
     (format #f "n = 100 and n = 1000), and compare the "))
    (display
     (format #f "results to those of~%"))
    (display
     (format #f "the integral procedure shown above.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.29 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
