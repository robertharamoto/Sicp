#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.18                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (fast-mult-iterative aa bb)
  (begin
    (cond
     ((< bb 0)
      (begin
        (let ((xx-list
               (fast-mult-iterative (* -1 aa) (* -1 bb))))
          (let ((xx-value (list-ref xx-list 0))
                (xx-count (list-ref xx-list 1)))
            (begin
              (list xx-value (1+ xx-count))
              )))
        ))
     ((= bb 0)
      (begin
        (list 0 0)
        ))
     ((even? bb)
      (begin
        (let ((xx-list
               (fast-mult-iterative aa (/ bb 2))))
          (let ((xx-value (list-ref xx-list 0))
                (xx-count (list-ref xx-list 1)))
            (begin
              (list (+ xx-value xx-value) (1+ xx-count))
              )))
        ))
     (else
      (begin
        (let ((xx-list
               (fast-mult-iterative aa (1- bb))))
          (let ((xx-value (list-ref xx-list 0))
                (xx-count (list-ref xx-list 1)))
            (begin
              (list (+ xx-value aa) (1+ xx-count))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fast-mult-iterative-1 result-hash-table)
 (begin
   (let ((sub-name "test-fast-mult-iterative-1")
         (test-list
          (list
           (list 2 0 0 0) (list 2 1 2 1) (list 2 2 4 2)
           (list 2 3 6 3) (list 2 4 8 3) (list 2 5 10 4)
           (list 2 6 12 4) (list 2 7 14 5) (list 2 8 16 4)
           (list 2 -3 -6 4) (list 2 -4 -8 4)
           (list 2 -5 -10 5)
           (list 3 0 0 0) (list 3 1 3 1) (list 3 2 6 2)
           (list 3 3 9 3) (list 3 4 12 3) (list 3 5 15 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((bb (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (shouldbe-value (list-ref this-list 2))
                  (shouldbe-count (list-ref this-list 3)))
              (let ((rlist (fast-mult-iterative bb nn)))
                (let ((result-value (list-ref rlist 0))
                      (result-count (list-ref rlist 1)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : bb=~a, nn=~a, "
                          sub-name test-label-index bb nn))
                        (err-2
                         (format
                          #f "shouldbe-value=~a, result=~a"
                          shouldbe-value result-value))
                        (err-3
                         (format
                          #f "shouldbe-count=~a, result=~a"
                          shouldbe-count result-count)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-value
                               result-value)
                       sub-name
                       (string-append
                        err-1 err-2)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? shouldbe-count
                               result-count)
                       sub-name
                       (string-append
                        err-1 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-loop aa bb)
  (begin
    (let ((mult-rec-list (fast-mult-iterative aa bb)))
      (let ((mult-value (list-ref mult-rec-list 0))
            (mult-count (list-ref mult-rec-list 1)))
        (begin
          (display
           (ice-9-format:format
            #f "~a * ~a = ~:d  :  count = ~:d~%"
            aa bb mult-value mult-count))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((aa 2))
      (begin
        (do ((bb 0 (1+ bb)))
            ((> bb 10))
          (begin
            (sub-loop aa bb)
            (force-output)
            ))

        (newline)
        (sub-loop 2 20)
        (sub-loop 2 30)
        (sub-loop 2 50)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Design an iterative multiplication "))
    (display
     (format #f "procedure analogous~%"))
    (display
     (format #f "to fast-expt that uses a logarithmic "))
    (display
     (format #f "number of steps.~%"))
    (newline)
    (display
     (format #f "See the websites:~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Exponentiation_by_squaring~%"))
    (display
     (format #f "https://www.geeksforgeeks.org/exponential-squaring-fast-modulo-multiplication/~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.18 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
