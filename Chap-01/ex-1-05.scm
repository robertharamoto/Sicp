#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.5                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle has invented a test "))
    (display
     (format #f "to determine whether~%"))
    (display
     (format #f "the interpreter he is faced with is "))
    (display
     (format #f "using applicative-order~%"))
    (display
     (format #f "evaluation or normal-order evaluation. "))
    (display
     (format #f "He defines the~%"))
    (display
     (format #f "following two procedures:~%"))
    (display
     (format #f "(define (p) (p))~%"))
    (display
     (format #f "(define (test x y)~%"))
    (display
     (format #f "  (if (= x 0)~%"))
    (display
     (format #f "      0~%"))
    (display
     (format #f "      y))~%"))
    (newline)
    (display
     (format #f "Then he evaluates the expression~%"))
    (display
     (format #f "(test 0 (p))~%"))
    (display
     (format #f "What behavior will Ben observe with an "))
    (display
     (format #f "interpreter that~%"))
    (display
     (format #f "uses applicative-order evaluation?~%"))
    (newline)
    (display
     (format #f "An infinite loop. In applicative "))
    (display
     (format #f "order evaluation,~%"))
    (display
     (format #f "the arguments are evaluated first, "))
    (display
     (format #f "so in the expression~%"))
    (display
     (format #f "(test 0 (p)), (p) will send the "))
    (display
     (format #f "interpreter off into an~%"))
    (display
     (format #f "infinite loop.~%"))
    (newline)
    (display
     (format #f "What behavior will he observe with "))
    (display
     (format #f "an interpreter that~%"))
    (display
     (format #f "uses normal-order evaluation?~%"))
    (newline)
    (display
     (format #f "In a normal-order evaluation, the "))
    (display
     (format #f "interpreter will~%"))
    (display
     (format #f "expand the expression before "))
    (display
     (format #f "evaluating, so~%"))
    (display
     (format #f "(test 0 (p)) -> (if (= 0 0) 0 (p))~%"))
    (display
     (format #f "-> 0, since the predicate is true, "))
    (display
     (format #f "and (p) is~%"))
    (display
     (format #f "not evaluated at all.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.5 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
