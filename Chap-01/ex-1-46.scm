#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.46                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (iterative-improve
         good-enough? improve-guess tolerance max-iterations)
  (begin
    (lambda (guess)
      (begin
        (let ((next-guess (improve-guess guess))
              (count 0))
          (begin
            (while (and (not (good-enough? guess next-guess tolerance))
                        (<= count max-iterations))
                   (begin
                     (let ((next (improve-guess next-guess)))
                       (begin
                         (set! guess next-guess)
                         (set! next-guess next)
                         (set! count (1+ count))
                         ))
                     ))
            (if (good-enough? guess next-guess tolerance)
                (begin
                  next-guess)
                (begin
                  (if (> count max-iterations)
                      (begin
                        (display
                         (ice-9-format:format
                          #f "max iterations ~:d exceeded, "
                          max-iterations))
                        (display
                         (ice-9-format:format
                          #f "stopping program...~%"))
                        (force-output)
                        (quit))
                      (begin
                        next-guess
                        ))
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (good-enough? guess next-guess tolerance)
  (begin
    (<= (abs (- guess next-guess)) tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (fixed-point func first-guess tolerance)
  (begin
    (let ((max-iterations 1000))
      (let ((iter-func
             (iterative-improve
              good-enough? func tolerance
              max-iterations)))
        (begin
          (iter-func first-guess)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (average-damp func)
  (define (average a b)
    (begin
      (/ (+ a b) 2)
      ))
  (begin
    (lambda (xx)
      (begin
        (average xx (func xx))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (square-root xx tolerance)
  (begin
    (fixed-point
     (average-damp
      (lambda (y)
        (begin
          (/ xx y)
          )))
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
;;; nth - nth root
;;; mm - number of times to average damp
(define (check-sqrt-root xx tol)
  (begin
    (let ((system-sqrt-root (sqrt xx)))
      (let ((ex-46-sqrt (square-root xx tol)))
        (let ((diff (abs (- ex-46-sqrt system-sqrt-root))))
          (begin
            (display
             (ice-9-format:format
              #f "(sqrt-root ~a) = ~12,10f, "
              xx ex-46-sqrt))
            (display
             (ice-9-format:format
              #f "diff = ~6,4e, (tol = ~a)~%"
              diff tol))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Several of the numerical methods "))
    (display
     (format #f "described in this chapter~%"))
    (display
     (format #f "are instances of an extremely general "))
    (display
     (format #f "computational strategy~%"))
    (display
     (format #f "known as iterative improvement. "))
    (display
     (format #f "Iterative improvement says~%"))
    (display
     (format #f "that, to compute something, we start "))
    (display
     (format #f "with an initial guess~%"))
    (display
     (format #f "for the answer, test if the guess is "))
    (display
     (format #f "good enough, and~%"))
    (display
     (format #f "otherwise improve the guess and "))
    (display
     (format #f "continue the process~%"))
    (display
     (format #f "using the improved guess as the new "))
    (display
     (format #f "guess. Write a procedure~%"))
    (display
     (format #f "iterative-improve that takes two "))
    (display
     (format #f "procedures as arguments:~%"))
    (display
     (format #f "a method for telling whether a guess "))
    (display
     (format #f "is good enough~%"))
    (display
     (format #f "and a method for improving a guess. "))
    (display
     (format #f "Iterative-improve should~%"))
    (display
     (format #f "return as its value a procedure that "))
    (display
     (format #f "takes a guess~%"))
    (display
     (format #f "as argument and keeps improving the "))
    (display
     (format #f "guess until it is~%"))
    (display
     (format #f "good enough. Rewrite the sqrt procedure "))
    (display
     (format #f "of section 1.1.7~%"))
    (display
     (format #f "and the fixed-point procedure of "))
    (display
     (format #f "section 1.3.3 in~%"))
    (display
     (format #f "terms of iterative-improve.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.46 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "square roots~%"))
             (let ((tol 1e-9)
                   (test-list
                    (list
                     (list 2.0) (list 3.0) (list 4.0) (list 5.0)
                     (list 6.0) (list 7.0) (list 8.0) (list 9.0))))
               (begin
                 (for-each
                  (lambda (alist)
                    (begin
                      (let ((xx (list-ref alist 0)))
                        (begin
                          (check-sqrt-root xx tol)
                          ))
                      )) test-list)
                 ))
             (newline)
             ))

          (newline)
          (display
           (format
            #f "~a~%"
            (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
