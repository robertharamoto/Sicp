#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.41                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 6, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (double afunc)
  (begin
    (lambda (xx)
      (begin
        (afunc (afunc xx))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (inc xx)
  (begin
    (+ xx 1)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a procedure double that takes "))
    (display
     (format #f "a procedure of one~%"))
    (display
     (format #f "argument as argument and returns a "))
    (display
     (format #f "procedure that applies the~%"))
    (display
     (format #f "original procedure twice. For example, "))
    (display
     (format #f "if inc is a procedure~%"))
    (display
     (format #f "that adds 1 to its argument, then "))
    (display
     (format #f "(double inc) should~%"))
    (display
     (format #f "be a procedure that adds 2.  What "))
    (display
     (format #f "value is returned~%"))
    (display
     (format #f "by (((double (double double)) inc) 5)~%"))
    (newline)
    (display
     (format #f "(define (double afunc)~%"))
    (display
     (format #f "  (lambda (xx) (afunc (afunc xx))))~%"))
    (newline)
    (display
     (format #f "Applicative order evaluates the arguments "))
    (display
     (format #f "first, then evaluates~%"))
    (display
     (format #f "the function.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "(((double (double double)) inc) 5)~%"))
    ;;; no arguments need to be evaluated, so evalute the function
    (display
     (format #f "(1) -> (((lambda (x) "))
    (display
     (format #f "((double double) ~%"))
    (display
     (format #f "((double double) x))) inc) 5) "))
    (display
     (format #f "= ~a~%"
             (((lambda (x) ((double double)
                            ((double double) x)))
               inc) 5)))
    (display
     (format #f "(2) -> (((double double)~%"))
    (display
     (format #f "((double double) inc)) 5) = ~a~%"
             (((double double) ((double double) inc)) 5)))
    ;;; no arguments need to be evaluated, so evalute the function
    (display
     (format #f "(3) -> (((lambda (x) (double (double x)))~%"))
    (display
     (format #f "((lambda (x) (double (double x))) inc)) 5)~%"))
    (display
     (format #f "= ~a~%"
             (((lambda (x) (double (double x)))
               ((lambda (x) (double (double x))) inc)) 5)))
    ;;; arguments to evaluate before completing the function valuation
    (display
     (format #f "(4) -> (((lambda (x) (double (double x)))~%"))
    (display
     (format #f "(lambda (z) ((double (double inc)) z))) 5)~%"))
    (display
     (format #f "= ~a~%"
             (((lambda (x) (double (double x)))
               (lambda (z)
                 ((double (double inc)) z))) 5)))
    (display
     (format #f "(5) -> (((lambda (x) (double (double x)))~%"))
    (display
     (format #f "(lambda (z) ((double (lambda (y)~%"))
    (display
     (format #f "(inc (inc y)))) z))) 5) = ~a~%"
             (((lambda (x) (double (double x)))
               (lambda (z) ((double
                             (lambda (y)
                               (inc (inc y)))) z))) 5)))
    (display
     (format #f "(6) -> (((lambda (x)~%"))
    (display
     (format #f "          (double (double x)))~%"))
    (display
     (format #f "            (lambda (z)~%"))
    (display
     (format #f "    (inc (inc (inc (inc z)))))) 5)~%"))
    (display
     (format #f "= ~a~%"
             (((lambda (x) (double (double x)))
               (lambda (z) (inc (inc (inc (inc z)))))) 5)))
    (display
     (format #f "(7) -> ((lambda (x) ~%"))
    (display
     (format #f "  ((double (double~%"))
    (display
     (format #f "    (lambda (z) (inc (inc (inc (inc z)~%"))
    (display
     (format #f "       )))))) x)) 5) = ~a~%"
             ((lambda (x) ((double
                            (double
                             (lambda (z)
                               (inc (inc (inc (inc z)))))))
                           x)) 5)))
    (display
     (format #f "(8) -> ((lambda (x)~%"))
    (display
     (format #f "  ((double (lambda (z)~%"))
    (display
     (format #f "    (inc (inc (inc (inc (inc~%"))
    (display
     (format #f "      (inc (inc (inc z)))))))))) x)) 5)~%"))
    (display
     (format
      #f "= ~a~%"
      ((lambda (x) ((double
                     (lambda (z)
                       (inc (inc
                             (inc
                              (inc
                               (inc
                                (inc
                                 (inc
                                  (inc z))))))))
                       )) x)) 5)))
    (display
     (format #f "(9) -> ((lambda (x)~%"))
    (display
     (format #f "(inc (inc (inc (inc (inc (inc (inc~%"))
    (display
     (format #f "(inc (inc (inc (inc (inc (inc (inc~%"))
    (display
     (format
      #f "(inc (inc x))))))))))))))))) 5) = ~a~%"
      ((lambda (x)
         (inc
          (inc
           (inc
            (inc
             (inc
              (inc
               (inc
                (inc
                 (inc
                  (inc
                   (inc
                    (inc
                     (inc
                      (inc
                       (inc
                        (inc x)))))))))))))))))
       5)))

    (newline)
    (display
     (format #f "(((double (double double)) inc) 5) = ~a~%"
             (((double (double double)) inc) 5)))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.41 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (main-loop)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
