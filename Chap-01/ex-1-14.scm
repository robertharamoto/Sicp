#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.14                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "count-change(11)~%"))
    (display
     (format #f "cc(11 5)~%"))
    (display
     (format #f "cc(11 4) + cc(-39 5)~%"))
    (display
     (format #f "cc(11 3) + cc(-14 4) + 0~%"))
    (display
     (format #f "cc(11 2) + cc(1 3) + 0 + 0~%"))
    (display
     (format #f "cc(11 1) + cc(6 2) + cc(1 2) + cc(-9 3) + 0 + 0~%"))
    (display
     (format #f "cc(11 0) + cc(10 1) + cc(6 1) + cc(1 2) + cc(1 1) + cc(-4 2) + 0 + 0 + 0~%"))
    (display
     (format #f "0 + cc(10 0) + cc(9 1) + cc(6 0) + cc(5 1) + cc(1 1) + cc(-4 2) + cc(1 0) + cc(0 1) + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + cc(9 0) + cc(8 1) + 0 + cc(5 0) + cc(4 1) + cc(1 0) + cc(0 1) + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + cc(8 0) + cc(7 1) + 0 + 0 + cc(4 0) + cc(3 1) + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + cc(7 0) + cc(6 1) + 0 + 0 + 0 + cc(3 0) + cc(2 1) + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + 0 + cc(6 0) + cc(5 1) + 0 + 0 + 0 + 0 + cc(2 0) + cc(1 1) + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + 0 + 0 + cc(5 0) + cc(4 1) + 0 + 0 + 0 + 0 + 0 + cc(1 0) + cc(0 1)+ 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + 0 + 0 + 0 + cc(4 0) + cc(3 1) + 0 + 0 + 0 + 0 + 0 + 0 + 1 + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + cc(3 0) + cc(2 1) + 0 + 0 + 0 + 0 + 0 + 0 + 1 + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + cc(2 0) + cc(1 1) + 0 + 0 + 0 + 0 + 0 + 0 + 1 + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + cc(1 0) + cc(0 1) + 0 + 0 + 0 + 0 + 0 + 0 + 1 + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + 0 + 1 + 0 + 0 + 0 + 0 + 0 + 0 + 1 + 0 + 1 + 0 + 0 + 1 + 0 + 0 + 0 + 0~%"))
    (display
     (format #f "= 4~%"))
    (newline)
    (display
     (format #f "The order of growth of the space "))
    (display
     (format #f "and number of~%"))
    (display
     (format #f "steps is theta(amount) for both.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.14 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
