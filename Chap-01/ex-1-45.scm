#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.45                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 6, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (compose a-func b-func)
  (begin
    (lambda (xx)
      (begin
        (a-func (b-func xx))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (repeated a-func nn)
  (begin
    (let ((result-func a-func)
          (nmax (1- nn)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (let ((next-func
                   (compose a-func result-func)))
              (begin
                (set! result-func next-func)
                ))
            ))
        result-func
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (fixed-point func first-guess tolerance)
  (define (local-try guess max-iter nstep)
    (begin
      (let ((next (func guess)))
        (let ((abs-diff (abs (- next guess))))
          (begin
            (if (> nstep max-iter)
                (begin
                  (display
                   (ice-9-format:format
                    #f "max iterations ~:d exceeded, "
                    max-iter))
                  (display
                   (ice-9-format:format
                    #f "stopping program...~%"))
                  (force-output)
                  (quit))
                (begin
                  (if (< abs-diff tolerance)
                      (begin
                        next)
                      (begin
                        (local-try next max-iter (1+ nstep))
                        ))
                  ))
            )))
      ))
  (begin
    (let ((max-iterations 1000))
      (begin
        (local-try first-guess max-iterations 0)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (average-damp func)
  (define (average a b)
    (begin
      (/ (+ a b) 2)
      ))
  (begin
    (lambda (xx)
      (begin
        (average xx (func xx))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (smoothed func)
  (define (avg-3 a b c)
    (begin
      (/ (+ a b c) 3.0)
      ))
  (define dx 1e-6)
  (begin
    (lambda (x)
      (begin
        (avg-3 (func (- x dx)) (func x) (func (+ x dx)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (n-fold-avg-damped func n)
  (begin
    (repeated
     (average-damp func) n)
    ))

;;;#############################################################
;;;#############################################################
(define (square-root xx tolerance)
  (begin
    (fixed-point
     (average-damp (lambda (y) (/ xx y)))
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (cube-root xx tolerance)
  (begin
    (fixed-point
     (average-damp (lambda (y) (/ xx (* y y))))
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (nth-root xx nn mm tolerance)
  (begin
    (fixed-point
     (n-fold-avg-damped
      (lambda (y) (/ xx (expt y (- nn 1))))
      mm)
     1.2 tolerance)
    ))

;;;#############################################################
;;;#############################################################
;;; nth - nth root
;;; mm - number of times to average damp
(define (check-nth-root-nfold xx nth mm)
  (begin
    (let ((tol 1e-6)
          (system-nth-root (expt xx (/ 1.0 nth))))
      (let ((nn-fold (nth-root xx nth mm tol)))
        (begin
          (display
           (ice-9-format:format
            #f "(nth-root ~a) = ~12,10f (nth-root=~a, "
            xx nn-fold nth))
          (display
           (ice-9-format:format
            #f "n-fold=~a, tol=~a), diff = ~6,4e~%"
            mm tol (abs (- nn-fold system-nth-root))))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "We saw in section 1.3.3 that attempting "))
    (display
     (format #f "to compute square~%"))
    (display
     (format #f "roots by naively finding a fixed point "))
    (display
     (format #f "of y -> x/y does~%"))
    (display
     (format #f "not converge, and that this can be "))
    (display
     (format #f "fixed by average~%"))
    (display
     (format #f "damping. The same method works for "))
    (display
     (format #f "for finding cube~%"))
    (display
     (format #f "roots as fixed points of the "))
    (display
     (format #f "average-damped~%"))
    (display
     (format #f "y -> x/y^2. Unfortunately, the process "))
    (display
     (format #f "does not work for~%"))
    (display
     (format #f "fourth roots -- a single average damp "))
    (display
     (format #f "is not enough to~%"))
    (display
     (format #f "make a fixed point search for "))
    (display
     (format #f "y -> x/y^3 converge.~%"))
    (display
     (format #f "On the other hand, if we average damp "))
    (display
     (format #f "twice (i.e., use~%"))
    (display
     (format #f "the average damp of the average damp "))
    (display
     (format #f "of y -> x/y^3) the~%"))
    (display
     (format #f "fixed-point search does converge. Do "))
    (display
     (format #f "some experiments~%"))
    (display
     (format #f "to determine how many average damps "))
    (display
     (format #f "are required to~%"))
    (display
     (format #f "compute nth roots as a fixed-point "))
    (display
     (format #f "search based upon~%"))
    (display
     (format #f "repeated average damping of "))
    (display
     (format #f "y -> x/y^n-1. Use this~%"))
    (display
     (format #f "to implement a simple procedure for "))
    (display
     (format #f "computing nth roots~%"))
    (display
     (format #f "using fixed-point, average-damp, and "))
    (display
     (format #f "the repeated~%"))
    (display
     (format #f "procedure of exercise 1.43. Assume "))
    (display
     (format #f "that any arithmetic~%"))
    (display
     (format #f "operations you need are available as "))
    (display
     (format #f "primitives.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.45 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "square roots~%"))
             (check-nth-root-nfold 2.0 2 1)
             (check-nth-root-nfold 3.0 2 1)
             (check-nth-root-nfold 4.0 2 1)
             (check-nth-root-nfold 9.0 2 1)
             (newline)

             (display (format #f "cube roots~%"))
             (check-nth-root-nfold 2.0 3 1)
             (check-nth-root-nfold 3.0 3 1)
             (check-nth-root-nfold 8.0 3 1)
             (check-nth-root-nfold 27.0 3 1)
             (newline)

             (display (format #f "quartic roots~%"))
             (check-nth-root-nfold 2.0 4 40)
             (check-nth-root-nfold 3.0 4 60)
             (check-nth-root-nfold 8.0 4 100)
             (check-nth-root-nfold 16.0 4 150)
             (check-nth-root-nfold 27.0 4 200)
             (newline)
             (force-output)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
