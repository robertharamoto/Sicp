#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.11                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (func-rec n)
  (begin
    (cond
     ((< n 3)
      (begin
        n
        ))
     (else
      (begin
        (+ (func-rec (- n 1))
           (* 2 (func-rec (- n 2)))
           (* 3 (func-rec (- n 3))))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-func-rec-1 result-hash-table)
 (begin
   (let ((sub-name "test-func-rec-1-1")
         (test-list
          (list
           (list 0 0)
           (list 1 1)
           (list 2 2)
           (list 3 4)
           (list 4 11)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (func-rec nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : n=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; f(n) = n if n<3 and f(n) = f(n - 1) + 2f(n - 2) + 3f(n - 3) if n>=3
;;; a=f(n-1), b=f(n-2), c=f(n-3), f(n)=a+2b+3c
;;; f(n)->a, old a->b, old b->c
(define (func-iter n)
  (define (local-func a b c count)
    (begin
      (if (= count 0)
          (begin
            a)
          (begin
            (local-func
             (+ a (* 2 b) (* 3 c))
             a b (- count 1))
            ))
      ))
  (begin
    (cond
     ((< n 3)
      (begin
        n
        ))
     (else
      (begin
        (local-func 2 1 0 (- n 2))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-func-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-func-iter-1-1")
         (test-list
          (list
           (list 0 0)
           (list 1 1)
           (list 2 2)
           (list 3 4)
           (list 4 11)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (func-iter nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : n=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((sub-name "main-loop"))
      (begin
        (do ((nn 0 (1+ nn)))
            ((> nn 10))
          (begin
            (let ((result-rec (func-rec nn))
                  (result-iter (func-iter nn)))
              (begin
                (if (not (= result-rec result-iter))
                    (begin
                      (display
                       (format
                        #f "~a : error (~a) : n=~a, "
                        sub-name nn nn))
                      (display
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        result-rec result-iter))
                      (force-output))
                    (begin
                      (display
                       (ice-9-format:format
                        #f "func-rec(~:d) = ~:d : "
                        nn result-rec))
                      (display
                       (ice-9-format:format
                        #f "func-iter(~:d) = ~:d~%"
                        nn result-iter))
                      (force-output)
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format
      #f "A function f is defined by the rule that~%"))
    (display
     (format
      #f "f(n) = n if n<3~%"))
    (display
     (format #f "and f(n) = f(n - 1) + 2f(n - 2) "))
    (display
     (format #f "+ 3f(n - 3) if n> 3.~%"))
    (display
     (format #f "Write a procedure that computes f by "))
    (display
     (format #f "means of a~%"))
    (display
     (format #f "recursive process. Write a procedure "))
    (display
     (format #f "that computes~%"))
    (display
     (format #f "f by means of an iterative process.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.11 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
