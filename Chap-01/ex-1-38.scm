#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.38                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 5, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (continued-fraction num-func den-func nn)
  (define (local-iter curr-iter acc)
    (begin
      (if (< curr-iter 1)
          (begin
            acc)
          (begin
            (let ((this-numerator (num-func curr-iter))
                  (this-denominator (den-func curr-iter)))
              (let ((next-acc
                     (/ this-numerator
                        (+ this-denominator acc))))
                (begin
                  (local-iter (1- curr-iter) next-acc)
                  )))
            ))
      ))
  (begin
    (local-iter nn 0)
    ))

;;;#############################################################
;;;#############################################################
(define (denom-function ii)
  (begin
    (let ((jj (modulo ii 3)))
      (begin
        (if (= jj 2)
            (begin
              (let ((kk (quotient ii 3)))
                (let ((denom (* 2 (1+ kk))))
                  (begin
                    denom
                    ))
                ))
            (begin
              1
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-denom-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-denom-function-1")
         (test-list
          (list
           (list 1 1) (list 2 2) (list 3 1)
           (list 4 1) (list 5 4) (list 6 1)
           (list 7 1) (list 8 6) (list 9 1)
           (list 10 1) (list 11 8) (list 12 1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ii (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (denom-function ii)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : ii=~a, "
                        sub-name test-label-index ii))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (e-iter kk)
  (begin
    (+ 2.0
       (continued-fraction
        (lambda (ii) 1.0)
        denom-function
        kk))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-e-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-e-iter-1")
         (test-list
          (list
           (list 10 1e-4 2.7182) (list 100 1e-6 2.7182818)
           ))
         (ee 2.71828182845904523536)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (tol (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (e-iter nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, tol=~a, "
                        sub-name test-label-index nn tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (continued-fraction-rec num-func den-func nn)
  (define (local-rec curr-iter max-iter)
    (begin
      (if (> curr-iter max-iter)
          (begin
            0.0)
          (begin
            (let ((this-numerator (num-func curr-iter))
                  (this-denominator (den-func curr-iter)))
              (let ((second-denom
                     (local-rec (1+ curr-iter) max-iter)))
                (let ((result-acc
                       (/ this-numerator
                          (+ this-denominator
                             second-denom))))
                  (begin
                    result-acc
                    ))
                ))
            ))
      ))
  (begin
    (local-rec 1 nn)
    ))

;;;#############################################################
;;;#############################################################
(define (e-rec nn)
  (begin
    (+ 2.0
       (continued-fraction-rec
        (lambda (ii) 1.0)
        denom-function
        nn))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-e-rec-1 result-hash-table)
 (begin
   (let ((sub-name "test-e-rec-1")
         (test-list
          (list
           (list 10 1e-4 2.7182) (list 100 1e-6 2.7182818)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (tol (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (e-rec nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, tol=~a, "
                        sub-name test-label-index nn tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-e-tol-kk e-func ee max-nn tolerance)
  (begin
    (let ((end-loop-flag #f)
          (result-nn 0))
      (begin
        (do ((nn 1 (1+ nn)))
            ((or (> nn max-nn)
                 (equal? end-loop-flag #t)))
          (begin
            (let ((ll-ee (e-func nn)))
              (let ((abs-diff (abs (- ll-ee ee))))
                (begin
                  (if (< abs-diff tolerance)
                      (begin
                        (set! end-loop-flag #t)
                        (set! result-nn nn)
                        ))
                  )))
            ))
        result-nn
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "e continued-fraction calculation~%"))
    (display
     (format #f "e = 2.71828182845904523536...~%"))
    (force-output)
    (let ((tolerance 1e-5)
          (ee 2.71828182845904523536)
          (max-nn 10000))
      (begin
        (let ((e-iter-kk
               (find-e-tol-kk
                e-iter ee max-nn tolerance)))
          (let ((e-iter-num (e-iter e-iter-kk)))
            (begin
              (display
               (ice-9-format:format
                #f "iteration: e(~:d) = ~12,10f  (tol=~a)~%"
                e-iter-kk e-iter-num tolerance))
              (force-output)
              )))

        (let ((e-rec-kk
               (find-e-tol-kk
                e-rec ee max-nn tolerance)))
          (let ((e-rec-num (e-rec e-rec-kk)))
            (begin
              (display
               (ice-9-format:format
                #f "recursive: e(~:d) = ~12,10f  (tol=~a)~%"
                e-rec-kk e-rec-num tolerance))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In 1737, the Swiss mathematician Leonhard "))
    (display
     (format #f "Euler published a~%"))
    (display
     (format #f "memoir De Fractionibus Continuis, which "))
    (display
     (format #f "included a continued~%"))
    (display
     (format #f "fraction expansion for e - 2, where e "))
    (display
     (format #f "is the base of the~%"))
    (display
     (format #f "natural logarithms. In this fraction, "))
    (display
     (format #f "the N_i are all 1,~%"))
    (display
     (format #f "and the D_i are successively~%"))
    (display
     (format #f "1, 2, 1, 1, 4, 1, 1, 6, 1, 1, 8, ....~%"))
    (display
     (format #f "Write a program that uses your cont-frac "))
    (display
     (format #f "procedure from~%"))
    (display
     (format #f "exercise 1.37 to approximate e, based "))
    (display
     (format #f "on Euler's expansion.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.38 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (force-output)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
