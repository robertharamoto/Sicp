#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.40                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 6, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (fixed-point func first-guess tolerance)
  (define (local-try guess max-iter nstep tol)
    (begin
      (let ((next (func guess)))
        (let ((abs-diff (abs (- next guess))))
          (begin
            (if (> nstep max-iter)
                (begin
                  (display
                   (ice-9-format:format
                    #f "max iterations ~:d exceeded, "
                    max-iter))
                  (display
                   (ice-9-format:format
                    #f "stopping program...~%"))
                  (force-output)
                  (quit))
                (begin
                  (if (< abs-diff tol)
                      (begin
                        next)
                      (begin
                        (local-try
                         next max-iter (1+ nstep) tol)
                        ))
                  ))
            )))
      ))
  (begin
    (let ((max-iterations 1000))
      (begin
        (local-try
         first-guess max-iterations 0 tolerance)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (fixed-point-of-transform
         transform-function g-func guess tolerance)
  (begin
    (fixed-point
     (transform-function g-func) guess tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (deriv g-func)
  (begin
    (let ((dx 1.0e-6))
      (begin
        (lambda (xx)
          (begin
            (/ (- (g-func (+ xx dx)) (g-func xx))
               dx)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (average-damp func)
  (define (average a b)
    (begin
      (/ (+ a b) 2)
      ))
  (begin
    (lambda (xx)
      (begin
        (average xx (func xx))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (newtons-transform g-func)
  (begin
    (lambda (xx)
      (begin
        (- xx (/ (g-func xx) ((deriv g-func) xx)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (newtons-method g-func guess tolerance)
  (begin
    (fixed-point
     (newtons-transform g-func)
     guess tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (square-root xx tolerance)
  (begin
    (fixed-point
     (average-damp
      (lambda (y)
        (begin
          (/ xx y)
          )))
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (cube-root xx tolerance)
  (begin
    (fixed-point
     (average-damp
      (lambda (y)
        (begin
          (/ xx (* y y))
          )))
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-avg-damp-1 result-hash-table)
 (begin
   (let ((sub-name "test-avg-damp-1")
         (test-list
          (list
           (list square-root 1.0 1e-6 1.0)
           (list square-root 2.0 1e-6 1.414213562)
           (list square-root 3.0 1e-6 1.732050808)
           (list cube-root 1.0 1e-6 1.0)
           (list cube-root 2.0 1e-6 1.25992105)
           (list cube-root 3.0 1e-6 1.44224957)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((func (list-ref this-list 0))
                  (xx (list-ref this-list 1))
                  (tol (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (func xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, tol=~a, "
                        sub-name test-label-index xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (square-root-nm xx tolerance)
  (begin
    (newtons-method
     (lambda (y)
       (begin
         (- (* y y) xx)
         ))
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (cube-root-nm xx tolerance)
  (begin
    (newtons-method
     (lambda (y)
       (begin
         (- (* y y y) xx)
         ))
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-newtons-method-1 result-hash-table)
 (begin
   (let ((sub-name "test-newtons-method-1")
         (test-list
          (list
           (list square-root-nm 1.0 1e-6 1.0)
           (list square-root-nm 2.0 1e-6 1.414213562)
           (list square-root-nm 3.0 1e-6 1.732050808)
           (list cube-root-nm 1.0 1e-6 1.0)
           (list cube-root-nm 2.0 1e-6 1.25992105)
           (list cube-root-nm 3.0 1e-6 1.44224957)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((func (list-ref this-list 0))
                  (xx (list-ref this-list 1))
                  (tol (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (func xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, tol=~a, "
                        sub-name test-label-index xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (cubic a b c)
  (begin
    (lambda (xx)
      (begin
        (+ (* xx (+ (* xx (+ xx a)) b)) c)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (cubic-zeros a b c tolerance)
  (begin
    (newtons-method
     (cubic a b c)
     1.0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-cubic-zeros-1 result-hash-table)
 (begin
   (let ((sub-name "test-cubic-zeros-1")
         (test-list
          (list
           (list 0.0 0.0 -1.0 1e-6 +1.0)
           (list 0.0 0.0 -2.0 1e-6 +1.25992105)
           (list 0.0 0.0 -3.0 1e-6 +1.44224957)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa (list-ref this-list 0))
                  (bb (list-ref this-list 1))
                  (cc (list-ref this-list 2))
                  (tol (list-ref this-list 3))
                  (shouldbe (list-ref this-list 4)))
              (let ((result (cubic-zeros aa bb cc tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : aa=~a, bb=~a, "
                        sub-name test-label-index aa bb))
                      (err-msg-2
                       (format
                        #f "cc=~a, tol=~a, shouldbe=~a, result=~a"
                        cc tol shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a procedure cubic that can be "))
    (display
     (format #f "used together with the~%"))
    (display
     (format #f "newtons-method procedure in "))
    (display
     (format #f "expressions of the form~%"))
    (display
     (format #f "(newtons-method (cubic a b c) 1)~%"))
    (display
     (format #f "to approximate zeros of the cubic~%"))
    (display
     (format #f "  x^3 + ax^2 + bx + c.~%"))
    (newline)
    (display
     (format #f "(define (cubic a b c)~%"))
    (display
     (format #f " (lambda (xx)~%"))
    (display
     (format #f "   (begin~%"))
    (display
     (format #f "     (+ (* xx (+ (* xx (+ xx a)) b)) c)~%"))
    (display
     (format #f "    )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.40 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
