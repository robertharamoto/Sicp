#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp counting change example                         ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append
       %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module - for date/time, time-code-macro
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-change-recursive amount)
  (define (local-cc amount kinds-of-coins)
    (begin
      (cond
       ((= amount 0)
        (begin
          1
          ))
       ((or (< amount 0) (= kinds-of-coins 0))
        (begin
          0
          ))
       (else
        (begin
          (+ (local-cc amount
                       (- kinds-of-coins 1))
             (local-cc
              (- amount
                 (local-first-denomination kinds-of-coins))
              kinds-of-coins))
          )))
      ))
  (define (local-first-denomination kinds-of-coins)
    (begin
      (cond
       ((= kinds-of-coins 1)
        (begin
          1
          ))
       ((= kinds-of-coins 2)
        (begin
          5
          ))
       ((= kinds-of-coins 3)
        (begin
          10
          ))
       ((= kinds-of-coins 4)
        (begin
          25
          ))
       ((= kinds-of-coins 5)
        (begin
          50
          ))
       )))
  (begin
    (local-cc amount 5)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-change-recursive-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-change-recursive-1")
         (test-list
          (list
           (list 1 1) (list 2 1) (list 3 1)
           (list 4 1) (list 5 2) (list 6 2)
           (list 7 2) (list 8 2) (list 9 2)
           (list 10 4) (list 11 4)
           (list 12 4) (list 13 4)
           (list 14 4) (list 15 6)
           (list 100 292)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((amount (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (count-change-recursive amount)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : amount=~a, "
                        sub-name test-label-index amount))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; use array to find the number of ways to make change
(define (count-change-dynamic amount)
  (begin
    (let ((d-array (make-array 0 (1+ amount)))
          (coin-list (list 1 5 10 25 50)))
      (let ((coin-array (list->array 1 coin-list))
            (kind-of-coins (length coin-list)))
        (begin
          (array-set! d-array 1 0)
          (do ((jj 0 (1+ jj)))
              ((>= jj kind-of-coins))
            (begin
              (let ((this-coin
                     (array-ref coin-array jj)))
                (begin
                  (do ((ii this-coin (1+ ii)))
                      ((> ii amount))
                    (begin
                      (let ((cdiff (- ii this-coin)))
                        (begin
                          (let ((cdiff-count
                                 (array-ref d-array cdiff))
                                (curr-count
                                 (array-ref d-array ii)))
                            (begin
                              (array-set!
                               d-array (+ curr-count cdiff-count) ii)
                              ))
                          ))
                      ))
                  ))
              ))

          (array-ref d-array amount)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-change-dynamic-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-change-dynamic-1")
         (test-list
          (list
           (list 1 1) (list 2 1) (list 3 1)
           (list 4 1) (list 5 2) (list 6 2)
           (list 7 2) (list 8 2) (list 9 2)
           (list 10 4) (list 11 4)
           (list 12 4) (list 13 4)
           (list 14 4) (list 15 6)
           (list 16 6) (list 17 6)
           (list 18 6) (list 19 6)
           (list 20 9) (list 21 9)
;;;          (list 100 292)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((amount (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (count-change-dynamic amount)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : amount=~a, "
                        sub-name test-label-index amount))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop-recursive)
  (begin
    (let ((amount 100))
      (begin
        (let ((num (count-change-recursive amount)))
          (begin
            (display
             (ice-9-format:format
              #f "number of ways to give change for ~:d "
              amount))
            (display
             (ice-9-format:format
              #f "is ~:d (recursive method)~%"
              num))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-dynamic)
  (begin
    (let ((amount 100))
      (let ((num (count-change-dynamic amount)))
        (begin
          (display
           (ice-9-format:format
            #f "number of ways to give change for ~a "
            amount))
          (display
           (ice-9-format:format
            #f "is ~a (dynamic method)~%"
            num))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format
              #f "Sicp counting change example (version ~a)"
              version-string)))
        (begin
          ;;; run tests
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (display
           (format
            #f "dynamic method described at:~%"))
          (display
           (format
            #f "https://www.geeksforgeeks.org/understanding-the-coin-change-problem-with-dynamic-programming/~%"))
          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop-recursive)
             (newline)
             ))

          (timer-module:time-code-macro
           (begin
             (main-loop-dynamic)
             (newline)
             ))

          (newline)
          (display
           (format
            #f "~a~%"
            (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
