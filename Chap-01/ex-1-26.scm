#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.26                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 4, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner is having great "))
    (display
     (format #f "difficulty doing exercise~%"))
    (display
     (format #f "1.24. His fast-prime? test seems to "))
    (display
     (format #f "run more slowly than~%"))
    (display
     (format #f "his prime? test. Louis calls his "))
    (display
     (format #f "friend Eva Lu Ator~%"))
    (display
     (format #f "over to help. When they examine Louis's "))
    (display
     (format #f "code, they find that~%"))
    (display
     (format #f "he has rewritten the expmod procedure "))
    (display
     (format #f "to use an explicit~%"))
    (display
     (format #f "multiplication, rather than calling "))
    (display
     (format #f "square:~%"))
    (newline)
    (display
     (format #f "(define (expmod base exp m)~%"))
    (display
     (format #f "  (cond ((= exp 0) 1)~%"))
    (display
     (format #f "        ((even? exp)~%"))
    (display
     (format #f "          (remainder (* (expmod base (/ exp 2) m)~%"))
    (display
     (format #f "                        (expmod base (/ exp 2) m))~%"))
    (display
     (format #f "                      m))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         (remainder (* base (expmod base (- exp 1) m))~%"))
    (display
     (format #f "                     m))))~%"))
    (newline)

    (display
     (format #f "By calling the expmod function twice, "))
    (display
     (format #f "Louis Reasoner has~%"))
    (display
     (format #f "erased the benefits of calling expmod "))
    (display
     (format #f "fewer times. For an~%"))
    (display
     (format #f "exponent of size 8, the calls to "))
    (display
     (format #f "expmod should look like~%"))
    (display
     (format #f "8->4->2->1->0 (5 calls), whereas "))
    (display
     (format #f "his version is~%"))
    (display
     (format #f "8->4+4->2+2->1+1->0+0 "))
    (display
     (format #f "(9 recursive calls).~%"))
    (display
     (format #f "So that's why the code goes from "))
    (display
     (format #f "theta(log(n)) to theta(n).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.26 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
