#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.1                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module - for date/time, time-code
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Below is a sequence of expressions. "))
    (display
     (format #f "What is the result~%"))
    (display
     (format #f "printed by the interpreter in response "))
    (display
     (format #f "to each expression?~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((a 3))
      (let ((b (+ a 1)))
        (begin
          (display
           (format #f "10 -> ~a~%" 10))
          (display
           (format #f "(+ 5 3 4) -> ~a~%" (+ 5 3 4)))
          (display
           (format #f "(- 9 1) -> ~a~%" (- 9 1)))
          (display
           (format #f "(/ 6 2) -> ~a~%" (/ 6 2)))
          (display
           (format
            #f "(+ (* 2 4) (- 4 6)) -> ~a~%"
            (+ (* 2 4) (- 4 6))))
          (display
           (format #f "(define a 3) -> ~a~%" a))
          (display
           (format #f "(define b (+ a 1)) -> ~a~%" b))
          (display
           (format
            #f "(+ a b (* a b)) -> ~a~%"
            (+ a b (* a b))))
          (display
           (format #f "(= a b) -> ~a~%"
                   (= a b)))
          (display
           (format
            #f "(if (and (> b a) (< b (* a b)))~%"))
          (display
           (format
            #f "    a  b) -> ~a~%"
            (if (and (> b a) (< b (* a b))) a b)))
          (display
           (format #f "(cond ((= a 4) 6)~%"))
          (display
           (format #f "      ((= b 4) (+ 6 7 a))~%"))
          (display
           (format #f "      (else 25)) -> ~a~%"
                   (cond ((= a 4) 6)
                         ((= b 4) (+ 6 7 a))
                         (else 25))
                   ))

          (display
           (format
            #f "(+ 2 (if (> b a) b a)) -> ~a~%"
            (+ 2 (if (> b a) b a))))
          (display
           (format #f "(* (cond ((> a b) a)~%"))
          (display
           (format #f "         ((< a b) b)~%"))
          (display
           (format #f "         (else -1))~%"))
          (display
           (format #f "   (+ a 1)) -> ~a~%"
                   (* (cond ((> a b) a)
                            ((< a b) b)
                            (else -1))
                      (+ a 1))))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.1 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
