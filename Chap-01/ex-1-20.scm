#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.20                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 4, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Using the substitution method (for "))
    (display
     (format #f "normal order), illustrate~%"))
    (display
     (format #f "the process generated in evaluating "))
    (display
     (format #f "(gcd 206 40) and indicate~%"))
    (display
     (format #f "the remainder operations that are "))
    (display
     (format #f "actually performed.~%"))
    (display
     (format #f "How many remainder operations are "))
    (display
     (format #f "actually performed in~%"))
    (display
     (format #f "the normal-order evaluation of "))
    (display
     (format #f "(gcd 206 40)?~%"))
    (display
     (format #f "In the applicative-order evaluation?~%"))
    (newline)
    (display
     (format #f "(define (gcd a b)~%"))
    (display
     (format #f "  (if (= b 0)~%"))
    (display
     (format #f "      a~%"))
    (display
     (format #f "      (gcd b (remainder a b))))~%"))
    (newline)
    (display
     (format #f "Substitution, normal order~%"))
    (display
     (format #f "(gcd 206 40)~%"))
    (display
     (format #f "(if (= 40 0) 206 (gcd 40 "))
    (display
     (format #f "(remainder 206 40)))~%"))
    (display
     (format #f "(if (= (remainder 206 40) 0)~%"))
    (display
     (format #f "  40~%"))
    (display
     (format #f "  (gcd (remainder 206 40)~%"))
    (display
     (format #f "       (remainder 40 (remainder 206 40))))~%"))
    (display
     (format #f "+1 remainder evaluation, predicate "))
    (display
     (format #f "remainders evaluate to (= 6 0)~%"))
    (display
     (format #f "(gcd (remainder 206 40) "))
    (display
     (format #f "(remainder 40 (remainder 206 40)))~%"))
    (newline)
    (display
     (format #f "(if (= (remainder 40 "))
    (display
     (format #f "(remainder 206 40)) 0)~%"))
    (display
     (format #f "    (remainder 206 40)~%"))
    (display
     (format #f "    (gcd (remainder 40 (remainder 206 40))~%"))
    (display
     (format #f "         (remainder (remainder 206 40)~%"))
    (display
     (format #f "         (remainder 40 (remainder 206 40)))))~%"))
    (display
     (format #f "+2 remainder evaluations, predicate "))
    (display
     (format #f "remainders evaluate to (= 4 0)~%"))
    (newline)
    (display
     (format #f "(gcd (remainder 40 (remainder 206 40))~%"))
    (display
     (format #f "     (remainder (remainder 206 40)~%"))
    (display
     (format #f "                (remainder 40 "))
    (display
     (format #f "(remainder 206 40))))~%"))
    (newline)
    (display
     (format #f "(if (= (remainder (remainder 206 40)~%"))
    (display
     (format #f "                  (remainder 40 "))
    (display
     (format #f "(remainder 206 40))) 0)~%"))
    (display
     (format #f "    (remainder 40 (remainder 206 40))~%"))
    (display
     (format #f "    (gcd (remainder (remainder 206 40)~%"))
    (display
     (format #f "                    (remainder 40 "))
    (display
     (format #f "(remainder 206 40)))~%"))
    (display
     (format #f "         (remainder (remainder 40 "))
    (display
     (format #f "(remainder 206 40))~%"))
    (display
     (format #f "                    (remainder "))
    (display
     (format #f "(remainder 206 40)~%"))
    (display
     (format #f "                      (remainder 40 "))
    (display
     (format #f "(remainder 206 40))))))~%"))
    (display
     (format #f "+4 remainder evaluations, "))
    (display
     (format #f "predicate evaluates to (= 2 0)~%"))
    (newline)
    (display
     (format #f "(gcd (remainder (remainder 206 40)~%"))
    (display
     (format #f "                (remainder 40 "))
    (display
     (format #f "(remainder 206 40)))~%"))
    (display
     (format #f "     (remainder (remainder 40 "))
    (display
     (format #f "(remainder 206 40))~%"))
    (display
     (format #f "                 (remainder "))
    (display
     (format #f "(remainder 206 40)~%"))
    (display
     (format #f "                (remainder 40~%"))
    (display
     (format #f "                           (remainder "))
    (display
     (format #f "206 40)))))~%"))
    (newline)
    (display
     (format #f "(if (= (remainder (remainder 40 "))
    (display
     (format #f "(remainder 206 40))~%"))
    (display
     (format #f "                  (remainder "))
    (display
     (format #f "(remainder 206 40)~%"))
    (display
     (format #f "                     (remainder 40 "))
    (display
     (format #f "(remainder 206 40)))) 0)~%"))
    (display
     (format #f "    (remainder (remainder 206 40)~%"))
    (display
     (format #f "               (remainder 40 "))
    (display
     (format #f "(remainder 206 40)))~%"))
    (display
     (format #f "    (gcd (remainder (remainder 40 "))
    (display
     (format #f "(remainder 206 40))~%"))
    (display
     (format #f "                    (remainder (remainder "))
    (display
     (format #f "206 40)~%"))
    (display
     (format #f "                        (remainder 40 "))
    (display
     (format #f "(remainder 206 40))))~%"))
    (display
     (format #f "         (remainder (remainder "))
    (display
     (format #f "(remainder 206 40)~%"))
    (display
     (format #f "                        (remainder 40 "))
    (display
     (format #f "(remainder 206 40)))~%"))
    (display
     (format #f "                    (remainder "))
    (display
     (format #f "(remainder 40 (remainder 206 40))~%"))
    (display
     (format #f "                                (remainder "))
    (display
     (format #f "(remainder 206 40)~%"))
    (display
     (format #f "                                    (remainder "))
    (display
     (format #f "40 (remainder 206 40)))))~%"))
    (display
     (format #f "+7 remainder evaluations, predicate is (= 0 0)~%"))
    (newline)
    (display
     (format #f "(remainder (remainder 206 40) "))
    (display
     (format #f "(remainder 40 (remainder 206 40)))~%"))
    (display
     (format #f "+4 remainder evaluations~%"))
    (display
     (format #f "= 2~%"))
    (newline)
    (display
     (format #f "There are 18 remainder evaluations in "))
    (display
     (format #f "the normal order method.~%"))
    (newline)
    (display
     (format #f "Substitution, normal order and "))
    (display
     (format #f "applicative order~%"))
    (display
     (format #f "(gcd 206 40)~%"))
    (display
     (format #f "(if (= 40 0)~%"))
    (display
     (format #f "    206~%"))
    (display
     (format #f "    (gcd 40 (remainder 206 40)))~%"))
    (display
     (format #f "(gcd 40 (remainder 206 40))~%"))
    (display
     (format #f "  -> (gcd 40 6) : +1 "))
    (display
     (format #f "remainder valuation~%"))
    (newline)
    (display
     (format #f "(if (= 6 0)~%"))
    (display
     (format #f "    40~%"))
    (display
     (format #f "    (gcd 6 (remainder 40 6)))~%"))
    (display
     (format #f "(gcd 6 (remainder 40 6))~%"))
    (display
     (format #f "  -> (gcd 6 4) : +1 "))
    (display
     (format #f "remainder valuation~%"))
    (newline)
    (display
     (format #f "(if (= 4 0) 6 "))
    (display
     (format #f "(gcd 4 (remainder 6 4)))~%"))
    (display
     (format #f "(gcd 4 (remainder 6 4))~%"))
    (display
     (format #f "  -> (gcd 4 2) : +1 "))
    (display
     (format #f "remainder valuation~%"))
    (display
     (format #f "(if (= 2 0) 4 "))
    (display
     (format #f "(gcd 2 (remainder 4 2)))~%"))
    (display
     (format #f "(gcd 2 (remainder 4 2))~%"))
    (display
     (format #f "  -> (gcd 2 0) : +1 "))
    (display
     (format #f "remainder valuation~%"))
    (display
     (format #f "(gcd 2 (remainder 4 2))~%"))
    (display
     (format #f "  -> (gcd 2 0) : +1 "))
    (display
     (format #f "remainder valuation~%"))
    (display
     (format #f "= 2~%"))
    (newline)
    (display
     (format #f "There are 4 remainder operations "))
    (display
     (format #f "performed in the~%"))
    (display
     (format #f "appplicative order evaluation.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.20 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
