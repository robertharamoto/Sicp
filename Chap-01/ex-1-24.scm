#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.24                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 4, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (smallest-divisor nn)
  (define (local-find-divisor nn test-divisor max-divisor)
    (begin
      (cond
       ((> test-divisor max-divisor)
        (begin
          nn
          ))
       ((zero? (remainder nn test-divisor))
        (begin
          test-divisor
          ))
       (else
        (begin
          (local-find-divisor
           nn (+ test-divisor 2) max-divisor)
          )))
      ))
  (begin
    (cond
     ((<= nn 1)
      (begin
        -1
        ))
     ((zero? (remainder nn 2))
      (begin
        2
        ))
     ((zero? (remainder nn 3))
      (begin
        3
        ))
     (else
      (begin
        (let ((max-divisor (1+ (sqrt nn))))
          (begin
            (local-find-divisor nn 5 max-divisor)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-smallest-divisor-1 result-hash-table)
 (begin
   (let ((sub-name "test-smallest-divisor-1")
         (test-list
          (list
           (list 2 2) (list 3 3) (list 4 2) (list 5 5)
           (list 6 2) (list 7 7) (list 8 2) (list 9 3)
           (list 10 2) (list 11 11) (list 12 2) (list 13 13)
           (list 14 2) (list 15 3) (list 16 2) (list 17 17)
           (list 18 2) (list 19 19) (list 20 2) (list 21 3)
           (list 22 2) (list 23 23) (list 24 2) (list 25 5)
           (list 26 2) (list 27 3) (list 28 2) (list 29 29)
           (list 30 2) (list 31 31) (list 32 2) (list 33 3)
           (list 34 2) (list 35 5) (list 36 2) (list 37 37)
           (list 38 2) (list 39 3) (list 40 2) (list 41 41)
           (list 42 2) (list 43 43) (list 44 2) (list 45 3)
           (list 46 2) (list 47 47) (list 48 2) (list 49 7)
           (list 50 2) (list 51 3) (list 52 2) (list 53 53)
           (list 54 2) (list 55 5) (list 56 2) (list 57 3)
           (list 58 2) (list 59 59) (list 60 2) (list 61 61)
           (list 62 2) (list 63 3) (list 64 2) (list 65 5)
           (list 66 2) (list 67 67) (list 68 2) (list 69 3)
           (list 70 2) (list 71 71) (list 72 2) (list 73 73)
           (list 74 2) (list 75 3) (list 76 2) (list 77 7)
           (list 78 2) (list 79 79) (list 80 2) (list 81 3)
           (list 82 2) (list 83 83) (list 84 2) (list 85 5)
           (list 86 2) (list 87 3) (list 88 2) (list 89 89)
           (list 90 2) (list 91 7) (list 92 2) (list 93 3)
           (list 94 2) (list 95 5) (list 96 2) (list 97 97)
           (list 98 2) (list 99 3) (list 100 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (smallest-divisor nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (smallest-divisor-prime? nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     (else
      (begin
        (= (smallest-divisor nn) nn)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-smallest-divisor-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-smallest-divisor-prime-1")
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 87 #f) (list 88 #f) (list 89 #t)
           (list 90 #f) (list 91 #f) (list 92 #f) (list 93 #f)
           (list 94 #f) (list 95 #f) (list 96 #f) (list 97 #t)
           (list 98 #f) (list 99 #f) (list 100 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (smallest-divisor-prime? nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f ", shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (exp-mod base exp mm)
  (begin
    (cond
     ((= exp 0)
      (begin
        1
        ))
     ((even? exp)
      (begin
        (let ((xx (exp-mod base (/ exp 2) mm)))
          (begin
            (remainder (* xx xx) mm)
            ))
        ))
     (else
      (begin
        (remainder
         (* base (exp-mod base (1- exp) mm))
         mm)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-exp-mod-1 result-hash-table)
 (begin
   (let ((sub-name "test-exp-mod-1")
         (test-list
          (list
           (list 2 0 2 1) (list 2 1 2 0)
           (list 3 0 11 1) (list 3 1 11 3)
           (list 3 2 11 9) (list 3 3 11 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((base (list-ref this-list 0))
                  (exp (list-ref this-list 1))
                  (mm (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (exp-mod base exp mm)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : base=~a, exp=~a, "
                        sub-name test-label-index base exp))
                      (err-msg-2
                       (format
                        #f "mm=~a, shouldbe=~a, result=~a"
                        mm shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (fast-prime? nn times)
  (define (local-fermat-test nn)
    (define (try-it aa)
      (begin
        (= (exp-mod aa nn nn) aa)
        ))
    (begin
      (try-it (1+ (random (1- nn))))
      ))
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((<= times 0)
      (begin
        #t
        ))
     ((local-fermat-test nn)
      (begin
        (fast-prime? nn (1- times))
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fast-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-fast-prime-1")
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 87 #f) (list 88 #f) (list 89 #t)
           (list 90 #f) (list 91 #f) (list 92 #f) (list 93 #f)
           (list 94 #f) (list 95 #f) (list 96 #f) (list 97 #t)
           (list 98 #f) (list 99 #f) (list 100 #f)
           ))
         (times 6)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fast-prime? nn times)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime? nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     (else
      (begin
        (fast-prime? nn 6)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-prime-1")
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 87 #f) (list 88 #f) (list 89 #t)
           (list 90 #f) (list 91 #f) (list 92 #f) (list 93 #f)
           (list 94 #f) (list 95 #f) (list 96 #f) (list 97 #t)
           (list 98 #f) (list 99 #f) (list 100 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (prime? nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-three-smallest-primes-greater-than-nn
         nn is-prime-function?)
  (begin
    (let ((primes-count 0)
          (pstart nn))
      (begin
        (if (even? nn)
            (begin
              (set! nn (1+ nn))
              ))
        (do ((ii nn (+ ii 2)))
            ((>= primes-count 3))
          (begin
            (if (is-prime-function? ii)
                (begin
                  (if (= primes-count 0)
                      (begin
                        (display (ice-9-format:format #f "~:d" ii)))
                      (begin
                        (display (ice-9-format:format #f ", ~:d" ii))
                        ))
                  (force-output)
                  (set! primes-count (1+ primes-count))
                  ))
            ))
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list 1000 10000 100000 1000000)))
      (begin
        (for-each
         (lambda (anum)
           (begin
             (display
              (ice-9-format:format
               #f "~a: the first three primes greater than ~:d~%"
               "smallest-divisor" anum))
             (find-three-smallest-primes-greater-than-nn
              anum smallest-divisor-prime?)
             (force-output)

             (display
              (ice-9-format:format
               #f "~a: the first three primes greater than ~:d~%"
               "exp-mod" anum))
             (find-three-smallest-primes-greater-than-nn
              anum prime?)
             (newline)
             (force-output)
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Use your procedure to find the three "))
    (display
     (format #f "smallest primes~%"))
    (display
     (format #f "larger than 1000; larger than 10,000; "))
    (display
     (format #f "larger than 100,000;~%"))
    (display
     (format #f "larger than 1,000,000;~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.24 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)

             (display (format #f "total "))
             ))

          (newline)
          (display
           (format #f "~a~%" (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
