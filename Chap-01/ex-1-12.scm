#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.12                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (calc-next-row previous-row)
  (begin
    (let ((next-list (list 1))
          (curr-nn (1+ (length previous-row))))
      (let ((iter-max (- curr-nn 2)))
        (begin
          (if (> iter-max 0)
              (begin
                (do ((ii 1 (1+ ii)))
                    ((> ii iter-max))
                  (begin
                    (let ((c0 (list-ref previous-row (1- ii)))
                          (c1 (list-ref previous-row ii)))
                      (let ((next-c1 (+ c0 c1)))
                        (begin
                          (set! next-list (cons next-c1 next-list))
                          )))
                    ))
                (set! next-list (cons 1 next-list))
                next-list)
              (begin
                (set! next-list (cons 1 next-list))
                next-list
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-next-row-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-next-row-1")
         (test-list
          (list
           (list (list 1) (list 1 1))
           (list (list 1 1) (list 1 2 1))
           (list (list 1 2 1) (list 1 3 3 1))
           (list (list 1 3 3 1) (list 1 4 6 4 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((prev-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (calc-next-row prev-list)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : prev-list=~a, "
                        sub-name test-label-index prev-list))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pascal-recursive n)
  (define (local-rec curr-nn max-nn prev-list acc-list)
    (begin
      (if (>= curr-nn max-nn)
          (begin
            acc-list)
          (begin
            (let ((next-list
                   (calc-next-row prev-list)))
              (let ((second-acc-list
                     (cons next-list acc-list)))
                (let ((next-acc-list
                       (local-rec
                        (1+ curr-nn) max-nn next-list
                        second-acc-list)))
                  (begin
                    next-acc-list
                    ))
                ))
            ))
      ))
  (begin
    (let ((zeroth-list (list (list 1))))
      (begin
        (cond
         ((<= n 0)
          (begin
            zeroth-list
            ))
         ((= n 1)
          (begin
            (reverse (cons (list 1 1) zeroth-list))
            ))
         (else
          (begin
            (let ((acc-list (list (list 1 1) (list 1))))
              (let ((result-list
                     (local-rec 1 n (list 1 1) acc-list)))
                (begin
                  (reverse result-list)
                  ))
              ))
          ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pascal-recursive-1 result-hash-table)
 (begin
   (let ((sub-name "test-pascal-recursive-1")
         (test-list
          (list
           (list 0 (list (list 1)))
           (list 1 (list (list 1) (list 1 1)))
           (list 2 (list (list 1) (list 1 1) (list 1 2 1)))
           (list 3 (list (list 1) (list 1 1) (list 1 2 1)
                         (list 1 3 3 1)))
           (list 4 (list (list 1) (list 1 1) (list 1 2 1)
                         (list 1 3 3 1) (list 1 4 6 4 1)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (pascal-recursive nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : n=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-pascals-triangle p-list-list)
  (begin
    (let ((current-row 0)
          (max-row (length p-list-list))
          (last-row-string
           (string-join
            (map
             (lambda (anum)
               (begin
                 (if (number? anum)
                     (begin
                       (ice-9-format:format #f "~2:d" anum))
                     (begin
                       (format #f "~a" anum)
                       ))
                 )) (last-pair p-list-list))
            " ")))
      (let ((last-half-width
             (1+ (euclidean/
                  (string-length last-row-string)
                  2))))
        (begin
          (display
           (format #f "Pascal's triangle for n = ~a~%"
                   (1- max-row)))
          (force-output)
          (for-each
           (lambda (row-list)
             (begin
               (let ((nlen (1+ (length row-list)))
                     (spacer-string
                      (make-string
                       (+ 3 (- last-half-width current-row))
                       #\space))
                     (data-string
                      (string-join
                       (map
                        (lambda (anum)
                          (ice-9-format:format
                           #f "~2,:d" anum))
                        row-list)
                       " ")))
                 (begin
                   (display
                    (format
                     #f "~a~a~%" spacer-string data-string))
                   (force-output)
                   ))
               (set! current-row (1+ current-row))
               )) p-list-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (do ((nn 4 (1+ nn)))
        ((> nn 5))
      (begin
        (let ((pas-list-list
               (pascal-recursive nn)))
          (begin
            (display-pascals-triangle pas-list-list)
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format
      #f "The following pattern of numbers is called "))
    (display
     (format
      #f "Pascals triangle.~%"))
    (display (format #f "      1~%"))
    (display (format #f "     1 1~%"))
    (display (format #f "    1 2 1~%"))
    (display (format #f "   1 3 3 1~%"))
    (display (format #f "  1 4 6 4 1~%"))
    (display (format #f "     ...~%"))
    (display
     (format #f "The numbers at the edge of the triangle "))
    (display
     (format #f "are all 1, and~%"))
    (display
     (format #f "and each number inside the triangle "))
    (display
     (format #f "is the sum of~%"))
    (display
     (format #f "the two numbers above it. Write a "))
    (display
     (format #f "procedure that computes~%"))
    (display
     (format #f "elements of Pascal's triangle by means "))
    (display
     (format #f "of a recursive process~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.12 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (force-output)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
