#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.10                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (ackermann-function x y)
  (begin
    (cond
     ((= y 0)
      (begin
        0
        ))
     ((= x 0)
      (begin
        (* 2 y)
        ))
     ((= y 1)
      (begin
        2
        ))
     (else
      (begin
        (ackermann-function
         (- x 1)
         (ackermann-function x (- y 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-ackermann-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-ackermann-function-1")
         (test-list
          (list
           (list 1 2 4)
           (list 1 3 8)
           (list 1 4 16)
           (list 1 5 32)
           (list 1 6 64)
           (list 2 4 65536)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (ackermann-function xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : x=~a, y=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (f n)
  (begin
    (ackermann-function 0 n)
    ))

;;;#############################################################
;;;#############################################################
(define (g n)
  (begin
    (ackermann-function 1 n)
    ))

;;;#############################################################
;;;#############################################################
(define (h n)
  (begin
    (ackermann-function 2 n)
    ))

;;;#############################################################
;;;#############################################################
(define (hstring n)
  (begin
    (let ((astring "")
          (bstring ""))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii n))
          (begin
            (if (string-ci=? astring "")
                (begin
                  (set! astring "2"))
                (begin
                  (set! astring (format #f "~a^(2" astring))
                  (set! bstring (format #f "~a)" bstring))
                  ))
            ))

        (if (not (string-ci=? bstring ""))
            (begin
              (string-append astring bstring))
            (begin
              astring
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-ackermann-function x y)
  (define (local-aa xx yy nparen acc-string)
    (begin
      (let ((pstring (make-string nparen #\) )))
        (begin
          (cond
           ((= yy 0)
            (begin
              (display
               (format
                #f "~a (A ~a 0~a~%" acc-string xx pstring))
              (force-output)
              0
              ))
           ((= xx 0)
            (begin
              (display
               (format
                #f "~a (A ~a ~a~a~%" acc-string xx yy pstring))
              (force-output)
              (* 2 yy)
              ))
           ((= yy 1)
            (begin
              (display
               (format
                #f "~a (A ~a ~a~a~%" acc-string xx yy pstring))
              (force-output)
              2
              ))
           (else
            (begin
              (display
               (format
                #f "~a (A ~a ~a~a~%"
                acc-string xx yy pstring))
              (force-output)
              (let ((next-nparen (1+ nparen))
                    (next-acc-string
                     (string-append
                      acc-string
                      (format #f " (A ~a" (- xx 1)))))
                (begin
                  (local-aa
                   (- xx 1)
                   (local-aa
                    xx (- yy 1)
                    next-nparen next-acc-string)
                   nparen
                   acc-string)
                  ))
              )))
          ))
      ))
  (begin
    (if (or (< x 0) (< y 0))
        (begin
          (display
           (format
            #f "ackermann-function error : x and y "))
          (display
           (format
            #f "must be greater than 0!~%"))
          (display
           (format
            #f "received x=~a, y=~a~%" x y))
          (display
           (format
            #f "quitting...~%"))
          (force-output)
          (quit))
        (begin
          (let ((result
                 (local-aa x y 1 "  ")))
            (begin
              (display
               (format #f "~a~%" result))
              (force-output)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "ackermann's function~%"))
    (newline)

    (display
     (format #f "(define (A x y)~%"))
    (display
     (format #f "  (cond ((= y 0) 0)~%"))
    (display
     (format #f "        ((= x 0) (* 2 y))~%"))
    (display
     (format #f "        ((= y 1) 2)~%"))
    (display
     (format #f "        (else (A (- x 1)~%"))
    (display
     (format #f "                 (A x (- y 1))))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((separator-line (make-string 64 #\=)))
      (begin
        (display
         (format #f "~a~%" separator-line))
        (newline)

        (let ((test-list
               (list
                (list 1 10)
                (list 2 4)
                (list 3 3))))
          (begin
            (for-each
             (lambda (alist)
               (begin
                 (let ((x (list-ref alist 0))
                       (y (list-ref alist 1)))
                   (begin
                     (display-ackermann-function x y)
                     (display
                      (format
                       #f "ackermann-function = ~a~%"
                       (ackermann-function x y)))
                     (newline)
                     (force-output)
                     ))
                 )) test-list)
            ))

        (newline)

        (display
         (format #f "~a~%" separator-line))
        (newline)
        (display
         (format #f "f(n) = (A 0 n)~%"))
        (do ((ii 1 (1+ ii)))
            ((> ii 5))
          (begin
            (display
             (format #f "f(~a) = ~a~%" ii (f ii)))
            ))
        (display
         (format #f "f(n) = 2 * n~%"))
        (newline)

        (display
         (format #f "~a~%" separator-line))
        (newline)
        (display
         (format #f "g(n) = (A 1 n)~%"))
        (do ((ii 1 (1+ ii)))
            ((> ii 5))
          (begin
            (display
             (format #f "g(~a) = ~a~%" ii (g ii)))
            ))
        (display
         (format #f "g(n) = 2^n~%"))
        (newline)

        (display
         (format #f "~a~%" separator-line))
        (newline)
        (display
         (format #f "h(n) = (A 2 n)~%"))
        (do ((ii 1 (1+ ii)))
            ((> ii 4))
          (begin
            (display
             (format
              #f "h(~a) = ~a = ~a~%" ii (h ii) (hstring ii)))
            ))
        (display
         (format
          #f "h(n) = 2^(2^(2^...^2)), with n 2's "))
        (display
         (format
          #f "in the expression~%"))
        ))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.10 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)
          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
