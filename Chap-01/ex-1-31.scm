#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.31                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 5, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (prod-rec term aa next bb tolerance)
  (begin
    (if (and (> aa bb)
             (> (abs (- aa bb)) tolerance))
        (begin
          1)
        (begin
          (* (term aa)
             (prod-rec
              term (next aa) next bb tolerance))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (prod-iter term aa next bb acc tolerance)
  (begin
    (if (and (> aa bb)
             (> (abs (- aa bb)) tolerance))
        (begin
          acc)
        (begin
          (let ((next-acc
                 (* acc (term aa))))
            (begin
              (prod-iter
               term (next aa) next bb next-acc tolerance)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (fac-rec nn)
  (define (fac-term xx)
    (begin
      xx
      ))
  (define (fac-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((tolerance 1e-12))
      (begin
        (prod-rec fac-term 1 fac-next nn tolerance)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fac-rec-1 result-hash-table)
 (begin
   (let ((sub-name "test-fac-rec-1")
         (test-list
          (list
           (list 2 2) (list 3 6) (list 4 24)
           (list 5 120) (list 6 720)
           ))
         (tolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fac-rec nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : n=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (fac-iter nn)
  (define (fac-term xx)
    (begin
      xx
      ))
  (define (fac-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((tolerance 1e-12))
      (begin
        (prod-iter fac-term 1 fac-next nn 1 tolerance)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fac-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-fac-iter-1")
         (test-list
          (list
           (list 2 2) (list 3 6) (list 4 24)
           (list 5 120) (list 6 720)
           ))
         (tolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fac-iter nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pi-term kk)
  (begin
    (cond
     ((even? kk) (/ (+ kk 2) (+ kk 1)))
     (else
      (begin
        (/ (+ kk 1) (+ kk 2))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pi-term-1 result-hash-table)
 (begin
   (let ((sub-name "test-pi-term-1")
         (test-list
          (list
           (list 1 (/ 2 3)) (list 2 (/ 4 3)) (list 3 (/ 4 5))
           (list 4 (/ 6 5)) (list 5 (/ 6 7)) (list 6 (/ 8 7))
           ))
         (tolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((kk (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (pi-term kk)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : kk=~a, "
                        sub-name test-label-index kk))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pi-rec nn)
  (define (pi-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((tolerance 1e-12))
      (let ((result
             (* 4
                (prod-rec
                 pi-term 1 pi-next nn tolerance)
                )))
        (begin
          result
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (pi-iter nn)
  (define (pi-next xx)
    (begin
      (1+ xx)
      ))
  (begin
    (let ((tolerance 1e-12))
      (let ((result
             (* 4
                (prod-iter
                 pi-term 1 pi-next nn 1 tolerance)
                )))
        (begin
          result
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-fac-list
           (list
            (list 2) (list 3) (list 4) (list 5) (list 6)
            (list 7) (list 8) (list 9) (list 10)))
          (test-pi-list
           (list
            (list 100) (list 1000)))
          (tolerance 1e-12)
          (test-label-index 0))
      (begin
        ;;; first display factorial calculations
        (for-each
         (lambda (tlist)
           (begin
             (let ((nn (list-ref tlist 0)))
               (let ((frec (fac-rec nn))
                     (fiter (fac-iter nn)))
                 (let ((diff (abs (- frec fiter))))
                   (begin
                     (if (> diff tolerance)
                         (begin
                           (display
                            (ice-9-format:format
                             #f "factorial-recursive(~a) = ~:d~%"
                             nn frec))
                           (display
                            (ice-9-format:format
                             #f "factorial-iterative(~a) = ~:d~%"
                             nn fiter))
                           (force-output))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "factorial-recursive(~a) = ~:d = "
                             nn frec))
                           (display
                            (ice-9-format:format
                             #f "factorial_iterative(~a)~%" nn))
                           (force-output)
                           ))
                     ))
                 ))
             (set! test-label-index (1+ test-label-index))
             )) test-fac-list)

        ;;; next display pi calculations
        (set! test-label-index 0)
        (newline)
        (force-output)

        (for-each
         (lambda (tlist)
           (begin
             (let ((nn (list-ref tlist 0)))
               (let ((pi-rec (pi-rec nn))
                     (pi-iter (pi-iter nn)))
                 (let ((diff (abs (- pi-rec pi-iter))))
                   (begin
                     (if (> diff tolerance)
                         (begin
                           (display
                            (ice-9-format:format
                             #f "pi-recursive(~:d) = ~a~%"
                             nn (exact->inexact pi-rec)))
                           (display
                            (ice-9-format:format
                             #f "pi-iterative(~:d) = ~a~%"
                             nn (exact->inexact pi-iter)))
                           (force-output))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "pi-recursive(~:d) = ~a = "
                             nn (exact->inexact pi-rec)))
                           (display
                            (ice-9-format:format
                             #f "pi_iterative(~:d)~%" nn))
                           (force-output)
                           ))
                     ))
                 ))
             (set! test-label-index (1+ test-label-index))
             )) test-pi-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-pi-iter-loop)
  (begin
    (let ((n-list (list 10000 100000)))
      (begin
        (for-each
         (lambda (num)
           (begin
             (let ((pi-iter (pi-iter num)))
               (begin
                 (display
                  (ice-9-format:format
                   #f "pi-iterative(~:d) = ~a~%"
                   num (exact->inexact pi-iter)))
                 (newline)
                 (force-output)
                 ))
             )) n-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "a.  The sum procedure is only the "))
    (display
     (format #f "simplest of a vast~%"))
    (display
     (format #f "number of similar abstractions that "))
    (display
     (format #f "can be captured as~%"))
    (display
     (format #f "higher-order procedures. Write an "))
    (display
     (format #f "analogous procedure~%"))
    (display
     (format #f "called product that returns the product "))
    (display
     (format #f "of the values~%"))
    (display
     (format #f "of a function at points over a given "))
    (display
     (format #f "range. Show how to~%"))
    (display
     (format #f "define factorial in terms of a product. "))
    (display
     (format #f "Also use product~%"))
    (display
     (format #f "to compute approximations to pi using "))
    (display
     (format #f "the formula~%"))
    (display
     (format #f "pi/4 = [2*4*4*6*6*8...]/[3*3*5*5*7*7...]~%"))
    (display
     (format #f "b.  If your product procedure generates "))
    (display
     (format #f "a recursive process,~%"))
    (display
     (format #f "write one that generates an iterative "))
    (display
     (format #f "process. If it~%"))
    (display
     (format #f "generates an iterative process, write "))
    (display
     (format #f "one that generates~%"))
    (display
     (format #f "a recursive process.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.31 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-pi-iter-loop)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
