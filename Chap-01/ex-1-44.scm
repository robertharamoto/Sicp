#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.44                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 6, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (compose a-func b-func)
  (begin
    (lambda (xx)
      (begin
        (a-func (b-func xx))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (repeated a-func nn)
  (begin
    (let ((result-func a-func)
          (nmax (1- nn)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (let ((next-func
                   (compose a-func result-func)))
              (begin
                (set! result-func next-func)
                ))
            ))
        result-func
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (avg-3 a b c)
  (begin
    (/ (+ a b c) 3.0)
    ))

;;;#############################################################
;;;#############################################################
(define (smoothed func)
  (define dx 1e-6)
  (begin
    (lambda (x)
      (begin
        (avg-3
         (func (- x dx))
         (func x)
         (func (+ x dx)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (n-fold-smoothed func n)
  (begin
    (repeated (smoothed func) n)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The idea of smoothing a function is an "))
    (display
     (format #f "important concept in signal~%"))
    (display
     (format #f "processing. If f is a function and dx "))
    (display
     (format #f "is some small number, then the~%"))
    (display
     (format #f "smoothed version of f is the function "))
    (display
     (format #f "whose value at a point x is the~%"))
    (display
     (format #f "average of f(x - dx), f(x), and "))
    (display
     (format #f "f(x + dx). Write a procedure smooth~%"))
    (display
     (format #f "that takes as input a procedure that "))
    (display
     (format #f "computes f and returns a procedure~%"))
    (display
     (format #f "that computes the smoothed f. It is "))
    (display
     (format #f "sometimes valuable to repeatedly~%"))
    (display
     (format #f "smooth a function (that is, smooth "))
    (display
     (format #f "the smoothed function, and so on)~%"))
    (display
     (format #f "to obtained the n-fold smoothed "))
    (display
     (format #f "function. Show how to generate~%"))
    (display
     (format #f "the n-fold smoothed function of any "))
    (display
     (format #f "given function using smooth and~%"))
    (display
     (format #f "repeated from exercise 1.43.~%"))
    (newline)
    (display
     (format #f "A smoothed function looks like~%"))
    (display
     (format #f "g1 = (lambda (x) (average (f (- x dx))~%"))
    (display
     (format #f "    (f x) (f (+ x dx)))).~%"))
    (display
     (format #f "A two-fold smoothed function looks "))
    (display
     (format #f "like g2 = (lambda (x) (average~%"))
    (display
     (format #f "(g1 (- x dx)) (g1 x) (g1 (+ x dx)))),~%"))
    (display
     (format #f "g2 = (compose g1 g1) = (repeated g1 2)~%"))
    (display
     (format #f "(define (repeated a-func nn)~%"))
    (display
     (format #f "  (let ((result-func a-func)~%"))
    (display
     (format #f "        (nmax (1- nn)))~%"))
    (display
     (format #f "     (begin~%"))
    (display
     (format #f "       (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "           ((>= ii nmax))~%"))
    (display
     (format #f "         (begin~%"))
    (display
     (format #f "           (let ((next-func~%"))
    (display
     (format #f "                  (compose a-func "))
    (display
     (format #f "result-func)))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "               (set! result-func next-func)~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        result-func~%"))
    (display
     (format #f "        )))~%"))
    (newline)
    (display
     (format #f "(define (compose a-func b-func)~%"))
    (display
     (format #f "  (lambda (xx) (a-func (b-func xx))))~%"))
    (newline)
    (display
     (format #f "(define (avg-3 a b c)~%"))
    (display
     (format #f " (/ (+ a b c) 3.0))~%"))
    (newline)
    (display
     (format #f "(define (smoothed func)~%"))
    (display
     (format #f "  (define dx 1e-6)~%"))
    (display
     (format #f "  (lambda (x) (avg-3 (func ~%"))
    (display
     (format #f "(- x dx)) (func x) (func (+ x dx)))))~%"))
    (newline)
    (display
     (format #f "(define (n-fold-smoothed func n)~%"))
    (display
     (format #f "  (repeated (smoothed func) n))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.44 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
