#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.43                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 6, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (compose a-func b-func)
  (begin
    (lambda (xx)
      (begin
        (a-func (b-func xx))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (repeated a-func nn)
  (begin
    (let ((result-func a-func)
          (nmax (1- nn)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (let ((next-func
                   (compose a-func result-func)))
              (begin
                (set! result-func next-func)
                ))
            ))
        result-func
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (square xx)
  (begin
    (* xx xx)
    ))

;;;#############################################################
;;;#############################################################
(define (inc xx)
  (begin
    (+ xx 1)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "If f is a numerical function and n is "))
    (display
     (format #f "a positive integer,~%"))
    (display
     (format #f "then we can form the "))
    (display
     (format #f "nth repeated application of f, which is~%"))
    (display
     (format #f "defined to be the function whose value "))
    (display
     (format #f "at x is~%"))
    (display
     (format #f "f(f(...(f(x))...)). For example, at x "))
    (display
     (format #f "is f(f(...(f(x))...)).~%"))
    (display
     (format #f "In addition, if f is the function x"))
    (display
     (format #f "-> x + 1, then~%"))
    (display
     (format #f "the nth repeated application of f is "))
    (display
     (format #f "the function~%"))
    (display
     (format #f "x -> x + n. If f is the operation of "))
    (display
     (format #f "squaring a number,~%"))
    (display
     (format #f "then the nth repeated application of f "))
    (display
     (format #f "is the function~%"))
    (display
     (format #f "that raises its argument to the 2*nth "))
    (display
     (format #f "power. Write a~%"))
    (display
     (format #f "procedure that takes as inputs a "))
    (display
     (format #f "procedure that computes~%"))
    (display
     (format #f "f and a positive integer n and returns "))
    (display
     (format #f "the procedure that~%"))
    (display
     (format #f "computes the nth repeated application of "))
    (display
     (format #f "f. Your procedure~%"))
    (display
     (format #f "should be able to be used as follows:~%"))
    (display
     (format #f "((repeated square 2) 5)~%"))
    (display
     (format #f "625~%"))
    (newline)

    (display
     (format #f "(define (repeated a-func nn)~%"))
    (display
     (format #f "  (let ((result-func a-func)~%"))
    (display
     (format #f "        (nmax (1- nn)))~%"))
    (display
     (format #f "     (begin~%"))
    (display
     (format #f "       (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "           ((>= ii nmax))~%"))
    (display
     (format #f "         (begin~%"))
    (display
     (format #f "           (let ((next-func~%"))
    (display
     (format #f "                  (compose a-func "))
    (display
     (format #f "result-func)))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "               (set! "))
    (display
     (format #f "result-func next-func)~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        result-func~%"))
    (display
     (format #f "        )))~%"))
    (newline)
    (display
     (format #f "(define (compose a-func b-func)~%"))
    (display
     (format #f "  (lambda (xx) (a-func (b-func xx))))~%"))
    (newline)
    (display
     (format #f "(define (square xx) (* xx xx))~%"))
    (display
     (format #f "(define (inc xx) (+ xx 1))~%"))
    (newline)
    (display
     (format #f "((repeated square 2) 5) = ~a~%"
             ((repeated square 2) 5)))
    (display
     (format #f "((repeated square 3) 2) = ~a~%"
             ((repeated square 3) 2)))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.43 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format
            #f "~a~%"
            (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
