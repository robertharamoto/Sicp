#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.6                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, run-all-tests, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (square x)
  (begin
    (* x x)
    ))

;;;#############################################################
;;;#############################################################
(define (improve guess x)
  (begin
    (average guess (/ x guess))
    ))

;;;#############################################################
;;;#############################################################
(define (average x y)
  (begin
    (/ (+ x y) 2)
    ))

;;;#############################################################
;;;#############################################################
(define (good-enough? guess x tol)
  (begin
    (< (abs (- (square guess) x)) tol)
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-iter guess x tol)
  (begin
    (if (good-enough? guess x tol)
        (begin
          (exact->inexact guess))
        (begin
          (sqrt-iter
           (improve guess x)
           x tol)
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sqrt-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-sqrt-iter-1")
         (test-list
          (list
           (list 1 2 1e-6 1.414214)
           (list 1 3 1e-6 1.732051)
           (list 1 4 1e-6 2.000000)
           (list 1 5 1e-6 2.236068)
           (list 1 6 1e-6 2.449490)
           (list 1 7 1e-6 2.645751)
           (list 1 8 1e-6 2.828427)
           (list 1 9 1e-6 3.000000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((guess (list-ref this-list 0))
                  (xx (list-ref this-list 1))
                  (tol (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (sqrt-iter guess xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : guess=~a, x=~a, tol=~a, "
                        sub-name test-label-index guess xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sicp-sqrt x tol)
  (begin
    (sqrt-iter 1.0 x tol)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sicp-sqrt-1 result-hash-table)
 (begin
   (let ((sub-name "test-sicp-sqrt-1")
         (test-list
          (list
           (list 2 1e-6 1.414214)
           (list 3 1e-6 1.732051)
           (list 4 1e-6 2.000000)
           (list 5 1e-6 2.236068)
           (list 6 1e-6 2.449490)
           (list 7 1e-6 2.645751)
           (list 8 1e-6 2.828427)
           (list 9 1e-6 3.000000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (tol (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (sicp-sqrt xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : x=~a, tol=~a, "
                        sub-name test-label-index xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (new-if predicate then-clause else-clause)
  (begin
    (cond
     (predicate
      (begin
        then-clause
        ))
     (else
      (begin
        else-clause
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (alyssa-sqrt-iter guess x tol)
  (begin
    (new-if
     (good-enough? guess x tol)
     (exact->inexact guess)
     (sqrt-iter
      (improve guess x)
      x tol))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-alyssa-sqrt-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-alyssa-sqrt-iter-1")
         (test-list
          (list
           (list 1 2 1e-6 1.414214)
           (list 1 3 1e-6 1.732051)
           (list 1 4 1e-6 2.000000)
           (list 1 5 1e-6 2.236068)
           (list 1 6 1e-6 2.449490)
           (list 1 7 1e-6 2.645751)
           (list 1 8 1e-6 2.828427)
           (list 1 9 1e-6 3.000000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((guess (list-ref this-list 0))
                  (xx (list-ref this-list 1))
                  (tol (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (alyssa-sqrt-iter guess xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : guess=~a, x=~a, tol=~a, "
                        sub-name test-label-index guess xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (alyssa-sicp-sqrt x tol)
  (begin
    (alyssa-sqrt-iter 1.0 x tol)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-alyssa-sicp-sqrt-1 result-hash-table)
 (begin
   (let ((sub-name "test-alyssa-sicp-sqrt-1")
         (test-list
          (list
           (list 2 1e-6 1.4142136)
           (list 3 1e-6 1.7320508)
           (list 4 1e-6 2.000000)
           (list 5 1e-6 2.236068)
           (list 6 1e-6 2.449490)
           (list 7 1e-6 2.645751)
           (list 8 1e-6 2.828427)
           (list 9 1e-6 3.000000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (tol (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (alyssa-sicp-sqrt xx tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : x=~a, tol=~a, "
                        sub-name test-label-index xx tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((sub-name "main-loop")
          (tolerance 1.0e-12)
          (min-ii 1)
          (max-ii 20))
      (begin
        (display (format #f "tolerance = ~a~%" tolerance))
        (force-output)
        (do ((ii min-ii (1+ ii)))
            ((> ii max-ii))
          (begin
            (let ((asqrt (alyssa-sicp-sqrt ii tolerance))
                  (ssqrt (sicp-sqrt ii tolerance)))
              (begin
                (if (> (abs (- asqrt ssqrt)) tolerance)
                    (begin
                      (display
                       (format
                        #f "~a : error (~a) : ii=~a, tol=~a, "
                        sub-name ii ii tolerance))
                      (display
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        ssqrt asqrt)))
                    (begin
                      (display
                       (ice-9-format:format
                        #f "sqrt(~a) = ~6,4f : sqrt^2 = ~6,4f~%"
                        ii asqrt (* asqrt asqrt)))
                      ))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "What happens when Alyssa attempts "))
    (display
     (format #f "to use this to~%"))
    (display
     (format #f "compute square roots? Explain.~%"))
    (display
     (format #f "According to the tests, the "))
    (display
     (format #f "new-if produced the~%"))
    (display
     (format #f "same results as the original program. "))
    (display
     (format #f "This is probably~%"))
    (display
     (format #f "because the if statement is a special "))
    (display
     (format #f "case of the cond~%"))
    (display
     (format #f "statement. However, the new-if "))
    (display
     (format #f "function will cause~%"))
    (display
     (format #f "scheme to evaluate all the arguments, "))
    (display
     (format #f "whereas the if~%"))
    (display
     (format #f "statement will evaluate only the "))
    (display
     (format #f "then-clause if the~%"))
    (display
     (format #f "predicate is true or only the "))
    (display
     (format #f "else-clause if the~%"))
    (display
     (format #f "predicate is false.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.6 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
