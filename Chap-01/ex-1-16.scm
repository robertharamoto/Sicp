#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.16                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; returns a (list expt-result niterations)
(define (fast-expt-recursive bb nn)
  (begin
    (cond
     ((< nn 0)
      (begin
        (let ((xx-list
               (fast-expt-recursive
                (/ 1 bb) (* -1 nn))))
          (let ((xx-value (list-ref xx-list 0))
                (xx-count (list-ref xx-list 1)))
            (begin
              (list xx-value (1+ xx-count))
              )))
        ))
     ((= nn 0)
      (begin
        (list 1 0)
        ))
     ((even? nn)
      (begin
        (let ((xx-list
               (fast-expt-recursive
                bb (euclidean/ nn 2))))
          (let ((xx-value (list-ref xx-list 0))
                (xx-count (list-ref xx-list 1)))
            (begin
              (list (* xx-value xx-value) (1+ xx-count))
              )))
        ))
     (else
      (begin
        (let ((xx-list
               (fast-expt-recursive
                (* bb bb) (euclidean/ (1- nn) 2))))
          (let ((xx-value (list-ref xx-list 0))
                (xx-count (list-ref xx-list 1)))
            (begin
              (list (* xx-value bb) (1+ xx-count))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fast-expt-recursive-1 result-hash-table)
 (begin
   (let ((sub-name "test-fast-expt-recursive-1")
         (test-list
          (list
           (list 2 0 1 0) (list 2 1 2 1)
           (list 2 2 4 2) (list 2 3 8 2)
           (list 2 4 16 3) (list 2 5 32 3)
           (list 2 6 64 3) (list 2 7 128 3)
           (list 2 8 256 4) (list 2 9 512 4)
           (list 2 10 1024 4)
           (list 2 -2 1/4 3) (list 2 -3 1/8 3)
           (list 2 -4 1/16 4)
           (list 3 0 1 0) (list 3 1 3 1)
           (list 3 2 9 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((bb (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (shouldbe-value (list-ref this-list 2))
                  (shouldbe-count (list-ref this-list 3)))
              (let ((result-list (fast-expt-recursive bb nn)))
                (let ((result-value (list-ref result-list 0))
                      (result-count (list-ref result-list 1)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : bb=~a, nn=~a, "
                          sub-name test-label-index bb nn))
                        (err-2
                         (format
                          #f "shouldbe-value=~a, result=~a"
                          shouldbe-value result-value))
                        (err-3
                         (format
                          #f "shouldbe-count=~a, result=~a"
                          shouldbe-count result-count)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-value result-value)
                       sub-name
                       (string-append
                        err-1 err-2)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? shouldbe-count result-count)
                       sub-name
                       (string-append
                        err-1 err-3)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax debug-iterative-macro
  (syntax-rules ()
    ((debug-iterative-macro
      location-string bb nn
      niter nn-count next-bb nresult)
     (begin
       (display
        (format
         #f "debug (~a) bb=~a, nn=~a, niter=~a, nn-count=~a, "
         location-string bb nn niter nn-count))
       (display
        (format
         #f "next-bb=~a, nresult=~a~%"
         next-bb nresult))
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a (list expt-result niterations)
(define (fast-expt-iterative bb nn)
  (begin
    (let ((continue-flag #t)
          (nresult 1)
          (niter 0)
          (nn-count nn)
          (next-bb bb))
      (begin
        (if (< nn 0)
            (begin
              (set! next-bb (/ 1 bb))
              (set! nn-count (* -1 nn))
              ))
        (while
         (equal? continue-flag #t)
         (begin
           (cond
            ((<= nn-count 0)
             (begin
               (set! continue-flag #f)
               ))
            ((= nn-count 1)
             (begin
               (set! nresult (* nresult next-bb))
               (set! nn-count (1- nn-count))
               (set! niter (1+ niter))
               (set! continue-flag #f)
               ))
            ((even? nn-count)
             (begin
               (let ((next-count
                      (euclidean/ nn-count 2))
                     (next-bb2 (* next-bb next-bb)))
                 (begin
                   (set! next-bb next-bb2)
                   (set! niter (1+ niter))
                   (set! nn-count next-count)
                   (if (<= nn-count 1)
                       (begin
                         (set! nresult (* next-bb nresult))
                         (set! continue-flag #f)
                         ))
                   ))
               ))
            (else
             (begin
               (set! niter (1+ niter))
               (set! nn-count (1- nn-count))

               (set! nresult (* nresult next-bb))
               )))
           ))

        (list nresult niter)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fast-expt-iterative-1 result-hash-table)
 (begin
   (let ((sub-name "test-fast-expt-iterative-1")
         (test-list
          (list
           (list 2 0 1 0) (list 2 1 2 1) (list 2 2 4 1)
           (list 2 3 8 2) (list 2 4 16 2) (list 2 5 32 3)
           (list 2 6 64 3) (list 2 7 128 4) (list 2 8 256 3)
           (list 2 9 512 4) (list 2 10 1024 4)
           (list 2 -2 1/4 1) (list 2 -3 1/8 2)
           (list 2 -4 1/16 2)
           (list 3 0 1 0) (list 3 1 3 1) (list 3 2 9 1)
           (list 3 3 27 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((bb (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (shouldbe-value (list-ref this-list 2))
                  (shouldbe-count (list-ref this-list 3)))
              (let ((rlist (fast-expt-iterative bb nn)))
                (let ((result-value (list-ref rlist 0))
                      (result-count (list-ref rlist 1)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : bb=~a, nn=~a, "
                          sub-name test-label-index bb nn))
                        (err-2
                         (format
                          #f "shouldbe-value=~a, result=~a"
                          shouldbe-value result-value))
                        (err-3
                         (format
                          #f "shouldbe-count=~a, result=~a"
                          shouldbe-count result-count)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-value result-value)
                       sub-name
                       (string-append
                        err-1 err-2)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? shouldbe-count result-count)
                       sub-name
                       (string-append
                        err-1 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-loop bb nn)
  (begin
    (let ((expt-rec-list (fast-expt-recursive bb nn))
          (expt-iter-list (fast-expt-iterative bb nn)))
      (let ((expt-rec-value (list-ref expt-rec-list 0))
            (expt-rec-count (list-ref expt-rec-list 1))
            (expt-iter-value (list-ref expt-iter-list 0))
            (expt-iter-count (list-ref expt-iter-list 1)))
        (begin
          (if (equal? expt-rec-value expt-iter-value)
              (begin
                (display
                 (ice-9-format:format
                  #f "~a^~a = ~:d    :    "
                  bb nn expt-iter-value))
                (display
                 (ice-9-format:format
                  #f "recursive count = ~:d, "
                  expt-rec-count))
                (display
                 (ice-9-format:format
                  #f "iterative count = ~:d~%"
                  expt-iter-count)))
              (begin
                (display
                 (ice-9-format:format
                  #f "***** ~a^~a = (~:d recursive) != "
                  bb nn expt-rec-value))
                (display
                 (ice-9-format:format
                  #f "(~:d iterative)    :    "
                  expt-iter-value))
                (display
                 (ice-9-format:format
                  #f "recursive count = ~:d, "
                  expt-rec-count))
                (display
                 (ice-9-format:format
                  #f "iterative count = ~:d~%"
                  expt-iter-count))
                ))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((sub-name "main-loop")
          (bb 2))
      (begin
        (do ((nn 0 (1+ nn)))
            ((> nn 10))
          (begin
            (sub-loop bb nn)
            ))

        (newline)
        (sub-loop 2 20)
        (sub-loop 2 32)
        (sub-loop 2 64)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Design a procedure that evolves "))
    (display
     (format #f "an iterative~%"))
    (display
     (format #f "exponentiation process that uses "))
    (display
     (format #f "successive squaring and~%"))
    (display
     (format #f "uses a logarithmic number of steps, "))
    (display
     (format #f "as does fast-expt.~%"))
    (newline)
    (display
     (format #f "See the websites:~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Exponentiation_by_squaring~%"))
    (display
     (format #f "https://www.geeksforgeeks.org/exponential-squaring-fast-modulo-multiplication/~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.16 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
