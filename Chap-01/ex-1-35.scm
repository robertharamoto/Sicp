#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.35                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 5, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (fixed-point func first-guess tolerance)
  (define (local-try guess tol)
    (let ((next (func guess)))
      (begin
        (if (< (abs (- next guess)) tol)
            (begin
              next)
            (begin
              (local-try next tol)
              ))
        )))
  (begin
    (local-try first-guess tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (golden-ratio tolerance)
  (define (local-func xx)
    (+ 1.0 (/ 1.0 xx)))
  (begin
    (fixed-point local-func 1 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-golden-ratio-1 result-hash-table)
 (begin
   (let ((sub-name "test-golden-ratio-1")
         (test-list
          (list
           (list 1e-3 1.618) (list 1e-4 1.6180)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((tol (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (golden-ratio tol)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : tol=~a, "
                        sub-name test-label-index tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (run-main-loop)
  (begin
    (let ((sub-name "run-main-loop")
          (test-tol-list
           (list
            (list 1e-4) (list 1e-5) (list 1e-6) (list 1e-7)
            (list 1e-8) (list 1e-9) (list 1e-10) (list 1e-11)
            ))
          (test-label-index 0))
      (begin
        ;;; first display factorial calculations
        (display
         (format #f "golden ratio fixed-point calculations~%"))
        (display
         (format #f "golden ratio phi = 1.6180339887...~%"))
        (force-output)
        (for-each
         (lambda (tlist)
           (begin
             (let ((tol (list-ref tlist 0)))
               (let ((phi (golden-ratio tol)))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "phi = ~14,12f  (tol=~a)~%"
                     phi tol))
                   (force-output))
                 ))
             (set! test-label-index (1+ test-label-index))
             )) test-tol-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Show that the golden ratio (section "))
    (display
     (format #f "1.2.2) is a fixed~%"))
    (display
     (format #f "point of the transformation x-> 1 + 1/x, "))
    (display
     (format #f "and use this fact~%"))
    (display
     (format #f "to compute by means of the fixed point "))
    (display
     (format #f "procedure.~%"))
    (newline)
    (display
     (format #f "A fixed point is defined as f(x) = x. "))
    (display
     (format #f "So if~%"))
    (display
     (format #f "f(x) = 1+1/x, then the fixed point if "))
    (display
     (format #f "f is~%"))
    (display
     (format #f "f(x) = 1+1/x = x~%"))
    (display
     (format #f "this implies x^2=x+1, which is satisfied "))
    (display
     (format #f "by the golden~%"))
    (display
     (format #f "ratio phi.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.35 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (run-main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
