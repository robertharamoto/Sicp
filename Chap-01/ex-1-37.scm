#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.37                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 5, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (continued-fraction num-func den-func nn)
  (define (local-iter curr-iter acc)
    (begin
      (if (< curr-iter 1)
          (begin
            acc)
          (begin
            (let ((this-numerator (num-func curr-iter))
                  (this-denominator (den-func curr-iter)))
              (let ((next-acc
                     (/ this-numerator
                        (+ this-denominator acc))))
                (begin
                  (local-iter (1- curr-iter) next-acc)
                  )))
            ))
      ))
  (begin
    (local-iter nn 0)
    ))

;;;#############################################################
;;;#############################################################
(define (golden-ratio kk)
  (begin
    (/ 1.0
       (continued-fraction
        (lambda (ii) 1.0)
        (lambda (ii) 1.0)
        kk))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-golden-ratio-1 result-hash-table)
 (begin
   (let ((sub-name "test-golden-ratio-1")
         (test-list
          (list
           (list 10 1.6182) (list 100 1.6180)
           ))
         (tolerance 1e-4)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((kk (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (golden-ratio kk)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : kk=~a, "
                        sub-name test-label-index kk))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (continued-fraction-rec num-func den-func nn)
  (define (local-rec curr-iter max-iter)
    (begin
      (if (> curr-iter max-iter)
          (begin
            0.0)
          (begin
            (let ((this-numerator (num-func curr-iter))
                  (this-denominator (den-func curr-iter)))
              (let ((second-denom
                     (local-rec (1+ curr-iter) max-iter)))
                (let ((result-acc
                       (/ this-numerator
                          (+ this-denominator
                             second-denom))))
                  (begin
                    result-acc
                    ))
                ))
            ))
      ))
  (begin
    (local-rec 1 nn)
    ))

;;;#############################################################
;;;#############################################################
(define (golden-ratio-rec kk)
  (begin
    (/ 1.0
       (continued-fraction-rec
        (lambda (ii) 1.0)
        (lambda (ii) 1.0)
        kk))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-golden-ratio-rec-1 result-hash-table)
 (begin
   (let ((sub-name "test-golden-ratio-rec-1")
         (test-list
          (list
           (list 10 1.6182) (list 100 1.6180)
           ))
         (tolerance 1e-4)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((kk (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (golden-ratio-rec kk)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : kk=~a, "
                        sub-name test-label-index kk))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tolerance)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-golden-ratio-kk gr-func phi max-kk tolerance)
  (begin
    (let ((end-loop-flag #f)
          (result-kk 0))
      (begin
        (do ((kk 1 (1+ kk)))
            ((or (> kk max-kk)
                 (equal? end-loop-flag #t)))
          (begin
            (let ((ll-phi (gr-func kk)))
              (let ((abs-diff (abs (- ll-phi phi))))
                (begin
                  (if (< abs-diff tolerance)
                      (begin
                        (set! end-loop-flag #t)
                        (set! result-kk kk)
                        ))
                  )))
            ))
        result-kk
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "golden ratio continued-fraction calculations~%"))
    (display
     (format #f "golden ratio phi = 1.6180339887...~%"))
    (force-output)
    (let ((tolerance 1e-4)
          (phi 1.6180339887)
          (max-kk 10000))
      (begin
        (let ((phi-iter-kk
               (find-golden-ratio-kk
                golden-ratio phi max-kk tolerance)))
          (let ((phi-iter (golden-ratio phi-iter-kk)))
            (begin
              (display
               (ice-9-format:format
                #f "iteration: phi(~:d) = ~12,10f  (tol=~a)~%"
                phi-iter-kk phi-iter tolerance))
              (force-output)
              )))

        (let ((phi-rec-kk
               (find-golden-ratio-kk
                golden-ratio-rec phi max-kk tolerance)))
          (let ((phi-rec (golden-ratio phi-rec-kk)))
            (begin
              (display
               (ice-9-format:format
                #f "recursive: phi(~:d) = ~12,10f  (tol=~a)~%"
                phi-rec-kk phi-rec tolerance))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "a. An infinite continued fraction is an "))
    (display
     (format #f "expression of the form~%"))
    (display
     (format #f "f=N_1/(D_1+(N_2/(D_2+N_3/(D3+...))))~%"))
    (display
     (format #f "As an example, one can show that the "))
    (display
     (format #f "infinite continued fraction~%"))
    (display
     (format #f "expansion with the N_i and the D_i all "))
    (display
     (format #f "equal to 1 produces 1/phi,~%"))
    (display
     (format #f "where phi is the golden ratio (described "))
    (display
     (format #f "in section 1.2.2). One ~%"))
    (display
     (format #f "way to approximate an infinite "))
    (display
     (format #f "continued fraction is to truncate~%"))
    (display
     (format #f "the expansion after a given number of terms. "))
    (display
     (format #f "Such a truncation~%"))
    (display
     (format #f "-- a so-called k-term finite continued "))
    (display
     (format #f "fraction -- has the form~%"))
    (display
     (format #f "N_1/(D_1+(N_2/(D_2+N_3/(D3+...+(N_k/D_k)))))~%"))
    (newline)
    (display
     (format #f "Suppose that n and d are procedures "))
    (display
     (format #f "of one argument (the term~%"))
    (display
     (format #f "index i) that return the N_i and D_i of "))
    (display
     (format #f "the terms of the~%"))
    (display
     (format #f "continued fraction. Define a procedure "))
    (display
     (format #f "cont-frac such that~%"))
    (display
     (format #f "evaluating (cont-frac n d k) computes "))
    (display
     (format #f "the value of the~%"))
    (display
     (format #f "k-term finite continued fraction. Check "))
    (display
     (format #f "your procedure by~%"))
    (display
     (format #f "approximating 1/phi using~%"))
    (display
     (format #f "(cont-frac (lambda (i) 1.0)~%"))
    (display
     (format #f "           (lambda (i) 1.0)~%"))
    (display
     (format #f "           k)~%"))
    (newline)
    (display
     (format #f "for successive values of k. How large "))
    (display
     (format #f "must you make k in~%"))
    (display
     (format #f "order to get an approximation that is "))
    (display
     (format #f "accurate to 4 decimal~%"))
    (display
     (format #f "places?~%"))
    (newline)
    (display
     (format #f "b. If your cont-frac procedure generates "))
    (display
     (format #f "a recursive process,~%"))
    (display
     (format #f "write one that generates an iterative "))
    (display
     (format #f "process. If it~%"))
    (display
     (format #f "generates an iterative process, write "))
    (display
     (format #f "one that generates~%"))
    (display
     (format #f "a recursive process.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.37 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
