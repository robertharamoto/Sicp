#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.13                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Prove that Fib(n) is the closest "))
    (display
     (format #f "integer to~%"))
    (display
     (format #f "(phi^2)/sqrt(5), where phi=(1+sqrt(5))/2.~%"))
    (display
     (format #f "Hint: Let psi=(1-sqrt(5))/2.~%"))
    (display
     (format #f "Use induction and the definition "))
    (display
     (format #f "of the Fibonacci~%"))
    (display
     (format #f "numbers (see section 1.2.2,) to prove~%"))
    (display
     (format #f "that Fib(n)=(phi^n - psi^n)/sqrt(5).~%"))
    (newline)

    (display
     (format #f "Induction method: Fib(n) = phi^n-psi^n~%"))
    (display
     (format #f "by assumption then, when n = 1, "))
    (display
     (format #f "Fib(1) = 1~%"))
    (display
     (format #f "by definition and (phi^1-psi^1)/sqrt(5) "))
    (display
     (format #f "= (1+sqrt(5)~%"))
    (display
     (format #f "-(1-sqrt(5)))/(2*sqrt(5)) = 1, "))
    (display
     (format #f "so formula is~%"))
    (display
     (format #f "true for n = 1.~%"))
    (newline)
    (display
     (format #f "when n = 2, Fib(2)=Fib(1)+Fib(0)"))
    (display
     (format #f "=1+0=1~%"))
    (display
     (format #f "(phi^2 - psi^2)/sqrt(5) = "))
    (display
     (format #f "(1+2sqrt(5)+~%"))
    (display
     (format #f "5-1+2sqrt(5)-5)/(4sqrt(5)) "))
    (display
     (format #f "= sqrt(5)/sqrt(5)~%"))
    (display
     (format #f "= 1 = Fib(2)~%"))
    (display
     (format #f "so formula is true for n = 2.~%"))
    (newline)
    (display
     (format #f "Suppose formula is true for some "))
    (display
     (format #f "n and n-1.~%"))
    (display
     (format #f "Fib(n) = (phi^n - psi^n)/sqrt(5)~%"))
    (display
     (format #f "Then Fib(n)+Fib(n-1) = "))
    (display
     (format #f "(phi^n-psi^n)/sqrt(5)~%"))
    (display
     (format #f "  + (phi^(n-1)-psi^(n-1))/sqrt(5)~%"))
    (display
     (format #f "Fib(n)+Fib(n-1) = "))
    (display
     (format #f "phi^(n+1)*(1/phi+1/phi^2)/sqrt(5)~%"))
    (display
     (format #f "  - psi^(n+1)*(1/psi+1/psi^2)/sqrt(5)~%"))
    (display
     (format #f "Fib(n)+Fib(n-1) = "))
    (display
     (format #f "phi^(n+1)*(1+phi)/(sqrt(5)*phi^2)~%"))
    (display
     (format #f "  - psi^(n+1)*(psi+1)/"))
    (display
     (format #f "(sqrt(5t)*psi^2).~%"))
    (newline)
    (display
     (format #f "aside: (1+phi)/phi^2 = "))
    (display
     (format #f "(1+(1+sqrt(5)/2))*4/(1+2sqrt(5)+5)~%"))
    (display
     (format #f "  = (6+2sqrt(5))/(6+2*sqrt(5)) = 1~%"))
    (display
     (format #f "also: (psi+1)/psi^2 = "))
    (display
     (format #f "((1-sqrt(5))/2+1)*4/(1-2sqrt(5)+5)~%"))
    (display
     (format #f "  = (6-2sqrt(5))/(6-2sqrt(5)) = 1~%"))
    (newline)
    (display
     (format #f "so Fib(n+1) = Fib(n)+Fib(n-1)~%"))
    (display
     (format #f " (by definition of fib), simplifies to~%"))
    (display
     (format #f "Fib(n+1) = phi^(n+1)/sqrt(5) "))
    (display
     (format #f " - psi^(n+1)/sqrt(5).~%"))
    (newline)
    (display
     (format #f "So we showed that the formula is "))
    (display
     (format #f "true when n=1 and~%"))
    (display
     (format #f "n=2. We then showed that if it's "))
    (display
     (format #f "true for arbitrary~%"))
    (display
     (format #f "n and n-1, then it's true for n+1. "))
    (display
     (format #f "Therefore the~%"))
    (display
     (format #f "formula is true for all n.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.13 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
