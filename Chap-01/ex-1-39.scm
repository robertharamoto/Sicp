#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.39                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 5, 2022                               ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (continued-fraction num-func den-func nn)
  (define (local-iter curr-iter acc)
    (begin
      (if (< curr-iter 1)
          (begin
            acc)
          (begin
            (let ((this-numerator (num-func curr-iter))
                  (this-denominator (den-func curr-iter)))
              (let ((next-acc
                     (/ this-numerator
                        (+ this-denominator acc))))
                (begin
                  (local-iter (1- curr-iter) next-acc)
                  )))
            ))
      ))
  (begin
    (local-iter nn 0)
    ))

;;;#############################################################
;;;#############################################################
(define (denom-function ii)
  (begin
    (let ((jj (- (* 2 ii) 1)))
      (begin
        jj
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-denom-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-denom-function-1")
         (test-list
          (list
           (list 1 1) (list 2 3) (list 3 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ii (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (denom-function ii)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : ii=~a, "
                        sub-name test-label-index ii))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (tan-cf-iter xx kk)
  (begin
    (let ((xx-2 (* xx xx)))
      (begin
        (continued-fraction
         (lambda (ii)
           (begin
             (if (= ii 1) xx (- xx-2))
             ))
         denom-function
         kk)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tan-cf-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-tan-cf-iter-1")
         (test-list
          (list
           (list 0.785398163 10 1e-4 1.0)
           (list 0.785398163 100 1e-6 1.0)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (tol (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (tan-cf-iter xx nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, nn=~a, tol=~a, "
                        sub-name test-label-index
                        xx nn tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (continued-fraction-rec num-func den-func nn)
  (define (local-rec curr-iter max-iter)
    (begin
      (if (> curr-iter max-iter)
          (begin
            0.0)
          (begin
            (let ((this-numerator (num-func curr-iter))
                  (this-denominator (den-func curr-iter)))
              (let ((second-denom
                     (local-rec (1+ curr-iter) max-iter)))
                (let ((result-acc
                       (/ this-numerator
                          (+ this-denominator
                             second-denom))))
                  (begin
                    result-acc
                    ))
                ))
            ))
      ))
  (begin
    (local-rec 1 nn)
    ))

;;;#############################################################
;;;#############################################################
(define (tan-cf-rec xx kk)
  (begin
    (let ((xx-2 (* xx xx)))
      (begin
        (continued-fraction-rec
         (lambda (ii)
           (begin
             (if (= ii 1) xx (- xx-2))
             ))
         denom-function
         kk)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tan-cf-rec-1 result-hash-table)
 (begin
   (let ((sub-name "test-tan-cf-rec-1")
         (test-list
          (list
           (list 0.785398163 10 1e-4 1.0)
           (list 0.785398163 100 1e-6 1.0)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (tol (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (tan-cf-rec xx nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, nn=~a, tol=~a, "
                        sub-name test-label-index
                        xx nn tol))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-tan-tol-kk tan-func xx expected-tan max-nn tolerance)
  (begin
    (let ((end-loop-flag #f)
          (result-nn 0))
      (begin
        (do ((nn 1 (1+ nn)))
            ((or (> nn max-nn)
                 (equal? end-loop-flag #t)))
          (begin
            (let ((ll-tan (tan-func xx nn)))
              (let ((abs-diff (abs (- ll-tan expected-tan))))
                (begin
                  (if (< abs-diff tolerance)
                      (begin
                        (set! end-loop-flag #t)
                        (set! result-nn nn)
                        ))
                  )))
            ))
        result-nn
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "tan continued-fraction calculation~%"))
    (display
     (format #f "pi/4 = 0.785398163, tan(pi/4) = 1.0~%"))
    (force-output)
    (let ((tolerance 1e-6)
          (pi-div-4 0.785398163)
          (expected-tan 1.00)
          (max-nn 10000))
      (begin
        (let ((tan-iter-kk
               (find-tan-tol-kk
                tan-cf-iter pi-div-4 expected-tan max-nn tolerance)))
          (let ((tan-iter-num (tan-cf-iter pi-div-4 tan-iter-kk)))
            (begin
              (display
               (ice-9-format:format
                #f "iteration: tan(~a, ~:d) = ~12,8f  (tol=~a)~%"
                pi-div-4 tan-iter-kk tan-iter-num tolerance))
              (force-output)
              )))

        (let ((tan-rec-kk
               (find-tan-tol-kk
                tan-cf-rec pi-div-4 expected-tan max-nn tolerance)))
          (let ((tan-rec-num (tan-cf-rec pi-div-4 tan-rec-kk)))
            (begin
              (display
               (ice-9-format:format
                #f "recursive: tan(~a, ~:d) = ~12,8f  (tol=~a)~%"
                pi-div-4 tan-rec-kk tan-rec-num tolerance))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A continued fraction representation of the "))
    (display
     (format #f "tangent function was~%"))
    (display
     (format #f "published in 1770 by the German "))
    (display
     (format #f "mathematician J.H. Lambert:~%"))
    (display
     (format #f "tan x = x/(1-x^2/(3-x^2/(5-x^2/(...~%"))
    (display
     (format #f "where x is in radians. Define a "))
    (display
     (format #f "procedure (tan-cf x k)~%"))
    (display
     (format #f "that computes an approximation to the "))
    (display
     (format #f "tangent function based on~%"))
    (display
     (format #f "Lambert's formula. K specifies the number "))
    (display
     (format #f "of terms to compute,~%"))
    (display
     (format #f "as in exercise 1.37.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.39 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
