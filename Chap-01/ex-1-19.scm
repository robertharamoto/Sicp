#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.19                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 2, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (fibonacci-iterative nn)
  (define (local-iter aa bb count)
    (begin
      (if (<= count 0)
          (begin
            bb)
          (begin
            (local-iter (+ aa bb) aa (1- count))
            ))
      ))
  (begin
    (local-iter 1 0 nn)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fibonacci-iterative-1 result-hash-table)
 (begin
   (let ((sub-name "test-fibonacci-iterative-1")
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1)
           (list 3 2) (list 4 3) (list 5 5)
           (list 6 8) (list 7 13) (list 8 21)
           (list 9 34) (list 10 55) (list 11 89)
           (list 12 144)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fibonacci-iterative nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (fibonacci-transform nn)
  (define (local-ft aa bb pp qq count)
    (begin
      (cond
       ((<= count 0)
        (begin
          bb
          ))
       ((even? count)
        (begin
          (let ((qq-2 (* qq qq)))
            (begin
              (local-ft aa bb
                        (+ (* pp pp) qq-2)
                        (+ (* 2 pp qq) qq-2)
                        (/ count 2))
              ))
          ))
       (else
        (begin
          (local-ft (+ (* bb qq) (* aa qq) (* aa pp))
                    (+ (* bb pp) (* aa qq))
                    pp qq (1- count))
          )))
      ))
  (begin
    (local-ft 1 0 0 1 nn)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fibonacci-transform-1 result-hash-table)
 (begin
   (let ((sub-name "test-fibonacci-transform-1")
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1)
           (list 3 2) (list 4 3) (list 5 5)
           (list 6 8) (list 7 13) (list 8 21)
           (list 9 34) (list 10 55) (list 11 89)
           (list 12 144)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fibonacci-transform nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((sub-name "main-loop")
          (max-nn 20)
          (break-count 5)
          (newline-flag #t))
      (begin
        (do ((nn 0 (1+ nn)))
            ((> nn max-nn))
          (begin
            (let ((fib-rec (fibonacci-iterative nn))
                  (fib-tr (fibonacci-transform nn)))
              (begin
                (if (equal? fib-rec fib-tr)
                    (begin
                      (if (equal? newline-flag #t)
                          (begin
                            (display
                             (ice-9-format:format
                              #f "~:d" fib-tr))
                            (set! newline-flag #f))
                          (begin
                            (display
                             (ice-9-format:format
                              #f ", ~:d" fib-tr))
                            )))
                    (begin
                      (display
                       (ice-9-format:format
                        #f "~%~a***error*** fibonacci(~a) = ~:d, "
                        sub-name nn fib-rec))
                      (display
                       (ice-9-format:format
                        #f "transform = ~:d~%" fib-tr))
                      ))
                (if (and (not (zero? nn))
                         (zero? (modulo nn break-count)))
                    (begin
                      (newline)
                      (set! newline-flag #t)
                      ))

                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Recall the transformation of the state "))
    (display
     (format #f "variables a and~%"))
    (display
     (format #f "b in the fib-iter process of section "))
    (display
     (format #f "1.2.2: a <- a + b~%"))
    (display
     (format #f "and b <- a. Call this transformation "))
    (display
     (format #f "T, and observe that~%"))
    (display
     (format #f "applying T over and over again n times, "))
    (display
     (format #f "starting with~%"))
    (display
     (format #f "1 and 0, produces the pair Fib(n+1) "))
    (display
     (format #f "and Fib(n). In~%"))
    (display
     (format #f "other words, the Fibonacci numbers "))
    (display
     (format #f "are produced by~%"))
    (display
     (format #f "applying Tn, the nth power of the "))
    (display
     (format #f "transformation T,~%"))
    (display
     (format #f "starting with the pair (1,0). Now "))
    (display
     (format #f "T to be the~%"))
    (display
     (format #f "special case of p = 0 and q = 1 in a "))
    (display
     (format #f "family of~%"))
    (display
     (format #f "transformations Tpq, where Tpq transforms "))
    (display
     (format #f "the pair (a,b)~%"))
    (display
     (format #f "according to a <- bq + aq + ap and "))
    (display
     (format #f "b <- bp + aq.~%"))
    (display
     (format #f "Show that if we apply such a "))
    (display
     (format #f "transformation Tpq~%"))
    (display
     (format #f "twice, the effect is the same as using "))
    (display
     (format #f "a single~%"))
    (display
     (format #f "transformation Tp'q' of the same form, "))
    (display
     (format #f "and compute p'~%"))
    (display
     (format #f "and q' in terms of p and q.~%"))
    (newline)
    (display
     (format #f "T_{p}_{q}(a,b) = (bq+aq+ap, bp+aq)~%"))
    (display
     (format #f "T_{p}_{q}T_{p}_{q}(a,b) =~%"))
    (display
     (format #f "  T_{p}_{q}(bq+aq+ap, bp+aq)~%"))
    (display
     (format #f "= ((bp+aq)*q+(bq+aq+ap)*q+(bq+aq+ap)*p,~%"))
    (display
     (format #f "   (bp+aq)*p+(bq+aq+ap)*q)~%"))
    (display
     (format #f "= ((b*(2pq+qq)+a*(2pq+qq)+a*(qq+pp),~%"))
    (display
     (format #f "   b*(pp+qq)+a*(2pq+qq))~%"))
    (display
     (format #f "= T_{qq+pp}_{2pq+qq}(a, b)~%"))
    (display
     (format #f "So the transformation T_{p}_{q}T_{p}_{q}(a,b) "))
    (display
     (format #f "is the same~%"))
    (display
     (format #f "as the transformation "))
    (display
     (format #f "T_{qq+pp}_{2pq+qq}(a, b).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.19 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
