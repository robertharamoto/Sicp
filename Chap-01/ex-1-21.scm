#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.21                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 4, 2022                               ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (smallest-divisor nn)
  (define (local-find-divisor nn test-divisor max-divisor)
    (begin
      (cond
       ((> test-divisor max-divisor)
        (begin
          nn
          ))
       ((zero? (remainder nn test-divisor))
        (begin
          test-divisor
          ))
       (else
        (begin
          (local-find-divisor
           nn (+ test-divisor 2) max-divisor)
          )))
      ))
  (begin
    (cond
     ((<= nn 1)
      (begin
        -1
        ))
     ((zero? (remainder nn 2))
      (begin
        2
        ))
     ((zero? (remainder nn 3))
      (begin
        3
        ))
     (else
      (begin
        (let ((max-divisor (1+ (sqrt nn))))
          (begin
            (local-find-divisor nn 5 max-divisor)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-smallest-divisor-1 result-hash-table)
 (begin
   (let ((sub-name "test-smallest-divisor-1")
         (test-list
          (list
           (list 2 2) (list 3 3) (list 4 2) (list 5 5)
           (list 6 2) (list 7 7) (list 8 2) (list 9 3)
           (list 10 2) (list 11 11) (list 12 2) (list 13 13)
           (list 14 2) (list 15 3) (list 16 2) (list 17 17)
           (list 18 2) (list 19 19) (list 20 2) (list 21 3)
           (list 22 2) (list 23 23) (list 24 2) (list 25 5)
           (list 26 2) (list 27 3) (list 28 2) (list 29 29)
           (list 30 2) (list 31 31) (list 32 2) (list 33 3)
           (list 34 2) (list 35 5) (list 36 2) (list 37 37)
           (list 38 2) (list 39 3) (list 40 2) (list 41 41)
           (list 42 2) (list 43 43) (list 44 2) (list 45 3)
           (list 46 2) (list 47 47) (list 48 2) (list 49 7)
           (list 50 2) (list 51 3) (list 52 2) (list 53 53)
           (list 54 2) (list 55 5) (list 56 2) (list 57 3)
           (list 58 2) (list 59 59) (list 60 2) (list 61 61)
           (list 62 2) (list 63 3) (list 64 2) (list 65 5)
           (list 66 2) (list 67 67) (list 68 2) (list 69 3)
           (list 70 2) (list 71 71) (list 72 2) (list 73 73)
           (list 74 2) (list 75 3) (list 76 2) (list 77 7)
           (list 78 2) (list 79 79) (list 80 2) (list 81 3)
           (list 82 2) (list 83 83) (list 84 2) (list 85 5)
           (list 86 2) (list 87 3) (list 88 2) (list 89 89)
           (list 90 2) (list 91 7) (list 92 2) (list 93 3)
           (list 94 2) (list 95 5) (list 96 2) (list 97 97)
           (list 98 2) (list 99 3) (list 100 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (smallest-divisor nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list 199 1999 19999)))
      (begin
        (for-each
         (lambda (anum)
           (begin
             (let ((sd (smallest-divisor anum)))
               (begin
                 (display
                  (ice-9-format:format
                   #f "smallest divisor of ~:d is ~:d~%"
                   anum sd))
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Use the smallest-divisor procedure to "))
    (display
     (format #f "find the smallest~%"))
    (display
     (format #f "divisor of each of the following "))
    (display
     (format #f "numbers: 199,~%"))
    (display
     (format #f "1999, 19999.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.21 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
