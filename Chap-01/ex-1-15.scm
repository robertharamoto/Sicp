#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 1.15                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 22, 2022                              ###
;;;###                                                       ###
;;;###  updated February 19, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "(define (cube x) (* x x x))~%"))
    (display
     (format #f "(define (p x) (- (* 3 x) "))
    (display
     (format #f "(* 4 (cube x))))~%"))
    (display
     (format #f "(define (sine angle)~%"))
    (display
     (format #f "        "))
    (display
     (format #f "(if (not (> (abs angle) 0.1))~%"))
    (display
     (format #f "            "))
    (display
     (format #f "angle~%"))
    (display
     (format #f "            "))
    (display
     (format #f "(p (sine (/ angle 3.0)))))~%"))
    (newline)

    (display
     (format #f "a.  How many times is the procedure p applied "))
    (display
     (format #f "when (sine 12.15)~%"))
    (display
     (format #f "is evaluated?~%"))
    (display
     (format #f "1) (sine 12.15) -> (p (sine 12.15 / 3)) "))
    (display
     (format #f "= (p (sine 4.05))~%"))
    (display
     (format #f "2) -> (p (p (sine 4.05 / 3)))~%"))
    (display
     (format #f "    = (p (p (sine 1.35)))~%"))
    (display
     (format #f "3) -> (p (p (p (sine 1.35 / 3))))~%"))
    (display
     (format #f "    = (p (p (p (sine 0.45))))~%"))
    (display
     (format #f "4) -> (p (p (p (p (sine 0.45 / 3)))))~%"))
    (display
     (format #f "    = (p (p (p (p (sine 0.15)))))~%"))
    (display
     (format #f "5) -> (p (p (p (p (p (sine 0.15 / 3))))))~%"))
    (display
     (format #f "    = (p (p (p (p (p (sine 0.05))))))~%"))
    (display
     (format #f "6) -> (p (p (p (p (p 0.05)))))~%"))
    (newline)
    (display
     (format #f "The procedure p is called 5 times "))
    (display
     (format #f "when (sine 12.15)~%"))
    (display
     (format #f "is evaluated.~%"))
    (newline)
    (display
     (format #f "b.  What is the order of growth in "))
    (display
     (format #f "space and number~%"))
    (display
     (format #f "of steps (as a function of a) used "))
    (display
     (format #f "by the process~%"))
    (display
     (format #f "generated by the sine procedure "))
    (display
     (format #f "when (sine a)~%"))
    (display
     (format #f "is evaluated?~%"))
    (display
     (format #f "a / (3^k) = 0.1, or ln(a) - "))
    (display
     (format #f "kln(3)=-ln(10)~%"))
    (display
     (format #f "k = (ln(a)+ln(10))/ln(3)).~%"))
    (display
     (format #f "The number of steps and space is about "))
    (display
     (format #f "theta(ln(a)).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 1.15 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
