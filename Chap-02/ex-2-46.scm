#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.46                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code macro and current-date-time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for run-all-tests functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-vect x y)
  (begin
    (cons x y)
    ))

;;;#############################################################
;;;#############################################################
(define (xcor-vect vec)
  (begin
    (car vec)
    ))

;;;#############################################################
;;;#############################################################
(define (ycor-vect vec)
  (begin
    (cdr vec)
    ))

;;;#############################################################
;;;#############################################################
(define (add-vect vec1 vec2)
  (begin
    (let ((new-x (+ (xcor-vect vec1) (xcor-vect vec2)))
          (new-y (+ (ycor-vect vec1) (ycor-vect vec2))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-vect-1")
         (test-list
          (list
           (list (make-vect 1 2) (make-vect 3 4) (make-vect 4 6))
           (list (make-vect 3 4) (make-vect 1 2) (make-vect 4 6))
           (list (make-vect 1 2) (make-vect 11 13) (make-vect 12 15))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((v1 (list-ref this-list 0))
                  (v2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (add-vect v1 v2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : v1=~a, v2=~a, "
                        sub-name test-label-index v1 v2))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-vect vec1 vec2)
  (begin
    (let ((new-x (- (xcor-vect vec1) (xcor-vect vec2)))
          (new-y (- (ycor-vect vec1) (ycor-vect vec2))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sub-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-sub-vect-1")
         (test-list
          (list
           (list (make-vect 1 2) (make-vect 3 4) (make-vect -2 -2))
           (list (make-vect 3 4) (make-vect 1 2) (make-vect 2 2))
           (list (make-vect 1 2) (make-vect 11 13) (make-vect -10 -11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((v1 (list-ref this-list 0))
                  (v2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (sub-vect v1 v2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : v1=~a, v2=~a, "
                        sub-name test-label-index v1 v2))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (scale-vect scalar vec1)
  (begin
    (let ((new-x (* scalar (xcor-vect vec1)))
          (new-y (* scalar (ycor-vect vec1))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-scale-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-scale-vect-1")
         (test-list
          (list
           (list 3 (make-vect 1 2) (make-vect 3 6))
           (list 5 (make-vect 3 4) (make-vect 15 20))
           (list -3 (make-vect 1 2) (make-vect -3 -6))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((scalar (list-ref this-list 0))
                  (v1 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (scale-vect scalar v1)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : scalar=~a, v1=~a, "
                        sub-name test-label-index scalar v1))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A two-dimensional vector v running "))
    (display
     (format #f "from the origin to a~%"))
    (display
     (format #f "point can be represented as a pair "))
    (display
     (format #f "consisting of an~%"))
    (display
     (format #f "x-coordinate and a y-coordinate. "))
    (display
     (format #f "Implement a data abstraction~%"))
    (display
     (format #f "for vectors by giving a constructor "))
    (display
     (format #f "make-vect and corresponding~%"))
    (display
     (format #f "selectors xcor-vect and ycor-vect. In "))
    (display
     (format #f "terms of your selectors~%"))
    (display
     (format #f "and constructor, implement procedures "))
    (display
     (format #f "add-vect, sub-vect, and~%"))
    (display
     (format #f "scale-vect that perform the operations "))
    (display
     (format #f "vector addition, vector~%"))
    (display
     (format #f "subtraction, and multiplying a "))
    (display
     (format #f "vector by a scalar:~%"))
    (newline)
    (display
     (format #f "(define (make-vect x y) (cons x y))~%"))
    (display
     (format #f "(define (xcor-vect vec) (car vec))~%"))
    (display
     (format #f "(define (ycor-vect vec) (cdr vec))~%"))
    (newline)
    (display
     (format #f "(define (add-vect vec1 vec2)~%"))
    (display
     (format #f "  (let ((new-x (+ (xcor-vect vec1) "))
    (display
     (format #f "(xcor-vect vec2)))~%"))
    (display
     (format #f "        (new-y (+ (ycor-vect vec1) "))
    (display
     (format #f "(ycor-vect vec2))))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (make-vect new-x new-y)~%"))
    (display
     (format #f "      )))~%"))
    (newline)
    (display
     (format #f "(define (sub-vect vec1 vec2)~%"))
    (display
     (format #f "  (let ((new-x (- (xcor-vect vec1) "))
    (display
     (format #f "(xcor-vect vec2)))~%"))
    (display
     (format #f "        (new-y (- (ycor-vect vec1) "))
    (display
     (format #f "(ycor-vect vec2))))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (make-vect new-x new-y)~%"))
    (display
     (format #f "      )))~%"))
    (newline)
    (display
     (format #f "(define (scale-vect scalar vec1)~%"))
    (display
     (format #f "  (let ((new-x (* scalar "))
    (display
     (format #f "(xcor-vect vec1)))~%"))
    (display
     (format #f "        (new-y (* scalar "))
    (display
     (format #f "(ycor-vect vec1))))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (make-vect new-x new-y)~%"))
    (display
     (format #f "     )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list 2 (make-vect 1 2) (make-vect 3 4))
            (list 3 (make-vect 1 2) (make-vect 5 7))
            (list 4 (make-vect 1 2) (make-vect 11 13))
            )))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((scalar (list-ref alist 0))
                   (vec1 (list-ref alist 1))
                   (vec2 (list-ref alist 2)))
               (begin
                 (display
                  (format
                   #f "vec1=~a, vec2=~a, scalar=~a~%"
                   vec1 vec2 scalar))
                 (display
                  (format
                   #f "    vec1+vec2=~a : vec1-vec2=~a~%"
                   (add-vect vec1 vec2)
                   (sub-vect vec1 vec2)))
                 (display
                  (format
                   #f "    scalar*vec2=~a~%"
                   (scale-vect scalar vec2)))
                 (newline)
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.46 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme tests~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
