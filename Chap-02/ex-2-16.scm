#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.16                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; ensure that interval is in the right order
(define (make-interval x y)
  (begin
    (if (< x y)
        (begin
          (cons x y))
        (begin
          (cons y x)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lower-bound xx)
  (begin
    (car xx)
    ))

;;;#############################################################
;;;#############################################################
(define (upper-bound xx)
  (begin
    (cdr xx)
    ))

;;;#############################################################
;;;#############################################################
(define (make-center-width c w)
  (begin
    (make-interval (- c w) (+ c w))
    ))

;;;#############################################################
;;;#############################################################
(define (center i)
  (begin
    (/ (+ (lower-bound i) (upper-bound i)) 2)
    ))

;;;#############################################################
;;;#############################################################
(define (width xx)
  (begin
    (/ (- (upper-bound xx) (lower-bound xx)) 2.0)
    ))

;;;#############################################################
;;;#############################################################
(define (make-center-percent c pcnt)
  (begin
    (make-center-width c (* c 0.010 pcnt))
    ))

;;;#############################################################
;;;#############################################################
(define (percent i)
  (begin
    (let ((c (center i))
          (w (width i)))
      (begin
        (if (zero? c)
            (begin
              (display
               (format
                #f "percent error: i = ~a, center = ~a, "
                i c))
              (display
               (format
                #f "divide by zero!~%"))
              (display
               (format #f "quitting...~%"))
              (force-output)
              (quit))
            (begin
              (let ((pcnt (* 100.0 (abs (/ w c)))))
                (begin
                  pcnt
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-percent-1 result-hash-table)
 (begin
   (let ((sub-name "test-percent-1")
         (test-list
          (list
           (list (make-center-percent 5 10) 10.0)
           (list (make-center-percent -5 15) 15.0)
           (list (make-center-percent 20 5) 5.0)
           (list (make-center-percent -20 10) 10.0)
           (list (make-center-percent 5 20) 20.0)
           ))
         (tol 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ii (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (percent ii)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : ii=~a, "
                        sub-name test-label-index ii))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (add-interval x y)
  (begin
    (make-interval
     (+ (lower-bound x) (lower-bound y))
     (+ (upper-bound x) (upper-bound y)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-interval-1")
         (test-list
          (list
           (list (make-interval 1 10) (make-interval 11 20)
                 (make-interval 12 30))
           (list (make-interval 10 30) (make-interval 20 20)
                 (make-interval 30 50))
           (list (make-interval -10 10) (make-interval -20 100)
                 (make-interval -30 110))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (add-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-interval x y)
  (begin
    (make-interval
     (- (lower-bound x) (lower-bound y))
     (- (upper-bound x) (upper-bound y)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sub-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-sub-interval-1")
         (test-list
          (list
           (list (make-interval 1 10) (make-interval 11 20)
                 (make-interval -10 -10))
           (list (make-interval 10 30) (make-interval 20 25)
                 (make-interval -10 5))
           (list (make-interval -10 10) (make-interval -20 100)
                 (make-interval 10 -90))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (sub-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sign xx)
  (begin
    (if (>= xx 0)
        (begin
          1)
        (begin
          -1
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (mul-interval x y)
  (begin
    (let ((lx (lower-bound x))
          (ly (lower-bound y))
          (ux (upper-bound x))
          (uy (upper-bound y)))
      (let ((sgn-lx (sign lx))
            (sgn-ly (sign ly))
            (sgn-ux (sign ux))
            (sgn-uy (sign uy)))
        (begin
          (cond
           ((and (< sgn-lx 0) (< sgn-ux 0)
                 (< sgn-ly 0) (< sgn-uy 0))
            (begin
              ;;; case 1
              (let ((p1 (* lx ly))
                    (p2 (* ux uy)))
                (begin
                  (make-interval p1 p2)
                  ))
              ))
           ((and (< sgn-lx 0) (> sgn-ux 0)
                 (< sgn-ly 0) (< sgn-uy 0))
            (begin
              ;;; case 2
              (let ((p1 (* ux ly))
                    (p2 (* lx ly)))
                (begin
                  (make-interval p1 p2)
                  ))
              ))
           ((and (> sgn-lx 0) (> sgn-ux 0)
                 (< sgn-ly 0) (< sgn-uy 0))
            (begin
              ;;; case 3
              (let ((p1 (* ux ly))
                    (p2 (* lx uy)))
                (begin
                  (make-interval p1 p2)
                  ))
              ))
           ((and (< sgn-lx 0) (< sgn-ux 0)
                 (< sgn-ly 0) (> sgn-uy 0))
            (begin
              ;;; case 4
              (let ((p1 (* lx uy))
                    (p2 (* lx ly)))
                (begin
                  (make-interval p1 p2)
                  ))
              ))
           ((and (< sgn-lx 0) (> sgn-ux 0)
                 (< sgn-ly 0) (> sgn-uy 0))
            (begin
              ;;; case 5
              (let ((p1 (* ux ly))
                    (p2 (* lx uy))
                    (p3 (* lx ly))
                    (p4 (* ux uy)))
                (begin
                  (make-interval (min p1 p2) (max p3 p4))
                  ))
              ))
           ((and (> sgn-lx 0) (> sgn-ux 0)
                 (< sgn-ly 0) (> sgn-uy 0))
            (begin
              ;;; case 6
              (let ((p1 (* ux ly))
                    (p2 (* ux uy)))
                (begin
                  (make-interval p1 p2)
                  ))
              ))
           ((and (< sgn-lx 0) (< sgn-ux 0)
                 (> sgn-ly 0) (> sgn-uy 0))
            (begin
              ;;; case 7
              (let ((p1 (* lx uy))
                    (p2 (* ux ly)))
                (begin
                  (make-interval p1 p2)
                  ))
              ))
           ((and (< sgn-lx 0) (> sgn-ux 0)
                 (> sgn-ly 0) (> sgn-uy 0))
            (begin
              ;;; case 8
              (let ((p1 (* lx uy))
                    (p2 (* ux uy)))
                (begin
                  (make-interval p1 p2)
                  ))
              ))
           ((and (> sgn-lx 0) (> sgn-ux 0)
                 (> sgn-ly 0) (> sgn-uy 0))
            (begin
              ;;; case 9
              (let ((p1 (* lx ly))
                    (p2 (* ux uy)))
                (begin
                  (make-interval p1 p2)
                  ))
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mul-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-mul-interval-1")
         (test-list
          (list
           (list (make-interval 1 3) (make-interval 2 4)
                 (make-interval 2 12))
           (list (make-interval 10 30) (make-interval 20 20)
                 (make-interval 200 600))
           (list (make-interval -10 10) (make-interval 2 -3)
                 (make-interval -30 30))
            ;;; case 1
           (list (make-interval -4 -2) (make-interval -3 -1)
                 (make-interval 12 2))
            ;;; case 2
           (list (make-interval -4 2) (make-interval -3 -1)
                 (make-interval -6 12))
            ;;; case 3
           (list (make-interval 2 5) (make-interval -3 -1)
                 (make-interval -15 -2))
            ;;; case 4
           (list (make-interval -5 -2) (make-interval -3 1)
                 (make-interval -5 15))
            ;;; case 5
           (list (make-interval -2 7) (make-interval -3 5)
                 (make-interval -21 35))
           (list (make-interval -7 2) (make-interval -3 5)
                 (make-interval -35 21))
            ;;; case 6
           (list (make-interval 2 5) (make-interval -3 1)
                 (make-interval -15 5))
            ;;; case 7
           (list (make-interval -5 -2) (make-interval 3 7)
                 (make-interval -35 -6))
           (list (make-interval -5 -1) (make-interval 3 7)
                 (make-interval -35 -3))
            ;;; case 8
           (list (make-interval -2 5) (make-interval 3 7)
                 (make-interval -14 35))
            ;;; case 9
           (list (make-interval 2 5) (make-interval 3 7)
                 (make-interval 6 35))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (mul-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (div-interval x y)
  (begin
    (let ((y-w (width y)))
      (begin
        (if (> y-w 0.0)
            (begin
              (mul-interval
               x
               (make-interval (/ 1.0 (lower-bound y))
                              (/ 1.0 (upper-bound y)))))
            (begin
              (display
               (format
                #f "div-interval error: dividing by zero "))
              (display
               (format
                #f "width interval ~a~%" y))
              (display
               (format #f "quitting...~%"))
              (force-output)
              (quit)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-div-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-div-interval-1")
         (test-list
          (list
           (list (make-interval 1 3) (make-interval 2 4)
                 (make-interval 0.25 1.5))
           (list (make-interval 10 30) (make-interval 20 40)
                 (make-interval 0.25 1.5))
           (list (make-interval -10 10) (make-interval 2 -3)
                 (make-interval -5.0 5.0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (div-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (par1 r1 r2)
  (begin
    (div-interval
     (mul-interval r1 r2)
     (add-interval r1 r2))
    ))

;;;#############################################################
;;;#############################################################
(define (par2 r1 r2)
  (begin
    (let ((one (make-interval 1 1)))
      (begin
        (div-interval
         one
         (add-interval
          (div-interval one r1)
          (div-interval one r2)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (interval-to-string x)
  (begin
    (format #f "(~a, ~a)" (lower-bound x) (upper-bound x))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Explain, in general, why equivalent "))
    (display
     (format #f "algebraic expressions may~%"))
    (display
     (format #f "lead to different answers. Can you "))
    (display
     (format #f "devise an~%"))
    (display
     (format #f "interval-arithmetic package that does "))
    (display
     (format #f "not have this~%"))
    (display
     (format #f "shortcoming, or is this task "))
    (display
     (format #f "impossible? (Warning: This~%"))
    (display
     (format #f "problem is very difficult.)~%"))
    (newline)

    (display
     (format #f "In general, equivalent algebraic "))
    (display
     (format #f "expressions may give~%"))
    (display
     (format #f "different answers because of the "))
    (display
     (format #f "algorithm for multiplying~%"))
    (display
     (format #f "and dividing uncertain intervals. It "))
    (display
     (format #f "impossible to compute~%"))
    (display
     (format #f "identical intervals for equivalent "))
    (display
     (format #f "expressions because of~%"))
    (display
     (format #f "the multiplication algorithm. Consider "))
    (display
     (format #f "the case where~%"))
    (display
     (format #f "both the upper and lower bounds for "))
    (display
     (format #f "x and y are~%"))
    (display
     (format #f "positive. Then when multiplying the "))
    (display
     (format #f "two together gives~%"))
    (display
     (format #f "an increased factor of "))
    (display
     (format #f "(1-pcnt-x)*(1-pcnt-y) for the~%"))
    (display
     (format #f "lower bound, and "))
    (display
     (format #f "(1+pcnt-x)*(1+pcnt-y) for the~%"))
    (display
     (format #f "upper bound.  This is an asymmetry "))
    (display
     (format #f "since the lower~%"))
    (display
     (format #f "bound decreases by 1 - pcnt-x - pcnt-y "))
    (display
     (format #f "+ pcnt-x * pcnt-y~%"))
    (display
     (format #f "and the upper bound increases by "))
    (display
     (format #f "1 + pcnt-x + pcnt-y +~%"))
    (display
     (format #f "pcnt-x * pcnt-y.  The upper and lower "))
    (display
     (format #f "bound would increase~%"))
    (display
     (format #f "by the same amount if the "))
    (display
     (format #f "(pcnt-x * pcnt-y) term~%"))
    (display
     (format #f "not there. This is the cause of the "))
    (display
     (format #f "shortcoming.~%"))
    (newline)
    (display
     (format #f "A potential solution would be to "))
    (display
     (format #f "use the center/width as~%"))
    (display
     (format #f "the fundamental data structure, "))
    (display
     (format #f "thereby allowing the~%"))
    (display
     (format #f "users to see that the center "))
    (display
     (format #f "calculated from one~%"))
    (display
     (format #f "expression is more or less equivalent "))
    (display
     (format #f "to another expression,~%"))
    (display
     (format #f "the only difference being captured "))
    (display
     (format #f "by the width~%"))
    (display
     (format #f "of the interval.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.16 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
