#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.51                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-to-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (below bottom-painter top-painter)
  (begin
    (let ((split-point (make-vect 0.0 0.5)))
      (let ((bottom-p
             (transform-painter
              bottom-painter
              (make-vect 0.0 0.0)
              (make-vect 1.0 0.0)
              split-point))
            (top-p
             (transform-painter
              top-painter
              split-point
              (make-vect 1.0 0.0)
              (make-vect 0.0 1.0))))
        (begin
          (lambda (frame)
            (begin
              (bottom-p frame)
              (top-p frame)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (below bottom-painter top-painter)
  (begin
    (rotate90
     (beside bottom-painter top-painter))
    ))

;;;#############################################################
;;;#############################################################
(define (rotate90 painter)
  (begin
    (transform-painter
     painter
     (make-vect 1.0 1.0)
     (make-vect 0.0 1.0)
     (make-vect 1.0 0.0))
    ))

;;;#############################################################
;;;#############################################################
(define (rotate270 painter)
  (begin
    (transform-painter
     painter
     (make-vect 0.0 1.0)
     (make-vect 0.0 0.0)
     (make-vect 1.0 1.0))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define the below operation for "))
    (display
     (format #f "painters. Below takes two~%"))
    (display
     (format #f "painters as arguments. The resulting "))
    (display
     (format #f "painter, given a frame,~%"))
    (display
     (format #f "draws with the first painter in the "))
    (display
     (format #f "bottom of the frame~%"))
    (display
     (format #f "and with the second painter in the top. "))
    (display
     (format #f "Define below in~%"))
    (display
     (format #f "two different ways -- first by writing "))
    (display
     (format #f "a procedure that is~%"))
    (display
     (format #f "analogous to the beside procedure "))
    (display
     (format #f "given above, and again~%"))
    (display
     (format #f "in terms of beside and suitable "))
    (display
     (format #f "rotation operations~%"))
    (display
     (format #f "(from exercise 2.50).~%"))
    (newline)
    (display
     (format #f "(define (below bottom-painter "))
    (display
     (format #f "top-painter)~%"))
    (display
     (format #f "  (let ((split-point "))
    (display
     (format #f "(make-vect 0.0 0.5)))~%"))
    (display
     (format #f "    (let ((bottom-p~%"))
    (display
     (format #f "           (transform-painter~%"))
    (display
     (format #f "            bottom-painter~%"))
    (display
     (format #f "            (make-vect 0.0 0.0)~%"))
    (display
     (format #f "            (make-vect 1.0 0.0)~%"))
    (display
     (format #f "            split-point))~%"))
    (display
     (format #f "          (top-p~%"))
    (display
     (format #f "           (transform-painter~%"))
    (display
     (format #f "            top-painter~%"))
    (display
     (format #f "            split-point~%"))
    (display
     (format #f "            (make-vect 1.0 0.0)~%"))
    (display
     (format #f "            (make-vect 0.0 1.0)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (lambda (frame)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (bottom-p frame)~%"))
    (display
     (format #f "            (top-p frame)~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "(define (below "))
    (display
     (format #f "bottom-painter top-painter)~%"))
    (display
     (format #f "  (rotate90~%"))
    (display
     (format #f "   (beside bottom-painter "))
    (display
     (format #f "top-painter)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.51 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
