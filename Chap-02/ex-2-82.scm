#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.82                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
        (begin
          (if proc
              (begin
                (apply proc (map contents args)))
              (begin
                (coercion-routine type-tags args op)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (coercion-routine type-tags args op)
  (define (change-to-same-type goal-type type-tag-list)
    (begin
      (let ((success-flag #t))
        (begin
          (for-each
           (lambda (atag)
             (begin
               (if (and (equal? success-flag #t)
                        (not (eq? goal-type atag)))
                   (begin
                     (let ((ti->tj (get-coercion atag goal-type)))
                       (begin
                         (if (not ti->tj)
                             (begin
                               (set! success-flag #f)
                               ))
                         ))
                     ))
               )) type-tag-list)

          success-flag
          ))
      ))
  (define (find-valid-coercion-type type-tag-list)
    (begin
      (let ((tlen (length type-tag-list))
            (final-type #f)
            (found-flag #f))
        (begin
          (do ((ii 0 (1+ ii)))
              ((or (>= ii tlen)
                   (equal? found-flag #t)))
            (begin
              (let ((goal-type (list-ref type-tag-list ii)))
                (let ((tok-flag
                       (change-to-same-type goal-type type-tag-list)))
                  (begin
                    (if (equal? tok-flag #t)
                        (begin
                          (set! found-flag #t)
                          (set! final-type goal-type)
                          ))
                    )))
              ))

          final-type
          ))
      ))
  (define (convert-tag-list goal-type args)
    (begin
      (let ((next-arg-list
             (map
              (lambda (an-arg)
                (begin
                  ((get-coercion (type-tag an-arg) goal-type) an-arg)
                  )) args)))
        (begin
          next-arg-list
          ))
      ))
  (begin
    (if (>= (length args) 2)
        (begin
          (let ((ftype (find-valid-coercion-type type-tags)))
            (begin
              (if (not (equal? ftype #f))
                  (begin
                    (let ((next-arg-list
                           (convert-tag-list ftype args)))
                      (begin
                        (apply-generic op next-arg-list)
                        )))
                  (begin
                    (error "No method for these types"
                           (list op type-tags))
                    ))
              )))
        (begin
          (error "No method for these types"
                 (list op type-tags))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Show how to generalize apply-generic "))
    (display
     (format #f "to handle coercion~%"))
    (display
     (format #f "in the general case of multiple "))
    (display
     (format #f "arguments. One strategy~%"))
    (display
     (format #f "is to attempt to coerce all the "))
    (display
     (format #f "arguments to the type~%"))
    (display
     (format #f "of the first argument, then to the "))
    (display
     (format #f "type of the second~%"))
    (display
     (format #f "argument, and so on. Give an example "))
    (display
     (format #f "of a situation~%"))
    (display
     (format #f "where this strategy (and likewise "))
    (display
     (format #f "the two-argument~%"))
    (display
     (format #f "version given above) is not "))
    (display
     (format #f "sufficiently general.~%"))
    (display
     (format #f "(Hint: Consider the case where there "))
    (display
     (format #f "are some suitable~%"))
    (display
     (format #f "mixed-type operations present in the "))
    (display
     (format #f "table that will not~%"))
    (display
     (format #f "be tried.)~%"))
    (newline)
    (display
     (format #f "Here's a strategy to coerce all "))
    (display
     (format #f "arguments to a type~%"))
    (display
     (format #f "of a single argument:~%"))
    (display
     (format #f "(define (apply-generic op . args)~%"))
    (display
     (format #f "  (let ((type-tags (map "))
    (display
     (format #f "type-tag args)))~%"))
    (display
     (format #f "    (let ((proc (get op type-tags)))~%"))
    (display
     (format #f "      (if proc~%"))
    (display
     (format #f "          (apply proc (map "))
    (display
     (format #f "contents args))~%"))
    (display
     (format #f "          (coercion-routine "))
    (display
     (format #f "type-tags args op)~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (coercion-routine "))
    (display
     (format #f "type-tags args op)~%"))
    (display
     (format #f "  (define (change-to-same-type "))
    (display
     (format #f "goal-type type-tag-list)~%"))
    (display
     (format #f "    (let ((success-flag #t))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (for-each~%"))
    (display
     (format #f "         (lambda (atag)~%"))
    (display
     (format #f "           (begin~%"))
    (display
     (format #f "             (if (and (equal? "))
    (display
     (format #f "success-flag #t)~%"))
    (display
     (format #f "                      (not (eq? "))
    (display
     (format #f "goal-type atag)))~%"))
    (display
     (format #f "                 (begin~%"))
    (display
     (format #f "                   (let ((ti->tj "))
    (display
     (format #f "(get-coercion atag goal-type)))~%"))
    (display
     (format #f "                     (begin~%"))
    (display
     (format #f "                       (if (not "))
    (display
     (format #f "ti->tj)~%"))
    (display
     (format #f "                           (begin~%"))
    (display
     (format #f "                             (set! "))
    (display
     (format #f "success-flag #f)~%"))
    (display
     (format #f "                             ))~%"))
    (display
     (format #f "                       ))~%"))
    (display
     (format #f "                   ))~%"))
    (display
     (format #f "             )) type-tag-list)~%"))
    (display
     (format #f "        success-flag~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (define (find-valid-coercion-type "))
    (display
     (format #f "type-tag-list)~%"))
    (display
     (format #f "    (let ((tlen (length type-tag-list))~%"))
    (display
     (format #f "          (final-type #f)~%"))
    (display
     (format #f "          (found-flag #f))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "            ((or (>= ii tlen)~%"))
    (display
     (format #f "                 (equal? "))
    (display
     (format #f "found-flag #t)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((goal-type "))
    (display
     (format #f "(list-ref type-tag-list ii)))~%"))
    (display
     (format #f "              (let ((tok-flag~%"))
    (display
     (format #f "                     (change-to-same-type "))
    (display
     (format #f "goal-type type-tag-list)))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (if (equal? "))
    (display
     (format #f "tok-flag #t)~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (set! "))
    (display
     (format #f "found-flag #t)~%"))
    (display
     (format #f "                        (set! "))
    (display
     (format #f "final-type goal-type)~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                  )))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        final-type~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (define (convert-tag-list "))
    (display
     (format #f "goal-type args)~%"))
    (display
     (format #f "    (let ((next-arg-list~%"))
    (display
     (format #f "           (map~%"))
    (display
     (format #f "            (lambda (an-arg)~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                ((get-coercion "))
    (display
     (format #f "(type-tag an-arg) goal-type) an-arg)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        next-arg-list~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (if (>= (length args) 2)~%"))
    (display
     (format #f "      (let ((ftype "))
    (display
     (format #f "(find-valid-coercion-type type-tags)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (if (not (equal? "))
    (display
     (format #f "ftype #f))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (let ((next-arg-list~%"))
    (display
     (format #f "                       (convert-tag-list "))
    (display
     (format #f "ftype args)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (apply-generic "))
    (display
     (format #f "op next-arg-list)~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (error \"No method "))
    (display
     (format #f "for these types\"~%"))
    (display
     (format #f "                       (list op "))
    (display
     (format #f "type-tags))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (error \"No method "))
    (display
     (format #f "for these types\"~%"))
    (display
     (format #f "               (list op "))
    (display
     (format #f "type-tags))~%"))
    (display
     (format #f "        )))~%"))
    (newline)
    (display
     (format #f "This system does not work if the "))
    (display
     (format #f "gaps are too big~%"))
    (display
     (format #f "in the tower of types, and if not all "))
    (display
     (format #f "of the conversions~%"))
    (display
     (format #f "are provided. If the list of arguments "))
    (display
     (format #f "are integer,~%"))
    (display
     (format #f "complex and real, then since there "))
    (display
     (format #f "is no integer->complex~%"))
    (display
     (format #f "or integer->real coercion function, "))
    (display
     (format #f "this method would fail.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.82 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
