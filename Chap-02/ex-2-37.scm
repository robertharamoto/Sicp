#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.37                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (accumulate-n op init seqs)
  (begin
    (if (or (null? seqs) (null? (car seqs)))
        (begin
          (list))
        (begin
          (cons
           (accumulate op init (map car seqs))
           (accumulate-n op init (map cdr seqs)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (dot-product v w)
  (begin
    (accumulate + 0 (map * v w))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dot-product-1 result-hash-table)
 (begin
   (let ((sub-name "test-dot-product-1")
         (test-list
          (list
           (list (list 1 2 3) (list 4 5 6) 32)
           (list (list 3 2 1) (list 4 5 6) 28)
           ))
         (tol 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu (list-ref this-list 0))
                  (vv (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (dot-product uu vv)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : uu=~a, vv=~a, "
                        sub-name test-label-index
                        uu vv))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- shouldbe result)) tol)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (matrix-*-vector m v)
  (begin
    (map
     (lambda (mat-vec)
       (begin
         (dot-product mat-vec v)
         )) m)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-*-vector-1 result-hash-table)
 (begin
   (let ((sub-name "test-matrix-*-vector-1")
         (test-list
          (list
           (list (list (list 1 2 3) (list 3 2 1) (list -1 0 1))
                 (list 2 2 2) (list 12 12 0))
           (list (list (list 1 2 3) (list 3 2 1) (list -1 0 1))
                 (list 1 1 -1) (list 0 4 -2))
           ))
         (tol 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mm (list-ref this-list 0))
                  (vv (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (matrix-*-vector mm vv)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : mm=~a, vv=~a, "
                        sub-name test-label-index
                        mm vv))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (transpose m)
  (begin
    (accumulate-n
     (lambda (x y) (cons x y)) (list) m)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-transpose-1 result-hash-table)
 (begin
   (let ((sub-name "test-transpose-1")
         (test-list
          (list
           (list (list (list 1 2) (list 3 4))
                 (list (list 1 3) (list 2 4)))
           (list (list (list 1 2 3) (list 3 2 1) (list -1 0 1))
                 (list (list 1 3 -1) (list 2 2 0) (list 3 1 1)))
           ))
         (tol 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mm (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (transpose mm)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : mm=~a, "
                        sub-name test-label-index
                        mm))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (matrix-*-matrix m n)
  (begin
    (let ((cols (transpose n)))
      (begin
        (map (lambda (m-vec) (matrix-*-vector cols m-vec)) m)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-*-matrix-1 result-hash-table)
 (begin
   (let ((sub-name "test-matrix-*-matrix-1")
         (test-list
          (list
           (list (list (list 1 2 3) (list 3 2 1) (list 4 5 6))
                 (list (list 1 0 0) (list 0 1 0) (list 0 0 1))
                 (list (list 1 2 3) (list 3 2 1) (list 4 5 6)))
           (list (list (list 1 0 0) (list 0 1 0) (list 0 0 1))
                 (list (list 1 2 3) (list 3 2 1) (list 4 5 6))
                 (list (list 1 2 3) (list 3 2 1) (list 4 5 6)))
           (list (list (list 1 2 3) (list 3 2 1) (list 4 5 6))
                 (list (list 0 1 0) (list 1 0 0) (list 0 0 1))
                 (list (list 2 1 3) (list 2 3 1) (list 5 4 6)))
           (list (list (list 0 0 1) (list 0 1 0) (list 1 0 0))
                 (list (list 1 2 3) (list 3 2 1) (list 4 5 6))
                 (list (list 4 5 6) (list 3 2 1) (list 1 2 3)))
           ))
         (tol 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mm (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (matrix-*-matrix mm nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : mm=~a, nn=~a, "
                        sub-name test-label-index
                        mm nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose we represent vectors v = (vi) "))
    (display
     (format #f "as sequences of~%"))
    (display
     (format #f "numbers, and matrices m = (mij) as "))
    (display
     (format #f "sequences of vectors~%"))
    (display
     (format #f "(the rows of the matrix). For example, "))
    (display
     (format #f "the matrix~%"))
    (newline)
    (display
     (format #f "[ 1 2 3 4 ]~%"))
    (display
     (format #f "[ 4 5 6 6 ]~%"))
    (display
     (format #f "[ 6 7 8 9 ]~%"))
    (newline)
    (display
     (format #f "is represented as the sequence~%"))
    (display
     (format #f "((1 2 3 4) (4 5 6 6) (6 7 8 9)).~%"))
    (display
     (format #f "With this representation, we can use "))
    (display
     (format #f "sequence operations~%"))
    (display
     (format #f "to concisely express the basic matrix "))
    (display
     (format #f "and vector operations.~%"))
    (display
     (format #f "These operations (which are described "))
    (display
     (format #f "in any book on~%"))
    (display
     (format #f "matrix algebra) are the following:~%"))
    (display
     (format #f "(dot-product v w)  "))
    (display
     (format #f "returns sum_i (v_i * w_i)~%"))
    (display
     (format #f "(matrix-*-vector m v)  "))
    (display
     (format #f "returns the vector t~%"))
    (display
     (format #f "where t_i = sum_j (m_ij * v_j)~%"))
    (display
     (format #f "(matrix-*-matrix m n)   returns "))
    (display
     (format #f "the matrix p where~%"))
    (display
     (format #f "p_ij = sum_k (m_ik * n_kj)~%"))
    (display
     (format #f "(transpose m)  returns the "))
    (display
     (format #f "matrix n where~%"))
    (display
     (format #f "n_ij = m_ji~%"))
    (display
     (format #f "We can define the dot product as~%"))
    (display
     (format #f "(define (dot-product v w)~%"))
    (display
     (format #f "  (accumulate + 0 (map * v w)))~%"))
    (display
     (format #f "Fill in the missing expressions "))
    (display
     (format #f "in the following~%"))
    (display
     (format #f "procedures for computing the other "))
    (display
     (format #f "matrix operations.~%"))
    (display
     (format #f "(The procedure accumulate-n "))
    (display
     (format #f "is defined in~%"))
    (display
     (format #f "exercise 2.36.)~%"))
    (display
     (format #f "(define (matrix-*-vector m v) "))
    (display
     (format #f "(map <??> m))~%"))
    (display
     (format #f "(define (transpose mat) "))
    (display
     (format #f "(accumulate-n <??> <??> mat))~%"))
    (display
     (format #f "(define (matrix-*-matrix m n)~%"))
    (display
     (format #f "  (let ((cols (transpose n)))~%"))
    (display
     (format #f "    (map <??> m)))~%"))
    (newline)
    (display
     (format #f "(define (matrix-*-vector m v)~%"))
    (display
     (format #f "  (map (lambda (mat-vec) "))
    (display
     (format #f "(accumulate + 0 mat-vec v)) m))~%"))
    (newline)
    (display
     (format #f "(define (transpose m)~%"))
    (display
     (format #f "  (accumulate-n~%"))
    (display
     (format #f "   (lambda (x y) "))
    (display
     (format #f "(cons x y)) (list) m))~%"))
    (newline)
    (display
     (format #f "(define (matrix-*-matrix m n)~%"))
    (display
     (format #f "  (let ((cols (transpose n)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (map (lambda (m-vec) "))
    (display
     (format #f "(matrix-*-vector cols m-vec)) m)~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((mm (list (list 1 2 3) (list 3 2 1) (list -1 2 4)))
          (nn (list (list 0 1 0) (list 1 0 0) (list 0 0 1)))
          (ii (list (list 1 0 0) (list 0 1 0) (list 0 0 1)))
          (vv (list 1 1 1)))
      (begin
        (display
         (format
          #f "vv=~a, vv=~a, (dot-product vv vv) = ~a~%"
          vv vv (dot-product vv vv)))
        (display
         (format
          #f "mm=~a, vv=~a, (matrix-*-vector mm vv) = ~a~%"
          mm vv (matrix-*-vector mm vv)))
        (display
         (format
          #f "mm=~a (transpose mm) = ~a~%"
          mm (transpose mm)))
        (display
         (format #f "mm=~a, ii=~a,~%"
                 mm ii))
        (display
         (format #f "(matrix-*-matrix mm ii) = ~a "
                 (matrix-*-matrix mm ii)))
        (display
         (format #f " (identity matrix)~%"))
        (display
         (format #f "mm=~a, nn=~a,~%" mm nn))
        (display
         (format #f "(matrix-*-matrix mm nn) = ~a~%"
                 (matrix-*-matrix mm nn)))
        (display
         (format #f "  (exchange first two columns)~%"))
        (display
         (format #f "nn=~a, mm=~a, ~%"
                 nn mm))
        (display
         (format #f "(matrix-*-matrix nn mm) = ~a~%"
                 (matrix-*-matrix nn mm)))
        (display
         (format #f "  (exchange first two rows)~%"))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.37 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
