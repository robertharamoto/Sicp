#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.80                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (=zero? num-1)
  (begin
    (cond
     ((number? num-1)
      (begin
        (= num-1 0)
        ))
     ((eq? (type-tag num-1) 'rational)
      (begin
        (let ((contents-1 (contents num-1)))
          (let ((numer-1 (car contents-1))
                (denom-1 (cdr contents-1)))
            (begin
              (= numer-1 0)
              )))
        ))
     ((eq? (type-tag num-1) 'complex)
      (begin
        (let ((contents-1 (contents num-1)))
          (let ((tag-1 (car contents-1))
                (rest-1 (cdr contents-1)))
            (let ((real-1 (car rest-1))
                  (imag-1 (cdr rest-1)))
              (begin
                (if (eq? tag-1 'rectangular)
                    (begin
                      (and (= real-1 0) (= imag-1 0)))
                    (begin
                      (= real-1 0)
                      ))
                ))
            ))
        ))
     (else
      (begin
        (display
         (format
          #f "=zero? error type ~a not found" t1))
        (force-output)
        (quit)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a generic predicate =zero? "))
    (display
     (format #f "that tests if its~%"))
    (display
     (format #f "argument is zero, and install it in "))
    (display
     (format #f "the generic arithmetic~%"))
    (display
     (format #f "package. This operation should work "))
    (display
     (format #f "for ordinary numbers,~%"))
    (display
     (format #f "rational numbers, and complex numbers.~%"))
    (newline)
    (display
     (format #f "(define (install-scheme-number-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (=zero? x)~%"))
    (display
     (format #f "    (= x 0))~%"))
    (display
     (format #f "  (put '=zero? '(scheme-number) =zero?)~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-rational-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (=zero? x)~%"))
    (display
     (format #f "    (= (numer x) 0))~%"))
    (display
     (format #f "  (put '=zero? '(rational) =zero?)~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-complex-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (=zero? x)~%"))
    (display
     (format #f "    (and (= (real-part x) 0)~%"))
    (display
     (format #f "         (= (imag-part x) 0)))~%"))
    (display
     (format #f "  (put '=zero? '(complex) =zero?)~%"))
    (display
     (format #f "  ...)~%"))
    (newline)
    (display
     (format #f "(define (=zero? anum)~%"))
    (display
     (format #f "  ((get '=zero? (type-tag anum)) anum))~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.80 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
