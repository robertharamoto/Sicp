#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.52                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-to-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (wave-painter frame)
  (begin
    (let ((origin (get-frame-origin frame))
          (edge1 (get-frame-edge1 frame))
          (edge2 (get-frame-edge2 frame)))
      (let ((pt-01 (scale-vect 0.60 edge2))
            (pt-02 (add-vect
                    (scale-vect 0.150 edge1)
                    (scale-vect 0.45 edge2)))
            (pt-03 (add-vect
                    (scale-vect 0.250 edge1)
                    (scale-vect 0.55 edge2)))
            (pt-04 (add-vect
                    (scale-vect 0.280 edge1)
                    (scale-vect 0.52 edge2)))
            (pt-05 (scale-vect 0.250 edge1)))
        (let ((line-list-1
               (list (make-segment pt-01 pt-02)
                     (make-segment pt-02 pt-03)
                     (make-segment pt-03 pt-04)
                     (make-segment pt-04 pt-05)))
              (pt-11 (scale-vect 0.35 edge1))
              (pt-12 (add-vect (scale-vect 0.50 edge1)
                               (scale-vect 0.30 edge2)))
              (pt-13 (scale-vect 0.65 edge1)))
          (let ((line-list-2
                 (list (make-segment pt-11 pt-12)
                       (make-segment pt-12 pt-13)))
                (pt-21 (scale-vect 0.75 edge1))
                (pt-22 (add-vect
                        (scale-vect 0.67 edge1)
                        (scale-vect 0.47 edge2)))
                (pt-23 (add-vect
                        edge1
                        (scale-vect 0.35 edge2))))
            (let ((line-list-3
                   (list (make-segment pt-21 pt-22)
                         (make-segment pt-22 pt-23)))
                  (pt-31
                   (add-vect
                    pt-23 (scale-vect 0.10 edge2)))
                  (pt-32
                   (add-vect
                    (scale-vect 0.75 edge1)
                    (scale-vect 0.65 edge2)))
                  (pt-33
                   (add-vect (scale-vect 0.67 edge1)
                             (scale-vect 0.65 edge2)))
                  (pt-34
                   (add-vect (scale-vect 0.70 edge1)
                             (scale-vect 0.80 edge2)))
                  (pt-35
                   (add-vect
                    (scale-vect 0.65 edge1) edge2)))
              (let ((line-list-4
                     (list (make-segment pt-31 pt-32)
                           (make-segment pt-32 pt-33)
                           (make-segment pt-33 pt-34)
                           (make-segment pt-34 pt-35)))
                    (pt-41
                     (add-vect (scale-vect 0.30 edge1) edge2))
                    (pt-42
                     (add-vect (scale-vect 0.27 edge1)
                               (scale-vect 0.80 edge2)))
                    (pt-43
                     (add-vect (scale-vect 0.30 edge1)
                               (scale-vect 0.65 edge2)))
                    (pt-44
                     (add-vect (scale-vect 0.25 edge1)
                               (scale-vect 0.65 edge2)))
                    (pt-45
                     (add-vect (scale-vect 0.15 edge1)
                               (scale-vect 0.57 edge2)))
                    (pt-46 (scale-vect 0.75 edge2)))
                (let ((line-list-5
                       (list (make-segment pt-41 pt-42)
                             (make-segment pt-42 pt-43)
                             (make-segment pt-43 pt-44)
                             (make-segment pt-44 pt-45)
                             (make-segment pt-45 pt-46)))
                      (pt-51
                       (add-vect (scale-vect 0.32 edge1)
                                 (scale-vect 0.70 edge2)))
                      (pt-52
                       (add-vect (scale-vect 0.33 edge1)
                                 (scale-vect 0.69 edge2)))
                      (pt-53
                       (add-vect (scale-vect 0.62 edge1)
                                 (scale-vect 0.69 edge2)))
                      (pt-54
                       (add-vect (scale-vect 0.63 edge1)
                                 (scale-vect 0.70 edge2))))
                  (let ((line-list-6
                         (list (make-segment pt-51 pt-52)
                               (make-segment pt-52 pt-53)
                               (make-segment pt-53 pt-54))))
                    (let ((sp-func-1
                           (segments->painter line-list-1))
                          (sp-func-2
                           (segments->painter line-list-2))
                          (sp-func-3
                           (segments->painter line-list-3))
                          (sp-func-4
                           (segments->painter line-list-4))
                          (sp-func-5
                           (segments->painter line-list-5))
                          (sp-func-6
                           (segments->painter line-list-6)))
                      (begin
                        (lambda (frame)
                          (begin
                            (sp-func-1 (frame))
                            (sp-func-2 (frame))
                            (sp-func-3 (frame))
                            (sp-func-4 (frame))
                            (sp-func-5 (frame))
                            (sp-func-6 (frame))
                            ))
                        ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (corner-split painter n)
  (begin
    (if (= n 0)
        (begin
          painter)
        (begin
          (let ((up (up-split painter (- n 1)))
                (right (right-split painter (- n 1))))
            (let ((corner (corner-split painter (- n 1))))
              (begin
                (beside (below painter up)
                        (below right corner))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (square-limit painter n)
  (begin
    (let ((combine4
           (square-of-four
            identity flip-vert identity flip-vert)))
      (begin
        (combine4 (corner-split painter n))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Make changes to the square limit of "))
    (display
     (format #f "wave shown in figure 2.9~%"))
    (display
     (format #f "by working at each of the levels "))
    (display
     (format #f "described above. In~%"))
    (display
     (format #f "particular:~%"))
    (newline)
    (display
     (format #f "a.  Add some segments to the primitive "))
    (display
     (format #f "wave painter of~%"))
    (display
     (format #f "exercise 2.49 (to add a smile, for "))
    (display
     (format #f "example).~%"))
    (display
     (format #f "b.  Change the pattern constructed by "))
    (display
     (format #f "corner-split (for~%"))
    (display
     (format #f "example, by using only one copy of the "))
    (display
     (format #f "up-split and~%"))
    (display
     (format #f "right-split images instead of two).~%"))
    (display
     (format #f "c.  Modify the version of square-limit "))
    (display
     (format #f "that uses~%"))
    (display
     (format #f "square-of-four so as to assemble the "))
    (display
     (format #f "corners in a~%"))
    (display
     (format #f "different pattern. (For example, you "))
    (display
     (format #f "might make the big~%"))
    (display
     (format #f "Mr. Rogers look outward from each "))
    (display
     (format #f "corner of the square.)~%"))
    (newline)
    (display
     (format #f "(d) wave painter~%"))
    (display
     (format #f "(define (wave-painter frame)~%"))
    (display
     (format #f "  (let ((origin "))
    (display
     (format #f "(get-frame-origin frame))~%"))
    (display
     (format #f "        (edge1 (get-frame-edge1 "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge2 (get-frame-edge2 "))
    (display
     (format #f "frame)))~%"))
    (display
     (format #f "    (let ((pt-01 "))
    (display
     (format #f "(scale-vect 0.60 edge1))~%"))
    (display
     (format #f "          (pt-02 "))
    (display
     (format #f "(add-vect (scale-vect 0.15 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.45 edge2)))~%"))
    (display
     (format #f "          (pt-03 "))
    (display
     (format #f "(add-vect (scale-vect 0.25 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.55 edge2)))~%"))
    (display
     (format #f "          (pt-04 "))
    (display
     (format #f "(add-vect (scale-vect 0.28 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.52 edge2)))~%"))
    (display
     (format #f "          (pt-05 "))
    (display
     (format #f "(scale-vect 0.250 edge1)))~%"))
    (display
     (format #f "      (let ((line-list-1~%"))
    (display
     (format #f "             (list "))
    (display
     (format #f "(make-segment pt-01 pt-02)~%"))
    (display
     (format #f "      (make-segment pt-02 pt-03)~%"))
    (display
     (format #f "                   (make-segment "))
    (display
     (format #f "pt-03 pt-04)~%"))
    (display
     (format #f "      (make-segment pt-04 pt-05)))~%"))
    (display
     (format #f "            (pt-11 "))
    (display
     (format #f "(scale-vect 0.35 edge1))~%"))
    (display
     (format #f "            (pt-12 "))
    (display
     (format #f "(add-vect (scale-vect 0.50 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.30 edge2)))~%"))
    (display
     (format #f "            (pt-13 "))
    (display
     (format #f "(scale-vect 0.65 edge1)))~%"))
    (display
     (format #f "        (let ((line-list-2~%"))
    (display
     (format #f "               (list "))
    (display
     (format #f "(make-segment pt-11 pt-12)~%"))
    (display
     (format #f "      (make-segment pt-12 pt-13)))~%"))
    (display
     (format #f "              (pt-21 "))
    (display
     (format #f "(scale-vect 0.75 edge1))~%"))
    (display
     (format #f "              (pt-22 "))
    (display
     (format #f "(add-vect (scale-vect 0.67 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.47 edge2)))~%"))
    (display
     (format #f "              (pt-23 "))
    (display
     (format #f "(add-vect edge1~%"))
    (display
     (format #f "      (scale-vect 0.35 edge2))))~%"))
    (display
     (format #f "          (let ((line-list-3~%"))
    (display
     (format #f "                 (list "))
    (display
     (format #f "(make-segment pt-21 pt-22)~%"))
    (display
     (format #f "      (make-segment pt-22 pt-23)))~%"))
    (display
     (format #f "                (pt-31 "))
    (display
     (format #f "(add-vect pt-23~%"))
    (display
     (format #f "      (scale-vect 0.10 edge2)))~%"))
    (display
     (format #f "                (pt-32 "))
    (display
     (format #f "(add-vect (scale-vect 0.75 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                (pt-33 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.67 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                (pt-34 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.70 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.80 edge2)))~%"))
    (display
     (format #f "                (pt-35 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.65 edge1) edge2)))~%"))
    (display
     (format #f "            (let ((line-list-4~%"))
    (display
     (format #f "                   (list "))
    (display
     (format #f "(make-segment pt-31 pt-32)~%"))
    (display
     (format #f "      (make-segment pt-32 pt-33)~%"))
    (display
     (format #f "      (make-segment pt-33 pt-34)~%"))
    (display
     (format #f "      (make-segment pt-34 pt-35)))~%"))
    (display
     (format #f "                  (pt-41 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.30 edge1) edge2))~%"))
    (display
     (format #f "                  (pt-42 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.27 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.80 edge2)))~%"))
    (display
     (format #f "                  (pt-43 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.30 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                  (pt-44 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.25 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                  (pt-45 "))
    (display
     (format #f "(add-vect~%"))
    (display
     (format #f "      (scale-vect 0.15 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.57 edge2)))~%"))
    (display
     (format #f "                  (pt-46 "))
    (display
     (format #f "(scale-vect 0.75 edge2)))~%"))
    (display
     (format #f "              (let ((line-list-5~%"))
    (display
     (format #f "                     (list "))
    (display
     (format #f "(make-segment pt-41 pt-42)~%"))
    (display
     (format #f "      (make-segment pt-42 pt-43)~%"))
    (display
     (format #f "         (make-segment pt-43 pt-44)~%"))
    (display
     (format #f "         (make-segment pt-44 pt-45)~%"))
    (display
     (format #f "         (make-segment pt-45 pt-46)))~%"))
    (display
     (format #f "                    (pt-51 (add-vect~%"))
    (display
     (format #f "      (scale-vect 0.32 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.70 edge2)))~%"))
    (display
     (format #f "                    (pt-52 (add-vect~%"))
    (display
     (format #f "      (scale-vect 0.33 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.69 edge2)))~%"))
    (display
     (format #f "                    (pt-53 (add-vect~%"))
    (display
     (format #f "      (scale-vect 0.62 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.69 edge2)))~%"))
    (display
     (format #f "                    (pt-54 (add-vect~%"))
    (display
     (format #f "      (scale-vect 0.63 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.70 edge2))))~%"))
    (display
     (format #f "                (let ((line-list-6~%"))
    (display
     (format #f "                       (list "))
    (display
     (format #f "(make-segment pt-51 pt-52)~%"))
    (display
     (format #f "      (make-segment pt-52 pt-53)~%"))
    (display
     (format #f "                             (make-segment "))
    (display
     (format #f "pt-53 pt-54))))~%"))
    (display
     (format #f "                  (let ~%"))
    (display
     (format #f "      ((sp-func-1 (segments->painter "))
    (display
     (format #f "line-list-1))~%"))
    (display
     (format #f "       (sp-func-2 (segments->painter "))
    (display
     (format #f "line-list-2))~%"))
    (display
     (format #f "        (sp-func-3 (segments->painter "))
    (display
     (format #f "line-list-3))~%"))
    (display
     (format #f "        (sp-func-4 (segments->painter "))
    (display
     (format #f "line-list-4))~%"))
    (display
     (format #f "        (sp-func-5 (segments->painter "))
    (display
     (format #f "line-list-5)))~%"))
    (display
     (format #f "           (begin~%"))
    (display
     (format #f "            (lambda (frame)~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "              (sp-func-1 (frame))~%"))
    (display
     (format #f "              (sp-func-2 (frame))~%"))
    (display
     (format #f "              (sp-func-3 (frame))~%"))
    (display
     (format #f "              (sp-func-4 (frame))~%"))
    (display
     (format #f "              (sp-func-5 (frame))~%"))
    (display
     (format #f "              (sp-func-6 (frame))~%"))
    (display
     (format #f "                 ))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "           ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     )))~%"))
    (newline)
    (display
     (format #f "(b) corner split~%"))
    (display
     (format #f "(define (corner-split painter n)~%"))
    (display
     (format #f "  (if (= n 0)~%"))
    (display
     (format #f "      painter~%"))
    (display
     (format #f "      (let ((up (up-split "))
    (display
     (format #f "painter (- n 1)))~%"))
    (display
     (format #f "            (right (right-split "))
    (display
     (format #f "painter (- n 1))))~%"))
    (display
     (format #f "        (let ((corner (corner-split "))
    (display
     (format #f "painter (- n 1))))~%"))
    (display
     (format #f "          (beside (below "))
    (display
     (format #f "painter up)~%"))
    (display
     (format #f "                  (below "))
    (display
     (format #f "right corner))))))~%"))
    (newline)
    (display
     (format #f "(c) square-limit~%"))
    (display
     (format #f "(define (square-limit painter n)~%"))
    (display
     (format #f "  (let ((combine4~%"))
    (display
     (format #f "         (square-of-four~%"))
    (display
     (format #f "          identity flip-vert "))
    (display
     (format #f "identity flip-vert)))~%"))
    (display
     (format #f "    (combine4 (corner-split painter n))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.52 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
