#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.36                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (accumulate-n op init seqs)
  (begin
    (if (or (null? seqs) (null? (car seqs)))
        (begin
          (list))
        (begin
          (cons (accumulate op init (map car seqs))
                (accumulate-n op init (map cdr seqs)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-accumulate-n-1 result-hash-table)
 (begin
   (let ((sub-name "test-accumulate-n-1")
         (test-list
          (list
           (list (list (list 1 2) (list 3 4))
                 (list 4 6))
           (list (list (list 1 2 3) (list 7 8 9))
                 (list 8 10 12))
           (list (list (list 1 2 3) (list 4 5 6)
                       (list 7 8 9) (list 10 11 12))
                 (list 22 26 30))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (accumulate-n + 0 alist-list)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist-list))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The procedure accumulate-n is similar "))
    (display
     (format #f "to accumulate except~%"))
    (display
     (format #f "that it takes as its third argument "))
    (display
     (format #f "a sequence of~%"))
    (display
     (format #f "sequences, which are all assumed to "))
    (display
     (format #f "have the same number~%"))
    (display
     (format #f "of elements. It applies the designated "))
    (display
     (format #f "accumulation~%"))
    (display
     (format #f "procedure to combine all the first "))
    (display
     (format #f "elements of the sequences,~%"))
    (display
     (format #f "all the second elements of the "))
    (display
     (format #f "sequences, and so on,~%"))
    (display
     (format #f "and returns a sequence of the results. "))
    (display
     (format #f "For instance, if~%"))
    (display
     (format #f "s is a sequence containing four "))
    (display
     (format #f "sequences,~%"))
    (display
     (format #f "((1 2 3) (4 5 6) (7 8 9) (10 11 12)),~%"))
    (display
     (format #f "then the value of (accumulate-n + 0 s) "))
    (display
     (format #f "should be the sequence~%"))
    (display
     (format #f "(22 26 30). Fill in the missing "))
    (display
     (format #f "expressions in the~%"))
    (display
     (format #f "following definition of accumulate-n:~%"))
    (display
     (format #f "(define (accumulate-n op init seqs)~%"))
    (display
     (format #f "  (if (null? (car seqs))~%"))
    (display
     (format #f "      nil~%"))
    (display
     (format #f "      (cons (accumulate op init <??>)~%"))
    (display
     (format #f "            (accumulate-n "))
    (display
     (format #f "op init <??>))))~%"))
    (newline)
    (display
     (format #f "(define (accumulate-n op init seqs)~%"))
    (display
     (format #f "  (if (or (null? seqs) "))
    (display
     (format #f "(null? (car seqs)))~%"))
    (display
     (format #f "      (list)~%"))
    (display
     (format #f "      (cons (accumulate op "))
    (display
     (format #f "init (map car seqs))~%"))
    (display
     (format #f "            (accumulate-n "))
    (display
     (format #f "op init (map cdr seqs)))~%"))
    (display
     (format #f "      ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((alist
           (list
            (list 1 2 3) (list 4 5 6)
            (list 7 8 9) (list 10 11 12))))
      (begin
        (display
         (format #f "(accumulate-n + 0 ~a) = ~a~%"
                 alist (accumulate-n + 0 alist)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.36 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
