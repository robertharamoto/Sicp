#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.64                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-1
             (format
              #f ", shouldbe=~a, result=~a"
              shouldbe-list result-list))
            (err-msg-2
             (format
              #f ", length shouldbe=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append
            err-start err-msg-1 err-msg-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((sflag (member s-elem result-list))
                      (err-msg-3
                       (format
                        #f ", missing element ~a"
                        s-elem)))
                  (begin
                    (unittest2:assert?
                     (not (equal? sflag #f))
                     sub-name
                     (string-append err-start err-msg-1 err-msg-3)
                     result-hash-table)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (entry tree)
  (begin
    (car tree)
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch tree)
  (begin
    (cadr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch tree)
  (begin
    (caddr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (make-tree entry left right)
  (begin
    (list entry left right)
    ))

;;;#############################################################
;;;#############################################################
(define (tree->list-1 tree)
  (begin
    (if (null? tree)
        (begin
          (list))
        (begin
          (append (tree->list-1 (left-branch tree))
                  (cons (entry tree)
                        (tree->list-1 (right-branch tree))))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tree->list-1-1 result-hash-table)
 (begin
   (let ((sub-name "test-tree->list-1-1")
         (test-list
          (list
           (list
            (list 7 (list 3 (list 1 (list) (list))
                          (list 5 (list) (list)))
                  (list 9 (list)
                        (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           (list
            (list 3 (list 1 (list) (list))
                  (list 7 (list 5 (list) (list))
                        (list 9 (list)
                              (list 11 (list) (list)))))
            (list 1 3 5 7 9 11))
           (list
            (list 5 (list 1 (list 3 (list) (list))
                          (list))
                  (list 9 (list 7 (list) (list))
                        (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((tree-1 (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (tree->list-1 tree-1)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : tree-1=~a, "
                        sub-name test-label-index
                        tree-1)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (begin
      (if (null? tree)
          (begin
            result-list)
          (begin
            (copy-to-list
             (left-branch tree)
             (cons (entry tree)
                   (copy-to-list
                    (right-branch tree)
                    result-list)))
            ))
      ))
  (begin
    (copy-to-list tree (list))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tree->list-2-1 result-hash-table)
 (begin
   (let ((sub-name "test-tree->list-2-1")
         (test-list
          (list
           (list
            (list 7 (list 3 (list 1 (list) (list))
                          (list 5 (list) (list)))
                  (list 9 (list) (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           (list
            (list 3 (list 1 (list) (list))
                  (list 7 (list 5 (list) (list))
                        (list 9 (list)
                              (list 11 (list) (list)))))
            (list 1 3 5 7 9 11))
           (list
            (list 5 (list 1 (list 3 (list) (list))
                          (list))
                  (list 9 (list 7 (list) (list))
                        (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((tree-2 (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (tree->list-2 tree-2)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : tree-2=~a, "
                        sub-name test-label-index
                        tree-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list->tree elements)
  (begin
    (car (partial-tree elements (length elements)))
    ))

;;;#############################################################
;;;#############################################################
(define (partial-tree elts n)
  (begin
    (if (= n 0)
        (begin
          (cons (list) elts))
        (begin
          (let ((left-size (quotient (- n 1) 2)))
            (let ((left-result (partial-tree elts left-size)))
              (let ((left-tree (car left-result))
                    (non-left-elts (cdr left-result))
                    (right-size (- n (+ left-size 1))))
                (let ((this-entry (car non-left-elts))
                      (right-result (partial-tree (cdr non-left-elts)
                                                  right-size)))
                  (let ((right-tree (car right-result))
                        (remaining-elts (cdr right-result)))
                    (begin
                      (cons (make-tree this-entry left-tree right-tree)
                            remaining-elts)
                      ))
                  ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (list->tree-debug elements)
  (begin
    (car (partial-tree-debug elements (length elements)))
    ))

;;;#############################################################
;;;#############################################################
(define (partial-tree-debug elts n)
  (begin
    (display (format #f "(n=~a), elts=~a~%" n elts))
    (force-output)
    (if (= n 0)
        (begin
          (cons (list) elts))
        (begin
          (let ((left-size (quotient (- n 1) 2)))
            (begin
              (let ((left-result
                     (partial-tree-debug elts left-size)))
                (let ((left-tree (car left-result))
                      (non-left-elts (cdr left-result))
                      (right-size (- n (+ left-size 1))))
                  (let ((this-entry (car non-left-elts))
                        (right-result
                         (partial-tree-debug
                          (cdr non-left-elts) right-size)))
                    (let ((right-tree (car right-result))
                          (remaining-elts (cdr right-result)))
                      (begin
                        (cons (make-tree
                               this-entry left-tree right-tree)
                              remaining-elts)
                        ))
                    )))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The following procedure list->tree "))
    (display
     (format #f "converts an ordered~%"))
    (display
     (format #f "list to a balanced binary tree. The "))
    (display
     (format #f "helper procedure~%"))
    (display
     (format #f "partial-tree takes as arguments an "))
    (display
     (format #f "integer n and list~%"))
    (display
     (format #f "of at least n elements and constructs "))
    (display
     (format #f "a balanced tree~%"))
    (display
     (format #f "containing the first n elements of "))
    (display
     (format #f "the list. The~%"))
    (display
     (format #f "result returned by partial-tree is a "))
    (display
     (format #f "pair (formed with~%"))
    (display
     (format #f "cons), whose car is the constructed "))
    (display
     (format #f "tree and whose cdr~%"))
    (display
     (format #f "is the list of elements not included "))
    (display
     (format #f "in the tree.~%"))
    (display
     (format #f "(define (list->tree elements)~%"))
    (display
     (format #f "  (car (partial-tree elements (length elements))))~%"))
    (display
     (format #f "(define (partial-tree elts n)~%"))
    (display
     (format #f "  (if (= n 0)~%"))
    (display
     (format #f "      (cons (list) elts)~%"))
    (display
     (format #f "      (let ((left-size "))
    (display
     (format #f "(quotient (- n 1) 2)))~%"))
    (display
     (format #f "        (let ((left-result "))
    (display
     (format #f "(partial-tree elts "))
    (display
     (format #f "left-size)))~%"))
    (display
     (format #f "          (let ((left-tree "))
    (display
     (format #f "(car left-result))~%"))
    (display
     (format #f "                (non-left-elts "))
    (display
     (format #f "(cdr left-result))~%"))
    (display
     (format #f "                (right-size "))
    (display
     (format #f "(- n (+ left-size 1))))~%"))
    (display
     (format #f "            (let ((this-entry "))
    (display
     (format #f "(car non-left-elts))~%"))
    (display
     (format #f "                  (right-result "))
    (display
     (format #f "(partial-tree (cdr non-left-elts)~%"))
    (display
     (format #f "                                "))
    (display
     (format #f "              right-size)))~%"))
    (display
     (format #f "              (let ((right-tree "))
    (display
     (format #f "(car right-result))~%"))
    (display
     (format #f "                    (remaining-elts "))
    (display
     (format #f "(cdr right-result)))~%"))
    (display
     (format #f "                (cons (make-tree "))
    (display
     (format #f "this-entry left-tree right-tree)~%"))
    (display
     (format #f "                      "))
    (display
     (format #f "remaining-elts))))))))~%"))
    (newline)
    (display
     (format #f "a. Write a short paragraph explaining "))
    (display
     (format #f "as clearly as you~%"))
    (display
     (format #f "can how partial-tree works. Draw the "))
    (display
     (format #f "tree produced by~%"))
    (display
     (format #f "list->tree for the list "))
    (display
     (format #f "(1 3 5 7 9 11).~%"))
    (display
     (format #f "b. What is the order of growth in the "))
    (display
     (format #f "number of steps~%"))
    (display
     (format #f "required by list->tree to convert a "))
    (display
     (format #f "list of n elements?~%"))
    (newline)
    (display
     (format #f "(a) the algorithm assumes that the "))
    (display
     (format #f "elements are sorted~%"))
    (display
     (format #f "in ascending order. It computes "))
    (display
     (format #f "(quotient (- n 1) 2)~%"))
    (display
     (format #f "which gives the number of elements "))
    (display
     (format #f "that will be on~%"))
    (display
     (format #f "the left side of the tree, one will "))
    (display
     (format #f "be in the center,~%"))
    (display
     (format #f "and the rest will be on the "))
    (display
     (format #f "right.~%"))
    (newline)
    (display
     (format #f "(b) when n=6, the number of times "))
    (display
     (format #f "partial-tree is~%"))
    (display
     (format #f "called is 13.~%"))
    (newline)
    (display
     (format #f "When n=12, the number of times "))
    (display
     (format #f "partial-tree is called~%"))
    (display
     (format #f "is 25. So the order of growth "))
    (display
     (format #f "is theta(n).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list 1 3 5 7 9 11)
            (list 1 3 5 7 9 11 13 17 19 21 23 25)
            )))
      (begin
        (for-each
         (lambda (tlist)
           (begin
             (let ((tree-result
                    (list->tree tlist)))
               (begin
                 (display
                  (format
                   #f "list=~a, tree=~a~%" tlist tree-result))
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.64 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
