#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.3                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-point x y)
  (begin
    (cons x y)
    ))

;;;#############################################################
;;;#############################################################
(define (x-point xx)
  (begin
    (car xx)
    ))

;;;#############################################################
;;;#############################################################
(define (y-point xx)
  (begin
    (cdr xx)
    ))

;;;#############################################################
;;;#############################################################
(define (make-segment start end)
  (begin
    (if (and
         (pair? start)
         (pair? end))
        (begin
          (list start end))
        (begin
          (display
           (format
            #f "make-segment error: expecting two points! "))
          (display
           (format
            #f "received ~a ~a~%" start end))
          (display (format #f "quitting...~%"))
          (force-output)
          (quit)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (start-segment xx)
  (begin
    (list-ref xx 0)
    ))

;;;#############################################################
;;;#############################################################
(define (end-segment xx)
  (begin
    (list-ref xx 1)
    ))

;;;#############################################################
;;;#############################################################
(define TYPE-1 1)
(define TYPE-2 2)

;;;#############################################################
;;;#############################################################
;;; points assumed to be in order (either clockwise or counter-clockwise)
(define (make-rectangle-1 xx yy zz tt)
  (begin
    (let ((rlist (list TYPE-1 xx yy zz tt)))
      (begin
        rlist
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; lines assumed to be in order (either clockwise or counter-clockwise)
(define (make-rectangle-2 line-1 line-2 line-3 line-4)
  (begin
    (let ((rlist
           (list TYPE-2 line-1 line-2 line-3 line-4)))
      (begin
        rlist
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-points-width p1 p2)
  (begin
    (let ((x-1 (car p1))
          (y-1 (cdr p1))
          (x-2 (car p2))
          (y-2 (cdr p2)))
      (let ((x-diff (- x-2 x-1))
            (y-diff (- y-2 y-1)))
        (let ((width
               (sqrt (+ (* x-diff x-diff)
                        (* y-diff y-diff)))
               ))
          (begin
            width
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (width rectangle)
  (begin
    (let ((rec-type (list-ref rectangle 0)))
      (begin
        (cond
         ((= rec-type TYPE-1)
          (begin
            ;;; 4 points
            (let ((p1 (list-ref rectangle 1))
                  (p2 (list-ref rectangle 2))
                  (p3 (list-ref rectangle 3))
                  (p4 (list-ref rectangle 4)))
              (begin
                (calc-points-width p1 p2)
                ))
            ))
         ((= rec-type TYPE-2)
          (begin
            ;;; 4 line segments
            (let ((l1 (list-ref rectangle 1))
                  (l2 (list-ref rectangle 2))
                  (l3 (list-ref rectangle 3))
                  (l4 (list-ref rectangle 4)))
              (let ((p1 (start-segment l1))
                    (p2 (end-segment l1)))
                (begin
                  (calc-points-width p1 p2)
                  )))
            ))
         (else
          (begin
            (display
             (format
              #f "debug width error: invalid type = ~a, "
              rec-type))
            (display
             (format
              #f "rectangle = ~a~%" rectangle))
            (display (format #f "quitting...~%"))
            (force-output)
            (quit)
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-width-1 result-hash-table)
 (begin
   (let ((sub-name "test-width-1")
         (test-list
          (list
           (list (make-rectangle-1
                  (cons 0 0) (cons 2 0) (cons 2 1) (cons 0 1))
                 2)
           (list (make-rectangle-1
                  (cons 0 0) (cons 5 0) (cons 5 10) (cons 0 10))
                 5)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 2 0))
                  (make-segment (cons 2 0) (cons 2 1))
                  (make-segment (cons 2 1) (cons 0 1))
                  (make-segment (cons 0 1) (cons 0 0)))
                 2)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 5 0))
                  (make-segment (cons 5 0) (cons 5 10))
                  (make-segment (cons 5 10) (cons 0 10))
                  (make-segment (cons 0 10) (cons 0 0)))
                 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((rect (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (width rect)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : rectangle=~a, "
                        sub-name test-label-index rect))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calc-points-height p1 p2)
  (begin
    (let ((x-1 (car p1))
          (y-1 (cdr p1))
          (x-2 (car p2))
          (y-2 (cdr p2)))
      (let ((x-diff (- x-2 x-1))
            (y-diff (- y-2 y-1)))
        (let ((height
               (sqrt (+ (* x-diff x-diff)
                        (* y-diff y-diff)))
               ))
          (begin
            height
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (height rectangle)
  (begin
    (let ((rec-type (list-ref rectangle 0)))
      (begin
        (cond
         ((= rec-type TYPE-1)
          (begin
            ;;; 4 points
            (let ((p1 (list-ref rectangle 1))
                  (p2 (list-ref rectangle 2))
                  (p3 (list-ref rectangle 3))
                  (p4 (list-ref rectangle 4)))
              (begin
                (calc-points-height p2 p3)
                ))
            ))
         ((= rec-type TYPE-2)
          (begin
            ;;; 4 line segments
            (let ((l1 (list-ref rectangle 1))
                  (l2 (list-ref rectangle 2))
                  (l3 (list-ref rectangle 3))
                  (l4 (list-ref rectangle 4)))
              (let ((p1 (start-segment l2))
                    (p2 (end-segment l2)))
                (begin
                  (calc-points-height p1 p2)
                  )))
            ))
         (else
          (begin
            (display
             (format
              #f "debug height error: invalid type = ~a, "
              rec-type))
            (display
             (format
              #f "rectangle = ~a~%" rectangle))
            (display
             (format #f "quitting...~%"))
            (force-output)
            (quit)
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-height-1 result-hash-table)
 (begin
   (let ((sub-name "test-height-1")
         (test-list
          (list
           (list (make-rectangle-1
                  (cons 0 0) (cons 2 0) (cons 2 1) (cons 0 1))
                 1)
           (list (make-rectangle-1
                  (cons 0 0) (cons 5 0) (cons 5 10) (cons 0 10))
                 10)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 2 0))
                  (make-segment (cons 2 0) (cons 2 1))
                  (make-segment (cons 2 1) (cons 0 1))
                  (make-segment (cons 0 1) (cons 0 0)))
                 1)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 5 0))
                  (make-segment (cons 5 0) (cons 5 10))
                  (make-segment (cons 5 10) (cons 0 10))
                  (make-segment (cons 0 10) (cons 0 0)))
                 10)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((rect (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (height rect)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : rectangle=~a, "
                        sub-name test-label-index rect))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (print-point x)
  (begin
    (display
     (format #f "(~a, ~a)" (x-point x) (y-point x)))
    ))

;;;#############################################################
;;;#############################################################
(define (perimeter rect)
  (begin
    (let ((width (width rect))
          (height (height rect)))
      (begin
        (+ width width height height)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-perimeter-1 result-hash-table)
 (begin
   (let ((sub-name "test-perimeter-1")
         (test-list
          (list
           (list (make-rectangle-1
                  (cons 0 0) (cons 2 0) (cons 2 1) (cons 0 1))
                 6)
           (list (make-rectangle-1
                  (cons 0 0) (cons 5 0) (cons 5 10) (cons 0 10))
                 30)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 2 0))
                  (make-segment (cons 2 0) (cons 2 1))
                  (make-segment (cons 2 1) (cons 0 1))
                  (make-segment (cons 0 1) (cons 0 0)))
                 6)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 5 0))
                  (make-segment (cons 5 0) (cons 5 10))
                  (make-segment (cons 5 10) (cons 0 10))
                  (make-segment (cons 0 10) (cons 0 0)))
                 30)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((rect (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (perimeter rect)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : rectangle=~a, "
                        sub-name test-label-index rect))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (area rect)
  (begin
    (let ((width (width rect))
          (height (height rect)))
      (begin
        (* width height)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-area-1 result-hash-table)
 (begin
   (let ((sub-name "test-area-1")
         (test-list
          (list
           (list (make-rectangle-1
                  (cons 0 0) (cons 2 0) (cons 2 1) (cons 0 1))
                 2)
           (list (make-rectangle-1
                  (cons 0 0) (cons 5 0) (cons 5 10) (cons 0 10))
                 50)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 2 0))
                  (make-segment (cons 2 0) (cons 2 1))
                  (make-segment (cons 2 1) (cons 0 1))
                  (make-segment (cons 0 1) (cons 0 0)))
                 2)
           (list (make-rectangle-2
                  (make-segment (cons 0 0) (cons 5 0))
                  (make-segment (cons 5 0) (cons 5 10))
                  (make-segment (cons 5 10) (cons 0 10))
                  (make-segment (cons 0 10) (cons 0 0)))
                 50)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((rect (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (area rect)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : rectangle=~a, "
                        sub-name test-label-index rect))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (= shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-loop-points)
  (begin
    (let ((test-list
           (list
            (list (make-rectangle-1
                   (cons 0 0) (cons 2 0) (cons 2 1) (cons 0 1)))
            (list (make-rectangle-1
                   (cons 0 0) (cons 5 0) (cons 5 10) (cons 0 10)))
            )))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((rect (list-ref alist 0)))
               (let ((r-p (perimeter rect))
                     (r-a (area rect)))
                 (begin
                   (display
                    (format
                     #f "rec=~a, perimeter=~a, area=~a~%"
                     rect r-p r-a))
                   (force-output)
                   )))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-segments)
  (begin
    (let ((test-list
           (list
            (list (make-rectangle-2
                   (make-segment (cons 0 0) (cons 2 0))
                   (make-segment (cons 2 0) (cons 2 1))
                   (make-segment (cons 2 1) (cons 0 1))
                   (make-segment (cons 0 1) (cons 0 0))))
            (list (make-rectangle-2
                   (make-segment (cons 0 0) (cons 5 0))
                   (make-segment (cons 5 0) (cons 5 10))
                   (make-segment (cons 5 10) (cons 0 10))
                   (make-segment (cons 0 10) (cons 0 0))))
            )))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((rect (list-ref alist 0)))
               (let ((r-p (perimeter rect))
                     (r-a (area rect)))
                 (begin
                   (display
                    (format
                     #f "segment rec=~a~%" rect))
                   (display
                    (format
                     #f "segment perimeter=~a, area=~a~%"
                     r-p r-a))
                   (newline)
                   (force-output)
                   )))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Implement a representation for "))
    (display
     (format #f "rectangles in a plane.~%"))
    (display
     (format #f "(Hint: You may want to make use "))
    (display
     (format #f "of exercise 2.2.)~%"))
    (display
     (format #f "In terms of your constructors and "))
    (display
     (format #f "selectors, create~%"))
    (display
     (format #f "procedures that compute the perimeter "))
    (display
     (format #f "and the area of a~%"))
    (display
     (format #f "given rectangle. Now implement a "))
    (display
     (format #f "different representation~%"))
    (display
     (format #f "for rectangles. Can you design your "))
    (display
     (format #f "system with suitable~%"))
    (display
     (format #f "abstraction barriers, so that the "))
    (display
     (format #f "same perimeter and~%"))
    (display
     (format #f "area procedures will work using either "))
    (display
     (format #f "representation?~%"))
    (newline)
    (display
     (format #f "represention 1) use four points, one "))
    (display
     (format #f "for each corner~%"))
    (display
     (format #f "represention 2) use four line segments~%"))
    (newline)
    (display
     (format #f "Both representations have a get-width "))
    (display
     (format #f "and get-height~%"))
    (display
     (format #f "function, so the perimeter and "))
    (display
     (format #f "area calculations~%"))
    (display
     (format #f "can be easily done.~%"))
    (newline)
    (display
     (format #f "programs that use rectangles~%"))
    (display
     (format #f "perimeter and area~%"))
    (display
     (format #f "-----------------------------------------------~%"))
    (display
     (format #f "width and height~%"))
    (display
     (format #f "-----------------------------------------------~%"))
    (display
     (format #f "make-rectangle-1 make-rectangle-2~%"))
    (display
     (format #f "calc-points-width, calc-points-height~%"))
    (display
     (format #f "-----------------------------------------------~%"))
    (display
     (format #f "list, list-ref, cons, car, cdr~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.03 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             ;;; rectangles represented as points
             (main-loop-points)
             (newline)
             ))

          (timer-module:time-code-macro
           (begin
             ;;; rectangles represented as line segments
             (main-loop-segments)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
