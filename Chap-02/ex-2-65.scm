#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.65                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-1
             (format
              #f ", shouldbe=~a, result=~a"
              shouldbe-list result-list))
            (err-msg-2
             (format
              #f ", length shouldbe=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-1 err-msg-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((sflag (member s-elem result-list))
                      (err-msg-3
                       (format
                        #f ", missing element ~a"
                        s-elem)))
                  (begin
                    (unittest2:assert?
                     (not (equal? sflag #f))
                     sub-name
                     (string-append
                      err-start err-msg-1 err-msg-3)
                     result-hash-table)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (entry tree)
  (begin
    (car tree)
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch tree)
  (begin
    (cadr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch tree)
  (begin
    (caddr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (make-tree entry left right)
  (begin
    (list entry left right)
    ))

;;;#############################################################
;;;#############################################################
(define (tree->list-1 tree)
  (begin
    (if (null? tree)
        (begin
          (list))
        (begin
          (append (tree->list-1 (left-branch tree))
                  (cons (entry tree)
                        (tree->list-1 (right-branch tree))))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tree->list-1-1 result-hash-table)
 (begin
   (let ((sub-name "test-tree->list-1-1")
         (test-list
          (list
           (list
            (list 7 (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
                  (list 9 (list) (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           (list
            (list 3 (list 1 (list) (list))
                  (list 7 (list 5 (list) (list))
                        (list 9 (list) (list 11 (list) (list)))))
            (list 1 3 5 7 9 11))
           (list
            (list 5 (list 1 (list 3 (list) (list)) (list))
                  (list 9 (list 7 (list) (list))
                        (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((tree-1 (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (tree->list-1 tree-1)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : tree-1=~a, "
                        sub-name test-label-index
                        tree-1)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (begin
      (if (null? tree)
          (begin
            result-list)
          (begin
            (copy-to-list (left-branch tree)
                          (cons
                           (entry tree)
                           (copy-to-list
                            (right-branch tree)
                            result-list)))
            ))
      ))
  (begin
    (copy-to-list tree (list))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tree->list-2-1 result-hash-table)
 (begin
   (let ((sub-name "test-tree->list-2-1")
         (test-list
          (list
           (list
            (list 7 (list 3 (list 1 (list) (list))
                          (list 5 (list) (list)))
                  (list 9 (list) (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           (list
            (list 3 (list 1 (list) (list))
                  (list 7 (list 5 (list) (list))
                        (list 9 (list)
                              (list 11 (list) (list)))))
            (list 1 3 5 7 9 11))
           (list
            (list 5 (list 1 (list 3 (list) (list)) (list))
                  (list 9 (list 7 (list) (list))
                        (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((tree-2 (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (tree->list-2 tree-2)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : tree-2=~a, "
                        sub-name test-label-index
                        tree-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list->tree elements)
  (begin
    (car (partial-tree elements (length elements)))
    ))

;;;#############################################################
;;;#############################################################
(define (partial-tree elts n)
  (begin
    (if (= n 0)
        (begin
          (cons (list) elts))
        (begin
          (let ((left-size (quotient (- n 1) 2)))
            (let ((left-result (partial-tree elts left-size)))
              (let ((left-tree (car left-result))
                    (non-left-elts (cdr left-result))
                    (right-size (- n (+ left-size 1))))
                (let ((this-entry (car non-left-elts))
                      (right-result
                       (partial-tree (cdr non-left-elts)
                                     right-size)))
                  (let ((right-tree (car right-result))
                        (remaining-elts (cdr right-result)))
                    (begin
                      (cons
                       (make-tree
                        this-entry left-tree right-tree)
                       remaining-elts)
                      ))
                  ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list->tree-1 result-hash-table)
 (begin
   (let ((sub-name "test-list->tree-1")
         (test-list
          (list
           (list
            (list 1 3 5)
            (list 3 (list 1 (list) (list))
                  (list 5 (list) (list))))
           (list
            (list 1 3 5 7)
            (list 3 (list 1 (list) (list))
                  (list 5 (list)
                        (list 7 (list) (list)))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe-tree (list-ref this-list 1)))
              (let ((result-tree (list->tree alist)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-tree result-tree
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (intersection-set set1 set2)
  (begin
    (if (or (null? set1) (null? set2))
        (begin
          (list))
        (begin
          (let ((x1 (car set1)) (x2 (car set2)))
            (begin
              (cond
               ((= x1 x2)
                (begin
                  (cons
                   x1 (intersection-set
                       (cdr set1) (cdr set2)))
                  ))
               ((< x1 x2)
                (begin
                  (intersection-set (cdr set1) set2)
                  ))
               (else
                (begin
                  (intersection-set set1 (cdr set2))
                  )))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-intersection-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-intersection-set-1")
         (test-list
          (list
           (list
            (list 1 2 3) (list 3 4 5)
            (list 3))
           (list
            (list 1 2 3) (list 4 5 6)
            (list))
           (list
            (list 1 2 3 4) (list 1 2 4 5 6)
            (list 1 2 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((list-1 (list-ref this-list 0))
                  (list-2 (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list (intersection-set list-1 list-2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "list-1=~a, list-2=~a, "
                        list-1 list-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (union-set set1 set2)
  (begin
    (cond
     ((null? set1)
      (begin
        set2
        ))
     ((null? set2)
      (begin
        set1
        ))
     (else
      (begin
        (let ((x1 (car set1)) (x2 (car set2)))
          (begin
            (cond
             ((= x1 x2)
              (begin
                (cons x1
                      (union-set (cdr set1)
                                 (cdr set2)))
                ))
             ((< x1 x2)
              (begin
                (cons x1 (union-set (cdr set1) set2))
                ))
             (else
              (begin
                (cons x2 (union-set set1 (cdr set2)))
                )))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-union-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-union-set-1")
         (test-list
          (list
           (list
            (list 1 2 3) (list 3 4 5) (list 1 2 3 4 5))
           (list
            (list 1 2 3) (list 4 5 6) (list 1 2 3 4 5 6))
           (list
            (list 1 2 3 4) (list 1 2 4) (list 1 2 3 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((list-1 (list-ref this-list 0))
                  (list-2 (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list (union-set list-1 list-2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "list-1=~a, list-2=~a, "
                        list-1 list-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (intersection-tree tree1 tree2)
  (begin
    (list->tree
     (intersection-set
      (tree->list-2 tree1)
      (tree->list-2 tree2)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-intersecion-tree-1 result-hash-table)
 (begin
   (let ((sub-name "test-intersection-tree-1")
         (test-list
          (list
           (list
            (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
            (list 3 (list 2 (list) (list)) (list 7 (list) (list)))
            (list 3 (list) (list)))
           (list
            (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
            (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
            (list 3 (list 1 (list) (list)) (list 5 (list) (list))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((list-1 (list-ref this-list 0))
                  (list-2 (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list (intersection-tree list-1 list-2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "list-1=~a, list-2=~a, "
                        list-1 list-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (union-tree tree1 tree2)
  (begin
    (list->tree
     (union-set
      (tree->list-2 tree1)
      (tree->list-2 tree2)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-union-tree-1 result-hash-table)
 (begin
   (let ((sub-name "test-union-tree-1")
         (test-list
          (list
           (list
            (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
            (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
            (list 3 (list 1 (list) (list)) (list 5 (list) (list))))
           (list
            (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
            (list 3 (list 2 (list) (list)) (list 7 (list) (list)))
            (list 3 (list 1 (list) (list 2 (list) (list)))
                  (list 5 (list) (list 7 (list) (list)))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((list-1 (list-ref this-list 0))
                  (list-2 (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list (union-tree list-1 list-2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "list-1=~a, list-2=~a, "
                        list-1 list-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Use the results of exercises 2.63 and "))
    (display
     (format #f "2.64 to give~%"))
    (display
     (format #f "theta(n) implementations of union-set "))
    (display
     (format #f "and~%"))
    (display
     (format #f "intersection-set for sets implemented "))
    (display
     (format #f "as (balanced)~%"))
    (display
     (format #f "binary trees.~%"))
    (newline)
    (display
     (format #f "The functions tree->list-2, "))
    (display
     (format #f "intersection-set, union-set,~%"))
    (display
     (format #f "and list->tree are all of order "))
    (display
     (format #f "theta(n), so their~%"))
    (display
     (format #f "composition is also order theta(n).~%"))
    (newline)
    (display
     (format #f "(define (intersection-tree "))
    (display
     (format #f "tree1 tree2)~%"))
    (display
     (format #f "  (list->tree~%"))
    (display
     (format #f "   (intersection-set~%"))
    (display
     (format #f "    (tree->list-2 tree1)~%"))
    (display
     (format #f "    (tree->list-2 tree2))))~%"))
    (newline)
    (display
     (format #f "(define (union-tree tree1 tree2)~%"))
    (display
     (format #f "  (list->tree~%"))
    (display
     (format #f "   (union-set~%"))
    (display
     (format #f "    (tree->list-2 tree1)~%"))
    (display
     (format #f "    (tree->list-2 tree2))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list (list 1 2 3) (list 1 2 3))
            (list (list 1 2 3) (list 1 2 4))
            )))
      (begin
        (for-each
         (lambda (tlist)
           (begin
             (let ((a1 (list-ref tlist 0))
                   (a2 (list-ref tlist 1)))
               (let ((tree-1 (list->tree a1))
                     (tree-2 (list->tree a2)))
                 (let ((in-tree
                        (intersection-tree tree-1 tree-2))
                       (un-tree
                        (union-tree tree-1 tree-2)))
                   (begin
                     (display
                      (format
                       #f "set1=~a, set2=~a, tree-1 "
                       a1 a2))
                     (display
                      (format
                       #f "intersect tree-2=~a~%"
                       in-tree))
                     (display
                      (format
                       #f "set1=~a, set2=~a, tree-1 "
                       a1 a2))
                     (display
                      (format
                       #f "union tree-2=~a~%"
                       un-tree))
                     (force-output)
                     ))
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.65 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
