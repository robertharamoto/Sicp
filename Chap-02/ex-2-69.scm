#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.69                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-leaf symbol weight)
  (begin
    (list 'leaf symbol weight)
    ))

;;;#############################################################
;;;#############################################################
(define (leaf? object)
  (begin
    (eq? (car object) 'leaf)
    ))

;;;#############################################################
;;;#############################################################
(define (symbol-leaf x)
  (begin
    (cadr x)
    ))

;;;#############################################################
;;;#############################################################
(define (weight-leaf x)
  (begin
    (caddr x)
    ))

;;;#############################################################
;;;#############################################################
(define (make-code-tree left right)
  (begin
    (list left
          right
          (append (symbols left) (symbols right))
          (+ (weight left) (weight right)))
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch tree)
  (begin
    (car tree)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch tree)
  (begin
    (cadr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (symbols tree)
  (begin
    (if (leaf? tree)
        (begin
          (list (symbol-leaf tree)))
        (begin
          (caddr tree)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight tree)
  (begin
    (if (leaf? tree)
        (begin
          (weight-leaf tree))
        (begin
          (cadddr tree)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (begin
      (if (null? bits)
          (begin
            (list))
          (begin
            (let ((next-branch
                   (choose-branch
                    (car bits) current-branch)))
              (begin
                (if (leaf? next-branch)
                    (begin
                      (cons (symbol-leaf next-branch)
                            (decode-1 (cdr bits) tree)))
                    (begin
                      (decode-1 (cdr bits) next-branch)
                      ))
                ))
            ))
      ))
  (begin
    (decode-1 bits tree)
    ))

;;;#############################################################
;;;#############################################################
(define (choose-branch bit branch)
  (begin
    (cond
     ((= bit 0)
      (begin
        (left-branch branch)
        ))
     ((= bit 1)
      (begin
        (right-branch branch)
        ))
     (else
      (begin
        (display (format #f "bad bit ~a -- CHOOSE-BRANCH~%" bit))
        (force-output)
        (quit)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-set x set)
  (begin
    (cond
     ((null? set)
      (begin
        (list x)
        ))
     ((< (weight x) (weight (car set)))
      (begin
        (cons x set)
        ))
     (else
      (begin
        (cons (car set)
              (adjoin-set x (cdr set)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (make-leaf-set pairs)
  (begin
    (if (null? pairs)
        (begin
          (list))
        (begin
          (let ((pair (car pairs)))
            (begin
              (adjoin-set (make-leaf (car pair)    ; symbol
                                     (cadr pair))  ; frequency
                          (make-leaf-set (cdr pairs)))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (encode message tree)
  (begin
    (if (null? message)
        (begin
          (list))
        (begin
          (append (encode-symbol (car message) tree)
                  (encode (cdr message) tree))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (encode-symbol asymbol enc-tree)
  (define (local-iter asymbol curr-tree acc-list)
    (begin
      (if (null? curr-tree)
          (begin
            (list #f acc-list))
          (begin
            (let ((found-flag #f)
                  (result-acc-list (list)))
              (begin
                ;;; handle left branch
                (let ((next-left-branch
                       (left-branch curr-tree))
                      (next-acc-list
                       (append acc-list (list 0))))
                  (begin
                    (if (leaf? next-left-branch)
                        (begin
                          (let ((this-symbol
                                 (symbol-leaf next-left-branch)))
                            (begin
                              (if (equal? asymbol this-symbol)
                                  (begin
                                    (set! found-flag #t)
                                    (set! result-acc-list next-acc-list)
                                    ))
                              )))
                        (begin
                          (let ((left-result
                                 (local-iter
                                  asymbol next-left-branch next-acc-list)))
                            (let ((lfound-flag (car left-result))
                                  (next-acc-list (cadr left-result)))
                              (begin
                                (if (equal? lfound-flag #t)
                                    (begin
                                      (set! found-flag #t)
                                      (set! result-acc-list next-acc-list)
                                      ))
                                )))
                          ))
                    ))
                ;;; handle right branch
                (if (equal? found-flag #f)
                    (begin
                      (let ((next-right-branch
                             (right-branch curr-tree))
                            (next-acc-list
                             (append acc-list (list 1))))
                        (begin
                          (if (leaf? next-right-branch)
                              (begin
                                (let ((this-symbol
                                       (symbol-leaf next-right-branch)))
                                  (begin
                                    (if (equal? asymbol this-symbol)
                                        (begin
                                          (set! found-flag #t)
                                          (set! result-acc-list next-acc-list)
                                          ))
                                    )))
                              (begin
                                (let ((right-result
                                       (local-iter
                                        asymbol next-right-branch next-acc-list)))
                                  (let ((rfound-flag (car right-result))
                                        (next-acc-list (cadr right-result)))
                                    (begin
                                      (if (equal? rfound-flag #t)
                                          (begin
                                            (set! found-flag #t)
                                            (set! result-acc-list next-acc-list)
                                            ))
                                      )))
                                ))
                          ))
                      ))

                (list found-flag result-acc-list)
                ))
            ))
      ))
  (begin
    (if (not (list? enc-tree))
        (begin
          (display
           (format
            #f "for symbol ~a, found invalid tree ~a~%"
            asymbol enc-tree))
          (display (format #f "quitting...~%"))
          (force-output)
          (quit)
          ))

    (let ((a-result (local-iter asymbol enc-tree (list))))
      (let ((fflag (car a-result))
            (acc-list (cadr a-result)))
        (begin
          (if (equal? fflag #t)
              (begin
                acc-list)
              (begin
                (display
                 (format
                  #f "encode-symbol error : unable to find symbol "))
                (display
                 (format
                  #f "~a in tree ~a~%" asymbol enc-tree))
                (force-output)
                #f
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (generate-huffman-tree pairs)
  (begin
    (successive-merge (make-leaf-set pairs))
    ))

;;;#############################################################
;;;#############################################################
(define (successive-merge ascending-weight-order-leaves)
  (define (local-iter leaves-list)
    (begin
      (cond
       ((null? leaves-list)
        (begin
          (list)
          ))
       ((= (length leaves-list) 1)
        (begin
          (car leaves-list)
          ))
       (else
        (begin
          (let ((left-leaf (car leaves-list))
                (right-leaf (car (cdr leaves-list)))
                (remaining-list (cdr (cdr leaves-list))))
            (let ((next-tree
                   (make-code-tree left-leaf right-leaf)))
              (let ((next-list
                     (adjoin-set next-tree remaining-list)))
                (begin
                  (local-iter next-list)
                  ))
              ))
          )))
      ))
  (begin
    (local-iter ascending-weight-order-leaves)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-generate-huffman-tree-1 result-hash-table)
 (begin
   (let ((sub-name "test-generate-huffman-tree-1")
         (test-list
          (list
           (list
            (list (list 'a 4) (list 'b 2) (list 'c 1) (list 'd 1))
            (list 'a 'b 'c 'd))
           (list
            (list
             (list 'a 8) (list 'e 5) (list 'b 3) (list 'q 2)
             (list 'c 1) (list 'd 1))
            (list 'a 'b 'a 'e 'c 'c 'd 'q 'q 'q))
           (list
            (list (list 1 5) (list 2 7) (list 3 10) (list 4 15)
                  (list 5 20) (list 6 45))
            (list 6 5 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((code-pairs (list-ref this-list 0))
                  (amessage (list-ref this-list 1)))
              (let ((code-tree
                     (generate-huffman-tree code-pairs)))
                (let ((encoded-message
                       (encode amessage code-tree)))
                  (let ((decoded-message
                         (decode encoded-message code-tree)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : "
                            sub-name test-label-index))
                          (err-2
                           (format
                            #f "code-pairs=~a, code-message=~a, "
                            code-pairs amessage))
                          (err-3
                           (format
                            #f "shouldbe=~a,  result=~a"
                            amessage decoded-message)))
                      (begin
                        (unittest2:assert?
                         (equal? amessage decoded-message)
                         sub-name
                         (string-append
                          err-1 err-2 err-3)
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The following procedure takes as its "))
    (display
     (format #f "argument a list~%"))
    (display
     (format #f "of symbol-frequency pairs (where no "))
    (display
     (format #f "symbol appears in~%"))
    (display
     (format #f "more than one pair) and generates a "))
    (display
     (format #f "Huffman encoding tree~%"))
    (display
     (format #f "according to the Huffman "))
    (display
     (format #f "algorithm.~%"))
    (newline)
    (display
     (format #f "(define (generate-huffman-tree pairs)~%"))
    (display
     (format #f "  (successive-merge (make-leaf-set "))
    (display
     (format #f "pairs)))~%"))
    (display
     (format #f "Make-leaf-set is the procedure given "))
    (display
     (format #f "above that~%"))
    (display
     (format #f "transforms the list of pairs into an "))
    (display
     (format #f "ordered set of~%"))
    (display
     (format #f "leaves. Successive-merge is the "))
    (display
     (format #f "procedure the~%"))
    (display
     (format #f "you must write, using make-code-tree to "))
    (display
     (format #f "successively~%"))
    (display
     (format #f "merge the smallest-weight elements of "))
    (display
     (format #f "the set until~%"))
    (display
     (format #f "there is only one element left, which "))
    (display
     (format #f "is the desired~%"))
    (display
     (format #f "Huffman tree. (This procedure is "))
    (display
     (format #f "slightly tricky,~%"))
    (display
     (format #f "but not really complicated. If you "))
    (display
     (format #f "find yourself~%"))
    (display
     (format #f "designing a complex procedure, then "))
    (display
     (format #f "you are almost~%"))
    (display
     (format #f "certainly doing something wrong. You "))
    (display
     (format #f "can take~%"))
    (display
     (format #f "significant advantage of the fact "))
    (display
     (format #f "that we are~%"))
    (display
     (format #f "using an ordered set "))
    (display
     (format #f "representation.)~%"))
    (newline)
    (display
     (format #f "For an introduction to Huffman "))
    (display
     (format #f "coding see:~%"))
    (display
     (format #f "https://www.programiz.com/dsa/huffman-coding"))
    (newline)
    (display
     (format #f "(define (successive-merge "))
    (display
     (format #f "ascending-weight-order-leaves)~%"))
    (display
     (format #f "  (define (local-iter leaves-list)~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (if (null? leaves-list)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (list))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((left-leaf (car "))
    (display
     (format #f "leaves-list))~%"))
    (display
     (format #f "                  (right "))
    (display
     (format #f "(local-iter (cdr leaves-list))))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (null? right)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      left-leaf)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (make-code-tree "))
    (display
     (format #f "left-leaf right)~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (let ((rev-list (reverse "))
    (display
     (format #f "ascending-weight-order-leaves)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (local-iter rev-list)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list
             (list (list 'a 4) (list 'b 2)
                   (list 'c 1) (list 'd 1))
             (list 'a 'b 'c 'd))
            (list
             (list
              (list 'a 8) (list 'e 5)
              (list 'b 3) (list 'q 2)
              (list 'c 1) (list 'd 1))
             (list 'a 'b 'a 'e 'c 'c 'd 'q 'q 'q))
            (list
             (list (list 1 5) (list 2 7)
                   (list 3 10) (list 4 15)
                   (list 5 20) (list 6 45))
             (list 6 5 4))
            )))
      (begin
        (for-each
         (lambda (tlist)
           (begin
             (let ((code-pairs (list-ref tlist 0))
                   (message (list-ref tlist 1)))
               (let ((code-tree
                      (generate-huffman-tree
                       code-pairs)))
                 (let ((encoded-message
                        (encode
                         message code-tree)))
                   (let ((decoded-message
                          (decode encoded-message
                                  code-tree)))
                     (begin
                       (display
                        (format #f "code pairs = ~a~%"
                                code-pairs))
                       (display
                        (format #f "code tree = ~a~%"
                                code-tree))
                       (display
                        (format #f "message = ~a~%"
                                message))
                       (display
                        (format #f "encoded message = ~a~%"
                                encoded-message))
                       (display
                        (format #f "decoded message = ~a~%"
                                decoded-message))
                       (force-output)
                       ))
                   )))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.69 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
