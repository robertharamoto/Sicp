#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.34                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (horner-eval x coefficient-sequence)
  (begin
    (accumulate
     (lambda (this-coeff higher-terms)
       (+ this-coeff (* higher-terms x)))
     0 coefficient-sequence)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-horner-eval-1 result-hash-table)
 (begin
   (let ((sub-name "test-horner-eval-1")
         (test-list
          (list
           (list 0 (list 1 2 3) 1)
           (list 1 (list 1 2 3) 6)
           (list 2 (list 1 2 3) 17)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (coeff (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (horner-eval xx coeff)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, coeff=~a, "
                        sub-name test-label-index
                        xx coeff))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Evaluating a polynomial in x at a "))
    (display
     (format #f "given value of x~%"))
    (display
     (format #f "can be formulated as an accumulation. "))
    (display
     (format #f "We evaluate the~%"))
    (display
     (format #f "polynomial a_n*x^n + a_(n-1)*x^(n-1) "))
    (display
     (format #f "+...+ a_1*x + a_0~%"))
    (display
     (format #f "using a well-known algorithm called "))
    (display
     (format #f "Horner's rule, which~%"))
    (display
     (format #f "structures the computation as~%"))
    (display
     (format #f "(...(a_n*x + a_(n-1))*x "))
    (display
     (format #f "+ ... + a_1)*x + a_0~%"))
    (display
     (format #f "In other words, we start with a_n, "))
    (display
     (format #f "multiply by x, add~%"))
    (display
     (format #f "a_(n-1), multiply by x, and so on, "))
    (display
     (format #f "until we reach a_0.~%"))
    (display
     (format #f "Fill in the following template to "))
    (display
     (format #f "produce a procedure~%"))
    (display
     (format #f "that evaluates a polynomial using "))
    (display
     (format #f "Horner's rule.~%"))
    (display
     (format #f "Assume that the coefficients of the "))
    (display
     (format #f "polynomial are~%"))
    (display
     (format #f "arranged in a sequence, from a_0 "))
    (display
     (format #f "through a_n.~%"))
    (display
     (format #f "(define (horner-eval x "))
    (display
     (format #f "coefficient-sequence)~%"))
    (display
     (format #f "  (accumulate (lambda "))
    (display
     (format #f "(this-coeff higher-terms) <??>)~%"))
    (display
     (format #f "              0~%"))
    (display
     (format #f "              coefficient-sequence))~%"))
    (display
     (format #f "For example, to compute "))
    (display
     (format #f "1 + 3x + 5x^3 + x^5~%"))
    (display
     (format #f "at x = 2, you would evaluate~%"))
    (display
     (format #f "(horner-eval 2 (list 1 3 0 5 0 1))~%"))
    (newline)
    (display
     (format #f "(define (horner-eval x "))
    (display
     (format #f "coefficient-sequence)~%"))
    (display
     (format #f "  (accumulate~%"))
    (display
     (format #f "   (lambda (this-coeff higher-terms)~%"))
    (display
     (format #f "     (+ this-coeff (* higher-terms x)))~%"))
    (display
     (format #f "   0 coefficient-sequence))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((alist (list 1 3 0 5 0 1)))
      (begin
        (display
         (format #f "(horner-eval 2 ~a) = ~a~%"
                 alist (horner-eval 2 alist)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.34 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
