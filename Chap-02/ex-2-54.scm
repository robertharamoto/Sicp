#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.54                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-to-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-equal? item1 item2)
  (begin
    (cond
     ((and (not (list? item1))
           (not (list? item2)))
      (begin
        (eq? item1 item2)
        ))
     ((and (null? item1)
           (null? item2))
      (begin
        #t
        ))
     ((or (and (null? item1)
               (not (null? item2)))
          (and (not (null? item1))
               (null? item2)))
      (begin
        #f
        ))
     ((and (list? item1)
           (list? item2))
      (begin
        (let ((a1 (car item1))
              (tail-1 (cdr item1))
              (a2 (car item2))
              (tail-2 (cdr item2)))
          (begin
            (and (my-equal? a1 a2)
                 (my-equal? tail-1 tail-2))
            ))
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-my-equal-1 result-hash-table)
 (begin
   (let ((sub-name "test-my-equal-1")
         (test-list
          (list
           (list 'a 'a #t) (list 'a 'b #f)
           (list '(a) '(a) #t) (list '(a) '(b) #f)
           (list '(a b) '(a b) #t) (list '(a b) '(b c) #f)
           (list '(1 2 3) '(1 2 3) #t) (list '(1 (2) 3) '(1 2 3) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-elem (list-ref this-list 0))
                  (b-elem (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (my-equal? a-elem b-elem)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "a-elem=~a, b-elem=~a, "
                        a-elem b-elem))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Two lists are said to be equal? if "))
    (display
     (format #f "they contain equal~%"))
    (display
     (format #f "elements arranged in the same order. "))
    (display
     (format #f "For example,~%"))
    (display
     (format #f "(equal? '(this is a list) "))
    (display
     (format #f "'(this is a list))~%"))
    (display
     (format #f "is true, but:~%"))
    (display
     (format #f "(equal? '(this is a list) "))
    (display
     (format #f "'(this (is a) list))~%"))
    (display
     (format #f "is false. To be more precise, we can "))
    (display
     (format #f "define equal?~%"))
    (display
     (format #f "recursively in terms of the basic eq? "))
    (display
     (format #f "equality of symbols~%"))
    (display
     (format #f "by saying that a and b are equal? if "))
    (display
     (format #f "they are both symbols~%"))
    (display
     (format #f "and the symbols are eq?, or if they "))
    (display
     (format #f "are both lists~%"))
    (display
     (format #f "such that (car a) is equal? to (car b) "))
    (display
     (format #f "and (cdr a) is~%"))
    (display
     (format #f "equal? to (cdr b). Using this idea, "))
    (display
     (format #f "implement equal? as~%"))
    (display
     (format #f "a procedure.~%"))
    (newline)
    (display
     (format #f "(define (my-equal? item1 item2)~%"))
    (display
     (format #f "  (cond~%"))
    (display
     (format #f "   ((and (not (list? item1))~%"))
    (display
     (format #f "         (not (list? item2)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (eq? item1 item2)~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "   ((and (null? item1)~%"))
    (display
     (format #f "         (null? item2))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      #t~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "   ((or (and (null? item1)~%"))
    (display
     (format #f "             (not (null? item2)))~%"))
    (display
     (format #f "        (and (not (null? item1))~%"))
    (display
     (format #f "             (null? item2)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      #f~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "   ((and (list? item1)~%"))
    (display
     (format #f "         (list? item2))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (let ((a1 (car item1))~%"))
    (display
     (format #f "            (tail-1 (cdr item1))~%"))
    (display
     (format #f "            (a2 (car item2))~%"))
    (display
     (format #f "            (tail-2 (cdr item2)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (and (my-equal? a1 a2)~%"))
    (display
     (format #f "               (my-equal? tail-1 tail-2))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "   (else~%"))
    (display
     (format #f "    #f~%"))
    (display
     (format #f "    )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "(my-equal? '(this is a list) "))
    (display
     (format #f "'(this is a list)) = ~a~%"
             (my-equal? '(this is a list)
                        '(this is a list))))
    (display
     (format #f "(equal? '(this is a list) "))
    (display
     (format #f "'(this (is a) list)) = ~a~%"
             (equal? '(this is a list)
                     '(this (is a) list))))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.54 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "scheme test~%"))
          (main-loop)
          (newline)

          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
