#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.62                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-1
             (format
              #f ", shouldbe=~a, result=~a"
              shouldbe-list result-list))
            (err-msg-2
             (format
              #f ", length shouldbe=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-1 err-msg-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((sflag (member s-elem result-list))
                      (err-msg-3
                       (format
                        #f ", missing element ~a"
                        s-elem)))
                  (begin
                    (unittest2:assert?
                     (not (equal? sflag #f))
                     sub-name
                     (string-append err-start err-msg-1 err-msg-3)
                     result-hash-table)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; ascending order, numeric set
(define (union-set set-1 set-2)
  (define (local-iter set-a set-b acc-set)
    (begin
      (cond
       ((null? set-a)
        (begin
          (append set-b acc-set)
          ))
       ((null? set-b)
        (begin
          (append set-a acc-set)
          ))
       (else
        (begin
          (let ((a-elem (car set-a))
                (b-elem (car set-b)))
            (begin
              (cond
               ((= a-elem b-elem)
                (begin
                  (local-iter
                   (cdr set-a) (cdr set-b) (cons a-elem acc-set))
                  ))
               ((< a-elem b-elem)
                (begin
                  (local-iter
                   (cdr set-a) set-b (cons a-elem acc-set))
                  ))
               (else
                (begin
                  (local-iter
                   set-a (cdr set-b) (cons b-elem acc-set))
                  )))
              ))
          )))
      ))
  (begin
    (reverse (local-iter set-1 set-2 (list)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-union-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-union-set-1")
         (test-list
          (list
           (list (list 1 3 4) (list 2 3 4) (list 1 2 3 4))
           (list (list 2 5) (list 2 3 4) (list 2 3 4 5))
           (list (list 7 9) (list 2 3 4) (list 2 3 4 7 9))
           (list (list 1) (list 2 3 4) (list 1 2 3 4))
           (list (list 7 11) (list 2 4 10 20) (list 2 4 7 10 11 20))
           (list (list 2 4 10 20) (list 7 11) (list 2 4 7 10 11 20))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-1 (list-ref this-list 0))
                  (set-2 (list-ref this-list 1))
                  (shouldbe-set (list-ref this-list 2)))
              (let ((result-set (union-set set-1 set-2)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : set-1=~a, set-2=~a, "
                        sub-name test-label-index
                        set-1 set-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-set result-set
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Give a theta(n) implementation of "))
    (display
     (format #f "union-set for sets~%"))
    (display
     (format #f "represented as ordered lists.~%"))
    (newline)
    (display
     (format #f "(define (union-set set-1 set-2)~%"))
    (display
     (format #f "  (define (local-iter "))
    (display
     (format #f "set-a set-b acc-set)~%"))
    (display
     (format #f "    (cond~%"))
    (display
     (format #f "     ((null? set-a)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (append set-b acc-set)~%"))
    (display
     (format #f "     ((null? set-b)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (append set-a acc-set)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     (else~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (let ((a-elem (car set-a))~%"))
    (display
     (format #f "              (b-elem (car set-b)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (cond~%"))
    (display
     (format #f "             ((= a-elem b-elem)~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (local-iter "))
    (display
     (format #f "(cdr set-a) (cdr set-b)~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "(cons a-elem acc-set))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "             ((< a-elem b-elem)~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (local-iter "))
    (display
     (format #f "(cdr set-a) set-b (cons a-elem acc-set))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "             (else~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (local-iter set-a "))
    (display
     (format #f "(cdr set-b) (cons b-elem acc-set))~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     ))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (reverse "))
    (display
     (format #f "(local-iter set-1 set-2 (list)))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list (list 1 3) (list 2 3 4))
            (list (list 2 5) (list 2 3 4))
            (list (list 7 11) (list 2 4 10 20))
            (list (list 2 4 10 20) (list 7 11))
            )))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((set-1 (list-ref a-list 0))
                   (set-2 (list-ref a-list 1)))
               (let ((result (union-set set-1 set-2)))
                 (begin
                   (display
                    (format
                     #f "set-1 = ~a, set-2 = ~a, "
                     set-1 set-2))
                   (display
                    (format #f "result = ~a~%"
                            result))
                   (force-output)
                   )))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.62 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
