#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.67                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-leaf symbol weight)
  (begin
    (list 'leaf symbol weight)
    ))

;;;#############################################################
;;;#############################################################
(define (leaf? object)
  (begin
    (eq? (car object) 'leaf)
    ))

;;;#############################################################
;;;#############################################################
(define (symbol-leaf x)
  (begin
    (cadr x)
    ))

;;;#############################################################
;;;#############################################################
(define (weight-leaf x)
  (begin
    (caddr x)
    ))

;;;#############################################################
;;;#############################################################
(define (make-code-tree left right)
  (begin
    (list left
          right
          (append (symbols left) (symbols right))
          (+ (weight left) (weight right)))
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch tree)
  (begin
    (car tree)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch tree)
  (begin
    (cadr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (symbols tree)
  (begin
    (if (leaf? tree)
        (begin
          (list (symbol-leaf tree)))
        (begin
          (caddr tree)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight tree)
  (begin
    (if (leaf? tree)
        (begin
          (weight-leaf tree))
        (begin
          (cadddr tree)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (begin
      (if (null? bits)
          (begin
            (list))
          (begin
            (let ((next-branch
                   (choose-branch (car bits) current-branch)))
              (begin
                (if (leaf? next-branch)
                    (begin
                      (cons (symbol-leaf next-branch)
                            (decode-1 (cdr bits) tree)))
                    (begin
                      (decode-1 (cdr bits) next-branch)
                      ))
                ))
            ))
      ))
  (begin
    (decode-1 bits tree)
    ))

;;;#############################################################
;;;#############################################################
(define (choose-branch bit branch)
  (begin
    (cond
     ((= bit 0)
      (begin
        (left-branch branch)
        ))
     ((= bit 1)
      (begin
        (right-branch branch)
        ))
     (else
      (begin
        (display
         (format #f "bad bit ~a -- CHOOSE-BRANCH~%" bit))
        (force-output)
        (quit)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-set x set)
  (begin
    (cond
     ((null? set)
      (begin
        (list x)
        ))
     ((< (weight x) (weight (car set)))
      (begin
        (cons x set)
        ))
     (else
      (begin
        (cons (car set)
              (adjoin-set x (cdr set)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (make-leaf-set pairs)
  (begin
    (if (null? pairs)
        (begin
          (list))
        (begin
          (let ((pair (car pairs)))
            (begin
              (adjoin-set (make-leaf (car pair)    ; symbol
                                     (cadr pair))  ; frequency
                          (make-leaf-set (cdr pairs)))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lookup-tree-1 result-hash-table)
 (begin
   (let ((sub-name "test-lookup-tree-1")
         (test-list
          (list
           (list
            (make-code-tree
             (make-code-tree
              (make-leaf 'a 8)
              (make-leaf 'e 5))
             (make-code-tree
              (make-code-tree
               (make-leaf 'b 3)
               (make-leaf 'q 2))
              (make-code-tree
               (make-leaf 'd 1)
               (make-leaf 'c 1))))
            (list 0 1 0 0 1 0 0 0 1 1 1 1 0)
            (list 'e 'a 'b 'e 'c)
            )))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-code-tree (list-ref this-list 0))
                  (acode (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (decode acode a-code-tree)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : a-code-tree=~a, "
                        sub-name test-label-index a-code-tree))
                      (err-msg-2
                       (format
                        #f "acode=~a, shouldbe=~a,  result=~a"
                        acode shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define an encoding tree and a sample "))
    (display
     (format #f "message:~%"))
    (newline)
    (display
     (format #f "(define sample-tree~%"))
    (display
     (format #f "  (make-code-tree "))
    (display
     (format #f "(make-leaf 'A 4)~%"))
    (display
     (format #f "                  (make-code-tree~%"))
    (display
     (format #f "                   (make-leaf 'B 2)~%"))
    (display
     (format #f "                   (make-code-tree "))
    (display
     (format #f "(make-leaf 'D 1)~%"))
    (display
     (format #f "                           "))
    (display
     (format #f "        (make-leaf 'C 1)))))~%"))
    (display
     (format #f "(define sample-message "))
    (display
     (format #f "'(0 1 1 0 0 1 0 1 0 1 1 1 0))~%"))
    (newline)
    (display
     (format #f "Use the decode procedure to decode "))
    (display
     (format #f "the message, and~%"))
    (display
     (format #f "give the result.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((code-tree
           (make-code-tree
            (make-code-tree
             (make-leaf 'a 8)
             (make-leaf 'e 5))
            (make-code-tree
             (make-code-tree
              (make-leaf 'b 3)
              (make-leaf 'q 2))
             (make-code-tree
              (make-leaf 'd 1)
              (make-leaf 'c 1)))))
          (coded-message
           (list 0 1 0 0 1 0 0 0 1 1 1 1 0)))
      (let ((decoded-message
             (decode coded-message code-tree)))
        (begin
          (display
           (format #f "code tree = ~a~%"
                   code-tree))
          (display
           (format #f "coded message = ~a~%"
                   coded-message))
          (display
           (format #f "decoded message = ~a~%"
                   decoded-message))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.67 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
