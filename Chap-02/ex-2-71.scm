#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.71                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose we have a Huffman tree for "))
    (display
     (format #f "an alphabet of n~%"))
    (display
     (format #f "symbols, and that the relative frequencies "))
    (display
     (format #f "of the~%"))
    (display
     (format #f "symbols are:~%"))
    (display
     (format #f "    1, 2, 4, ..., 2^(n-1)~%"))
    (display
     (format #f "Sketch the tree for n=5; for n=10. "))
    (display
     (format #f "In such a tree (for~%"))
    (display
     (format #f "general n) how many bits are required "))
    (display
     (format #f "to encode the most~%"))
    (display
     (format #f "frequent symbol? the least "))
    (display
     (format #f "frequent symbol?~%"))
    (display
     (format #f "((a b c d e) 31)~%"))
    (display
     (format #f "(e 16) ((a b c d) 15)~%"))
    (display
     (format #f "        (d 8)   ((a b c) 7)~%"))
    (display
     (format #f "                 (c 4)  ((a b 3))~%"))
    (display
     (format #f "                         (b 2) (a 1)~%"))
    (newline)
    (display
     (format #f "((1 2 3 4 5 6 7 8 9 10) 1023)~%"))
    (display
     (format #f " (10 512)   ((1 2 3 4 5 6 7 8 9) 511)~%"))
    (display
     (format #f "             (9 256) "))
    (display
     (format #f "((1 2 3 4 5 6 7 8) 255)~%"))
    (display
     (format #f "                 ...~%"))
    (display
     (format #f "                      (2 2) (1 1)~%"))
    (newline)
    (display
     (format #f "For a general n, the maximum depth "))
    (display
     (format #f "of the tree is n.~%"))
    (display
     (format #f "The number of bits to encode the "))
    (display
     (format #f "most frequent symbol~%"))
    (display
     (format #f "is 1.  The number of bits to encode "))
    (display
     (format #f "the least frequent~%"))
    (display
     (format #f "symbol is n-1.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.71 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
