#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.81                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
        (begin
          (if proc
              (begin
                (apply proc (map contents args)))
              (begin
                (if (= (length args) 2)
                    (begin
                      (let ((type1 (car type-tags))
                            (type2 (cadr type-tags))
                            (a1 (car args))
                            (a2 (cadr args)))
                        (let ((t1->t2 (get-coercion type1 type2))
                              (t2->t1 (get-coercion type2 type1)))
                          (begin
                            (cond
                             ((and t1->t2
                                   (not (eq? type1 type2)))
                              (begin
                                (apply-generic op (t1->t2 a1) a2)
                                ))
                             ((and t2->t1
                                   (not (eq? type1 type2)))
                              (begin
                                (apply-generic op a1 (t2->t1 a2))
                                ))
                             (else
                              (begin
                                (error "No method for these types"
                                       (list op type-tags))
                                )))
                            ))
                        ))
                    (begin
                      (error "No method for these types"
                             (list op type-tags))
                      ))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner has noticed that "))
    (display
     (format #f "apply-generic may try~%"))
    (display
     (format #f "to coerce the arguments to each "))
    (display
     (format #f "other's type even if~%"))
    (display
     (format #f "they already have the same type. "))
    (display
     (format #f "Therefore, he reasons, we~%"))
    (display
     (format #f "need to put procedures in the coercion "))
    (display
     (format #f "table to \"coerce\"~%"))
    (display
     (format #f "arguments of each type to their "))
    (display
     (format #f "own type. For example,~%"))
    (display
     (format #f "in addition to the "))
    (display
     (format #f "scheme-number->complex coercion shown~%"))
    (display
     (format #f "above, he would do:~%"))
    (display
     (format #f "(define "))
    (display
     (format #f "(scheme-number->scheme-number n) n)~%"))
    (display
     (format #f "(define (complex->complex z) z)~%"))
    (display
     (format #f "(put-coercion 'scheme-number "))
    (display
     (format #f "'scheme-number~%"))
    (display
     (format #f "              "))
    (display
     (format #f "scheme-number->scheme-number)~%"))
    (display
     (format #f "(put-coercion 'complex "))
    (display
     (format #f "'complex complex->complex)~%"))
    (display
     (format #f "a. With Louis's coercion procedures "))
    (display
     (format #f "installed, what happens~%"))
    (display
     (format #f "if apply-generic is called with two "))
    (display
     (format #f "arguments of type~%"))
    (display
     (format #f "scheme-number or two arguments of type "))
    (display
     (format #f "complex for an ~%"))
    (display
     (format #f "operation that is not found in the "))
    (display
     (format #f "table for those~%"))
    (display
     (format #f "types? For example, assume that we've "))
    (display
     (format #f "defined a generic~%"))
    (display
     (format #f "exponentiation operation:~%"))
    (display
     (format #f "(define (exp x y) (apply-generic "))
    (display
     (format #f "'exp x y))~%"))
    (display
     (format #f "and have put a procedure for "))
    (display
     (format #f "exponentiation in the~%"))
    (display
     (format #f "Scheme-number package but not in "))
    (display
     (format #f "any other package:~%"))
    (display
     (format #f ";;; following added to "))
    (display
     (format #f "Scheme-number package~%"))
    (display
     (format #f "(put 'exp '(scheme-number "))
    (display
     (format #f "scheme-number)~%"))
    (display
     (format #f "     (lambda (x y) "))
    (display
     (format #f "(tag (expt x y)))) "))
    (display
     (format #f ";;; using primitive expt~%"))
    (display
     (format #f "What happens if we call exp with "))
    (display
     (format #f "two complex numbers as~%"))
    (display
     (format #f "arguments?~%"))
    (display
     (format #f "b. Is Louis correct that something "))
    (display
     (format #f "had to be done about~%"))
    (display
     (format #f "coercion with arguments of the same "))
    (display
     (format #f "type, or does~%"))
    (display
     (format #f "apply-generic work correctly "))
    (display
     (format #f "as is?~%"))
    (display
     (format #f "c. Modify apply-generic so that it "))
    (display
     (format #f "doesn't try coercion if~%"))
    (display
     (format #f "the two arguments have the same type.~%"))
    (newline)
    (display
     (format #f "(a) if we call exp with two complex "))
    (display
     (format #f "numbers as arguments,~%"))
    (display
     (format #f "then apply-generic will not find "))
    (display
     (format #f "the proc, and since the length~%"))
    (display
     (format #f "of the args is 2, it will try to "))
    (display
     (format #f "coerce the values.~%"))
    (display
     (format #f "However, since both arguments are complex, "))
    (display
     (format #f "t1->t2 will be the same~%"))
    (display
     (format #f "as t2->t1.  Since Louis Reasoner installed "))
    (display
     (format #f "the complex to complex~%"))
    (display
     (format #f "coercion, t1->t2 will not be false, so it "))
    (display
     (format #f "will recursively~%"))
    (display
     (format #f "apply-generic again, and will "))
    (display
     (format #f "enter an infinite loop.~%"))
    (newline)
    (display
     (format #f "(b) Louis Reasoner is not correct, "))
    (display
     (format #f "the apply-generic method~%"))
    (display
     (format #f "works correctly as is.~%"))
    (newline)
    (display
     (format #f "(c)~%"))
    (display
     (format #f "(define (apply-generic op . args)~%"))
    (display
     (format #f "  (let ((type-tags (map "))
    (display
     (format #f "type-tag args)))~%"))
    (display
     (format #f "    (let ((proc (get op type-tags)))~%"))
    (display
     (format #f "      (if proc~%"))
    (display
     (format #f "          (apply proc (map "))
    (display
     (format #f "contents args))~%"))
    (display
     (format #f "          (if (= (length args) 2)~%"))
    (display
     (format #f "              (let ((type1 (car "))
    (display
     (format #f "type-tags))~%"))
    (display
     (format #f "                    (type2 (cadr "))
    (display
     (format #f "type-tags))~%"))
    (display
     (format #f "                    (a1 (car args))~%"))
    (display
     (format #f "                    (a2 (cadr args)))~%"))
    (display
     (format #f "                (let ((t1->t2 (get-coercion "))
    (display
     (format #f "type1 type2))~%"))
    (display
     (format #f "                      (t2->t1 (get-coercion "))
    (display
     (format #f "type2 type1)))~%"))
    (display
     (format #f "                  (cond ((and t1->t2~%"))
    (display
     (format #f "                              (not (eq? "))
    (display
     (format #f "type1 type2)))~%"))
    (display
     (format #f "                         (apply-generic op "))
    (display
     (format #f "(t1->t2 a1) a2))~%"))
    (display
     (format #f "                        ((and t2->t1~%"))
    (display
     (format #f "                              (not (eq? "))
    (display
     (format #f "type1 type2)))~%"))
    (display
     (format #f "                         (apply-generic op "))
    (display
     (format #f "a1 (t2->t1 a2)))~%"))
    (display
     (format #f "                        (else~%"))
    (display
     (format #f "                         (error "))
    (display
     (format #f "\"No method for these types\"~%"))
    (display
     (format #f "                                (list "))
    (display
     (format #f "op type-tags))))))~%"))
    (display
     (format #f "              (error \"No "))
    (display
     (format #f "method for these types\"~%"))
    (display
     (format #f "                     (list op "))
    (display
     (format #f "type-tags)))))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.81 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
