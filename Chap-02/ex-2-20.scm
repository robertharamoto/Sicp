#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.20                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (same-parity xx . alist)
  (define (local-iter xx-even-parity rem-list acc-list)
    (begin
      (if (null? rem-list)
          (begin
            acc-list)
          (begin
            (let ((yy (car rem-list))
                  (tail-list (cdr rem-list)))
              (begin
                (if (and (list? yy) (null? tail-list))
                    (begin
                      (let ((atmp (cdr yy)))
                        (begin
                          (set! yy (car yy))
                          (set! tail-list atmp)
                          ))
                      ))
                (if (or (and
                         (equal? xx-even-parity #t)
                         (even? yy))
                        (and
                         (equal? xx-even-parity #f)
                         (odd? yy)))
                    (begin
                      (local-iter
                       xx-even-parity tail-list
                       (cons yy acc-list)))
                    (begin
                      (local-iter
                       xx-even-parity tail-list acc-list)
                      ))
                ))
            ))
      ))
  (begin
    (let ((xx-even-parity (even? xx)))
      (begin
        (reverse
         (local-iter
          xx-even-parity alist (list xx)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-same-parity-1 result-hash-table)
 (begin
   (let ((sub-name "test-same-parity-1")
         (test-list
          (list
           (list (list 1 2 3 4 5) (list 1 3 5))
           (list (list 2 3 4 5 6) (list 2 4 6))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (same-parity (car alist) (cdr alist))))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Use this notation to write a procedure "))
    (display
     (format #f "same-parity that takes~%"))
    (display
     (format #f "one or more integers and returns a list "))
    (display
     (format #f "of all the~%"))
    (display
     (format #f "arguments that have the same even-odd "))
    (display
     (format #f "parity as the~%"))
    (display
     (format #f "first argument. For example,~%"))
    (newline)
    (display
     (format #f "(same-parity 1 2 3 4 5 6 7)~%"))
    (display
     (format #f "(1 3 5 7)~%"))
    (display
     (format #f "(same-parity 2 3 4 5 6 7)~%"))
    (display
     (format #f "(2 4 6)~%"))
    (newline)
    (display
     (format #f "(define (same-parity xx . alist)~%"))
    (display
     (format #f "  (define (local-iter xx-even-parity "))
    (display
     (format #f "rem-list acc-list)~%"))
    (display
     (format #f "    (if (null? rem-list)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          acc-list)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((yy (car rem-list))~%"))
    (display
     (format #f "                (tail-list (cdr rem-list)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (if (or (and~%"))
    (display
     (format #f "                       (equal? "))
    (display
     (format #f "xx-even-parity #t)~%"))
    (display
     (format #f "                       (even? yy))~%"))
    (display
     (format #f "                      (and~%"))
    (display
     (format #f "                       (equal? "))
    (display
     (format #f "xx-even-parity #f)~%"))
    (display
     (format #f "                       (odd? yy)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (local-iter~%"))
    (display
     (format #f "                     xx-even-parity "))
    (display
     (format #f "tail-list~%"))
    (display
     (format #f "                     (cons yy acc-list)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (local-iter~%"))
    (display
     (format #f "                     xx-even-parity "))
    (display
     (format #f "tail-list acc-list)~%"))
    (display
     (format #f "                    ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (let ((xx-even-parity (even? xx)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (reverse~%"))
    (display
     (format #f "          (local-iter~%"))
    (display
     (format #f "            xx-even-parity "))
    (display
     (format #f "alist (list xx)))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format
      #f "(same-parity 1 2 3 4 5 6 7) = ~a~%"
      (same-parity 1 2 3 4 5 6 7)))
    (display
     (format
      #f "(same-parity 2 3 4 5 6 7) = ~a~%"
      (same-parity 2 3 4 5 6 7)))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.20 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "scheme test~%"))

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
