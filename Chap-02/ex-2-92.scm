#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.92                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define the-empty-termlist (list))

;;;#############################################################
;;;#############################################################
(define (empty-termlist? term-list)
  (begin
    (null? term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (first-term term-list)
  (begin
    (car term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (rest-terms term-list)
  (begin
    (cdr term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (order term)
  (begin
    (car term)
    ))

;;;#############################################################
;;;#############################################################
(define (coeff term)
  (begin
    (cadr term)
    ))

;;;#############################################################
;;;#############################################################
(define div /)

;;;#############################################################
;;;#############################################################
(define (make-term order coeff)
  (begin
    (list order coeff)
    ))

;;;#############################################################
;;;#############################################################
(define (=zero? anum)
  (begin
    (cond
     ((integer? anum)
      (begin
        (zero? anum)
        ))
     ((real? anum)
      (begin
        (< (abs anum) 1e-12)
        ))
     ((rational? anum)
      (begin
        (< (abs (exact->inexact anum) 1e-12))
        ))
     ((and (pair? anum)
           (symbol? (variable anum)))
      (begin
        (null? (term-list anum))
        ))
     ((and (pair? anum)
           (not (null? anum)))
      (begin
        #f
        ))
     (else
      (begin
        (= anum 0)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-term term term-list)
  (begin
    (if (=zero? (coeff term))
        (begin
          term-list)
        (begin
          (cons term term-list)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (variable apoly)
  (begin
    (car apoly)
    ))

;;;#############################################################
;;;#############################################################
(define (term-list apoly)
  (begin
    (cdr apoly)
    ))

;;;#############################################################
;;;#############################################################
(define (make-polynomial var term-list)
  (begin
    (cons var term-list)
    ))

;;;#############################################################
;;;#############################################################
;;; each term-list is of the form (list 'x (list order coeff)...)
;;; each modified term is of the form
;;; (list 'x order (list 'y (list order coeff)... )
;;; so a modified term list is of the form
;;; (list (list 'x order coeff) (list 'y order coeff))
(define (polynomial-to-modified-term-list apoly)
  (begin
    (let ((acc-list (list))
          (var-1 (variable apoly))
          (tl-1 (term-list apoly)))
      (let ((tl-len (length tl-1)))
        (begin
          (for-each
           (lambda (aterm)
             (begin
               (if (and (pair? aterm) (pair? (coeff aterm))
                        (symbol? (variable (coeff aterm))))
                   (begin
                     (let ((oterm (order aterm))
                           (coeff-mterm-list
                            (polynomial-to-modified-term-list
                             (coeff aterm))))
                       (let ((next-list
                              (map
                               (lambda (bterm)
                                 (begin
                                   (cons (list var-1 oterm #f)
                                         bterm)
                                   )) coeff-mterm-list)))
                         (begin
                           (set! acc-list
                                 (append acc-list next-list))
                           ))
                       ))
                   (begin
                     (let ((next-term (cons var-1 aterm)))
                       (begin
                         (set! acc-list
                               (cons (list next-term) acc-list))
                         ))
                     ))
               )) tl-1)

          acc-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-3-list-lists-are-equal?
         slist-list rlist-list
         sub-name error-message
         result-hash-table)
  (begin
    (let ((slist-1 (car slist-list))
          (stail-1 (cdr slist-list))
          (rlist-1 (car rlist-list))
          (rtail-1 (cdr rlist-list)))
      (begin
        (if (not (list? slist-1))
            (begin
              (let ((slen (length slist-list))
                    (rlen (length rlist-list)))
                (begin
                  (do ((ii 0 (1+ ii)))
                      ((>= ii slen))
                    (begin
                      (let ((slist (list-ref slist-list ii))
                            (rlist (list-ref rlist-list ii)))
                        (let ((err-msg-1
                               (format #f ", comparing item ~a"
                                       slist)))
                          (begin
                            (if (or (symbol? slist)
                                    (equal? slist #f)
                                    (number? slist))
                                (begin
                                  (unittest2:assert?
                                   (equal? slist rlist)
                                   sub-name
                                   (string-append error-message err-msg-1)
                                   result-hash-table)
                                  ))
                            )))
                      ))
                  )))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii (length slist-list)))
                (begin
                  (let ((slist (list-ref slist-list ii))
                        (rlist (list-ref rlist-list ii)))
                    (begin
                      (if (list? slist)
                          (begin
                            (assert-3-list-lists-are-equal?
                             slist rlist
                             sub-name
                             error-message
                             result-hash-table))
                          (begin
                            (let ((err-msg-1
                                   (format
                                    #f ", comparing item ~a"
                                    slist)))
                              (begin
                                (if (or (symbol? slist)
                                        (equal? slist #f)
                                        (number? slist))
                                    (begin
                                      (unittest2:assert?
                                       (equal? slist rlist)
                                       sub-name
                                       (string-append
                                        error-message err-msg-1)
                                       result-hash-table)
                                      ))
                                ))
                            ))
                      ))
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-polynomial-to-modified-term-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-polynomial-to-modified-term-list-1")
         (test-list
          (list
           (list (make-polynomial
                  'x (list (list 2 1) (list 0 -1)))
                 (list
                  (list (list 'x 0 -1))
                  (list (list 'x 2 1))))
           (list (make-polynomial
                  'x
                  (list (list 0 -1)
                        (list 2 (make-polynomial
                                 'y (list
                                     (list 0 3)
                                     (list 1 2)
                                     (list 2 5))))))
                 (list (list (list 'x 0 -1))
                       (list (list 'x 2 #f) (list 'y 2 5))
                       (list (list 'x 2 #f) (list 'y 1 2))
                       (list (list 'x 2 #f) (list 'y 0 3))
                       ))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((apoly (list-ref this-list 0))
                  (shouldbe-list-list
                   (list-ref this-list 1)))
              (let ((result-list-list
                     (polynomial-to-modified-term-list
                      apoly)))
                (let ((slen
                       (length shouldbe-list-list))
                      (rlen
                       (length result-list-list)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : apoly=~a, "
                          sub-name test-label-index
                          apoly))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-list-list result-list-list))
                        (err-msg-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2 err-msg-3)
                       result-hash-table)

                      (assert-3-list-lists-are-equal?
                       shouldbe-list-list result-list-list
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sort-mod-terms-by-symbols mod-terms-list)
  (begin
    (stable-sort
     mod-terms-list
     (lambda (a b)
       (begin
         (let ((sa (symbol->string (car a)))
               (sb (symbol->string (car b))))
           (begin
             (if (string-ci<? sa sb)
                 (begin
                   #t)
                 (begin
                   (if (string-ci=? sa sb)
                       (begin
                         (> (order (cdr a))
                            (order (cdr b))))
                       (begin
                         #f
                         ))
                   ))
             ))
         ))
     )))

;;;#############################################################
;;;#############################################################
;;; mod-term = (list (list 'x x-order #f)
;;; (list 'y y-order #f) (list 'z z-or 23))
;;; ...to-pterms result = (list 'x (list (list x-order
;;; (list 'y (list (list y-order
;;; (list 'z (list (list z-or 23)))))))))
(define (turn-mod-terms-to-pterms amod-terms acc-list)
  (begin
    (if (null? amod-terms)
        (begin
          acc-list)
        (begin
          (let ((mod-terms-descending-list
                 (reverse
                  (sort-mod-terms-by-symbols amod-terms))))
            (let ((this-term (car mod-terms-descending-list))
                  (tail-list (cdr mod-terms-descending-list)))
              (let ((this-var (car this-term))
                    (this-order (cadr this-term))
                    (this-coeff (caddr this-term)))
                (begin
                  (if (null? acc-list)
                      (begin
                        ;;; construct the first term, a polynomial
                        (let ((real-coeff #f))
                          (begin
                            ;;; first find the non-#f coefficient,
                            ;;; must have at least 1
                            (for-each
                             (lambda (amod-term)
                               (begin
                                 (let ((acoeff (caddr amod-term)))
                                   (begin
                                     (if (and (equal? real-coeff #f)
                                              (not (equal? acoeff #f)))
                                         (begin
                                           (set! real-coeff acoeff)
                                           ))
                                     ))
                                 )) amod-terms)
                            ;;; next, put it into the deepest term
                            (let ((next-acc-list
                                   (make-polynomial
                                    this-var
                                    (list
                                     (make-term
                                      this-order real-coeff)))))
                              (begin
                                (turn-mod-terms-to-pterms
                                 tail-list next-acc-list)
                                ))
                            )))
                      (begin
                        ;;; first term ready to be used as the next coefficient
                        (let ((next-acc-list
                               (make-polynomial
                                this-var
                                (list
                                 (make-term
                                  this-order acc-list)))))
                          (begin
                            (turn-mod-terms-to-pterms
                             tail-list next-acc-list)
                            ))
                        ))
                  ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-2-list-lists-are-equal?
         slist-list rlist-list
         sub-name error-message
         result-hash-table)
  (begin
    (let ((slist-1 (car slist-list))
          (stail-1 (car (cdr slist-list)))
          (rlist-1 (car rlist-list))
          (rtail-1 (car (cdr rlist-list))))
      (begin
        (if (not (list? slist-1))
            (begin
              (let ((selem-1 (list-ref slist-list 0))
                    (selem-2 (list-ref slist-list 1))
                    (relem-1 (list-ref rlist-list 0))
                    (relem-2 (list-ref rlist-list 1))
                    (tolerance 1e-9))
                (let ((err-msg-1
                       (format
                        #f ", shouldbe 1=~a, result 1=~a, "
                        selem-1 relem-1))
                      (err-msg-2
                       (format
                        #f ", shouldbe 2=~a, result 2=~a, "
                        selem-2 relem-2)))
                  (begin
                    (if (not (list? selem-1))
                        (begin
                          (if (or (symbol? selem-1) (equal? selem-1 #f))
                              (begin
                                (unittest2:assert?
                                 (equal? selem-1 relem-1)
                                 sub-name
                                 (string-append
                                  error-message err-msg-1)
                                 result-hash-table))
                              (begin
                                (unittest2:assert?
                                 (< (abs (- selem-1 relem-1)) tolerance)
                                 sub-name
                                 (string-append
                                  error-message err-msg-1)
                                 result-hash-table)
                                )))
                        (begin
                          (assert-2-list-lists-are-equal?
                           selem-1 relem-1
                           sub-name error-message
                           result-hash-table)
                          ))

                    (if (not (list? selem-2))
                        (begin
                          (if (or (symbol? selem-2) (equal? selem-2 #f))
                              (begin
                                (unittest2:assert?
                                 (equal? selem-2 relem-2)
                                 sub-name
                                 (string-append
                                  error-message err-msg-1)
                                 result-hash-table))
                              (begin
                                (unittest2:assert?
                                 (< (abs (- selem-2 relem-2)) tolerance)
                                 sub-name
                                 (string-append
                                  error-message err-msg-2)
                                 result-hash-table)
                                )))
                        (begin
                          (assert-2-list-lists-are-equal?
                           selem-2 relem-2
                           sub-name error-message
                           result-hash-table)
                          ))
                    ))
                ))
            (begin
              (assert-2-list-lists-are-equal?
               slist-1 rlist-1
               sub-name error-message
               result-hash-table)
              ))

        (if (list? stail-1)
            (begin
              (assert-2-list-lists-are-equal?
               stail-1 rtail-1
               sub-name error-message
               result-hash-table)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-turn-mod-terms-to-pterms-1 result-hash-table)
 (begin
   (let ((sub-name "test-turn-mod-terms-to-pterms-1")
         (test-list
          (list
           (list (list (list 'x 10 #f) (list 'y 2 1))
                 (make-polynomial
                  'x (list (list
                            10 (make-polynomial
                                'y (list (list 2 1)))))))
           (list (list (list 'x 10 #f)
                       (list 'y 2 #f)
                       (list 'z 3 8))
                 (make-polynomial
                  'x
                  (list
                   (make-term
                    10
                    (make-polynomial
                     'y (list
                         (make-term
                          2 (make-polynomial
                             'z (list (make-term 3 8))))))
                    ))))
           (list (list (list 't 10 #f)
                       (list 'y 2 #f)
                       (list 'x 3 5))
                 (make-polynomial
                  't
                  (list
                   (list
                    10
                    (make-polynomial
                     'x (list
                         (list
                          3 (make-polynomial
                             'y (list (list 2 5))))))
                    ))))
           (list (list (list 'x 10 77)
                       (list 'y 2 #f)
                       (list 'z 3 #f))
                 (make-polynomial
                  'x
                  (list
                   (list
                    10
                    (make-polynomial
                     'y
                     (list
                      (make-term
                       2 (make-polynomial
                          'z (list (make-term 3 77))))))
                    ))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((am-terms (list-ref this-list 0))
                  (shouldbe-list-list
                   (list-ref this-list 1)))
              (let ((result-list-list
                     (turn-mod-terms-to-pterms
                      am-terms (list))))
                (let ((slen
                       (length shouldbe-list-list))
                      (rlen
                       (length result-list-list)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : am-terms=~a, "
                          sub-name test-label-index
                          am-terms))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-list-list result-list-list))
                        (err-msg-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2 err-msg-3)
                       result-hash-table)

                      (assert-2-list-lists-are-equal?
                       shouldbe-list-list result-list-list
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sort-term-list-by-order terms-list)
  (begin
    (stable-sort
     terms-list
     (lambda (a b)
       (begin
         (< (order a) (order b))
         )))
    ))

;;;#############################################################
;;;#############################################################
;;; assume symbol for the amod-term is not in terms-list
;;; pterm = (list 't (list (list 0
;;; (list 'y (list (list 0 3))) (list 1 3))))
;;; terms-list = (list 'x (list (list 0 1) (list 1 3)
;;; (list 2 (list 'y (list (list 0 1) (list 1
;;; (list 'z (list (list 0 2) (list 1 3)))))))))
;;; result = (list 'x (list (list 0 (list 't (list
;;; (list 0 (list 'y (list (list 0 4))) (list 1 3)))))
;;; (list 1 3) (list 2 (list 'y (list (list 0 1)
;;; (list 1 (list 'z (list (list 0 2) (list 1 3)))))))))
(define (add-to-zero-term pterm terms-list)
  (begin
    (let ((found-flag #f)
          (result-list (list)))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (let ((a-order (order aterm))
                   (a-coeff (coeff aterm)))
               (begin
                 (if (equal? a-order 0)
                     (begin
                       (set! found-flag #t)
                       (cond
                        ((number? a-coeff)
                         (begin
                           (if (number? pterm)
                               (begin
                                 (let ((next-aterm
                                        (make-term
                                         0 (+ a-coeff pterm))))
                                   (begin
                                     (set! result-list
                                           (cons next-aterm
                                                 result-list))
                                     )))
                               (begin
                                 (let ((pterm-list
                                        (term-list pterm))
                                       (pvar
                                        (variable pterm)))
                                   (let ((ptlist
                                          (add-to-zero-term
                                           a-coeff pterm-list)))
                                     (let ((next-aterm
                                            (make-term
                                             0 (make-polynomial
                                                pvar ptlist))))
                                       (begin
                                         (set!
                                          result-list
                                          (cons next-aterm
                                                result-list))
                                         ))
                                     ))
                                 ))
                           ))
                        ((pair? a-coeff)
                         (begin
                           (let ((a-var (variable a-coeff))
                                 (a-tl (term-list a-coeff)))
                             (let ((atlist
                                    (add-to-zero-term
                                     pterm a-tl)))
                               (let ((next-aterm
                                      (make-term
                                       0 (make-polynomial
                                          a-var atlist))))
                                 (begin
                                   (set! result-list
                                         (cons next-aterm
                                               result-list))
                                   ))
                               ))
                           ))
                        ))
                     (begin
                       (set! result-list
                             (cons aterm result-list))
                       ))
                 ))
             )) terms-list)

        (if (equal? found-flag #f)
            (begin
              (let ((next-aterm
                     (make-term 0 pterm)))
                (begin
                  (set! result-list
                        (cons next-aterm result-list))
                  ))
              ))

        (sort-term-list-by-order result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-to-zero-term-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-to-zero-term-1")
         (test-list
          (list
           (list 3
                 (list (list 3 4))
                 (list (list 0 3) (list 3 4)))
           (list 3
                 (list (list 3 4) (list 0 2))
                 (list (list 0 5) (list 3 4)))
           (list (make-polynomial
                  't (list (list 0 1)))
                 (list (list 3 4))
                 (list (list 0 (make-polynomial
                                't (list (list 0 1))))
                       (list 3 4)))
           (list (make-polynomial
                  'z (list (list 0 1)))
                 (list (list 0 (make-polynomial
                                't (list (list 0 1) (list 1 2))))
                       (list 3 4))
                 (list (list
                        0 (make-polynomial
                           't (list
                               (list 0 (make-polynomial
                                        'z (list (list 0 2))))
                               (list 1 2))))
                       (list 3 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((pterm (list-ref this-list 0))
                  (terms-list (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list
                     (add-to-zero-term
                      pterm terms-list)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "pterm=~a, terms-list=~a, "
                        pterm terms-list))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-4
                       (format
                        #f "shouldbe length=~a, result=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3
                      err-msg-4)
                     result-hash-table)

                    (assert-2-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sort-mod-terms-by-symbols-order mod-terms-list)
  (begin
    (stable-sort
     (map
      (lambda (alist)
        (begin
          (stable-sort
           alist
           (lambda (a b)
             (begin
               (string-ci<? (symbol->string (car a))
                            (symbol->string (car b)))
               )))
          )) mod-terms-list)
     (lambda (a b)
       (begin
         (let ((sa (symbol->string (car (car a))))
               (sb (symbol->string (car (car b)))))
           (begin
             (if (string-ci<? sa sb)
                 (begin
                   #t)
                 (begin
                   (if (string-ci=? sa sb)
                       (begin
                         (> (cadr (car a)) (cadr (car b))))
                       (begin
                         #f
                         ))
                   ))
             ))
         )))
    ))

;;;#############################################################
;;;#############################################################
;;; term-list-1 and term-list-2 already sorted, ascending order in the
;;; symbol, and descending order in the term order
(define (subterm1-greater-subterm2? term-1 term-2)
  (begin
    (let ((sym-string-1 (symbol->string (car term-1)))
          (sym-string-2 (symbol->string (car term-2))))
      (begin
        (if (string-ci<? sym-string-1 sym-string-2)
            (begin
              #t)
            (begin
              (if (string-ci=? sym-string-1 sym-string-2)
                  (begin
                    (let ((or-1 (cadr term-1))
                          (or-2 (cadr term-2)))
                      (begin
                        (> or-1 or-2)
                        )))
                  (begin
                    #f
                    ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; term-list-1 and term-list-2 already sorted, ascending order in the
;;; symbol, and descending order in the term order
(define (subterm1-equal-subterm2? term-1 term-2)
  (begin
    (let ((sym-string-1 (symbol->string (car term-1)))
          (sym-string-2 (symbol->string (car term-2)))
          (or-1 (cadr term-1))
          (or-2 (cadr term-2)))
      (begin
        (if (and (string-ci=? sym-string-1 sym-string-2)
                 (> or-1 or-2))
            (begin
              #t)
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (term1-greater-term2? term-1 term-2)
  (begin
    (cond
     ((null? term-1)
      (begin
        #f
        ))
     ((null? term-2)
      (begin
        #f
        ))
     (else
      (begin
        (let ((this-1 (car term-1))
              (tail-1 (cdr term-1))
              (this-2 (car term-2))
              (tail-2 (cdr term-2))
              (result-flag -1))
          (begin
            (if (subterm1-greater-subterm2? this-1 this-2)
                (begin
                  (set! result-flag #t))
                (begin
                  (if (subterm1-greater-subterm2? this-2 this-1)
                      (begin
                        (set! result-flag #f)
                        ))
                  ))

            (if (not (equal? result-flag -1))
                (begin
                  result-flag)
                (begin
                  (term1-greater-term2? tail-1 tail-2)
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-term1-greater-term2-1 result-hash-table)
 (begin
   (let ((sub-name "test-term1-greater-term2-1")
         (test-list
          (list
           (list (list (list 'x 2 2) (list 'z 3 #f))
                 (list (list 'x 1 2) (list 'z 3 #f))
                 #t)
           (list (list (list 'x 2 2) (list 'z 3 #f))
                 (list (list 'x 2 2) (list 'z 2 #f))
                 #t)
           (list (list (list 'x 2 2) (list 'z 2 #f))
                 (list (list 'x 2 2) (list 'z 3 #f))
                 #f)
           (list (list (list 'x 2 2) (list 'y 1 #f)
                       (list 'z 2 #f))
                 (list (list 'x 2 2) (list 'z 3 #f))
                 #t)
           (list (list (list 'x 2 2) (list 'z 3 #f))
                 (list (list 'x 2 2) (list 'y 1 #f)
                       (list 'z 2 #f))
                 #f)
           (list (list (list 't 2 2) (list 'z 3 #f))
                 (list (list 'x 2 2) (list 'y 1 #f)
                       (list 'z 2 #f))
                 #t)
           (list (list (list 'y 2 2) (list 'z 3 #f))
                 (list (list 'x 2 2) (list 'y 1 #f)
                       (list 'z 2 #f))
                 #f)
           (list (list (list 't 2 2) (list 'y 3 #f))
                 (list (list 't 2 2) (list 'y 1 #f)
                       (list 'z 2 #f))
                 #t)
           (list (list (list 't 2 2) (list 'y 3 #f))
                 (list (list 't 2 2) (list 'y 3 #f)
                       (list 'z 2 #f))
                 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mt-1 (list-ref this-list 0))
                  (mt-2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (term1-greater-term2? mt-1 mt-2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "mt-1=~a, mt-2=~a, " mt-1 mt-2))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-single-mt-coeff mt)
  (begin
    (let ((result #f))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (if (and (equal? result #f) (pair? aterm))
                 (begin
                   (let ((this-coeff (list-ref aterm 2)))
                     (begin
                       (if (not (equal? this-coeff #f))
                           (begin
                             (set! result this-coeff)
                             ))
                       ))
                   ))
             )) mt)
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-single-mt-coeff-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-single-mt-coeff-1")
         (test-list
          (list
           (list (list (list 'x 2 2)
                       (list 'z 3 #f))
                 2)
           (list (list (list 'x 2 #f)
                       (list 'z 3 3))
                 3)
           (list (list (list 'x 2 2)
                       (list 'y 1 #f)
                       (list 'z 2 #f))
                 2)
           (list (list (list 'x 2 #f)
                       (list 'y 1 3)
                       (list 'z 2 #f))
                 3)
           (list (list (list 'x 2 #f)
                       (list 'y 1 #f)
                       (list 'z 2 4))
                 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((smt (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (find-single-mt-coeff smt)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : smt=~a, "
                        sub-name test-label-index smt))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (replace-coeff-in-single-mt new-coeff mt)
  (begin
    (let ((result #f)
          (acc-list (list)))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (if (and (equal? result #f) (pair? aterm))
                 (begin
                   (let ((this-coeff (list-ref aterm 2)))
                     (begin
                       (if (not (equal? this-coeff #f))
                           (begin
                             (let ((new-sym
                                    (list-ref aterm 0))
                                   (new-order
                                    (list-ref aterm 1)))
                               (let ((next-term
                                      (list
                                       new-sym
                                       new-order
                                       new-coeff)))
                                 (begin
                                   (set! result #t)
                                   (set!
                                    acc-list
                                    (cons next-term acc-list))
                                   ))
                               ))
                           (begin
                             (set! acc-list
                                   (cons aterm acc-list))
                             ))
                       )))
                 (begin
                   (set! acc-list
                         (cons aterm acc-list))
                   ))
             )) mt)

        acc-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-replace-coeff-in-single-mt-1 result-hash-table)
 (begin
   (let ((sub-name "test-replace-coeff-in-single-mt-1")
         (test-list
          (list
           (list 7 (list (list 'x 2 2))
                 (list (list 'x 2 7)))
           (list 7 (list (list 'x 2 2)
                         (list 'y 1 #f))
                 (list (list 'x 2 7)
                       (list 'y 1 #f)))
           (list 7 (list (list 'x 2 #f)
                         (list 'y 1 2))
                 (list (list 'x 2 #f)
                       (list 'y 1 7)))
           (list 7 (list (list 'x 2 #f)
                         (list 'y 1 2)
                         (list 'z 3 #f))
                 (list (list 'x 2 #f)
                       (list 'y 1 7)
                       (list 'z 3 #f)))
           (list 7 (list (list 'x 2 #f)
                         (list 'y 1 #f)
                         (list 'z 3 2))
                 (list (list 'x 2 #f)
                       (list 'y 1 #f)
                       (list 'z 3 7)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((new-coeff (list-ref this-list 0))
                  (smt (list-ref this-list 1))
                  (shouldbe-list-list
                   (sort-mod-terms-by-symbols
                    (list-ref this-list 2))))
              (let ((result-list-list
                     (sort-mod-terms-by-symbols
                      (replace-coeff-in-single-mt new-coeff smt))))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : smt=~a, "
                        sub-name test-label-index smt))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-3
                       (format
                        #f "shouldbe length=~a, result=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; mt-1 (((x 2 2) (z 3 #f)) ((x 1 1) (z 2 #f))
;;; ((x 0 #f) (y 1 3) (z 1 #f)))
;;; mt-2 (((x 3 2) (z 2 #f)) ((x 2 3) (z 2 #f))
;;; ((x 1 #f) (z 2 3) (z 1 #f)) ((x 0 #f) (y 1 2) (z 1 #f))
;;; result (((x 3 2) (z 2 #f)) ((x 2 #f) (z 3 2))
;;; ((x 2 3) (z 2 #f)) ((x 1 #f) (z 2 4))
;;; ((x 0 #f) (y 1 #f) (z 1 5)))
(define (add-mod-terms mt-1 mt-2)
  (define (local-add-mts mt-1 mt-2 acc-list)
    (begin
      (cond
       ((null? mt-1)
        (begin
          (if (null? mt-2)
              (begin
                acc-list)
              (begin
                (append acc-list mt-2)
                ))
          ))
       ((null? mt-2)
        (begin
          (append acc-list mt-1)
          ))
       (else
        (let ((this-1 (car mt-1))
              (tail-1 (cdr mt-1))
              (this-2 (car mt-2))
              (tail-2 (cdr mt-2)))
          (begin
            (cond
             ((term1-greater-term2? this-1 this-2)
              (begin
                (let ((next-acc-list (cons this-1 acc-list)))
                  (begin
                    (local-add-mts tail-1 mt-2 next-acc-list)
                    ))
                ))
             ((term1-greater-term2? this-2 this-1)
              (begin
                (let ((next-acc-list (cons this-2 acc-list)))
                  (begin
                    (local-add-mts mt-1 tail-2 next-acc-list)
                    ))
                ))
             (else
              (begin
                (let ((c1 (find-single-mt-coeff this-1))
                      (c2 (find-single-mt-coeff this-2)))
                  (let ((next-coeff (+ c1 c2)))
                    (let ((next-term
                           (replace-coeff-in-single-mt
                            next-coeff this-1)))
                      (let ((next-acc-list
                             (cons next-term acc-list)))
                        (begin
                          (local-add-mts
                           tail-1 tail-2 next-acc-list)
                          ))
                      )))
                )))
            ))
        ))
      ))
  (begin
    (sort-mod-terms-by-symbols-order
     (local-add-mts
      (sort-mod-terms-by-symbols-order mt-1)
      (sort-mod-terms-by-symbols-order mt-2)
      (list)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-mod-terms-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-mod-terms-1")
         (test-list
          (list
           (list (list (list (list 'x 2 2)))
                 (list (list (list 'x 2 3)))
                 (list (list (list 'x 2 5))))
           (list (list (list (list 'x 1 2)))
                 (list (list (list 'x 2 3)))
                 (list (list (list 'x 2 3))
                       (list (list 'x 1 2))))
           (list (list (list (list 'x 2 2)
                             (list 'z 3 #f)))
                 (list (list (list 'x 1 2)
                             (list 'z 3 #f)))
                 (list (list (list 'x 2 2)
                             (list 'z 3 #f))
                       (list (list 'x 1 2)
                             (list 'z 3 #f))))
           (list (list (list (list 'x 2 2)
                             (list 'z 3 #f)))
                 (list (list (list 'x 2 2)
                             (list 'z 3 #f)))
                 (list (list (list 'x 2 4)
                             (list 'z 3 #f))))
           (list (list (list (list 'x 2 #f)
                             (list 'z 3 5)))
                 (list (list (list 'x 2 #f)
                             (list 'z 3 7)))
                 (list (list (list 'x 2 #f)
                             (list 'z 3 12))))
           (list (list (list (list 'x 2 #f)
                             (list 'z 3 5))
                       (list (list 'x 1 #f)
                             (list 'z 2 8)))
                 (list (list (list 'x 2 #f)
                             (list 'z 3 7)))
                 (list (list (list 'x 2 #f)
                             (list 'z 3 12))
                       (list (list 'x 1 #f)
                             (list 'z 2 8))))
           (list (list (list (list 'x 2 #f)
                             (list 'z 3 5))
                       (list (list 'x 1 #f)
                             (list 'z 2 8)))
                 (list (list (list 'x 2 #f)
                             (list 'z 3 7))
                       (list (list 'x 1 #f)
                             (list 'z 2 3)))
                 (list (list (list 'x 2 #f)
                             (list 'z 3 12))
                       (list (list 'x 1 #f)
                             (list 'z 2 11))))
           (list (list (list (list 'x 2 2)
                             (list 'y 3 #f)
                             (list 'z 1 #f))
                       (list (list 'x 1 #f)
                             (list 'y 2 3)))
                 (list (list (list 'x 2 #f)
                             (list 'y 3 #f)
                             (list 'z 1 3)))
                 (list (list (list 'x 2 5)
                             (list 'y 3 #f)
                             (list 'z 1 #f))
                       (list (list 'x 1 #f)
                             (list 'y 2 3))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mt-1 (list-ref this-list 0))
                  (mt-2 (list-ref this-list 1))
                  (shouldbe-list-list
                   (list-ref this-list 2)))
              (let ((result-list-list
                     (add-mod-terms mt-1 mt-2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "mt-1=~a, mt-2=~a, "
                        mt-1 mt-2))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-4
                       (format
                        #f "shouldbe length=~a, result=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2
                      err-msg-3 err-msg-4)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (mul-single-mod-terms smt-1 smt-2)
  (begin
    (let ((result-list (list))
          (matched-b-terms (list))
          (result-coefficient 0))
      (begin
        (for-each
         (lambda (a-term)
           (begin
             (let ((a-sym (list-ref a-term 0))
                   (a-order (list-ref a-term 1))
                   (a-coeff (list-ref a-term 2))
                   (a-found-flag #f))
               (begin
                 (for-each
                  (lambda (b-term)
                    (begin
                      (let ((b-sym (list-ref b-term 0))
                            (b-order (list-ref b-term 1))
                            (b-coeff (list-ref b-term 2)))
                        (begin
                          (if (and
                               (equal? a-found-flag #f)
                               (equal?
                                (member b-term matched-b-terms)
                                #f)
                               (eq? a-sym b-sym))
                              (begin
                                (set! a-found-flag #t)
                                (set!
                                 matched-b-terms
                                 (cons b-term matched-b-terms))
                                (let ((a-c
                                       (find-single-mt-coeff
                                        smt-1))
                                      (b-c
                                       (find-single-mt-coeff
                                        smt-2)))
                                  (let ((new-term
                                         (list
                                          a-sym
                                          (+ a-order b-order) #f)))
                                    (begin
                                      (set!
                                       result-list
                                       (cons new-term result-list))
                                      (set!
                                       result-coefficient
                                       (* a-c b-c))
                                      )))
                                ))
                          ))
                      )) smt-2)
                 (if (equal? a-found-flag #f)
                     (begin
                       ;;; add in unmatched terms from smt-1
                       (let ((new-term
                              (list
                               (list-ref a-term 0)
                               (list-ref a-term 1) #f)))
                         (begin
                           (set!
                            result-list
                            (cons new-term result-list))
                           ))
                       ))
                 ))
             )) smt-1)

        ;;; add in unmatched terms from smt-2
        (for-each
         (lambda (b-term)
           (begin
             (if (equal?
                  (member b-term matched-b-terms) #f)
                 (begin
                   (let ((new-term
                          (list
                           (list-ref b-term 0)
                           (list-ref b-term 1) #f)))
                     (begin
                       (set!
                        result-list
                        (cons new-term result-list))
                       ))
                   ))
             )) smt-2)

        ;;; update coefficient
        (let ((aa-list
               (sort-mod-terms-by-symbols
                result-list)))
          (begin
            (let ((bb-list (reverse aa-list)))
              (let ((cc-term (car bb-list)))
                (let ((next-term
                       (list (list-ref cc-term 0)
                             (list-ref cc-term 1)
                             result-coefficient)))
                  (begin
                    (if (= result-coefficient 0)
                        (begin
                          (let ((a-c (find-single-mt-coeff smt-1))
                                (b-c (find-single-mt-coeff smt-2)))
                            (begin
                              (let ((anext-term
                                     (list (list-ref cc-term 0)
                                           (list-ref cc-term 1)
                                           (* a-c b-c))))
                                (begin
                                  (set! next-term anext-term)
                                  ))
                              ))
                          ))

                    (set!
                     bb-list
                     (cons next-term (cdr bb-list)))
                    (set!
                     aa-list (reverse bb-list))
                    ))
                ))
            aa-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mul-single-mod-terms-1 result-hash-table)
 (begin
   (let ((sub-name "test-mul-single-mod-terms-1")
         (test-list
          (list
           (list (list (list 'x 2 2))
                 (list (list 'x 2 3))
                 (list (list 'x 4 6)))
           (list (list (list 'x 2 2)
                       (list 'y 1 #f))
                 (list (list 'x 2 3))
                 (list (list 'x 4 #f)
                       (list 'y 1 6)))
           (list (list (list 'x 2 2))
                 (list (list 'y 1 3))
                 (list (list 'x 2 #f)
                       (list 'y 1 6)))
           (list (list (list 'x 3 5))
                 (list (list 'y 2 4))
                 (list (list 'x 3 #f)
                       (list 'y 2 20)))
           (list (list (list 'x 2 2)
                       (list 'y 1 #f))
                 (list (list 'y 1 3))
                 (list (list 'x 2 #f)
                       (list 'y 2 6)))
           (list (list (list 'x 2 2)
                       (list 'y 1 #f)
                       (list 'z 1 #f))
                 (list (list 'y 1 5)
                       (list 'z 2 #f))
                 (list (list 'x 2 #f)
                       (list 'y 2 #f)
                       (list 'z 3 10)))
           (list (list (list 'y 1 5)
                       (list 'z 2 #f))
                 (list (list 'x 2 2)
                       (list 'y 1 #f)
                       (list 'z 1 #f))
                 (list (list 'x 2 #f)
                       (list 'y 2 #f)
                       (list 'z 3 10)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mt-1 (list-ref this-list 0))
                  (mt-2 (list-ref this-list 1))
                  (shouldbe-list-list
                   (sort-mod-terms-by-symbols
                    (list-ref this-list 2))))
              (let ((result-list-list
                     (mul-single-mod-terms mt-1 mt-2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "mt-1=~a, mt-2=~a, " mt-1 mt-2))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-4
                       (format
                        #f "shouldbe length=~a, result=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2
                      err-msg-3 err-msg-4)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (mul-sort-mod-terms mul-mod-terms-list)
  (begin
    (stable-sort
     (map
      (lambda (alist)
        (begin
          (stable-sort
           alist
           (lambda (a b)
             (begin
               (string-ci<?
                (symbol->string (car a))
                (symbol->string (car b)))
               )))
          )) mul-mod-terms-list)
     (lambda (a b)
       (begin
         (let ((sa (symbol->string (car (car a))))
               (sb (symbol->string (car (car b)))))
           (begin
             (if (string-ci<? sa sb)
                 (begin
                   #t)
                 (begin
                   (if (string-ci=? sa sb)
                       (begin
                         (> (cadr (car a)) (cadr (car b))))
                       (begin
                         #f
                         ))
                   ))
             ))
         )))
    ))

;;;#############################################################
;;;#############################################################
;;; mt-1 (((x 2 2) (z 3 #f)) ((x 1 1) (z 2 #f))
;;; ((x 0 #f) (y 1 3) (z 1 #f)))
;;; mt-2 (((x 3 2) (z 2 #f)))
;;; result (((x 5 4) (z 5 #f)) ((x 4 #f) (z 4 2))
;;; ((x 3 #f) (y 1 6) (z 1 #f)))
(define (mul-mod-terms mt-1 mt-2)
  (begin
    (let ((result-list (list)))
      (begin
        (for-each
         (lambda (single-1)
           (begin
             (for-each
              (lambda (single-2)
                (begin
                  (let ((new-term
                         (mul-single-mod-terms
                          single-1 single-2)))
                    (begin
                      (set!
                       result-list
                       (cons new-term result-list))
                      ))
                  )) mt-2)
             )) mt-1)

        (mul-sort-mod-terms result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mul-mod-terms-1 result-hash-table)
 (begin
   (let ((sub-name "test-mul-mod-terms-1")
         (test-list
          (list
           (list (list (list (list 'x 2 2)))
                 (list (list (list 'x 2 3)))
                 (list (list (list 'x 4 6))))
           (list (list (list (list 'x 2 2)
                             (list 'y 1 #f)))
                 (list (list (list 'x 2 3)))
                 (list (list (list 'x 4 #f)
                             (list 'y 1 6))))
           (list (list (list (list 'x 2 2)
                             (list 'y 1 #f)))
                 (list (list (list 'y 1 3)))
                 (list (list (list 'x 2 #f)
                             (list 'y 2 6))))
           (list (list (list (list 'x 2 2)
                             (list 'y 1 #f)
                             (list 'z 1 #f)))
                 (list (list (list 'y 1 5)
                             (list 'z 2 #f)))
                 (list (list (list 'x 2 #f)
                             (list 'y 2 #f)
                             (list 'z 3 10))))
           (list (list (list (list 'y 1 5)
                             (list 'z 2 #f)))
                 (list (list (list 'x 2 2)
                             (list 'y 1 #f)
                             (list 'z 1 #f)))
                 (list (list (list 'x 2 #f)
                             (list 'y 2 #f)
                             (list 'z 3 10))))
           (list (list (list (list 'x 2 2))
                       (list (list 'x 1 3)))
                 (list (list (list 'x 2 3)))
                 (list (list (list 'x 4 6))
                       (list (list 'x 3 9))))
           (list (list (list (list 'x 2 3)))
                 (list (list (list 'x 2 2))
                       (list (list 'x 1 3)))
                 (list (list (list 'x 4 6))
                       (list (list 'x 3 9))))
           (list (list (list (list 'x 2 2))
                       (list (list 'x 1 3)))
                 (list (list (list 'y 2 3)))
                 (list (list (list 'x 2 #f)
                             (list 'y 2 6))
                       (list (list 'x 1 #f)
                             (list 'y 2 9))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mt-1 (list-ref this-list 0))
                  (mt-2 (list-ref this-list 1))
                  (shouldbe-list-list
                   (mul-sort-mod-terms
                    (list-ref this-list 2))))
              (let ((result-list-list
                     (mul-mod-terms mt-1 mt-2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "mt-1=~a, mt-2=~a, "
                        mt-1 mt-2))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-4
                       (format
                        #f "shouldbe length=~a, result=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2
                      err-msg-3 err-msg-4)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (single-modified-term-to-polynomial mod-term)
  (begin
    (let ((smod-term
           (sort
            mod-term
            (lambda (a b)
              (string-ci>?
               (symbol->string (car a))
               (symbol->string (car b))))))
          (result-poly (list))
          (term-coeff
           (find-single-mt-coeff mod-term))
          (init-flag #t))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (let ((avar (list-ref aterm 0))
                   (aorder (list-ref aterm 1))
                   (acoeff (list-ref aterm 2)))
               (begin
                 (if (equal? init-flag #t)
                     (begin
                       (set! init-flag #f)
                       (let ((apoly
                              (make-polynomial
                               avar
                               (list (list aorder term-coeff)))))
                         (begin
                           (set! result-poly apoly)
                           )))
                     (begin
                       (let ((apoly
                              (make-polynomial
                               avar
                               (list (list aorder result-poly)))))
                         (begin
                           (set! result-poly apoly)
                           ))
                       ))
                 ))
             )) smod-term)

        result-poly
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-single-modified-term-to-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name "test-single-modified-term-to-polynomial-1")
         (test-list
          (list
           (list (list (list 'x 2 2))
                 (make-polynomial
                  'x (list (list 2 2))))
           (list (list (list 'x 2 #f)
                       (list 'y 1 3))
                 (make-polynomial
                  'x (list (list 2 (make-polynomial
                                    'y (list (list 1 3)))))))
           (list (list (list 'x 2 #f)
                       (list 'y 1 #f)
                       (list 'z 2 3))
                 (make-polynomial
                  'x (list
                      (list
                       2
                       (make-polynomial
                        'y
                        (list
                         (list 1 (make-polynomial
                                  'z (list (list 2 3))))))))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((mt (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list
                     (single-modified-term-to-polynomial mt)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : mt=~a, "
                        sub-name test-label-index
                        mt))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-3
                       (format
                        #f "shouldbe length=~a, result length=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; given a symbol, restructure the modified term list
;;; mod-term = (list (list 'x x-order #f) (list 'y y-order 33))
;;; result = (make-polynomial 'x (list (list x-order
;;; (make-polynomial 'y (list (list y-order 33))))))
(define (modified-term-list-to-polynomial mt-list)
  (define (local-iter mt-list acc-list)
    (begin
      (if (null? mt-list)
          (begin
            acc-list)
          (begin
            (let ((this-term (car mt-list))
                  (tail-list (cdr mt-list)))
              (begin
                (let ((apoly
                       (single-modified-term-to-polynomial
                        this-term)))
                  (begin
                    (local-iter
                     tail-list (cons apoly acc-list))
                    ))
                ))
            ))
      ))
  (define (local-poly-to-mod-term apoly)
    (begin
      (let ((var (car apoly)))
        (let ((term (cadr apoly)))
          (let ((order (car term))
                (coeff (cadr term)))
            (begin
              (list var order coeff)
              ))
          ))
      ))
  (define (local-group-terms apoly-list)
    (begin
      (let ((result-list (list))
            (other-list (list))
            (primary-variable #f))
        (begin
          (for-each
           (lambda (apoly)
             (begin
               (if (not (null? apoly))
                   (begin
                     (let ((this-variable (car apoly)))
                       (begin
                         (if (equal? primary-variable #f)
                             (begin
                               (set! primary-variable
                                     this-variable)
                               (set! result-list
                                     (append (cdr apoly)
                                             result-list)))
                             (begin
                               (if (equal? this-variable
                                           primary-variable)
                                   (begin
                                     (set! result-list
                                           (append (cdr apoly)
                                                   result-list)))
                                   (begin
                                     (let ((m-term
                                            (local-poly-to-mod-term
                                             apoly)))
                                       (begin
                                         (set!
                                          other-list
                                          (cons m-term other-list))
                                         ))
                                     ))
                               ))
                         ))
                     ))
               )) apoly-list)
          (let ((result-poly
                 (make-polynomial
                  primary-variable result-list)))
            (let ((answer
                   (list result-poly other-list)))
              (begin
                answer
                )))
          ))
      ))
  (define (local-add-constant-to-zero primary-polynomial aconst)
    (begin
      (let ((prim-var
             (variable primary-polynomial))
            (prim-term-list
             (term-list primary-polynomial))
            (has-zero-flag #f)
            (result-list (list)))
        (begin
          (for-each
           (lambda (p-term)
             (begin
               (if (= (order p-term) 0)
                   (begin
                     (set! has-zero-flag #t)
                     (let ((p-order (order p-term))
                           (p-coeff (coeff p-term)))
                       (let ((new-term
                              (make-term
                               p-order (+ p-coeff aconst))))
                         (begin
                           (set! result-list
                                 (cons new-term result-list))
                           ))
                       ))
                   (begin
                     (set! result-list
                           (cons p-term result-list))
                     ))
               )) prim-term-list)

          (if (equal? has-zero-flag #f)
              (begin
                (set! result-list
                      (cons (make-term 0 aconst) result-list))
                ))
          (make-polynomial prim-var (reverse result-list))
          ))
      ))
  (define (local-add-others-to-zero primary-polynomial others-list)
    (cond
     ((null? primary-polynomial)
      (begin
        (single-modified-term-to-polynomial others-list)
        ))
     ((null? others-list)
      (begin
        primary-polynomial
        ))
     (else
      (begin
        (let ((prim-var
               (variable primary-polynomial))
              (prim-term-list
               (term-list primary-polynomial))
              (has-zero-flag #f)
              (result-list (list)))
          (begin
            (for-each
             (lambda (p-term)
               (begin
                 (if (= (order p-term) 0)
                     (begin
                       (set! has-zero-flag #t)
                       (let ((p-order (order p-term))
                             (p-coeff (coeff p-term)))
                         (let ((sorted-olist
                                (map
                                 (lambda (aa) (list aa))
                                 (stable-sort
                                  others-list
                                  (lambda (a b) (> (list-ref a 1) (list-ref b 1)))
                                  ))))
                           (let ((new-poly
                                  (modified-term-list-to-polynomial
                                   sorted-olist)))
                             (let ((new-term
                                    (local-add-constant-to-zero
                                     new-poly p-coeff)))
                               (begin
                                 (set! result-list
                                       (cons (make-term 0 new-term)
                                             result-list))
                                 ))
                             ))
                         ))
                     (begin
                       (set! result-list
                             (cons p-term result-list))
                       ))
                 )) prim-term-list)
            (if (equal? has-zero-flag #f)
                (begin
                  (let ((ocount (length others-list)))
                    (begin
                      (cond
                       ((= ocount 1)
                        (begin
                          (set! result-list
                                (cons
                                 (make-term
                                  0 (single-modified-term-to-polynomial
                                     others-list))
                                 result-list))
                          ))
                       ((> ocount 1)
                        (begin
                          (let ((a-list-list
                                 (modified-term-list-to-polynomial
                                  (map
                                   (lambda (aa) (list aa))
                                   others-list))
                                 ))
                            (let ((a-var (variable a-list-list))
                                  (a-tl (term-list a-list-list)))
                              (let ((sorted-a-tl
                                     (stable-sort
                                      a-tl
                                      (lambda (a b)
                                        (> (car a) (car b))))))
                                (let ((sub-poly
                                       (make-polynomial
                                        a-var sorted-a-tl)))
                                  (begin
                                    (set!
                                     result-list
                                     (cons (make-term 0 sub-poly)
                                           result-list))
                                    ))
                                )))
                          )))
                      ))
                  ))
            (make-polynomial
             prim-var (reverse result-list))
            ))
        ))
     ))
  (begin
    (let ((sorted-list
           (sort-mod-terms-by-symbols-order mt-list)))
      (let ((plist-list
             (reverse
              (local-iter sorted-list (list)))))
        (let ((p-list-list
               (local-group-terms plist-list)))
          (let ((prime-list (car p-list-list))
                (prime-var (car (car p-list-list)))
                (other-list (list-ref p-list-list 1)))
            (begin
              (if (not (null? other-list))
                  (begin
                    (let ((next-prime-list
                           (local-add-others-to-zero
                            prime-list other-list)))
                      (begin
                        (set! prime-list next-prime-list)
                        ))
                    ))
              (begin
                prime-list
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-modified-term-list-to-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name
          "test-modified-term-list-to-polynomial-1")
         (test-list
          (list
           (list (list (list (list 'x 3 5)))
                 (make-polynomial 'x (list (list 3 5))))
           (list (list (list (list 'x 3 5) (list 'y 1 #f)))
                 (make-polynomial
                  'x (list (list 3 (make-polynomial
                                    'y (list (list 1 5)))))))
           (list (list (list (list 'x 3 5))
                       (list (list 'x 2 1)))
                 (make-polynomial
                  'x (list (list 3 5) (list 2 1))))
           (list (list (list (list 'x 3 5))
                       (list (list 'y 1 3)))
                 (make-polynomial
                  'x (list (list 3 5)
                           (list 0 (make-polynomial
                                    'y (list (list 1 3))))
                           )))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((amod-terms
                   (list-ref this-list 0))
                  (shouldbe-list-list
                   (list-ref this-list 1)))
              (let ((result-list-list
                     (modified-term-list-to-polynomial
                      amod-terms)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : amod-terms=~a, "
                        sub-name test-label-index amod-terms))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-3
                       (format
                        #f "shouldbe length=~a, result length=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (add-polynomial apoly-1 apoly-2)
  (begin
    (let ((mt-1
           (polynomial-to-modified-term-list apoly-1))
          (mt-2
           (polynomial-to-modified-term-list apoly-2)))
      (let ((mt (add-mod-terms mt-1 mt-2)))
        (let ((result-poly
               (modified-term-list-to-polynomial mt)))
          (begin
            result-poly
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-polynomial-1")
         (test-list
          (list
           (list (make-polynomial
                  'x (list (list 3 5)))
                 (make-polynomial
                  'x (list (list 3 4)))
                 (make-polynomial
                  'x (list (list 3 9))))
           (list (make-polynomial
                  'x (list (list 3 5)))
                 (make-polynomial
                  'x (list (list 2 4)))
                 (make-polynomial
                  'x (list (list 3 5) (list 2 4))))
           (list (make-polynomial
                  'x (list (list 3 5)))
                 (make-polynomial
                  'y (list (list 2 4)))
                 (make-polynomial
                  'x (list
                      (list 3 5)
                      (list 0 (make-polynomial
                               'y (list (list 2 4)))))))
           (list (make-polynomial
                  'x (list (list 3 5)))
                 (make-polynomial
                  'y (list (list 2 4)))
                 (make-polynomial
                  'x (list
                      (list 3 5)
                      (list 0 (make-polynomial
                               'y (list (list 2 4)))))))
           (list (make-polynomial
                  'x (list (list 3 5)))
                 (make-polynomial
                  'y (list (list 2 4) (list 1 3)))
                 (make-polynomial
                  'x (list
                      (list 3 5)
                      (list 0 (make-polynomial
                               'y (list (list 2 4) (list 1 3)))))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((apoly-1 (list-ref this-list 0))
                  (apoly-2 (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list
                     (add-polynomial apoly-1 apoly-2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "apoly-1=~a, apoly-2=~a"
                        apoly-1 apoly-2))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list-list result-list-list))
                      (err-msg-4
                       (format
                        #f "shouldbe length=~a, result=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2
                      err-msg-4)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (mul-polynomial apoly-1 apoly-2)
  (begin
    (let ((mt-1
           (polynomial-to-modified-term-list apoly-1))
          (mt-2
           (polynomial-to-modified-term-list apoly-2)))
      (let ((mt
             (mul-mod-terms mt-1 mt-2)))
        (let ((result-poly
               (modified-term-list-to-polynomial mt)))
          (begin
            result-poly
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mul-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name "test-mul-polynomial-1")
         (test-list
          (list
           (list (make-polynomial
                  'x (list (list 3 5)))
                 (make-polynomial
                  'x (list (list 3 4)))
                 (make-polynomial
                  'x (list (list 6 20))))
           (list (make-polynomial
                  'x (list (list 3 7)))
                 (make-polynomial
                  'x (list (list 2 3)))
                 (make-polynomial
                  'x (list (list 5 21))))
           (list (make-polynomial
                  'x (list (list 3 5)))
                 (make-polynomial
                  'y (list (list 2 4)))
                 (make-polynomial
                  'x (list
                      (list 3 (make-polynomial
                               'y (list (list 2 20)))))))
           (list (make-polynomial
                  'x (list (list 3 5) (list 1 4)))
                 (make-polynomial
                  'y (list (list 2 4)))
                 (make-polynomial
                  'x (list
                      (list 3 (make-polynomial
                               'y (list (list 2 20))))
                      (list 1 (make-polynomial
                               'y (list (list 2 16)))))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((apoly-1 (list-ref this-list 0))
                  (apoly-2 (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list
                     (mul-polynomial apoly-1 apoly-2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) :  "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "apoly-1=~a, apoly-2=~a, "
                        apoly-1 apoly-2))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list-list result-list-list))
                      (err-msg-4
                       (format
                        #f "shouldbe length=~a, result=~a"
                        (length shouldbe-list-list)
                        (length result-list-list)))
                      (slen (length shouldbe-list-list))
                      (rlen(length result-list-list)))
                  (begin
                    (unittest2:assert?
                     (equal? slen rlen)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2
                      err-msg-3 err-msg-4)
                     result-hash-table)

                    (assert-3-list-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (polynomial-to-string apoly)
  (begin
    (let ((result-string "")
          (mt
           (sort-mod-terms-by-symbols-order
            (polynomial-to-modified-term-list apoly))))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (let ((current-string "")
                   (current-coeff 0))
               (begin
                 (for-each
                  (lambda (bterm)
                    (begin
                      (let ((svar
                             (symbol->string
                              (list-ref bterm 0)))
                            (border (list-ref bterm 1))
                            (bcoeff (list-ref bterm 2)))
                        (begin
                          (if (not (equal? bcoeff #f))
                              (begin
                                (set! current-coeff bcoeff)
                                (if (<=
                                     (string-length current-string)
                                     0)
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set! current-string
                                                  (format #f "~a" svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a^~a"
                                                    svar border))
                                                  ))
                                            )))
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set!
                                             current-string
                                             (format
                                              #f "~a*~a"
                                              current-string svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a*~a^~a"
                                                    current-string svar border))
                                                  ))
                                            ))
                                      )))
                              (begin
                                (if (<= (string-length current-string) 0)
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set!
                                             current-string
                                             (format
                                              #f "~a" svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a^~a"
                                                    svar border))
                                                  ))
                                            )))
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set!
                                             current-string
                                             (format
                                              #f "~a*~a"
                                              current-string
                                              svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a*~a^~a"
                                                    current-string
                                                    svar border))
                                                  ))
                                            ))
                                      ))
                                ))
                          ))
                      )) aterm)
                 (if (<= (string-length current-string) 0)
                     (begin
                       (set! current-string
                             (format #f "~a" current-coeff)))
                     (begin
                       (set! current-string
                             (format
                              #f "~a*~a"
                              current-coeff current-string))
                       ))

                 (if (<= (string-length result-string) 0)
                     (begin
                       (set! result-string current-string))
                     (begin
                       (set! result-string
                             (format
                              #f "~a + ~a"
                              result-string current-string))
                       ))
                 ))
             )) mt)

        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-polynomial-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-polynomial-to-string-1")
         (test-list
          (list
           (list (make-polynomial
                  'x (list (list 3 5)))
                 "5*x^3")
           (list (make-polynomial
                  'x (list (list 3 7) (list 2 3)))
                 "7*x^3 + 3*x^2")
           (list (make-polynomial
                  'x (list
                      (list 3 (make-polynomial
                               'y (list (list 2 20))))))
                 "20*x^3*y^2")
           (list (make-polynomial
                  'x (list
                      (list 3 (make-polynomial
                               'y (list (list 2 20))))
                      (list 1 (make-polynomial
                               'y (list (list 2 16))))))
                 "20*x^3*y^2 + 16*x*y^2")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((apoly (list-ref this-list 0))
                  (shouldbe-string (list-ref this-list 1)))
              (let ((result-string (polynomial-to-string apoly)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : apoly=~a, "
                        sub-name test-label-index
                        apoly))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-string result-string)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "By imposing an ordering on variables, "))
    (display
     (format #f "extend the polynomial~%"))
    (display
     (format #f "package so that addition and "))
    (display
     (format #f "multiplication of polynomials~%"))
    (display
     (format #f "works for polynomials in different "))
    (display
     (format #f "variables. (This is~%"))
    (display
     (format #f "not easy!)~%"))
    (newline)
    (display
     (format #f "The ordering of variables was accomplished "))
    (display
     (format #f "by splitting up~%"))
    (display
     (format #f "the polynomial into modified term lists, "))
    (display
     (format #f "where each term~%"))
    (display
     (format #f "in an expression is its own list, which "))
    (display
     (format #f "can have 1 or~%"))
    (display
     (format #f "more variables. For example the "))
    (display
     (format #f "polynomial x^2*y+3~%"))
    (display
     (format #f "has polynomial representation.~%"))
    (newline)
    (display
     (format #f "(list x (0 3) (2 (list y "))
    (display
     (format #f "(list (list 1 1))))).~%"))
    (display
     (format #f "The modified representation simplifies "))
    (display
     (format #f "this to:~%"))
    (display
     (format #f "(list (list (list x 0 3)) (list "))
    (display
     (format #f "(list x 2 #f) (list y 1 1)))~%"))
    (display
     (format #f "With this intermediate representation, "))
    (display
     (format #f "and functions to~%"))
    (display
     (format #f "go back and forth between the two, it "))
    (display
     (format #f "is easy to add~%"))
    (display
     (format #f "and multiply polynomial expressions, "))
    (display
     (format #f "since each modified~%"))
    (display
     (format #f "list consists of terms and only terms "))
    (display
     (format #f "that match in~%"))
    (display
     (format #f "the symbols and the orders are "))
    (display
     (format #f "added together.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list
             (make-polynomial
              'x (list (list 2 3)
                       (list 1 4)
                       (list 0 -1)))
             (make-polynomial
              'x (list (list 2 4)
                       (list 1 8)
                       (list 0 3))))
            (list
             (make-polynomial
              'x (list (list 2 3)
                       (list 1 4)
                       (list 0 -1)))
             (make-polynomial
              'y (list (list 2 4)
                       (list 1 8)
                       (list 0 3))))
            )))
      (begin
        ;;; addition
        (for-each
         (lambda (alist)
           (begin
             (let ((apoly-1 (list-ref alist 0))
                   (apoly-2 (list-ref alist 1)))
               (let ((asum (add-polynomial apoly-1 apoly-2)))
                 (let ((astr-1 (polynomial-to-string apoly-1))
                       (astr-2 (polynomial-to-string apoly-2))
                       (asum-str (polynomial-to-string asum)))
                   (begin
                     (display
                      (format
                       #f "(~a) + (~a)~% = ~a~%"
                       astr-1 astr-2 asum-str))
                     (newline)
                     ))
                 ))
             )) test-list)

        (newline)
        ;;; multiplication
        (for-each
         (lambda (alist)
           (begin
             (let ((apoly-1 (list-ref alist 0))
                   (apoly-2 (list-ref alist 1)))
               (let ((amult
                      (mul-polynomial apoly-1 apoly-2)))
                 (let ((astr-1
                        (polynomial-to-string apoly-1))
                       (astr-2
                        (polynomial-to-string apoly-2))
                       (amult-str
                        (polynomial-to-string amult)))
                   (begin
                     (display
                      (format
                       #f "(~a) * (~a)~% = ~a~%"
                       astr-1 astr-2 amult-str))
                     (newline)
                     ))
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.92 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme tests~%"))
             (main-loop)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
