#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.85                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (raise-integer->rational ii-int)
  (begin
    (if (eq? (type-tag ii-int) 'scheme-number)
        (begin
          (let ((result (make-rat (contents ii-int) 1)))
            (begin
              result
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (raise-rational->real rr-rat)
  (begin
    (if (eq? (type-tag rr-rat) 'rational)
        (begin
          (let ((rr (contents rr-rat)))
            (let ((rr-numer (numer rr))
                  (rr-denom (denom rr)))
              (begin
                (exact->inexact
                 (/ rr-numer rr-denom))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (raise-real->complex rr-real)
  (begin
    (if (eq? (type-tag rr-real) 'real)
        (begin
          (let ((result (make-complex (contents rr-real) 0)))
            (begin
              result
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-max-type t1 t2)
  (begin
    (cond
     ((eq? t1 'scheme-number)
      (begin
        t2
        ))
     ((eq? t2 'scheme-number)
      (begin
        t1
        ))
     ((eq? t1 'rational)
      (begin
        t2
        ))
     ((eq? t2 'rational)
      (begin
        t1
        ))
     ((eq? t1 'real)
      (begin
        t2
        ))
     ((eq? t2 'real)
      (begin
        t1
        ))
     (else
      (begin
        t1
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
        (begin
          (if proc
              (begin
                (drop (apply proc (map contents args))))
              (begin
                (if (= (length args) 2)
                    (begin
                      (let ((type1 (car type-tags))
                            (type2 (cadr type-tags))
                            (a1 (car args))
                            (a2 (cadr args)))
                        (let ((max-type (find-max-type type1 type2)))
                          (begin
                            (if (eq? max-type type1)
                                (begin
                                  (let ((next-a2
                                         ((get-coercion 'raise type2) a2)))
                                    (begin
                                      (apply-generic op a1 next-a2)
                                      )))
                                (begin
                                  (let ((next-a1
                                         ((get-coercion 'raise type1) a1)))
                                    (begin
                                      (apply-generic op next-a1 a2)
                                      ))
                                  ))
                            ))
                        ))
                    (begin
                      (display
                       (format
                        #f "apply-generic error: no method for these types ~a ~a~%"
                        op args))
                      (force-output)
                      (quit)
                      ))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (project-complex anum)
  (begin
    (let ((ttag (type-tag anum))
          (num (contents anum)))
      (let ((real ((get 'real-part '(complex)) num)))
        (begin
          real
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (project-real anum)
  (begin
    (let ((ttag (type-tag anum))
          (num (contents anum)))
      (let ((rat (inexact->exact num)))
        (let ((nn (numerator rat))
              (dd (denominator rat)))
          (begin
            ((get 'make 'rational) nn dd)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (project-rat anum)
  (begin
    (let ((ttag (type-tag anum))
          (num (contents anum)))
      (let ((numer (car num))
            (denom (cdr num)))
        (let ((result
               (inexact->exact (truncate (/ numer denom)))))
          (begin
            result
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (drop anum)
  (begin
    (let ((ttype (type-tag anum)))
      (begin
        (if (eq? ttype 'scheme-number)
            (begin
              anum)
            (begin
              (let ((lower-num
                     ((get-coercion 'project ttype) anum)))
                (let ((ntype (type-tag lower-num)))
                  (let ((raised-num
                         ((get-coercion 'raise ntype) lower-num)))
                    (begin
                      (if (not (eqn? anum raised-num))
                          (begin
                            anum)
                          (begin
                            (drop lower-num)
                            ))
                      ))
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "This section mentioned a method "))
    (display
     (format #f "for \"simplifying\"~%"))
    (display
     (format #f "a data object by lowering it in "))
    (display
     (format #f "the tower of types as~%"))
    (display
     (format #f "far as possible. Design a procedure "))
    (display
     (format #f "drop that accomplishes~%"))
    (display
     (format #f "this for the tower described in "))
    (display
     (format #f "exercise 2.83. The~%"))
    (display
     (format #f "key is to decide, in some general "))
    (display
     (format #f "way, whether an~%"))
    (display
     (format #f "object can be lowered. For example, "))
    (display
     (format #f "the complex number~%"))
    (display
     (format #f "1.5 + 0i can be lowered as far as "))
    (display
     (format #f "real, the complex number~%"))
    (display
     (format #f "1 + 0i can be lowered as far as "))
    (display
     (format #f "integer, and the complex~%"))
    (display
     (format #f "number 2 + 3i cannot be lowered at "))
    (display
     (format #f "all. Here is a~%"))
    (display
     (format #f "plan for determining whether an "))
    (display
     (format #f "object can be lowered:~%"))
    (display
     (format #f "Begin by defining a generic operation "))
    (display
     (format #f "project that \"pushes\"~%"))
    (display
     (format #f "an object down in the tower. For example, "))
    (display
     (format #f "projecting a complex~%"))
    (display
     (format #f "number would involve throwing away the "))
    (display
     (format #f "imaginary part. Then a~%"))
    (display
     (format #f "number can be dropped if, when we project "))
    (display
     (format #f "it and raise the~%"))
    (display
     (format #f "result back to the type we started with, "))
    (display
     (format #f "we end up with~%"))
    (display
     (format #f "something equal to what we started with. "))
    (display
     (format #f "Show how to implement~%"))
    (display
     (format #f "this idea in detail, by writing a drop "))
    (display
     (format #f "procedure that drops an~%"))
    (display
     (format #f "object as far as possible. You will need "))
    (display
     (format #f "to design the various~%"))
    (display
     (format #f "projection operations53 and install "))
    (display
     (format #f "project as a generic~%"))
    (display
     (format #f "operation in the system. You will also "))
    (display
     (format #f "need to make use of~%"))
    (display
     (format #f "a generic equality predicate, such as "))
    (display
     (format #f "described in exercise 2.79.~%"))
    (display
     (format #f "Finally, use drop to rewrite apply-generic "))
    (display
     (format #f "from exercise 2.84 so~%"))
    (display
     (format #f "that it \"simplifies\" its answers.~%"))
    (newline)
    (display
     (format #f "(define (project-complex anum)~%"))
    (display
     (format #f "  (let ((ttag (type-tag anum))~%"))
    (display
     (format #f "        (num (contents anum)))~%"))
    (display
     (format #f "    (let ((real ((get "))
    (display
     (format #f "'real-part '(complex)) num)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        real~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (project-real anum)~%"))
    (display
     (format #f "  (let ((ttag (type-tag anum))~%"))
    (display
     (format #f "        (num (contents anum)))~%"))
    (display
     (format #f "    (let ((rat (inexact->exact num)))~%"))
    (display
     (format #f "      (let ((nn (numerator rat))~%"))
    (display
     (format #f "            (dd (denominator rat)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          ((get 'make 'rational) nn dd)~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f "(define (project-rat anum)~%"))
    (display
     (format #f "  (let ((ttag (type-tag anum))~%"))
    (display
     (format #f "        (num (contents anum)))~%"))
    (display
     (format #f "    (let ((numer (car num))~%"))
    (display
     (format #f "          (denom (cdr num)))~%"))
    (display
     (format #f "      (let ((result~%"))
    (display
     (format #f "             (inexact->exact "))
    (display
     (format #f "(truncate (/ numer denom)))))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          result~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      )))~%"))
    (newline)
    (display
     (format #f "(define (drop anum)~%"))
    (display
     (format #f "  (let ((ttype (type-tag anum)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (if (eq? ttype 'scheme-number)~%"))
    (display
     (format #f "          anum~%"))
    (display
     (format #f "          (let ((lower-num "))
    (display
     (format #f "((get-coercion 'project ttype) anum)))~%"))
    (display
     (format #f "            (let ((ntype (type-tag lower-num)))~%"))
    (display
     (format #f "              (let ((raised-num "))
    (display
     (format #f "((get-coercion 'raise ntype) lower-num)))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (if (not (eqn? "))
    (display
     (format #f "anum raised-num))~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        anum)~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (drop lower-num)~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                  ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "(put-coercion 'project "))
    (display
     (format #f "'complex project-complex)~%"))
    (display
     (format #f "(put-coercion 'project "))
    (display
     (format #f "'real project-real)~%"))
    (display
     (format #f "(put-coercion 'project "))
    (display
     (format #f "'rational project-rational)~%"))
    (newline)
    (display
     (format #f "(define (apply-generic op . args)~%"))
    (display
     (format #f "  (let ((type-tags "))
    (display
     (format #f "(map type-tag args)))~%"))
    (display
     (format #f "    (let ((proc (get op type-tags)))~%"))
    (display
     (format #f "      (if proc~%"))
    (display
     (format #f "          (drop (apply proc "))
    (display
     (format #f "(map contents args)))~%"))
    (display
     (format #f "          (if (= (length args) 2)~%"))
    (display
     (format #f "              (let ((type1 "))
    (display
     (format #f "(car type-tags))~%"))
    (display
     (format #f "                    (type2 "))
    (display
     (format #f "(cadr type-tags))~%"))
    (display
     (format #f "                    (a1 (car args))~%"))
    (display
     (format #f "                    (a2 (cadr args)))~%"))
    (display
     (format #f "                (let ((max-type "))
    (display
     (format #f "(find-max-type type1 type2)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (if (eq? "))
    (display
     (format #f "max-type type1)~%"))
    (display
     (format #f "                        (begin~%"))
    (display
     (format #f "                          (let ((next-a2~%"))
    (display
     (format #f "                                 "))
    (display
     (format #f "((get-coercion 'raise type2) a2)))~%"))
    (display
     (format #f "                            (begin~%"))
    (display
     (format #f "                              "))
    (display
     (format #f "(apply-generic op a1 next-a2)~%"))
    (display
     (format #f "                              )))~%"))
    (display
     (format #f "                        (begin~%"))
    (display
     (format #f "                          (let ((next-a1~%"))
    (display
     (format #f "                                 "))
    (display
     (format #f "((get-coercion 'raise type1) a1)))~%"))
    (display
     (format #f "                            (begin~%"))
    (display
     (format #f "                              "))
    (display
     (format #f "(apply-generic op next-a1 a2)~%"))
    (display
     (format #f "                              ))~%"))
    (display
     (format #f "                          ))~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (display~%"))
    (display
     (format #f "                 (format~%"))
    (display
     (format #f "                  #f "))
    (display
     (format #f "\"apply-generic error: "))
    (display
     (format #f "no method for these types ~~a ~~a~~%\"~%"))
    (display
     (format #f "                  op args))~%"))
    (display
     (format #f "                (force-output)~%"))
    (display
     (format #f "                (quit)~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "    ))~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.85 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
