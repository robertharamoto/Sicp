#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.56                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-to-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (variable? x)
  (begin
    (symbol? x)
    ))

;;;#############################################################
;;;#############################################################
(define (same-variable? v1 v2)
  (begin
    (and (variable? v1) (variable? v2) (eq? v1 v2))
    ))

;;;#############################################################
;;;#############################################################
(define (=number? exp num)
  (begin
    (and (number? exp) (= exp num))
    ))

;;;#############################################################
;;;#############################################################
(define (make-sum a1 a2)
  (begin
    (cond
     ((=number? a1 0)
      (begin
        a2
        ))
     ((=number? a2 0)
      (begin
        a1
        ))
     ((and (number? a1) (number? a2))
      (begin
        (+ a1 a2)
        ))
     (else
      (begin
        (list '+ a1 a2)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (make-product m1 m2)
  (begin
    (cond
     ((or (=number? m1 0) (=number? m2 0))
      (begin
        0
        ))
     ((=number? m1 1)
      (begin
        m2
        ))
     ((=number? m2 1)
      (begin
        m1
        ))
     ((and (number? m1) (number? m2))
      (begin
        (* m1 m2)
        ))
     (else
      (begin
        (list '* m1 m2)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (sum? x)
  (begin
    (and (pair? x) (eq? (car x) '+))
    ))

;;;#############################################################
;;;#############################################################
(define (addend s)
  (begin
    (cadr s)
    ))

;;;#############################################################
;;;#############################################################
(define (augend s)
  (begin
    (caddr s)
    ))

;;;#############################################################
;;;#############################################################
(define (product? x)
  (begin
    (and (pair? x) (eq? (car x) '*))
    ))

;;;#############################################################
;;;#############################################################
(define (multiplier p)
  (begin
    (cadr p)
    ))

;;;#############################################################
;;;#############################################################
(define (multiplicand p)
  (begin
    (caddr p)
    ))

;;;#############################################################
;;;#############################################################
(define (exponentiation? expr)
  (begin
    (if (pair? expr)
        (begin
          (let ((oper (car expr)))
            (begin
              (and (string? oper)
                   (string-ci=? oper "**"))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-exponent base-expr exponent)
  (begin
    (cond
     ((and (number? base-expr)
           (equal? base-expr 0))
      (begin
        0
        ))
     ((and (number? base-expr)
           (equal? base-expr 1))
      (begin
        1
        ))
     ((and (number? exponent)
           (equal? exponent 0))
      (begin
        1
        ))
     ((and (number? exponent)
           (equal? exponent 1))
      (begin
        base-expr
        ))
     ((and (number? base-expr)
           (number? exponent))
      (begin
        (expt base-expr exponent)
        ))
     (else
      (begin
        (list "**" base-expr exponent)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (exponent-base expr)
  (begin
    (cadr expr)
    ))

;;;#############################################################
;;;#############################################################
(define (exponent-exponent expr)
  (begin
    (caddr expr)
    ))

;;;#############################################################
;;;#############################################################
;;; a^b = exp(b*ln(a))
;;; (d/dx)a^b = exp(b*ln(a))*((b/a)*da/dx+ db/dx*ln(a))
;;; = b*a^(b-1)*da/dx + ln(a)*a^b*db/dx
;;; assume exponent b a constant
(define (deriv exp var)
  (begin
    (cond
     ((number? exp)
      (begin
        0
        ))
     ((variable? exp)
      (begin
        (if (same-variable? exp var)
            (begin
              1)
            (begin
              0))
        ))
     ((sum? exp)
      (begin
        (make-sum (deriv (addend exp) var)
                  (deriv (augend exp) var))
        ))
     ((product? exp)
      (begin
        (make-sum
         (make-product (deriv (multiplier exp) var)
                       (multiplicand exp))
         (make-product (multiplier exp)
                       (deriv (multiplicand exp) var)))
        ))
     ((exponentiation? exp)
      (begin
        (let ((aa (exponent-base exp))
              (bb (exponent-exponent exp)))
          (begin
            (make-product
             bb
             (make-product
              (make-exponent aa (1- bb))
              (deriv aa var)))
            ))
        ))
     (else
      (begin
        (error "unknown expression type -- DERIV" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-deriv-1 result-hash-table)
 (begin
   (let ((sub-name "test-deriv-1")
         (test-list
          (list
           (list (list '+ 'x 1) 'x 1)
           (list (list '* 'x 'y) 'x 'y)
           (list (list '* (list '* 'x 'y) (list '+ 'x 3))
                 'x (list '+ (list '* 'y (list '+ 'x 3))
                          (list '* 'x 'y)))
           (list (list "**" 'x 2) 'x
                 (list '* 2 'x))
           (list (list "**" 'x 3) 'x
                 (list '* 3 (list "**" 'x 2)))
           (list (list "**" 'x 4) 'x
                 (list '* 4 (list "**" 'x 3)))
           (list (list "**" 'x 5) 'x
                 (list '* 5 (list "**" 'x 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((expr (list-ref this-list 0))
                  (var (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (deriv expr var)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : expr=~a, var=~a, "
                        sub-name test-label-index
                        expr var))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Show how to extend the basic "))
    (display
     (format #f "differentiator to handle~%"))
    (display
     (format #f "more kinds of expressions. For "))
    (display
     (format #f "instance, implement~%"))
    (display
     (format #f "the differentiation rule~%"))
    (display
     (format #f "  d(u^n)/dx = n*u^(n-1)*(du/dx)~%"))
    (display
     (format #f "by adding a new clause to the deriv "))
    (display
     (format #f "program and defining~%"))
    (display
     (format #f "appropriate procedures exponentiation? "))
    (display
     (format #f "base, exponent,~%"))
    (display
     (format #f "and make-exponentiation. (You may use "))
    (display
     (format #f "the symbol **~%"))
    (display
     (format #f "to denote exponentiation.) Build in "))
    (display
     (format #f "the rules that~%"))
    (display
     (format #f "anything raised to the power 0 is 1 "))
    (display
     (format #f "and anything raised~%"))
    (display
     (format #f "to the power 1 is the thing itself.~%"))
    (newline)
    (display
     (format #f "(define (deriv exp var)~%"))
    (display
     (format #f "  (cond ((number? exp) 0)~%"))
    (display
     (format #f "        ((variable? exp)~%"))
    (display
     (format #f "         (if (same-variable? "))
    (display
     (format #f "exp var) 1 0))~%"))
    (display
     (format #f "        ((sum? exp)~%"))
    (display
     (format #f "         (make-sum (deriv "))
    (display
     (format #f "(addend exp) var)~%"))
    (display
     (format #f "                   (deriv "))
    (display
     (format #f "(augend exp) var)))~%"))
    (display
     (format #f "        ((product? exp)~%"))
    (display
     (format #f "         (make-sum~%"))
    (display
     (format #f "          (make-product (deriv "))
    (display
     (format #f "(multiplier exp) var)~%"))
    (display
     (format #f "                        (multiplicand "))
    (display
     (format #f "exp))~%"))
    (display
     (format #f "          (make-product (multiplier "))
    (display
     (format #f "exp)~%"))
    (display
     (format #f "                        (deriv "))
    (display
     (format #f "(multiplicand exp) var))))~%"))
    (display
     (format #f "        ((exponentiation? exp)~%"))
    (display
     (format #f "         (begin~%"))
    (display
     (format #f "           (let ((aa (exponent-base "))
    (display
     (format #f "exp))~%"))
    (display
     (format #f "                 (bb (exponent-exponent "))
    (display
     (format #f "exp)))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "               (make-product~%"))
    (display
     (format #f "                bb~%"))
    (display
     (format #f "                (make-product~%"))
    (display
     (format #f "                 (make-exponent "))
    (display
     (format #f "aa (1- bb))~%"))
    (display
     (format #f "                 (deriv aa var)))~%"))
    (display
     (format #f "               ))~%"))
    (display
     (format #f "           ))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         (error \"unknown expression "))
    (display
     (format #f "type -- DERIV\" exp))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list (list "**" 'x 5) 'x)
            (list (list "**" (list '+ 'x 'y) 5) 'x)
            (list (list "**" (list '* 'x 3) 5) 'x)
            )))
      (begin
        (for-each
         (lambda (tlist)
           (begin
             (let ((expr (list-ref tlist 0))
                   (var (list-ref tlist 1)))
               (begin
                 (display
                  (format
                   #f "u=~a, du/dx = ~a~%"
                   expr (deriv expr var)))
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.56 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
