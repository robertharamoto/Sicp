#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.73                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (variable? x)
  (begin
    (symbol? x)
    ))

;;;#############################################################
;;;#############################################################
(define (same-variable? v1 v2)
  (begin
    (and (variable? v1)
         (variable? v2)
         (eq? v1 v2))
    ))

;;;#############################################################
;;;#############################################################
(define (=number? exp num)
  (begin
    (and (number? exp)
         (= exp num))
    ))

;;;#############################################################
;;;#############################################################
(define (make-sum a1 . a-rest)
  (begin
    (if (number? a-rest)
        (begin
          (cond
           ((=number? a1 0)
            (begin
              a-rest
              ))
           ((=number? a-rest 0)
            (begin
              a1
              ))
           ((number? a1)
            (begin
              (+ a1 a2)
              ))
           (else
            (begin
              (list a1 '+ a2)
              ))
           ))
        (begin
          (cond
           ((null? a1)
            (begin
              0
              ))
           ((null? a-rest)
            (begin
              a1
              ))
           ((or (null? (car a-rest))
                (and (number? (car a-rest))
                     (zero? (car a-rest))))
            (begin
              a1
              ))
           ((and (=number? a1 0)
                 (<= (length a-rest) 1))
            (begin
              (car a-rest)
              ))
           ((=number? a1 0)
            (begin
              (cons (car a-rest) (cons '+ (cdr a-rest)))
              ))
           ((and (number? a1)
                 (number? (car a-rest)))
            (begin
              (+ a1 (car a-rest))
              ))
           (else
            (begin
              (cons a1 (cons '+ a-rest))
              )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-product m1 . m-rest)
  (begin
    (if (number? m-rest)
        (begin
          (cond
           ((or (=number? m1 0)
                (=number? m-rest 0))
            (begin
              0
              ))
           ((=number? m1 1)
            (begin
              m-rest
              ))
           ((=number? m-rest 1)
            (begin
              m1
              ))
           ((and (number? m1)
                 (number? m-rest))
            (begin
              (* m1 m-rest)
              ))
           (else
            (begin
              (list m1 '* m-rest)
              ))
           ))
        (begin
          (cond
           ((null? m1)
            (begin
              0
              ))
           ((null? m-rest)
            (begin
              m1
              ))
           ((=number? m1 0)
            (begin
              0
              ))
           ((and (=number? m1 1)
                 (pair? m-rest))
            (begin
              (make-product (car m-rest) (cdr m-rest))
              ))
           ((=number? m1 1)
            (begin
              m-rest
              ))
           ((and (number? (car m-rest))
                 (zero? (car m-rest)))
            (begin
              0
              ))
           ((and (number? (car m-rest))
                 (eq? (car m-rest) 1))
            (begin
              m1
              ))
           ((and (not (number? m1))
                 (null? (car m-rest)))
            (begin
              m1
              ))
           (else
            (begin
              (cons m1 (cons '* m-rest))
              )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (sum? x)
  (begin
    (and (pair? x)
         (eq? (cadr x) '+))
    ))

;;;#############################################################
;;;#############################################################
(define (addend s)
  (begin
    (car s)
    ))

;;;#############################################################
;;;#############################################################
(define (augend s)
  (begin
    (if (<= (length (cddr s)) 1)
        (begin
          (caddr s))
        (begin
          (cddr s)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (product? x)
  (begin
    (and (pair? x)
         (eq? (cadr x) '*))
    ))

;;;#############################################################
;;;#############################################################
(define (multiplier p)
  (begin
    (car p)
    ))

;;;#############################################################
;;;#############################################################
(define (multiplicand p)
  (begin
    (if (<= (length (cddr p)) 1)
        (begin
          (caddr p))
        (begin
          (cddr p)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (exponentiation? expr)
  (begin
    (if (pair? expr)
        (begin
          (let ((oper (cadr expr)))
            (begin
              (and (string? oper)
                   (string-ci=? oper "**"))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-exponent base-expr exponent)
  (begin
    (cond
     ((and (number? base-expr)
           (equal? base-expr 0))
      (begin
        0
        ))
     ((and (number? base-expr)
           (equal? base-expr 1))
      (begin
        1
        ))
     ((and (number? exponent)
           (equal? exponent 0))
      (begin
        1
        ))
     ((and (number? exponent)
           (equal? exponent 1))
      (begin
        base-expr
        ))
     ((and (number? base-expr)
           (number? exponent))
      (begin
        (expt base-expr exponent)
        ))
     (else
      (begin
        (list base-expr "**" exponent)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (exponent-base expr)
  (begin
    (car expr)
    ))

;;;#############################################################
;;;#############################################################
(define (exponent-exponent expr)
  (begin
    (caddr expr)
    ))

;;;#############################################################
;;;#############################################################
(define (deriv exp var)
  (begin
    (cond
     ((number? exp)
      (begin
        0
        ))
     ((variable? exp)
      (begin
        (if (same-variable? exp var)
            (begin
              1)
            (begin
              0
              ))
        ))
     ((sum? exp)
      (begin
        (make-sum (deriv (addend exp) var)
                  (deriv (augend exp) var))
        ))
     ((product? exp)
      (begin
        (let ((uu (multiplier exp))
              (vv (multiplicand exp)))
          (let ((term1 (make-product (deriv uu var) vv))
                (term2 (make-product uu (deriv vv var))))
            (begin
              (make-sum term1 term2)
              )))
        ))
     ((exponentiation? exp)
      (begin
;;; a^b = exp(b*ln(a))
;;; (d/dx)a^b = exp(b*ln(a))*((b/a)*da/dx+ db/dx*ln(a))
;;; = b*a^(b-1)*da/dx + ln(a)*a^b*db/dx
;;; assume exponent b a constant
        (let ((aa (exponent-base exp))
              (bb (exponent-exponent exp)))
          (begin
            (make-product
             bb
             (make-product
              (make-exponent aa (1- bb))
              (deriv aa var)))
            ))
        ))
     (else
      (begin
        (display
         (format
          #f "unknown expression type -- "))
        (display
         (format
          #f "DERIV expresstion = ~a~%" exp))
        (force-output)
        (quit)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-deriv-1 result-hash-table)
 (begin
   (let ((sub-name "test-deriv-1")
         (test-list
          (list
           (list (list 'x '+ 1) 'x 1)
           (list (list 'x '+ 'y) 'x 1)
           (list (list 'x '* 'y) 'x 'y)
           (list (list 'x '* (list 'y '* 'z)) 'x (list 'y '* 'z))
           (list (list (list 'x '* 'y) '* (list 'x '+ 3))
                 'x (list (list 'y '* (list 'x '+ 3)) '+
                          (list 'x '* 'y)))
           (list (list 'x "**" 5) 'x
                 (list 5 '* (list 'x "**" 4)))
           (list (list 'x '* (list 'y '* (list 'x '+ 3))) 'x
                 (list (list 'y '* (list 'x '+ 3)) '+ (list 'x '* 'y)))
           (list (list 'x '* 'y '* (list 'x '+ 3)) 'x
                 (list (list 'y '* (list 'x '+ 3)) '+ (list 'x '* 'y)))
           (list (list 'x '+ 3 '* (list 'x '+ 'y '+ 2)) 'x
                 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((expr (list-ref this-list 0))
                  (var (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (deriv expr var)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : expr=~a, "
                        sub-name test-label-index expr))
                      (err-msg-2
                       (format
                        #f "var=~a, shouldbe=~a, result=~a"
                        var shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Section 2.3.2 described a program that "))
    (display
     (format #f "performs symbolic~%"))
    (display
     (format #f "differentiation:~%"))
    (newline)
    (display
     (format #f "(define (deriv exp var)~%"))
    (display
     (format #f "  (cond ((number? exp) 0)~%"))
    (display
     (format #f "        ((variable? exp) "))
    (display
     (format #f "(if (same-variable? exp var) 1 0))~%"))
    (display
     (format #f "        ((sum? exp)~%"))
    (display
     (format #f "         (make-sum "))
    (display
     (format #f "(deriv (addend exp) var)~%"))
    (display
     (format #f "                   (deriv "))
    (display
     (format #f "(augend exp) var)))~%"))
    (display
     (format #f "        ((product? exp)~%"))
    (display
     (format #f "         (make-sum~%"))
    (display
     (format #f "           (make-product "))
    (display
     (format #f "(multiplier exp)~%"))
    (display
     (format #f "                         (deriv "))
    (display
     (format #f "(multiplicand exp) var))~%"))
    (display
     (format #f "           (make-product (deriv "))
    (display
     (format #f "(multiplier exp) var)~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(multiplicand exp))))~%"))
    (display
     (format #f "        <more rules can be added here>~%"))
    (display
     (format #f "        (else (error "))
    (display
     (format #f "\"unknown expression type~%"))
    (display
     (format #f "         \"-- DERIV\" exp))))~%"))
    (display
     (format #f "We can regard this program as "))
    (display
     (format #f "performing a dispatch~%"))
    (display
     (format #f "on the type of the expression to "))
    (display
     (format #f "be differentiated. In~%"))
    (display
     (format #f "this situation the \"type tag\" of "))
    (display
     (format #f "the datum is the~%"))
    (display
     (format #f "algebraic operator symbol (such "))
    (display
     (format #f "as +) and the operation~%"))
    (display
     (format #f "being performed is deriv. We can "))
    (display
     (format #f "transform this program~%"))
    (display
     (format #f "into data-directed style by "))
    (display
     (format #f "rewriting the basic~%"))
    (display
     (format #f "derivative procedure as:~%"))
    (display
     (format #f "(define (deriv exp var)~%"))
    (display
     (format #f "   (cond ((number? exp) 0)~%"))
    (display
     (format #f "         ((variable? exp) (if "))
    (display
     (format #f "(same-variable? exp var) 1 0))~%"))
    (display
     (format #f "         (else ((get 'deriv "))
    (display
     (format #f "(operator exp)) (operands exp)~%"))
    (display
     (format #f "                                            "))
    (display
     (format #f "var))))~%"))
    (display
     (format #f "(define (operator exp) (car exp))~%"))
    (display
     (format #f "(define (operands exp) (cdr exp))~%"))
    (display
     (format #f "a.  Explain what was done above. "))
    (display
     (format #f "Why can't we assimilate~%"))
    (display
     (format #f "the predicates number? and "))
    (display
     (format #f "same-variable? into the~%"))
    (display
     (format #f "data-directed dispatch?~%"))
    (display
     (format #f "b.  Write the procedures for "))
    (display
     (format #f "derivatives of sums and~%"))
    (display
     (format #f "products, and the auxiliary code "))
    (display
     (format #f "required to install~%"))
    (display
     (format #f "them in the table used by the "))
    (display
     (format #f "program above.~%"))
    (display
     (format #f "c.  Choose any additional differentiation "))
    (display
     (format #f "rule that~%"))
    (display
     (format #f "you like, such as the one for "))
    (display
     (format #f "exponents (exercise 2.56),~%"))
    (display
     (format #f "and install it in this "))
    (display
     (format #f "data-directed system.~%"))
    (display
     (format #f "d.  In this simple algebraic manipulator "))
    (display
     (format #f "the type~%"))
    (display
     (format #f "of an expression is the algebraic "))
    (display
     (format #f "operator that binds~%"))
    (display
     (format #f "it together. Suppose, however, "))
    (display
     (format #f "we indexed the procedures~%"))
    (display
     (format #f "in the opposite way, so that the "))
    (display
     (format #f "dispatch line in deriv~%"))
    (display
     (format #f "looked like:~%"))
    (display
     (format #f "((get (operator exp) 'deriv) (operands exp) var)~%"))
    (display
     (format #f "What corresponding changes to the "))
    (display
     (format #f "derivative system are~%"))
    (display
     (format #f "required?~%"))
    (newline)
    (display
     (format #f "(a) the derive function is recursive, "))
    (display
     (format #f "and the number? and~%"))
    (display
     (format #f "variable conditions are there to "))
    (display
     (format #f "stop the recursion, once~%"))
    (display
     (format #f "there are no more terms to process.~%"))
    (newline)
    (display
     (format #f "(b)~%"))
    (display
     (format #f "(define (install-deriv-package)~%"))
    (display
     (format #f "  (define (variable? x) (symbol? x))~%"))
    (display
     (format #f "  (define (same-variable? v1 v2)~%"))
    (display
     (format #f "    (and (variable? v1) (variable? v2) "))
    (display
     (format #f "(eq? v1 v2)))~%"))
    (display
     (format #f "  (define (=number? exp num)~%"))
    (display
     (format #f "    (and (number? exp) (= exp num)))~%"))
    (display
     (format #f "  (define (make-sum a1 . a-rest)~%"))
    (display
     (format #f "    ( ... ))~%"))
    (display
     (format #f "  (define (make-product m1 . m-rest)~%"))
    (display
     (format #f "    ( ... ))~%"))
    (display
     (format #f "  (define (sum? x)~%"))
    (display
     (format #f "    (and (pair? x) (eq? (cadr x) '+)))~%"))
    (display
     (format #f "  (define (addend s) (car s))~%"))
    (display
     (format #f "  (define (augend s)~%"))
    (display
     (format #f "    (if (<= (length (cddr s)) 1)~%"))
    (display
     (format #f "        (caddr s)~%"))
    (display
     (format #f "        (cddr s)))~%"))
    (display
     (format #f "  (define (product? x)~%"))
    (display
     (format #f "    (and (pair? x) (eq? (cadr x) '*)))~%"))
    (display
     (format #f "  (define (multiplier p) (car p))~%"))
    (display
     (format #f "  (define (multiplicand p)~%"))
    (display
     (format #f "    (if (<= (length (cddr p)) 1)~%"))
    (display
     (format #f "        (caddr p)~%"))
    (display
     (format #f "        (cddr p)))~%"))
    (display
     (format #f "  (define (deriv exp var)~%"))
    (display
     (format #f "     (cond ((number? exp) 0)~%"))
    (display
     (format #f "           ((variable? exp) (if "))
    (display
     (format #f "(same-variable? exp var) 1 0))~%"))
    (display
     (format #f "           (else ((get 'deriv "))
    (display
     (format #f "(operator exp)) (operands exp)~%"))
    (display
     (format #f "                                             "))
    (display
     (format #f "var))))~%"))
    (display
     (format #f "  (define (operator exp) (car exp))~%"))
    (display
     (format #f "  (define (operands exp) (cdr exp))~%"))
    (display
     (format #f "  (put 'deriv '+~%"))
    (display
     (format #f "    (lambda (expr var) (make-sum "))
    (display
     (format #f "(deriv (adden expr) var)~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "(deriv (augend expr) var))))~%"))
    (display
     (format #f "  (put 'deriv '*~%"))
    (display
     (format #f "    (lambda (expr var) (make-product "))
    (display
     (format #f "(deriv (multiplier expr) var)~%"))
    (display
     (format #f "                                 "))
    (display
     (format #f "(deriv (multiplicand expr) var))))~%"))
    (display
     (format #f "  'done)~%"))
    (newline)
    (display
     (format #f "(c)~%"))
    (display
     (format #f "(define (install-deriv-package)~%"))
    (display
     (format #f "  ( ... )~%"))
    (display
     (format #f "  (define (exponentiation? expr)~%"))
    (display
     (format #f "    ( ... ))~%"))
    (display
     (format #f "  (define (make-exponent base-expr exponent)~%"))
    (display
     (format #f "    ( ... ))~%"))
    (display
     (format #f "  (define (exponent-base expr) (car expr))~%"))
    (display
     (format #f "  (define (exponent-exponent expr) (cadr expr))~%"))
    (display
     (format #f "~%"))
    (display
     (format #f "~%"))
    (display
     (format #f "  (define (deriv exp var)~%"))
    (display
     (format #f "     (cond ((number? exp) 0)~%"))
    (display
     (format #f "           ((variable? exp) "))
    (display
     (format #f "(if (same-variable? exp var) 1 0))~%"))
    (display
     (format #f "           (else ((get 'deriv "))
    (display
     (format #f "(operator exp)) (operands exp)~%"))
    (display
     (format
      #f "                                             "))
    (display
     (format #f "var))))~%"))
    (display
     (format #f "  (define (operator exp) (car exp))~%"))
    (display
     (format #f "  (define (operands exp) (cdr exp))~%"))
    (display
     (format #f "  (put 'deriv '+~%"))
    (display
     (format #f "    (lambda (expr var) (make-sum "))
    (display
     (format #f "(deriv (adden expr) var)~%"))
    (display
     (format #f "                            (deriv "))
    (display
     (format #f "(augend expr) var))))~%"))
    (display
     (format #f "  (put 'deriv '*~%"))
    (display
     (format #f "    (lambda (expr var) (make-product "))
    (display
     (format #f "(deriv (multiplier expr) var)~%"))
    (display
     (format #f "                                 "))
    (display
     (format #f "(deriv (multiplicand expr) var))))~%"))
    (display
     (format #f "  (put 'deriv \"**\"~%"))
    (display
     (format #f "    (lambda (expr var)~%"))
    (display
     (format #f "      (let ((aa (exponent-base expr))~%"))
    (display
     (format #f "            (bb (exponent-exponent expr)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (make-product~%"))
    (display
     (format #f "            bb~%"))
    (display
     (format #f "            (make-product~%"))
    (display
     (format #f "              (make-exponent~%"))
    (display
     (format #f "                aa (1- bb))~%"))
    (display
     (format #f "              ((get 'deriv "))
    (display
     (format #f "(operator aa)) (operands aa) var)~%"))
    (display
     (format #f "              ))))))~%"))
    (display
     (format #f "  'done)~%"))
    (newline)
    (display
     (format #f "(d) the only difference would be "))
    (display
     (format #f "to reverse the order~%"))
    (display
     (format #f "of the symbols when calling put.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.73 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
