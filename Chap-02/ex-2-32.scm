#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.32                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (subsets s)
  (begin
    (if (null? s)
        (begin
          (list (list)))
        (begin
          (let ((rest (subsets (cdr s))))
            (begin
              (append
               rest (map (lambda (t) (cons (car s) t)) rest))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-sets-are-equal
         shouldbe-set result-set
         sub-name err-msg result-hash-table)
  (begin
    (for-each
     (lambda (slist)
       (begin
         (let ((sflag (member slist result-set)))
           (begin
             (let ((err-msg-1
                    (format
                     #f "~a : looking for shouldbe element ~a"
                     err-msg slist)))
               (begin
                 (unittest2:assert?
                  (not (equal? sflag #f))
                  sub-name
                  err-msg-1
                  result-hash-table)
                 ))
             ))
         )) shouldbe-set)

    (for-each
     (lambda (rlist)
       (begin
         (let ((rflag (member rlist shouldbe-set)))
           (begin
             (let ((err-msg-1
                    (format
                     #f "~a : looking for result element ~a"
                     err-msg rlist)))
               (begin
                 (unittest2:assert?
                  (not (equal? rflag #f))
                  sub-name
                  err-msg-1
                  result-hash-table)
                 ))
             ))
         )) result-set)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-subsets-1 result-hash-table)
 (begin
   (let ((sub-name "test-subsets-1")
         (test-list
          (list
           (list (list 1 2)
                 (list (list) (list 1) (list 2) (list 1 2)))
           (list (list 1 2 3)
                 (list (list) (list 1) (list 2) (list 3)
                       (list 1 2) (list 1 3) (list 2 3)
                       (list 1 2 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-set (list-ref this-list 0))
                  (shouldbe-set (list-ref this-list 1)))
              (let ((result-set (subsets input-set)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : input set=~a, "
                        sub-name test-label-index input-set))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-set result-set)))
                  (let ((error-message
                         (string-append err-msg-1 err-msg-2)))
                    (begin
                      (if (and (list? shouldbe-set)
                               (list? result-set))
                          (begin
                            (assert-sets-are-equal
                             shouldbe-set result-set
                             sub-name
                             error-message
                             result-hash-table))
                          (begin
                            (unittest2:assert?
                             (equal? shouldbe result)
                             sub-name
                             error-message
                             result-hash-table)
                            ))
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "We can represent a set as a list of "))
    (display
     (format #f "distinct elements, and~%"))
    (display
     (format #f "we can represent the set of all "))
    (display
     (format #f "subsets of the set~%"))
    (display
     (format #f "as a list of lists. For example, if "))
    (display
     (format #f "the set is (1 2 3),~%"))
    (display
     (format #f "then the set of all subsets is~%"))
    (display
     (format #f "(() (3) (2) (2 3) (1) "))
    (display
     (format #f "(1 3) (1 2) (1 2 3)).~%"))
    (display
     (format #f "Complete the following definition of "))
    (display
     (format #f "a procedure that~%"))
    (display
     (format #f "generates the set of subsets of a set "))
    (display
     (format #f "and give a~%"))
    (display
     (format #f "clear explanation of why it works:~%"))
    (display
     (format #f "(define (subsets s)~%"))
    (display
     (format #f "  (if (null? s)~%"))
    (display
     (format #f "      (list (list))~%"))
    (display
     (format #f "      (let ((rest (subsets (cdr s))))~%"))
    (display
     (format #f "        (append rest (map <??> rest)))))~%"))
    (newline)
    (display
     (format #f "<??> = (lambda (t) (cons (car s) t))~%"))

    (newline)
    (display
     (format #f "Consider the case of s=(list 1 2 3):~%"))
    (display
     (format #f "The line (let ((rest (subsets (cdr s))))~%"))
    (display
     (format #f "causes the program to repeatedly "))
    (display
     (format #f "call subsets with~%"))
    (display
     (format #f "smaller and smaller s, (list 2 3), "))
    (display
     (format #f "(list 3), until s~%"))
    (display
     (format #f "becomes null at which point, it "))
    (display
     (format #f "returns (list (list)).~%"))
    (display
     (format #f "When it returns, the set s = "))
    (display
     (format #f "(list 3), and rest~%"))
    (display
     (format #f "= (list (list)), so applying the "))
    (display
     (format #f "function~%"))
    (display
     (format #f "(lambda (t) (cons "))
    (display
     (format #f "(car (list 3)) t)) to~%"))
    (display
     (format #f "(list (list)) "))
    (display
     (format #f "gives the pair (cons 3 (list)),~%"))
    (display
     (format #f "which is appended to (list (list)),~%"))
    (display
     (format #f "so at that point, the result is "))
    (display
     (format #f "(list (list) (list 3)).~%"))
    (display
     (format #f "That result gets returned as the next "))
    (display
     (format #f "rest, one level~%"))
    (display
     (format #f "previous, where s = (list 2 3). "))
    (display
     (format #f "Then append~%"))
    (display
     (format #f "(list (list) (list 3)) with the~%"))
    (display
     (format #f "(list (cons 2 (list)) "))
    (display
     (format #f "(cons 2 (list 3))),~%"))
    (display
     (format #f "gives (list (list) (list 3) "))
    (display
     (format #f "(list 2) (list 2 3)).~%"))
    (display
     (format #f "Finally, at the top level, "))
    (display
     (format #f "s=(list 1 2 3), and rest =~%"))
    (display
     (format #f "(list (list) (list 3) "))
    (display
     (format #f "(list 2) (list 2 3)),~%"))
    (display
     (format #f "append rest to each element of rest "))
    (display
     (format #f "with 1 cons to~%"))
    (display
     (format #f "it, gives (() (3) (2) (2 3) (1) "))
    (display
     (format #f "(1 3) (1 2) (1 2 3))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((alist (list 1 2 3)))
      (begin
        (display
         (format #f "(subsets ~a) = ~a~%"
                 alist (subsets alist)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.32 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
