#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.31                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (tree-iter proc atree)
  (define (local-iter atree acc-tree)
    (begin
      (if (null? atree)
          (begin
            (reverse acc-tree))
          (begin
            (let ((elem (car atree))
                  (tail-list (cdr atree)))
              (begin
                (if (list? elem)
                    (begin
                      (let ((elist (local-iter elem (list))))
                        (begin
                          (set! acc-tree (cons elist acc-tree))
                          )))
                    (begin
                      (let ((e2 (proc elem)))
                        (begin
                          (set! acc-tree (cons e2 acc-tree))
                          ))
                      ))
                (local-iter tail-list acc-tree)
                ))
            ))
      ))
  (begin
    (local-iter atree (list))
    ))

;;;#############################################################
;;;#############################################################
(define (tree-map proc atree)
  (begin
    (map (lambda (sub-tree)
           (begin
             (if (pair? sub-tree)
                 (square-tree-map sub-tree)
                 (proc sub-tree))
             )) atree)
    ))

;;;#############################################################
;;;#############################################################
(define (square x)
  (begin
    (* x x)
    ))

;;;#############################################################
;;;#############################################################
(define (square-tree-iter tree)
  (begin
    (tree-iter square tree)
    ))

;;;#############################################################
;;;#############################################################
(define (assert-trees-are-equal
         shouldbe-tree result-tree
         sub-name err-msg result-hash-table)
  (begin
    (let ((sfirst (car shouldbe-tree))
          (srest (cdr shouldbe-tree))
          (rfirst (car result-tree))
          (rrest (cdr result-tree)))
      (begin
        (if (and (not (list? sfirst))
                 (not (list? rfirst)))
            (begin
              (let ((err-msg-1
                     (format
                      #f "~a : shouldbe=~a, result=~a"
                      err-msg sfirst rfirst)))
                (begin
                  (unittest2:assert?
                   (equal? sfirst rfirst)
                   sub-name
                   err-msg-1
                   result-hash-table)
                  ))
              ))

        (if (and (not (equal? srest (list)))
                 (not (equal? rrest (list))))
            (begin
              (assert-trees-are-equal
               srest rrest sub-name err-msg
               result-hash-table)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-square-tree-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-square-tree-iter-1")
         (test-list
          (list
           (list (list 1 (list 2 (list 3 4)))
                 (list 1 (list 4 (list 9 16))))
           (list (list 1 (list 2 (list 3 4) 5))
                 (list 1 (list 4 (list 9 16) 25)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((atree (list-ref this-list 0))
                  (shouldbe-tree (list-ref this-list 1)))
              (let ((result-tree (square-tree-iter atree)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : atree=~a, "
                        sub-name test-label-index
                        atree))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-tree result-tree)))
                  (let ((error-message
                         (string-append err-msg-1 err-msg-2)))
                    (begin
                      (if (and (list? shouldbe-tree)
                               (list? result-tree))
                          (begin
                            (assert-trees-are-equal
                             shouldbe-tree result-tree
                             sub-name
                             error-message
                             result-hash-table))
                          (begin
                            (unittest2:assert?
                             (equal? shouldbe result)
                             sub-name
                             error-message
                             result-hash-table)
                            ))
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (square-tree-map tree)
  (begin
    (tree-map square tree)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-square-tree-map-1 result-hash-table)
 (begin
   (let ((sub-name "test-square-tree-map-1")
         (test-list
          (list
           (list (list 1 (list 2 (list 3 4)))
                 (list 1 (list 4 (list 9 16))))
           (list (list 1 (list 2 (list 3 4) 5))
                 (list 1 (list 4 (list 9 16) 25)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((atree (list-ref this-list 0))
                  (shouldbe-tree (list-ref this-list 1)))
              (let ((result-tree (square-tree-map atree)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : atree=~a, "
                        sub-name test-label-index
                        atree))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-tree result-tree)))
                  (let ((error-message
                         (string-append err-msg-1 err-msg-2)))
                    (begin
                      (if (and (list? shouldbe-tree)
                               (list? result-tree))
                          (begin
                            (assert-trees-are-equal
                             shouldbe-tree result-tree
                             sub-name
                             error-message
                             result-hash-table))
                          (begin
                            (unittest2:assert?
                             (equal? shouldbe result)
                             sub-name
                             error-message
                             result-hash-table)
                            ))
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Abstract your answer to exercise 2.30 "))
    (display
     (format #f "to produce a~%"))
    (display
     (format #f "procedure tree-map with the property "))
    (display
     (format #f "that square-tree~%"))
    (display
     (format #f "could be defined as~%"))
    (newline)
    (display
     (format #f "(define (square-tree tree) "))
    (display
     (format #f "(tree-map square tree))~%"))
    (newline)
    (display
     (format #f "(define (tree-map proc atree)~%"))
    (display
     (format #f "  (map (lambda (sub-tree)~%"))
    (display
     (format #f "         (begin~%"))
    (display
     (format #f "           (if (pair? sub-tree)~%"))
    (display
     (format #f "               (square-tree-map "))
    (display
     (format #f "sub-tree)~%"))
    (display
     (format #f "               (proc sub-tree))~%"))
    (display
     (format #f "           )) atree))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((atree
           (list
            1 (list 2 (list 3 4) 5) (list 6 7))))
      (begin
        (display
         (format #f "(square-tree-iter ~a)~%    = ~a~%"
                 atree (square-tree-iter atree)))
        (display
         (format #f "(square-tree-map ~a)~%    = ~a~%"
                 atree (square-tree-map atree)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.31 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
