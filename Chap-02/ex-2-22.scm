#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.22                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define nil (list))

;;;#############################################################
;;;#############################################################
(define (square x)
  (begin
    (* x x)
    ))

;;;#############################################################
;;;#############################################################
(define (square-list items)
  (define (iter things answer)
    (begin
      (if (null? things)
          (begin
            answer)
          (begin
            (iter
             (cdr things)
             (append answer
                     (list (square (car things)))))
            ))
      ))
  (begin
    (iter items nil)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-square-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-square-list-1")
         (test-list
          (list
           (list (list 1 2) (list 1 4))
           (list (list 1 2 3) (list 1 4 9))
           (list (list 2 3 4 5 ) (list 4 9 16 25))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (square-list alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner tries to rewrite the "))
    (display
     (format #f "first square-list~%"))
    (display
     (format #f "procedure of exercise 2.21 so that "))
    (display
     (format #f "it evolves an~%"))
    (display
     (format #f "iterative process:~%"))
    (newline)
    (display
     (format #f "(define (square-list items)~%"))
    (display
     (format #f "  (define (iter things answer)~%"))
    (display
     (format #f "    (if (null? things)~%"))
    (display
     (format #f "        answer~%"))
    (display
     (format #f "        (iter (cdr things)~%"))
    (display
     (format #f "              (cons (square (car things))~%"))
    (display
     (format #f "                    answer))))~%"))
    (display
     (format #f "  (iter items nil))~%"))
    (display
     (format #f "Unfortunately, defining square-list "))
    (display
     (format #f "this way produces~%"))
    (display
     (format #f "the answer list in the reverse order "))
    (display
     (format #f "of the one~%"))
    (display
     (format #f "desired. Why?~%"))
    (newline)

    (display
     (format #f "By tracking answer, we can see why "))
    (display
     (format #f "the resulting list~%"))
    (display
     (format #f "is in reverse order. Consider~%"))
    (display
     (format #f "  (square-list (list 1 2 3 4)):~%"))
    (display
     (format #f "The answer starts as nil, then after "))
    (display
     (format #f "the first call~%"))
    (display
     (format #f "it becomes (1 nil), then (4 1 nil), "))
    (display
     (format #f "then (9 4 1 nil),~%"))
    (display
     (format #f "and finally (16 9 4 1 nil).~%"))
    (newline)
    (display
     (format #f "Louis then tries to fix his bug by "))
    (display
     (format #f "interchanging the~%"))
    (display
     (format #f "arguments to cons:~%"))
    (newline)
    (display
     (format #f "(define (square-list items)~%"))
    (display
     (format #f "  (define (iter things answer)~%"))
    (display
     (format #f "    (if (null? things)~%"))
    (display
     (format #f "        answer~%"))
    (display
     (format #f "        (iter (cdr things)~%"))
    (display
     (format #f "              (cons answer~%"))
    (display
     (format #f "                    (square "))
    (display
     (format #f "(car things))))))~%"))
    (display
     (format #f "  (iter items nil))~%"))
    (newline)
    (display
     (format #f "Consider (square-list (list 1 2 3 4)): "))
    (display
     (format #f "The answer~%"))
    (display
     (format #f "starts as nil, then after the first "))
    (display
     (format #f "call it becomes~%"))
    (display
     (format #f "(list 1), then (list 1 4), then "))
    (display
     (format #f "(list 1 4 9), and finally~%"))
    (display
     (format #f "(list 1 4 9 16).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format
      #f "(square-list (list 1 2 3 4)) = ~a~%"
      (square-list (list 1 2 3 4))))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.22 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
