#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.1                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-rat n d)
  (begin
    (let ((g (gcd n d)))
      (begin
        (if (negative? d)
            (begin
              (set! n (* -1 n))
              (set! d (* -1 d))
              ))

        (let ((final-numer (/ n g))
              (final-denom (/ d g)))
          (begin
            (cons final-numer final-denom)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-rat-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-rat-1")
         (test-list
          (list
           (list 1 10 (cons 1 10))
           (list 2 10 (cons 1 5))
           (list 12 24 (cons 1 2))
           (list 1 -10 (cons -1 10))
           (list -1 10 (cons -1 10))
           (list -1 -10 (cons 1 10))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((numer (list-ref this-list 0))
                  (denom (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (make-rat numer denom)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : numer=~a, denom=~a, "
                        sub-name test-label-index numer denom))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (numer x)
  (begin
    (car x)
    ))

;;;#############################################################
;;;#############################################################
(define (denom x)
  (begin
    (cdr x)
    ))

;;;#############################################################
;;;#############################################################
(define (print-rat x)
  (begin
    (display
     (format #f "~a / ~a" (numer x) (denom x)))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list 1 2) (list -1 2) (list 1 -3) (list -1 -4)
            (list 3 4) (list 3 9) (list 8 -12) (list 9 -45))))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((numer (list-ref alist 0))
                   (denom (list-ref alist 1)))
               (let ((rr (make-rat numer denom)))
                 (begin
                   (display
                    (format #f "(~a, ~a) = " numer denom))
                   (print-rat rr)
                   (newline)
                   )))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a better version of make-rat that "))
    (display
     (format #f "handles both positive~%"))
    (display
     (format #f "and negative arguments. Make-rat should "))
    (display
     (format #f "normalize the sign~%"))
    (display
     (format #f "so that if the rational number is "))
    (display
     (format #f "positive, both the~%"))
    (display
     (format #f "numerator and denominator are positive, "))
    (display
     (format #f "and if the rational~%"))
    (display
     (format #f "number is negative, only the "))
    (display
     (format #f "numerator is negative.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.01 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
