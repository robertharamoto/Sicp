#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.57                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-to-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (variable? x)
  (begin
    (symbol? x)
    ))

;;;#############################################################
;;;#############################################################
(define (same-variable? v1 v2)
  (begin
    (and (variable? v1)
         (variable? v2)
         (eq? v1 v2))
    ))

;;;#############################################################
;;;#############################################################
(define (=number? exp num)
  (begin
    (and (number? exp) (= exp num))
    ))

;;;#############################################################
;;;#############################################################
(define (make-sum a1 . a-rest)
  (begin
    (if (number? a-rest)
        (begin
          (cond
           ((=number? a1 0)
            (begin
              a-rest
              ))
           ((=number? a-rest 0)
            (begin
              a1
              ))
           ((number? a1)
            (begin
              (+ a1 a2)
              ))
           (else
            (begin
              (list '+ a1 a2)
              ))
           ))
        (begin
          (cond
           ((null? a1)
            (begin
              0
              ))
           ((null? a-rest)
            (begin
              a1
              ))
           ((or (null? (car a-rest))
                (and (number? (car a-rest))
                     (zero? (car a-rest))))
            (begin
              a1
              ))
           ((and (=number? a1 0)
                 (<= (length a-rest) 1))
            (begin
              (car a-rest)
              ))
           ((=number? a1 0)
            (begin
              (cons '+ a-rest)
              ))
           ((and (number? a1) (number? (car a-rest)))
            (begin
              (+ a1 a-rest)
              ))
           (else
            (begin
              (cons '+ (cons a1 a-rest))
              )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-product m1 . m-rest)
  (begin
    (if (number? m-rest)
        (begin
          (cond
           ((or (=number? m1 0)
                (=number? m-rest 0))
            (begin
              0
              ))
           ((=number? m1 1)
            (begin
              m-rest
              ))
           ((=number? m-rest 1)
            (begin
              m1
              ))
           ((and (number? m1)
                 (number? m-rest))
            (begin
              (* m1 m-rest)
              ))
           (else
            (begin
              (list '* m1 m-rest)
              ))
           ))
        (begin
          (cond
           ((null? m1)
            (begin
              0
              ))
           ((null? m-rest)
            (begin
              m1
              ))
           ((=number? m1 0)
            (begin
              0
              ))
           ((and (=number? m1 1)
                 (pair? m-rest))
            (begin
              (make-product (car m-rest) (cdr m-rest))
              ))
           ((=number? m1 1)
            (begin
              m-rest
              ))
           ((and (number? (car m-rest))
                 (zero? (car m-rest)))
            (begin
              0
              ))
           ((and (number? (car m-rest))
                 (eq? (car m-rest) 1))
            (begin
              m1
              ))
           ((and (not (number? m1))
                 (null? (car m-rest)))
            (begin
              m1
              ))
           (else
            (begin
              (cons '* (cons m1 m-rest))
              )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (sum? x)
  (begin
    (and (pair? x) (eq? (car x) '+))
    ))

;;;#############################################################
;;;#############################################################
(define (addend s)
  (begin
    (cadr s)
    ))

;;;#############################################################
;;;#############################################################
(define (augend s)
  (begin
    (if (<= (length (cddr s)) 1)
        (begin
          (caddr s))
        (begin
          (cons '+ (cddr s))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (product? x)
  (begin
    (and (pair? x) (eq? (car x) '*))
    ))

;;;#############################################################
;;;#############################################################
(define (multiplier p)
  (begin
    (cadr p)
    ))

;;;#############################################################
;;;#############################################################
(define (multiplicand p)
  (begin
    (if (<= (length (cddr p)) 1)
        (begin
          (caddr p))
        (begin
          (cons '* (cddr p))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (exponentiation? expr)
  (begin
    (if (pair? expr)
        (begin
          (let ((oper (car expr)))
            (begin
              (and (string? oper)
                   (string-ci=? oper "**"))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-exponent base-expr exponent)
  (begin
    (cond
     ((and (number? base-expr)
           (equal? base-expr 0))
      (begin
        0
        ))
     ((and (number? base-expr)
           (equal? base-expr 1))
      (begin
        1
        ))
     ((and (number? exponent)
           (equal? exponent 0))
      (begin
        1
        ))
     ((and (number? exponent)
           (equal? exponent 1))
      (begin
        base-expr
        ))
     ((and (number? base-expr)
           (number? exponent))
      (begin
        (expt base-expr exponent)
        ))
     (else
      (begin
        (list "**" base-expr exponent)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (exponent-base expr)
  (begin
    (cadr expr)
    ))

;;;#############################################################
;;;#############################################################
(define (exponent-exponent expr)
  (begin
    (caddr expr)
    ))

;;;#############################################################
;;;#############################################################
(define (deriv exp var)
  (begin
    (cond
     ((number? exp)
      (begin
        0
        ))
     ((variable? exp)
      (begin
        (if (same-variable? exp var)
            (begin
              1)
            (begin
              0
              ))
        ))
     ((sum? exp)
      (begin
        (make-sum (deriv (addend exp) var)
                  (deriv (augend exp) var))
        ))
     ((product? exp)
      (begin
        (let ((uu (multiplier exp))
              (vv (multiplicand exp)))
          (let ((term1 (make-product (deriv uu var) vv))
                (term2 (make-product uu (deriv vv var))))
            (begin
              (make-sum term1 term2)
              )))
        ))
     ((exponentiation? exp)
      (begin
;;; a^b = exp(b*ln(a))
;;; (d/dx)a^b = exp(b*ln(a))*((b/a)*da/dx+ db/dx*ln(a))
;;; = b*a^(b-1)*da/dx + ln(a)*a^b*db/dx
;;; assume exponent b a constant
        (let ((aa (exponent-base exp))
              (bb (exponent-exponent exp)))
          (begin
            (make-product
             bb
             (make-product
              (make-exponent aa (1- bb))
              (deriv aa var)))
            ))
        ))
     (else
      (begin
        (display
         (format
          #f "unknown expression type -- "))
        (display
         (format
          #f "DERIV expresstion = ~a~%" exp))
        (force-output)
        (quit)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-deriv-1 result-hash-table)
 (begin
   (let ((sub-name "test-deriv-1")
         (test-list
          (list
           (list (list '+ 'x 1) 'x 1)
           (list (list '+ 'x 'y) 'x 1)
           (list (list '* 'x 'y) 'x 'y)
           (list (list '* 'x 'y 'z) 'x (list '* 'y 'z))
           (list (list '* (list '* 'x 'y) (list '+ 'x 3))
                 'x (list '+ (list '* 'y (list '+ 'x 3))
                          (list '* 'x 'y)))
           (list (list "**" 'x 2) 'x
                 (list '* 2 'x))
           (list (list "**" 'x 3) 'x
                 (list '* 3 (list "**" 'x 2)))
           (list (list "**" 'x 4) 'x
                 (list '* 4 (list "**" 'x 3)))
           (list (list "**" 'x 5) 'x
                 (list '* 5 (list "**" 'x 4)))
           (list (list '* 'x 'y (list '+ 'x 3)) 'x
                 (list '+ (list '* 'y (list '+ 'x 3))
                       (list '* 'x 'y)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((expr (list-ref this-list 0))
                  (var (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (deriv expr var)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : expr=~a, var=~a, "
                        sub-name test-label-index
                        expr var))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Extend the differentiation program to "))
    (display
     (format #f "handle sums and~%"))
    (display
     (format #f "products of arbitrary numbers of (two "))
    (display
     (format #f "or more) terms.~%"))
    (display
     (format #f "Then the last example above could be "))
    (display
     (format #f "expressed as:~%"))
    (display
     (format #f "(deriv '(* x y (+ x 3)) 'x)~%"))
    (display
     (format #f "Try to do this by changing only the "))
    (display
     (format #f "representation for sums~%"))
    (display
     (format #f "and products, without changing the "))
    (display
     (format #f "deriv procedure at all.~%"))
    (display
     (format #f "For example, the addend of a sum would "))
    (display
     (format #f "be the first~%"))
    (display
     (format #f "term, and the augend would be the sum "))
    (display
     (format #f "of the rest of~%"))
    (display
     (format #f "the terms.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list (list "**" 'x 5) 'x)
            (list (list "**" (list '+ 'x 'y) 5) 'x)
            (list (list "**" (list '* 'x 3) 5) 'x)
            (list (list '* (list '* 'x 'y) (list '+ 'x 3)) 'x)
            (list (list "**" 'x 5) 'x)
            (list (list '* 'x 'y (list '+ 'x 3)) 'x)
            )))
      (begin
        (for-each
         (lambda (tlist)
           (begin
             (let ((expr (list-ref tlist 0))
                   (var (list-ref tlist 1)))
               (begin
                 (display
                  (format #f "u=~a, du/dx = ~a~%"
                          expr (deriv expr var)))
                 (newline)
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.57 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
