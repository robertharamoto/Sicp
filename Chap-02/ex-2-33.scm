#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.33                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (map p sequence)
  (begin
    (accumulate
     (lambda (x y) (cons (p x) y)) (list) sequence)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-map-1 result-hash-table)
 (begin
   (let ((sub-name "test-map-1")
         (test-list
          (list
           (list (lambda(x) (* 2 x)) (list 1 2 3)
                 (list 2 4 6))
           (list (lambda(x) (+ 3 x))
                 (list 1 2 3)
                 (list 4 5 6))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((operator (list-ref this-list 0))
                  (sequence (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (map operator sequence)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : op=~a, seq=~a, "
                        sub-name test-label-index
                        operator sequence))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (append seq1 seq2)
  (begin
    (accumulate
     cons seq2 seq1)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-apppend-1 result-hash-table)
 (begin
   (let ((sub-name "test-append-1")
         (test-list
          (list
           (list (list 1 2 3) (list 4 5 6)
                 (list 1 2 3 4 5 6))
           (list (list 1 2 3) (list 9 10 11 12)
                 (list 1 2 3 9 10 11 12))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((seq1 (list-ref this-list 0))
                  (seq2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (append seq1 seq2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : seq1=~a, seq2=~a, "
                        sub-name test-label-index
                        seq1 seq2))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (length sequence)
  (begin
    (accumulate (lambda (x y) (1+ y)) 0 sequence)
    ))

;;;#############################################################
;;;#############################################################
(define (square x)
  (begin
    (* x x)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Fill in the missing expressions to "))
    (display
     (format #f "complete the following~%"))
    (display
     (format #f "definitions of some basic "))
    (display
     (format #f "list-manipulation operations as~%"))
    (display
     (format #f "accumulations:~%"))
    (display
     (format #f "(define (map p sequence)~%"))
    (display
     (format #f "  (accumulate (lambda "))
    (display
     (format #f "(x y) <??>) nil sequence))~%"))
    (display
     (format #f "(define (append seq1 seq2)~%"))
    (display
     (format #f "  (accumulate cons <??> <??>))~%"))
    (display
     (format #f "(define (length sequence)~%"))
    (display
     (format #f "  (accumulate <??> 0 sequence))~%"))
    (newline)
    (display
     (format #f "(define (map p sequence)~%"))
    (display
     (format #f "  (accumulate~%"))
    (display
     (format #f "   (lambda (x y) (cons "))
    (display
     (format #f "(p x) y)) (list) sequence))~%"))
    (newline)
    (display
     (format #f "(define (append seq1 seq2)~%"))
    (display
     (format #f "  (accumulate~%"))
    (display
     (format #f "   cons seq2 seq1))~%"))
    (newline)
    (display
     (format #f "(define (length sequence)~%"))
    (display
     (format #f "  (accumulate (lambda (x y) "))
    (display
     (format #f "(1+ y)) 0 sequence))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((alist (list 1 2 3 4 5))
          (blist (list 6 7 8 9 10)))
      (begin
        (display
         (format #f "(map square ~a) = ~a~%"
                 alist (map square alist)))
        (display
         (format #f "(append ~a ~a) = ~a~%"
                 alist blist
                 (append alist blist)))
        (display
         (format #f "(length ~a) = ~a~%"
                 alist (length alist)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.33 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
