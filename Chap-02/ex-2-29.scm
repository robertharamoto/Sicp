#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.29                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-mobile left right)
  (begin
    (list left right)
    ))

;;;#############################################################
;;;#############################################################
(define (make-branch length structure)
  (begin
    (list length structure)
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch amobile)
  (begin
    (car amobile)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch amobile)
  (begin
    (car (cdr amobile))
    ))

;;;#############################################################
;;;#############################################################
(define (branch-length abranch)
  (begin
    (car abranch)
    ))

;;;#############################################################
;;;#############################################################
(define (branch-structure abranch)
  (begin
    (car (cdr abranch))
    ))

;;;#############################################################
;;;#############################################################
(define (total-weight amobile)
  (define (local-add-up-weight-iter amobile acc-weight)
    (begin
      (if (null? amobile)
          (begin
            acc-weight)
          (begin
            (if (number? amobile)
                (begin
                  (+ acc-weight amobile))
                (begin
                  (let ((left (left-branch amobile))
                        (right (right-branch amobile)))
                    (let ((lstruct (branch-structure left))
                          (rstruct (branch-structure right)))
                      (let ((lweight (local-add-up-weight-iter lstruct  0))
                            (rweight (local-add-up-weight-iter rstruct 0)))
                        (begin
                          (+ acc-weight lweight rweight)
                          ))
                      ))
                  ))
            ))
      ))
  (begin
    (local-add-up-weight-iter amobile 0)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-total-weight-1 result-hash-table)
 (begin
   (let ((sub-name "test-total-weight-1")
         (test-list
          (list
           (list (make-mobile
                  (make-branch 2 6) (make-branch 2 6))
                 12)
           (list (make-mobile
                  (make-branch
                   2 (make-mobile
                      (make-branch 1 4) (make-branch 2 2)))
                  (make-branch 2 6))
                 12)
           (list (make-mobile
                  (make-branch
                   3
                   (make-mobile
                    (make-branch
                     2 (make-mobile
                        (make-branch 2 1) (make-branch 1 2)))
                    (make-branch
                     3 (make-mobile
                        (make-branch 4 3) (make-branch 3 4)))))
                  (make-branch
                   3
                   (make-mobile
                    (make-branch 2 4)
                    (make-branch
                     3 (make-mobile
                        (make-branch 1 2) (make-branch 1 2))))))
                 18)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (total-weight alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (is-balanced? amobile)
  (define (local-is-balanced-iter amobile acc-result)
    (begin
      (if (null? amobile)
          (begin
            #f)
          (begin
            (if (equal? acc-result #f)
                (begin
                  #f)
                (begin
                  (let ((lbranch (left-branch amobile))
                        (rbranch (right-branch amobile)))
                    (let ((llen (branch-length lbranch))
                          (rlen (branch-length rbranch))
                          (lmobile (branch-structure lbranch))
                          (rmobile (branch-structure rbranch)))
                      (let ((lweight (total-weight lmobile))
                            (rweight (total-weight rmobile)))
                        (let ((ltorque (* llen lweight))
                              (rtorque (* rlen rweight)))
                          (begin
                            (if (equal? ltorque rtorque)
                                (begin
                                  (set! acc-result #t)
                                  ;;; check one level deeper
                                  (if (not (number? lmobile))
                                      (begin
                                        (let ((lresult
                                               (local-is-balanced-iter
                                                lmobile #t)))
                                          (begin
                                            (set!
                                             acc-result
                                             (and acc-result lresult))
                                            ))
                                        ))
                                  (if (not (number? rmobile))
                                      (begin
                                        (let ((rresult
                                               (local-is-balanced-iter
                                                rmobile #t)))
                                          (begin
                                            (set!
                                             acc-result
                                             (and acc-result rresult))
                                            ))
                                        ))
                                  acc-result)
                                (begin
                                  #f
                                  ))
                            ))
                        )))
                  ))
            ))
      ))
  (begin
    (if (number? amobile)
        (begin
          #f)
        (begin
          (local-is-balanced-iter amobile #t)
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-balanced-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-balanced-1")
         (test-list
          (list
           (list (make-mobile
                  (make-branch 2 6) (make-branch 2 6))
                 #t)
           (list (make-mobile
                  (make-branch
                   2 (make-mobile
                      (make-branch 1 4) (make-branch 2 2)))
                  (make-branch 2 6))
                 #t)
           (list (make-mobile
                  (make-branch
                   3
                   (make-mobile
                    (make-branch
                     2 (make-mobile
                        (make-branch 2 1) (make-branch 1 2)))
                    (make-branch
                     3 (make-mobile
                        (make-branch 4 3) (make-branch 3 4)))))
                  (make-branch
                   3
                   (make-mobile
                    (make-branch 2 4)
                    (make-branch
                     3 (make-mobile
                        (make-branch 1 2) (make-branch 1 2))))))
                 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (is-balanced? alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A binary mobile consists of two "))
    (display
     (format #f "branches, a left~%"))
    (display
     (format #f "branch and a right branch. Each "))
    (display
     (format #f "branch is a rod~%"))
    (display
     (format #f "of a certain length, from which hangs "))
    (display
     (format #f "either a weight~%"))
    (display
     (format #f "or another binary mobile. We can "))
    (display
     (format #f "represent a binary~%"))
    (display
     (format #f "mobile using coumpound data by "))
    (display
     (format #f "constructing it~%"))
    (display
     (format #f "from two branches (for example, using "))
    (display
     (format #f "list):~%"))
    (newline)
    (display
     (format #f "(define (make-mobile left right)~%"))
    (display
     (format #f "  (list left right))~%"))
    (display
     (format #f "A branch is constructed from a length "))
    (display
     (format #f "(which must be~%"))
    (display
     (format #f "a number) together with a structure, "))
    (display
     (format #f "which may be~%"))
    (display
     (format #f "either a number (representing a simple "))
    (display
     (format #f "weight) or another~%"))
    (display
     (format #f "mobile:~%"))
    (newline)
    (display
     (format #f "(define (make-branch "))
    (display
     (format #f "length structure)~%"))
    (display
     (format #f "  (list length structure))~%"))
    (display
     (format #f "a.  Write the corresponding selectors "))
    (display
     (format #f "left-branch and~%"))
    (display
     (format #f "right-branch, which return the "))
    (display
     (format #f "branches of a mobile,~%"))
    (display
     (format #f "and branch-length and "))
    (display
     (format #f "branch-structure, which~%"))
    (display
     (format #f "return the components of a branch.~%"))
    (display
     (format #f "b.  Using your selectors, define a "))
    (display
     (format #f "procedure total-weight~%"))
    (display
     (format #f "that returns the total weight of a "))
    (display
     (format #f "mobile.~%"))
    (display
     (format #f "c.  A mobile is said to be balanced "))
    (display
     (format #f "if the torque~%"))
    (display
     (format #f "applied by its top-left branch is "))
    (display
     (format #f "equal to that~%"))
    (display
     (format #f "applied by its top-right branch, "))
    (display
     (format #f "that is, if the~%"))
    (display
     (format #f "length of the left rod multiplied "))
    (display
     (format #f "by the weight~%"))
    (display
     (format #f "hanging from that rod is equal to "))
    (display
     (format #f "the corresponding~%"))
    (display
     (format #f "product for the right side, and if "))
    (display
     (format #f "each of the~%"))
    (display
     (format #f "submobiles hanging off its branches "))
    (display
     (format #f "is balanced. Design~%"))
    (display
     (format #f "a predicate that tests whether a "))
    (display
     (format #f "binary mobile~%"))
    (display
     (format #f "is balanced.~%"))
    (display
     (format #f "d.  Suppose we change the representation "))
    (display
     (format #f "of mobiles so~%"))
    (display
     (format #f "that the constructors are~%"))
    (display
     (format #f "(define (make-mobile left right)~%"))
    (display
     (format #f "  (cons left right))~%"))
    (display
     (format #f "(define (make-branch length structure)~%"))
    (display
     (format #f "  (cons length structure))~%"))
    (display
     (format #f "How much do you need to change your "))
    (display
     (format #f "programs to convert~%"))
    (display
     (format #f "to the new representation?~%"))
    (newline)
    (display
     (format #f "(a) selectors~%"))
    (display
     (format #f "(define (left-branch amobile)~%"))
    (display
     (format #f "  (car amobile))~%"))
    (display
     (format #f "(define (right-branch amobile)~%"))
    (display
     (format #f "  (car (cdr amobile)))~%"))
    (display
     (format #f "(define (branch-length abranch)~%"))
    (display
     (format #f "  (car abranch))~%"))
    (display
     (format #f "(define (branch-structure abranch)~%"))
    (display
     (format #f "  (car (cdr abranch)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x-list
           (list
            (make-mobile
             (make-branch 2 6) (make-branch 2 6))
            (make-mobile
             (make-branch
              2 (make-mobile
                 (make-branch 1 4) (make-branch 2 2)))
             (make-branch 2 6))
            (make-mobile
             (make-branch
              2
              (make-mobile
               (make-branch 1 4) (make-branch 2 2)))
             (make-branch
              2
              (make-mobile
               (make-branch 1 3) (make-branch 1 3))))
            (make-mobile
             (make-branch
              3
              (make-mobile
               (make-branch 3 2) (make-branch 1 6)))
             (make-branch
              2 (make-mobile
                 (make-branch 1 6) (make-branch 3 2))))
            (make-mobile
             (make-branch
              3
              (make-mobile
               (make-branch
                2 (make-mobile
                   (make-branch 2 1) (make-branch 1 2)))
               (make-branch
                3 (make-mobile
                   (make-branch 4 3) (make-branch 3 4)))))
             (make-branch
              3
              (make-mobile
               (make-branch 2 4)
               (make-branch
                3 (make-mobile
                   (make-branch 1 2) (make-branch 1 2))))))
            )))
      (begin
        (for-each
         (lambda (amobile)
           (begin
             (display
              (format #f "mobile = ~a~%" amobile))
             (force-output)
             (display
              (format #f "(total-weight ~a) = ~a~%"
                      amobile (total-weight amobile)))
             (display
              (format #f "    is-balanced? = ~a~%"
                      (if (is-balanced? amobile) "true" "false")))
             (force-output)
             )) x-list)
        (newline)
        ))

    (newline)
    (display
     (format #f "(d) changing the mobile and branch "))
    (display
     (format #f "representation would~%"))
    (display
     (format #f "lead to a change in the "))
    (display
     (format #f "right-branch and the~%"))
    (display
     (format #f "branch-structure methods.~%"))
    (newline)
    (display
     (format #f "(define (right-branch amobile)~%"))
    (display
     (format #f "  (cdr amobile))~%"))
    (display
     (format #f "(define (branch-structure abranch)~%"))
    (display
     (format #f "  (cdr abranch))~%"))
    (display
     (format #f "since the (cdr (list 1 2)) = (2)~%"))
    (display
     (format #f "whereas the (cdr (cons 1 2)) = 2~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.29 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
