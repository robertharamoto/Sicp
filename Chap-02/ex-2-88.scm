#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.88                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Extend the polynomial system to "))
    (display
     (format #f "include subtraction of~%"))
    (display
     (format #f "polynomials. (Hint: You may find "))
    (display
     (format #f "it helpful to define~%"))
    (display
     (format #f "a generic negation operation.)~%"))
    (newline)
    (display
     (format #f "(define (install-scheme-number-package)~%"))
    (display
     (format #f "  (define (negate x)~%"))
    (display
     (format #f "    (* -1 x))~%"))
    (display
     (format #f "  (put 'negate '(scheme-number) negate)~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-rational-package)~%"))
    (display
     (format #f "  (define (negate x)~%"))
    (display
     (format #f "    (make-rat (* -1 (numer x)) (denom x)))~%"))
    (display
     (format #f "  (put 'negate '(rational) negate)~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-complex-package)~%"))
    (display
     (format #f "  (define (negate z)~%"))
    (display
     (format #f "    (make-complex-from-real-imag~%"))
    (display
     (format #f "      (* -1 (real-part z))~%"))
    (display
     (format #f "      (* -1 (imag-part z))))~%"))
    (display
     (format #f "  (put 'negate '(complex) negate)~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-polynomial-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (negate apoly)~%"))
    (display
     (format #f "    (define (local-iter "))
    (display
     (format #f "term-list acc-list)~%"))
    (display
     (format #f "      (if (null? term-list)~%"))
    (display
     (format #f "          acc-list~%"))
    (display
     (format #f "          (let ((first-term "))
    (display
     (format #f "(car term-list))~%"))
    (display
     (format #f "                (tail-list "))
    (display
     (format #f "(cdr term-list)))~%"))
    (display
     (format #f "            (let ((o1 (order "))
    (display
     (format #f "first-term))~%"))
    (display
     (format #f "                  (c1 (coeff "))
    (display
     (format #f "first-term)))~%"))
    (display
     (format #f "              (let ((neg-coeff~%"))
    (display
     (format #f "                     ((get 'negate "))
    (display
     (format #f "(type-tag c1)) c1)))~%"))
    (display
     (format #f "                (let ((neg-term "))
    (display
     (format #f "(make-term o1 neg-coeff)))~%"))
    (display
     (format #f "                  (let ((next-acc-list "))
    (display
     (format #f "(cons neg-term acc-list)))~%"))
    (display
     (format #f "                    (local-iter "))
    (display
     (format #f "tail-list next-acc-list)~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "                ))))~%"))
    (display
     (format #f "    (let ((neg-term-list~%"))
    (display
     (format #f "           (reverse (local-iter "))
    (display
     (format #f "(term-list apoly) (list))))~%"))
    (display
     (format #f "          (var (variable apoly)))~%"))
    (display
     (format #f "       ((get 'make 'polynomial) "))
    (display
     (format #f "var neg-term-list)~%"))
    (display
     (format #f "       ))~%"))
    (display
     (format #f "  (define (sub-poly p1 p2) ~%"))
    (display
     (format #f "    (let ((n-p2 ((get "))
    (display
     (format #f "'negate '(polynomial)) p2)))~%"))
    (display
     (format #f "      ((get 'add "))
    (display
     (format #f "'(polynomial)) p1 n-p2)))~%"))
    (display
     (format #f "  (put 'negate "))
    (display
     (format #f "'(polynomial) negate)~%"))
    (display
     (format #f "  (put 'sub "))
    (display
     (format #f "'(polynomial polynomial) sub-poly)~%"))
    (display
     (format #f "  ...)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.88 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
