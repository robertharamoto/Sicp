#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.66                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (entry tree)
  (begin
    (car tree)
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch tree)
  (begin
    (cadr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch tree)
  (begin
    (caddr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (make-tree entry left right)
  (begin
    (list entry left right)
    ))

;;;#############################################################
;;;#############################################################
(define (lookup-tree given-key tree-of-records)
  (begin
    (if (null? tree-of-records)
        (begin
          #f)
        (begin
          (let ((this-element (entry tree-of-records)))
            (begin
              (cond
               ((equal? given-key this-element)
                (begin
                  this-element
                  ))
               ((< given-key this-element)
                (begin
                  (lookup-tree
                   given-key
                   (left-branch tree-of-records))
                  ))
               (else
                (begin
                  (lookup-tree
                   given-key
                   (right-branch tree-of-records))
                  )))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lookup-tree-1 result-hash-table)
 (begin
   (let ((sub-name "test-lookup-tree-1")
         (test-list
          (list
           (list
            11
            (make-tree 33
                       (make-tree 22
                                  (make-tree 11 (list) (list))
                                  (make-tree 12 (list) (list)))
                       (make-tree 48
                                  (make-tree 47 (list) (list))
                                  (make-tree 55 (list) (list))))
            11)
           (list
            55
            (make-tree 33
                       (make-tree 22
                                  (make-tree 11 (list) (list))
                                  (make-tree 15 (list) (list)))
                       (make-tree 48
                                  (make-tree 47 (list) (list))
                                  (make-tree 55 (list) (list))))
            55)
           (list
            47
            (make-tree 33
                       (make-tree 22
                                  (make-tree 11 (list) (list))
                                  (make-tree 15 (list) (list)))
                       (make-tree 48
                                  (make-tree 47 (list) (list))
                                  (make-tree 55 (list) (list))))
            47)
           (list
            9
            (make-tree 33
                       (make-tree 22
                                  (make-tree 11 (list) (list))
                                  (make-tree 15 (list) (list)))
                       (make-tree 55
                                  (make-tree 50 (list) (list))
                                  (make-tree 60 (list) (list))))
            #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((given-key (list-ref this-list 0))
                  (atree (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (lookup-tree given-key atree)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : given-key=~a, "
                        sub-name test-label-index given-key))
                      (err-msg-2
                       (format
                        #f "atree=~a, shouldbe=~a,  result=~a"
                        atree shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Implement the lookup procedure for the "))
    (display
     (format #f "case where the set~%"))
    (display
     (format #f "of records is structured as a binary "))
    (display
     (format #f "tree, ordered by the~%"))
    (display
     (format #f "numerical values of the keys.~%"))
    (newline)
    (display
     (format #f "(define (lookup-tree "))
    (display
     (format #f "given-key tree-of-records)~%"))
    (display
     (format #f "  (if (null? tree-of-records)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        #f)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (let ((this-element "))
    (display
     (format #f "(entry tree-of-records)))~%"))
    (display
     (format #f "          (let ((this-key "))
    (display
     (format #f "(key this-element)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (cond~%"))
    (display
     (format #f "               ((equal? "))
    (display
     (format #f "given-key this-key)~%"))
    (display
     (format #f "                this-element)~%"))
    (display
     (format #f "               ((< given-key this-key)~%"))
    (display
     (format #f "                (lookup-tree given-key~%"))
    (display
     (format #f "      (left-branch tree-of-records)))~%"))
    (display
     (format #f "               (else~%"))
    (display
     (format #f "                (lookup-tree given-key~%"))
    (display
     (format #f "      (right-branch tree-of-records))))~%"))
    (display
     (format #f "              )))~%"))
    (display
     (format #f "        )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.66 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
