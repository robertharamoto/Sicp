#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.75                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (square xx)
  (begin
    (* xx xx)
    ))

;;;#############################################################
;;;#############################################################
(define (make-from-real-imag x y)
  (define (local-dispatch op)
    (begin
      (cond
       ((eq? op 'real-part)
        (begin
          x
          ))
       ((eq? op 'imag-part)
        (begin
          y
          ))
       ((eq? op 'magnitude)
        (begin
          (sqrt (+ (square x) (square y)))
          ))
       ((eq? op 'angle)
        (begin
          (atan y x)
          ))
       (else
        (begin
          (error "Unknown op -- MAKE-FROM-REAL-IMAG" op)
          )))
      ))
  (begin
    local-dispatch
    ))

;;;#############################################################
;;;#############################################################
(define (make-from-mag-ang r theta)
  (define (local-dispatch op)
    (begin
      (cond
       ((eq? op 'real-part)
        (begin
          (* r (cos theta))
          ))
       ((eq? op 'imag-part)
        (begin
          (* r (sin theta))
          ))
       ((eq? op 'magnitude)
        (begin
          r
          ))
       ((eq? op 'angle)
        (begin
          theta
          ))
       (else
        (begin
          (error "Unknown op -- MAKE-FROM-MAG-ANG" op)
          )))
      ))
  (begin
    local-dispatch
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-polar-1 result-hash-table)
 (begin
   (let ((sub-name "test-polar-1")
         (test-list
          (list
           (list 1 0)
           (list 1 1)
           (list 0 1)
           (list 0 -1)
           (list -1 -1)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1)))
              (let ((rect-obj (make-from-real-imag xx yy)))
                (let ((rr (rect-obj 'magnitude))
                      (theta (rect-obj 'angle)))
                  (let ((polar-obj (make-from-mag-ang rr theta)))
                    (let ((pxx (polar-obj 'real-part))
                          (pyy (polar-obj 'imag-part)))
                      (let ((err-msg-1
                             (format
                              #f "~a : error (~a) : xx=~a, yy=~a, "
                              sub-name test-label-index
                              xx yy))
                            (err-msg-2
                             (format
                              #f "pxx=~a, pxx=~a"
                              pxx pyy)))
                        (let ((error-message
                               (string-append err-msg-1 err-msg-2)))
                          (begin
                            (unittest2:assert?
                             (< (abs (- xx pxx)) tolerance)
                             sub-name
                             error-message
                             result-hash-table)

                            (unittest2:assert?
                             (< (abs (- yy pyy)) tolerance)
                             sub-name
                             error-message
                             result-hash-table)
                            )))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Implement the constructor "))
    (display
     (format #f "make-from-mag-ang in~%"))
    (display
     (format #f "message-passing style. This "))
    (display
     (format #f "procedure should be~%"))
    (display
     (format #f "analogous to the make-from-real-imag "))
    (display
     (format #f "procedure given~%"))
    (display
     (format #f "above.~%"))
    (newline)
    (display
     (format #f "(define (make-from-mag-ang r theta)~%"))
    (display
     (format #f "  (define (dispatch op)~%"))
    (display
     (format #f "    (cond~%"))
    (display
     (format #f "     ((eq? op 'real-part) (* r (cos theta)))~%"))
    (display
     (format #f "     ((eq? op 'imag-part) (* r (sin theta)))~%"))
    (display
     (format #f "     ((eq? op 'magnitude) r)~%"))
    (display
     (format #f "     ((eq? op 'angle) theta)~%"))
    (display
     (format #f "     (else~%"))
    (display
     (format #f "      (error \"Unknown op -- "))
    (display
     (format #f "MAKE-FROM-MAG-ANG\" op))))~%"))
    (display
     (format #f "  dispatch)~%"))
    (newline)

    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list 1 0)
            (list 1 1)
            (list 0 1)
            (list 0 -1)
            (list -1 -1))))
      (begin
        (for-each
         (lambda (tlist)
           (begin
             (let ((xx (list-ref tlist 0))
                   (yy (list-ref tlist 1)))
               (let ((rect-obj
                      (make-from-real-imag xx yy)))
                 (let ((rr (rect-obj 'magnitude))
                       (theta (rect-obj 'angle)))
                   (let ((polar-obj
                          (make-from-mag-ang rr theta)))
                     (let ((pxx (polar-obj 'real-part))
                           (pyy (polar-obj 'imag-part)))
                       (begin
                         (display
                          (format
                           #f
                           "initial rectangular coordinates "))
                         (display
                          (format
                           #f "= (~a, ~a)~%" xx yy))
                         (display
                          (ice-9-format:format
                           #f "magnitude = ~8,4f, " rr))
                         (display
                          (ice-9-format:format
                           #f "angle = ~8,4f~%" theta))
                         (display
                          (format
                           #f "polar real-part = ~8,4f, "
                           pxx))
                         (display
                          (format
                           #f "imag-part = ~8,4f~%" pyy))
                         (newline)
                         (force-output)
                         ))
                     ))
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.75 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
