#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.38                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (accumulate-n op init seqs)
  (begin
    (if (or (null? seqs) (null? (car seqs)))
        (begin
          (list))
        (begin
          (cons (accumulate op init (map car seqs))
                (accumulate-n op init (map cdr seqs)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define fold-right accumulate)

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fold-right-1 result-hash-table)
 (begin
   (let ((sub-name "test-fold-right-1")
         (test-list
          (list
           (list + 0 (list 1 2 3) 6)
           (list - 0 (list 1 2 3) 2)
           (list * 1 (list 1 2 3) 6)
           (list / 1 (list 1 2 3) 3/2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((oper (list-ref this-list 0))
                  (initial (list-ref this-list 1))
                  (alist (list-ref this-list 2))
                  (shouldbe-list (list-ref this-list 3)))
              (let ((result-list
                     (fold-right oper initial alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : oper=~a, "
                        sub-name test-label-index oper))
                      (err-msg-2
                       (format
                        #f "initial=~a, alist=~a, "
                        initial alist))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (fold-left op initial sequence)
  (define (iter result rest)
    (begin
      (if (null? rest)
          (begin
            result)
          (begin
            (iter (op result (car rest))
                  (cdr rest))
            ))
      ))
  (begin
    (iter initial sequence)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fold-left-1 result-hash-table)
 (begin
   (let ((sub-name "test-fold-left-1")
         (test-list
          (list
           (list + 0 (list 1 2 3) 6)
           (list - 0 (list 1 2 3) -6)
           (list * 1 (list 1 2 3) 6)
           (list / 1 (list 1 2 3) 1/6)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((oper (list-ref this-list 0))
                  (initial (list-ref this-list 1))
                  (alist (list-ref this-list 2))
                  (shouldbe-list (list-ref this-list 3)))
              (let ((result-list
                     (fold-left oper initial alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : oper=~a, "
                        sub-name test-label-index oper))
                      (err-msg-2
                       (format
                        #f "initial=~a, alist=~a, "
                        initial alist))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The accumulate procedure is also "))
    (display
     (format #f "known as fold-right,~%"))
    (display
     (format #f "because it combines the first element "))
    (display
     (format #f "of the sequence with~%"))
    (display
     (format #f "the result of combining all the "))
    (display
     (format #f "elements to the right.~%"))
    (display
     (format #f "There is also a fold-left, which is "))
    (display
     (format #f "similar to fold-right,~%"))
    (display
     (format #f "except that it combines elements "))
    (display
     (format #f "working in the opposite~%"))
    (display
     (format #f "direction:~%"))
    (display
     (format #f "(define (fold-left op initial sequence)~%"))
    (display
     (format #f "  (define (iter result rest)~%"))
    (display
     (format #f "    (if (null? rest)~%"))
    (display
     (format #f "        result~%"))
    (display
     (format #f "        (iter (op result (car rest))~%"))
    (display
     (format #f "              (cdr rest))))~%"))
    (display
     (format #f "  (iter initial sequence))~%"))
    (display
     (format #f "What are the values of~%"))
    (display
     (format #f "(fold-right / 1 (list 1 2 3))~%"))
    (display
     (format #f "(fold-left / 1 (list 1 2 3))~%"))
    (display
     (format #f "(fold-right list nil (list 1 2 3))~%"))
    (display
     (format #f "(fold-left list nil (list 1 2 3))~%"))
    (display
     (format #f "Give a property that op should "))
    (display
     (format #f "satisfy to guarantee that~%"))
    (display
     (format #f "fold-right and fold-left will produce "))
    (display
     (format #f "the same values for~%"))
    (display
     (format #f "any sequence.~%"))
    (newline)
    (display
     (format #f "If fold-right and fold-left are to "))
    (display
     (format #f "produce equal results,~%"))
    (display
     (format #f "then op should be associative. For "))
    (display
     (format #f "example, in the case~%"))
    (display
     (format #f "where there are just two elements in "))
    (display
     (format #f "the list, (+ a b)~%"))
    (display
     (format #f "should be the same as (+ b a), but "))
    (display
     (format #f "(- a b) =/= (- b a).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((alist (list 1 2 3)))
      (begin
        (display
         (format
          #f "(fold-right / 1 ~a) = ~a~%"
          alist (fold-right / 1 alist)))
        (display
         (format
          #f "(fold-left / 1 ~a) = ~a~%"
          alist (fold-left / 1 alist)))
        (display
         (format
          #f "(fold-right list (list) ~a) = ~a~%"
          alist (fold-right list (list) alist)))
        (display
         (format
          #f "(fold-left list (list) ~a) = ~a~%"
          alist (fold-left list (list) alist)))
        (newline)
        (display
         (format
          #f "(fold-right + 0 ~a) = ~a~%"
          alist (fold-right + 0 alist)))
        (display
         (format
          #f "(fold-left + 0 ~a) = ~a~%"
          alist (fold-left + 0 alist)))
        (display
         (format
          #f "(fold-right - 0 ~a) = ~a~%"
          alist (fold-right - 0 alist)))
        (display
         (format
          #f "(fold-left - 0 ~a) = ~a~%"
          alist (fold-left - 0 alist)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.38 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
