#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.91                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define the-empty-termlist (list))

;;;#############################################################
;;;#############################################################
(define (empty-termlist? term-list)
  (begin
    (null? term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (first-term term-list)
  (begin
    (car term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (rest-terms term-list)
  (begin
    (cdr term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (order term)
  (begin
    (car term)
    ))

;;;#############################################################
;;;#############################################################
(define (coeff term)
  (begin
    (cadr term)
    ))

;;;#############################################################
;;;#############################################################
(define div /)

;;;#############################################################
;;;#############################################################
(define (make-term order coeff)
  (begin
    (list order coeff)
    ))

;;;#############################################################
;;;#############################################################
(define (=zero? anum)
  (begin
    (cond
     ((integer? anum)
      (begin
        (zero? anum)
        ))
     ((real? anum)
      (begin
        (< (abs anum) 1e-12)
        ))
     ((rational? anum)
      (begin
        (< (abs (exact->inexact anum) 1e-12))
        ))
     (else
      (begin
        (= anum 0)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-term term term-list)
  (begin
    (if (=zero? (coeff term))
        (begin
          term-list)
        (begin
          (cons term term-list)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (variable apoly)
  (begin
    (car apoly)
    ))

;;;#############################################################
;;;#############################################################
(define (term-list apoly)
  (begin
    (cdr apoly)
    ))

;;;#############################################################
;;;#############################################################
(define (make-polynomial var term-list)
  (begin
    (cons var term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (add-poly p1 p2)
  (begin
    (let ((var-1 (variable p1))
          (var-2 (variable p2)))
      (begin
        (if (eq? var-1 var-2)
            (begin
              (let ((term-1-list (term-list p1))
                    (term-2-list (term-list p2)))
                (let ((result-term-list
                       (add-terms term-1-list term-2-list)))
                  (begin
                    (make-polynomial var-1 result-term-list)
                    ))
                ))
            (begin
              (display
               (format
                #f "add-poly error: var-1 (~a) must be " var-1))
              (display
               (format
                #f "equal to var-2 (~a)~%" var-2))
              (display (format #f "quitting...~%"))
              (force-output)
              (quit)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-terms L1 L2)
  (begin
    (cond
     ((empty-termlist? L1)
      (begin
        L2
        ))
     ((empty-termlist? L2)
      (begin
        L1
        ))
     (else
      (begin
        (let ((t1 (first-term L1))
              (t2 (first-term L2)))
          (begin
            (cond
             ((> (order t1) (order t2))
              (begin
                (adjoin-term
                 t1 (add-terms (rest-terms L1) L2))
                ))
             ((< (order t1) (order t2))
              (begin
                (adjoin-term
                 t2 (add-terms L1 (rest-terms L2)))
                ))
             (else
              (begin
                (adjoin-term
                 (make-term (order t1)
                            (+ (coeff t1) (coeff t2)))
                 (add-terms (rest-terms L1)
                            (rest-terms L2)))
                )))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (negate apoly)
  (define (local-iter term-list acc-list)
    (begin
      (if (null? term-list)
          (begin
            acc-list)
          (begin
            (let ((first-term (car term-list))
                  (tail-list (cdr term-list)))
              (let ((o1 (order first-term))
                    (c1 (coeff first-term)))
                (let ((neg-coeff (* -1 c1)))
                  (let ((neg-term (make-term o1 c1)))
                    (let ((next-acc-list
                           (cons neg-term acc-list)))
                      (begin
                        (local-iter tail-list next-acc-list)
                        ))
                    ))
                ))
            ))
      ))
  (begin
    (let ((neg-term-list
           (reverse
            (local-iter (term-list apoly) (list))))
          (var (variable apoly)))
      (begin
        (make-polynomial var neg-term-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-poly p1 p2)
  (begin
    (let ((n-p2 (negate p2)))
      (begin
        (add-poly p1 n-p2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (negate-terms at-list)
  (define (local-iter term-list acc-list)
    (begin
      (if (null? term-list)
          (begin
            acc-list)
          (begin
            (let ((first-term (car term-list))
                  (tail-list (cdr term-list)))
              (let ((o1 (order first-term))
                    (c1 (coeff first-term)))
                (let ((neg-coeff (* -1 c1)))
                  (let ((neg-term
                         (make-term o1 neg-coeff)))
                    (let ((next-acc-list
                           (cons neg-term acc-list)))
                      (begin
                        (local-iter tail-list next-acc-list)
                        ))
                    ))
                ))
            ))
      ))
  (begin
    (let ((neg-term-list
           (reverse
            (local-iter at-list (list)))))
      (begin
        neg-term-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-terms t1 t2)
  (begin
    (let ((n-t2 (negate-terms t2)))
      (begin
        (add-terms t1 n-t2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mul-terms L1 L2)
  (begin
    (if (empty-termlist? L1)
        (begin
          (the-empty-termlist))
        (begin
          (add-terms
           (mul-term-by-all-terms (first-term L1) L2)
           (mul-terms (rest-terms L1) L2))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (mul-term-by-all-terms t1 L)
  (begin
    (if (empty-termlist? L)
        (begin
          the-empty-termlist)
        (begin
          (let ((t2 (first-term L)))
            (begin
              (adjoin-term
               (make-term
                (+ (order t1) (order t2))
                (* (coeff t1) (coeff t2)))
               (mul-term-by-all-terms
                t1 (rest-terms L)))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (sort-terms-list a-term-list)
  (begin
    (stable-sort
     a-term-list
     (lambda (a b)
       (begin
         (> (order a) (order b))
         )))
    ))

;;;#############################################################
;;;#############################################################
(define (div-terms L1 L2)
  (define (local-iter L1 L2)
    (begin
      (if (empty-termlist? L1)
          (begin
            (list the-empty-termlist
                  the-empty-termlist))
          (begin
            (let ((t1 (first-term L1))
                  (t2 (first-term L2)))
              (begin
                (if (> (order t2) (order t1))
                    (begin
                      (list the-empty-termlist L1))
                    (begin
                      (let ((new-c
                             (exact->inexact
                              (/ (coeff t1) (coeff t2))))
                            (new-o (- (order t1) (order t2))))
                        (let ((new-term
                               (make-term new-o new-c)))
                          (let ((rest-of-result
                                 (sub-terms
                                  L1 (mul-term-by-all-terms
                                      new-term L2))))
                            (let ((end-list-list
                                   (local-iter
                                    rest-of-result L2)))
                              (let ((acc-list
                                     (car end-list-list))
                                    (remainder
                                     (list-ref end-list-list 1)))
                                (begin
                                  (list
                                   (cons new-term acc-list)
                                   remainder)
                                  ))
                              ))
                          ))
                      ))
                ))
            ))
      ))
  (begin
    (let ((s-l1 (sort-terms-list L1))
          (s-l2 (sort-terms-list L2)))
      (begin
        (local-iter s-l1 s-l2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-list-lists-are-equal?
         slist-list rlist-list
         sub-name error-message
         result-hash-table)
  (begin
    (let ((slist-1 (car slist-list))
          (stail-1 (cdr slist-list))
          (rlist-1 (car rlist-list))
          (rtail-1 (cdr rlist-list)))
      (begin
        (if (not (list? slist-1))
            (begin
              (let ((selem-1 (list-ref slist-list 0))
                    (selem-2 (list-ref slist-list 1))
                    (relem-1 (list-ref rlist-list 0))
                    (relem-2 (list-ref rlist-list 1))
                    (tolerance 1e-9))
                (let ((err-msg-1
                       (format
                        #f ", shouldbe 1=~a, result 1=~a, "
                        selem-1 relem-1))
                      (err-msg-2
                       (format
                        #f ", shouldbe 2=~a, result 2=~a, "
                        selem-2 relem-2)))
                  (begin
                    (unittest2:assert?
                     (< (abs (- selem-1 relem-1)) tolerance)
                     sub-name
                     (string-append error-message err-msg-1)
                     result-hash-table)

                    (unittest2:assert?
                     (< (abs (- selem-2 relem-2)) tolerance)
                     sub-name
                     (string-append error-message err-msg-2)
                     result-hash-table)
                    ))
                ))
            (begin
              (if (not (null? slist-1))
                  (begin
                    (assert-list-lists-are-equal?
                     slist-1 rlist-1
                     sub-name error-message
                     result-hash-table)
                    ))
              (if (not (null? stail-1))
                  (begin
                    (assert-list-lists-are-equal?
                     stail-1 rtail-1
                     sub-name error-message
                     result-hash-table)
                    ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-div-terms-1 result-hash-table)
 (begin
   (let ((sub-name "test-div-terms-1")
         (test-list
          (list
           (list (list (list 0 -1) (list 5 1))
                 (list (list 0 -1) (list 2 1))
                 (list (list (list 3 1.0) (list 1 1.0))
                       (list (list 1 1.0) (list 0 -1))))
           (list (list (list 0 -1) (list 2 1))
                 (list (list 0 -1) (list 1 1))
                 (list (list (list 1 1.0) (list 0 1.0))
                       (list)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((dividend (list-ref this-list 0))
                  (divisor (list-ref this-list 1))
                  (shouldbe-list-list
                   (list-ref this-list 2)))
              (let ((result-list-list
                     (div-terms dividend divisor)))
                (let ((slen-1
                       (length (car shouldbe-list-list)))
                      (slen-2
                       (length (cadr shouldbe-list-list)))
                      (rlen-1
                       (length (car result-list-list)))
                      (rlen-2
                       (length (cadr result-list-list))))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-msg-2
                         (format
                          #f "dividend=~a, divisor=~a, "
                          dividend divisor))
                        (err-msg-3
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-list-list result-list-list))
                        (err-msg-4
                         (format
                          #f "shouldbe length=(~a, ~a), "
                          slen-1 slen-2))
                        (err-msg-5
                         (format
                          #f "result length=(~a, ~a)"
                          rlen-1 rlen-2)))
                    (begin
                      (unittest2:assert?
                       (and (equal? slen-1 rlen-1)
                            (equal? slen-2 rlen-2))
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2 err-msg-3
                        err-msg-4 err-msg-5)
                       result-hash-table)

                      (assert-list-lists-are-equal?
                       shouldbe-list-list result-list-list
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2
                        err-msg-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A univariate polynomial can be divided "))
    (display
     (format #f "by another one~%"))
    (display
     (format #f "to produce a polynomial quotient and "))
    (display
     (format #f "a polynomial~%"))
    (display
     (format #f "remainder. For example,~%"))
    (display
     (format #f "(x^5-1)/(x^2-1)=x^3+x, "))
    (display
     (format #f "remainder r-1~%"))
    (newline)
    (display
     (format #f "Division can be performed via "))
    (display
     (format #f "long division. That is,~%"))
    (display
     (format #f "divide the highest-order term of the "))
    (display
     (format #f "dividend by the~%"))
    (display
     (format #f "highest-order term of the divisor. "))
    (display
     (format #f "The result is the~%"))
    (display
     (format #f "first term of the quotient. Next, "))
    (display
     (format #f "multiply the result by~%"))
    (display
     (format #f "the divisor, subtract that from the "))
    (display
     (format #f "dividend, and produce the~%"))
    (display
     (format #f "rest of the answer by recursively "))
    (display
     (format #f "dividing the difference~%"))
    (display
     (format #f "by the divisor. Stop when the order "))
    (display
     (format #f "of the divisor exceeds~%"))
    (display
     (format #f "the order of the dividend and "))
    (display
     (format #f "declare the dividend to~%"))
    (display
     (format #f "be the remainder. Also, if the "))
    (display
     (format #f "dividend ever becomes zero,~%"))
    (display
     (format #f "return zero as both quotient "))
    (display
     (format #f "and remainder.~%"))
    (newline)
    (display
     (format #f "We can design a div-poly procedure "))
    (display
     (format #f "on the model of~%"))
    (display
     (format #f "add-poly and mul-poly. The procedure "))
    (display
     (format #f "checks to see if~%"))
    (display
     (format #f "the two polys have the same variable. "))
    (display
     (format #f "If so, div-poly~%"))
    (display
     (format #f "strips off the variable and passes "))
    (display
     (format #f "the problem to~%"))
    (display
     (format #f "div-terms, which performs the division "))
    (display
     (format #f "operation on term~%"))
    (display
     (format #f "lists. Div-poly finally reattaches the "))
    (display
     (format #f "variable to the~%"))
    (display
     (format #f "result supplied by div-terms. It is "))
    (display
     (format #f "convenient to design~%"))
    (display
     (format #f "div-terms to compute both the quotient "))
    (display
     (format #f "and the remainder of~%"))
    (display
     (format #f "a division. Div-terms can take two "))
    (display
     (format #f "term lists as arguments~%"))
    (display
     (format #f "and return a list of the quotient term "))
    (display
     (format #f "list and the remainder~%"))
    (display
     (format #f "term list.~%"))
    (newline)
    (display
     (format #f "Complete the following definition of "))
    (display
     (format #f "div-terms by filling in~%"))
    (display
     (format #f "the missing expressions. Use this to "))
    (display
     (format #f "implement div-poly,~%"))
    (display
     (format #f "which takes two polys as arguments and "))
    (display
     (format #f "returns a list of the~%"))
    (display
     (format #f "quotient and remainder polys.~%"))
    (display
     (format #f "(define (div-terms L1 L2)~%"))
    (display
     (format #f "  (if (empty-termlist? L1)~%"))
    (display
     (format #f "      (list (the-empty-termlist) "))
    (display
     (format #f "(the-empty-termlist))~%"))
    (display
     (format #f "      (let ((t1 (first-term L1))~%"))
    (display
     (format #f "            (t2 (first-term L2)))~%"))
    (display
     (format #f "        (if (> (order t2) (order t1))~%"))
    (display
     (format #f "            (list "))
    (display
     (format #f "(the-empty-termlist) L1)~%"))
    (display
     (format #f "            (let ((new-c "))
    (display
     (format #f "(div (coeff t1) (coeff t2)))~%"))
    (display
     (format #f "                  (new-o "))
    (display
     (format #f "(- (order t1) (order t2))))~%"))
    (display
     (format #f "              (let ((rest-of-result~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "<compute rest of result recursively>~%"))
    (display
     (format #f "                     ))~%"))
    (display
     (format #f "                "))
    (display
     (format #f "<form complete result>~%"))
    (display
     (format #f "                ))))))~%"))
    (newline)
    (display
     (format #f ";;; sort terms list in "))
    (display
     (format #f "descending order~%"))
    (display
     (format #f "(define (sort-terms-list "))
    (display
     (format #f "a-term-list)~%"))
    (display
     (format #f "  (stable-sort~%"))
    (display
     (format #f "   a-term-list~%"))
    (display
     (format #f "   (lambda (a b) "))
    (display
     (format #f "(> (order a) (order b)))~%"))
    (display
     (format #f "   ))~%"))
    (newline)
    (display
     (format #f "(define (div-terms L1 L2)~%"))
    (display
     (format #f "  (define (local-iter L1 L2)~%"))
    (display
     (format #f "    (if (empty-termlist? L1)~%"))
    (display
     (format #f "        (list the-empty-termlist "))
    (display
     (format #f "the-empty-termlist)~%"))
    (display
     (format #f "        (let ((t1 (first-term L1))~%"))
    (display
     (format #f "              (t2 (first-term L2)))~%"))
    (display
     (format #f "          (if (> (order t2) "))
    (display
     (format #f "(order t1))~%"))
    (display
     (format #f "              (list "))
    (display
     (format #f "the-empty-termlist L1)~%"))
    (display
     (format #f "              (let ((new-c "))
    (display
     (format #f "(exact->inexact (/ (coeff t1) "))
    (display
     (format #f "(coeff t2))))~%"))
    (display
     (format #f "                    (new-o "))
    (display
     (format #f "(- (order t1) (order t2))))~%"))
    (display
     (format #f "                (let ((new-term "))
    (display
     (format #f "(make-term new-o new-c)))~%"))
    (display
     (format #f "                  (let ((rest-of-result~%"))
    (display
     (format #f "                         (sub-terms "))
    (display
     (format #f "L1 (mul-term-by-all-terms "))
    (display
     (format #f "new-term L2))))~%"))
    (display
     (format #f "                    (let ((end-list-list "))
    (display
     (format #f "(local-iter rest-of-result L2)))~%"))
    (display
     (format #f "                      (let ((acc-list "))
    (display
     (format #f "(car end-list-list))~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "(remainder (list-ref "))
    (display
     (format #f "end-list-list 1)))~%"))
    (display
     (format #f "                        "))
    (display
     (format #f "(begin~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(list~%"))
    (display
     (format #f "                           "))
    (display
     (format #f "(cons new-term acc-list)~%"))
    (display
     (format #f "                           "))
    (display
     (format #f "remainder)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "))~%"))
    (display
     (format #f "                      "))
    (display
     (format #f "))~%"))
    (display
     (format #f "                  "))
    (display
     (format #f ")))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (let ((s-l1 "))
    (display
     (format #f "(sort-terms-list L1))~%"))
    (display
     (format #f "          (s-l2 "))
    (display
     (format #f "(sort-terms-list L2)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (local-iter s-l1 s-l2)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.91 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
