#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.76                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "As a large system with generic "))
    (display
     (format #f "operations evolves, new~%"))
    (display
     (format #f "types of data objects or new "))
    (display
     (format #f "operations may be needed.~%"))
    (display
     (format #f "For each of the three strategies -- "))
    (display
     (format #f "generic operations~%"))
    (display
     (format #f "with explicit dispatch, data-directed "))
    (display
     (format #f "style, and~%"))
    (display
     (format #f "message-passing-style -- describe the "))
    (display
     (format #f "changes that must~%"))
    (display
     (format #f "be made to a system in order to add "))
    (display
     (format #f "new types or new~%"))
    (display
     (format #f "operations. Which organization would "))
    (display
     (format #f "be most appropriate~%"))
    (display
     (format #f "for a system in which new types must "))
    (display
     (format #f "often be added?~%"))
    (display
     (format #f "Which would be most appropriate for "))
    (display
     (format #f "a system in which~%"))
    (display
     (format #f "new operations must often be added?~%"))
    (newline)
    (display
     (format #f "Explicit dispatch - if a new operation "))
    (display
     (format #f "or a new type~%"))
    (display
     (format #f "is added, then lots of work is "))
    (display
     (format #f "required to ensure~%"))
    (display
     (format #f "that every type supports the new "))
    (display
     (format #f "operation, or the~%"))
    (display
     (format #f "new type supports every required "))
    (display
     (format #f "operation. In other~%"))
    (display
     (format #f "words, changes must be made in many "))
    (display
     (format #f "places in the~%"))
    (display
     (format #f "code base.~%"))
    (newline)
    (display
     (format #f "Data-directed style - this technique "))
    (display
     (format #f "is additive, if a~%"))
    (display
     (format #f "new operation is added, then one new "))
    (display
     (format #f "operation needs to be~%"))
    (display
     (format #f "added (or put), for each type. If a "))
    (display
     (format #f "new type is added,~%"))
    (display
     (format #f "only one installed package needs to "))
    (display
     (format #f "be created.~%"))
    (newline)
    (display
     (format #f "Message-passing style - this "))
    (display
     (format #f "technique is similar to~%"))
    (display
     (format #f "the data-directed style, an 'inverse' "))
    (display
     (format #f "of the data-directed~%"))
    (display
     (format #f "style.  If a new operation is "))
    (display
     (format #f "to be added, then~%"))
    (display
     (format #f "each type must be modified to "))
    (display
     (format #f "support the new operation.~%"))
    (display
     (format #f "If a new type is added, then "))
    (display
     (format #f "all the required~%"))
    (display
     (format #f "operations must written.~%"))
    (newline)
    (display
     (format #f "All three types are appropriate "))
    (display
     (format #f "for organizations where~%"))
    (display
     (format #f "the pace of change is glacial, and "))
    (display
     (format #f "code never changes.~%"))
    (newline)
    (display
     (format #f "When an organization often adds "))
    (display
     (format #f "new types then either~%"))
    (display
     (format #f "data-directed style or message-passing "))
    (display
     (format #f "style are appropriate.~%"))
    (newline)
    (display
     (format #f "When an organization often adds new "))
    (display
     (format #f "operations, then either~%"))
    (display
     (format #f "data-directed style or message-passing "))
    (display
     (format #f "style are appropriate,~%"))
    (display
     (format #f "since they both have about the same "))
    (display
     (format #f "amount of work to~%"))
    (display
     (format #f "accomplish.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.76 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
