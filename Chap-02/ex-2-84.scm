#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.84                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (raise-integer->rational ii-int)
  (begin
    (if (eq? (type-tag ii-int) 'scheme-number)
        (begin
          (let ((result (make-rat (contents ii-int) 1)))
            (begin
              result
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (raise-rational->real rr-rat)
  (begin
    (if (eq? (type-tag rr-rat) 'rational)
        (begin
          (let ((rr (contents rr-rat)))
            (let ((rr-numer (numer rr))
                  (rr-denom (denom rr)))
              (begin
                (exact->inexact
                 (/ rr-numer rr-denom))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (raise-real->complex rr-real)
  (begin
    (if (eq? (type-tag rr-real) 'real)
        (begin
          (let ((result (make-complex (contents rr-real) 0)))
            (begin
              result
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-max-type t1 t2)
  (begin
    (cond
     ((eq? t1 'scheme-number)
      (begin
        t2
        ))
     ((eq? t2 'scheme-number)
      (begin
        t1
        ))
     ((eq? t1 'rational)
      (begin
        t2
        ))
     ((eq? t2 'rational)
      (begin
        t1
        ))
     ((eq? t1 'real)
      (begin
        t2
        ))
     ((eq? t2 'real)
      (begin
        t1
        ))
     (else
      (begin
        t1
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
        (begin
          (if proc
              (begin
                (apply proc (map contents args)))
              (begin
                (if (= (length args) 2)
                    (begin
                      (let ((type1 (car type-tags))
                            (type2 (cadr type-tags))
                            (a1 (car args))
                            (a2 (cadr args)))
                        (let ((max-type
                               (find-max-type type1 type2)))
                          (begin
                            (if (eq? max-type type1)
                                (begin
                                  (let ((next-a2
                                         ((get-coercion
                                           'raise type2) a2)))
                                    (begin
                                      (apply-generic op a1 next-a2)
                                      )))
                                (begin
                                  (let ((next-a1
                                         ((get-coercion 'raise type1) a1)))
                                    (begin
                                      (apply-generic op next-a1 a2)
                                      ))
                                  ))
                            ))
                        ))
                    (begin
                      (display
                       (format
                        #f "apply-generic error: no method~%"))
                      (display
                       (format
                        #f "for these types ~a ~a~%" op args))
                      (display
                       (format
                        #f "quitting...~%"))
                      (force-output)
                      (quit)
                      ))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Using the raise operation of exercise "))
    (display
     (format #f "2.83, modify the~%"))
    (display
     (format #f "apply-generic procedure so that it "))
    (display
     (format #f "coerces its arguments~%"))
    (display
     (format #f "to have the same type by the method "))
    (display
     (format #f "of successive raising,~%"))
    (display
     (format #f "as discussed in this section. You will "))
    (display
     (format #f "need to devise a way~%"))
    (display
     (format #f "to test which of two types is higher "))
    (display
     (format #f "in the tower. Do~%"))
    (display
     (format #f "this in a manner that is \"compatible\" "))
    (display
     (format #f "with the rest of~%"))
    (display
     (format #f "the system and will not lead to problems "))
    (display
     (format #f "in adding new~%"))
    (display
     (format #f "levels to the tower.~%"))
    (newline)
    (display
     (format #f "(define (find-max-type t1 t2)~%"))
    (display
     (format #f "  (cond~%"))
    (display
     (format #f "   ((eq? t1 'scheme-number) t2)~%"))
    (display
     (format #f "   ((eq? t2 'scheme-number) t1)~%"))
    (display
     (format #f "   ((eq? t1 'rational) t2)~%"))
    (display
     (format #f "   ((eq? t2 'rational) t1)~%"))
    (display
     (format #f "   ((eq? t1 'real) t2)~%"))
    (display
     (format #f "   ((eq? t2 'real) t1)~%"))
    (display
     (format #f "   (else~%"))
    (display
     (format #f "    t1~%"))
    (display
     (format #f "    )))~%"))
    (newline)
    (display
     (format #f "(define (apply-generic op . args)~%"))
    (display
     (format #f "  (let ((type-tags (map "))
    (display
     (format #f "type-tag args)))~%"))
    (display
     (format #f "    (let ((proc (get op type-tags)))~%"))
    (display
     (format #f "      (if proc~%"))
    (display
     (format #f "          (apply proc (map "))
    (display
     (format #f "contents args))~%"))
    (display
     (format #f "          (if (= (length args) 2)~%"))
    (display
     (format #f "              (let ((type1 (car "))
    (display
     (format #f "type-tags))~%"))
    (display
     (format #f "                    (type2 (cadr "))
    (display
     (format #f "type-tags))~%"))
    (display
     (format #f "                    (a1 (car args))~%"))
    (display
     (format #f "                    (a2 (cadr args)))~%"))
    (display
     (format #f "                (let ((max-type "))
    (display
     (format #f "(find-max-type type1 type2)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (if (eq? max-type "))
    (display
     (format #f "type1)~%"))
    (display
     (format #f "                        (begin~%"))
    (display
     (format #f "                          (let ((next-a2~%"))
    (display
     (format #f "                                 "))
    (display
     (format #f "((get-coercion 'raise type2) a2)))~%"))
    (display
     (format #f "                            (begin~%"))
    (display
     (format #f "                              "))
    (display
     (format #f "(apply-generic op a1 next-a2)~%"))
    (display
     (format #f "                              "))
    (display
     (format #f ")))~%"))
    (display
     (format #f "                        (begin~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(let ((next-a1~%"))
    (display
     (format #f "                                 "))
    (display
     (format #f "((get-coercion 'raise type1) a1)))~%"))
    (display
     (format #f "                            (begin~%"))
    (display
     (format #f "                              "))
    (display
     (format #f "(apply-generic op next-a1 a2)~%"))
    (display
     (format #f "                              ))~%"))
    (display
     (format #f "                          ))~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (display~%"))
    (display
     (format #f "                 (format~%"))
    (display
     (format #f "                  #f \"apply-generic "))
    (display
     (format #f "error: no method for these \"))~%"))
    (display
     (format #f "                (display~%"))
    (display
     (format #f "                 (format~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "#f \"types ~~a ~~a~~%\"~%"))
    (display
     (format #f "                  op args))~%"))
    (display
     (format #f "                (force-output)~%"))
    (display
     (format #f "                (quit)~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "    ))~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.84 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
