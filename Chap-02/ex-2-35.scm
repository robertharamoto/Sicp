#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.35                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (enumerate-tree tree)
  (begin
    (cond
     ((null? tree)
      (begin
        (list)
        ))
     ((not (pair? tree))
      (begin
        (list tree)
        ))
     (else
      (begin
        (append (enumerate-tree (car tree))
                (enumerate-tree (cdr tree)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (length sequence)
  (begin
    (accumulate (lambda (x y) (1+ y)) 0 sequence)
    ))

;;;#############################################################
;;;#############################################################
(define (count-leaves atree)
  (begin
    (accumulate (lambda (x y) (1+ y)) 0
                (enumerate-tree atree))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-leaves-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-leaves-1")
         (test-list
          (list
           (list (list 1) 1)
           (list (list 1 2) 2)
           (list (list 1 2 3) 3)
           (list (list
                  1
                  (list
                   2 (list 3 (list 4 5) (list 6 7))))
                 7)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((atree (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (count-leaves atree)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : atree=~a, "
                        sub-name test-label-index
                        atree))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Redefine count-leaves from section "))
    (display
     (format #f "2.2.2 as an accumulation:~%"))
    (display
     (format #f "(define (count-leaves t)~%"))
    (display
     (format #f "  (accumulate <??> <??> "))
    (display
     (format #f "(map <??> <??>)))~%"))
    (newline)
    (display
     (format #f "(define (count-leaves atree)~%"))
    (display
     (format #f "  (accumulate (lambda (x y) (1+ y)) 0~%"))
    (display
     (format #f "              (enumerate-tree atree)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((alist
           (list 1 2 (list 3 4 (list (list 5 6) 7))))
          (blist
           (list (list 1 (list 2 (list 3 (list 4 5) (list 6 7)))))))
      (begin
        (display
         (format #f "(count-leaves ~a) = ~a~%"
                 alist (count-leaves alist)))
        (display
         (format #f "(count-leaves ~a) = ~a~%"
                 blist (count-leaves blist)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.35 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
