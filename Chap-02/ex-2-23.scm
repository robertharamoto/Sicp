#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.23                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-for-each proc alist)
  (define (local-iter alist)
    (begin
      (if (not (null? alist))
          (begin
            (proc (car alist))
            (local-iter (cdr alist))
            ))
      ))
  (begin
    (local-iter alist)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The procedure for-each is similar "))
    (display
     (format #f "to map. It takes~%"))
    (display
     (format #f "as arguments a procedure and a list "))
    (display
     (format #f "of elements. However,~%"))
    (display
     (format #f "rather than forming a list of the "))
    (display
     (format #f "results, for-each~%"))
    (display
     (format #f "just applies the procedure to each of "))
    (display
     (format #f "of the elements~%"))
    (display
     (format #f "in turn, from left to right. The "))
    (display
     (format #f "values returned by~%"))
    (display
     (format #f "applying the procedure to the "))
    (display
     (format #f "elements are not~%"))
    (display
     (format #f "used at all -- for-each is used with "))
    (display
     (format #f "procedures that~%"))
    (display
     (format #f "perform an action, such as printing. "))
    (display
     (format #f "For example,~%"))
    (newline)
    (display
     (format #f "(for-each (lambda (x) "))
    (display
     (format #f "(newline) (display x))~%"))
    (display
     (format #f "          (list 57 321 88))~%"))
    (display
     (format #f "57~%"))
    (display
     (format #f "321~%"))
    (display
     (format #f "88~%"))
    (display
     (format #f "The value returned by the call to "))
    (display
     (format #f "for-each (not~%"))
    (display
     (format #f "illustrated above) can be something "))
    (display
     (format #f "arbitrary, such as~%"))
    (display
     (format #f "true. Give an implementation of "))
    (display
     (format #f "for-each.~%"))
    (newline)
    (display
     (format #f "(define (my-for-each proc alist)~%"))
    (display
     (format #f "  (define (local-iter alist)~%"))
    (display
     (format #f "    (if (not (null? alist))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (proc (car alist))~%"))
    (display
     (format #f "          (local-iter (cdr tail-list))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (local-iter alist)~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format
      #f "(my-for-each (lambda (x) (newline) "))
    (display
     (format #f "(display x)) (list 57 321 88))~%"))

    (my-for-each
     (lambda (x) (newline) (display x))
     (list 57 321 88))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.23 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
