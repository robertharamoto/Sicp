#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.60                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-1
             (format
              #f ", shouldbe=~a, result=~a"
              shouldbe-list result-list))
            (err-msg-2
             (format
              #f ", length shouldbe=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append
            err-start err-msg-1 err-msg-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((sflag (member s-elem result-list))
                      (err-msg-3
                       (format
                        #f ", missing element ~a"
                        s-elem)))
                  (begin
                    (unittest2:assert?
                     (not (equal? sflag #f))
                     sub-name
                     (string-append
                      err-start err-msg-1 err-msg-3)
                     result-hash-table)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (element-of-set? elem set)
  (begin
    (let ((aflag (member elem set)))
      (begin
        (if (equal? aflag #f)
            (begin
              #f)
            (begin
              #t
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-set elem set)
  (begin
    (cons elem set)
    ))

;;;#############################################################
;;;#############################################################
(define (union-set set-1 set-2)
  (begin
    (append set-1 set-2)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-union-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-union-set-1")
         (test-list
          (list
           (list (list 'a 'b) (list 'c 'd)
                 (list 'a 'b 'c 'd))
           (list (list 'a 'b 'c) (list 'c 'd)
                 (list 'a 'b 'c 'c 'd))
           (list (list 'a 'b 'c) (list 'c 'd 'e)
                 (list 'a 'b 'c 'c 'd 'e))
           (list (list 1 4) (list 2 3 4)
                 (list 1 4 2 3 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-1 (list-ref this-list 0))
                  (set-2 (list-ref this-list 1))
                  (shouldbe-set (list-ref this-list 2)))
              (let ((result-set (union-set set-1 set-2)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : set-1=~a, set-2=~a, "
                        sub-name test-label-index
                        set-1 set-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-set result-set
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (intersection-set set-1 set-2)
  (define (local-iter set-a set-b acc-set)
    (begin
      (if (or (null? set-a)
              (<= (length set-a) 0))
          (begin
            acc-set)
          (begin
            (let ((a-elem (car set-a))
                  (a-tail (cdr set-a)))
              (let ((a-in-b (member a-elem set-b)))
                (begin
                  (if (equal? a-in-b #f)
                      (begin
                        (local-iter
                         a-tail set-b acc-set))
                      (begin
                        (local-iter
                         a-tail set-b
                         (cons a-elem acc-set))
                        ))
                  )))
            ))
      ))
  (begin
    (local-iter set-1 set-2 (list))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-intersection-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-intersection-set-1")
         (test-list
          (list
           (list (list 'a 'b) (list 'c 'd)
                 (list))
           (list (list 'a 'b 'c) (list 'c 'd)
                 (list 'c))
           (list (list 'a 'b 'c 'e) (list 'c 'd 'e)
                 (list 'c 'e))
           (list (list 1 2 4) (list 2 3 4)
                 (list 2 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-1 (list-ref this-list 0))
                  (set-2 (list-ref this-list 1))
                  (shouldbe-set (list-ref this-list 2)))
              (let ((result-set (intersection-set set-1 set-2)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : set-1=~a, set-2=~a, "
                        sub-name test-label-index
                        set-1 set-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-set result-set
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "We specified that a set would be "))
    (display
     (format #f "represented as a list~%"))
    (display
     (format #f "with no duplicates. Now suppose we "))
    (display
     (format #f "allow duplicates. For~%"))
    (display
     (format #f "instance, the set {1,2,3} could be "))
    (display
     (format #f "represented as~%"))
    (display
     (format #f "the list (2 3 2 1 3 2 2). Design "))
    (display
     (format #f "procedures element-of-set?,~%"))
    (display
     (format #f "adjoin-set, union-set, and "))
    (display
     (format #f "intersection-set that operate~%"))
    (display
     (format #f "on this representation. How does the "))
    (display
     (format #f "efficiency of each~%"))
    (display
     (format #f "compare with the corresponding "))
    (display
     (format #f "procedure for the~%"))
    (display
     (format #f "non-duplicate representation? Are "))
    (display
     (format #f "there applications for~%"))
    (display
     (format #f "which you would use this representation "))
    (display
     (format #f "in preference to~%"))
    (display
     (format #f "to the non-duplicate one?~%"))
    (newline)
    (display
     (format #f "The efficiency of element-of-set? and "))
    (display
     (format #f "intersection-set~%"))
    (display
     (format #f "are about the same as in the "))
    (display
     (format #f "non-duplicate case,~%"))
    (display
     (format #f "with the exception that the sets are "))
    (display
     (format #f "larger in this~%"))
    (display
     (format #f "case. The efficiency of adjoin-set and "))
    (display
     (format #f "union-set are much~%"))
    (display
     (format #f "greater since there is no need to "))
    (display
     (format #f "check to see if an~%"))
    (display
     (format #f "element already exists in the "))
    (display
     (format #f "resulting set.~%"))
    (newline)
    (display
     (format #f "When absolute speed is key, the "))
    (display
     (format #f "non-duplicate method would~%"))
    (display
     (format #f "be preferred, in that case, a "))
    (display
     (format #f "delete-duplicates function~%"))
    (display
     (format #f "should be defined to trim the excess "))
    (display
     (format #f "elements periodically.~%"))
    (newline)
    (display
     (format #f "(define (element-of-set? elem set)~%"))
    (display
     (format #f "  (let ((aflag (member elem set)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (if (equal? aflag #f)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            #f)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            #t~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "      )))~%"))
    (newline)
    (display
     (format #f "(define (adjoin-set elem set)~%"))
    (display
     (format #f "  (cons elem set))~%"))
    (newline)
    (display
     (format #f "(define (union-set set-1 set-2)~%"))
    (display
     (format #f "  (append set-1 set-2))~%"))
    (newline)
    (display
     (format #f "(define (intersection-set set-1 set-2)~%"))
    (display
     (format #f "  (define (local-iter "))
    (display
     (format #f "set-a set-b acc-set)~%"))
    (display
     (format #f "    (if (or (null? set-a)~%"))
    (display
     (format #f "            (<= (length set-a) 0))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          acc-set)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((a-elem (car set-a))~%"))
    (display
     (format #f "                (a-tail (cdr set-a)))~%"))
    (display
     (format #f "            (let ((a-in-b "))
    (display
     (format #f "(member a-elem set-b)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (equal? a-in-b #f)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (local-iter "))
    (display
     (format #f "a-tail set-b acc-set))~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (local-iter "))
    (display
     (format #f "a-tail set-b (cons a-elem acc-set))~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (local-iter set-1 set-2 (list))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.60 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
