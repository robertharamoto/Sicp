#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.2                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-point x y)
  (begin
    (cons x y)
    ))

;;;#############################################################
;;;#############################################################
(define (x-point xx)
  (begin
    (car xx)
    ))

;;;#############################################################
;;;#############################################################
(define (y-point xx)
  (begin
    (cdr xx)
    ))

;;;#############################################################
;;;#############################################################
(define (make-segment start end)
  (begin
    (if (and
         (pair? start)
         (pair? end))
        (begin
          (list start end))
        (begin
          (display
           (format
            #f "make-segment error: expecting two points! "))
          (display
           (format
            #f "received ~a ~a~%" start end))
          (display (format #f "quitting...~%"))
          (force-output)
          (quit)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (start-segment xx)
  (begin
    (list-ref xx 0)
    ))

;;;#############################################################
;;;#############################################################
(define (end-segment xx)
  (begin
    (list-ref xx 1)
    ))

;;;#############################################################
;;;#############################################################
(define (midpoint-segment xx)
  (begin
    (let ((start-point (start-segment xx))
          (end-point (end-segment xx)))
      (let ((mid-xx
             (/
              (+ (x-point start-point)
                 (x-point end-point))
              2))
            (mid-yy
             (/
              (+ (y-point start-point)
                 (y-point end-point))
              2)))
        (begin
          (make-point mid-xx mid-yy)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-midpoint-segement-1 result-hash-table)
 (begin
   (let ((sub-name "test-midpoint-segement-1")
         (test-list
          (list
           (list (cons 1 10) (cons 11 20) (cons 6 15))
           (list (cons 10 30) (cons 20 20) (cons 15 25))
           (list (cons -10 10) (cons 100 -20) (cons 45 -5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((start (list-ref this-list 0))
                  (end (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((lseg (make-segment start end)))
                (let ((result (midpoint-segment lseg)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : start=~a, end=~a, "
                          sub-name test-label-index start end))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-msg-1 err-msg-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (print-point x)
  (begin
    (display
     (format #f "(~a, ~a)" (x-point x) (y-point x)))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list (cons 1 2) (cons 2 4))
            (list (cons 40 50) (cons 40 100))
            (list (cons -1 -2) (cons 1 2))
            )))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((start (list-ref alist 0))
                   (end (list-ref alist 1)))
               (let ((lseg (make-segment start end)))
                 (let ((mid-point (midpoint-segment lseg)))
                   (begin
                     (display "start=")
                     (print-point start)
                     (display ", end=")
                     (print-point end)
                     (display ", midpoint=")
                     (print-point mid-point)
                     (newline)
                     ))
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the problem of representing "))
    (display
     (format #f "line segments in~%"))
    (display
     (format #f "a plane. Each segment is represented "))
    (display
     (format #f "as a pair of~%"))
    (display
     (format #f "points: a starting point and an "))
    (display
     (format #f "ending point. Define~%"))
    (display
     (format #f "a constructor make-segment and "))
    (display
     (format #f "selectors start-segment~%"))
    (display
     (format #f "and end-segment that define the "))
    (display
     (format #f "representation of segments~%"))
    (display
     (format #f "in terms of points. Furthermore, a "))
    (display
     (format #f "point can be represented~%"))
    (display
     (format #f "as a pair of numbers: the x coordinate "))
    (display
     (format #f "and the y coordinate.~%"))
    (display
     (format #f "Accordingly, specify a constructor "))
    (display
     (format #f "make-point and selectors~%"))
    (display
     (format #f "x-point and y-point that define this "))
    (display
     (format #f "representation. Finally,~%"))
    (display
     (format #f "using your selectors and constructors, "))
    (display
     (format #f "define a procedure~%"))
    (display
     (format #f "midpoint-segment that takes a line "))
    (display
     (format #f "segment as argument~%"))
    (display
     (format #f "and returns its midpoint (the point "))
    (display
     (format #f "whose coordinates~%"))
    (display
     (format #f "are the average of the coordinates "))
    (display
     (format #f "of the endpoints).~%"))
    (display
     (format #f "To try your procedures, you'll need "))
    (display
     (format #f "a way to print~%"))
    (display
     (format #f "points:~%"))
    (newline)
    (display
     (format #f "(define (print-point p)~%"))
    (display
     (format #f "  (newline)~%"))
    (display
     (format #f "  (display \"(\")~%"))
    (display
     (format #f "  (display (x-point p))~%"))
    (display
     (format #f "  (display \",\")~%"))
    (display
     (format #f "  (display (y-point p))~%"))
    (display
     (format #f "  (display \")\"))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.02 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
