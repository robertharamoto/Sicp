#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.39                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (accumulate-n op init seqs)
  (begin
    (if (or (null? seqs) (null? (car seqs)))
        (begin
          (list))
        (begin
          (cons (accumulate op init (map car seqs))
                (accumulate-n op init (map cdr seqs)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define fold-right accumulate)

;;;#############################################################
;;;#############################################################
(define (fold-left op initial sequence)
  (define (local-iter result rest)
    (begin
      (if (null? rest)
          (begin
            result)
          (begin
            (local-iter (op result (car rest))
                        (cdr rest))
            ))
      ))
  (begin
    (local-iter initial sequence)
    ))

;;;#############################################################
;;;#############################################################
(define (reverse-right sequence)
  (begin
    (fold-right
     (lambda (x y)
       (begin
         (append y (list x))))
     (list) sequence)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-right-1 result-hash-table)
 (begin
   (let ((sub-name "test-reverse-right-1")
         (test-list
          (list
           (list (list 1 2 3) (list 3 2 1))
           (list (list 1 2 3 4) (list 4 3 2 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (reverse-right alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (reverse-left sequence)
  (begin
    (fold-left
     (lambda (x y)
       (begin
         (cons y x)
         )) (list) sequence)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-left-1 result-hash-table)
 (begin
   (let ((sub-name "test-reverse-left-1")
         (test-list
          (list
           (list (list 1 2 3) (list 3 2 1))
           (list (list 1 2 3 4) (list 4 3 2 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (reverse-left alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Complete the following definitions of "))
    (display
     (format #f "reverse (exercise 2.18)~%"))
    (display
     (format #f "in terms of fold-right and fold-left "))
    (display
     (format #f "from exercise 2.38:~%"))
    (display
     (format #f "(define (reverse sequence)~%"))
    (display
     (format #f " (fold-right (lambda (x y) <??>) "))
    (display
     (format #f "nil sequence))~%"))
    (display
     (format #f "(define (reverse sequence)~%"))
    (display
     (format #f "  (fold-left (lambda (x y) <??>) "))
    (display
     (format #f " nil sequence))~%"))
    (newline)
    (display
     (format #f "(define (reverse-right sequence)~%"))
    (display
     (format #f "  (fold-right (lambda (x y)~%"))
    (display
     (format #f "    (append y (list x))) "))
    (display
     (format #f "(list) sequence))~%"))
    (newline)
    (display
     (format #f "(define (reverse-left sequence)~%"))
    (display
     (format #f "  (fold-left (lambda (x y)~%"))
    (display
     (format #f "    (cons y x)) (list) sequence))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((alist (list 1 2 3)))
      (begin
        (display
         (format #f "(reverse-right ~a) = ~a~%"
                 alist (reverse-right alist)))
        (display
         (format #f "(reverse-left ~a) = ~a~%"
                 alist (reverse-left alist)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.39 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
