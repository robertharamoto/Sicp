#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.90                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (order term)
  (begin
    (car term)
    ))

;;;#############################################################
;;;#############################################################
(define (coeff term)
  (begin
    (cadr term)
    ))

;;;#############################################################
;;;#############################################################
(define (empty-termlist? term-list)
  (begin
    (null? term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (tag term)
  (begin
    (cons 'dense term)
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-term term term-list)
  (begin
    (let ((torder (order term))
          (tl-order (1- (length term-list))))
      (begin
        (if (<= torder tl-order)
            (begin
              (let ((tl-index (- tl-order torder))
                    (local-term-list
                     (list-copy term-list)))
                (let ((tl-item
                       (list-ref
                        term-list tl-index)))
                  (let ((new-coeff
                         (+ (coeff term) tl-item)))
                    (begin
                      (list-set!
                       local-term-list tl-index new-coeff)
                      (tag
                       local-term-list)
                      ))
                  )))
            (begin
              (let ((zero-list
                     (make-list (1- (- torder tl-order)) 0)))
                (let ((new-term-list
                       (append (list (coeff term))
                               zero-list term-list)))
                  (begin
                    (tag new-term-list)
                    )))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-adjoin-term-1 result-hash-table)
 (begin
   (let ((sub-name "test-adjoin-term-1")
         (test-list
          (list
           (list (list 0 3) (list 2 1 5)
                 (list 'dense 2 1 8))
           (list (list 1 3) (list 2 1 5)
                 (list 'dense 2 4 5))
           (list (list 2 3) (list 2 1 5)
                 (list 'dense 5 1 5))
           (list (list 3 3) (list 2 1 5)
                 (list 'dense 3 2 1 5))
           (list (list 4 3) (list 2 1 5)
                 (list 'dense 3 0 2 1 5))
           (list (list 5 3) (list 2 1 5)
                 (list 'dense 3 0 0 2 1 5))
           (list (list 6 3) (list 2 1 5)
                 (list 'dense 3 0 0 0 2 1 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((term (list-ref this-list 0))
                  (term-list (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (adjoin-term term term-list)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "term=~a, term-list=~a"
                        term term-list))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose we want to have a polynomial "))
    (display
     (format #f "system that is efficient~%"))
    (display
     (format #f "for both sparse and dense polynomials. "))
    (display
     (format #f "One way to do this~%"))
    (display
     (format #f "is to allow both kinds of term-list "))
    (display
     (format #f "representations in our~%"))
    (display
     (format #f "system. The situation is analogous to "))
    (display
     (format #f "the complex-number example~%"))
    (display
     (format #f "of section 2.4, where we allowed both "))
    (display
     (format #f "rectangular and polar~%"))
    (display
     (format #f "representations. To do this we must "))
    (display
     (format #f "distinguish different~%"))
    (display
     (format #f "types of term lists and make the "))
    (display
     (format #f "operations on term~%"))
    (display
     (format #f "lists generic. Redesign the polynomial "))
    (display
     (format #f "system to implement~%"))
    (display
     (format #f "this generalization. This is a major "))
    (display
     (format #f "effort, not a~%"))
    (display
     (format #f "local change.~%"))
    (newline)
    (display
     (format #f ";;; within the "))
    (display
     (format #f "install-polynomial-package~%"))
    (display
     (format #f "(define (rest-terms term-list) "))
    (display
     (format #f "(cdr term-list))~%"))
    (display
     (format #f "(define (empty-termlist? "))
    (display
     (format #f "term-list) (null? term-list))~%"))
    (display
     (format #f "(define (make-term order "))
    (display
     (format #f "coeff) (list order coeff))~%"))
    (display
     (format #f "(define (order term) (car term))~%"))
    (display
     (format #f "(define (coeff term) (cadr term))~%"))
    (newline)
    (display
     (format #f "(define (install-sparse-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (tag x) "))
    (display
     (format #f "(attach-tag 'sparse x)) ~%"))
    (display
     (format #f "  (define (first-term term-list) "))
    (display
     (format #f "(car term-list))~%"))
    (display
     (format #f "  (define (adjoin-term "))
    (display
     (format #f "term term-list)~%"))
    (display
     (format #f "    (if (=zero? "))
    (display
     (format #f "(coeff term))~%"))
    (display
     (format #f "        term-list~%"))
    (display
     (format #f "        (cons term term-list)))~%"))
    (display
     (format #f "  (put 'first-term '(sparse)~%"))
    (display
     (format #f "    (lambda (x) (tag "))
    (display
     (format #f "(first-term x))))~%"))
    (display
     (format #f "  (put 'adjoin-term '(sparse sparse)~%"))
    (display
     (format #f "    (lambda (x y) (tag "))
    (display
     (format #f "(adjoin-term x y))))~%"))
    (display
     (format #f "  ...)~%"))
    (newline)
    (display
     (format #f "(define (install-dense-package)~%"))
    (display
     (format #f "    ...~%"))
    (display
     (format #f "  (define (tag x) (attach-tag "))
    (display
     (format #f "'dense x)) ~%"))
    (display
     (format #f "  (define (first-term term-list)~%"))
    (display
     (format #f "    (make-term (1- (length term-list)) "))
    (display
     (format #f "(car term-list)))~%"))
    (display
     (format #f "  (define (adjoin-term "))
    (display
     (format #f "term term-list)~%"))
    (display
     (format #f "    (let ((torder (order term))~%"))
    (display
     (format #f "          (tl-order (1- (length "))
    (display
     (format #f "term-list))))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (if (<= torder tl-order)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((tl-index "))
    (display
     (format #f "(- tl-order torder))~%"))
    (display
     (format #f "                    (local-term-list "))
    (display
     (format #f "(list-copy term-list)))~%"))
    (display
     (format #f "                (let ((tl-item~%"))
    (display
     (format #f "                       (list-ref "))
    (display
     (format #f "term-list tl-index)))~%"))
    (display
     (format #f "                  (let ((new-coeff "))
    (display
     (format #f "(+ (coeff term) tl-item)))~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (list-set! "))
    (display
     (format #f "local-term-list tl-index new-coeff)~%"))
    (display
     (format #f "                      (tag local-term-list)~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                  )))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((zero-list "))
    (display
     (format #f "(make-list (1- (- torder tl-order)) 0)))~%"))
    (display
     (format #f "                (let ((new-term-list~%"))
    (display
     (format #f "                       (append (list "))
    (display
     (format #f "(coeff term)) zero-list term-list)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (tag "))
    (display
     (format #f "new-term-list)~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (put 'first-term '(dense) first-term~%"))
    (display
     (format #f "    (lambda (x) (tag (first-term x))))~%"))
    (display
     (format #f "  (put 'adjoin-term '(dense dense)~%"))
    (display
     (format #f "    (lambda (x y) (tag "))
    (display
     (format #f "(adjoin-term x y))))~%"))
    (display
     (format #f "  ...)~%"))
    (newline)
    (display
     (format #f "(define (apply-generic op . args)~%"))
    (display
     (format #f "  (let ((type-tags "))
    (display
     (format #f "(map type-tag args)))~%"))
    (display
     (format #f "    (let ((proc (get op type-tags)))~%"))
    (display
     (format #f "      (if proc~%"))
    (display
     (format #f "          (apply proc "))
    (display
     (format #f "(map contents args))~%"))
    (display
     (format #f "          (error \"No method "))
    (display
     (format #f "for these types\"~%"))
    (display
     (format #f "                  (list "))
    (display
     (format #f "op type-tags))))))~%"))
    (newline)
    (display
     (format #f "(define (add-terms L1 L2)~%"))
    (display
     (format #f "  (cond ((empty-termlist? "))
    (display
     (format #f "L1) L2)~%"))
    (display
     (format #f "        ((empty-termlist? "))
    (display
     (format #f "L2) L1)~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         (let ((t1 "))
    (display
     (format #f "(apply-generic 'first-term L1))~%"))
    (display
     (format #f "               (t2 "))
    (display
     (format #f "(apply-generic 'first-term L2)))~%"))
    (display
     (format #f "           (let ((ct1 "))
    (display
     (format #f "(contents t1))~%"))
    (display
     (format #f "                 (ct2 "))
    (display
     (format #f "(contents t2)))~%"))
    (display
     (format #f "             (cond ((> (order ct1) "))
    (display
     (format #f "(order ct2))~%"))
    (display
     (format #f "                    (apply-generic "))
    (display
     (format #f "                   'adjoin-term~%"))
    (display
     (format #f "                      t1 "))
    (display
     (format #f "(add-terms (rest-terms L1) L2)))~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "((< (order ct1) (order ct2))~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "(apply-generic 'adjoin-term~%"))
    (display
     (format #f "                      "))
    (display
     (format #f "t2 (add-terms L1 (rest-terms L2))))~%"))
    (display
     (format #f "                   (else~%"))
    (display
     (format #f "                    (apply-generic "))
    (display
     (format #f "'adjoin-term~%"))
    (display
     (format #f "                      (attach-tag "))
    (display
     (format #f "(type-tag t1) (make-term (order ct1)~%"))
    (display
     (format #f "                          (add "))
    (display
     (format #f "(coeff ct1) (coeff ct2))))~%"))
    (display
     (format #f "                      (add-terms "))
    (display
     (format #f "(rest-terms L1)~%"))
    (display
     (format #f "                                 "))
    (display
     (format #f "(rest-terms L2))))))))))~%"))
    (newline)
    (display
     (format #f "(define (mul-terms L1 L2)~%"))
    (display
     (format #f "  (if (empty-termlist? L1)~%"))
    (display
     (format #f "      the-empty-termlist~%"))
    (display
     (format #f "      (add-terms "))
    (display
     (format #f "(mul-term-by-all-terms~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(apply-generic 'first-term L1) L2)~%"))
    (display
     (format #f "                 (mul-terms~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(rest-terms L1) L2))))~%"))
    (display
     (format #f "(define (mul-term-by-all-terms t1 L)~%"))
    (display
     (format #f "  (if (empty-termlist? L)~%"))
    (display
     (format #f "      the-empty-termlist~%"))
    (display
     (format #f "      (let ((t2 "))
    (display
     (format #f "(apply-generic 'first-term L))~%"))
    (display
     (format #f "            (this-type "))
    (display
     (format #f "(type-tag L)))~%"))
    (display
     (format #f "        (let ((ct1 (contents t1))~%"))
    (display
     (format #f "              (ct2 (contents t2)))~%"))
    (display
     (format #f "          (apply-generic 'adjoin-term~%"))
    (display
     (format #f "             (attach-tag "))
    (display
     (format #f "this-type (make-term~%"))
    (display
     (format #f "               (+ (order ct1) "))
    (display
     (format #f "(order ct2))~%"))
    (display
     (format #f "               (mul "))
    (display
     (format #f "(coeff ct1) (coeff ct2)))~%"))
    (display
     (format #f "              (mul-term-by-all-terms "))
    (display
     (format #f "t1 (rest-terms L))))~%"))
    (display
     (format #f "        ))))~%"))
    (display
     (format #f " ...~%"))
    (display
     (format #f " 'done)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.90 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
