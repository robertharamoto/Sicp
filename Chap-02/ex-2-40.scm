#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.40                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code macro and current-date-time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for run-all-tests functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (smallest-divisor nn)
  (define (local-find-divisor nn test-divisor max-divisor)
    (begin
      (cond
       ((> test-divisor max-divisor)
        (begin
          nn
          ))
       ((zero? (remainder nn test-divisor))
        (begin
          test-divisor
          ))
       (else
        (local-find-divisor nn (+ test-divisor 2) max-divisor)
        ))
      ))
  (begin
    (if (zero? (remainder nn 2))
        (begin
          2)
        (begin
          (let ((max-divisor (1+ (sqrt nn))))
            (begin
              (local-find-divisor nn 3 max-divisor)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-smallest-divisor-1 result-hash-table)
 (begin
   (let ((sub-name "test-smallest-divisor-1")
         (test-list
          (list
           (list 2 2) (list 3 3) (list 4 2)
           (list 5 5) (list 6 2) (list 7 7)
           (list 8 2) (list 9 3) (list 10 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((anum (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (smallest-divisor anum)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : anum=~a, "
                        sub-name test-label-index anum))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime? nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     (else
      (begin
        (= (smallest-divisor nn) nn)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-prime-1")
         (test-list
          (list
           (list 2 #t) (list 3 #t) (list 4 #f)
           (list 5 #t) (list 6 #f) (list 7 #t)
           (list 8 #f) (list 9 #f) (list 10 #f)
           (list 11 #t) (list 12 #f) (list 13 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((anum (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (prime? anum)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : anum=~a, "
                        sub-name test-label-index anum))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-accumulate-1 result-hash-table)
 (begin
   (let ((sub-name "test-accumulate-1")
         (test-list
          (list
           (list + 0 (list 1 2 3) 6)
           (list + 0 (list 1 2 3 4) 10)
           (list * 1 (list 1 2 3) 6)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((oper (list-ref this-list 0))
                  (initial (list-ref this-list 1))
                  (alist (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (accumulate oper initial alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : oper=~a, "
                        sub-name test-label-index oper))
                      (err-msg-2
                       (format
                        #f "initial=~a, alist=~a, "
                        initial alist))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (accumulate-n op init seqs)
  (begin
    (if (or (null? seqs) (null? (car seqs)))
        (begin
          (list))
        (begin
          (cons (accumulate op init (map car seqs))
                (accumulate-n op init (map cdr seqs)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define fold-right accumulate)

;;;#############################################################
;;;#############################################################
(define (fold-left op initial sequence)
  (define (iter result rest)
    (begin
      (if (null? rest)
          (begin
            result)
          (begin
            (iter (op result (car rest))
                  (cdr rest))
            ))
      ))
  (begin
    (iter initial sequence)
    ))

;;;#############################################################
;;;#############################################################
(define (enumerate-interval low high)
  (define (local-iter current acc-list)
    (begin
      (if (> current high)
          (begin
            acc-list)
          (begin
            (local-iter
             (1+ current)
             (append acc-list (list current)))
            ))
      ))
  (begin
    (local-iter low (list))
    ))

;;;#############################################################
;;;#############################################################
(define (flatmap proc seq)
  (begin
    (accumulate append (list) (map proc seq))
    ))

;;;#############################################################
;;;#############################################################
(define (unique-pairs n)
  (begin
    (flatmap
     (lambda (i)
       (begin
         (let ((imax (1- i)))
           (begin
             (map
              (lambda (j)
                (begin
                  (list j i)
                  )) (enumerate-interval 1 imax))
             ))
         )) (enumerate-interval 1 n))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-unique-pairs-1 result-hash-table)
 (begin
   (let ((sub-name "test-unique-pairs-1")
         (test-list
          (list
           (list 3 (list (list 1 2) (list 1 3) (list 2 3)))
           (list 4 (list
                    (list 1 2) (list 1 3) (list 1 4)
                    (list 2 3) (list 2 4) (list 3 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list (unique-pairs nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list-list
                        result-list-list)))
                  (begin
                    (for-each
                     (lambda (slist)
                       (begin
                         (let ((sflag
                                (member slist result-list-list)))
                           (begin
                             (unittest2:assert?
                              (not (equal? sflag #f))
                              sub-name
                              (string-append err-msg-1 err-msg-2)
                              result-hash-table)
                             ))
                         )) shouldbe-list-list)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime-sum? pair)
  (begin
    (prime? (+ (car pair) (cadr pair)))
    ))

;;;#############################################################
;;;#############################################################
(define (make-pair-sum pair)
  (begin
    (list (car pair) (cadr pair) (+ (car pair) (cadr pair)))
    ))

;;;#############################################################
;;;#############################################################
(define (prime-sum-pairs n)
  (begin
    (map make-pair-sum
         (filter prime-sum?
                 (unique-pairs n)))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a procedure unique-pairs that, "))
    (display
     (format #f "given an integer n,~%"))
    (display
     (format #f "generates the sequence of pairs (i,j) "))
    (display
     (format #f "with 1<= j< i<= n.~%"))
    (display
     (format #f "Use unique-pairs to simplify the "))
    (display
     (format #f "definition of~%"))
    (display
     (format #f "prime-sum-pairs given above.~%"))
    (newline)
    (display
     (format #f "(define (unique-pairs n)~%"))
    (display
     (format #f "  (flatmap~%"))
    (display
     (format #f "   (lambda (i)~%"))
    (display
     (format #f "     (begin~%"))
    (display
     (format #f "       (let ((imax (1- i)))~%"))
    (display
     (format #f "         (begin~%"))
    (display
     (format #f "           (map (lambda (j)~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (list i j)~%"))
    (display
     (format #f "                    )) "))
    (display
     (format #f "(enumerate-interval i))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "       )) (enumerate-interval n)))~%"))
    (newline)
    (display
     (format #f "(define (prime-sum-pairs n)~%"))
    (display
     (format #f "  (map make-pair-sum~%"))
    (display
     (format #f "       (filter prime-sum?~%"))
    (display
     (format #f "               (unique-pairs n))~%"))
    (display
     (format #f "       ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num-list (list 3 4 5)))
      (begin
        (for-each
         (lambda (anum)
           (begin
             (let ((llist (unique-pairs anum))
                   (ppairs (prime-sum-pairs anum)))
               (begin
                 (display
                  (format
                   #f "(unique-pairs ~a) = ~a~%"
                   anum (unique-pairs anum)))
                 (display
                  (format
                   #f "(prime-sum-pairs ~a) = ~a~%"
                   anum ppairs))
                 (newline)
                 (force-output)
                 ))
             )) num-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.40 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
