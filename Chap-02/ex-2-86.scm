#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.86                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
        (begin
          (if proc
              (begin
                (drop (apply proc (map contents args))))
              (begin
                (if (= (length args) 2)
                    (begin
                      (let ((type1 (car type-tags))
                            (type2 (cadr type-tags))
                            (a1 (car args))
                            (a2 (cadr args)))
                        (let ((max-type (find-max-type type1 type2)))
                          (begin
                            (if (eq? max-type type1)
                                (begin
                                  (let ((next-a2
                                         ((get-coercion 'raise type2) a2)))
                                    (begin
                                      (apply-generic op a1 next-a2)
                                      )))
                                (begin
                                  (let ((next-a1
                                         ((get-coercion 'raise type1) a1)))
                                    (begin
                                      (apply-generic op next-a1 a2)
                                      ))
                                  ))
                            ))
                        ))
                    (begin
                      (display
                       (format
                        #f "apply-generic error: no method "))
                      (display
                       (format
                        #f "for these types ~a ~a~%"
                        op args))
                      (display
                       (format
                        #f "quitting...~%"))
                      (force-output)
                      (quit)
                      ))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (install-complex-package)
  ;; imported procedures from rectangular and polar packages
  (define (make-from-real-imag x y)
    (begin
      ((get 'make-from-real-imag 'rectangular) x y)
      ))
  (define (make-from-mag-ang r a)
    (begin
      ((get 'make-from-mag-ang 'polar) r a)
      ))
  ;; internal procedures
  (define (add-complex z1 z2)
    (begin
      (let ((re1 (real-part z1))
            (re2 (real-part z2))
            (im1 (imag-part z1))
            (im2 (imag-part z1)))
        (begin
          (make-from-real-imag
           (apply-generic 'add re1 re2)
           (apply-generic 'add im1 im2))
          ))
      ))
  (define (sub-complex z1 z2)
    (begin
      (let ((re1 (real-part z1))
            (re2 (real-part z2))
            (im1 (imag-part z1))
            (im2 (imag-part z1)))
        (begin
          (make-from-real-imag
           (apply-generic 'sub re1 re2)
           (apply-generic 'sub im1 im2))
          ))
      ))
  (define (mul-complex z1 z2)
    (begin
      (let ((ma1 (magnitude z1))
            (ma2 (magnitude z2))
            (an1 (angle z1))
            (an2 (angle z1)))
        (begin
          (make-from-mag-ang
           (apply-generic 'mul ma1 ma2)
           (apply-generic 'add an1 an2))
          ))
      ))
  (define (div-complex z1 z2)
    (begin
      (let ((ma1 (magnitude z1))
            (ma2 (magnitude z2))
            (an1 (angle z1))
            (an2 (angle z1)))
        (begin
          (make-from-mag-ang
           (apply-generic 'div ma1 ma2)
           (apply-generic 'sub an1 an2))
          ))
      ))
  ;; interface to rest of the system
  (define (tag z)
    (begin
      (attach-tag 'complex z)
      ))
  (begin
    (put 'add '(complex complex)
         (lambda (z1 z2)
           (begin
             (tag (add-complex z1 z2))
             )))
    (put 'sub '(complex complex)
         (lambda (z1 z2)
           (begin
             (tag (sub-complex z1 z2))
             )))
    (put 'mul '(complex complex)
         (lambda (z1 z2)
           (begin
             (tag (mul-complex z1 z2))
             )))
    (put 'div '(complex complex)
         (lambda (z1 z2)
           (begin
             (tag (div-complex z1 z2))
             )))
    (put 'make-from-real-imag 'complex
         (lambda (x y)
           (begin
             (tag (make-from-real-imag x y))
             )))
    (put 'make-from-mag-ang 'complex
         (lambda (r a)
           (begin
             (tag (make-from-mag-ang r a))
             )))
    'done
    ))

;;;#############################################################
;;;#############################################################
(define (real-conversion anum)
  (begin
    (let ((ttype (type-tag anum)))
      (begin
        (cond
         ((eq? ttype 'scheme-number)
          (begin
            (exact->inexact (contents anum))
            ))
         ((eq? ttype 'rational)
          (begin
            (let ((this-rat (contents anum)))
              (let ((this-numer (car this-rat))
                    (this-denom (cdr this-rat)))
                (begin
                  (exact->inexact (/ this-numer this-denom))
                  )))
            ))
         (else
          (begin
            (contents anum)
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-sine anum)
  (begin
    (let ((areal (real-conversion anum)))
      (begin
        (sin areal)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-cosine anum)
  (begin
    (let ((areal (real-conversion anum)))
      (begin
        (cos areal)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose we want to handle complex "))
    (display
     (format #f "numbers whose real~%"))
    (display
     (format #f "parts, imaginary parts, magnitudes, "))
    (display
     (format #f "and angles can be~%"))
    (display
     (format #f "either ordinary numbers, rational "))
    (display
     (format #f "numbers, or other~%"))
    (display
     (format #f "numbers we might wish to add to "))
    (display
     (format #f "the system. Describe~%"))
    (display
     (format #f "and implement the changes to the "))
    (display
     (format #f "system needed to~%"))
    (display
     (format #f "accommodate this. You will have to "))
    (display
     (format #f "define operations such~%"))
    (display
     (format #f "as sine and cosine that are generic "))
    (display
     (format #f "over ordinary numbers~%"))
    (display
     (format #f "and rational numbers.~%"))
    (newline)
    (display
     (format #f "Everywhere in the complex package "))
    (display
     (format #f "where +, -, *, or /~%"))
    (display
     (format #f "occurs, you should replace it by a "))
    (display
     (format #f "call to~%"))
    (display
     (format #f "(apply-generic ...). In addition, "))
    (display
     (format #f "every math function~%"))
    (display
     (format #f "or library must be redefined so that "))
    (display
     (format #f "the arguments are~%"))
    (display
     (format #f "promoted to reals before calling "))
    (display
     (format #f "the math function.~%"))
    (newline)
    (display
     (format #f "The complex number package should "))
    (display
     (format #f "be changed as follows:~%"))
    (display
     (format #f "(define (install-complex-package)~%"))
    (display
     (format #f "  ;; imported procedures from "))
    (display
     (format #f "rectangular and polar packages~%"))
    (display
     (format #f "  (define "))
    (display
     (format #f "(make-from-real-imag x y)~%"))
    (display
     (format #f "    ((get 'make-from-real-imag "))
    (display
     (format #f "'rectangular) x y))~%"))
    (display
     (format #f "  (define (make-from-mag-ang r a)~%"))
    (display
     (format #f "    ((get 'make-from-mag-ang "))
    (display
     (format #f "'polar) r a))~%"))
    (display
     (format #f "  ;; internal procedures~%"))
    (display
     (format #f "  (define (add-complex z1 z2)~%"))
    (display
     (format #f "    (let ((re1 (real-part z1))~%"))
    (display
     (format #f "          (re2 (real-part z2))~%"))
    (display
     (format #f "          (im1 (imag-part z1))~%"))
    (display
     (format #f "          (im2 (imag-part z1)))~%"))
    (display
     (format #f "      (make-from-real-imag~%"))
    (display
     (format #f "       (apply-generic 'add re1 re2)~%"))
    (display
     (format #f "       (apply-generic 'add "))
    (display
     (format #f "im1 im2))))~%"))
    (display
     (format #f "  (define (sub-complex z1 z2)~%"))
    (display
     (format #f "    (let ((re1 (real-part z1))~%"))
    (display
     (format #f "          (re2 (real-part z2))~%"))
    (display
     (format #f "          (im1 (imag-part z1))~%"))
    (display
     (format #f "          (im2 (imag-part z1)))~%"))
    (display
     (format #f "      (make-from-real-imag~%"))
    (display
     (format #f "       (apply-generic 'sub "))
    (display
     (format #f "re1 re2)~%"))
    (display
     (format #f "       (apply-generic 'sub "))
    (display
     (format #f "im1 im2))))~%"))
    (display
     (format #f "  (define (mul-complex z1 z2)~%"))
    (display
     (format #f "    (let ((ma1 (magnitude z1))~%"))
    (display
     (format #f "          (ma2 (magnitude z2))~%"))
    (display
     (format #f "          (an1 (angle z1))~%"))
    (display
     (format #f "          (an2 (angle z1)))~%"))
    (display
     (format #f "      (make-from-mag-ang~%"))
    (display
     (format #f "       (apply-generic 'mul "))
    (display
     (format #f "ma1 ma2)~%"))
    (display
     (format #f "       (apply-generic 'add "))
    (display
     (format #f "an1 an2))))~%"))
    (display
     (format #f "  (define (div-complex z1 z2)~%"))
    (display
     (format #f "    (let ((ma1 (magnitude z1))~%"))
    (display
     (format #f "          (ma2 (magnitude z2))~%"))
    (display
     (format #f "          (an1 (angle z1))~%"))
    (display
     (format #f "          (an2 (angle z1)))~%"))
    (display
     (format #f "      (make-from-mag-ang~%"))
    (display
     (format #f "       (apply-generic 'div "))
    (display
     (format #f "ma1 ma2)~%"))
    (display
     (format #f "       (apply-generic 'sub "))
    (display
     (format #f "an1 an2))))~%"))
    (display
     (format #f "  ;; interface to rest of the system~%"))
    (display
     (format #f "  (define (tag z) (attach-tag "))
    (display
     (format #f "'complex z))~%"))
    (display
     (format #f "  (put 'add '(complex complex)~%"))
    (display
     (format #f "       (lambda (z1 z2) (tag "))
    (display
     (format #f "(add-complex z1 z2))))~%"))
    (display
     (format #f "  (put 'sub '(complex complex)~%"))
    (display
     (format #f "       (lambda (z1 z2) (tag "))
    (display
     (format #f "(sub-complex z1 z2))))~%"))
    (display
     (format #f "  (put 'mul '(complex complex)~%"))
    (display
     (format #f "       (lambda (z1 z2) (tag "))
    (display
     (format #f "(mul-complex z1 z2))))~%"))
    (display
     (format #f "  (put 'div '(complex complex)~%"))
    (display
     (format #f "       (lambda (z1 z2) (tag "))
    (display
     (format #f "(div-complex z1 z2))))~%"))
    (display
     (format #f "  (put 'make-from-real-imag "))
    (display
     (format #f "'complex~%"))
    (display
     (format #f "       (lambda (x y) (tag "))
    (display
     (format #f "(make-from-real-imag x y))))~%"))
    (display
     (format #f "  (put 'make-from-mag-ang "))
    (display
     (format #f "'complex~%"))
    (display
     (format #f "       (lambda (r a) (tag "))
    (display
     (format #f "(make-from-mag-ang r a))))~%"))
    (display
     (format #f "  'done)~%"))
    (newline)
    (display
     (format #f "Must also convert integers and "))
    (display
     (format #f "rationals to reals~%"))
    (display
     (format #f "for math functions: There are two "))
    (display
     (format #f "ways to do this,~%"))
    (display
     (format #f "the first is to define a "))
    (display
     (format #f "conversion function~%"))
    (display
     (format #f "and call it before calling the math "))
    (display
     (format #f "function. The~%"))
    (display
     (format #f "second is to uses the generic coercion "))
    (display
     (format #f "method.~%"))
    (display
     (format #f "(define (real-conversion anum)~%"))
    (display
     (format #f "  (let ((ttype "))
    (display
     (format #f "(type-tag anum)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (cond~%"))
    (display
     (format #f "       ((eq? ttype 'scheme-number) "))
    (display
     (format #f "(exact->inexact (contents anum)))~%"))
    (display
     (format #f "       ((eq? ttype 'rational)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((this-rat "))
    (display
     (format #f "(contents anum)))~%"))
    (display
     (format #f "            (let ((this-numer "))
    (display
     (format #f "(car this-rat))~%"))
    (display
     (format #f "                  (this-denom "))
    (display
     (format #f "(cdr this-rat)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (exact->inexact "))
    (display
     (format #f "(/ this-numer this-denom))~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "       (else~%"))
    (display
     (format #f "        (contents anum)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f "(define (my-sine anum)~%"))
    (display
     (format #f "  (let ((areal "))
    (display
     (format #f "(real-conversion anum)))~%"))
    (display
     (format #f "    (sin areal)~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (my-cosine anum)~%"))
    (display
     (format #f "  (let ((areal "))
    (display
     (format #f "(real-conversion anum)))~%"))
    (display
     (format #f "    (cos areal)~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "The generic way:~%"))
    (display
     (format #f "(define (install-integer-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (put-coercion "))
    (display
     (format #f "'scheme-number 'real~%"))
    (display
     (format #f "    (lambda (anum) "))
    (display
     (format #f "(exact->inexact (contents anum))))~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-rational-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (put-coercion 'rational 'real~%"))
    (display
     (format #f "    (lambda (anum)~%"))
    (display
     (format #f "      (let ((acontents "))
    (display
     (format #f "(contents anum)))~%"))
    (display
     (format #f "        (let ((num "))
    (display
     (format #f "(numer acontents))~%"))
    (display
     (format #f "              (den "))
    (display
     (format #f "(denom acontents)))~%"))
    (display
     (format #f "          (exact->inexact "))
    (display
     (format #f "(/ num den))))))~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-real-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (put-coercion 'real 'real~%"))
    (display
     (format #f "    (lambda (anum) (contents anum)))~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (my-sine anum)~%"))
    (display
     (format #f "  (let ((areal ((get-coercion "))
    (display
     (format #f "(type-tag anum) 'real) anum)))~%"))
    (display
     (format #f "    (sin areal)~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (my-cosine anum)~%"))
    (display
     (format #f "  (let ((areal "))
    (display
     (format #f "((get-coercion (type-tag anum) "))
    (display
     (format #f "'real) anum)))~%"))
    (display
     (format #f "    (cos areal)~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "...~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.86 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
