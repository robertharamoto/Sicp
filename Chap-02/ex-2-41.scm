#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.41                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code macro and current-date-time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for run-all-tests functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-accumulate-1 result-hash-table)
 (begin
   (let ((sub-name "test-accumulate-1")
         (test-list
          (list
           (list + 0 (list 1 2 3) 6)
           (list + 0 (list 1 2 3 4) 10)
           (list * 1 (list 1 2 3) 6)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((oper (list-ref this-list 0))
                  (initial (list-ref this-list 1))
                  (alist (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (accumulate oper initial alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : oper=~a, "
                        sub-name test-label-index oper))
                      (err-msg-2
                       (format
                        #f "initial=~a, alist=~a, "
                        initial alist))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (flatmap proc seq)
  (begin
    (accumulate append (list) (map proc seq))
    ))

;;;#############################################################
;;;#############################################################
(define (enumerate-interval low high)
  (define (local-iter current acc-list)
    (begin
      (if (> current high)
          (begin
            acc-list)
          (begin
            (local-iter
             (1+ current) (append acc-list (list current)))
            ))
      ))
  (begin
    (local-iter low (list))
    ))

;;;#############################################################
;;;#############################################################
(define (unique-pairs n)
  (begin
    (flatmap
     (lambda (i)
       (begin
         (let ((imax (1- i)))
           (begin
             (map (lambda (j)
                    (begin
                      (list j i)
                      )) (enumerate-interval 1 imax))
             ))
         )) (enumerate-interval 1 n))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-unique-pairs-1 result-hash-table)
 (begin
   (let ((sub-name "test-unique-pairs-1")
         (test-list
          (list
           (list 3 (list (list 1 2) (list 1 3) (list 2 3)))
           (list 4 (list
                    (list 1 2) (list 1 3) (list 1 4)
                    (list 2 3) (list 2 4) (list 3 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list (unique-pairs nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list-list
                        result-list-list)))
                  (begin
                    (for-each
                     (lambda (slist)
                       (begin
                         (let ((sflag
                                (member slist result-list-list)))
                           (begin
                             (unittest2:assert?
                              (not (equal? sflag #f))
                              sub-name
                              (string-append
                               err-msg-1 err-msg-2)
                              result-hash-table)
                             ))
                         )) shouldbe-list-list)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-triple-sum triple)
  (begin
    (list (car triple) (cadr triple)
          (car (cdr (cdr triple)))
          (+ (car triple) (cadr triple)
             (car (cdr (cdr triple)))))
    ))

;;;#############################################################
;;;#############################################################
(define (my-delete-duplicates a-list-list)
  (begin
    (let ((an-htable (make-hash-table)))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (hash-set! an-htable alist 1)
             )) a-list-list)
        (hash-map->list
         (lambda (key value)
           (begin
             key
             )) an-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-distinct-triplet? apair s)
  (begin
    (let ((a1 (car apair))
          (a2 (cadr apair)))
      (let ((a3 (- s a1 a2)))
        (begin
          (if (and (> a3 0)
                   (equal? (member a3 apair) #f))
              (begin
                #t)
              (begin
                #f
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (make-triplet a-pair s)
  (begin
    (let ((a1 (car a-pair))
          (a2 (cadr a-pair)))
      (let ((a3 (- s a1 a2)))
        (begin
          (sort (list a1 a2 a3) <)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-triplet-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-triplet-1")
         (test-list
          (list
           (list (list 1 2) 5 (list 1 2 2))
           (list (list 1 2) 6 (list 1 2 3))
           (list (list 1 2) 7 (list 1 2 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((apair (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list (make-triplet apair nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : apair=~a, nn=~a, "
                        sub-name test-label-index
                        apair nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list
                        result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sum-to-triples s)
  (begin
    (map
     make-triple-sum
     (my-delete-duplicates
      (map
       (lambda (apair)
         (begin
           (make-triplet apair s)
           ))
       (filter
        (lambda (apair)
          (begin
            (is-distinct-triplet? apair s)
            ))
        (unique-pairs s)))
      ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-to-triples-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-to-triples-1")
         (test-list
          (list
           (list 6 (list (list 1 2 3 6)))
           (list 7 (list (list 1 2 4 7)))
           (list 8 (list (list 1 3 4 8) (list 1 2 5 8)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list (sum-to-triples nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index
                        nn))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list-list
                        result-list-list)))
                  (begin
                    (for-each
                     (lambda (slist)
                       (begin
                         (let ((sflag
                                (member slist result-list-list)))
                           (begin
                             (unittest2:assert?
                              (not (equal? sflag #f))
                              sub-name
                              (string-append err-msg-1 err-msg-2)
                              result-hash-table)
                             ))
                         )) shouldbe-list-list)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Write a procedure to find all ordered "))
    (display
     (format #f "triples of distinct~%"))
    (display
     (format #f "positive integers i, j, and k less "))
    (display
     (format #f "than or equal to a~%"))
    (display
     (format #f "given integer n that sum to a given "))
    (display
     (format #f "integer s.~%"))
    (newline)

    (display
     (format #f "(define (sum-to-triples s)~%"))
    (display
     (format #f "  (map~%"))
    (display
     (format #f "   make-triple-sum~%"))
    (display
     (format #f "   (my-delete-duplicates~%"))
    (display
     (format #f "    (map~%"))
    (display
     (format #f "     (lambda (apair) "))
    (display
     (format #f "(make-triplet apair s))~%"))
    (display
     (format #f "     (filter~%"))
    (display
     (format #f "      (lambda (apair) "))
    (display
     (format #f "(is-distinct-triplet? apair s))~%"))
    (display
     (format #f "      (unique-pairs s)))~%"))
    (display
     (format #f "    )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num-list (list 6 7 8 9 10)))
      (begin
        (for-each
         (lambda (anum)
           (begin
             (let ((llist (sum-to-triples anum)))
               (begin
                 (display
                  (format
                   #f "(sum-to-triples ~a) = ~a~%"
                   anum llist))
                 (force-output)
                 ))
             )) num-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.41 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
