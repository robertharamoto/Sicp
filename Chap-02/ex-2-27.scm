#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.27                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-reverse alist)
  (define (local-iter this-list acc-list)
    (begin
      (if (null? this-list)
          (begin
            acc-list)
          (begin
            (local-iter
             (cdr this-list)
             (cons (car this-list) acc-list))
            ))
      ))
  (begin
    (if (null? alist)
        (begin
          (list))
        (begin
          (local-iter alist (list))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-my-reverse-1 result-hash-table)
 (begin
   (let ((sub-name "test-my-reverse-1")
         (test-list
          (list
           (list (list 1 2) (list 2 1))
           (list (list 1 (list 2 3))
                 (list (list 2 3) 1))
           (list (list 1 (list 2 3 (list 4 5)))
                 (list (list 2 3 (list 4 5)) 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (my-reverse alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (deep-reverse alist)
  (define (local-iter this-list acc-list)
    (begin
      (if (null? this-list)
          (begin
            acc-list)
          (begin
            (let ((this-elem (car this-list))
                  (tail-list (cdr this-list)))
              (begin
                (if (list? this-elem)
                    (begin
                      (let ((rev-elem
                             (local-iter this-elem (list))))
                        (begin
                          (local-iter
                           tail-list
                           (cons rev-elem acc-list))
                          )))
                    (begin
                      (local-iter
                       tail-list
                       (cons this-elem acc-list))
                      ))
                ))
            ))
      ))
  (begin
    (if (null? alist)
        (begin
          (list))
        (begin
          (local-iter alist (list))
          ))
    ))
;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-deep-reverse-1 result-hash-table)
 (begin
   (let ((sub-name "test-deep-reverse-1")
         (test-list
          (list
           (list (list 1 2) (list 2 1))
           (list (list 1 (list 2 3))
                 (list (list 3 2) 1))
           (list (list 1 (list 2 3 (list 4 5)))
                 (list (list (list 5 4) 3 2) 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (deep-reverse alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Modify your reverse procedure of "))
    (display
     (format #f "exercise 2.18 to~%"))
    (display
     (format #f "produce a deep-reverse procedure "))
    (display
     (format #f "that takes a list~%"))
    (display
     (format #f "as argument and returns as its value "))
    (display
     (format #f "the list with~%"))
    (display
     (format #f "its elements reversed and with all "))
    (display
     (format #f "sublists~%"))
    (display
     (format #f "deep-reversed as well. For example,~%"))
    (newline)
    (display
     (format #f "(define x (list "))
    (display
     (format #f "(list 1 2) (list 3 4)))~%"))
    (display
     (format #f "x~%"))
    (display
     (format #f "((1 2) (3 4))~%"))
    (display
     (format #f "(reverse x)~%"))
    (display
     (format #f "((3 4) (1 2))~%"))
    (display
     (format #f "(deep-reverse x)~%"))
    (display
     (format #f "((4 3) (2 1))~%"))
    (newline)
    (display
     (format #f "(define (deep-reverse alist)~%"))
    (display
     (format #f "  (define (local-iter "))
    (display
     (format #f "this-list acc-list)~%"))
    (display
     (format #f "    (if (null? this-list)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          acc-list)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((this-elem "))
    (display
     (format #f "(car this-list))~%"))
    (display
     (format #f "                (tail-list "))
    (display
     (format #f "(cdr this-list)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (if (list? this-elem)~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (let ((rev-elem~%"))
    (display
     (format #f "                           (local-iter "))
    (display
     (format #f "this-elem (list))))~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (local-iter~%"))
    (display
     (format #f "                         tail-list~%"))
    (display
     (format #f "                         (cons "))
    (display
     (format #f "rev-elem acc-list))~%"))
    (display
     (format #f "                        )))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (local-iter~%"))
    (display
     (format #f "                     tail-list~%"))
    (display
     (format #f "                     (cons "))
    (display
     (format #f "this-elem acc-list))~%"))
    (display
     (format #f "                    ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (if (null? alist)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (list))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (local-iter alist (list))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x (list (list 1 2) (list 3 4))))
      (begin
        (display
         (format #f "(reverse ~a) = ~a~%"
                 x (my-reverse x)))
        (display
         (format #f "(deep-reverse ~a) = ~a~%"
                 x (deep-reverse x)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.27 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
