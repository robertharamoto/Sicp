#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.72                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-leaf symbol weight)
  (begin
    (list 'leaf symbol weight)
    ))

;;;#############################################################
;;;#############################################################
(define (leaf? object)
  (begin
    (eq? (car object) 'leaf)
    ))

;;;#############################################################
;;;#############################################################
(define (symbol-leaf x)
  (begin
    (cadr x)
    ))

;;;#############################################################
;;;#############################################################
(define (weight-leaf x)
  (begin
    (caddr x)
    ))

;;;#############################################################
;;;#############################################################
(define (make-code-tree left right)
  (begin
    (list left
          right
          (append (symbols left) (symbols right))
          (+ (weight left) (weight right)))
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch tree)
  (begin
    (car tree)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch tree)
  (begin
    (cadr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (symbols tree)
  (begin
    (if (leaf? tree)
        (begin
          (list (symbol-leaf tree)))
        (begin
          (caddr tree)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight tree)
  (begin
    (if (leaf? tree)
        (begin
          (weight-leaf tree))
        (begin
          (cadddr tree)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (decode bits tree)
  (define (local-decode-1 bits current-branch)
    (begin
      (if (null? bits)
          (begin
            (list))
          (begin
            (let ((next-branch
                   (choose-branch (car bits) current-branch)))
              (begin
                (if (leaf? next-branch)
                    (begin
                      (cons (symbol-leaf next-branch)
                            (local-decode-1 (cdr bits) tree)))
                    (begin
                      (local-decode-1 (cdr bits) next-branch)
                      ))
                ))
            ))
      ))
  (begin
    (local-decode-1 bits tree)
    ))

;;;#############################################################
;;;#############################################################
(define (choose-branch bit branch)
  (begin
    (cond
     ((= bit 0)
      (begin
        (left-branch branch)
        ))
     ((= bit 1)
      (begin
        (right-branch branch)
        ))
     (else
      (begin
        (display
         (format #f "bad bit ~a -- CHOOSE-BRANCH~%" bit))
        (force-output)
        (quit)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-set x set)
  (begin
    (cond
     ((null? set)
      (begin
        (list x)
        ))
     ((< (weight x) (weight (car set)))
      (begin
        (cons x set)
        ))
     (else
      (begin
        (cons (car set)
              (adjoin-set x (cdr set)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (make-leaf-set pairs)
  (begin
    (if (null? pairs)
        (begin
          (list))
        (begin
          (let ((pair (car pairs)))
            (begin
              (adjoin-set (make-leaf (car pair)    ; symbol
                                     (cadr pair))  ; frequency
                          (make-leaf-set (cdr pairs)))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (encode message tree)
  (begin
    (if (null? message)
        (begin
          (list))
        (begin
          (append (encode-symbol (car message) tree)
                  (encode (cdr message) tree))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (encode-symbol asymbol enc-tree)
  (define (local-iter asymbol curr-tree acc-list dcount)
    (begin
      (if (null? curr-tree)
          (begin
            (list #f acc-list))
          (begin
            (let ((found-flag #f)
                  (result-acc-list (list)))
              (begin
                ;;; handle left branch
                (let ((next-left-branch
                       (left-branch curr-tree))
                      (next-acc-list
                       (append acc-list (list 0))))
                  (begin
                    (if (leaf? next-left-branch)
                        (begin
                          (let ((this-symbol
                                 (symbol-leaf next-left-branch)))
                            (begin
                              (if (equal? asymbol this-symbol)
                                  (begin
                                    (set! found-flag #t)
                                    (set! result-acc-list next-acc-list)
                                    ))
                              )))
                        (begin
                          (let ((left-result
                                 (local-iter
                                  asymbol next-left-branch
                                  next-acc-list (1+ dcount))))
                            (let ((lfound-flag (car left-result))
                                  (next-acc-list (cadr left-result))
                                  (next-dcount (caddr left-result)))
                              (begin
                                (if (equal? lfound-flag #t)
                                    (begin
                                      (set! found-flag #t)
                                      (set! result-acc-list next-acc-list)
                                      (set! dcount next-dcount)
                                      ))
                                )))
                          ))
                    ))
                ;;; handle right branch
                (if (equal? found-flag #f)
                    (begin
                      (let ((next-right-branch
                             (right-branch curr-tree))
                            (next-acc-list
                             (append acc-list (list 1))))
                        (begin
                          (if (leaf? next-right-branch)
                              (begin
                                (let ((this-symbol
                                       (symbol-leaf next-right-branch)))
                                  (begin
                                    (if (equal? asymbol this-symbol)
                                        (begin
                                          (set! found-flag #t)
                                          (set! result-acc-list next-acc-list)
                                          ))
                                    )))
                              (begin
                                (let ((right-result
                                       (local-iter
                                        asymbol next-right-branch
                                        next-acc-list (1+ dcount))))
                                  (let ((rfound-flag (car right-result))
                                        (next-acc-list (cadr right-result))
                                        (next-dcount (caddr right-result)))
                                    (begin
                                      (if (equal? rfound-flag #t)
                                          (begin
                                            (set! found-flag #t)
                                            (set! result-acc-list next-acc-list)
                                            (set! dcount next-dcount)
                                            ))
                                      )))
                                ))
                          ))
                      ))

                (list found-flag result-acc-list dcount)
                ))
            ))
      ))
  (begin
    (if (not (list? enc-tree))
        (begin
          (display
           (format #f "for symbol ~a, found invalid tree ~a~%"
                   asymbol enc-tree))
          (display (format #f "quitting...~%"))
          (force-output)
          (quit)
          ))

    (let ((a-result (local-iter asymbol enc-tree (list) 1)))
      (let ((fflag (car a-result))
            (acc-list (cadr a-result))
            (dcount (caddr a-result)))
        (begin
;;;          (display
;;;           (format #f "debug ~a took ~a iterations~%"
;;;                   asymbol dcount))
;;;          (force-output)
          (if (equal? fflag #t)
              (begin
                acc-list)
              (begin
                #f
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (generate-huffman-tree pairs)
  (begin
    (successive-merge (make-leaf-set pairs))
    ))

;;;#############################################################
;;;#############################################################
(define (successive-merge ascending-weight-order-leaves)
  (define (local-iter leaves-list)
    (begin
      (cond
       ((null? leaves-list)
        (begin
          (list)
          ))
       ((= (length leaves-list) 1)
        (begin
          (car leaves-list)
          ))
       (else
        (begin
          (let ((left-leaf (car leaves-list))
                (right-leaf (car (cdr leaves-list)))
                (remaining-list (cdr (cdr leaves-list))))
            (let ((next-tree
                   (make-code-tree left-leaf right-leaf)))
              (let ((next-list
                     (adjoin-set next-tree remaining-list)))
                (begin
                  (local-iter next-list)
                  ))
              ))
          )))
      ))
  (begin
    (local-iter ascending-weight-order-leaves)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-encode-decode-1 result-hash-table)
 (begin
   (let ((sub-name "test-encode-decode-1")
         (code-pairs
          (list (list 'a 2) (list 'na 16) (list 'boom 1)
                (list 'sha 3) (list 'get 2) (list 'yip 9)
                (list 'job 2) (list 'wah 1)))
         (test-list
          (list
           (list 'get 'a 'job)
           (list 'sha 'na 'na 'na 'na 'na 'na 'na 'na)
           (list 'get 'a 'job)
           (list 'sha 'na 'na 'na 'na 'na 'na 'na 'na)
           (list 'wah 'yip 'yip 'yip 'yip 'yip 'yip 'yip 'yip 'yip)
           (list 'sha 'boom)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-message)
          (begin
            (let ((code-tree
                   (generate-huffman-tree code-pairs)))
              (let ((encoded-message
                     (encode this-message code-tree)))
                (let ((decoded-message
                       (decode encoded-message code-tree)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : encoded=~a, "
                          sub-name test-label-index
                          encoded-message))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          this-message decoded-message)))
                    (begin
                      (unittest2:assert?
                       (equal? this-message decoded-message)
                       sub-name
                       (string-append err-msg-1 err-msg-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the encoding procedure "))
    (display
     (format #f "that you designed in~%"))
    (display
     (format #f "exercise 2.68. What is the order "))
    (display
     (format #f "of growth in the number~%"))
    (display
     (format #f "of steps needed to encode a symbol? "))
    (display
     (format #f "Be sure to include~%"))
    (display
     (format #f "the number of steps needed to "))
    (display
     (format #f "search the symbol list~%"))
    (display
     (format #f "at each node encountered. To answer "))
    (display
     (format #f "this question in general~%"))
    (display
     (format #f "is difficult. Consider the special "))
    (display
     (format #f "case where the relative~%"))
    (display
     (format #f "frequencies of the n symbols are as "))
    (display
     (format #f "described in exercise~%"))
    (display
     (format #f "2.71, and give the order of growth "))
    (display
     (format #f "(as a function of n)~%"))
    (display
     (format #f "of the number of steps needed to "))
    (display
     (format #f "encode the most frequent~%"))
    (display
     (format #f "and least frequent symbols in the "))
    (display
     (format #f "alphabet.~%"))
    (newline)
    (display
     (format #f "For the special case where the "))
    (display
     (format #f "encoding tree is the~%"))
    (display
     (format #f "same as exercise 2.71 (each "))
    (display
     (format #f "frequency is 2^(k-1)),~%"))
    (display
     (format #f "then the tree n-1 levels deep. "))
    (display
     (format #f "To find the most frequent~%"))
    (display
     (format #f "symbol takes 1 iteration "))
    (display
     (format #f "regardless of the~%"))
    (display
     (format #f "number of elements in the tree. "))
    (display
     (format #f "To find the least~%"))
    (display
     (format #f "frequent symbol takes n-1 "))
    (display
     (format #f "iterations or theta(n-1).~%"))
    (newline)
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list (list 'a 1) (list 'b 2) (list 'c 4)
                  (list 'd 8) (list 'e 16))
            (list (list 'a 1) (list 'b 2) (list 'c 4)
                  (list 'd 8) (list 'e 16) (list 'f 32)
                  (list 'g 64) (list 'h 128) (list 'i 256)
                  (list 'j 512)))))
      (begin
        (for-each
         (lambda (code-pairs)
           (begin
             (let ((code-tree
                    (generate-huffman-tree code-pairs))
                   (clen (length code-pairs)))
               (begin
                 (display
                  (format #f "code pairs = ~a, n = ~a~%"
                          code-pairs clen))
                 (display
                  (format #f "code tree = ~a~%" code-tree))
                 ;;; most frequent
                 (if (equal? clen 5)
                     (begin
                       (encode-symbol 'e code-tree))
                     (begin
                       (encode-symbol 'j code-tree)
                       ))
                 ;;; least frequent
                 (encode-symbol 'a code-tree)
                 (newline)
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.72 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
