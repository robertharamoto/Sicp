#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.79                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
        (begin
          (if proc
              (begin
                (apply proc (map contents args)))
              (begin
                (error
                 "No method for these types -- APPLY-GENERIC"
                 (list op type-tags))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (equ? num-1 num-2)
  (begin
    (cond
     ((and
       (number? num-1)
       (number? num-2))
      (begin
        (= num-1 num-2)
        ))
     ((eq? (type-tag num-1)
           (type-tag num-2))
      (begin
        (let ((t1 (type-tag num-1))
              (contents-1 (contents num-1))
              (t2 (type-tag num-2))
              (contents-2 (contents num-2)))
          (begin
            (cond
             ((eq? t1 'rational)
              (begin
                (let ((numer-1 (car contents-1))
                      (denom-1 (cdr contents-1))
                      (numer-2 (car contents-2))
                      (denom-2 (cdr contents-2)))
                  (begin
                    (and
                     (= numer-1 numer-2)
                     (= denom-1 denom-2))
                    ))
                ))
             ((eq? t1 'complex)
              (begin
                (let ((tag-1 (car contents-1))
                      (rest-1 (cdr contents-1))
                      (tag-2 (car contents-2))
                      (rest-2 (cdr contents-2)))
                  (let ((real-1 (car rest-1))
                        (imag-1 (cdr rest-1))
                        (real-2 (car rest-2))
                        (imag-2 (cdr rest-2)))
                    (begin
                      (and
                       (eq? tag-1 tag-2)
                       (= real-1 real-2)
                       (= imag-1 imag-2))
                      )))
                ))
             (else
              (begin
                (display
                 (format
                  #f "equ? error type ~a not found" t1))
                (force-output)
                (quit)
                )))
            ))
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a generic equality predicate "))
    (display
     (format #f "equ? that tests the~%"))
    (display
     (format #f "equality of two numbers, and install "))
    (display
     (format #f "it in the generic~%"))
    (display
     (format #f "arithmetic package. This operation "))
    (display
     (format #f "should work for~%"))
    (display
     (format #f "ordinary numbers, rational numbers, "))
    (display
     (format #f "and complex~%"))
    (display
     (format #f "numbers.~%"))
    (newline)
    (display
     (format #f "(define (install-scheme-number-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (equ? num-1 num-2)~%"))
    (display
     (format #f "    (= (contents num-1) (contents num-2)))~%"))
    (display
     (format #f "  (put 'equ? '(scheme-number "))
    (display
     (format #f "scheme-number) equ?)~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-rational-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (equ? num-1 num-2)~%"))
    (display
     (format #f "    (let ((cn-1 (contents num-1))~%"))
    (display
     (format #f "          (cn-2 (contents num-2)))~%"))
    (display
     (format #f "      (and (= (numer cn-1) (numer cn-2))~%"))
    (display
     (format #f "           (= (denom cn-1) (denom cn-2)))))~%"))
    (display
     (format #f "  (put 'equ? '(rational "))
    (display
     (format #f "rational) equ?)~%"))
    (display
     (format #f "  ...)~%"))
    (display
     (format #f "(define (install-complex-package)~%"))
    (display
     (format #f "  ...~%"))
    (display
     (format #f "  (define (equ? num-1 num-2)~%"))
    (display
     (format #f "    (let ((cn-1 (contents num-1))~%"))
    (display
     (format #f "          (cn-2 (contents num-2)))~%"))
    (display
     (format #f "      (and (= (real-part cn-1) "))
    (display
     (format #f "(real-part cn-2))~%"))
    (display
     (format #f "           (= (imag-part cn-1) "))
    (display
     (format #f "(imag-part cn-2)))))~%"))
    (display
     (format #f "  (put 'equ? '(complex complex) equ?)~%"))
    (display
     (format #f "  ...)~%"))
    (newline)
    (display
     (format #f "(define (equ? n1 n2)~%"))
    (display
     (format #f "  (apply-generic 'equ? n1 n2))~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.79 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
