#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.19                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (first-denomination alist)
  (begin
    (car alist)
    ))

;;;#############################################################
;;;#############################################################
(define (except-first-denomination alist)
  (begin
    (cdr alist)
    ))

;;;#############################################################
;;;#############################################################
(define (no-more? alist)
  (begin
    (null? alist)
    ))

;;;#############################################################
;;;#############################################################
(define (cc amount coin-values)
  (begin
    (cond
     ((= amount 0)
      (begin
        1
        ))
     ((or (< amount 0) (no-more? coin-values))
      (begin
        0
        ))
     (else
      (begin
        (+ (cc amount
               (except-first-denomination coin-values))
           (cc (- amount
                  (first-denomination coin-values))
               coin-values)))))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-cc-1 result-hash-table)
 (begin
   (let ((sub-name "test-cc-1")
         (test-list
          (list
           (list 5 (list 1) 1)
           (list 5 (list 5 1) 2)
           (list 10 (list 1) 1)
           (list 10 (list 5 1) 3)
           (list 10 (list 10 5 1) 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((value (list-ref this-list 0))
                  (coin-list (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (cc value coin-list)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : value=~a, "
                        sub-name test-label-index value))
                      (err-msg-2
                       (format
                        #f "coin list=~a, " coin-list))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define the procedures "))
    (display
     (format #f "first-denomination,~%"))
    (display
     (format #f "except-first-denomination, and "))
    (display
     (format #f "no-more? in terms of~%"))
    (display
     (format #f "primitive operations on list "))
    (display
     (format #f "structures. Does the order~%"))
    (display
     (format #f "of the list coin-values affect the "))
    (display
     (format #f "answer produced~%"))
    (display
     (format #f "by cc? Why or why not?~%"))
    (newline)
    (display
     (format #f "(define (first-denomination alist)~%"))
    (display
     (format #f "  (car alist))~%"))
    (newline)
    (display
     (format #f "(define (except-first-denomination alist)~%"))
    (display
     (format #f "  (cdr alist))~%"))
    (newline)
    (display
     (format #f "(define (no-more? alist)~%"))
    (display
     (format #f "  (null? alist))~%"))
    (newline)
    (display
     (format #f "The order of the coin value list "))
    (display
     (format #f "matters, since the largest~%"))
    (display
     (format #f "coins reduce the problem much quicker "))
    (display
     (format #f "than starting with~%"))
    (display
     (format #f "the smallest coins first.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((us-coins (list 50 25 10 5 1)))
      (begin
        (display
         (ice-9-format:format
          #f "(cc 100 us-coins) = ~:d~%"
          (cc 100 us-coins)))
        ))
    (newline)

    (let ((us-coins (list 50 25 10 5 1)))
      (begin
        (display
         (ice-9-format:format
          #f "(cc 100 (reverse us-coins)) = ~:d~%"
          (cc 100 (reverse us-coins))))
        ))
    (newline)

    (let ((uk-coins (list 100 50 20 10 5 2 1 0.5)))
      (begin
        (display
         (ice-9-format:format
          #f "(cc 100 uk-coins) = ~:d~%"
          (cc 100 uk-coins)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.19 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "scheme test~%"))

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (force-output)
             ))

          (newline)

          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
