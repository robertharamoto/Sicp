#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.83                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (raise-integer->rational ii-int)
  (begin
    (if (eq? (type-tag ii-int) 'scheme-number)
        (begin
          (let ((result (make-rat (contents ii-int) 1)))
            (begin
              result
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (raise-rational->real rr-rat)
  (begin
    (if (eq? (type-tag rr-rat) 'rational)
        (begin
          (let ((rr (contents rr-rat)))
            (let ((rr-numer (numer rr))
                  (rr-denom (denom rr)))
              (begin
                (exact->inexact
                 (/ rr-numer rr-denom))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (raise-real->complex rr-real)
  (begin
    (if (eq? (type-tag rr-real) 'real)
        (begin
          (let ((result (make-complex (contents rr-real) 0)))
            (begin
              result
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose you are designing a generic "))
    (display
     (format #f "arithmetic system for~%"))
    (display
     (format #f "dealing with the tower of types shown "))
    (display
     (format #f "in figure 2.25:~%"))
    (display
     (format #f "integer, rational, real, complex. For "))
    (display
     (format #f "each type (except~%"))
    (display
     (format #f "complex), design a procedure that raises "))
    (display
     (format #f "objects of that type~%"))
    (display
     (format #f "one level in the tower. Show how to "))
    (display
     (format #f "install a generic raise~%"))
    (display
     (format #f "operation that will work for each "))
    (display
     (format #f "type (except complex).~%"))
    (newline)
    (display
     (format #f "(define (raise-integer->rational "))
    (display
     (format #f "ii-int)~%"))
    (display
     (format #f "  (if (eq? (type-tag ii-int) "))
    (display
     (format #f "'scheme-number)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (let ((result (make-rat "))
    (display
     (format #f "(contents ii-int) 1)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            result~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        #f~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "(define (raise-rational->real "))
    (display
     (format #f "rr-rat)~%"))
    (display
     (format #f "  (if (eq? (type-tag rr-rat) "))
    (display
     (format #f "'rational)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (let ((rr (contents "))
    (display
     (format #f "rr-rat)))~%"))
    (display
     (format #f "          (let ((rr-numer "))
    (display
     (format #f "(numer rr))~%"))
    (display
     (format #f "                (rr-denom "))
    (display
     (format #f "(denom rr)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (exact->inexact~%"))
    (display
     (format #f "               (/ rr-numer "))
    (display
     (format #f "rr-denom))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        #f~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "(define (raise-real->complex "))
    (display
     (format #f "rr-real)~%"))
    (display
     (format #f "  (if (eq? (type-tag rr-real) "))
    (display
     (format #f "'real)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (let ((result (make-complex "))
    (display
     (format #f "(contents rr-real) 0)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            result~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        #f~%"))
    (display
     (format #f "        )))~%"))
    (newline)
    (display
     (format #f "(put-coercion 'raise 'scheme-number "))
    (display
     (format #f "raise-integer->rational)~%"))
    (display
     (format #f "(put-coercion 'raise 'rational "))
    (display
     (format #f "raise-rational->real)~%"))
    (display
     (format #f "(put-coercion 'raise 'real "))
    (display
     (format #f "raise-real->complex)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.83 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
