#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.9                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; ensure that interval is in the right order
(define (make-interval x y)
  (begin
    (if (< x y)
        (begin
          (cons x y))
        (begin
          (cons y x)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lower-bound xx)
  (begin
    (car xx)
    ))

;;;#############################################################
;;;#############################################################
(define (upper-bound xx)
  (begin
    (cdr xx)
    ))

;;;#############################################################
;;;#############################################################
(define (width xx)
  (begin
    (/ (- (upper-bound xx) (lower-bound xx)) 2.0)
    ))

;;;#############################################################
;;;#############################################################
(define (add-interval x y)
  (begin
    (make-interval
     (+ (lower-bound x) (lower-bound y))
     (+ (upper-bound x) (upper-bound y)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-interval-1")
         (test-list
          (list
           (list (make-interval 1 10) (make-interval 11 20)
                 (make-interval 12 30))
           (list (make-interval 10 30) (make-interval 20 20)
                 (make-interval 30 50))
           (list (make-interval -10 10) (make-interval -20 100)
                 (make-interval -30 110))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (add-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-interval x y)
  (begin
    (make-interval
     (- (lower-bound x) (lower-bound y))
     (- (upper-bound x) (upper-bound y)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sub-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-sub-interval-1")
         (test-list
          (list
           (list (make-interval 1 10) (make-interval 11 20)
                 (make-interval -10 -10))
           (list (make-interval 10 30) (make-interval 20 25)
                 (make-interval -10 5))
           (list (make-interval -10 10) (make-interval -20 100)
                 (make-interval 10 -90))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (sub-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) :  xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (mul-interval x y)
  (begin
    (let ((p1 (* (lower-bound x) (lower-bound y)))
          (p2 (* (lower-bound x) (upper-bound y)))
          (p3 (* (upper-bound x) (lower-bound y)))
          (p4 (* (upper-bound x) (upper-bound y))))
      (begin
        (make-interval (min p1 p2 p3 p4)
                       (max p1 p2 p3 p4))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mul-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-mul-interval-1")
         (test-list
          (list
           (list (make-interval 1 3) (make-interval 2 4)
                 (make-interval 2 12))
           (list (make-interval 10 30) (make-interval 20 20)
                 (make-interval 200 600))
           (list (make-interval -10 10) (make-interval 2 -3)
                 (make-interval -30 30))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (mul-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) :  xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (div-interval x y)
  (begin
    (mul-interval
     x
     (make-interval (/ 1.0 (upper-bound y))
                    (/ 1.0 (lower-bound y))))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-div-interval-1 result-hash-table)
 (begin
   (let ((sub-name "test-div-interval-1")
         (test-list
          (list
           (list (make-interval 1 3) (make-interval 2 4)
                 (make-interval 0.25 1.5))
           (list (make-interval 10 30) (make-interval 20 20)
                 (make-interval 0.5 1.5))
           (list (make-interval -10 10) (make-interval 2 -3)
                 (make-interval -5.0 5.0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx (list-ref this-list 0))
                  (yy (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (div-interval xx yy)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) :  xx=~a, yy=~a, "
                        sub-name test-label-index xx yy))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (interval-to-string x)
  (begin
    (format #f "(~a, ~a)" (lower-bound x) (upper-bound x))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The width of an interval is half of "))
    (display
     (format #f "the difference between~%"))
    (display
     (format #f "its upper and lower bounds. The width "))
    (display
     (format #f "is a measure of~%"))
    (display
     (format #f "the uncertainty of the number specified "))
    (display
     (format #f "by the interval.~%"))
    (display
     (format #f "For some arithmetic operations the "))
    (display
     (format #f "width of the result~%"))
    (display
     (format #f "of combining two intervals is a function "))
    (display
     (format #f "two intervals is~%"))
    (display
     (format #f "a function only of the widths of the "))
    (display
     (format #f "argument intervals,~%"))
    (display
     (format #f "whereas for others the width of the "))
    (display
     (format #f "combination is not a~%"))
    (display
     (format #f "function of the widths of the argument "))
    (display
     (format #f "intervals. Show that~%"))
    (display
     (format #f "the width of the sum (or difference) of "))
    (display
     (format #f "two intervals is a~%"))
    (display
     (format #f "function only of the widths of the "))
    (display
     (format #f "intervals being added~%"))
    (display
     (format #f "(or subtracted). Give examples to "))
    (display
     (format #f "show that this is~%"))
    (display
     (format #f "not true for multiplication "))
    (display
     (format #f "or division.~%"))
    (newline)

    (display
     (format #f "width~%"))
    (display
     (format #f "(define (width x)~%"))
    (display
     (format #f "  (/ (- (upper-bound x)~%"))
    (display
     (format #f "        (lower-bound x))~%"))
    (display
     (format #f "      2))~%"))
    (newline)
    (display
     (format #f "add~%"))
    (display
     (format #f "(define (add-interval x y)~%"))
    (display
     (format #f "  (make-interval (+ (lower-bound x) "))
    (display
     (format #f "(lower-bound y))~%"))
    (display
     (format #f "                 (+ (upper-bound x) "))
    (display
     (format #f "(upper-bound y))))~%"))
    (newline)
    (display
     (format #f "width x = (/ (- (upper-bound x) "))
    (display
     (format #f "(lower-bound x)) 2)~%"))
    (display
     (format #f "width y = (/ (+ (upper-bound y) "))
    (display
     (format #f "(lower-bound y)) 2)~%"))
    (display
     (format #f "width x+y = (/ (- (+ (upper-bound x) "))
    (display
     (format #f "(upper-bound y))~%"))
    (display
     (format #f "                  (+ (lower-bound x) "))
    (display
     (format #f "(lower-bound y)))~%"))
    (display
     (format #f "                2)~%"))
    (display
     (format #f "          = (+ (/ (- (upper-bound x) "))
    (display
     (format #f "(lower-bound x)) 2)~%"))
    (display
     (format #f "               (/ (- (upper-bound y) "))
    (display
     (format #f "(lower-bound y)) 2))~%"))
    (display
     (format #f "          = (+ (width x) (width y))~%"))
    (newline)
    (display
     (format #f "(define (sub-interval x y)~%"))
    (display
     (format #f "  (make-interval (- (lower-bound x) "))
    (display
     (format #f "(lower-bound y))~%"))
    (display
     (format #f "                 (- (upper-bound x) "))
    (display
     (format #f "(upper-bound y))))~%"))
    (newline)
    (display
     (format #f "width x-y = (/ (- (- (upper-bound x) "))
    (display
     (format #f "(upper-bound y))~%"))
    (display
     (format #f "                  (- (lower-bound x) "))
    (display
     (format #f "(lower-bound y)))~%"))
    (display
     (format #f "                2)~%"))
    (display
     (format #f "          = (- (/ (- (upper-bound x) "))
    (display
     (format #f "(lower-bound x)) 2)~%"))
    (display
     (format #f "               (/ (- (upper-bound y) "))
    (display
     (format #f "(lower-bound y)) 2))~%"))
    (display
     (format #f "          = (- (width x) (width y))~%"))
    (newline)
    (display
     (format #f "multiplication~%"))
    (display
     (format #f "(width (make-interval 1 2)) = ~a~%"
             (width (make-interval 1 2))))
    (display
     (format #f "(width (make-interval 3 4)) = ~a~%"
             (width (make-interval 3 4))))
    (display
     (format #f "(mul-interval (make-interval 1 2) "))
    (display
     (format #f "(make-interval 3 4)) = ~a~%"
             (mul-interval (make-interval 1 2)
                           (make-interval 3 4))))
    (newline)
    (display
     (format #f "(width (mul-interval (make-interval 1 2) "))
    (display
     (format #f "(make-interval 3 4))) = ~a~%"
             (width (mul-interval
                     (make-interval 1 2)
                     (make-interval 3 4)))))
    (display
     (format #f "(+ (width (make-interval 1 2)) "))
    (display
     (format #f "(width (make-interval 3 4))) = ~a~%"
             (+ (width (make-interval 1 2))
                (width (make-interval 3 4)))))
    (newline)
    (display
     (format #f "division~%"))
    (display
     (format #f "(width (make-interval 1 2)) = ~a~%"
             (width (make-interval 1 2))))
    (display
     (format #f "(width (make-interval 3 4)) = ~a~%"
             (width (make-interval 3 4))))
    (display
     (format #f "(div-interval (make-interval 1 2) "))
    (display
     (format #f "(make-interval 3 4)) = ~a~%"
             (div-interval (make-interval 1 2)
                           (make-interval 3 4))))
    (newline)
    (display
     (format #f "(width (div-interval (make-interval 1 2) "))
    (display
     (format #f "(make-interval 3 4))) = ~a~%"
             (width (div-interval
                     (make-interval 1 2)
                     (make-interval 3 4)))))
    (display
     (format #f "(/ (width (make-interval 1 2)) "))
    (display
     (format #f "(width (make-interval 3 4))) = ~a~%"
             (/ (width (make-interval 1 2))
                (width (make-interval 3 4)))))
    (newline)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.09 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
