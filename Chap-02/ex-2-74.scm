#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.74                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Insatiable Enterprises, Inc., is a "))
    (display
     (format #f "highly decentralized~%"))
    (display
     (format #f "conglomerate company consisting of a "))
    (display
     (format #f "large number of~%"))
    (display
     (format #f "independent divisions located all over "))
    (display
     (format #f "the world. The~%"))
    (display
     (format #f "company's computer facilities have just "))
    (display
     (format #f "been interconnected~%"))
    (display
     (format #f "by means of a clever network-interfacing "))
    (display
     (format #f "scheme that makes~%"))
    (display
     (format #f "the entire network appear to any user "))
    (display
     (format #f "to be a single computer.~%"))
    (display
     (format #f "Insatiable's president, in her first "))
    (display
     (format #f "attempt to exploit the~%"))
    (display
     (format #f "ability of the network to extract "))
    (display
     (format #f "administrative information~%"))
    (display
     (format #f "from division files, is dismayed to "))
    (display
     (format #f "discover that, although~%"))
    (display
     (format #f "all the division files have been "))
    (display
     (format #f "implemented as data~%"))
    (display
     (format #f "structures in Scheme, the particular "))
    (display
     (format #f "data structure used~%"))
    (display
     (format #f "varies from division to division. A "))
    (display
     (format #f "meeting of division~%"))
    (display
     (format #f "managers is hastily called to search "))
    (display
     (format #f "for a strategy to~%"))
    (display
     (format #f "integrate the files that will satisfy "))
    (display
     (format #f "headquarters' needs while~%"))
    (display
     (format #f "preserving the existing autonomy of "))
    (display
     (format #f "the divisions.~%"))
    (display
     (format #f "Show how such a strategy can be "))
    (display
     (format #f "implemented with data-directed~%"))
    (display
     (format #f "programming. As an example, suppose that "))
    (display
     (format #f "each division's~%"))
    (display
     (format #f "personnel records consist of a single "))
    (display
     (format #f "file, which contains~%"))
    (display
     (format #f "a set of records keyed on employees' "))
    (display
     (format #f "names. The structure~%"))
    (display
     (format #f "of the set varies from division to "))
    (display
     (format #f "division. Furthermore, each~%"))
    (display
     (format #f "employee's record is itself a set "))
    (display
     (format #f "(structured differently~%"))
    (display
     (format #f "from division to division), that "))
    (display
     (format #f "contains information keyed~%"))
    (display
     (format #f "under identifiers such as address and "))
    (display
     (format #f "salary. In particular:~%"))
    (display
     (format #f "a.  Implement for headquarters a "))
    (display
     (format #f "get-record procedure that~%"))
    (display
     (format #f "retrieves a specified employee's record "))
    (display
     (format #f "from a specified personnel~%"))
    (display
     (format #f "file. The procedure should be applicable "))
    (display
     (format #f "to any division's file.~%"))
    (display
     (format #f "Explain how the individual divisions' "))
    (display
     (format #f "files should be structured.~%"))
    (display
     (format #f "In particular, what type information "))
    (display
     (format #f "must be supplied?~%"))
    (display
     (format #f "b.  Implement for headquarters a "))
    (display
     (format #f "get-salary procedure that~%"))
    (display
     (format #f "returns the salary information from "))
    (display
     (format #f "a given employee's record~%"))
    (display
     (format #f "from any division's personnel file. "))
    (display
     (format #f "How should the record be~%"))
    (display
     (format #f "structured in order to make "))
    (display
     (format #f "this operation work?~%"))
    (display
     (format #f "c.  Implement for headquarters a "))
    (display
     (format #f "find-employee-record procedure.~%"))
    (display
     (format #f "This should search all the divisions' "))
    (display
     (format #f "files for the record of a~%"))
    (display
     (format #f "given employee and return the record. "))
    (display
     (format #f "Assume that this procedure~%"))
    (display
     (format #f "takes as arguments an employee's name "))
    (display
     (format #f "and a list of all the~%"))
    (display
     (format #f "divisions' files.~%"))
    (display
     (format #f "d.  When Insatiable takes over a new "))
    (display
     (format #f "company, what changes must~%"))
    (display
     (format #f "be made in order to incorporate the new "))
    (display
     (format #f "personnel information into~%"))
    (display
     (format #f "the central system?~%"))
    (newline)
    (display
     (format #f "(a) Each division should provide a "))
    (display
     (format #f "data-directed solution~%"))
    (display
     (format #f "using the following keywords and "))
    (display
     (format #f "division names:~%"))
    (display
     (format #f "+------------------+-----------+----------+-----+---------+~%"))
    (display
     (format #f "|  function name   |   div 1   |   div 2  | ... |  div n  |~%"))
    (display
     (format #f "+------------------+-----------+----------+-----+---------+~%"))
    (display
     (format #f "|  get-record      |           |          | ... |         |~%"))
    (display
     (format #f "|  get-name        |           |          | ... |         |~%"))
    (display
     (format #f "|  get-address     |           |          | ... |         |~%"))
    (display
     (format #f "|  get-salary      |           |          | ... |         |~%"))
    (display
     (format #f "+------------------+-----------+----------+-----+---------+~%"))
    (newline)
    (display
     (format #f "The records should be in a scheme "))
    (display
     (format #f "list, with accessors like~%"))
    (display
     (format #f "get-record, get-name, get-address, "))
    (display
     (format #f "get-salary, ...~%"))
    (display
     (format #f "The individual files should be wrapped "))
    (display
     (format #f "by an installation~%"))
    (display
     (format #f "package, with type-name = division "))
    (display
     (format #f "name, for example, div-1:~%"))
    (display
     (format #f "(define (install-div-1-package)~%"))
    (display
     (format #f "  (define records-list ( ... data ... ))~%"))
    (display
     (format #f "  (define (attach-tag type-tag contents)~%"))
    (display
     (format #f "    (cons type-tag contents))~%"))
    (display
     (format #f "  (define (type-tag datum)~%"))
    (display
     (format #f "    (if (pair? datum)~%"))
    (display
     (format #f "        (car datum)~%"))
    (display
     (format #f "        (error \"Bad tagged datum -- "))
    (display
     (format #f "TYPE-TAG\" datum)))~%"))
    (display
     (format #f "  (define (contents datum)~%"))
    (display
     (format #f "    (if (pair? datum)~%"))
    (display
     (format #f "        (cdr datum)~%"))
    (display
     (format #f "        (error \"Bad tagged datum -- "))
    (display
     (format #f "CONTENTS\" datum)))~%"))
    (display
     (format #f "  ( ... )~%"))
    (display
     (format #f "  (define (get-record name-to-find)~%"))
    (display
     (format #f "    (let ((rlen (length records-list))~%"))
    (display
     (format #f "          (result-record #f)~%"))
    (display
     (format #f "          (found-flag #f))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "            ((or (>= ii rlen)~%"))
    (display
     (format #f "                 (equal? found-flag #t)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((this-rec (list-ref "))
    (display
     (format #f "records-list ii)))~%"))
    (display
     (format #f "              (let ((tname "))
    (display
     (format #f "(get-name record)))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (if (equal? "))
    (display
     (format #f "tname name-to-find)~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (set! "))
    (display
     (format #f "found-flag #t)~%"))
    (display
     (format #f "                        (set! "))
    (display
     (format #f "result-record this-rec)~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                   )))~%"))
    (display
     (format #f "             ))~%"))
    (display
     (format #f "         result-record~%"))
    (display
     (format #f "         )))~%"))
    (display
     (format #f "  (define (get-name record)~%"))
    (display
     (format #f "    (list-ref record 0))~%"))
    (display
     (format #f "  (define (get-address record)~%"))
    (display
     (format #f "    (list-ref record 1))~%"))
    (display
     (format #f "  (define (get-salary record)~%"))
    (display
     (format #f "    (list-ref record 2))~%"))
    (display
     (format #f "  ( ... )~%"))
    (display
     (format #f "  (put 'get-record 'div-1 get-record)~%"))
    (display
     (format #f "  (put 'get-name 'div-1 get-name)~%"))
    (display
     (format #f "  (put 'address 'div-1 get-address)~%"))
    (display
     (format #f "  (put 'salary 'div-1 get-salary)~%"))
    (display
     (format #f "  'done)~%"))
    (newline)
    (display
     (format #f "  (define (get-record name division)~%"))
    (display
     (format #f "    ((get 'get-record division) name))~%"))
    (newline)
    (display
     (format #f "(b)~%"))
    (display
     (format #f "  (define (get-salary division record)~%"))
    (display
     (format #f "    ((get 'get-salary division) record))~%"))
    (display
     (format #f "The record can be constructed in "))
    (display
     (format #f "any way, as long as the~%"))
    (display
     (format #f "get-salary function is put into the "))
    (display
     (format #f "data-directed matrix.~%"))
    (display
     (format #f "The installation package will ensure "))
    (display
     (format #f "that the function names~%"))
    (display
     (format #f "are private.~%"))
    (newline)
    (display
     (format #f "(c)~%"))
    (display
     (format #f "(define (find-employee-record "))
    (display
     (format #f "name division-list)~%"))
    (display
     (format #f "  (let ((dlen (length division-list))~%"))
    (display
     (format #f "        (result-record #f)~%"))
    (display
     (format #f "        (found-flag #f))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "          ((or (>= ii dlen)~%"))
    (display
     (format #f "               (equal? found-flag #t)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((this-division "))
    (display
     (format #f "(list-ref divison-list ii)))~%"))
    (display
     (format #f "            (let ((this-record "))
    (display
     (format #f "((get 'get-record this-division) name)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (not "))
    (display
     (format #f "(equal? this-record #f))~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (set! "))
    (display
     (format #f "found-flag #t)~%"))
    (display
     (format #f "                      (set! "))
    (display
     (format #f "result-record this-record)~%"))
    (display
     (format #f "                     ))~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "           ))~%"))
    (display
     (format #f "      result-record~%"))
    (display
     (format #f "      )))~%"))
    (newline)
    (display
     (format #f "(d)~%"))
    (display
     (format #f "When a new company is taken over, "))
    (display
     (format #f "then that company must send~%"))
    (display
     (format #f "over a similar formatted installation "))
    (display
     (format #f "packages, and the division~%"))
    (display
     (format #f "list must be updated.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.74 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
