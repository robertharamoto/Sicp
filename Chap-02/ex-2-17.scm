#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.17                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-last-pair alist)
  (define (local-iter this-list)
    (begin
      (if (null? (cdr this-list))
          (begin
            this-list)
          (begin
            (local-iter (cdr this-list))
            ))
      ))
  (begin
    (if (or (null? alist)
            (null? (car alist)))
        (begin
          (list))
        (begin
          (local-iter alist)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a procedure last-pair that "))
    (display
     (format #f "returns the list~%"))
    (display
     (format #f "that contains only the last element "))
    (display
     (format #f "of a given~%"))
    (newline)
    (display
     (format #f "(nonempty) list:~%"))
    (display
     (format #f "(last-pair (list 23 72 149 34))~%"))
    (display
     (format #f "(34)~%"))
    (newline)
    (display
     (format #f "(define (my-last-pair alist)~%"))
    (display
     (format #f "  (define (local-iter this-list)~%"))
    (display
     (format #f "    (if (null? (cdr this-list))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          this-list)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (local-iter (cdr this-list))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (if (or (null? alist)~%"))
    (display
     (format #f "            (null? (car alist)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (list))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (local-iter alist)~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "scheme test~%"))
    (display
     (format #f "(last-pair (list)) = ~a~%"
             (my-last-pair (list))))
    (display
     (format #f "(last-pair (list 23)) = ~a~%"
             (my-last-pair (list 23))))
    (display
     (format #f "(last-pair (list 23 72 149 34)) = ~a~%"
             (my-last-pair (list 23 72 149 34))))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.17 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
