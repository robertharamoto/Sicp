#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.21                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (square-list-rec items)
  (begin
    (if (null? items)
        (begin
          (list))
        (begin
          (cons
           ((lambda (xx) (* xx xx)) (car items))
           (square-list-rec (cdr items)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-square-list-rec-1 result-hash-table)
 (begin
   (let ((sub-name "test-square-list-rec-1")
         (test-list
          (list
           (list (list 1 2) (list 1 4))
           (list (list 1 2 3) (list 1 4 9))
           (list (list 2 3 4 5 ) (list 4 9 16 25))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (square-list-rec alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (square-list-map items)
  (begin
    (map
     (lambda (xx) (* xx xx))
     items)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-square-list-map-1 result-hash-table)
 (begin
   (let ((sub-name "test-square-list-map-1")
         (test-list
          (list
           (list (list 1 2 3) (list 1 4 9))
           (list (list 1 2 3 4) (list 1 4 9 16))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((alist (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (square-list-map alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : alist=~a, "
                        sub-name test-label-index
                        alist))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The procedure square-list takes a "))
    (display
     (format #f "list of numbers~%"))
    (display
     (format #f "as argument and returns a list of "))
    (display
     (format #f "the squares of~%"))
    (display
     (format #f "those numbers. (square-list "))
    (display
     (format #f "(list 1 2 3 4))~%"))
    (display
     (format #f " -> (1 4 9 16)~%"))
    (display
     (format #f "Here are two different definitions of "))
    (display
     (format #f "square-list. Complete~%"))
    (display
     (format #f "both of them by filling in the "))
    (display
     (format #f "missing expressions:~%"))
    (newline)
    (display
     (format #f "(define (square-list items)~%"))
    (display
     (format #f "  (if (null? items)~%"))
    (display
     (format #f "      nil~%"))
    (display
     (format #f "      (cons <??> <??>)))~%"))
    (display
     (format #f "(define (square-list items)~%"))
    (display
     (format #f "  (map <??> <??>))~%"))
    (newline)
    (display
     (format #f "(define (square-list-rec items)~%"))
    (display
     (format #f "  (if (null? items)~%"))
    (display
     (format #f "      (list)~%"))
    (display
     (format #f "      (cons~%"))
    (display
     (format #f "        ((lambda (xx) (* xx xx)) (car items))~%"))
    (display
     (format #f "        (square-list-rec (cdr items)))~%"))
    (display
     (format #f "     ))~%"))
    (display
     (format #f "(define (square-list-map items)~%"))
    (display
     (format #f "  (map (lambda (xx) (* xx xx)) items))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format
      #f "(square-list-rec (list 1 2 3 4)) = ~a~%"
      (square-list-rec (list 1 2 3 4))))
    (display
     (format
      #f "(square-list-map (list 1 2 3 4)) = ~a~%"
      (square-list-map (list 1 2 3 4))))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.21 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "scheme test~%"))

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             (force-output)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
