#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.63                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-1
             (format
              #f ", shouldbe=~a, result=~a"
              shouldbe-list result-list))
            (err-msg-2
             (format
              #f ", length shouldbe=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-1 err-msg-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((sflag (member s-elem result-list))
                      (err-msg-3
                       (format
                        #f ", missing element ~a"
                        s-elem)))
                  (begin
                    (unittest2:assert?
                     (not (equal? sflag #f))
                     sub-name
                     (string-append err-start err-msg-1 err-msg-3)
                     result-hash-table)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (entry tree)
  (begin
    (car tree)
    ))

;;;#############################################################
;;;#############################################################
(define (left-branch tree)
  (begin
    (cadr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (right-branch tree)
  (begin
    (caddr tree)
    ))

;;;#############################################################
;;;#############################################################
(define (make-tree entry left right)
  (begin
    (list entry left right)
    ))

;;;#############################################################
;;;#############################################################
(define (tree->list-1 tree)
  (begin
    (if (null? tree)
        (begin
          (list))
        (begin
          (append (tree->list-1 (left-branch tree))
                  (cons (entry tree)
                        (tree->list-1 (right-branch tree))))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tree->list-1-1 result-hash-table)
 (begin
   (let ((sub-name "test-tree->list-1-1")
         (test-list
          (list
           (list
            (list 7 (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
                  (list 9 (list) (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           (list
            (list 3 (list 1 (list) (list))
                  (list 7 (list 5 (list) (list))
                        (list 9 (list) (list 11 (list) (list)))))
            (list 1 3 5 7 9 11))
           (list
            (list 5 (list 1 (list 3 (list) (list)) (list))
                  (list 9 (list 7 (list) (list))
                        (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((tree-1 (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (tree->list-1 tree-1)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : tree-1=~a, "
                        sub-name test-label-index
                        tree-1)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (begin
      (if (null? tree)
          (begin
            result-list)
          (begin
            (copy-to-list (left-branch tree)
                          (cons (entry tree)
                                (copy-to-list (right-branch tree)
                                              result-list)))
            ))
      ))
  (begin
    (copy-to-list tree (list))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tree->list-2-1 result-hash-table)
 (begin
   (let ((sub-name "test-tree->list-2-1")
         (test-list
          (list
           (list
            (list 7 (list 3 (list 1 (list) (list)) (list 5 (list) (list)))
                  (list 9 (list) (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           (list
            (list 3 (list 1 (list) (list))
                  (list 7 (list 5 (list) (list))
                        (list 9 (list) (list 11 (list) (list)))))
            (list 1 3 5 7 9 11))
           (list
            (list 5 (list 1 (list 3 (list) (list)) (list))
                  (list 9 (list 7 (list) (list))
                        (list 11 (list) (list))))
            (list 1 3 5 7 9 11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((tree-2 (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (tree->list-2 tree-2)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : tree-2=~a, "
                        sub-name test-label-index
                        tree-2)))
                  (begin
                    (assert-lists-are-equal
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Each of the following two procedures "))
    (display
     (format #f "converts a binary~%"))
    (display
     (format #f "tree to a list.~%"))
    (display
     (format #f "(define (tree->list-1 tree)~%"))
    (display
     (format #f "  (if (null? tree)~%"))
    (display
     (format #f "      (list)~%"))
    (display
     (format #f "      (append (tree->list-1 "))
    (display
     (format #f "(left-branch tree))~%"))
    (display
     (format #f "              (cons (entry tree)~%"))
    (display
     (format #f "                    (tree->list-1 "))
    (display
     (format #f "(right-branch tree))))))~%"))
    (display
     (format #f "(define (tree->list-2 tree)~%"))
    (display
     (format #f "  (define (copy-to-list "))
    (display
     (format #f "tree result-list)~%"))
    (display
     (format #f "    (if (null? tree)~%"))
    (display
     (format #f "        result-list~%"))
    (display
     (format #f "        (copy-to-list "))
    (display
     (format #f "(left-branch tree)~%"))
    (display
     (format #f "                      "))
    (display
     (format #f "(cons (entry tree)~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "(copy-to-list (right-branch tree)~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "        result-list)))))~%"))
    (display
     (format #f "  (copy-to-list tree (list)))~%"))
    (newline)
    (display
     (format #f "a. Do the two procedures produce the "))
    (display
     (format #f "same result for~%"))
    (display
     (format #f "every tree? If not, how do the results "))
    (display
     (format #f "differ? What lists~%"))
    (display
     (format #f "do the two procedures produce for the "))
    (display
     (format #f "trees in figure 2.16?~%"))
    (display
     (format #f "b. Do the two procedures have the same "))
    (display
     (format #f "order of growth in~%"))
    (display
     (format #f "the number of steps required to convert "))
    (display
     (format #f "a balanced tree~%"))
    (display
     (format #f "with n elements to a list? If not, which "))
    (display
     (format #f "one grows more~%"))
    (display
     (format #f "slowly?~%"))
    (newline)
    (display
     (format #f "(a) consider the tree = "))
    (display
     (format #f "(list 7 (list 1 (list) (list))~%"))
    (display
     (format #f "    (list 13 (list) (list)))~%"))
    (display
     (format #f "(tree->list-1 tree)~%"))
    (display
     (format #f "-> (append (tree->list-1 "))
    (display
     (format #f "(list 1 (list) (list)))~%"))
    (display
     (format #f "           (cons 7 (tree->list-1 "))
    (display
     (format #f "(list 13 (list) (list)))))~%"))
    (display
     (format #f "-> (append (append "))
    (display
     (format #f "(list) (cons 1 (list)))~%"))
    (display
     (format #f "           (cons 7 (append "))
    (display
     (format #f "(list) (cons 13 (list)))))~%"))
    (display
     (format #f "-> (append (list 1) (list 7 13))~%"))
    (display
     (format #f "-> (list 1 7 13)~%"))
    (newline)
    (display
     (format #f "(tree->list-2 tree)~%"))
    (display
     (format #f "-> (copy-to-list tree (list))~%"))
    (display
     (format #f "-> (copy-to-list "))
    (display
     (format #f "(list 1 (list) (list))~%"))
    (display
     (format #f "                 (cons 7 "))
    (display
     (format #f "(copy-to-list (list 13~%"))
    (display
     (format #f "      (list) (list)) (list))))~%"))
    (display
     (format #f "-> (cons 1 (cons 7 "))
    (display
     (format #f "(cons 13 (list))))~%"))
    (display
     (format #f "-> (list 1 7 13)~%"))
    (newline)
    (display
     (format #f "tree->list-1 uses n-appends and "))
    (display
     (format #f "n-cons's when a~%"))
    (display
     (format #f "tree has n-elements. The tree->list-2 "))
    (display
     (format #f "uses n-cons's for~%"))
    (display
     (format #f "the same tree. So the iterative version "))
    (display
     (format #f "(tree->list-2) uses~%"))
    (display
     (format #f "fewer operations than the recursive "))
    (display
     (format #f "version (tree->list-1).~%"))
    (newline)
    (display
     (format #f "For small trees they will produce "))
    (display
     (format #f "the same results,~%"))
    (display
     (format #f "but the recursive version may overflow "))
    (display
     (format #f "the stack when~%"))
    (display
     (format #f "the trees are large.~%"))
    (newline)
    (display
     (format #f "(b) both procedures have the same "))
    (display
     (format #f "order of growth~%"))
    (display
     (format #f "= theta(n), but the iterative "))
    (display
     (format #f "version (tree->list-2)~%"))
    (display
     (format #f "grows slower (by a factor of 2).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.63 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
