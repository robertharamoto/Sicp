#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.42                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code macro and current-date-time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for run-all-tests functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (accumulate op initial sequence)
  (begin
    (if (null? sequence)
        (begin
          initial)
        (begin
          (op (car sequence)
              (accumulate op initial (cdr sequence)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-accumulate-1 result-hash-table)
 (begin
   (let ((sub-name "test-accumulate-1")
         (test-list
          (list
           (list + 0 (list 1 2 3) 6)
           (list + 0 (list 1 2 3 4) 10)
           (list * 1 (list 1 2 3) 6)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((oper (list-ref this-list 0))
                  (initial (list-ref this-list 1))
                  (alist (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (accumulate oper initial alist)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : oper=~a, "
                        sub-name test-label-index oper))
                      (err-msg-2
                       (format
                        #f "initial=~a, alist=~a, "
                        initial alist))
                      (err-msg-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (flatmap proc seq)
  (begin
    (accumulate append (list) (map proc seq))
    ))

;;;#############################################################
;;;#############################################################
(define (enumerate-interval low high)
  (define (local-iter current acc-list)
    (begin
      (if (> current high)
          (begin
            acc-list)
          (begin
            (local-iter (1+ current) (append acc-list (list current)))
            ))
      ))
  (begin
    (local-iter low (list))
    ))

;;;#############################################################
;;;#############################################################
(define empty-board (list))

;;;#############################################################
;;;#############################################################
(define (adjoin-position new-row k rest-of-queens)
  (begin
    (let ((new-queen (cons new-row k)))
      (let ((new-queens-list
             (cons new-queen rest-of-queens)))
        (begin
          new-queens-list
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; assumes the first k-1 queens positions are ok
(define (safe? k queens-positions)
  (begin
    (let ((result-flag #t))
      (let ((current-pos
             (list-ref queens-positions 0)))
        (let ((queen-row (car current-pos))
              (queen-col (cdr current-pos)))
          (begin
            (do ((ii 1 (1+ ii)))
                ((or (>= ii k)
                     (equal? result-flag #f)))
              (begin
                (let ((qpos (list-ref queens-positions ii)))
                  (let ((qrow (car qpos))
                        (qcol (cdr qpos)))
                    (let ((qrdiff (abs (- queen-row qrow)))
                          (qcdiff (abs (- queen-col qcol))))
                      (begin
                        ;;; first check if the rows or columns are equal
                        (if (or (equal? queen-row qrow)
                                (equal? queen-col qcol))
                            (begin
                              (set! result-flag #f)
                              ))
                        ;;; check if the queens are on the same diagonal
                        (if (equal? qrdiff qcdiff)
                            (begin
                              (set! result-flag #f)
                              ))
                        ))
                    ))
                ))

            result-flag
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (queens board-size)
  (define (local-queen-cols k)
    (begin
      (if (= k 0)
          (begin
            (list empty-board))
          (begin
            (filter
             (lambda (positions)
               (begin
                 (safe? k positions)
                 ))
             (flatmap
              (lambda (rest-of-queens)
                (begin
                  (map
                   (lambda (new-row)
                     (begin
                       (adjoin-position new-row k rest-of-queens)
                       ))
                   (enumerate-interval 1 board-size))
                  ))
              (local-queen-cols (- k 1))))
            ))
      ))
  (begin
    (local-queen-cols board-size)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-queens-1 result-hash-table)
 (begin
   (let ((sub-name "test-queens-1")
         (test-list
          (list
           (list 3 (list))
           (list 4
                 (list (list
                        (cons 3 4) (cons 1 3)
                        (cons 4 2) (cons 2 1))
                       (list
                        (cons 2 4) (cons 4 3)
                        (cons 1 2) (cons 3 1))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((board-size (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list (queens board-size)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : board=~a, "
                        sub-name test-label-index
                        board-size))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list-list result-list-list)))
                  (begin
                    (for-each
                     (lambda (slist)
                       (begin
                         (let ((sflag
                                (member slist result-list-list))
                               (err-msg-3
                                (format #f ", searching for ~a"
                                        slist)))
                           (begin
                             (unittest2:assert?
                              (not (equal? sflag #f))
                              sub-name
                              (string-append
                               err-msg-1 err-msg-2 err-msg-3)
                              result-hash-table)
                             ))
                         )) shouldbe-list-list)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (print-queens-board board-size a-list-list)
  (define (print-row-border)
    (begin
      (display (format #f " +"))
      (do ((icol 0 (1+ icol)))
          ((>= icol board-size))
        (begin
          (display (format #f "---+"))
          ))
      (newline)
      (force-output)
      ))
  (define (has-queen? irow icol qlist-list)
    (begin
      (let ((result-flag #f))
        (begin
          (for-each
           (lambda (qpos)
             (begin
               (let ((qrow (car qpos))
                     (qcol (cdr qpos)))
                 (begin
                   (if (and (equal? irow qrow)
                            (equal? icol qcol))
                       (begin
                         (set! result-flag #t)
                         ))
                   ))
               )) qlist-list)
          result-flag
          ))
      ))
  (begin
    (display (format #f "~a x ~a board~%" board-size board-size))
    (print-row-border)
    (do ((irow 1 (1+ irow)))
        ((> irow board-size))
      (begin
        (display (format #f " |"))
        (do ((icol 1 (1+ icol)))
            ((> icol board-size))
          (begin
            (if (has-queen? irow icol a-list-list)
                (begin
                  (display (format #f " Q |")))
                (begin
                  (display (format #f "   |"))
                  ))
            ))
        (newline)
        (print-row-border)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Complete the program by implementing "))
    (display
     (format #f "the representation for~%"))
    (display
     (format #f "sets of board positions, including "))
    (display
     (format #f "the procedure~%"))
    (display
     (format #f "adjoin-position, which adjoins a new "))
    (display
     (format #f "row-column position~%"))
    (display
     (format #f "to a set of positions, and empty-board, "))
    (display
     (format #f "which represents an~%"))
    (display
     (format #f "empty set of positions. You must also "))
    (display
     (format #f "write the procedure~%"))
    (display
     (format #f "safe?, which determines for a set of "))
    (display
     (format #f "positions, whether the~%"))
    (display
     (format #f "queen in the kth column is safe with "))
    (display
     (format #f "respect to the~%"))
    (display
     (format #f "others. (Note that we need only check "))
    (display
     (format #f "whether the new~%"))
    (display
     (format #f "queen is safe -- the other queens are "))
    (display
     (format #f "already guaranteed~%"))
    (display
     (format #f "safe with respect to each other.)~%"))
    (newline)
    (display
     (format #f "(define empty-board (list))~%"))
    (newline)
    (display
     (format #f "(define (adjoin-position "))
    (display
     (format #f "new-row k rest-of-queens)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (let ((new-queen "))
    (display
     (format #f "(cons new-row k)))~%"))
    (display
     (format #f "      (let ((new-queens-list~%"))
    (display
     (format #f "             (cons "))
    (display
     (format #f "new-queen rest-of-queens)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          new-queens-list~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num-list (list 4 5 6 8)))
      (begin
        (for-each
         (lambda (board-size)
           (begin
             (let ((llist (queens board-size)))
               (begin
                 (if (> (length llist) 0)
                     (begin
                       (print-queens-board
                        board-size (list-ref llist 0))
                       (newline)
                       (force-output)
                       ))

                 (display
                  (ice-9-format:format
                   #f "~:d x ~:d : found ~:d "
                   board-size board-size
                   (length llist)))
                 (display
                  (ice-9-format:format
                   #f "~a-queens configurations~%"
                   board-size))
                 (newline)
                 (force-output)
                 ))
             )) num-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.42 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
