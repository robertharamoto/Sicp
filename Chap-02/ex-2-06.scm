#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.6                                    ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 7, 2022                               ###
;;;###                                                       ###
;;;###  updated February 20, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-cons x y)
  (begin
    (* (expt 2 x) (expt 3 y))
    ))

;;;#############################################################
;;;#############################################################
(define (my-car z)
  (define (local-iter anum count)
    (begin
      (if (not (zero? (modulo anum 2)))
          (begin
            count)
          (begin
            (local-iter (/ anum 2) (1+ count))
            ))
      ))
  (begin
    (local-iter z 0)
    ))

;;;#############################################################
;;;#############################################################
(define (my-cdr z)
  (define (local-iter anum count)
    (begin
      (if (not (zero? (modulo anum 3)))
          (begin
            count)
          (begin
            (local-iter (/ anum 3) (1+ count))
            ))
      ))
  (begin
    (local-iter z 0)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In case representing pairs as "))
    (display
     (format #f "procedures wasn't mind-boggling~%"))
    (display
     (format #f "enough, consider that, in a language "))
    (display
     (format #f "that can manipulate~%"))
    (display
     (format #f "procedures, we can get by without "))
    (display
     (format #f "numbers (at least insofar~%"))
    (display
     (format #f "as nonnegative integers are concerned) "))
    (display
     (format #f "by implementing 0 and the~%"))
    (display
     (format #f "the operation of adding 1 as~%"))
    (newline)
    (display
     (format #f "(define zero (lambda (f) "))
    (display
     (format #f "(lambda (x) x)))~%"))
    (display
     (format #f "(define (add-1 n)~%"))
    (display
     (format #f "  (lambda (f) "))
    (display
     (format #f "(lambda (x) (f ((n f) x)))))~%"))
    (newline)
    (display
     (format #f "This representation is known as Church "))
    (display
     (format #f "numerals, after its~%"))
    (display
     (format #f "inventor, Alonzo Church, the logician "))
    (display
     (format #f "who invented the lambda~%"))
    (display
     (format #f "calculus. Define one and two directly "))
    (display
     (format #f "(not in terms~%"))
    (display
     (format #f "of zero and add-1). (Hint: Use "))
    (display
     (format #f "substitution to evaluate~%"))
    (display
     (format #f "(add-1 zero)). Give a direct definition "))
    (display
     (format #f "of the addition~%"))
    (display
     (format #f "procedure + (not in terms of repeated "))
    (display
     (format #f "application of~%"))
    (display
     (format #f "add-1).~%"))
    (newline)
    (display
     (format #f "one = (add-1 zero)~%"))
    (display
     (format #f "-> (add-1 (lambda (f) (lambda (x) x)))~%"))
    (display
     (format #f "-> (lambda (f) (lambda (x) (f (((lambda (f) (lambda (x) x)) f) x))))~%"))
    (display
     (format #f "-> (lambda (f) (lambda (x) (f ((lambda (x) x) x))))~%"))
    (display
     (format #f "-> (lambda (f) (lambda (x) (f x)))~%"))
    (newline)
    (display
     (format #f "two = (add-1 (add-1 zero))~%"))
    (display
     (format #f "-> (add-1 (lambda (f) (lambda (x) (f x))))~%"))
    (display
     (format #f "-> (lambda (f) (lambda (x) (f (((lambda (f) (lambda (x) (f x))) f) x))))~%"))
    (display
     (format #f "-> (lambda (f) (lambda (x) (f ((lambda (x) (f x)) x))))~%"))
    (display
     (format #f "-> (lambda (f) (lambda (x) (f (f x))))~%"))
    (newline)
    (display
     (format #f "n +  m = ~%"))
    (display
     (format #f "(lambda (f) (lambda (x) (f (f ... (f x)))))~%"))
    (display
     (format #f "where f is applied n+m times.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.06 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
