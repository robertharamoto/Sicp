#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.47                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code macro and current-date-time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for run-all-tests functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-vect x y)
  (begin
    (cons x y)
    ))

;;;#############################################################
;;;#############################################################
(define (xcor-vect vec)
  (begin
    (car vec)
    ))

;;;#############################################################
;;;#############################################################
(define (ycor-vect vec)
  (begin
    (cdr vec)
    ))

;;;#############################################################
;;;#############################################################
(define (add-vect vec1 vec2)
  (begin
    (let ((new-x (+ (xcor-vect vec1) (xcor-vect vec2)))
          (new-y (+ (ycor-vect vec1) (ycor-vect vec2))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-vect-1")
         (test-list
          (list
           (list (make-vect 1 2) (make-vect 3 4) (make-vect 4 6))
           (list (make-vect 3 4) (make-vect 1 2) (make-vect 4 6))
           (list (make-vect 1 2) (make-vect 11 13) (make-vect 12 15))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((v1 (list-ref this-list 0))
                  (v2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (add-vect v1 v2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : v1=~a, v2=~a, "
                        sub-name test-label-index v1 v2))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-vect vec1 vec2)
  (begin
    (let ((new-x (- (xcor-vect vec1) (xcor-vect vec2)))
          (new-y (- (ycor-vect vec1) (ycor-vect vec2))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sub-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-sub-vect-1")
         (test-list
          (list
           (list (make-vect 1 2) (make-vect 3 4) (make-vect -2 -2))
           (list (make-vect 3 4) (make-vect 1 2) (make-vect 2 2))
           (list (make-vect 1 2) (make-vect 11 13) (make-vect -10 -11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((v1 (list-ref this-list 0))
                  (v2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (sub-vect v1 v2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : v1=~a, v2=~a, "
                        sub-name test-label-index v1 v2))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (scale-vect scalar vec1)
  (begin
    (let ((new-x (* scalar (xcor-vect vec1)))
          (new-y (* scalar (ycor-vect vec1))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-scale-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-scale-vect-1")
         (test-list
          (list
           (list 3 (make-vect 1 2) (make-vect 3 6))
           (list 5 (make-vect 3 4) (make-vect 15 20))
           (list -3 (make-vect 1 2) (make-vect -3 -6))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((scalar (list-ref this-list 0))
                  (v1 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (scale-vect scalar v1)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : scalar=~a, v1=~a, "
                        sub-name test-label-index scalar v1))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-frame-list origin edge1 edge2)
  (begin
    (list origin edge1 edge2)
    ))

;;;#############################################################
;;;#############################################################
(define (get-frame-list-origin frame)
  (begin
    (list-ref frame 0)
    ))

;;;#############################################################
;;;#############################################################
(define (get-frame-list-edge1 frame)
  (begin
    (list-ref frame 1)
    ))

;;;#############################################################
;;;#############################################################
(define (get-frame-list-edge2 frame)
  (begin
    (list-ref frame 2)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-frame-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-frame-list-1")
         (test-list
          (list
           (list (make-vect 0 0) (make-vect 1 2) (make-vect 3 6))
           (list (make-vect 0 0) (make-vect 3 4) (make-vect -1 -2))
           (list (make-vect 1 2) (make-vect 50 55) (make-vect 22 23))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((origin (list-ref this-list 0))
                  (edge1 (list-ref this-list 1))
                  (edge2 (list-ref this-list 2)))
              (let ((lframe (make-frame-list origin edge1 edge2)))
                (let ((r-origin (get-frame-list-origin lframe))
                      (r-edge1 (get-frame-list-edge1 lframe))
                      (r-edge2 (get-frame-list-edge2 lframe)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : origin=~a, "
                          sub-name test-label-index origin))
                        (err-msg-2
                         (format
                          #f "shouldbe origin=~a, result=~a, "
                          origin r-origin))
                        (err-msg-3
                         (format
                          #f "shouldbe edge1=~a, result=~a, "
                          edge1 r-edge1))
                        (err-msg-4
                         (format
                          #f "shouldbe edge1=~a, result=~a"
                          edge2 r-edge2))
                        (aflag
                         (and
                          (equal? origin r-origin)
                          (equal? edge1 r-edge1)
                          (equal? edge2 r-edge2))))
                    (begin
                      (unittest2:assert?
                       (equal? aflag #t)
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2
                        err-msg-3 err-msg-4)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-frame-cons origin edge1 edge2)
  (begin
    (cons origin (cons edge1 edge2))
    ))

;;;#############################################################
;;;#############################################################
(define (get-frame-cons-origin frame)
  (begin
    (car frame)
    ))

;;;#############################################################
;;;#############################################################
(define (get-frame-cons-edge1 frame)
  (begin
    (car (cdr frame))
    ))

;;;#############################################################
;;;#############################################################
(define (get-frame-cons-edge2 frame)
  (begin
    (cdr (cdr frame))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-frame-cons-1 result-hash-table)
 (begin
   (let ((sub-name "test-frame-cons-1")
         (test-list
          (list
           (list (make-vect 0 0) (make-vect 1 2) (make-vect 3 6))
           (list (make-vect 0 0) (make-vect 3 4) (make-vect -1 -2))
           (list (make-vect 1 2) (make-vect 50 55) (make-vect 22 23))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((origin (list-ref this-list 0))
                  (edge1 (list-ref this-list 1))
                  (edge2 (list-ref this-list 2)))
              (let ((lframe (make-frame-cons origin edge1 edge2)))
                (let ((r-origin (get-frame-cons-origin lframe))
                      (r-edge1 (get-frame-cons-edge1 lframe))
                      (r-edge2 (get-frame-cons-edge2 lframe)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : origin=~a, "
                          sub-name test-label-index origin))
                        (err-msg-2
                         (format
                          #f "shouldbe origin=~a, result=~a, "
                          origin r-origin))
                        (err-msg-3
                         (format
                          #f "shouldbe edge1=~a, result=~a, "
                          edge1 r-edge1))
                        (err-msg-4
                         (format
                          #f "shouldbe edge1=~a, result=~a"
                          edge2 r-edge2))
                        (aflag
                         (and
                          (equal? origin r-origin)
                          (equal? edge1 r-edge1)
                          (equal? edge2 r-edge2))))
                    (begin
                      (unittest2:assert?
                       (equal? aflag #t)
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2
                        err-msg-3 err-msg-4)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Here are two possible constructors "))
    (display
     (format #f "for frames:~%"))
    (display
     (format #f "(define (make-frame origin "))
    (display
     (format #f "edge1 edge2)~%"))
    (display
     (format #f "  (list origin edge1 edge2))~%"))
    (display
     (format #f "(define (make-frame origin "))
    (display
     (format #f "edge1 edge2)~%"))
    (display
     (format #f "  (cons origin (cons edge1 edge2)))~%"))
    (display
     (format #f "For each constructor supply the "))
    (display
     (format #f "appropriate selectors~%"))
    (display
     (format #f "to produce an implementation for "))
    (display
     (format #f "frames.~%"))
    (newline)
    (display
     (format #f "(define (make-frame-list "))
    (display
     (format #f "origin edge1 edge2)~%"))
    (display
     (format #f "    (list origin edge1 edge2))~%"))
    (display
     (format #f "(define (get-frame-list-origin frame)~%"))
    (display
     (format #f "    (list-ref frame 0))~%"))
    (display
     (format #f "(define (get-frame-list-edge1 frame)~%"))
    (display
     (format #f "    (list-ref frame 1))~%"))
    (display
     (format #f "(define (get-frame-list-edge2 frame)~%"))
    (display
     (format #f "    (list-ref frame 2))~%"))
    (newline)
    (display
     (format #f "(define (make-frame-cons "))
    (display
     (format #f "origin edge1 edge2)~%"))
    (display
     (format #f "    (cons origin (cons edge1 edge2)))~%"))
    (display
     (format #f "(define (get-frame-cons-origin frame)~%"))
    (display
     (format #f "    (car frame))~%"))
    (display
     (format #f "(define (get-frame-cons-edge1 frame)~%"))
    (display
     (format #f "    (car (cdr frame)))~%"))
    (display
     (format #f "(define (get-frame-cons-edge2 frame)~%"))
    (display
     (format #f "    (cdr (cdr frame)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list (make-vect 0 0)
                  (make-vect 1 2)
                  (make-vect 3 6))
            (list (make-vect 0 0)
                  (make-vect 3 4)
                  (make-vect -1 -2))
            )))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((origin (list-ref alist 0))
                   (edge1 (list-ref alist 1))
                   (edge2 (list-ref alist 2)))
               (begin
                 (display
                  (format
                   #f "origin=~a, edge1=~a, edge2=~a~%"
                   origin edge1 edge2))
                 (display
                  (format
                   #f "frame-list=~a : frame-cons=~a~%"
                   (make-frame-list origin edge1 edge2)
                   (make-frame-cons origin edge1 edge2)))
                 (newline)
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.47 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme tests~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
