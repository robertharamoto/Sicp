#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.96                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define the-empty-termlist (list))

;;;#############################################################
;;;#############################################################
(define (empty-termlist? term-list)
  (begin
    (null? term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (first-term term-list)
  (begin
    (car term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (rest-terms term-list)
  (begin
    (cdr term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (order term)
  (begin
    (car term)
    ))

;;;#############################################################
;;;#############################################################
(define (coeff term)
  (begin
    (cadr term)
    ))

;;;#############################################################
;;;#############################################################
(define div /)

;;;#############################################################
;;;#############################################################
(define (make-term order coeff)
  (begin
    (list order coeff)
    ))

;;;#############################################################
;;;#############################################################
(define (=zero? anum)
  (begin
    (cond
     ((integer? anum)
      (begin
        (zero? anum)
        ))
     ((real? anum)
      (begin
        (< (abs anum) 1e-12)
        ))
     ((rational? anum)
      (begin
        (< (abs (exact->inexact anum) 1e-12))
        ))
     ((and (pair? anum)
           (symbol? (variable anum)))
      (begin
        (null? (term-list anum))
        ))
     ((and (pair? anum)
           (not (null? anum)))
      (begin
        #f
        ))
     (else
      (begin
        (= anum 0)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (adjoin-term term term-list)
  (begin
    (if (=zero? (coeff term))
        (begin
          term-list)
        (begin
          (cons term term-list)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (variable apoly)
  (begin
    (car apoly)
    ))

;;;#############################################################
;;;#############################################################
(define (term-list apoly)
  (begin
    (cdr apoly)
    ))

;;;#############################################################
;;;#############################################################
(define (make-polynomial var term-list)
  (begin
    (cons var term-list)
    ))

;;;#############################################################
;;;#############################################################
(define (numer x)
  (begin
    (car x)
    ))

;;;#############################################################
;;;#############################################################
(define (denom x)
  (begin
    (cdr x)
    ))

;;;#############################################################
;;;#############################################################
(define (add-terms L1 L2)
  (begin
    (cond
     ((empty-termlist? L1)
      (begin
        L2
        ))
     ((empty-termlist? L2)
      (begin
        L1
        ))
     (else
      (begin
        (let ((t1 (first-term L1))
              (t2 (first-term L2)))
          (begin
            (cond
             ((> (order t1) (order t2))
              (begin
                (adjoin-term
                 t1 (add-terms (rest-terms L1) L2))
                ))
             ((< (order t1) (order t2))
              (begin
                (adjoin-term
                 t2 (add-terms L1 (rest-terms L2)))
                ))
             (else
              (begin
                (adjoin-term
                 (make-term (order t1)
                            (+ (coeff t1) (coeff t2)))
                 (add-terms (rest-terms L1)
                            (rest-terms L2)))
                )))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (add-poly p1 p2)
  (begin
    (let ((var-1 (variable p1))
          (var-2 (variable p2)))
      (begin
        (if (eq? var-1 var-2)
            (begin
              (let ((term-1-list (term-list p1))
                    (term-2-list (term-list p2)))
                (let ((result-term-list
                       (add-terms term-1-list term-2-list)))
                  (begin
                    (make-polynomial var-1 result-term-list)
                    ))
                ))
            (begin
              (display
               (format
                #f "add-poly error: var-1 (~a) must be " var-1))
              (display
               (format
                #f "equal to var-2 (~a)~%" var-2))
              (display (format #f "quitting...~%"))
              (force-output)
              (quit)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-rat r1 r2)
  (begin
    (let ((numer-1 (numer r1))
          (denom-1 (denom r1))
          (numer-2 (numer r2))
          (denom-2 (denom r2)))
      (begin
        (cond
         ((and (number? numer-1) (number? denom-1)
               (number? numer-2) (number? denom-2))
          (begin
            (let ((next-numer
                   (+ (* numer-1 denom-2) (* numer-2 denom-1)))
                  (next-denom (* denom-1 denom-2)))
              (let ((gg (gcd next-numer next-denom)))
                (let ((anum (/ next-numer gg))
                      (aden (/ next-denom gg)))
                  (begin
                    (make-rat anum aden)
                    ))
                ))
            ))
         ((and (pair? numer-1) (pair? denom-1)
               (pair? numer-2) (pair? denom-2))
          (begin
            (let ((num-tl-1 (term-list numer-1))
                  (den-tl-1 (term-list denom-1))
                  (num-tl-2 (term-list numer-2))
                  (den-tl-2 (term-list denom-2)))
              (let ((t1 (mul-terms num-tl-1 den-tl-2))
                    (t2 (mul-terms num-tl-2 den-tl-1)))
                (let ((res-num
                       (add-terms t1 t2))
                      (res-den
                       (mul-terms den-tl-1 den-tl-2)))
                  (begin
                    (make-rat
                     (make-polynomial (car numer-1) res-num)
                     (make-polynomial (car denom-1) res-den))
                    ))
                ))
            ))
         (else
          (begin
            (display
             (format
              #f "add-rat error: unhandled addition case "))
            (display
             (format
              #f "for ~a, ~a~%" r1 r2))
            (display
             (format
              #f "quitting...~%"))
            (force-output)
            (quit)
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (negate apoly)
  (define (local-iter term-list acc-list)
    (begin
      (if (null? term-list)
          (begin
            acc-list)
          (begin
            (let ((first-term (car term-list))
                  (tail-list (cdr term-list)))
              (let ((o1 (order first-term))
                    (c1 (coeff first-term)))
                (let ((neg-coeff (* -1 c1)))
                  (let ((neg-term (make-term o1 c1)))
                    (let ((next-acc-list (cons neg-term acc-list)))
                      (begin
                        (local-iter tail-list next-acc-list)
                        ))
                    ))
                ))
            ))
      ))
  (begin
    (let ((neg-term-list
           (reverse (local-iter (term-list apoly) (list))))
          (var (variable apoly)))
      (begin
        (make-polynomial var neg-term-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-poly p1 p2)
  (begin
    (let ((n-p2 (negate p2)))
      (begin
        (add-poly p1 n-p2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (negate-terms at-list)
  (define (local-iter term-list acc-list)
    (begin
      (if (null? term-list)
          (begin
            acc-list)
          (begin
            (let ((first-term (car term-list))
                  (tail-list (cdr term-list)))
              (let ((o1 (order first-term))
                    (c1 (coeff first-term)))
                (let ((neg-coeff (* -1 c1)))
                  (let ((neg-term (make-term o1 neg-coeff)))
                    (let ((next-acc-list (cons neg-term acc-list)))
                      (begin
                        (local-iter tail-list next-acc-list)
                        ))
                    ))
                ))
            ))
      ))
  (begin
    (let ((neg-term-list
           (reverse (local-iter at-list (list)))))
      (begin
        neg-term-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-terms t1 t2)
  (begin
    (let ((n-t2 (negate-terms t2)))
      (begin
        (add-terms t1 n-t2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mul-terms L1 L2)
  (begin
    (if (empty-termlist? L1)
        (begin
          the-empty-termlist)
        (begin
          (add-terms (mul-term-by-all-terms (first-term L1) L2)
                     (mul-terms (rest-terms L1) L2))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (mul-term-by-all-terms t1 L)
  (begin
    (if (empty-termlist? L)
        (begin
          the-empty-termlist)
        (begin
          (let ((t2 (first-term L)))
            (begin
              (adjoin-term
               (make-term (+ (order t1) (order t2))
                          (* (coeff t1) (coeff t2)))
               (mul-term-by-all-terms t1 (rest-terms L)))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (sort-terms-list a-term-list)
  (begin
    (stable-sort
     a-term-list
     (lambda (a b)
       (begin
         (> (order a) (order b))
         )))
    ))

;;;#############################################################
;;;#############################################################
(define (div-terms L1 L2)
  (define (local-iter L1 L2)
    (begin
      (if (empty-termlist? L1)
          (begin
            (list
             the-empty-termlist the-empty-termlist))
          (begin
            (let ((t1 (first-term L1))
                  (t2 (first-term L2)))
              (begin
                (if (> (order t2) (order t1))
                    (begin
                      (list the-empty-termlist L1))
                    (begin
                      (let ((new-c
                             (exact->inexact (/ (coeff t1) (coeff t2))))
                            (new-o
                             (- (order t1) (order t2))))
                        (let ((new-term (make-term new-o new-c)))
                          (let ((rest-of-result
                                 (stable-sort
                                  (sub-terms
                                   L1
                                   (mul-term-by-all-terms new-term L2))
                                  (lambda (a b)
                                    (begin
                                      (> (order a) (order b))
                                      )))
                                 ))
                            (let ((end-list-list
                                   (local-iter rest-of-result L2)))
                              (let ((acc-list (car end-list-list))
                                    (remainder (list-ref end-list-list 1)))
                                (begin
                                  (list
                                   (cons new-term acc-list)
                                   remainder)
                                  ))
                              ))
                          ))
                      ))
                ))
            ))
      ))
  (begin
    (let ((s-l1 (sort-terms-list L1))
          (s-l2 (sort-terms-list L2)))
      (begin
        (local-iter s-l1 s-l2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-2-list-lists-are-equal?
         slist-list rlist-list
         sub-name error-message
         result-hash-table)
  (begin
    (let ((slist-1 (car slist-list))
          (slen (length slist-list))
          (rlist-1 (car rlist-list))
          (rlen (length rlist-list)))
      (begin
        (if (not (list? slist-1))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii slen))
                (begin
                  (let ((selem-1 (list-ref slist-list ii))
                        (relem-1 (list-ref rlist-list ii)))
                    (let ((err-msg-1
                           (format
                            #f ", shouldbe 1=~a, result 1=~a, "
                            selem-1 relem-1)))
                      (begin
                        (unittest2:assert?
                         (equal? selem-1 relem-1)
                         sub-name
                         (string-append error-message err-msg-1)
                         result-hash-table)
                        )))
                  )))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii slen))
                (begin
                  (let ((slist-1 (list-ref slist-list ii))
                        (rlist-1 (list-ref rlist-list ii)))
                    (begin
                      (if (and (not (null? slist-1))
                               (not (null? rlist-1)))
                          (begin
                            (assert-2-list-lists-are-equal?
                             slist-1 rlist-1
                             sub-name error-message
                             result-hash-table)
                            ))
                      ))
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-div-terms-1 result-hash-table)
 (begin
   (let ((sub-name "test-div-terms-1")
         (test-list
          (list
           (list (list (list 0 -1) (list 5 1))
                 (list (list 0 -1) (list 2 1))
                 (list (list (list 3 1.0) (list 1 1.0))
                       (list (list 1 1.0) (list 0 -1))))
           (list (list (list 0 -1) (list 2 1))
                 (list (list 0 -1) (list 1 1))
                 (list (list (list 1 1.0) (list 0 1.0))
                       (list)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((dividend (list-ref this-list 0))
                  (divisor (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list (div-terms dividend divisor)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-msg-2
                         (format
                          #f "dividend=~a, divisor=~a, "
                          dividend divisor))
                        (err-msg-3
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-list-list result-list-list))
                        (err-msg-4
                         (format
                          #f "shouldbe length=~a, result=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2
                        err-msg-3 err-msg-4)
                       result-hash-table)

                      (assert-2-list-lists-are-equal?
                       shouldbe-list-list result-list-list
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2 err-msg-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (remainder-terms L1 L2)
  (begin
    (if (null? L2)
        (begin
          L2)
        (begin
          (let ((div-results (div-terms L1 L2)))
            (begin
              (list-ref div-results 1)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (gcd-terms L1 L2)
  (begin
    (if (empty-termlist? L2)
        (begin
          L1)
        (begin
          (gcd-terms L2 (remainder-terms L1 L2))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-gcd anum1 anum2)
  (begin
    (let ((my-tol 1e-15))
      (begin
        (if (< (abs anum2) my-tol)
            (begin
              anum1)
            (begin
              (my-gcd anum2 (remainder anum1 anum2))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (remove-gcd-coeff a-term-list)
  (begin
    (let ((common-gcd #f)
          (result (list)))
      (begin
        (if (null? a-term-list)
            (begin
              a-term-list)
            (begin
              (for-each
               (lambda (aterm)
                 (begin
                   (let ((this-coeff (list-ref aterm 1)))
                     (begin
                       (if (equal? common-gcd #f)
                           (begin
                             (set! common-gcd this-coeff))
                           (begin
                             (set! common-gcd
                                   (my-gcd this-coeff common-gcd))
                             ))
                       ))
                   )) a-term-list)

              (for-each
               (lambda (aterm)
                 (begin
                   (let ((this-coeff (list-ref aterm 1)))
                     (let ((next-coeff
                            (exact->inexact
                             (/ this-coeff common-gcd))))
                       (let ((next-term
                              (make-term
                               (car aterm) next-coeff)))
                         (begin
                           (set! result (cons next-term result))
                           ))
                       ))
                   )) a-term-list)

              (reverse result)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (pseudogcd-terms L1 L2)
  (begin
    (if (empty-termlist? L2)
        (begin
          (remove-gcd-coeff L1))
        (begin
          (pseudogcd-terms
           L2 (pseudoremainder-terms L1 L2))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-leading-order-from-term-list a-term-list)
  (begin
    (let ((result-order #f))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (let ((aorder (order aterm)))
               (begin
                 (if (or (equal? result-order #f)
                         (< result-order aorder))
                     (begin
                       (set! result-order aorder)
                       ))
                 ))
             )) a-term-list)

        result-order
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-leading-coeff-from-term-list a-term-list)
  (begin
    (let ((result-order #f)
          (result-coeff #f))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (let ((aorder (order aterm)))
               (begin
                 (if (or (equal? result-order #f)
                         (< result-order aorder))
                     (begin
                       (set! result-order aorder)
                       (set! result-coeff (coeff aterm))
                       ))
                 ))
             )) a-term-list)

        result-coeff
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mult-coeff-term-list aconstant a-term-list)
  (begin
    (let ((result (list)))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (let ((aorder (order aterm))
                   (acoeff (* aconstant (coeff aterm))))
               (let ((next-term
                      (make-term aorder acoeff)))
                 (begin
                   (set! result (cons next-term result))
                   )))
             )) a-term-list)

        (reverse result)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (pseudoremainder-terms L1 L2)
  (begin
    (if (null? L2)
        (begin
          L2)
        (begin
          (let ((o1 (get-leading-order-from-term-list L1))
                (o2 (get-leading-order-from-term-list L2))
                (c2 (get-leading-coeff-from-term-list L2)))
            (let ((intfact (expt c2 (- (+ 1 o1) o2))))
              (let ((m-tl-1 (mult-coeff-term-list intfact L1)))
                (let ((div-results (div-terms m-tl-1 L2)))
                  (begin
                    (list-ref div-results 1)
                    ))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (gcd-poly p1 p2)
  (begin
    (let ((v1 (variable p1))
          (tl-1 (term-list p1))
          (v2 (variable p2))
          (tl-2 (term-list p2)))
      (begin
        (if (equal? v1 v2)
            (begin
              (let ((r-tl
                     (pseudogcd-terms
                      tl-2 (pseudoremainder-terms tl-1 tl-2))))
                (begin
                  (make-polynomial v1 r-tl)
                  )))
            (begin
              (display
               (format
                #f "gcd-poly error: variables must be equal!~%"))
              (display
               (format
                #f "v1 = ~a, v2 = ~a  : p1 = ~a, p2 = ~a~%"
                v1 v2 p1 p2))
              (force-output)
              (quit)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-rat n d)
  (begin
    (cons n d)
    ))

;;;#############################################################
;;;#############################################################
;;; each term-list is of the form
;;; (list 'x (list order coeff)...)
;;; each modified term is of the form
;;; (list 'x order (list 'y (list order coeff)... )
;;; so a modified term list is of the form
;;; (list (list 'x order coeff) (list 'y order coeff))
(define (polynomial-to-modified-term-list apoly)
  (begin
    (let ((acc-list (list))
          (var-1 (variable apoly))
          (tl-1 (term-list apoly)))
      (let ((tl-len (length tl-1)))
        (begin
          (for-each
           (lambda (aterm)
             (begin
               (if (and (pair? aterm) (pair? (coeff aterm))
                        (symbol? (variable (coeff aterm))))
                   (begin
                     (let ((oterm (order aterm))
                           (coeff-mterm-list
                            (polynomial-to-modified-term-list
                             (coeff aterm))))
                       (let ((next-list
                              (map
                               (lambda (bterm)
                                 (begin
                                   (cons (list var-1 oterm #f) bterm)
                                   )) coeff-mterm-list)))
                         (begin
                           (set! acc-list
                                 (append acc-list next-list))
                           ))
                       ))
                   (begin
                     (let ((next-term (cons var-1 aterm)))
                       (begin
                         (set! acc-list
                               (cons (list next-term) acc-list))
                         ))
                     ))
               )) tl-1)

          acc-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-polynomial-to-modified-term-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-polynomial-to-modified-term-list-1")
         (test-list
          (list
           (list (make-polynomial
                  'x (list (list 0 -1) (list 2 1)))
                 (list (list (list 'x 2 1))
                       (list (list 'x 0 -1))))
           (list (make-polynomial
                  'x
                  (list (list 0 -1)
                        (list
                         2
                         (make-polynomial
                          'y (list (list 0 3)
                                   (list 1 2)
                                   (list 2 5))))))
                 (list (list (list 'x 0 -1))
                       (list (list 'x 2 #f) (list 'y 2 5))
                       (list (list 'x 2 #f) (list 'y 1 2))
                       (list (list 'x 2 #f) (list 'y 0 3))
                       ))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((apoly (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list
                     (polynomial-to-modified-term-list apoly)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : apoly=~a, "
                          sub-name test-label-index
                          apoly))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-list-list result-list-list))
                        (err-msg-3
                         (format
                          #f "shouldbe length=~a, result=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2 err-msg-3)
                       result-hash-table)

                      (assert-2-list-lists-are-equal?
                       shouldbe-list-list result-list-list
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sort-mod-terms-by-symbols-order mod-terms-list)
  (begin
    (stable-sort
     (map
      (lambda (alist)
        (begin
          (stable-sort
           alist
           (lambda (a b)
             (begin
               (string-ci<? (symbol->string (car a))
                            (symbol->string (car b)))
               )))
          )) mod-terms-list)
     (lambda (a b)
       (begin
         (let ((sa (symbol->string (car (car a))))
               (sb (symbol->string (car (car b)))))
           (begin
             (if (string-ci<? sa sb)
                 (begin
                   #t)
                 (begin
                   (if (string-ci=? sa sb)
                       (begin
                         (> (cadr (car a)) (cadr (car b))))
                       (begin
                         #f
                         ))
                   ))
             ))
         )))
    ))

;;;#############################################################
;;;#############################################################
(define (polynomial-to-string apoly)
  (begin
    (let ((result-string "")
          (mt
           (sort-mod-terms-by-symbols-order
            (polynomial-to-modified-term-list apoly))))
      (begin
        (for-each
         (lambda (aterm)
           (begin
             (let ((current-string "")
                   (current-coeff 0))
               (begin
                 (for-each
                  (lambda (bterm)
                    (begin
                      (let ((svar
                             (symbol->string
                              (list-ref bterm 0)))
                            (border (list-ref bterm 1))
                            (bcoeff (list-ref bterm 2)))
                        (begin
                          (if (not (equal? bcoeff #f))
                              (begin
                                (set! current-coeff bcoeff)
                                (if (<= (string-length current-string) 0)
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set!
                                             current-string
                                             (format #f "~a" svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a^~a"
                                                    svar border))
                                                  ))
                                            )))
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set!
                                             current-string
                                             (format
                                              #f "~a*~a"
                                              current-string svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a*~a^~a"
                                                    current-string
                                                    svar border))
                                                  ))
                                            ))
                                      )))
                              (begin
                                (if (<= (string-length current-string) 0)
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set!
                                             current-string
                                             (format #f "~a" svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a^~a"
                                                    svar border))
                                                  ))
                                            )))
                                    (begin
                                      (if (= border 1)
                                          (begin
                                            (set!
                                             current-string
                                             (format
                                              #f "~a*~a"
                                              current-string svar)))
                                          (begin
                                            (if (> border 1)
                                                (begin
                                                  (set!
                                                   current-string
                                                   (format
                                                    #f "~a*~a^~a"
                                                    current-string
                                                    svar border))
                                                  ))
                                            ))
                                      ))
                                ))
                          ))
                      )) aterm)
                 (if (<= (string-length current-string) 0)
                     (begin
                       (set!
                        current-string
                        (format
                         #f "~a" current-coeff)))
                     (begin
                       (set!
                        current-string
                        (format
                         #f "~a*~a"
                         current-coeff current-string))
                       ))

                 (if (<= (string-length result-string) 0)
                     (begin
                       (set!
                        result-string current-string))
                     (begin
                       (set!
                        result-string
                        (format
                         #f "~a + ~a"
                         result-string current-string))
                       ))
                 ))
             )) mt)

        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-polynomial-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-polynomial-to-string-1")
         (test-list
          (list
           (list (make-polynomial
                  'x (list (list 3 5)))
                 "5*x^3")
           (list (make-polynomial
                  'x (list (list 3 7) (list 2 3)))
                 "7*x^3 + 3*x^2")
           (list (make-polynomial
                  'x (list
                      (list 3 (make-polynomial
                               'y (list (list 2 20))))))
                 "20*x^3*y^2")
           (list (make-polynomial
                  'x (list
                      (list 3 (make-polynomial
                               'y (list (list 2 20))))
                      (list 1 (make-polynomial
                               'y (list (list 2 16))))))
                 "20*x^3*y^2 + 16*x*y^2")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((apoly (list-ref this-list 0))
                  (shouldbe-string
                   (list-ref this-list 1)))
              (let ((result-string
                     (polynomial-to-string apoly)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : apoly=~a, "
                        sub-name test-label-index
                        apoly))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe-string result-string)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sample-0 p1 p2 p3)
  (begin
    (let ((str-p1 (polynomial-to-string p1))
          (str-p2 (polynomial-to-string p2))
          (str-p3 (polynomial-to-string p3))
          (asym (variable p1)))
      (let ((q1
             (make-polynomial
              asym (mul-terms (term-list p1) (term-list p2))))
            (q2
             (make-polynomial
              asym (mul-terms
                    (term-list p1) (term-list p3)))))
        (let ((str-q1
               (polynomial-to-string q1))
              (str-q2
               (polynomial-to-string q2))
              (alist-list
               (div-terms (term-list p1) (term-list p2))))
          (begin
            (display
             (format
              #f "p1 = (~a), p2 = (~a), p3 = (~a)~%"
              str-p1 str-p2 str-p3))
            (display
             (format
              #f "q1 = p1 * p2 = (~a), q2 = p1 * p3 = (~a)~%"
              str-q1 str-q2))
            (force-output)
            (display
             (format
              #f "(~a) / (~a) = ~a~%"
              str-q1 str-q2
              (polynomial-to-string
               (make-polynomial
                asym (car alist-list)))))
            (display
             (format
              #f "remainder = ~a (not using pseudo-division)~%"
              (polynomial-to-string
               (make-polynomial
                asym (list-ref alist-list 1)))))
            (force-output)

            (let ((gcdp (gcd-poly q1 q2)))
              (begin
                (display
                 (format
                  #f "gcd(q1, q2) = ~a (pseudo-division)~%"
                  (polynomial-to-string gcdp)))
                (force-output)

                (let ((dl-1
                       (div-terms (term-list q1) (term-list gcdp)))
                      (dl-2
                       (div-terms (term-list q2) (term-list gcdp))))
                  (let ((dp-1 (make-polynomial asym (car dl-1)))
                        (rm-1 (make-polynomial asym (cadr dl-1)))
                        (dp-2 (make-polynomial asym (car dl-2)))
                        (rm-2 (make-polynomial asym (cadr dl-2))))
                    (begin
                      (display
                       (format #f "gcd check~%"))
                      (display
                       (format
                        #f "(~a) / (~a) = ~a, remainder =~a~%"
                        str-q1 (polynomial-to-string gcdp)
                        (polynomial-to-string dp-1)
                        (polynomial-to-string rm-1)))
                      (display
                       (format
                        #f "(~a) / (~a) = ~a, remainder =~a~%"
                        str-q2 (polynomial-to-string gcdp)
                        (polynomial-to-string dp-2)
                        (polynomial-to-string rm-2)))
                      (force-output)
                      )))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "a. Implement the procedure "))
    (display
     (format #f "pseudoremainder-terms,~%"))
    (display
     (format #f "which is just like remainder-terms "))
    (display
     (format #f "except that it~%"))
    (display
     (format #f "multiplies the dividend by the "))
    (display
     (format #f "integerizing factor~%"))
    (display
     (format #f "described above before calling div-terms. "))
    (display
     (format #f "Modify~%"))
    (display
     (format #f "gcd-terms to use pseudoremainder-terms, "))
    (display
     (format #f "and verify that~%"))
    (display
     (format #f "greatest-common-divisor now produces "))
    (display
     (format #f "an answer with~%"))
    (display
     (format #f "integer coefficients on the example "))
    (display
     (format #f "in exercise 2.95.~%"))
    (display
     (format #f "b. The GCD now has integer coefficients, "))
    (display
     (format #f "but they are~%"))
    (display
     (format #f "larger than those of P1. Modify gcd-terms "))
    (display
     (format #f "so that it~%"))
    (display
     (format #f "removes common factors from the "))
    (display
     (format #f "coefficients of the~%"))
    (display
     (format #f "answer by dividing all the coefficients "))
    (display
     (format #f "by their~%"))
    (display
     (format #f "(integer) greatest common divisor.~%"))
    (newline)
    (display
     (format #f "Thus, here is how to reduce a rational "))
    (display
     (format #f "function to lowest~%"))
    (display
     (format #f "terms: Compute the GCD of the numerator "))
    (display
     (format #f "and denominator,~%"))
    (display
     (format #f "using the version of gcd-terms from "))
    (display
     (format #f "exercise 2.96.~%"))
    (display
     (format #f "When you obtain the GCD, multiply "))
    (display
     (format #f "both numerator and~%"))
    (display
     (format #f "denominator by the same integerizing "))
    (display
     (format #f "factor before dividing~%"))
    (display
     (format #f "through by the GCD, so that division by "))
    (display
     (format #f "the GCD will not~%"))
    (display
     (format #f "introduce any noninteger coefficients. As "))
    (display
     (format #f "the factor you can~%"))
    (display
     (format #f "use the leading coefficient of the GCD "))
    (display
     (format #f "raised to the power~%"))
    (display
     (format #f "1 + O1 - O2, where O2 is the order of "))
    (display
     (format #f "the GCD and O1 is the~%"))
    (display
     (format #f "maximum of the orders of the numerator "))
    (display
     (format #f "and denominator. This~%"))
    (display
     (format #f "will ensure that dividing the numerator and "))
    (display
     (format #f "denominator by the~%"))
    (display
     (format #f "GCD will not introduce any fractions. "))
    (display
     (format #f "The result of~%"))
    (display
     (format #f "this operation will be a numerator and "))
    (display
     (format #f "denominator with~%"))
    (display
     (format #f "integer coefficients. The coefficients "))
    (display
     (format #f "will normally be~%"))
    (display
     (format #f "very large because of all of the "))
    (display
     (format #f "integerizing factors,~%"))
    (display
     (format #f "so the last step is to remove the "))
    (display
     (format #f "redundant factors by~%"))
    (display
     (format #f "computing the (integer) greatest common "))
    (display
     (format #f "divisor of all~%"))
    (display
     (format #f "the coefficients of the numerator and "))
    (display
     (format #f "the denominator and~%"))
    (display
     (format #f "dividing through by this "))
    (display
     (format #f "factor.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((p1 (make-polynomial 'x '((2 1) (1 -2) (0 1))))
          (p2 (make-polynomial 'x '((2 11) (0 7))))
          (p3 (make-polynomial 'x '((1 13) (0 5)))))
      (begin
        (display
         (format #f "(test 1) simple division test~%"))
        (sample-0 p1 p2 p3)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.96 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme tests~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
