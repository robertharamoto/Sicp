#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.78                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
        (begin
          (if proc
              (begin
                (apply proc (map contents args)))
              (begin
                (error
                 "No method for these types -- APPLY-GENERIC"
                 (list op type-tags))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (attach-tag type-tag contents)
  (begin
    (if (number? contents)
        (begin
          contents)
        (begin
          (cons type-tag contents)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (type-tag datum)
  (begin
    (cond
     ((number? datum)
      (begin
        'scheme-number
        ))
     ((pair? datum)
      (begin
        (car datum)
        ))
     (else
      (begin
        (error "Bad tagged datum -- TYPE-TAG" datum)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (contents datum)
  (begin
    (cond
     ((number? datum)
      (begin
        datum
        ))
     ((pair? datum)
      (begin
        (cdr datum)
        ))
     (else
      (begin
        (error "Bad tagged datum -- CONTENTS" datum)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The internal procedures in the "))
    (display
     (format #f "scheme-number package~%"))
    (display
     (format #f "are essentially nothing more than "))
    (display
     (format #f "calls to the~%"))
    (display
     (format #f "primitive procedures +, -, etc. "))
    (display
     (format #f "It was not possible~%"))
    (display
     (format #f "to use the primitives of the "))
    (display
     (format #f "language directly because~%"))
    (display
     (format #f "our type-tag system requires that "))
    (display
     (format #f "each data object have~%"))
    (display
     (format #f "a type attached to it. In fact, "))
    (display
     (format #f "however, all Lisp~%"))
    (display
     (format #f "implementations do have a type "))
    (display
     (format #f "system, which they use~%"))
    (display
     (format #f "internally. Primitive predicates "))
    (display
     (format #f "such as symbol? and~%"))
    (display
     (format #f "number? determine whether data "))
    (display
     (format #f "objects have particular~%"))
    (display
     (format #f "types. Modify the definitions of "))
    (display
     (format #f "type-tag, contents, and~%"))
    (display
     (format #f "attach-tag from section 2.4.2 so that "))
    (display
     (format #f "our generic system~%"))
    (display
     (format #f "takes advantage of Scheme's internal "))
    (display
     (format #f "type system. That is~%"))
    (display
     (format #f "to say, the system should work as "))
    (display
     (format #f "before except that~%"))
    (display
     (format #f "ordinary numbers should be represented "))
    (display
     (format #f "simply as Scheme~%"))
    (display
     (format #f "numbers rather than as pairs whose "))
    (display
     (format #f "car is the symbol~%"))
    (display
     (format #f "scheme-number.~%"))
    (newline)
    (display
     (format #f "(define (attach-tag "))
    (display
     (format #f "type-tag contents)~%"))
    (display
     (format #f "  (if (number? contents)~%"))
    (display
     (format #f "      contents~%"))
    (display
     (format #f "      (cons type-tag contents)))~%"))
    (newline)
    (display
     (format #f "(define (type-tag datum)~%"))
    (display
     (format #f "  (cond~%"))
    (display
     (format #f "   ((number? datum)~%"))
    (display
     (format #f "    'scheme-number)~%"))
    (display
     (format #f "   ((pair? datum)~%"))
    (display
     (format #f "    (car datum))~%"))
    (display
     (format #f "   (else~%"))
    (display
     (format #f "    (error \"Bad tagged datum -- "))
    (display
     (format #f "TYPE-TAG\" datum)~%"))
    (display
     (format #f "    )))~%"))
    (newline)
    (display
     (format #f "(define (contents datum)~%"))
    (display
     (format #f "  (cond~%"))
    (display
     (format #f "   ((number? datum)~%"))
    (display
     (format #f "    datum)~%"))
    (display
     (format #f "   ((pair? datum)~%"))
    (display
     (format #f "    (cdr datum))~%"))
    (display
     (format #f "   (else~%"))
    (display
     (format #f "    (error \"Bad tagged datum -- "))
    (display
     (format #f "CONTENTS\" datum)~%"))
    (display
     (format #f "    )))~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.78 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
