#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.49                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 11, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code macro and current-date-time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for run-all-tests functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-vect x y)
  (begin
    (cons x y)
    ))

;;;#############################################################
;;;#############################################################
(define (xcor-vect vec)
  (begin
    (car vec)
    ))

;;;#############################################################
;;;#############################################################
(define (ycor-vect vec)
  (begin
    (cdr vec)
    ))

;;;#############################################################
;;;#############################################################
(define (add-vect vec1 vec2)
  (begin
    (let ((new-x (+ (xcor-vect vec1) (xcor-vect vec2)))
          (new-y (+ (ycor-vect vec1) (ycor-vect vec2))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-vect-1")
         (test-list
          (list
           (list (make-vect 1 2) (make-vect 3 4) (make-vect 4 6))
           (list (make-vect 3 4) (make-vect 1 2) (make-vect 4 6))
           (list (make-vect 1 2) (make-vect 11 13) (make-vect 12 15))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((v1 (list-ref this-list 0))
                  (v2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (add-vect v1 v2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : v1=~a, v2=~a, "
                        sub-name test-label-index v1 v2))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-vect vec1 vec2)
  (begin
    (let ((new-x (- (xcor-vect vec1) (xcor-vect vec2)))
          (new-y (- (ycor-vect vec1) (ycor-vect vec2))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sub-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-sub-vect-1")
         (test-list
          (list
           (list (make-vect 1 2) (make-vect 3 4) (make-vect -2 -2))
           (list (make-vect 3 4) (make-vect 1 2) (make-vect 2 2))
           (list (make-vect 1 2) (make-vect 11 13) (make-vect -10 -11))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((v1 (list-ref this-list 0))
                  (v2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (sub-vect v1 v2)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : v1=~a, v2=~a, "
                        sub-name test-label-index v1 v2))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (scale-vect scalar vec1)
  (begin
    (let ((new-x (* scalar (xcor-vect vec1)))
          (new-y (* scalar (ycor-vect vec1))))
      (begin
        (make-vect new-x new-y)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-scale-vect-1 result-hash-table)
 (begin
   (let ((sub-name "test-scale-vect-1")
         (test-list
          (list
           (list 3 (make-vect 1 2) (make-vect 3 6))
           (list 5 (make-vect 3 4) (make-vect 15 20))
           (list -3 (make-vect 1 2) (make-vect -3 -6))
           ))
         (test-label-index 0)
         (ok-flag #t))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((scalar (list-ref this-list 0))
                  (v1 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (scale-vect scalar v1)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : scalar=~a, v1=~a, "
                        sub-name test-label-index scalar v1))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-segment svec evec)
  (begin
    (cons svec evec)
    ))

;;;#############################################################
;;;#############################################################
(define (start-segment seg)
  (begin
    (car seg)
    ))

;;;#############################################################
;;;#############################################################
(define (end-segment seg)
  (begin
    (cdr seg)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-outline-painter frame)
  (begin
    (let ((origin (get-frame-origin frame))
          (edge1 (get-frame-edge1 frame))
          (edge2 (get-frame-edge2 frame)))
      (let ((seg1 (make-segment edge1 origin))
            (seg2 (make-segment edge1 (add-vect edge1 edge2)))
            (seg3 (sub-vect edge2 (add-vect edge1 edge2)))
            (seg4 (make-segment edge2 origin)))
        (begin
          (segments->painter (list seg1 seg2 seg3 seg4))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (frame-x-painter frame)
  (begin
    (let ((origin (get-frame-origin frame))
          (edge1 (get-frame-edge1 frame))
          (edge2 (get-frame-edge2 frame)))
      (let ((seg1 (make-segment origin (add-vect edge1 edge2)))
            (seg2 (make-segment edge1 edge2)))
        (begin
          (segments->painter (list seg1 seg2))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (frame-diamond-painter frame)
  (begin
    (let ((origin (get-frame-origin frame))
          (edge1 (get-frame-edge1 frame))
          (edge2 (get-frame-edge2 frame)))
      (let ((midpoint1 (scale-vect 0.50 edge1))
            (midpoint2 (scale-vect 0.50 edge2)))
        (let ((midpoint3 (add-vect edge1 midpoint2))
              (midpoint4 (add-vect edge2 midpoint1)))
          (let ((seg1 (make-segment midpoint1 midpoint3))
                (seg2 (make-segment midpoint3 midpoint4))
                (seg3 (make-segment midpoint4 midpoint2))
                (seg4 (make-segment midpoint2 midpoint1)))
            (begin
              (segments->painter (list seg1 seg2 seg3 seg4))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (wave-painter frame)
  (begin
    (let ((origin (get-frame-origin frame))
          (edge1 (get-frame-edge1 frame))
          (edge2 (get-frame-edge2 frame)))
      (let ((pt-01 (scale-vect 0.60 edge2))
            (pt-02 (add-vect (scale-vect 0.150 edge1) (scale-vect 0.45 edge2)))
            (pt-03 (add-vect (scale-vect 0.250 edge1) (scale-vect 0.55 edge2)))
            (pt-04 (add-vect (scale-vect 0.280 edge1) (scale-vect 0.52 edge2)))
            (pt-05 (scale-vect 0.250 edge1)))
        (let ((line-list-1
               (list (make-segment pt-01 pt-02) (make-segment pt-02 pt-03)
                     (make-segment pt-03 pt-04) (make-segment pt-04 pt-05)))
              (pt-11 (scale-vect 0.35 edge1))
              (pt-12 (add-vect (scale-vect 0.50 edge1) (scale-vect 0.30 edge2)))
              (pt-13 (scale-vect 0.65 edge1)))
          (let ((line-list-2
                 (list (make-segment pt-11 pt-12) (make-segment pt-12 pt-13)))
                (pt-21 (scale-vect 0.75 edge1))
                (pt-22 (add-vect (scale-vect 0.67 edge1) (scale-vect 0.47 edge2)))
                (pt-23 (add-vect edge1 (scale-vect 0.35 edge2))))
            (let ((line-list-3
                   (list (make-segment pt-21 pt-22) (make-segment pt-22 pt-23)))
                  (pt-31 (add-vect pt-23 (scale-vect 0.10 edge2)))
                  (pt-32 (add-vect (scale-vect 0.75 edge1) (scale-vect 0.65 edge2)))
                  (pt-33 (add-vect (scale-vect 0.67 edge1) (scale-vect 0.65 edge2)))
                  (pt-34 (add-vect (scale-vect 0.70 edge1) (scale-vect 0.80 edge2)))
                  (pt-35 (add-vect (scale-vect 0.65 edge1) edge2)))
              (let ((line-list-4
                     (list (make-segment pt-31 pt-32) (make-segment pt-32 pt-33)
                           (make-segment pt-33 pt-34) (make-segment pt-34 pt-35)))
                    (pt-41 (add-vect (scale-vect 0.30 edge1) edge2))
                    (pt-42 (add-vect (scale-vect 0.27 edge1) (scale-vect 0.80 edge2)))
                    (pt-43 (add-vect (scale-vect 0.30 edge1) (scale-vect 0.65 edge2)))
                    (pt-44 (add-vect (scale-vect 0.25 edge1) (scale-vect 0.65 edge2)))
                    (pt-45 (add-vect (scale-vect 0.15 edge1) (scale-vect 0.57 edge2)))
                    (pt-46 (scale-vect 0.75 edge2)))
                (let ((line-list-5
                       (list (make-segment pt-41 pt-42) (make-segment pt-42 pt-43)
                             (make-segment pt-43 pt-44) (make-segment pt-44 pt-45)
                             (make-segment pt-45 pt-46))))
                  (let ((sp-func-1 (segments->painter line-list-1))
                        (sp-func-2 (segments->painter line-list-2))
                        (sp-func-3 (segments->painter line-list-3))
                        (sp-func-4 (segments->painter line-list-4))
                        (sp-func-5 (segments->painter line-list-5)))
                    (begin
                      (lambda (frame)
                        (begin
                          (sp-func-1 (frame))
                          (sp-func-2 (frame))
                          (sp-func-3 (frame))
                          (sp-func-4 (frame))
                          (sp-func-5 (frame))
                          ))
                      ))
                  ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Use segments->painter to define the "))
    (display
     (format #f "following primitive~%"))
    (display
     (format #f "painters:~%"))
    (display
     (format #f "a.  The painter that draws the outline "))
    (display
     (format #f "of the designated~%"))
    (display
     (format #f "frame.~%"))
    (display
     (format #f "b.  The painter that draws an \"X\" "))
    (display
     (format #f "by connecting opposite~%"))
    (display
     (format #f "corners of the frame.~%"))
    (display
     (format #f "c.  The painter that draws a diamond "))
    (display
     (format #f "shape by connecting~%"))
    (display
     (format #f "the midpoints of the sides of the "))
    (display
     (format #f "frame.~%"))
    (display
     (format #f "d.  The wave painter.~%"))
    (newline)
    (display
     (format #f "(a) outline painter~%"))
    (display
     (format #f "(define (frame-outline-painter frame)~%"))
    (display
     (format #f "  (let ((origin (get-frame-origin "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge1 (get-frame-edge1 "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge2 (get-frame-edge2 "))
    (display
     (format #f "frame)))~%"))
    (display
     (format #f "    (let ((seg1 (make-segment edge1 "))
    (display
     (format #f "origin))~%"))
    (display
     (format #f "          (seg2 (make-segment edge1 "))
    (display
     (format #f "(add-vect edge1 edge2)))~%"))
    (display
     (format #f "          (seg3 (sub-vect edge2 "))
    (display
     (format #f "(add-vect edge1 edge2)))~%"))
    (display
     (format #f "          (seg4 (make-segment "))
    (display
     (format #f "edge2 origin)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (segments->painter "))
    (display
     (format #f "(list seg1 seg2 seg3 seg4))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     ))~%"))
    (newline)
    (display
     (format #f "(b) x painter~%"))
    (display
     (format #f "(define (frame-x-painter frame)~%"))
    (display
     (format #f "  (let ((origin (get-frame-origin "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge1 (get-frame-edge1 "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge2 (get-frame-edge2 "))
    (display
     (format #f "frame)))~%"))
    (display
     (format #f "    (let ((seg1 (make-segment "))
    (display
     (format #f "origin (add-vec edge1 edge2)))~%"))
    (display
     (format #f "          (seg2 (make-segment "))
    (display
     (format #f "edge1 edge2)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (segments->painter "))
    (display
     (format #f "(list seg1 seg2))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "(c) diamond painter~%"))
    (display
     (format #f "(define (frame-diamond-painter "))
    (display
     (format #f "frame)~%"))
    (display
     (format #f "  (let ((origin (get-frame-origin "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge1 (get-frame-edge1 "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge2 (get-frame-edge2 "))
    (display
     (format #f "frame)))~%"))
    (display
     (format #f "    (let ((midpoint1 (scale-vect "))
    (display
     (format #f "edge1 2))~%"))
    (display
     (format #f "          (midpoint2 (scale-vect "))
    (display
     (format #f "edge2 2)))~%"))
    (display
     (format #f "      (let ((midpoint3 (add-vect "))
    (display
     (format #f "edge1 midpoint2))~%"))
    (display
     (format #f "            (midpoint4 (add-vect "))
    (display
     (format #f "edge2 midpoint1)))~%"))
    (display
     (format #f "        (let ((seg1 (make-segment "))
    (display
     (format #f "midpoint1 midpoint3))~%"))
    (display
     (format #f "              (seg2 (make-segment "))
    (display
     (format #f "midpoint3 midpoint4))~%"))
    (display
     (format #f "              (seg3 (make-segment "))
    (display
     (format #f "midpoint4 midpoint2))~%"))
    (display
     (format #f "              (seg4 (make-segment "))
    (display
     (format #f "midpoint2 midpoint1)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (segments->painter "))
    (display
     (format #f "(list seg1 seg2 seg3 seg4))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "(d) wave painter~%"))
    (display
     (format #f "(define (wave-painter frame)~%"))
    (display
     (format #f "  (let ((origin (get-frame-origin "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge1 (get-frame-edge1 "))
    (display
     (format #f "frame))~%"))
    (display
     (format #f "        (edge2 (get-frame-edge2 "))
    (display
     (format #f "frame)))~%"))
    (display
     (format #f "    (let ((pt-01 (scale-vect 0.60 "))
    (display
     (format #f "edge1))~%"))
    (display
     (format #f "          (pt-02 (add-vect "))
    (display
     (format #f "(scale-vect 0.15 edge1)~%"))
    (display
     (format #f "             (scale-vect 0.45 edge2)))~%"))
    (display
     (format #f "          (pt-03 (add-vect "))
    (display
     (format #f "(scale-vect 0.25 edge1)~%"))
    (display
     (format #f "              (scale-vect 0.55 edge2)))~%"))
    (display
     (format #f "          (pt-04 (add-vect "))
    (display
     (format #f "(scale-vect 0.28 edge1)~%"))
    (display
     (format #f "              (scale-vect 0.52 edge2)))~%"))
    (display
     (format #f "          (pt-05 "))
    (display
     (format #f "(scale-vect 0.250 edge1)))~%"))
    (display
     (format #f "      (let ((line-list-1~%"))
    (display
     (format #f "             (list (make-segment "))
    (display
     (format #f "pt-01 pt-02)~%"))
    (display
     (format #f "          (make-segment pt-02 pt-03)~%"))
    (display
     (format #f "                   (make-segment "))
    (display
     (format #f "pt-03 pt-04)~%"))
    (display
     (format #f "          (make-segment pt-04 pt-05)))~%"))
    (display
     (format #f "            (pt-11 "))
    (display
     (format #f "(scale-vect 0.35 edge1))~%"))
    (display
     (format #f "            (pt-12 "))
    (display
     (format #f "(add-vect (scale-vect 0.50 edge1)~%"))
    (display
     (format #f "        (scale-vect 0.30 edge2)))~%"))
    (display
     (format #f "            (pt-13 "))
    (display
     (format #f "(scale-vect 0.65 edge1)))~%"))
    (display
     (format #f "        (let ((line-list-2~%"))
    (display
     (format #f "               (list "))
    (display
     (format #f "(make-segment pt-11 pt-12)~%"))
    (display
     (format #f "      (make-segment pt-12 pt-13)))~%"))
    (display
     (format #f "              (pt-21 "))
    (display
     (format #f "(scale-vect 0.75 edge1))~%"))
    (display
     (format #f "              (pt-22 "))
    (display
     (format #f "(add-vect (scale-vect 0.67 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.47 edge2)))~%"))
    (display
     (format #f "              (pt-23 (add-vect "))
    (display
     (format #f "edge1 (scale-vect 0.35 edge2))))~%"))
    (display
     (format #f "          (let ((line-list-3~%"))
    (display
     (format #f "                 (list "))
    (display
     (format #f "(make-segment pt-21 pt-22)~%"))
    (display
     (format #f "      (make-segment pt-22 pt-23)))~%"))
    (display
     (format #f "                (pt-31 (add-vect "))
    (display
     (format #f "pt-23 (scale-vect 0.10 edge2)))~%"))
    (display
     (format #f "                (pt-32 (add-vect "))
    (display
     (format #f "(scale-vect 0.75 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                (pt-33 (add-vect "))
    (display
     (format #f "(scale-vect 0.67 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                (pt-34 (add-vect "))
    (display
     (format #f "(scale-vect 0.70 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.80 edge2)))~%"))
    (display
     (format #f "                (pt-35 (add-vect "))
    (display
     (format #f "(scale-vect 0.65 edge1) edge2)))~%"))
    (display
     (format #f "            (let ((line-list-4~%"))
    (display
     (format #f "                   (list (make-segment "))
    (display
     (format #f "pt-31 pt-32)~%"))
    (display
     (format #f "      (make-segment pt-32 pt-33)~%"))
    (display
     (format #f "                         (make-segment "))
    (display
     (format #f "pt-33 pt-34)~%"))
    (display
     (format #f "      (make-segment pt-34 pt-35)))~%"))
    (display
     (format #f "                  (pt-41 (add-vect "))
    (display
     (format #f "(scale-vect 0.30 edge1) edge2))~%"))
    (display
     (format #f "                  (pt-42 (add-vect "))
    (display
     (format #f "(scale-vect 0.27 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.80 edge2)))~%"))
    (display
     (format #f "                  (pt-43 (add-vect "))
    (display
     (format #f "(scale-vect 0.30 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                  (pt-44 (add-vect "))
    (display
     (format #f "(scale-vect 0.25 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.65 edge2)))~%"))
    (display
     (format #f "                  (pt-45 (add-vect "))
    (display
     (format #f "(scale-vect 0.15 edge1)~%"))
    (display
     (format #f "      (scale-vect 0.57 edge2)))~%"))
    (display
     (format #f "                  (pt-46 "))
    (display
     (format #f "(scale-vect 0.75 edge2)))~%"))
    (display
     (format #f "              (let ((line-list-5~%"))
    (display
     (format #f "                     (list "))
    (display
     (format #f "(make-segment pt-41 pt-42)~%"))
    (display
     (format #f "      (make-segment pt-42 pt-43)~%"))
    (display
     (format #f "                           (make-segment "))
    (display
     (format #f "pt-43 pt-44)~%"))
    (display
     (format #f "      (make-segment pt-44 pt-45)~%"))
    (display
     (format #f "                           (make-segment "))
    (display
     (format #f "pt-45 pt-46))))~%"))
    (display
     (format #f "                (let ((sp-func-1 "))
    (display
     (format #f "(segments->painter line-list-1))~%"))
    (display
     (format #f "                      (sp-func-2 "))
    (display
     (format #f "(segments->painter line-list-2))~%"))
    (display
     (format #f "                      (sp-func-3 "))
    (display
     (format #f "(segments->painter line-list-3))~%"))
    (display
     (format #f "                      (sp-func-4 "))
    (display
     (format #f "(segments->painter line-list-4))~%"))
    (display
     (format #f "                      (sp-func-5 "))
    (display
     (format #f "(segments->painter line-list-5)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (lambda (frame)~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (sp-func-1 "))
    (display
     (format #f "(frame))~%"))
    (display
     (format #f "                        (sp-func-2 "))
    (display
     (format #f "(frame))~%"))
    (display
     (format #f "                        (sp-func-3 "))
    (display
     (format #f "(frame))~%"))
    (display
     (format #f "                        (sp-func-4 "))
    (display
     (format #f "(frame))~%"))
    (display
     (format #f "                        (sp-func-5 "))
    (display
     (format #f "(frame))~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                    ))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "       ))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.49 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
