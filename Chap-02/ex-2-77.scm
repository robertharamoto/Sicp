#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.77                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 13, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (apply-generic op . args)
  (begin
    (let ((type-tags-list (map type-tag args)))
      (let ((proc (get op type-tags-list)))
        (begin
          (if proc
              (begin
                (apply proc (map contents args)))
              (begin
                (error
                 "No method for these types -- APPLY-GENERIC"
                 (list op type-tags))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner tries to evaluate the "))
    (display
     (format #f "expression (magnitude z)~%"))
    (display
     (format #f "where z is the object shown in figure "))
    (display
     (format #f "2.24. To his surprise,~%"))
    (display
     (format #f "instead of the answer 5 he gets an "))
    (display
     (format #f "error message from~%"))
    (display
     (format #f "apply-generic, saying there is no "))
    (display
     (format #f "method for the operation~%"))
    (display
     (format #f "magnitude on the types (complex). He "))
    (display
     (format #f "shows this interaction~%"))
    (display
     (format #f "to Alyssa P. Hacker, who says \"The "))
    (display
     (format #f "problem is that the~%"))
    (display
     (format #f "complex-number selectors were never "))
    (display
     (format #f "defined for complex~%"))
    (display
     (format #f "numbers, just for polar and rectangular "))
    (display
     (format #f "numbers. All you~%"))
    (display
     (format #f "have to do to make this work is add the "))
    (display
     (format #f "following to the ~%"))
    (display
     (format #f "complex package:\"~%"))
    (display
     (format #f "(put 'real-part '(complex) real-part)~%"))
    (display
     (format #f "(put 'imag-part '(complex) imag-part)~%"))
    (display
     (format #f "(put 'magnitude '(complex) magnitude)~%"))
    (display
     (format #f "(put 'angle '(complex) angle)~%"))
    (display
     (format #f "Describe in detail why this works. "))
    (display
     (format #f "As an example,~%"))
    (display
     (format #f "trace through all the procedures "))
    (display
     (format #f "called in evaluating~%"))
    (display
     (format #f "the expression (magnitude z) where z "))
    (display
     (format #f "is the object shown~%"))
    (display
     (format #f "in figure 2.24. In particular, how many "))
    (display
     (format #f "times is apply-generic~%"))
    (display
     (format #f "invoked? What procedure is dispatched "))
    (display
     (format #f "to in each case?~%"))
    (newline)
    (display
     (format #f "(define z "))
    (display
     (format #f "(make-complex-from-real-imag 3 4))~%"))
    (display
     (format #f "z -> (list 'complex 'rectangular 3 4)~%"))
    (display
     (format #f "(define (magnitude z) "))
    (display
     (format #f "(apply-generic 'magnitude z))~%"))
    (display
     (format #f "  type-tags = (list 'complex)~%"))
    (display
     (format #f "  proc = (get 'magnitude 'complex)~%"))
    (display
     (format #f "  (apply proc (contents "))
    (display
     (format #f "(cons 'rectangular (cons 3 4))))~%"))
    (display
     (format #f "  (apply proc (cons 3 4))~%"))
    (newline)
    (display
     (format #f "The procedure apply-generic is called "))
    (display
     (format #f "once in evaluating~%"))
    (display
     (format #f "(magnitude z), where proc = magnitude "))
    (display
     (format #f "from the~%"))
    (display
     (format #f "install-rectangular-package, within "))
    (display
     (format #f "the~%"))
    (display
     (format #f "install-complex-package.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.77 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
