#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 2.59                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 12, 2022                              ###
;;;###                                                       ###
;;;###  updated February 23, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-to-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (union-set set-1 set-2)
  (define (local-iter set-a acc-set)
    (begin
      (if (or (null? set-a)
              (<= (length set-a) 0))
          (begin
            acc-set)
          (begin
            (let ((a-elem (car set-a))
                  (a-tail (cdr set-a)))
              (let ((a-in-b (member a-elem acc-set)))
                (begin
                  (if (equal? a-in-b #f)
                      (begin
                        (local-iter
                         a-tail (cons a-elem acc-set)))
                      (begin
                        (local-iter a-tail acc-set)
                        ))
                  )))
            ))
      ))
  (begin
    (local-iter set-1 (list-copy set-2))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-union-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-union-set-1")
         (test-list
          (list
           (list (list 'a 'b) (list 'c 'd) (list 'a 'b 'c 'd))
           (list (list 'a 'b 'c) (list 'c 'd) (list 'a 'b 'c 'd))
           (list (list 'a 'b 'c) (list 'c 'd 'e) (list 'a 'b 'c 'd 'e))
           (list (list 1 2) (list 2 3 4) (list 1 2 3 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-1 (list-ref this-list 0))
                  (set-2 (list-ref this-list 1))
                  (shouldbe-set (list-ref this-list 2)))
              (let ((result-set (union-set set-1 set-2)))
                (let ((slen (length shouldbe-set))
                      (rlen (length result-set)))
                  (let ((err-msg-1
                         (format
                          #f "~a : error (~a) : set-1=~a, set-2=~a, "
                          sub-name test-label-index
                          set-1 set-2))
                        (err-msg-2
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-set result-set))
                        (err-msg-3
                         (format
                          #f "length shouldbe=~a, result=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2 err-msg-3)
                       result-hash-table)

                      (do ((ii 0 (1+ ii)))
                          ((>= ii slen))
                        (begin
                          (let ((s-elem (list-ref shouldbe-set ii)))
                            (let ((sflag (member s-elem result-set))
                                  (err-msg-4
                                   (format
                                    #f ", missing element=~a"
                                    s-elem)))
                              (begin
                                (unittest2:assert?
                                 (equal? slen rlen)
                                 sub-name
                                 (string-append
                                  err-msg-1 err-msg-2 err-msg-4)
                                 result-hash-table)
                                )))
                          ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Implement the union-set operation for "))
    (display
     (format #f "the unordered-list~%"))
    (display
     (format #f "representation of sets.~%"))
    (newline)
    (display
     (format #f "(define (union-set set-1 set-2)~%"))
    (display
     (format #f "  (define (local-iter set-a acc-set)~%"))
    (display
     (format #f "    (if (or (null? set-a)~%"))
    (display
     (format #f "            (<= (length set-a) 0))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          acc-set)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((a-elem (car set-a))~%"))
    (display
     (format #f "                (a-tail (cdr set-a)))~%"))
    (display
     (format #f "            (let ((a-in-b (member "))
    (display
     (format #f "a-elem acc-set)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (equal? a-in-b #f)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (local-iter~%"))
    (display
     (format #f "                       a-tail "))
    (display
     (format #f "(cons a-elem acc-set)))~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (local-iter "))
    (display
     (format #f "a-tail acc-set)~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (local-iter set-1 "))
    (display
     (format #f "(list-copy set-2))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 2.59 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
