#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.65                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (average x y)
  (begin
    (* 0.50 (+ x y))
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-improve guess x)
  (begin
    (average guess (/ x guess))
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-stream x)
  (define guesses
    (begin
      (srfi-41:stream-cons
       1.0
       (srfi-41:stream-map
        (lambda (guess)
          (begin
            (display (format #f "debug guess=~a~%" guess))
            (force-output)
            (sqrt-improve guess x)))
        guesses))
      ))
  (begin
    guesses
    ))

;;;#############################################################
;;;#############################################################
(define (stream-limit astream tolerance)
  (define (local-rec astream prev-val count tol)
    (begin
      (if (srfi-41:stream-null? astream)
          (begin
            (list prev-val count))
          (begin
            (let ((a0 (srfi-41:stream-car astream))
                  (tail-stream (srfi-41:stream-cdr astream)))
              (let ((diff (abs (- a0 prev-val))))
                (begin
                  (if (< diff tol)
                      (begin
                        (list a0 count))
                      (begin
                        (local-rec tail-stream a0 (1+ count) tol)
                        ))
                  )))
            ))
      ))
  (begin
    (local-rec (srfi-41:stream-cdr astream)
               (srfi-41:stream-car astream)
               0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (ln2-stream n)
  (begin
    (srfi-41:stream-cons
     (/ 1.0 n)
     (srfi-41:stream-map - (ln2-stream (+ n 1.0))))
    ))

;;;#############################################################
;;;#############################################################
(define ln2-stream-v0
  (partial-sums (ln2-stream 1.0)))

;;;#############################################################
;;;#############################################################
(define (square x)
  (begin
    (* x x)
    ))

;;;#############################################################
;;;#############################################################
(define (euler-transform s)
  (begin
    (let ((s0 (my-stream-ref s 0))           ; Sn-1
          (s1 (my-stream-ref s 1))           ; Sn
          (s2 (my-stream-ref s 2)))          ; Sn+1
      (begin
        (srfi-41:stream-cons
         (- s2 (/ (square (- s2 s1))
                  (+ s0 (* -2 s1) s2)))
         (euler-transform (srfi-41:stream-cdr s)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define ln2-stream-v1
  (euler-transform ln2-stream-v0))

;;;#############################################################
;;;#############################################################
(define (make-tableau transform s)
  (begin
    (srfi-41:stream-cons
     s (make-tableau transform
                     (transform s)))
    ))

;;;#############################################################
;;;#############################################################
(define (accelerated-sequence transform s)
  (begin
    (srfi-41:stream-map
     srfi-41:stream-car
     (make-tableau transform s))
    ))

;;;#############################################################
;;;#############################################################
(define ln2-stream-v2
  (accelerated-sequence euler-transform ln2-stream-v0))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Use the series:~%"))
    (newline)
    (display
     (format #f "ln 2 = 1 - 1/2 + 1/3 - 1/4 "))
    (display
     (format #f "+ 1/5 - ...~%"))
    (newline)
    (display
     (format #f "to compute three sequences of "))
    (display
     (format #f "approximations to the~%"))
    (display
     (format #f "natural logarithm of 2, in the same way "))
    (display
     (format #f "we did above~%"))
    (display
     (format #f "for pi. How rapidly do these sequences "))
    (display
     (format #f "converge?~%"))
    (newline)
    (display
     (format #f "The tableau method converges the fastest, "))
    (display
     (format #f "the euler~%"))
    (display
     (format #f "transform converges the next fastest, "))
    (display
     (format #f "while the straight~%"))
    (display
     (format #f "sum took about 1,000 iterations to get "))
    (display
     (format #f "10^(-3) accuracy.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display (format #f "ln(2) = 0.693147181...~%"))
    (let ((tol-list (list 1e-2 1e-3))
          (ln-list
           (reverse (list ln2-stream-v0 ln2-stream-v1 ln2-stream-v2)))
          (ln-desc-list
           (reverse (list "sum" "euler transform" "tableau"))))
      (let ((nmax (length ln-list)))
        (begin
          (for-each
           (lambda (tol)
             (begin
               (do ((ii 0 (1+ ii)))
                   ((>= ii nmax))
                 (begin
                   (let ((astream (list-ref ln-list ii))
                         (desc (list-ref ln-desc-list ii)))
                     (let ((val-list (stream-limit astream tol)))
                       (begin
                         (display
                          (ice-9-format:format
                           #f "(stream-limit (~a) ~a) = "
                           desc tol))
                         (display
                          (ice-9-format:format
                           #f "~9,6f : niters = ~:d~%"
                           (car val-list) (cadr val-list)))
                         (force-output)
                         )))
                   ))
               (newline)
               )) tol-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.65 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
