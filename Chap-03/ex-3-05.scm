#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.05                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (random-in-range low high)
  (begin
    (let ((range (- high low)))
      (begin
        (+ low (* (random:uniform) range))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (unit-circle-pred xx yy)
  (begin
    (< (+ (* xx xx) (* yy yy)) 1)
    ))

;;;#############################################################
;;;#############################################################
(define (estimate-integral pred x1 x2 y1 y2 ntrials)
  (define (predicate-test)
    (begin
      (let ((xx (random-in-range x1 x2))
            (yy (random-in-range y1 y2)))
        (begin
          (pred xx yy)
          ))
      ))
  (begin
    (* (- x2 x1) (- y2 y1)
       (monte-carlo ntrials predicate-test))
    ))

;;;#############################################################
;;;#############################################################
(define (monte-carlo trials experiment)
  (define (local-iter trials-remaining trials-passed)
    (begin
      (cond
       ((= trials-remaining 0)
        (begin
          (/ trials-passed trials)
          ))
       ((experiment)
        (begin
          (local-iter
           (- trials-remaining 1) (+ trials-passed 1))
          ))
       (else
        (begin
          (local-iter
           (- trials-remaining 1) trials-passed)
          )))
      ))
  (begin
    (local-iter trials 0)
    ))

;;;#############################################################
;;;#############################################################
(define (pi-via-unit-circle-estimate ntrials)
  (begin
    (set! *random-state* (random-state-from-platform))

    (let ((pi-est
           (estimate-integral
            unit-circle-pred -1 1 -1 1 ntrials)))
      (begin
        (exact->inexact pi-est)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Monte Carlo integration is a method of "))
    (display
     (format #f "estimating definite~%"))
    (display
     (format #f "integrals by means of Monte Carlo "))
    (display
     (format #f "simulation. Consider~%"))
    (display
     (format #f "computing the area of a region of space "))
    (display
     (format #f "described by a~%"))
    (display
     (format #f "predicate P(x, y) that is true for "))
    (display
     (format #f "points (x, y) in~%"))
    (display
     (format #f "the region and false for points not in "))
    (display
     (format #f "the region. For~%"))
    (display
     (format #f "example, the region contained within a "))
    (display
     (format #f "circle of radius~%"))
    (display
     (format #f "3 centered at (5, 7) is described by "))
    (display
     (format #f "the predicate that~%"))
    (display
     (format #f "tests whether (x - 5)2 + (y - 7)2< 32. To "))
    (display
     (format #f "estimate the area~%"))
    (display
     (format #f "of the region described by such a "))
    (display
     (format #f "predicate, begin by~%"))
    (display
     (format #f "choosing a rectangle that contains the "))
    (display
     (format #f "region. For example,~%"))
    (display
     (format #f "a rectangle with diagonally opposite "))
    (display
     (format #f "corners at (2, 4)~%"))
    (display
     (format #f "and (8, 10) contains the circle above. "))
    (display
     (format #f "The desired integral~%"))
    (display
     (format #f "is the area of that portion of the "))
    (display
     (format #f "rectangle that lies~%"))
    (display
     (format #f "in the region. We can estimate the "))
    (display
     (format #f "integral by picking,~%"))
    (display
     (format #f "at random, points (x, y) that lie "))
    (display
     (format #f "in the rectangle,~%"))
    (display
     (format #f "and testing P(x, y) for each point "))
    (display
     (format #f "to determine whether~%"))
    (display
     (format #f "the point lies in the region. If we "))
    (display
     (format #f "try this with~%"))
    (display
     (format #f "many points, then the fraction of "))
    (display
     (format #f "points that fall~%"))
    (display
     (format #f "in the region should give an estimate "))
    (display
     (format #f "of the proportion~%"))
    (display
     (format #f "of the rectangle that lies in the "))
    (display
     (format #f "region. Hence,~%"))
    (display
     (format #f "multiplying this fraction by the area "))
    (display
     (format #f "of the entire~%"))
    (display
     (format #f "rectangle should produce an estimate of "))
    (display
     (format #f "the integral.~%"))
    (newline)
    (display
     (format #f "Implement Monte Carlo integration as a "))
    (display
     (format #f "procedure~%"))
    (display
     (format #f "estimate-integral that takes as "))
    (display
     (format #f "arguments a predicate~%"))
    (display
     (format #f "P, upper and lower bounds x1, x2, y1, "))
    (display
     (format #f "and y2 for~%"))
    (display
     (format #f "the rectangle, and the number of trials "))
    (display
     (format #f "to perform in~%"))
    (display
     (format #f "order to produce the estimate. Your "))
    (display
     (format #f "procedure should use~%"))
    (display
     (format #f "the same monte-carlo procedure that was "))
    (display
     (format #f "used above to~%"))
    (display
     (format #f "estimate . Use your estimate-integral to "))
    (display
     (format #f "produce an estimate~%"))
    (display
     (format #f "by measuring the area of a unit circle. "))
    (display
     (format #f "You will find~%"))
    (display
     (format #f "it useful to have a procedure that returns "))
    (display
     (format #f "a number chosen~%"))
    (display
     (format #f "at random from a given range. The following "))
    (display
     (format #f "random-in-range~%"))
    (display
     (format #f "procedure implements this in terms of the "))
    (display
     (format #f "random procedure~%"))
    (display
     (format #f "used in section 1.2.6, which returns a "))
    (display
     (format #f "non-negative number~%"))
    (display
     (format #f "less than its input.~%"))
    (newline)
    (display
     (format #f "(define (random-in-range low high)~%"))
    (display
     (format #f "  (let ((range (- high low)))~%"))
    (display
     (format #f "    (+ low (random range))))~%"))
    (newline)
    (display
     (format #f "(define (unit-circle-pred xx yy)~%"))
    (display
     (format #f "  (< (+ (* xx xx) (* yy yy)) 1))~%"))
    (display
     (format #f "(define (estimate-integral "))
    (display
     (format #f "pred x1 x2 y1 y2 ntrials)~%"))
    (display
     (format #f "  (define (predicate-test)~%"))
    (display
     (format #f "    (let ((xx (random-in-range "))
    (display
     (format #f "x1 x2))~%"))
    (display
     (format #f "          (yy (random-in-range "))
    (display
     (format #f "y1 y2)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (pred xx yy)~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (* (- x2 x1) (- y2 y1)~%"))
    (display
     (format #f "     (monte-carlo "))
    (display
     (format #f "ntrials predicate-test)))~%"))
    (display
     (format #f "(define (monte-carlo trials experiment)~%"))
    (display
     (format #f "  (define (iter "))
    (display
     (format #f "trials-remaining trials-passed)~%"))
    (display
     (format #f "    (cond ((= trials-remaining 0)~%"))
    (display
     (format #f "           (/ trials-passed trials))~%"))
    (display
     (format #f "          ((experiment)~%"))
    (display
     (format #f "           (iter (- trials-remaining 1) "))
    (display
     (format #f "(+ trials-passed 1)))~%"))
    (display
     (format #f "          (else~%"))
    (display
     (format #f "           (iter (- trials-remaining 1) "))
    (display
     (format #f "trials-passed))))~%"))
    (display
     (format #f "  (iter trials 0))~%"))
    (display
     (format #f "(define (pi-via-unit-circle-estimate "))
    (display
     (format #f "ntrials)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "      (set! *random-state* "))
    (display
     (format #f "(random-state-from-platform))~%"))
    (display
     (format #f "      (let ((pi-est~%"))
    (display
     (format #f "             (estimate-integral~%"))
    (display
     (format #f "              unit-circle-pred "))
    (display
     (format #f "-1 1 -1 1 ntrials)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          pi-est~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list 100 1000 10000 100000)))
      (begin
        (for-each
         (lambda (ntrials)
           (begin
             (let ((pi-est
                    (pi-via-unit-circle-estimate ntrials)))
               (begin
                 (display

                  (ice-9-format:format
                   #f "ntrials = ~:d  :  " ntrials))
                 (force-output)
                 (display

                  (format #f "pi = ~a~%" pi-est))
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.05 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
