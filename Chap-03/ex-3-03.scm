#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.03                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-account pwd init-bal)
  (define balance init-bal)
  (define password pwd)
  (define (withdraw amount)
    (begin
      (if (>= balance amount)
          (begin (set! balance (- balance amount))
                 balance)
          (begin
            "Insufficient funds"
            ))
      ))
  (define (deposit amount)
    (begin
      (set! balance (+ balance amount))
      balance
      ))
  (define (dispatch secret-pwd m)
    (begin
      (if (and (symbol? secret-pwd)
               (eq? password secret-pwd))
          (begin
            (cond
             ((eq? m 'withdraw)
              (begin
                withdraw
                ))
             ((eq? m 'deposit)
              (begin
                deposit
                ))
             (else
              (begin
                (display
                 (format
                  #f "Unknown request -- MAKE-ACCOUNT ~a~%"
                  m))
                (quit)
                ))
             ))
          (begin
            (lambda (anum)
              "incorrect password")
            ))
      ))
  (begin
    dispatch
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Modify the make-account procedure so "))
    (display
     (format #f "that it creates~%"))
    (display
     (format #f "password-protected accounts. That is, "))
    (display
     (format #f "make-account should~%"))
    (display
     (format #f "take a symbol as an additional "))
    (display
     (format #f "argument, as in:~%"))
    (display
     (format #f "(define acc (make-account 100 'secret-password))~%"))
    (display
     (format #f "The resulting account object should process "))
    (display
     (format #f "a request only~%"))
    (display
     (format #f "if it is accompanied by the password with "))
    (display
     (format #f "which the account~%"))
    (display
     (format #f "was created, and should otherwise return "))
    (display
     (format #f "a complaint:~%"))
    (display
     (format #f "((acc 'some-other-password 'deposit) 50)~%"))
    (display
     (format #f "\"Incorrect password\"~%"))
    (newline)
    (display
     (format #f "(define (make-account pwd init-bal)~%"))
    (display
     (format #f "  (define balance init-bal)~%"))
    (display
     (format #f "  (define password pwd)~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (if (>= balance amount)~%"))
    (display
     (format #f "        (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "               balance)~%"))
    (display
     (format #f "        \"Insufficient funds\"))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (set! balance (+ balance amount))~%"))
    (display
     (format #f "    balance)~%"))
    (display
     (format #f "  (define (dispatch secret-pwd m)~%"))
    (display
     (format #f "    (if (and (symbol? secret-pwd)~%"))
    (display
     (format #f "             (eq? password secret-pwd))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (cond ((eq? m 'withdraw) withdraw)~%"))
    (display
     (format #f "                ((eq? m 'deposit) deposit)~%"))
    (display
     (format #f "                (else~%"))
    (display
     (format #f "                 (display~%"))
    (display
     (format #f "                   (format #f "))
    (display
     (format #f "\"Unknown request -- "))
    (display
     (format #f "MAKE-ACCOUNT ~~a~~%\"~%"))
    (display
     (format #f "                        m))~%"))
    (display
     (format #f "                 (quit))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (lambda (anum)~%"))
    (display
     (format #f "            \"incorrect password\")~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  dispatch)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((acc (make-account 'secret 100)))
      (begin
        (display
         (format
          #f "(define acc (make-account 'secret 100))~%"))
        (display
         (format #f "((acc 'secret 'withdraw) 0) = ~a~%"
                 ((acc 'secret 'withdraw) 0)))
        (newline)
        (display
         (format #f "((acc 'secret 'withdraw) 10) = ~a~%"
                 ((acc 'secret 'withdraw) 10)))
        (newline)
        (display
         (format #f "((acc 'secret 'deposit) 100) = ~a~%"
                 ((acc 'secret 'deposit) 100)))
        (newline)
        (display
         (format #f "((acc 'wrong 'withdraw) 100) = ~a~%"
                 ((acc 'wrong 'withdraw) 10)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.03 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
