#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.23                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (front-ptr deque)
  (begin
    (car deque)
    ))

;;;#############################################################
;;;#############################################################
(define (rear-ptr deque)
  (begin
    (cdr deque)
    ))

;;;#############################################################
;;;#############################################################
(define (set-front-ptr! deque item)
  (begin
    (set-car! deque item)
    ))

;;;#############################################################
;;;#############################################################
(define (set-rear-ptr! deque item)
  (begin
    (set-cdr! deque item)
    ))

;;;#############################################################
;;;#############################################################
(define (empty-deque? deque)
  (begin
    (null? (front-ptr deque))
    ))

;;;#############################################################
;;;#############################################################
(define (make-deque)
  (begin
    (cons (list) (list))
    ))

;;;#############################################################
;;;#############################################################
(define (front-deque deque)
  (begin
    (if (empty-deque? deque)
        (begin
          (error "front-deque called with an empty deque" deque))
        (begin
          (car (front-ptr deque))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (rear-deque deque)
  (begin
    (if (empty-deque? deque)
        (begin
          (error
           "rear-deque called with an empty deque"
           deque))
        (begin
          (car (rear-ptr deque))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (front-insert-deque! deque item)
  (begin
    (let ((new-pair (cons item (list))))
      (begin
        (cond
         ((empty-deque? deque)
          (begin
            (set-front-ptr! deque new-pair)
            (set-rear-ptr! deque new-pair)
            deque
            ))
         (else
          (begin
            (set-cdr! new-pair (front-ptr deque))
            (set-front-ptr! deque new-pair)
            deque
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (rear-insert-deque! deque item)
  (begin
    (let ((new-pair (cons item (list))))
      (begin
        (cond
         ((empty-deque? deque)
          (begin
            (set-front-ptr! deque new-pair)
            (set-rear-ptr! deque new-pair)
            deque
            ))
         (else
          (begin
            (set-cdr! (rear-ptr deque) new-pair)
            (set-rear-ptr! deque new-pair)
            deque
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (front-delete-deque! deque)
  (begin
    (cond
     ((empty-deque? deque)
      (begin
        (error
         "front-delete! called with an empty deque"
         deque)
        ))
     (else
      (begin
        (if (equal? (front-ptr deque) (rear-ptr deque))
            (begin
              (set-front-ptr! deque (list))
              (set-rear-ptr! deque (list)))
            (begin
              (set-front-ptr! deque (cdr (front-ptr deque)))
              (if (null? (front-ptr deque))
                  (begin
                    (set-rear-ptr! deque (list))
                    ))
              ))
        deque
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (rear-delete-deque! deque)
  (define (walk-over-deque curr-deque-data prev-ptr rear-pointer)
    (begin
      (if (or (null? curr-deque-data)
              (equal? curr-deque-data rear-pointer))
          (begin
            prev-ptr)
          (begin
            (let ((tail-list (cdr curr-deque-data)))
              (begin
                (walk-over-deque
                 tail-list
                 curr-deque-data rear-pointer)
                ))
            ))
      ))
  (begin
    (cond
     ((empty-deque? deque)
      (begin
        (error
         "rear-delete! called with an empty deque"
         deque)
        ))
     (else
      (begin
        (if (equal? (front-ptr deque) (rear-ptr deque))
            (begin
              (set-front-ptr! deque (list))
              (set-rear-ptr! deque (list)))
            (begin
              (let ((prev-rear
                     (walk-over-deque
                      (front-ptr deque) (front-ptr deque)
                      (rear-ptr deque)))
                    (rear (rear-ptr deque)))
                (begin
                  (if (null? prev-rear)
                      (begin
                        (set-rear-ptr! deque (list)))
                      (begin
                        (set-cdr! prev-rear (list))
                        (set-rear-ptr! deque prev-rear)
                        ))
                  ))
              ))
        deque
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (print-deque d1)
  (begin
    (if (empty-deque? d1)
        (begin
          (display (format #f "()~%")))
        (begin
          (let ((d-data (front-ptr d1)))
            (begin
              (display
               (format #f "~a~%" d-data))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A deque (\"double-ended queue\") is a "))
    (display
     (format #f "sequence in which items~%"))
    (display
     (format #f "can be inserted and deleted at either "))
    (display
     (format #f "the front or the~%"))
    (display
     (format #f "rear. Operations on deques are the "))
    (display
     (format #f "constructor make-deque,~%"))
    (display
     (format #f "the predicate empty-deque?, selectors "))
    (display
     (format #f "front-deque and~%"))
    (display
     (format #f "rear-deque, and mutators "))
    (display
     (format #f "front-insert-deque!,~%"))
    (display
     (format #f "rear-insert-deque!, "))
    (display
     (format #f "front-delete-deque!, and~%"))
    (display
     (format #f "rear-delete-deque!. Show how to "))
    (display
     (format #f "represent deques using~%"))
    (display
     (format #f "pairs, and give implementations of the "))
    (display
     (format #f "operations. All~%"))
    (display
     (format #f "operations should be accomplished in "))
    (display
     (format #f "theta(1) steps.~%"))
    (newline)
    (display
     (format #f "The deque is defined similar to the "))
    (display
     (format #f "queue, with a~%"))
    (display
     (format #f "front and a rear pointer.~%"))
    (newline)
    (display
     (format #f "(define (make-deque) "))
    (display
     (format #f "(cons (list) (list)))~%"))
    (display
     (format #f "(define (front-ptr deque) (car deque))~%"))
    (display
     (format #f "(define (rear-ptr deque) (cdr deque))~%"))
    (display
     (format #f "(define (set-front-ptr! "))
    (display
     (format #f "deque item) (set-car! deque item))~%"))
    (display
     (format #f "(define (set-rear-ptr! "))
    (display
     (format #f "deque item) (set-cdr! deque item))~%"))
    (display
     (format #f "(define (empty-deque? deque) "))
    (display
     (format #f "(null? (front-ptr deque)))~%"))
    (newline)
    (display
     (format #f "(define (front-deque deque)~%"))
    (display
     (format #f "  (if (empty-deque? deque)~%"))
    (display
     (format #f "      (error~%"))
    (display
     (format #f "         \"front-deque called with "))
    (display
     (format #f "an empty deque\" deque)~%"))
    (display
     (format #f "      (car (front-ptr deque))))~%"))
    (display
     (format #f "(define (rear-deque deque)~%"))
    (display
     (format #f "  (if (empty-deque? deque)~%"))
    (display
     (format #f "      (error~%"))
    (display
     (format #f "        \"rear-deque called with "))
    (display
     (format #f "an empty deque\" deque)~%"))
    (display
     (format #f "      (car (rear-ptr deque))))~%"))
    (display
     (format #f "(define (front-insert-deque! deque item)~%"))
    (display
     (format #f "  (let ((new-pair (cons item (list))))~%"))
    (display
     (format #f "    (cond~%"))
    (display
     (format #f "     ((empty-deque? deque)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (set-front-ptr! deque new-pair)~%"))
    (display
     (format #f "        (set-rear-ptr! deque new-pair)~%"))
    (display
     (format #f "        deque~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     (else~%"))
    (display
     (format #f "      (set-cdr! new-pair (front-ptr deque))~%"))
    (display
     (format #f "      (set-front-ptr! deque new-pair)~%"))
    (display
     (format #f "      deque))))~%"))
    (display
     (format #f "(define (rear-insert-deque! deque item)~%"))
    (display
     (format #f "  (let ((new-pair (cons item (list))))~%"))
    (display
     (format #f "    (cond~%"))
    (display
     (format #f "     ((empty-deque? deque)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (set-front-ptr! deque new-pair)~%"))
    (display
     (format #f "        (set-rear-ptr! deque new-pair)~%"))
    (display
     (format #f "        deque~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     (else~%"))
    (display
     (format #f "      (set-cdr! (rear-ptr deque) new-pair)~%"))
    (display
     (format #f "      (set-rear-ptr! deque new-pair)~%"))
    (display
     (format #f "      deque))))~%"))
    (display
     (format #f "(define (front-delete-deque! deque)~%"))
    (display
     (format #f "  (cond ((empty-deque? deque)~%"))
    (display
     (format #f "         (error~%"))
    (display
     (format #f "           \"front-delete! called "))
    (display
     (format #f "with an empty deque\" deque))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         (if (equal? "))
    (display
     (format #f "(front-ptr deque) (rear-ptr deque))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "               (set-front-ptr! "))
    (display
     (format #f "deque (list))~%"))
    (display
     (format #f "               (set-rear-ptr! "))
    (display
     (format #f "deque (list)))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "               (set-front-ptr! "))
    (display
     (format #f "deque (cdr (front-ptr deque)))~%"))
    (display
     (format #f "               (if (null? "))
    (display
     (format #f "(front-ptr deque))~%"))
    (display
     (format #f "                   (set-rear-ptr! "))
    (display
     (format #f "deque (list)))~%"))
    (display
     (format #f "               ))~%"))
    (display
     (format #f "         deque)))~%"))
    (display
     (format #f "(define (rear-delete-deque! deque)~%"))
    (display
     (format #f "  (define (walk-over-deque~%"))
    (display
     (format #f "      curr-deque-data prev-ptr "))
    (display
     (format #f "rear-pointer)~%"))
    (display
     (format #f "    (if (or (null? curr-deque-data)~%"))
    (display
     (format #f "            (equal? curr-deque-data "))
    (display
     (format #f "rear-pointer))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          prev-ptr)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((tail-list "))
    (display
     (format #f "(cdr curr-deque-data)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (walk-over-deque "))
    (display
     (format #f "tail-list curr-deque-data rear-pointer)~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (cond~%"))
    (display
     (format #f "   ((empty-deque? deque)~%"))
    (display
     (format #f "    (error \"rear-delete! called "))
    (display
     (format #f "with an empty deque\" deque))~%"))
    (display
     (format #f "   (else~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (if (equal? (front-ptr deque) "))
    (display
     (format #f "(rear-ptr deque))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (set-front-ptr! "))
    (display
     (format #f "deque (list))~%"))
    (display
     (format #f "            (set-rear-ptr! "))
    (display
     (format #f "deque (list)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((prev-rear~%"))
    (display
     (format #f "                   (walk-over-deque~%"))
    (display
     (format #f "                    (front-ptr deque) "))
    (display
     (format #f "(front-ptr deque)~%"))
    (display
     (format #f "                    (rear-ptr deque)))~%"))
    (display
     (format #f "                  (rear (rear-ptr "))
    (display
     (format #f "deque)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (null? prev-rear)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (set-rear-ptr! "))
    (display
     (format #f "deque (list)))~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (set-cdr! "))
    (display
     (format #f "prev-rear (list))~%"))
    (display
     (format #f "                      (set-rear-ptr! "))
    (display
     (format #f "deque prev-rear)~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "      deque~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "   ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((d1 (make-deque)))
      (begin
        (display
         (format #f "(define d1 (make-deque))~%"))
        (front-insert-deque! d1 'b)
        (display
         (format #f "(front-insert-deque! d1 'b) -> "))
        (print-deque d1)

        (front-insert-deque! d1 'a)
        (display
         (format #f "(front-insert-deque! d1 'a) -> "))
        (print-deque d1)

        (front-delete-deque! d1)
        (display
         (format #f "(front-delete-deque! d1) -> "))
        (print-deque d1)

        (front-delete-deque! d1)
        (display
         (format #f "(front-delete-deque! d1) -> "))
        (print-deque d1)

        (rear-insert-deque! d1 'c)
        (display
         (format #f "(rear-insert-deque! d1 'c) -> "))
        (print-deque d1)

        (rear-insert-deque! d1 'd)
        (display
         (format #f "(rear-insert-deque! d1 'd) -> "))
        (print-deque d1)

        (rear-delete-deque! d1)
        (display
         (format #f "(rear-delete-deque! d1) -> "))
        (print-deque d1)

        (rear-delete-deque! d1)
        (display
         (format #f "(rear-delete-deque! d1) -> "))
        (print-deque d1)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.23 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
