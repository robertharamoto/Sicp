#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.25                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-table same-key?)
  (begin
    (let ((local-table (list '*table*)))
      (begin
        (define (assoc key records)
          (begin
            (cond
             ((null? records)
              (begin
                #f
                ))
             ((same-key? key (caar records))
              (begin
                (car records)
                ))
             (else
              (begin
                (assoc key (cdr records))
                )))
            ))
        (define (lookup a-key-list)
          (define (local-lookup a-key-list a-local-table)
            (begin
              (if (null? a-key-list)
                  (begin
                    (if a-local-table
                        (begin
                          (cdr a-local-table))
                        (begin
                          #f
                          )))
                  (begin
                    (let ((this-key (car a-key-list))
                          (tail-list (cdr a-key-list)))
                      (let ((subtable
                             (assoc
                              this-key (cdr a-local-table))))
                        (begin
                          (if subtable
                              (begin
                                (local-lookup tail-list subtable))
                              (begin
                                #f
                                ))
                          )))
                    ))
              ))
          (begin
            (local-lookup a-key-list local-table)
            ))
        (define (insert! a-key-list value)
          (define (local-insert! a-key-list value a-local-table)
            (begin
              (if (null? a-key-list)
                  (begin
                    (if a-local-table
                        (begin
                          (set-cdr! a-local-table value))
                        (begin
                          #f
                          )))
                  (begin
                    (let ((this-key (car a-key-list))
                          (tail-list (cdr a-key-list)))
                      (let ((subtable
                             (assoc
                              this-key (cdr a-local-table))))
                        (begin
                          (if subtable
                              (begin
                                (if (null? tail-list)
                                    (begin
                                      (set-cdr! subtable value))
                                    (begin
                                      (local-insert! tail-list value subtable)
                                      )))
                              (begin
                                (let ((next-local-table (list this-key)))
                                  (begin
                                    (set-cdr!
                                     a-local-table
                                     (cons
                                      next-local-table (cdr a-local-table)))
                                    (local-insert!
                                     tail-list value next-local-table)
                                    ))
                                ))
                          )))
                    ))
              ))
          (begin
            (local-insert! a-key-list value local-table)
            'ok
            ))
        (define (dispatch m)
          (begin
            (cond
             ((eq? m 'lookup-proc)
              (begin
                lookup
                ))
             ((eq? m 'insert-proc!)
              (begin
                insert!
                ))
             (else
              (begin
                (error "Unknown operation -- TABLE" m)
                )))
            ))
        (begin
          dispatch
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (get z)
  (begin
    (z 'lookup-proc)
    ))

;;;#############################################################
;;;#############################################################
(define (put! z)
  (begin
    (z 'insert-proc!)
    ))

;;;#############################################################
;;;#############################################################
(define (main-scheme-test test-list)
  (begin
    (let ((table-1 (make-table equal?)))
      (begin
        (display
         (format #f "setup table~%"))
        (display
         (format #f "(define table-1 (make-table equal?))~%"))
        (for-each
         (lambda (alist)
           (begin
             (let ((key-list (list-ref alist 0))
                   (value (list-ref alist 1)))
               (begin
                 ((put! table-1) key-list value)
                 (display
                  (format
                   #f "(put! table-1 '~a ~a)~%"
                   key-list value))
                 (force-output)
                 ))
             )) test-list)

        (newline)
        (display (format #f "retrieve data~%"))
        (for-each
         (lambda (alist)
           (begin
             (let ((key-list (list-ref alist 0))
                   (value (list-ref alist 1)))
               (begin
                 (let ((tvalue
                        ((get table-1) key-list)))
                   (begin
                     (if (equal? tvalue value)
                         (begin
                           (display
                            (format
                             #f "(get table-1 '~a) -> ~a~%"
                             key-list tvalue)))
                         (begin
                           (let ((atmp
                                  (format
                                   #f "result=~a, shouldbe=~a"
                                   tvalue value)))
                             (begin
                               (display
                                (format
                                 #f "(get table-1 '~a), ~a~%"
                                 key-list atmp))
                               ))
                           ))
                     (force-output)
                     ))
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-1)
  (begin
    (let ((test-list
           (list
            (list (list 'alphabet 'a) 23)
            (list (list 'alphabet 'b) 24)
            (list (list 'alphabet 'c) 25)
            (list (list 'alphabet 'd) 26)
            (list (list 'numbers '33) 33)
            (list (list 'numbers '34) 34)
            (list (list 'numbers '35) 35)
            (list (list 'numbers '36) 36)
            (list (list 'numbers '37) 37))))
      (begin
        (main-scheme-test test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-2)
  (begin
    (let ((test-list
           (list
            (list (list 'alphabet 'a) 23)
            (list (list 'alphabet 'b) 24)
            (list (list 'alphabet 'c) 25)
            (list (list 'alphabet 'd) 26)
            (list (list 'numbers 'unique '33) 33)
            (list (list 'numbers 'common '34) 34)
            (list (list 'numbers 'unique '35) 35)
            (list (list 'numbers 'common '36) 36)
            (list (list 'numbers 'unique '37) 37))))
      (begin
        (main-scheme-test test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-3)
  (begin
    (let ((test-list
           (list
            (list (list 'alphabet 'a) 23)
            (list (list 'alphabet 'b) 24)
            (list (list 'alphabet 'c) 25)
            (list (list 'alphabet 'd) 26)
            (list (list 'numbers 'arbitrary 'unique '33) 33)
            (list (list 'numbers 'arbitrary 'common '34) 34)
            (list (list 'numbers 'arbitrary 'unique '35) 35)
            (list (list 'numbers 'arbitrary 'common '36) 36)
            (list (list 'numbers 'arbitrary 'unique '37) 37))))
      (begin
        (main-scheme-test test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Generalizing one- and two-dimensional "))
    (display
     (format #f "tables, show how~%"))
    (display
     (format #f "to implement a table in which values "))
    (display
     (format #f "are stored under~%"))
    (display
     (format #f "an arbitrary number of keys and "))
    (display
     (format #f "different values may be~%"))
    (display
     (format #f "stored under different numbers of keys. "))
    (display
     (format #f "The lookup and~%"))
    (display
     (format #f "insert! procedures should take as input "))
    (display
     (format #f "a list of keys~%"))
    (display
     (format #f "used to access the table.~%"))
    (newline)
    (display
     (format #f "(define (make-table same-key?)~%"))
    (display
     (format #f "  (let ((local-table (list '*table*)))~%"))
    (display
     (format #f "    (define (assoc key records)~%"))
    (display
     (format #f "      (cond ((null? records) #f)~%"))
    (display
     (format #f "            ((same-key? key "))
    (display
     (format #f "(caar records)) (car records))~%"))
    (display
     (format #f "            (else (assoc key "))
    (display
     (format #f "(cdr records)))))~%"))
    (display
     (format #f "    (define (lookup a-key-list)~%"))
    (display
     (format #f "      (define (local-lookup "))
    (display
     (format #f "a-key-list a-local-table)~%"))
    (display
     (format #f "        (if (null? a-key-list)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (if a-local-table~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (cdr a-local-table))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    #f~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((this-key "))
    (display
     (format #f "(car a-key-list))~%"))
    (display
     (format #f "                    (tail-list "))
    (display
     (format #f "(cdr a-key-list)))~%"))
    (display
     (format #f "                (let ((subtable "))
    (display
     (format #f "(assoc this-key "))
    (display
     (format #f "(cdr a-local-table))))~%"))
    (display
     (format #f "                  (if subtable~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (local-lookup "))
    (display
     (format #f "tail-list subtable))~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        #f~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                  ))~%"))
    (display
     (format #f "              )))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (local-lookup "))
    (display
     (format #f "a-key-list local-table)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    (define (insert! a-key-list value)~%"))
    (display
     (format #f "      (define (local-insert! "))
    (display
     (format #f "a-key-list value a-local-table)~%"))
    (display
     (format #f "        (if (null? a-key-list)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (if a-local-table~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (set-cdr! "))
    (display
     (format #f "a-local-table value))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    #f~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((this-key "))
    (display
     (format #f "(car a-key-list))~%"))
    (display
     (format #f "                    (tail-list "))
    (display
     (format #f "(cdr a-key-list)))~%"))
    (display
     (format #f "                (let ((subtable "))
    (display
     (format #f "(assoc this-key (cdr a-local-table))))~%"))
    (display
     (format #f "                  (if subtable~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (if (null? "))
    (display
     (format #f "tail-list)~%"))
    (display
     (format #f "                            (begin~%"))
    (display
     (format #f "                              (set-cdr! "))
    (display
     (format #f "subtable value))~%"))
    (display
     (format #f "                            (begin~%"))
    (display
     (format #f "                              "))
    (display
     (format #f "(local-insert! "))
    (display
     (format #f "tail-list value subtable)~%"))
    (display
     (format #f "                              )))~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        "))
    (display
     (format #f "(let ((next-local-table "))
    (display
     (format #f "(list this-key)))~%"))
    (display
     (format #f "                          (begin~%"))
    (display
     (format #f "                            (set-cdr!~%"))
    (display
     (format #f "                             "))
    (display
     (format #f "a-local-table~%"))
    (display
     (format #f "                             "))
    (display
     (format #f "(cons next-local-table "))
    (display
     (format #f "(cdr a-local-table)))~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "(local-insert! tail-list "))
    (display
     (format #f "value next-local-table)~%"))
    (display
     (format #f "                            ))~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                  ))~%"))
    (display
     (format #f "              )))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (local-insert! "))
    (display
     (format #f "a-key-list value local-table)~%"))
    (display
     (format #f "        'ok))~%"))
    (display
     (format #f "    (define (dispatch m)~%"))
    (display
     (format #f "      (cond ((eq? m 'lookup-proc) "))
    (display
     (format #f "lookup)~%"))
    (display
     (format #f "            ((eq? m 'insert-proc!) "))
    (display
     (format #f "insert!)~%"))
    (display
     (format #f "            (else~%"))
    (display
     (format #f "              "))
    (display
     (format #f "(error \"Unknown operation "))
    (display
     (format #f "-- TABLE\" m))))~%"))
    (display
     (format #f "    dispatch))~%"))
    (display
     (format #f "(define (get z) (z 'lookup-proc))~%"))
    (display
     (format #f "(define (put! z) (z 'insert-proc!))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (scheme-test-1)
    (newline)
    (scheme-test-2)
    (newline)
    (scheme-test-3)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.25 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
