#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.04                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-account pwd init-bal)
  (define balance init-bal)
  (define password pwd)
  (define consecutive-counter 0)
  (define (reset-cc)
    (begin
      (set! consecutive-counter 0)
      ))
  (define (increment-cc)
    (begin
      (set!
       consecutive-counter (1+ consecutive-counter))
      ))
  (define (call-the-cops anum)
    (begin
      "call-the-cops"
      ))
  (define (withdraw amount)
    (begin
      (if (>= balance amount)
          (begin
            (set! balance (- balance amount))
            balance)
          (begin
            "Insufficient funds"
            ))
      ))
  (define (deposit amount)
    (begin
      (set! balance (+ balance amount))
      balance
      ))
  (define (dispatch secret-pwd m)
    (begin
      (if (and (symbol? secret-pwd)
               (eq? password secret-pwd))
          (begin
            (reset-cc)
            (cond
             ((eq? m 'withdraw)
              (begin
                withdraw
                ))
             ((eq? m 'deposit)
              (begin
                deposit
                ))
             (else
              (begin
                (display
                 (format
                  #f "Unknown request -- MAKE-ACCOUNT ~a~%"
                  m))
                (quit)
                ))
             ))
          (begin
            (increment-cc)
            (if (> consecutive-counter 7)
                (begin
                  call-the-cops)
                (begin
                  (lambda (anum)
                    "incorrect password")
                  ))
            ))
      ))
  (begin
    dispatch
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Modify the make-account procedure of "))
    (display
     (format #f "exercise 3.3 by~%"))
    (display
     (format #f "adding another local state variable so "))
    (display
     (format #f "that, if an~%"))
    (display
     (format #f "account is accessed more than seven "))
    (display
     (format #f "consecutive times~%"))
    (display
     (format #f "with an incorrect password, it invokes "))
    (display
     (format #f "the procedure~%"))
    (display
     (format #f "call-the-cops.~%"))
    (newline)
    (display
     (format #f "(define (make-account pwd init-bal)~%"))
    (display
     (format #f "  (define balance init-bal)~%"))
    (display
     (format #f "  (define password pwd)~%"))
    (display
     (format #f "  (define consecutive-counter 0)~%"))
    (display
     (format #f "  (define (reset-cc)~%"))
    (display
     (format #f "    (set! consecutive-counter 0))~%"))
    (display
     (format #f "  (define (increment-cc)~%"))
    (display
     (format #f "    (set! consecutive-counter "))
    (display
     (format #f "(1+ consecutive-counter)))~%"))
    (display
     (format #f "  (define (call-the-cops anum)~%"))
    (display
     (format #f "    \"call-the-cops\")~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (if (>= balance amount)~%"))
    (display
     (format #f "        (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "               balance)~%"))
    (display
     (format #f "        \"Insufficient funds\"))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (set! balance "))
    (display
     (format #f "(+ balance amount))~%"))
    (display
     (format #f "    balance)~%"))
    (display
     (format #f "  (define (dispatch secret-pwd m)~%"))
    (display
     (format #f "    (if (and (symbol? secret-pwd)~%"))
    (display
     (format #f "             (eq? password secret-pwd))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (reset-cc)~%"))
    (display
     (format #f "          (cond ((eq? m 'withdraw) "))
    (display
     (format #f "withdraw)~%"))
    (display
     (format #f "                ((eq? m 'deposit) "))
    (display
     (format #f "deposit)~%"))
    (display
     (format #f "                (else~%"))
    (display
     (format #f "                 (display~%"))
    (display
     (format #f "                   (format #f \"Unknown "))
    (display
     (format #f "request -- MAKE-ACCOUNT ~~a~~%\"~%"))
    (display
     (format #f "                       m))~%"))
    (display
     (format #f "                 (quit))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (increment-cc)~%"))
    (display
     (format #f "          (if (> consecutive-counter 7)~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                call-the-cops)~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (lambda (anum)~%"))
    (display
     (format #f "                  \"incorrect password\")~%"))
    (display
     (format #f "                 ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  dispatch)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((acc (make-account 'secret 100)))
      (begin
        (display
         (format
          #f "(define acc (make-account 'secret 100))~%"))
        (display
         (format
          #f "((acc 'secret 'withdraw) 0) = ~a~%"
          ((acc 'secret 'withdraw) 0)))
        (newline)
        (display
         (format #f "((acc 'secret 'withdraw) 10) = ~a~%"
                 ((acc 'secret 'withdraw) 10)))
        (newline)
        (display
         (format #f "((acc 'secret 'deposit) 100) = ~a~%"
                 ((acc 'secret 'deposit) 100)))
        (newline)
        (display
         (format #f "(1) ((acc 'wrong 'withdraw) 100) = ~a~%"
                 ((acc 'wrong 'withdraw) 100)))
        (display
         (format #f "((acc 'secret 'deposit) 100) = ~a~%"
                 ((acc 'secret 'deposit) 100)))
        (newline)
        (do ((ii 1 (1+ ii)))
            ((> ii 8))
          (begin
            (display
             (format
              #f "(~a) ((acc 'wrong 'withdraw) 100) = ~a~%"
              ii ((acc 'wrong 'withdraw) 100)))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.04 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
