#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.47                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A semaphore (of size n) is a "))
    (display
     (format #f "generalization of a mutex.~%"))
    (display
     (format #f "Like a mutex, a semaphore supports "))
    (display
     (format #f "acquire and release~%"))
    (display
     (format #f "operations, but it is more general "))
    (display
     (format #f "in that up to~%"))
    (display
     (format #f "n processes can acquire it "))
    (display
     (format #f "concurrently. Additional~%"))
    (display
     (format #f "processes that attempt to acquire "))
    (display
     (format #f "the semaphore must~%"))
    (display
     (format #f "wait for release operations. Give "))
    (display
     (format #f "implementations~%"))
    (display
     (format #f "of semaphores:~%"))
    (display
     (format #f "a. in terms of mutexes~%"))
    (display
     (format #f "b. in terms of atomic test-and-set! "))
    (display
     (format #f "operations.~%"))
    (newline)
    (display
     (format #f "(a) semaphore definition in "))
    (display
     (format #f "terms of mutexes~%"))
    (display
     (format #f "sem-list - contains a list of mutex's~%"))
    (display
     (format #f "counter-index-list - number of "))
    (display
     (format #f "processes waiting~%"))
    (display
     (format #f "for each index.~%"))
    (newline)
    (display
     (format #f "(define (make-semaphore n)~%"))
    (display
     (format #f "  (let ((sem-list (list))~%"))
    (display
     (format #f "        (counter-index-list (list)))~%"))
    (display
     (format #f "    (define (build-sem-list ii max)~%"))
    (display
     (format #f "      (if (< ii max)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (set! sem-list (cons "))
    (display
     (format #f "(make-mutex) sem-list))~%"))
    (display
     (format #f "            (set! counter-index-list "))
    (display
     (format #f "(cons 0 sem-list))~%"))
    (display
     (format #f "            (build-sem-list "))
    (display
     (format #f "(1+ ii) max))))~%"))
    (display
     (format #f "    (define (find-min-used-index)~%"))
    (display
     (format #f "      (let ((min-count-used #f) "))
    (display
     (format #f "(min-used #f))~%"))
    (display
     (format #f "        (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "            ((>= ii n))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((count "))
    (display
     (format #f "(list-ref counter-index-list ii)))~%"))
    (display
     (format #f "              (if (or (equal? "))
    (display
     (format #f "min-count-used #f)~%"))
    (display
     (format #f "                      (< count "))
    (display
     (format #f "min-count-used))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (set! min-used ii)~%"))
    (display
     (format #f "                    (set! "))
    (display
     (format #f "min-count-used count)~%"))
    (display
     (format #f "                   )))))~%"))
    (display
     (format #f "         min-used))~%"))
    (display
     (format #f "    (define (acquire-semaphore)~%"))
    (display
     (format #f "      (let ((this-index "))
    (display
     (format #f "(find-min-used-index)))~%"))
    (display
     (format #f "        (let ((this-mutex "))
    (display
     (format #f "(list-ref sem-list this-index))~%"))
    (display
     (format #f "              (this-count "))
    (display
     (format #f "(list-ref counter-index-list "))
    (display
     (format #f "this-index)))~%"))
    (display
     (format #f "           (this-mutex 'acquire)~%"))
    (display
     (format #f "           (list-set! counter-index-list "))
    (display
     (format #f "this-index (1+ this-count))~%"))
    (display
     (format #f "           this-index)))~%"))
    (display
     (format #f "    (define (release-semaphore ii)~%"))
    (display
     (format #f "      (if (<= ii n)~%"))
    (display
     (format #f "          (let ((this-mutex "))
    (display
     (format #f "(list-ref sem-list ii))~%"))
    (display
     (format #f "                (this-count "))
    (display
     (format #f "(list-ref counter-index-list ii)))~%"))
    (display
     (format #f "            (this-mutex 'release)~%"))
    (display
     (format #f "            (list-set! "))
    (display
     (format #f "counter-index-list ii (1- this-count))~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "    (define (dispatch message)~%"))
    (display
     (format #f "      (let ((sem-index 0))~%"))
    (display
     (format #f "        (cond ((eq? message 'acquire)~%"))
    (display
     (format #f "               (let ((this-index "))
    (display
     (format #f "(acquire-semaphore)))~%"))
    (display
     (format #f "                 (set! sem-index "))
    (display
     (format #f "this-index)))~%"))
    (display
     (format #f "              ((eq? message 'release)~%"))
    (display
     (format #f "               (begin~%"))
    (display
     (format #f "                 (release-ii-mutex "))
    (display
     (format #f "sem-index)~%"))
    (display
     (format #f "                 (set! sem-index 0))))))~%"))
    (display
     (format #f "     (build-sem-list 0 n)~%"))
    (display
     (format #f "     'ok))~%"))
    (newline)
    (display
     (format #f "(b) semaphore definition in terms "))
    (display
     (format #f "of test-and-set!~%"))
    (display
     (format #f "(define (make-semaphore n)~%"))
    (display
     (format #f "  (let ((sem-list (list)) "))
    (display
     (format #f "(counter-index-list (list)))~%"))
    (display
     (format #f "    (define (build-sem-list ii max)~%"))
    (display
     (format #f "      (if (< ii max)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (set! sem-list "))
    (display
     (format #f "(cons #f sem-list))~%"))
    (display
     (format #f "            (set! counter-index-list "))
    (display
     (format #f "(cons 0 counter-index-list))~%"))
    (display
     (format #f "            (build-sem-list "))
    (display
     (format #f "(1+ ii) max))))~%"))
    (display
     (format #f "    (define (find-min-used-index)~%"))
    (display
     (format #f "      (let ((min-count-used #f) "))
    (display
     (format #f "(min-used #f))~%"))
    (display
     (format #f "        (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "            ((>= ii n))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((count "))
    (display
     (format #f "(list-ref counter-index-list ii)))~%"))
    (display
     (format #f "              (if (or (equal? "))
    (display
     (format #f "min-count-used #f)~%"))
    (display
     (format #f "                      (< count "))
    (display
     (format #f "min-count-used))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (set! min-used ii)~%"))
    (display
     (format #f "                    (set! min-count-used "))
    (display
     (format #f "count)~%"))
    (display
     (format #f "                   )))))~%"))
    (display
     (format #f "         min-used))~%"))
    (display
     (format #f "    (define (test-and-set! cell)~%"))
    (display
     (format #f "      (if (equal? cell #t)~%"))
    (display
     (format #f "          #t~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (set! cell #t)~%"))
    (display
     (format #f "            #f)))~%"))
    (display
     (format #f "    (define (acquire-semaphore)~%"))
    (display
     (format #f "      (let ((this-index "))
    (display
     (format #f "(find-min-used-index)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (if (test-and-set! "))
    (display
     (format #f "(list-ref sem-list this-index))~%"))
    (display
     (format #f "                (acquire-semaphore)~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (let ((this-count "))
    (display
     (format #f "(list-ref counter-index-list "))
    (display
     (format #f "this-index)))~%"))
    (display
     (format #f "                    (list-set! "))
    (display
     (format #f "counter-index-list this-index "))
    (display
     (format #f "(1+ this-count))~%"))
    (display
     (format #f "                    this-index))))))~%"))
    (display
     (format #f "    (define (release-semaphore ii)~%"))
    (display
     (format #f "      (if (<= ii n)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (list-set! sem-list ii #f)~%"))
    (display
     (format #f "            (let ((this-count "))
    (display
     (format #f "(list-ref counter-index-list ii)))~%"))
    (display
     (format #f "              (list-set! "))
    (display
     (format #f "counter-index-list ii (max 0 "))
    (display
     (format #f "(1- this-count)))~%"))
    (display
     (format #f "            ))))~%"))
    (display
     (format #f "    (define (dispatch message)~%"))
    (display
     (format #f "      (let ((sem-index 0))~%"))
    (display
     (format #f "        (cond ((eq? message 'acquire)~%"))
    (display
     (format #f "               (let ((this-index "))
    (display
     (format #f "(acquire-semaphore)))~%"))
    (display
     (format #f "                 (set! sem-index "))
    (display
     (format #f "this-index)))~%"))
    (display
     (format #f "              ((eq? message 'release)~%"))
    (display
     (format #f "               (begin~%"))
    (display
     (format #f "                 (release-ii-mutex "))
    (display
     (format #f "sem-index)~%"))
    (display
     (format #f "                 (set! "))
    (display
     (format #f "sem-index 0))))))~%"))
    (display
     (format #f "     (build-sem-list 0 n)~%"))
    (display
     (format #f "     'ok))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.47 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
