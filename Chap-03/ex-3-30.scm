#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.30                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Figure 3.27 shows a ripple-carry "))
    (display
     (format #f "adder formed by~%"))
    (display
     (format #f "stringing together n full-adders. "))
    (display
     (format #f "This is the~%"))
    (display
     (format #f "simplest form of parallel adder for "))
    (display
     (format #f "adding two n-bit~%"))
    (display
     (format #f "binary numbers. The inputs A1, A2, A3, "))
    (display
     (format #f "..., An and B1,~%"))
    (display
     (format #f "B2, B3, ..., Bn are the two binary "))
    (display
     (format #f "numbers to be~%"))
    (display
     (format #f "added (each Ak and Bk is a 0 or a 1). "))
    (display
     (format #f "The circuit~%"))
    (display
     (format #f "generates S1, S2, S3, ..., Sn, the "))
    (display
     (format #f "n bits of the~%"))
    (display
     (format #f "sum, and C, the carry from the "))
    (display
     (format #f "addition. Write a~%"))
    (display
     (format #f "procedure ripple-carry-adder that "))
    (display
     (format #f "generates this~%"))
    (display
     (format #f "circuit. The procedure should take "))
    (display
     (format #f "as arguments three~%"))
    (display
     (format #f "lists of n wires n wires each -- "))
    (display
     (format #f "the Ak, the Bk,~%"))
    (display
     (format #f "and the Sk -- and also another "))
    (display
     (format #f "wire C. The major~%"))
    (display
     (format #f "drawback of the ripple-carry adder "))
    (display
     (format #f "is the need~%"))
    (display
     (format #f "to wait for the carry signals to "))
    (display
     (format #f "propagate. What is~%"))
    (display
     (format #f "the delay needed to obtain the "))
    (display
     (format #f "complete output from~%"))
    (display
     (format #f "an n-bit ripple-carry adder, "))
    (display
     (format #f "expressed in terms of~%"))
    (display
     (format #f "the delays for and-gates, or-gates, "))
    (display
     (format #f "and inverters?~%"))
    (newline)
    (display
     (format #f "(define (ripple-carry-adder "))
    (display
     (format #f "a-list b-list s-list out-carry-bit)~%"))
    (display
     (format #f "  (define full-adder-delay "))
    (display
     (format #f "(+ (* 2 (+ or-gate-delay "))
    (display
     (format #f "(* 2 and-gate-delay)~%"))
    (display
     (format #f "                                     "))
    (display
     (format #f "inverter-delay)) or-gate-delay))~%"))
    (display
     (format #f "  (define (local-iter a-list b-list "))
    (display
     (format #f "in-carry-bit s-list out-carry-bit)~%"))
    (display
     (format #f "    (if (null? a-list)~%"))
    (display
     (format #f "        (set-signal! "))
    (display
     (format #f "out-carry-bit in-carry-bit)~%"))
    (display
     (format #f "        (let ((ak (car a-list))~%"))
    (display
     (format #f "              (bk (car b-list))~%"))
    (display
     (format #f "              (sk (car s-list))~%"))
    (display
     (format #f "              (ck (make-wire))~%"))
    (display
     (format #f "              (a-tail (cdr a-list))~%"))
    (display
     (format #f "              (b-tail (cdr b-list))~%"))
    (display
     (format #f "              (s-tail (cdr s-list)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (full-adder ak bk "))
    (display
     (format #f "in-carry-bit sk ck)~%"))
    (display
     (format #f "            (after-delay "))
    (display
     (format #f "full-adder-delay~%"))
    (display
     (format #f "              (lambda ()~%"))
    (display
     (format #f "                (local-iter a-tail b-tail "))
    (display
     (format #f "ck s-tail out-carry-bit)~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "             ))))~%"))
    (display
     (format #f "  (define "))
    (display
     (format #f "(ripple-carry-action-procedure)~%"))
    (display
     (format #f "    (let ((in-carry-bit "))
    (display
     (format #f "(make-wire)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (set-signal! in-carry-bit 0)~%"))
    (display
     (format #f "        (local-iter a-list b-list "))
    (display
     (format #f "in-carry-bit s-list out-carry-bit)~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (define "))
    (display
     (format #f "(add-action-to-list a-list)~%"))
    (display
     (format #f "    (for-each~%"))
    (display
     (format #f "      (lambda (a-elem)~%"))
    (display
     (format #f "        (add-action! a-elem "))
    (display
     (format #f "ripple-carry-action-procedure)~%"))
    (display
     (format #f "        ) a-list))~%"))
    (display
     (format #f "  (add-action-to-list a-list)~%"))
    (display
     (format #f "  (add-action-to-list b-list)~%"))
    (display
     (format #f " 'ok)~%"))
    (newline)
    (display
     (format #f "The total delay for an n-bit adder "))
    (display
     (format #f "is n*full-adder-delay,~%"))
    (display
     (format #f "where the full-adder-delay="))
    (display
     (format #f "(+ (* 2 (+ or-gate-delay~%"))
    (display
     (format #f "(* 2 and-gate-delay) inverter-delay)) "))
    (display
     (format #f "or-gate-delay).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.30 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
