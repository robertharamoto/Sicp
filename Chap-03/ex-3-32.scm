#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.32                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The procedures to be run during each "))
    (display
     (format #f "time segment of~%"))
    (display
     (format #f "the agenda are kept in a queue. Thus, "))
    (display
     (format #f "the procedures~%"))
    (display
     (format #f "for each segment are called in the order "))
    (display
     (format #f "in which they~%"))
    (display
     (format #f "were added to the agenda (first in, "))
    (display
     (format #f "first out). Explain~%"))
    (display
     (format #f "why this order must be used. In particular, "))
    (display
     (format #f "trace the~%"))
    (display
     (format #f "behavior of an and-gate whose inputs "))
    (display
     (format #f "change from 0,1~%"))
    (display
     (format #f "to 1,0 in the same segment and say "))
    (display
     (format #f "how the behavior~%"))
    (display
     (format #f "would differ if we stored a segment's "))
    (display
     (format #f "procedures in an~%"))
    (display
     (format #f "ordinary list, adding and removing "))
    (display
     (format #f "procedures only at~%"))
    (display
     (format #f "the front (last in, first out).~%"))
    (newline)
    (display
     (format #f "(define (and-gate a1 a2 output)~%"))
    (display
     (format #f "  (define (and-action-procedure)~%"))
    (display
     (format #f "    (let ((new-value~%"))
    (display
     (format #f "           (logical-and "))
    (display
     (format #f "(get-signal a1) (get-signal a2))))~%"))
    (display
     (format #f "      (after-delay and-gate-delay~%"))
    (display
     (format #f "                   (lambda ()~%"))
    (display
     (format #f "                     (set-signal! "))
    (display
     (format #f "output new-value)))))~%"))
    (display
     (format #f "  (add-action! a1 and-action-procedure)~%"))
    (display
     (format #f "  (add-action! a2 and-action-procedure)~%"))
    (display
     (format #f "  'ok)~%"))
    (newline)
    (display
     (format #f "When the and-gate is constructed with "))
    (display
     (format #f "wires a1, a2,~%"))
    (display
     (format #f "and output, the actions for a1 and a2 are "))
    (display
     (format #f "added to the~%"))
    (display
     (format #f "wires a1 and a2, respectively. Suppose "))
    (display
     (format #f "we have the~%"))
    (display
     (format #f "values a1=0, and a2=1. Let the value of "))
    (display
     (format #f "a1 change~%"))
    (display
     (format #f "from 0 to 1 using the wire's set-signal! "))
    (display
     (format #f "function.~%"))
    (display
     (format #f "The set-signal! function will call the "))
    (display
     (format #f "a1 wire's~%"))
    (display
     (format #f "action procedure, and-action-procedure.~%"))
    (display
     (format #f "The and-action-procedure will call "))
    (display
     (format #f "after-delay with~%"))
    (display
     (format #f "a1=1 and a2=1, so output=1.  after-delay "))
    (display
     (format #f "then~%"))
    (display
     (format #f "adds the function with delay time "))
    (display
     (format #f "equal to~%"))
    (display
     (format #f "and-gate-delay, and action function, "))
    (display
     (format #f "and stores it~%"))
    (display
     (format #f "in the-agenda.  When the value of a2 "))
    (display
     (format #f "changes from~%"))
    (display
     (format #f "1 to 0, the and-action-procedure "))
    (display
     (format #f "will call~%"))
    (display
     (format #f "after-delay again with the final "))
    (display
     (format #f "transition state~%"))
    (display
     (format #f "for the output wire with a1=1, a2=0, "))
    (display
     (format #f "and output=0.~%"))
    (display
     (format #f "Since these happen at the same 'time', "))
    (display
     (format #f "(at current-time~%"))
    (display
     (format #f "plus and-gate-delay), these two actions "))
    (display
     (format #f "get put into~%"))
    (display
     (format #f "the same segment.~%"))
    (newline)
    (display
     (format #f "A queue is necessary to ensure that the~%"))
    (display
     (format #f "and-action-procedure's are called in "))
    (display
     (format #f "the right order,~%"))
    (display
     (format #f "the final and-action-procedure "))
    (display
     (format #f "should be called~%"))
    (display
     (format #f "last, since it has the final correct "))
    (display
     (format #f "output value.~%"))
    (display
     (format #f "Note that the queue adds from the "))
    (display
     (format #f "back and behaves~%"))
    (display
     (format #f "like a first-in, first-out stack.~%"))
    (newline)
    (display
     (format #f "A list would not behave properly for "))
    (display
     (format #f "a segment, since~%"))
    (display
     (format #f "if you only add procedures and take "))
    (display
     (format #f "the data off~%"))
    (display
     (format #f "the front, then you get last-in, "))
    (display
     (format #f "first-out behavior,~%"))
    (display
     (format #f "so the and-action-procedure with "))
    (display
     (format #f "the correct and~%"))
    (display
     (format #f "final output value, will be executed "))
    (display
     (format #f "first, with~%"))
    (display
     (format #f "the intermediate output value being "))
    (display
     (format #f "executed last,~%"))
    (display
     (format #f "so you will end up with an incorrect "))
    (display
     (format #f "final output~%"))
    (display
     (format #f "value.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.32 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
