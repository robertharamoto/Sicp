#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.68                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-limit astream tolerance)
  (define (local-rec astream prev-val count tol)
    (begin
      (if (srfi-41:stream-null? astream)
          (begin
            (list prev-val count))
          (begin
            (let ((a0 (srfi-41:stream-car astream))
                  (tail-stream (srfi-41:stream-cdr astream)))
              (let ((diff (abs (- a0 prev-val))))
                (begin
                  (if (< diff tol)
                      (begin
                        (list a0 count))
                      (begin
                        (local-rec tail-stream a0 (1+ count) tol)
                        ))
                  )))
            ))
      ))
  (begin
    (local-rec (srfi-41:stream-cdr astream)
               (srfi-41:stream-car astream)
               0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (merge-pairs s1 s2)
  (begin
    (cond
     ((srfi-41:stream-null? s1)
      (begin
        s2
        ))
     ((srfi-41:stream-null? s2)
      (begin
        s1
        ))
     (else
      (begin
        (display
         (format #f "debug merge-pairs (stream-car s1)=~a, "
                 (srfi-41:stream-car s1)))
        (display
         (format #f "(stream-car s2)=~a~%"
                 (srfi-41:stream-car s2)))
        (force-output)
        (let ((s1carcar (car (srfi-41:stream-car s1)))
              (s1car (srfi-41:stream-car s1))
              (s2carcar (car (srfi-41:stream-car s2)))
              (s2car (srfi-41:stream-car s2)))
          (begin
            (cond
             ((< s1carcar s2carcar)
              (begin
                (srfi-41:stream-cons
                 s1car
                 (merge-pairs (srfi-41:stream-cdr s1) s2))
                ))
             ((> s1carcar s2carcar)
              (begin
                (srfi-41:stream-cons
                 s1car
                 (merge-pairs s1 (srfi-41:stream-cdr s2)))
                ))
             (else
              (begin
                (srfi-41:stream-cons
                 s2car
                 (merge-pairs (srfi-41:stream-cdr s1)
                              (srfi-41:stream-cdr s2)))
                )))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (display
           (format
            #f "  debug interleave (car s1)=~a, (car s2)=~a~%"
            (srfi-41:stream-car s1) (srfi-41:stream-car s2)))
          (force-output)
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (interleave
       (srfi-41:stream-map
        (lambda (x)
          (begin
            (list x (srfi-41:stream-car s))
            ))
        (srfi-41:stream-cdr t))
       (pairs (srfi-41:stream-cdr s) (srfi-41:stream-cdr t))
       )))
    ))

;;;#############################################################
;;;#############################################################
(define (lr-pairs s t)
  (begin
    (display
     (format
      #f "debug lr-pairs (car s)=~a, (car t)=~a~%"
      (srfi-41:stream-car s) (srfi-41:stream-car t)))
    (force-output)
    (interleave
     (srfi-41:stream-map
      (lambda (x)
        (begin
          (list (srfi-41:stream-car s) x)
          ))
      t)
     (lr-pairs
      (srfi-41:stream-cdr s) (srfi-41:stream-cdr t)))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner thinks that building a "))
    (display
     (format #f "stream of pairs~%"))
    (display
     (format #f "from three parts is unnecessarily "))
    (display
     (format #f "complicated. Instead~%"))
    (display
     (format #f "of separating the pair (S0,T0) from "))
    (display
     (format #f "the rest of the~%"))
    (display
     (format #f "pairs in the first row, he proposes to "))
    (display
     (format #f "work with the~%"))
    (display
     (format #f "whole first row, as follows:~%"))
    (newline)
    (display
     (format #f "(define (pairs s t)~%"))
    (display
     (format #f "  (interleave~%"))
    (display
     (format #f "   (stream-map (lambda (x) "))
    (display
     (format #f "(list (stream-car s) x))~%"))
    (display
     (format #f "               t)~%"))
    (display
     (format #f "   (pairs (stream-cdr s) "))
    (display
     (format #f "(stream-cdr t))))~%"))
    (newline)
    (display
     (format #f "Does this work? Consider what happens "))
    (display
     (format #f "if we evaluate~%"))
    (display
     (format #f "(pairs integers integers) using Louis's "))
    (display
     (format #f "definition of pairs.~%"))
    (newline)
    (display
     (format #f "This version of pairs doesn't work, because "))
    (display
     (format #f "it's a~%"))
    (display
     (format #f "recursive procedure. Arguments to a "))
    (display
     (format #f "function are evaluated~%"))
    (display
     (format #f "first so in the call to interleave "))
    (display
     (format #f "(which produces a stream),~%"))
    (display
     (format #f "both arguments to interleave are "))
    (display
     (format #f "evaluated. The~%"))
    (display
     (format #f "(pairs (stream-cdr s)...) term is "))
    (display
     (format #f "called which produces~%"))
    (display
     (format #f "an infinite loop, before interleave "))
    (display
     (format #f "is ever called.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.68 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
