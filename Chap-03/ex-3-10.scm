#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.10                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the make-withdraw procedure, the "))
    (display
     (format #f "local variable balance~%"))
    (display
     (format #f "is created as a parameter of "))
    (display
     (format #f "make-withdraw. We could also~%"))
    (display
     (format #f "create the local state variable "))
    (display
     (format #f "explicitly, using let,~%"))
    (display
     (format #f "as follows:~%"))
    (display
     (format #f "(define (make-withdraw initial-amount)~%"))
    (display
     (format #f "  (let ((balance initial-amount))~%"))
    (display
     (format #f "    (lambda (amount)~%"))
    (display
     (format #f "      (if (>= balance amount)~%"))
    (display
     (format #f "          (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "                 balance)~%"))
    (display
     (format #f "          \"Insufficient funds\"))))~%"))
    (display
     (format #f "Recall from section 1.3.2 that let is "))
    (display
     (format #f "simply syntactic~%"))
    (display
     (format #f "sugar for a procedure call:~%"))
    (display
     (format #f "(let ((<var> <exp>)) <body>)~%"))
    (display
     (format #f "is interpreted as an alternate "))
    (display
     (format #f "syntax for~%"))
    (display
     (format #f "((lambda (<var>) <body>) <exp>)~%"))
    (display
     (format #f "Use the environment model to analyze "))
    (display
     (format #f "this alternate version~%"))
    (display
     (format #f "of make-withdraw, drawing figures like "))
    (display
     (format #f "the ones above to~%"))
    (display
     (format #f "illustrate the interactions:~%"))
    (display
     (format #f "(define W1 (make-withdraw 100))~%"))
    (display
     (format #f "(W1 50)~%"))
    (display
     (format #f "(define W2 (make-withdraw 100))~%"))
    (display
     (format #f "Show that the two versions of "))
    (display
     (format #f "make-withdraw create~%"))
    (display
     (format #f "objects with the same behavior. How do "))
    (display
     (format #f "the environment~%"))
    (display
     (format #f "structures differ for the two "))
    (display
     (format #f "versions?~%"))
    (newline)
    (display
     (format #f "make-withdraw is equivalent to:~%"))
    (display
     (format #f "(define (make-withdraw init-amount)~%"))
    (display
     (format #f "  ((lambda (balance)~%"))
    (display
     (format #f "     (lambda (amount)~%"))
    (display
     (format #f "       (if (>= balance amount)~%"))
    (display
     (format #f "         (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "                balance~%"))
    (display
     (format #f "           \"Insufficient funds\"))))~%"))
    (display
     (format #f "    initial-amount))~%"))
    (newline)
    (display
     (format #f "(define W1 (make-withdraw 100)) : "))
    (display
     (format #f "E1 { balance=100}~%"))
    (display
     (format #f "  -> Global { make-withdraw, W1 }~%"))
    (display
     (format #f "(W1 50) : E2 { amount = 50 } "))
    (display
     (format #f "-> E1 { balance = 100 }~%"))
    (display
     (format #f "  -> Global { make-withdraw, W1 }~%"))
    (display
     (format #f "(define W2 (make-withdraw 100)) : "))
    (display
     (format #f "E3 { balance=100 }~%"))
    (display
     (format #f "  -> Global { make-withdraw, W1, W2 }, "))
    (display
     (format #f "<- E1 { balance = 50 }~%"))
    (display
     (format #f "The environment structures are "))
    (display
     (format #f "identical in both cases.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.10 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
