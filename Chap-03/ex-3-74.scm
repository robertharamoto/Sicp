#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.74                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons
   1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons
   1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm)
      (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter
         pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s)
           (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (pairs
       (srfi-41:stream-cdr s)
       (srfi-41:stream-cdr t))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Alyssa P. Hacker is designing a system "))
    (display
     (format #f "to process signals~%"))
    (display
     (format #f "coming from physical sensors. One "))
    (display
     (format #f "important feature she~%"))
    (display
     (format #f "wishes to produce is a signal that "))
    (display
     (format #f "describes the zero~%"))
    (display
     (format #f "crossings of the input signal. That "))
    (display
     (format #f "is, the resulting~%"))
    (display
     (format #f "signal should be +1 whenever the input "))
    (display
     (format #f "signal changes~%"))
    (display
     (format #f "from negative to positive, -1 whenever "))
    (display
     (format #f "the input signal~%"))
    (display
     (format #f "changes from positive to negative, and "))
    (display
     (format #f "0 otherwise.~%"))
    (display
     (format #f "(Assume that the sign of a 0 input is "))
    (display
     (format #f "positive.) For~%"))
    (display
     (format #f "example, a typical input signal with its "))
    (display
     (format #f "associated~%"))
    (display
     (format #f "zero-crossing signal would be:~%"))
    (newline)
    (display
     (format #f "...1  2  1.5  1  0.5  -0.1  -2  "))
    (display
     (format #f "-3  -2  -0.5  0.2  3  4 ......~%"))
    (display
     (format #f "... 0    0  0    0     -1  0   "))
    (display
     (format #f "0   0     0    1  0  0 ...~%"))
    (newline)
    (display
     (format #f "In Alyssa's system, the signal from the "))
    (display
     (format #f "sensor is~%"))
    (display
     (format #f "represented as a stream sense-data and "))
    (display
     (format #f "the stream~%"))
    (display
     (format #f "zero-crossings is the corresponding stream "))
    (display
     (format #f "of zero~%"))
    (display
     (format #f "crossings. Alyssa first writes a "))
    (display
     (format #f "procedure~%"))
    (display
     (format #f "sign-change-detector that takes two "))
    (display
     (format #f "values as arguments and compares the "))
    (display
     (format #f "signs of the~%"))
    (display
     (format #f "values to produce and appropriate 0, 1, "))
    (display
     (format #f "or -1. She~%"))
    (display
     (format #f "then constructs her zero-crossing stream "))
    (display
     (format #f "as follows:~%"))
    (newline)
    (display
     (format #f "(define (make-zero-crossings "))
    (display
     (format #f "input-stream last-value)~%"))
    (display
     (format #f "  (cons-stream~%"))
    (display
     (format #f "   (sign-change-detector "))
    (display
     (format #f "(stream-car input-stream) last-value)~%"))
    (display
     (format #f "   (make-zero-crossings "))
    (display
     (format #f "(stream-cdr input-stream)~%"))
    (display
     (format #f "                        "))
    (display
     (format #f "(stream-car input-stream))))~%"))
    (display
     (format #f "(define zero-crossings "))
    (display
     (format #f "(make-zero-crossings sense-data 0))~%"))
    (newline)
    (display
     (format #f "Alyssa's boss, Eva Lu Ator, walks by and "))
    (display
     (format #f "suggests that this~%"))
    (display
     (format #f "program is approximately equivalent to "))
    (display
     (format #f "the following~%"))
    (display
     (format #f "one, which uses the generalized version "))
    (display
     (format #f "of stream-map~%"))
    (display
     (format #f "from exercise 3.50:~%"))
    (newline)
    (display
     (format #f "(define zero-crossings~%"))
    (display
     (format #f "  (stream-map "))
    (display
     (format #f "sign-change-detector~%"))
    (display
     (format #f "sense-data <expression>))~%"))
    (newline)
    (display
     (format #f "Complete the program by supplying "))
    (display
     (format #f "the indicated <expression>.~%"))
    (newline)
    (display
     (format #f "(define zero-crossings~%"))
    (display
     (format #f " (stream-map sign-change-detector "))
    (display
     (format #f "sense-data~%"))
    (display
     (format #f "  (stream-cdr sense-data)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.74 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
