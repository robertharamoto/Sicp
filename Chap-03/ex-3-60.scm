#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.60                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         ))
     stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons
   1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons
   1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm)
      (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
;;; turns a stream into another stream s0, (s0 s1), (s0 s1 s2), (s0 s1 s2 s3), ...
(define (partial-cons strm)
  (begin
    (let ((s0 (srfi-41:stream-car strm))
          (stail (srfi-41:stream-cdr strm)))
      (begin
        (srfi-41:stream-cons
         (list s0)
         (my-stream-map
          (lambda (x y)
            (begin
              (cons x y)
              ))
          stail (partial-cons strm)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mult-then-add-lists a1-list a2-list)
  (begin
    (let ((llen (min (length a1-list) (length a2-list)))
          (result 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii llen))
          (begin
            (let ((t1 (list-ref a1-list ii))
                  (t2 (list-ref a2-list ii)))
              (begin
                (set! result (+ result (* t1 t2)))
                ))
            ))
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (integrate-series astream)
  (begin
    (srfi-41:stream-map / astream integers)
    ))

;;;#############################################################
;;;#############################################################
(define cos-stream
  (srfi-41:stream-cons
   1 (scale-stream
      (integrate-series sin-stream) -1)))

;;;#############################################################
;;;#############################################################
(define sin-stream
  (srfi-41:stream-cons
   0 (integrate-series cos-stream)))

;;;#############################################################
;;;#############################################################
(define (original-mul-series s1 s2)
  (begin
    (let ((par-sum-1 (partial-cons s1))
          (par-sum-2 (partial-cons s2)))
      (begin
        (my-stream-map
         (lambda (s1 s2)
           (begin
             (let ((result
                    (mult-then-add-lists
                     s1 (reverse s2))))
               (begin
                 result
                 ))
             ))
         par-sum-1 par-sum-2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mul-series s1 s2)
  (begin
    (srfi-41:stream-cons
     (* (srfi-41:stream-car s1) (srfi-41:stream-car s2))
     (add-streams
      (scale-stream (srfi-41:stream-cdr s2)
                    (srfi-41:stream-car s1))
      (mul-series s2 (srfi-41:stream-cdr s1))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "With power series represented as streams "))
    (display
     (format #f "of coefficients as~%"))
    (display
     (format #f "in exercise 3.59, adding series is "))
    (display
     (format #f "implemented by add-streams.~%"))
    (display
     (format #f "Complete the definition of the following "))
    (display
     (format #f "procedure for~%"))
    (display
     (format #f "multiplying series:~%"))
    (newline)
    (display
     (format #f "(define (mul-series s1 s2)~%"))
    (display
     (format #f "  (cons-stream <??> "))
    (display
     (format #f "(add-streams <??> <??>)))~%"))
    (newline)
    (display
     (format #f "You can test your procedure by verifying "))
    (display
     (format #f "that sin2 x~%"))
    (display
     (format #f "+ cos2 x = 1, using the series from "))
    (display
     (format #f "exercise 3.59.~%"))
    (newline)
    (display
     (format #f "Let s1 = { a0, a1, a2, a3, a4, ... },~%"))
    (display
     (format #f "and s2 = { b0, b1, b2, b3, b4, ... }.~%"))
    (display
     (format #f "Taking into account the proper grouping "))
    (display
     (format #f "of powers of x,~%"))
    (display
     (format #f "the multiplication of these two "))
    (display
     (format #f "polynomials should be:~%"))
    (newline)
    (display
     (format #f "s1*s2 = { a0*b0,~%"))
    (display
     (format #f "          a0*b1+a1*b0,~%"))
    (display
     (format #f "          a0*b2+a1*b1+a2*b0,~%"))
    (display
     (format #f "          a0*b3+a1*b2+a2*b1+a3*b0,~%"))
    (display
     (format #f "          a0*b4+a1*b3+a2*b2+a3*b1+"))
    (display
     (format #f "a4*b0,~%"))
    (display
     (format #f "          a0*b5+a1*b4+a2*b3+a3*b2+"))
    (display
     (format #f "a4*b1+a5*b0, ... }~%"))
    (newline)
    (display
     (format #f "where the first element of the stream is "))
    (display
     (format #f "the 0th power~%"))
    (display
     (format #f "of x, the second element is the 1st "))
    (display
     (format #f "power of x, ...~%"))
    (display
     (format #f "This is similar to the problem of "))
    (display
     (format #f "constructing partial~%"))
    (display
     (format #f "sums. One can see a pattern in the "))
    (display
     (format #f "calculation of~%"))
    (display
     (format #f "s1*s2, that is the first column is a0 "))
    (display
     (format #f "times the series~%"))
    (display
     (format #f "s2, which is added to a1 times the "))
    (display
     (format #f "series s2,~%"))
    (display
     (format #f "which is added to a2 times the series "))
    (display
     (format #f "s2, ...~%"))
    (newline)
    (display
     (format #f "(define (mul-series s1 s2)~%"))
    (display
     (format #f "  (srfi-41:stream-cons~%"))
    (display
     (format #f "   (* (srfi-41:stream-car s1)~%"))
    (display
     (format #f "      (srfi-41:stream-car s2))~%"))
    (display
     (format #f "   (add-streams~%"))
    (display
     (format #f "    (scale-stream "))
    (display
     (format #f "(srfi-41:stream-cdr s2)~%"))
    (display
     (format #f "    (srfi-41:stream-car s1))~%"))
    (display
     (format #f "    (mul-series s2 "))
    (display
     (format #f "(srfi-41:stream-cdr s1))~%"))
    (display
     (format #f "    )))~%"))
    (newline)
    (display
     (format #f "The first term in stream-cons gives the "))
    (display
     (format #f "a0*b0 term in~%"))
    (display
     (format #f "the series multiplication. The scale-stream "))
    (display
     (format #f "term in~%"))
    (display
     (format #f "the add-streams function gives the first "))
    (display
     (format #f "row of the~%"))
    (display
     (format #f "multiplication (a0*b1, a0*b2,...). The "))
    (display
     (format #f "second mul-series~%"))
    (display
     (format #f "gives subsequent terms in the expressions "))
    (display
     (format #f "for the~%"))
    (display
     (format #f "coefficient of each power in x. That is "))
    (display
     (format #f "each additional~%"))
    (display
     (format #f "term in s1 (stream-cdr s1) gets "))
    (display
     (format #f "multiplied by~%"))
    (display
     (format #f "another stream of b coefficients "))
    (display
     (format #f "(s2).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((c-stream cos-stream)
          (s-stream sin-stream)
          (astream (partial-cons integers))
          (nmax 7))
      (begin
        (display
         (format #f "(stream-ref cos-stream)~%"))
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (display
             (format
              #f "(stream-ref cos-stream ~a) = ~a~%"
              ii (my-stream-ref c-stream ii)))
            (force-output)
            ))
        (newline)
        (display
         (format #f "(stream-ref sin-stream)~%"))
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (display
             (format
              #f "(stream-ref sin-stream ~a) = ~a~%"
              ii (my-stream-ref s-stream ii)))
            (force-output)
            ))
        ))

    (let ((c-stream cos-stream)
          (s-stream sin-stream)
          (nmax 10))
      (let ((c2-stream (mul-series c-stream c-stream))
            (s2-stream (mul-series s-stream s-stream)))
        (let ((c2-s2-stream (add-streams c2-stream s2-stream)))
          (begin
            (display
             (format #f "(stream-ref cos^2+sin^2)~%"))
            (do ((ii 0 (1+ ii)))
                ((>= ii nmax))
              (begin
                (display
                 (format
                  #f "(stream-ref cos^2+sin^2 ~a) = ~a~%"
                  ii (my-stream-ref c2-s2-stream ii)))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.60 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
