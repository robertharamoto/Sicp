#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.49                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Give a scenario where the "))
    (display
     (format #f "deadlock-avoidance mechanism~%"))
    (display
     (format #f "described above does not work. (Hint: "))
    (display
     (format #f "In the exchange~%"))
    (display
     (format #f "problem, each process knows in advance "))
    (display
     (format #f "which accounts it~%"))
    (display
     (format #f "will need to get access to. Consider "))
    (display
     (format #f "a situation where~%"))
    (display
     (format #f "a process must get access to some shared "))
    (display
     (format #f "resources before~%"))
    (display
     (format #f "it can know which additional shared "))
    (display
     (format #f "resources it will~%"))
    (display
     (format #f "require.)~%"))
    (newline)
    (display
     (format #f "Suppose when processing an account, "))
    (display
     (format #f "you need 2, 3, or 4~%"))
    (display
     (format #f "more records to complete the transaction, "))
    (display
     (format #f "and there are~%"))
    (display
     (format #f "joint accounts, and if two people are "))
    (display
     (format #f "accessing both~%"))
    (display
     (format #f "accounts and trying to update it "))
    (display
     (format #f "(say~%"))
    (display
     (format #f "address/telelphone/zip code...). Both "))
    (display
     (format #f "joint accounts~%"))
    (display
     (format #f "will have different id's, so the deadlock "))
    (display
     (format #f "avoidence~%"))
    (display
     (format #f "mechanism will work for balance information "))
    (display
     (format #f "but not for~%"))
    (display
     (format #f "other data linked to the accounts.~%"))
    (newline)
    (display
     (format #f "If one joint account needs to change "))
    (display
     (format #f "the address, phone,~%"))
    (display
     (format #f "and zip code, while the other joint "))
    (display
     (format #f "account needs to~%"))
    (display
     (format #f "change the phone, and zip code, then there "))
    (display
     (format #f "could be a~%"))
    (display
     (format #f "deadlock if each of those items are "))
    (display
     (format #f "individually~%"))
    (display
     (format #f "serialized, but one account to wait "))
    (display
     (format #f "for the other~%"))
    (display
     (format #f "to release the phone, while the "))
    (display
     (format #f "other needs~%"))
    (display
     (format #f "to release the zip code.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.49 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
