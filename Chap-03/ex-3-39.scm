#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.39                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Which of the five possibilities in the "))
    (display
     (format #f "parallel execution~%"))
    (display
     (format #f "shown above remain if we instead "))
    (display
     (format #f "serialize execution~%"))
    (display
     (format #f "as follows:~%"))
    (newline)
    (display
     (format #f "(define x 10)~%"))
    (display
     (format #f "(define s (make-serializer))~%"))
    (display
     (format #f "(parallel-execute (lambda ()~%"))
    (display
     (format #f "    (set! x ((s (lambda () (* x x))))))~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(s (lambda () (set! x (+ x 1)))))~%"))
    (newline)
    (display
     (format #f "The five possibilities are:~%"))
    (display
     (format #f "101:   P1 sets x to 100 and then "))
    (display
     (format #f "P2 increments~%"))
    (display
     (format #f "x to 101.~%"))
    (display
     (format #f "121:   P2 increments x to 11 and then "))
    (display
     (format #f "P1 sets x to~%"))
    (display
     (format #f "x times x.~%"))
    (display
     (format #f "110:   P2 changes x from 10 to 11 "))
    (display
     (format #f "between the two~%"))
    (display
     (format #f "times that P1 accesses the value of x "))
    (display
     (format #f "during the~%"))
    (display
     (format #f "evaluation of (* x x).~%"))
    (display
     (format #f "11:    P2 accesses x, then P1 sets x "))
    (display
     (format #f "to 100, then~%"))
    (display
     (format #f "P2 sets x.~%"))
    (display
     (format #f "100:   P1 accesses x (twice), then "))
    (display
     (format #f "P2 sets x to 11,~%"))
    (display
     (format #f "then P1 sets x.~%"))
    (newline)
    (display
     (format #f "Since the serializer protects both "))
    (display
     (format #f "accesses to the~%"))
    (display
     (format #f "initial value of x, possibility 110 is "))
    (display
     (format #f "ruled out.~%"))
    (display
     (format #f "Since the (set! x (+ x 1)) statement is "))
    (display
     (format #f "serialized,~%"))
    (display
     (format #f "11 is ruled out (cannot allow P1 to "))
    (display
     (format #f "set x within~%"))
    (display
     (format #f "the serialized (set! x (+ x 1)) "))
    (display
     (format #f "statement).~%"))
    (newline)
    (display
     (format #f "The only possible values for x are "))
    (display
     (format #f "101, 121 and 100.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.39 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
