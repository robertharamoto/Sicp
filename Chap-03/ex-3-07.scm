#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.07                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-account pwd init-bal)
  (define balance init-bal)
  (define password pwd)
  (define consecutive-counter 0)
  (define (reset-cc)
    (begin
      (set! consecutive-counter 0)
      ))
  (define (increment-cc)
    (begin
      (set! consecutive-counter (1+ consecutive-counter))
      ))
  (define (call-the-cops anum)
    (begin
      "call-the-cops"
      ))
  (define (show-balance)
    (begin
      balance
      ))
  (define (withdraw amount)
    (begin
      (if (>= balance amount)
          (begin
            (set! balance (- balance amount))
            balance)
          (begin
            "Insufficient funds"
            ))
      ))
  (define (deposit amount)
    (begin
      (set! balance (+ balance amount))
      balance
      ))
  (define (dispatch secret-pwd m)
    (begin
      (if (and (symbol? secret-pwd)
               (eq? password secret-pwd))
          (begin
            (reset-cc)
            (cond
             ((eq? m 'withdraw)
              (begin
                withdraw
                ))
             ((eq? m 'deposit)
              (begin
                deposit
                ))
             ((eq? m 'balance)
              (begin
                show-balance
                ))
             (else
              (begin
                (display
                 (format #f "Unknown request -- MAKE-ACCOUNT ~a~%"
                         m))
                (quit)
                ))
             ))

          (begin
            (increment-cc)
            (if (> consecutive-counter 7)
                (begin
                  call-the-cops)
                (begin
                  (lambda (anum)
                    "incorrect password")
                  ))
            ))
      ))
  (begin
    dispatch
    ))

;;;#############################################################
;;;#############################################################
(define (make-joint-account acc-obj acc-pwd personal-pwd)
  (define my-acc-obj acc-obj)
  (define my-acc-pwd acc-pwd)
  (define my-password personal-pwd)
  (define consecutive-counter 0)
  (define (reset-cc)
    (begin
      (set! consecutive-counter 0)
      ))
  (define (increment-cc)
    (begin
      (set! consecutive-counter (1+ consecutive-counter))
      ))
  (define (call-the-cops anum)
    (begin
      "call-the-cops"
      ))
  (define (show-balance)
    (begin
      ((my-acc-obj my-acc-pwd 'balance))
      ))
  (define (withdraw amount)
    (begin
      ((my-acc-obj my-acc-pwd 'withdraw) amount)
      ))
  (define (deposit amount)
    (begin
      ((my-acc-obj my-acc-pwd 'deposit) amount)
      ))
  (define (dispatch secret-pwd m)
    (begin
      (if (and (symbol? secret-pwd)
               (eq? my-password secret-pwd))
          (begin
            (reset-cc)
            (cond
             ((eq? m 'withdraw)
              (begin
                withdraw
                ))
             ((eq? m 'deposit)
              (begin
                deposit
                ))
             ((eq? m 'balance)
              (begin
                show-balance
                ))
             (else
              (begin
                (display
                 (format #f "Unknown request -- joint-account ~a~%"
                         m))
                (quit)
                ))
             ))
          (begin
            (increment-cc)
            (if (> consecutive-counter 7)
                (begin
                  call-the-cops)
                (begin
                  (lambda (anum)
                    "incorrect password")
                  ))
            ))
      ))
  (begin
    dispatch
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the bank account objects "))
    (display
     (format #f "created by make-account,~%"))
    (display
     (format #f "with the password modification "))
    (display
     (format #f "described in exercise 3.3.~%"))
    (display
     (format #f "Suppose that our banking system "))
    (display
     (format #f "requires the ability~%"))
    (display
     (format #f "to make joint accounts. Define a "))
    (display
     (format #f "procedure make-joint~%"))
    (display
     (format #f "that accomplishes this. Make-joint "))
    (display
     (format #f "should take three~%"))
    (display
     (format #f "arguments. The first is a "))
    (display
     (format #f "password-protected account.~%"))
    (display
     (format #f "The second argument must match the "))
    (display
     (format #f "password with which~%"))
    (display
     (format #f "the account was defined in order for "))
    (display
     (format #f "the make-joint~%"))
    (display
     (format #f "operation to proceed. The third argument "))
    (display
     (format #f "is a new~%"))
    (display
     (format #f "password. Make-joint is to create an "))
    (display
     (format #f "additional access~%"))
    (display
     (format #f "to the original account using the new "))
    (display
     (format #f "password. For~%"))
    (display
     (format #f "example, if peter-acc is a bank account "))
    (display
     (format #f "with password~%"))
    (display
     (format #f "open-sesame, then:~%"))
    (newline)
    (display
     (format #f "(define paul-acc~%"))
    (display
     (format #f "  (make-joint peter-acc "))
    (display
     (format #f "'open-sesame 'rosebud))~%"))
    (newline)
    (display
     (format #f "will allow one to make transactions on "))
    (display
     (format #f "peter-acc using the~%"))
    (display
     (format #f "name paul-acc and the password rosebud. "))
    (display
     (format #f "You may wish to~%"))
    (display
     (format #f "modify your solution to exercise 3.3 to "))
    (display
     (format #f "accommodate this new~%"))
    (display
     (format #f "feature.~%"))
    (newline)
    (display
     (format #f "(define (make-joint-account "))
    (display
     (format #f "acc-obj acc-pwd personal-pwd)~%"))
    (display
     (format #f "  (define my-acc-obj acc-obj)~%"))
    (display
     (format #f "  (define my-acc-pwd acc-pwd)~%"))
    (display
     (format #f "  (define my-password personal-pwd)~%"))
    (display
     (format #f "  (define consecutive-counter 0)~%"))
    (display
     (format #f "  (define (reset-cc)~%"))
    (display
     (format #f "    (set! consecutive-counter 0))~%"))
    (display
     (format #f "  (define (increment-cc)~%"))
    (display
     (format #f "    (set! consecutive-counter "))
    (display
     (format #f "(1+ consecutive-counter)))~%"))
    (display
     (format #f "  (define (call-the-cops anum)~%"))
    (display
     (format #f "    \"call-the-cops\")~%"))
    (display
     (format #f "  (define (show-balance)~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      ((my-acc-obj my-acc-pwd 'balance))~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      ((my-acc-obj my-acc-pwd "))
    (display
     (format #f "'withdraw) amount)~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      ((my-acc-obj my-acc-pwd "))
    (display
     (format #f "'deposit) amount)~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "  (define (dispatch secret-pwd m)~%"))
    (display
     (format #f "    (if (and (symbol? secret-pwd)~%"))
    (display
     (format #f "             (eq? my-password "))
    (display
     (format #f "secret-pwd))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (reset-cc)~%"))
    (display
     (format #f "          (cond ((eq? m 'withdraw) "))
    (display
     (format #f "withdraw)~%"))
    (display
     (format #f "                ((eq? m 'deposit) "))
    (display
     (format #f "deposit)~%"))
    (display
     (format #f "                ((eq? m 'balance) "))
    (display
     (format #f "show-balance)~%"))
    (display
     (format #f "                (else~%"))
    (display
     (format #f "                 (display~%"))
    (display
     (format #f "                   (format #f \"Unknown "))
    (display
     (format #f "request -- joint-account ~~a~~%\"~%"))
    (display
     (format #f "                         m))~%"))
    (display
     (format #f "                 (quit))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (increment-cc)~%"))
    (display
     (format #f "          (if (> consecutive-counter 7)~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                call-the-cops)~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "                (lambda (anum)~%"))
    (display
     (format #f "                  \"incorrect password\")~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  dispatch)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((peter-acc
           (make-account 'open-sesame 100)))
      (let ((paul-acc
             (make-joint-account peter-acc 'open-sesame 'rosebud)))
        (begin
          (display
           (format
            #f "(define peter-acc (make-account "))
          (display
           (format #f "'open-sesame 100))~%"))
          (display
           (format #f "(define paul-acc "))
          (display
           (format #f "(make-joint-account "))
          (display
           (format #f "'open-sesame 'rosebud))~%"))
          (newline)
          (display
           (format #f "((peter-acc 'open-sesame "))
          (display
           (format #f "'balance)) = ~a~%"
                   ((peter-acc 'open-sesame 'balance))))
          (display
           (format #f "((paul-acc 'rosebud "))
          (display
           (format #f "'balance)) = ~a~%"
                   ((paul-acc 'rosebud 'balance))))
          (newline)
          (display
           (format #f "((peter-acc 'open-sesame"))
          (display
           (format #f " 'withdraw) 10) = ~a~%"
                   ((peter-acc 'open-sesame 'withdraw) 10)))
          (newline)
          (display
           (format #f "((paul-acc 'rosebud "))
          (display
           (format #f "'deposit) 100) = ~a~%"
                   ((paul-acc 'rosebud 'deposit) 100)))
          (newline)
          (display
           (format #f "(1) ((peter-acc "))
          (display
           (format #f "'wrong-pwd 'withdraw) 100) = ~a~%"
                   ((peter-acc 'wrong-pwd 'withdraw) 100)))
          (display
           (format #f "((peter-acc "))
          (display
           (format #f "'open-sesame 'deposit) 100) = ~a~%"
                   ((peter-acc 'open-sesame 'deposit) 100)))
          (newline)
          (do ((ii 1 (1+ ii)))
              ((> ii 8))
            (begin
              (display
               (format #f "(~a) ((paul-acc 'wrong-pwd "
                       ii))
              (display
               (format #f "'withdraw) 100) = ~a~%"
                       ((paul-acc
                         'wrong-pwd 'withdraw) 100)))
              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.07 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
