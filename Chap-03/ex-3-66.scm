#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.66                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-limit astream tolerance)
  (define (local-rec astream prev-val count tol)
    (begin
      (if (srfi-41:stream-null? astream)
          (begin
            (list prev-val count))
          (begin
            (let ((a0 (srfi-41:stream-car astream))
                  (tail-stream (srfi-41:stream-cdr astream)))
              (let ((diff (abs (- a0 prev-val))))
                (begin
                  (if (< diff tol)
                      (begin
                        (list a0 count))
                      (begin
                        (local-rec tail-stream a0 (1+ count) tol)
                        ))
                  )))
            ))
      ))
  (begin
    (local-rec (srfi-41:stream-cdr astream)
               (srfi-41:stream-car astream)
               0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (force-output)
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (pairs (srfi-41:stream-cdr s) (srfi-41:stream-cdr t))))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Examine the stream (pairs integers "))
    (display
     (format #f "integers). Can you~%"))
    (display
     (format #f "make any general comments about the "))
    (display
     (format #f "order in which~%"))
    (display
     (format #f "the pairs are placed into the stream? "))
    (display
     (format #f "For example,~%"))
    (display
     (format #f "about how many pairs precede the pair "))
    (display
     (format #f "(1,100)?~%"))
    (display
     (format #f "the pair (99,100)? the pair (100,100)? "))
    (display
     (format #f "(If you can~%"))
    (display
     (format #f "make precise mathematical statements "))
    (display
     (format #f "here, all the~%"))
    (display
     (format #f "better. But feel free to give more "))
    (display
     (format #f "qualitative answers~%"))
    (display
     (format #f "if you find yourself getting "))
    (display
     (format #f "bogged down.)~%"))
    (newline)
    (display
     (format #f "The interleave function does work as "))
    (display
     (format #f "advertised, it~%"))
    (display
     (format #f "puts the (1 n) pairs in between "))
    (display
     (format #f "other pairs.~%"))
    (newline)
    (display
     (format #f "The (1 n) pair:~%"))
    (display
     (format #f "pair:      (1 1)  (1 2)  (1 3)  (1 4) "))
    (display
     (format #f "(1 5) (1 6) (1 7) ...~%"))
    (display
     (format #f "position:    0      1      3      5     "))
    (display
     (format #f "7     9     11  ...~%"))
    (display
     (format #f "f(1)=0, f(2)=1, f(3)=3=f(2)+2, "))
    (display
     (format #f "f(4)=5=f(3)+2=f(2)+2*2~%"))
    (display
     (format #f "f(5)=f(4)+2=f(2)+3*2~%"))
    (display
     (format #f "f(k)=1+2*(k-2)=2*k-3~%"))
    (newline)
    (display
     (format #f "To find pair (1 100) see position=197, "))
    (display
     (format #f "or there~%"))
    (display
     (format #f "are 197 elements that precede (1 100) "))
    (display
     (format #f "(positions~%"))
    (display
     (format #f "start from 0).~%"))
    (newline)
    (display
     (format #f "For terms of the form (n-1, n) we see "))
    (display
     (format #f "that they~%"))
    (display
     (format #f "occur at positions~%"))
    (display
     (format #f "pair:      (1 2)  (2 3)  (3 4)  (4 5)  "))
    (display
     (format #f "(5 6)  (6 7) ...~%"))
    (display
     (format #f "position:    1      4      10     22     "))
    (display
     (format #f "46     94  ...~%"))
    (display
     (format #f "for (k-2, k-1) that occurs in position m, "))
    (display
     (format #f "(k-1, k) occurs~%"))
    (display
     (format #f "in position 2*m+2.~%"))
    (display
     (format #f "f(k-1) = m, f(k) = 2m+2, then f(2)=1, "))
    (display
     (format #f "f(3)=4=(2*f(2)+2),~%"))
    (display
     (format #f "f(4)=10=(2*f(3)+2)"))
    (display
     (format #f "=(2*2*f(2)+4+2)=2^2+4+2,~%"))
    (display
     (format #f "f(5)=22=(2*f(4)+2)=(4*f(3)+4+2)"))
    (display
     (format #f "=(8*f(2)+8+4+2)~%"))
    (display
     (format #f "=(2^3+2^3+4+2)~%"))
    (display
     (format #f "Aside: Let s(k)=2^1+2^2+2^3+...+2^k,~%"))
    (display
     (format #f "s(k+1)=2^1+2^2+2^3+...+2^k+2^(k+1)~%"))
    (display
     (format #f "So s(k+1)-s(k)=2^(k+1), s(k+1)-2=2*s(k)~%"))
    (display
     (format #f "and 2*s(k)+2-s(k)=2^(k+1)~%"))
    (display
     (format #f "s(k)=2^(k+1)-2~%"))
    (display
     (format #f "So, f(5)=(2^3+2^3+2^2+2)="))
    (display
     (format #f "(2^3+s(3))=(2^3+(2^4-2))=22~%"))
    (display
     (format #f "And we have f(k)=2^(k-2)+(2^(k-1)-2)~%"))
    (display
     (format #f "Check: f(2)=1+2-2=1, "))
    (display
     (format #f "f(3)=2^1+2^2-2=4,~%"))
    (display
     (format #f "f(4)=2^2+2^3-2=10,~%"))
    (display
     (format #f "f(5)=(2^3+2^4-2)=22, "))
    (display
     (format #f "f(6)=2^4+2^5-2=46.~%"))
    (display
     (format #f "To find the pair (99, 100) look at "))
    (display
     (format #f "position~%"))
    (display
     (format #f "f(100)=2^98+2^99-2=9.507x10^29.~%"))
    (newline)
    (display
     (format #f "For terms of the form (n, n) we see "))
    (display
     (format #f "that they occur~%"))
    (display
     (format #f "at positions~%"))
    (display
     (format #f "pair:      (1 1)  (2 2)  (3 3)  (4 4)  "))
    (display
     (format #f "(5 5)  (6 6) ...~%"))
    (display
     (format #f "position:    0      2      6      14     "))
    (display
     (format #f "30     62  ...~%"))
    (display
     (format #f "for (k-1, k-1) that occurs in position m, "))
    (display
     (format #f "(k, k) occurs in~%"))
    (display
     (format #f "position 2*m+2.~%"))
    (display
     (format #f "f(k-1) = m, f(k) = 2m+2, then f(1)=0, "))
    (display
     (format #f "f(2)=2,~%"))
    (display
     (format #f "f(3)=6=(2*f(2)+2)=(2^2+2),~%"))
    (display
     (format #f "f(4)=14=(2*f(3)+2)=(2*(2*f(2)+2)+2)"))
    (display
     (format #f "=(2^3+4+2),~%"))
    (display
     (format #f "f(5)=30=(2*f(4)+2)=(4*f(3)+4+2)"))
    (display
     (format #f "=(8*f(2)+8+4+2)=2^4+8+4+2~%"))
    (display
     (format #f "Aside: from above,~%"))
    (display
     (format #f "s(k)=2^1+2^2+2^3+...+2^k=2^(k+1)-2~%"))
    (display
     (format #f "f(k)=2^k-2~%"))
    (display
     (format #f "Check: f(1)=2-2=0, f(2)=2^2-2=2, "))
    (display
     (format #f "f(3)=2^3-2=6,~%"))
    (display
     (format #f "f(4)=2^4-2=14, f(5)=2^5-2=30,~%"))
    (display
     (format #f "f(6)=2^6-2=62~%"))
    (display
     (format #f "To find the pair (100, 100) look at "))
    (display
     (format #f "position~%"))
    (display
     (format #f "f(100)=2^100-2=1.267x10^30.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((p-int-int-streams (pairs integers integers))
          (cmax 5)
          (ccount 1)
          (nmax 100))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (let ((alist
                   (my-stream-ref p-int-int-streams ii)))
              (let ((a0 (list-ref alist 0))
                    (a1 (list-ref alist 1)))
                (let ((adiff (- a1 a0)))
                  (begin
                    (cond
                     ((equal? adiff 1)
                      (begin
                        (display
                         (format
                          #f "(pairs ~a) = ~a (n-1, n)~%"
                          ii alist))
                        ))
                     ((equal? adiff 0)
                      (begin
                        (display
                         (format
                          #f "(pairs ~a) = ~a (n, n)~%"
                          ii alist))
                        ))
                     (else
                      (begin
                        (display
                         (format
                          #f "(pairs ~a) = ~a~%"
                          ii alist))
                        )))
                    (force-output)
                    ))
                ))
            (if (zero? (modulo ccount cmax))
                (begin
                  (newline)
                  (force-output)
                  ))
            (set! ccount (1+ ccount))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.66 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
