#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.57                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  last updated August 16, 2022                         ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (display
               (ice-9-format:format
                #f "my-stream-map: (stream-car s1) = ~:d~%"
                a1))
              (display
               (ice-9-format:format
                #f "(stream-car s2) = ~:d~%"
                a2))
              (display
               (ice-9-format:format
                #f "(+ (car s1) (car s2)) = ~:d~%"
                (proc a1 a2)))
              (force-output)

              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         ))
     stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define fibonacci-stream
  (begin
    (srfi-41:stream-cons
     0 (srfi-41:stream-cons
        1 (add-streams
           (srfi-41:stream-cdr fibonacci-stream)
           fibonacci-stream)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (fib-explode n)
  (begin
    (cond
     ((<= n 0)
      (begin
        0
        ))
     ((= n 1)
      (begin
        1
        ))
     (else
      (begin
        (+ (fib-explode (- n 1))
           (fib-explode (- n 2)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "How many additions are performed when "))
    (display
     (format #f "we compute the nth~%"))
    (display
     (format #f "Fibonacci number using the definition "))
    (display
     (format #f "of fibs based on~%"))
    (display
     (format #f "the add-streams procedure? Show that "))
    (display
     (format #f "the number of~%"))
    (display
     (format #f "additions would be exponentially "))
    (display
     (format #f "greater if we had~%"))
    (display
     (format #f "implemented (delay <exp>) simply "))
    (display
     (format #f "as (lambda() <exp>),~%"))
    (display
     (format #f "without using the optimization "))
    (display
     (format #f "provided by the~%"))
    (display
     (format #f "memo-proc procedure described in "))
    (display
     (format #f "section 3.5.1.~%"))
    (newline)
    (display
     (format #f "With memoization, the number of additions "))
    (display
     (format #f "for the nth~%"))
    (display
     (format #f "Fibonacci number is n-1 (the first two "))
    (display
     (format #f "numbers require~%"))
    (display
     (format #f "no additions). It's easiest seen using the "))
    (display
     (format #f "illustration~%"))
    (display
     (format #f "in the book:~%"))
    (display
     (format #f "      1  1  2  3  5  8  13  21     = "))
    (display
     (format #f "(stream-cdr fibs)~%"))
    (display
     (format #f "      0  1  1  2  3  5   8  13     = "))
    (display
     (format #f "fibs~%"))
    (display
     (format #f "0  1  1  2  3  5  8  13 21  34     = "))
    (display
     (format #f "fibs~%"))
    (display
     (format #f "      1  2  3  4  5  6   7   8     = "))
    (display
     (format #f "8 additions~%"))
    (display
     (format #f "to calculate the 9th Fibonacci number.~%"))
    (newline)
    (display
     (format #f "If there is no memoization:  first "))
    (display
     (format #f "recall that,~%"))
    (display
     (format #f "(define (stream-ref s n)~%"))
    (display
     (format #f "  (if (= n 0)~%"))
    (display
     (format #f "      (stream-car s)~%"))
    (display
     (format #f "      (stream-ref (stream-cdr s) (- n 1))))~%"))
    (display
     (format #f "(define (stream-map proc s1 s2)~%"))
    (display
     (format #f "  (if (stream-null? s1)~%"))
    (display
     (format #f "      the-empty-stream~%"))
    (display
     (format #f "      (cons-stream (proc "))
    (display
     (format #f "(stream-car s1) (stream-car s2))~%"))
    (display
     (format #f "   (stream-map proc "))
    (display
     (format #f "(stream-cdr s1) (stream-cdr s2)))))~%"))
    (display
     (format #f "(define (add-streams s1 s2)~%"))
    (display
     (format #f "  (stream-map + s1 s2))~%"))
    (display
     (format #f "(define fibs~%"))
    (display
     (format #f "  (cons-stream 0~%"))
    (display
     (format #f "               (cons-stream 1~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "(add-streams (stream-cdr fibs)~%"))
    (display
     (format #f "                                         "))
    (display
     (format #f "fibs))))~%"))
    (newline)
    (display
     (format #f "To compute the 0th and 1st Fibonacci "))
    (display
     (format #f "number requires~%"))
    (display
     (format #f "no additions. To compute the 2nd number, "))
    (display
     (format #f "requires~%"))
    (display
     (format #f "add-streams to perform 1 addition, where "))
    (display
     (format #f "in add-streams~%"))
    (display
     (format #f "(stream-cdr fibs) = "))
    (display
     (format #f "(cons 1 ...),~%"))
    (display
     (format #f "and (car fibs) = 0. To compute the "))
    (display
     (format #f "3rd number:~%"))
    (display
     (format #f "fibs = (cons 0 (cons 1 (cons 1 "))
    (display
     (format #f "(stream-map +~%"))
    (display
     (format #f "    (stream-cdr fibs) fibs))))~%"))
    (display
     (format #f "where in add-streams (stream-cdr fibs) = "))
    (display
     (format #f "(cons 1 ...),~%"))
    (display
     (format #f "and (car fibs) = 1 in fibs.~%"))
    (display
     (format #f "(stream-ref fibs 3)~%"))
    (display
     (format #f "(stream-ref (cons 1 (cons 1 ...)) 2)~%"))
    (display
     (format #f "(stream-ref (cons 1 "))
    (display
     (format #f "(stream-map + (stream-cdr fibs)~%"))
    (display
     (format #f "...)) 1)~%"))
    (display
     (format #f "(stream-car (stream-map + "))
    (display
     (format #f "(stream-cdr fibs)~%"))
    (display
     (format #f "fibs) 0)~%"))
    (display
     (format #f "(+ 1 1) = 2~%"))
    (display
     (format #f "fibs = (cons 0 (cons 1 (cons 1 "))
    (display
     (format #f "(cons 2 (stream-map +~%"))
    (display
     (format #f "(stream-cdr fibs) fibs))))~%"))
    (display
     (format #f "where in stream-map "))
    (display
     (format #f "(stream-cdr fibs) = (cons 2 ...),~%"))
    (display
     (format #f "and (car fibs) = 1.~%"))
    (display
     (format #f "(stream-ref fibs 4)~%"))
    (display
     (format #f "(stream-ref (cons 1~%"))
    (display
     (format #f "(cons 1 ...)) 3)~%"))
    (display
     (format #f "(stream-ref (cons 1 (cons 2 "))
    (display
     (format #f "(stream-map +~%"))
    (display
     (format #f "(stream-cdr fibs) ...)) 2)~%"))
    (display
     (format #f "(stream-car (cons 2 (stream-map "))
    (display
     (format #f "+ (stream-cdr fibs)~%"))
    (display
     (format #f "fibs) 1)~%"))
    (display
     (format #f "(stream-car (stream-map + "))
    (display
     (format #f "(stream-cdr fibs)~%"))
    (display
     (format #f "fibs) 0)~%"))
    (display
     (format #f "(+ 2 1) = 3~%"))
    (display
     (format #f "fibs = (cons 0 (cons 1 (cons 1 "))
    (display
     (format #f "(cons 2 (cons 3~%"))
    (display
     (format #f "(stream-map + (stream-cdr fibs) fibs))))~%"))
    (display
     (format #f "where in stream-map~%"))
    (display
     (format #f "(stream-cdr fibs) = (cons 3 ...),~%"))
    (display
     (format #f "and (car fibs) = 2.~%"))
    (display
     (format #f "(stream-ref fibs 5)~%"))
    (display
     (format #f "(stream-ref (cons 1 (cons 1 ...)) 4)~%"))
    (display
     (format #f "(stream-ref (cons 1 (cons 2 ...)) 3)~%"))
    (display
     (format #f "(stream-ref (cons 2 (cons 3~%"))
    (display
     (format #f "    (stream-map + (stream-cdr fibs) ...))"))
    (display
     (format #f "2)~%"))
    (display
     (format #f "(stream-car (cons 3 "))
    (display
     (format #f "(stream-map +~%"))
    (display
     (format #f "    (stream-cdr fibs) fibs) 1)~%"))
    (display
     (format #f "(stream-car (stream-map + "))
    (display
     (format #f "(stream-cdr fibs)~%"))
    (display
     (format #f "    fibs) 0)~%"))
    (display
     (format #f "(+ 3 2) = 5~%"))
    (display
     (format #f "fibs = (cons 0 (cons 1 (cons 1 (cons 2 "))
    (display
     (format #f "(cons 3 (cons 5~%"))
    (display
     (format #f "(stream-map + (stream-cdr fibs) fibs))))~%"))
    (display
     (format #f "where in stream-map~%"))
    (display
     (format #f "(stream-cdr fibs) = (cons 5 ...)~%"))
    (display
     (format #f "and (car fibs) = 3.~%"))
    (display
     (format #f "(stream-ref fibs 6)~%"))
    (display
     (format #f "(stream-ref (cons 1 (cons 1 ...)) 5)~%"))
    (display
     (format #f "(stream-ref (cons 1 (cons 2 ...)) 4)~%"))
    (display
     (format #f "(stream-ref (cons 2 (cons 3 ...)) 3)~%"))
    (display
     (format #f "(stream-ref (cons 3 (cons 5 "))
    (display
     (format #f "(stream-map + (stream-cdr fibs)~%"))
    (display
     (format #f "...)) 2)~%"))
    (display
     (format #f "(stream-car (cons 5 "))
    (display
     (format #f "(stream-map + (stream-cdr fibs)~%"))
    (display
     (format #f "fibs) 1)~%"))
    (display
     (format #f "(stream-car (stream-map + "))
    (display
     (format #f "(stream-cdr fibs) fibs) 0)~%"))
    (display
     (format #f "(+ 5 3) = 8~%"))
    (newline)
    (display
     (format #f "So we see that the even with the simple "))
    (display
     (format #f "delay with no~%"))
    (display
     (format #f "memoization, the number of additions is "))
    (display
     (format #f "the same as in~%"))
    (display
     (format #f "the memoization case.~%"))
    (newline)
    (display
     (format #f "The version of Fibonacci that does "))
    (display
     (format #f "explode exponentially~%"))
    (display
     (format #f "is the recursively descending "))
    (display
     (format #f "algorithm:~%"))
    (display
     (format #f "(define (fib-explode n)~%"))
    (display
     (format #f "  (cond~%"))
    (display
     (format #f "    ((<= n 0) 0)~%"))
    (display
     (format #f "    ((= n 1) 1)~%"))
    (display
     (format #f "    (else~%"))
    (display
     (format #f "      (+ (fib-explode (- n 1))~%"))
    (display
     (format #f "         (fib-explode (- n 2)))~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f "one can easily see how this version "))
    (display
     (format #f "calculates the~%"))
    (display
     (format #f "same numbers recursively without "))
    (display
     (format #f "memoization.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format
      #f "the following set of calls to stream-ref "))
    (display
     (format
      #f "shows that~%"))
    (display
     (format #f "my-stream-map is calling the "))
    (display
     (format
      #f "addition function~%"))
    (display
     (format
      #f "one time for each new fibonacci "))
    (display
     (format
      #f "number.~%"))

    (newline)
    (timer-module:time-code-macro
     (begin
       (let ((fib-stream fibonacci-stream)
             (nmax 10))
         (begin
           (do ((ii 0 (1+ ii)))
               ((>= ii nmax))
             (begin
               (let ((fib-num (my-stream-ref fib-stream ii)))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "(stream-ref fibonacci-stream ~:d) = ~:d~%"
                     ii fib-num))
                   (force-output)
                   ))
               ))
           ))
       ))

    (newline)
    (timer-module:time-code-macro
     (begin
       (let ((n 30))
         (begin
           (display
            (ice-9-format:format
             #f "(fibonacci-stream ~:d) = ~:d~%"
             n (my-stream-ref fibonacci-stream n)))
           (force-output)
           ))
       ))

    (newline)
    (timer-module:time-code-macro
     (begin
       (let ((n 30))
         (begin
           (display
            (ice-9-format:format
             #f "(fib-explode ~:d) = ~:d~%"
             n (fib-explode n)))
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.57 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
