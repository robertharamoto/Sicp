#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.45                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner thinks our bank-account "))
    (display
     (format #f "system is unnecessarily~%"))
    (display
     (format #f "complex and error-prone now that deposits "))
    (display
     (format #f "and withdrawals~%"))
    (display
     (format #f "aren't automatically serialized. He "))
    (display
     (format #f "suggests that~%"))
    (display
     (format #f "make-account-and-serializer should have "))
    (display
     (format #f "exported the~%"))
    (display
     (format #f "serializer (for use by such procedures as "))
    (display
     (format #f "serialized-exchange),~%"))
    (display
     (format #f "in addition to (rather than instead of), "))
    (display
     (format #f "using it to serialize~%"))
    (display
     (format #f "accounts and deposits as make-account did. "))
    (display
     (format #f "He proposes~%"))
    (display
     (format #f "to redefine accounts as follows:~%"))
    (newline)
    (display
     (format #f "(define (make-account-and-serializer "))
    (display
     (format #f "balance)~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (if (>= balance amount)~%"))
    (display
     (format #f "        (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "               balance)~%"))
    (display
     (format #f "        \"Insufficient funds\"))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (set! balance (+ balance amount))~%"))
    (display
     (format #f "    balance)~%"))
    (display
     (format #f "  (let ((balance-serializer "))
    (display
     (format #f "(make-serializer)))~%"))
    (display
     (format #f "    (define (dispatch m)~%"))
    (display
     (format #f "      (cond ((eq? m 'withdraw) "))
    (display
     (format #f "(balance-serializer withdraw))~%"))
    (display
     (format #f "            ((eq? m 'deposit) "))
    (display
     (format #f "(balance-serializer deposit))~%"))
    (display
     (format #f "            ((eq? m 'balance) "))
    (display
     (format #f "balance)~%"))
    (display
     (format #f "            ((eq? m 'serializer) "))
    (display
     (format #f "balance-serializer)~%"))
    (display
     (format #f "            (else~%"))
    (display
     (format #f "       "))
    (display
     (format #f "(error \"Unknown request "))
    (display
     (format #f "-- MAKE-ACCOUNT\"~%"))
    (display
     (format #f "                         m))))~%"))
    (display
     (format #f "    dispatch))~%"))
    (newline)
    (display
     (format #f "Then deposits are handled as with the "))
    (display
     (format #f "original make-account:~%"))
    (newline)
    (display
     (format #f "(define (deposit account amount)~%"))
    (display
     (format #f " ((account 'deposit) amount))~%"))
    (newline)
    (display
     (format #f "Explain what is wrong with Louis's "))
    (display
     (format #f "reasoning. In particular,~%"))
    (display
     (format #f "consider what happens when "))
    (display
     (format #f "serialized-exchange is called.~%"))
    (newline)
    (display
     (format #f "(define (serialized-exchange "))
    (display
     (format #f "account1 account2)~%"))
    (display
     (format #f "  (let ((serializer1 "))
    (display
     (format #f "(account1 'serializer))~%"))
    (display
     (format #f "        (serializer2 "))
    (display
     (format #f "(account2 'serializer)))~%"))
    (display
     (format #f "    ((serializer1 "))
    (display
     (format #f "(serializer2 exchange))~%"))
    (display
     (format #f "     account1~%"))
    (display
     (format #f "     account2)))~%"))
    (display
     (format #f "(define (exchange account1 account2)~%"))
    (display
     (format #f "  (let ((difference "))
    (display
     (format #f "(- (account1 'balance)~%"))
    (display
     (format #f "                       "))
    (display
     (format #f "(account2 'balance))))~%"))
    (display
     (format #f "    ((account1 'withdraw) difference)~%"))
    (display
     (format #f "    ((account2 'deposit) difference)))~%"))
    (newline)
    (display
     (format #f "It looks like the main difference between "))
    (display
     (format #f "the two versions~%"))
    (display
     (format #f "is that each serializer is called twice "))
    (display
     (format #f "for each account,~%"))
    (display
     (format #f "first called by serialized-exchange, "))
    (display
     (format #f "and called again~%"))
    (display
     (format #f "by Louis Reasoner's version of "))
    (display
     (format #f "make-account.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.45 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
