#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.36                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (inform-about-value constraint)
  (begin
    (constraint 'I-have-a-value)
    ))

;;;#############################################################
;;;#############################################################
(define (inform-about-no-value constraint)
  (begin
    (constraint 'I-lost-my-value)
    ))

;;;#############################################################
;;;#############################################################
(define (adder a1 a2 sum)
  (define (process-new-value)
    (begin
      (cond
       ((and (has-value? a1) (has-value? a2))
        (begin
          (set-value! sum
                      (+ (get-value a1) (get-value a2))
                      me)
          ))
       ((and (has-value? a1) (has-value? sum))
        (begin
          (set-value! a2
                      (- (get-value sum) (get-value a1))
                      me)
          ))
       ((and (has-value? a2) (has-value? sum))
        (begin
          (set-value! a1
                      (- (get-value sum) (get-value a2))
                      me)
          )))
      ))
  (define (process-forget-value)
    (begin
      (forget-value! sum me)
      (forget-value! a1 me)
      (forget-value! a2 me)
      (process-new-value)
      ))
  (define (me request)
    (begin
      (cond
       ((eq? request 'I-have-a-value)
        (begin
          (process-new-value)
          ))
       ((eq? request 'I-lost-my-value)
        (begin
          (process-forget-value)
          ))
       (else
        (begin
          (error "Unknown request -- ADDER" request)
          )))
      ))
  (begin
    (connect a1 me)
    (connect a2 me)
    (connect sum me)
    me
    ))

;;;#############################################################
;;;#############################################################
(define (multiplier m1 m2 product)
  (define (process-new-value)
    (begin
      (cond
       ((or (and (has-value? m1) (= (get-value m1) 0))
            (and (has-value? m2) (= (get-value m2) 0)))
        (begin
          (set-value! product 0 me)
          ))
       ((and (has-value? m1) (has-value? m2))
        (begin
          (set-value! product
                      (* (get-value m1) (get-value m2))
                      me)
          ))
       ((and (has-value? product) (has-value? m1))
        (begin
          (set-value! m2
                      (/ (get-value product) (get-value m1))
                      me)
          ))
       ((and (has-value? product) (has-value? m2))
        (begin
          (set-value! m1
                      (/ (get-value product) (get-value m2))
                      me)
          )))
      ))
  (define (process-forget-value)
    (begin
      (forget-value! product me)
      (forget-value! m1 me)
      (forget-value! m2 me)
      (process-new-value)
      ))
  (define (me request)
    (begin
      (cond
       ((eq? request 'I-have-a-value)
        (begin
          (process-new-value)
          ))
       ((eq? request 'I-lost-my-value)
        (begin
          (process-forget-value)
          ))
       (else
        (begin
          (error "Unknown request -- MULTIPLIER" request)
          )))
      ))
  (begin
    (connect m1 me)
    (connect m2 me)
    (connect product me)
    me
    ))

;;;#############################################################
;;;#############################################################
(define (constant value connector)
  (define (me request)
    (begin
      (error "Unknown request -- CONSTANT" request)
      ))
  (begin
    (connect connector me)
    (set-value! connector value me)
    me
    ))

;;;#############################################################
;;;#############################################################
(define (probe name connector)
  (define (print-probe value)
    (begin
      (display "Probe: ")
      (display name)
      (display " = ")
      (display value)
      (newline)
      (force-output)
      ))
  (define (process-new-value)
    (begin
      (print-probe (get-value connector))
      ))
  (define (process-forget-value)
    (begin
      (print-probe "?")
      ))
  (define (me request)
    (begin
      (cond
       ((eq? request 'I-have-a-value)
        (begin
          (process-new-value)
          ))
       ((eq? request 'I-lost-my-value)
        (begin
          (process-forget-value)
          ))
       (else
        (begin
          (error "Unknown request -- PROBE" request)
          )))
      ))
  (begin
    (connect connector me)
    me
    ))

;;;#############################################################
;;;#############################################################
(define (make-connector)
  (begin
    (let ((value #f) (informant #f) (constraints (list)))
      (begin
        (define (set-my-value newval setter)
          (begin
            (cond
             ((not (has-value? me))
              (begin
                (set! value newval)
                (set! informant setter)
                (for-each-except setter
                                 inform-about-value
                                 constraints)
                ))
             ((not (= value newval))
              (begin
                (error "Contradiction" (list value newval))
                ))
             (else
              (begin
                'ignored
                )))
            ))
        (define (forget-my-value retractor)
          (begin
            (if (eq? retractor informant)
                (begin
                  (set! informant #f)
                  (for-each-except retractor
                                   inform-about-no-value
                                   constraints))
                (begin
                  'ignored
                  ))
            ))
        (define (connect new-constraint)
          (begin
            (if (not (memq new-constraint constraints))
                (begin
                  (set! constraints
                        (cons new-constraint constraints))
                  ))
            (if (has-value? me)
                (begin
                  (inform-about-value new-constraint)
                  ))
            'done
            ))
        (define (me request)
          (begin
            (cond
             ((eq? request 'has-value?)
              (begin
                (if informant
                    (begin
                      #t)
                    (begin
                      #f
                      ))
                ))
             ((eq? request 'value)
              (begin
                value
                ))
             ((eq? request 'set-value!)
              (begin
                set-my-value
                ))
             ((eq? request 'forget)
              (begin
                forget-my-value
                ))
             ((eq? request 'connect)
              (begin
                connect
                ))
             (else
              (begin
                (error "Unknown operation -- CONNECTOR"
                       request)
                )))
            ))
        (begin
          me
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (for-each-except exception procedure list)
  (define (loop items)
    (begin
      (cond
       ((null? items)
        (begin
          'done
          ))
       ((eq? (car items) exception)
        (begin
          (loop (cdr items))
          ))
       (else
        (begin
          (procedure (car items))
          (loop (cdr items))
          )))
      ))
  (begin
    (loop list)
    ))

;;;#############################################################
;;;#############################################################
(define (has-value? connector)
  (begin
    (connector 'has-value?)
    ))

;;;#############################################################
;;;#############################################################
(define (get-value connector)
  (begin
    (connector 'value)
    ))

;;;#############################################################
;;;#############################################################
(define (set-value! connector new-value informant)
  (begin
    ((connector 'set-value!) new-value informant)
    ))

;;;#############################################################
;;;#############################################################
(define (forget-value! connector retractor)
  (begin
    ((connector 'forget) retractor)
    ))

;;;#############################################################
;;;#############################################################
(define (connect connector new-constraint)
  (begin
    ((connector 'connect) new-constraint)
    ))

;;;#############################################################
;;;#############################################################
(define (squarer a b)
  (define (process-new-value)
    (begin
      (if (has-value? b)
          (begin
            (let ((b-value (get-value b)))
              (begin
                (if (< b-value 0)
                    (begin
                      (error
                       "square less than 0 -- SQUARER" b-value)
                      (set-value! a (sqrt b-value) me))
                    (begin
                      (if (has-value? a)
                          (begin
                            (let ((a-value (get-value a)))
                              (begin
                                (set-value!
                                 b (* a-value a-value) me)
                                ))
                            ))
                      ))
                ))
            ))
      ))
  (define (process-forget-value)
    (begin
      (forget-value! b me)
      (forget-value! a me)
      (process-new-value)
      ))
  (define (me request)
    (begin
      (cond
       ((eq? request 'I-have-a-value)
        (begin
          (process-new-value)
          ))
       ((eq? request 'I-lost-my-value)
        (begin
          (process-forget-value)
          ))
       (else
        (begin
          (error "Unknown request -- squarer" request)
          )))
      ))
  (begin
    (connect a me)
    (connect b me)
    me
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose we evaluate the following sequence "))
    (display
     (format #f "of expressions~%"))
    (display
     (format #f "in the global environment:~%"))
    (newline)
    (display
     (format #f "(define a (make-connector))~%"))
    (display
     (format #f "(define b (make-connector))~%"))
    (display
     (format #f "(set-value! a 10 'user)~%"))
    (newline)
    (display
     (format #f "At some time during evaluation of the "))
    (display
     (format #f "set-value!, the~%"))
    (display
     (format #f "following expression from the connector's "))
    (display
     (format #f "local procedure~%"))
    (display
     (format #f "is evaluated:~%"))
    (newline)
    (display
     (format #f "(for-each-except setter "))
    (display
     (format #f "inform-about-value constraints)~%"))
    (newline)
    (display
     (format #f "Draw an environment diagram showing the "))
    (display
     (format #f "environment in which~%"))
    (display
     (format #f "the above expression is evaluated.~%"))
    (newline)
    (display
     (format #f "(define a (make-connector))~%"))
    (display
     (format #f "  -> E1: { local-value=false,~%"))
    (display
     (format #f "    informant=false, constraints=(list), "))
    (display
     (format #f "global }~%"))
    (newline)
    (display
     (format #f "When (set-value! a 10 'user) is "))
    (display
     (format #f "executed:~%"))
    (newline)
    (display
     (format #f "(define (set-value! connector "))
    (display
     (format #f "new-value informant)~%"))
    (display
     (format #f "  ((connector 'set-value!) "))
    (display
     (format #f "new-value informant))~%"))
    (display
     (format #f "(set-value! a 10 'user)~%"))
    (display
     (format #f "  -> E2: "))
    (display
     (format #f "{ connector=a, new-value=10, "))
    (display
     (format #f "informant='user,~%"))
    (display
     (format #f "    E1, global }~%"))
    (display
     (format #f "(connector 'set-value!) returns the "))
    (display
     (format #f "make-connector's~%"))
    (display
     (format #f "local function "))
    (display
     (format #f "set-my-value :~%"))
    (display
     (format #f "  -> E3: { me=a, newval=10,~%"))
    (display
     (format #f "    setter='user, E2, global }~%"))
    (display
     (format #f "since a doesn't have a value, E1 becomes "))
    (display
     (format #f "(through environment~%"))
    (display
     (format #f "E3/E2/E1),~%"))
    (display
     (format #f "  (E1: { local-value=10, "))
    (display
     (format #f "informant='user,~%"))
    (display
     (format #f "    constraints=(list), global }~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.36 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
