#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.41                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle worries that it would be "))
    (display
     (format #f "better to implement~%"))
    (display
     (format #f "the bank account as follows (where the "))
    (display
     (format #f "commented line has~%"))
    (display
     (format #f "been changed):~%"))
    (newline)
    (display
     (format #f "(define (make-account balance)~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (if (>= balance amount)~%"))
    (display
     (format #f "        (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "               balance)~%"))
    (display
     (format #f "        \"Insufficient funds\"))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (set! balance "))
    (display
     (format #f "(+ balance amount))~%"))
    (display
     (format #f "    balance)~%"))
    (display
     (format #f "  (let ((protected "))
    (display
     (format #f "(make-serializer)))~%"))
    (display
     (format #f "    (define (dispatch m)~%"))
    (display
     (format #f "      (cond ((eq? m 'withdraw) "))
    (display
     (format #f "(protected withdraw))~%"))
    (display
     (format #f "            ((eq? m 'deposit) "))
    (display
     (format #f "(protected deposit))~%"))
    (display
     (format #f "            ((eq? m 'balance)~%"))
    (display
     (format #f "             ((protected "))
    (display
     (format #f "(lambda () balance)))) ; serialized~%"))
    (display
     (format #f "            (else~%"))
    (display
     (format #f "              "))
    (display
     (format #f "(error \"Unknown request "))
    (display
     (format #f "-- MAKE-ACCOUNT\"~%"))
    (display
     (format #f "                         m))))~%"))
    (display
     (format #f "    dispatch))~%"))
    (newline)
    (display
     (format #f "because allowing unserialized access to "))
    (display
     (format #f "the bank balance~%"))
    (display
     (format #f "can result in anomalous behavior. Do you "))
    (display
     (format #f "agree? Is there~%"))
    (display
     (format #f "any scenario that demonstrates Ben's "))
    (display
     (format #f "concern?~%"))
    (newline)
    (display
     (format #f "The serializer protects the balance from "))
    (display
     (format #f "being updated~%"))
    (display
     (format #f "before the entire transaction (the "))
    (display
     (format #f "protected withdraw or~%"))
    (display
     (format #f "deposit), has completed.  There is no "))
    (display
     (format #f "need to protect~%"))
    (display
     (format #f "the balance request, since it will report "))
    (display
     (format #f "the valid~%"))
    (display
     (format #f "balance at the stated time. I don't agree "))
    (display
     (format #f "with Ben~%"))
    (display
     (format #f "Bitdiddle.~%"))
    (newline)
    (display
     (format #f "There is a scenario where Peter and Paul "))
    (display
     (format #f "have a joint~%"))
    (display
     (format #f "account, and Peter withdraws money, at "))
    (display
     (format #f "the same time~%"))
    (display
     (format #f "that Paul checks the balance. It may "))
    (display
     (format #f "happen that Paul~%"))
    (display
     (format #f "sees a balance before the withdraw "))
    (display
     (format #f "transaction has~%"))
    (display
     (format #f "completed.  However this is a valid "))
    (display
     (format #f "possibility with~%"))
    (display
     (format #f "any joint account, so should not be a "))
    (display
     (format #f "concern. For~%"))
    (display
     (format #f "example, suppose Paul sees he has 100 in "))
    (display
     (format #f "the joint~%"))
    (display
     (format #f "account, but Peter withdraws 50 at the "))
    (display
     (format #f "same moment,~%"))
    (display
     (format #f "then Paul may be confused when trying "))
    (display
     (format #f "to withdraw 60.~%"))
    (display
     (format #f "From the banks perspective, Paul's "))
    (display
     (format #f "failed withdraw~%"))
    (display
     (format #f "attempt was not as serious as it "))
    (display
     (format #f "left the balance~%"))
    (display
     (format #f "in a consistant state.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.41 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
