#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.14                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The following procedure is quite "))
    (display
     (format #f "useful, although obscure:~%"))
    (display
     (format #f "(define (mystery x)~%"))
    (display
     (format #f "  (define (loop x y)~%"))
    (display
     (format #f "    (if (null? x)~%"))
    (display
     (format #f "        y~%"))
    (display
     (format #f "        (let ((temp (cdr x)))~%"))
    (display
     (format #f "          (set-cdr! x y)~%"))
    (display
     (format #f "          (loop temp x))))~%"))
    (display
     (format #f "  (loop x (list)))~%"))
    (display
     (format #f "Loop uses the \"temporary\" variable "))
    (display
     (format #f "temp to hold the old~%"))
    (display
     (format #f "value of the cdr of x, since the set-cdr! "))
    (display
     (format #f "on the next~%"))
    (display
     (format #f "line destroys the cdr. Explain what "))
    (display
     (format #f "mystery does in~%"))
    (display
     (format #f "general. Suppose v is defined by~%"))
    (display
     (format #f "(define v (list 'a 'b 'c 'd)).~%"))
    (newline)
    (display
     (format #f "Draw the box-and-pointer diagram that "))
    (display
     (format #f "represents the list~%"))
    (display
     (format #f "to which v is bound. Suppose that we "))
    (display
     (format #f "now evaluate~%"))
    (display
     (format #f "(define w (mystery v)). Draw box-and-pointer "))
    (display
     (format #f "diagrams~%"))
    (display
     (format #f "that show the structures v and w after "))
    (display
     (format #f "evaluating this~%"))
    (display
     (format #f "expression. What would be printed as the "))
    (display
     (format #f "values of v~%"))
    (display
     (format #f "and w?~%"))
    (newline)
    (display
     (format #f "mystery reverses the list v, but "))
    (display
     (format #f "destroys it in~%"))
    (display
     (format #f "the process.~%"))
    (display
     (format #f "(define v (list 'a 'b 'c 'd))~%"))
    (display
     (format #f "v -> [ 'a, --]-->[ 'b, "))
    (display
     (format #f "--]-->[ 'c, --]-->[ 'd, / ]~%"))
    (newline)
    (display
     (format #f "(define w (mystery v))~%"))
    (display
     (format #f "(1) (loop v (list))~%"))
    (display
     (format #f "where v -> [ 'a, --]-->[ 'b, "))
    (display
     (format #f "--]-->[ 'c, --]-->[ 'd, / ]~%"))
    (display
     (format #f "(2) temp = (list 'b 'c 'd)~%"))
    (display
     (format #f "(3) (set-cdr! v (list)), "))
    (display
     (format #f "then v becomes [ 'a, / ]~%"))
    (display
     (format #f "(4) (loop (list 'b 'c 'd) v)~%"))
    (display
     (format #f "(5) temp =  (list 'c 'd)~%"))
    (display
     (format #f "(6) (set-cdr! x y), then temp "))
    (display
     (format #f "(from 2) becomes~%"))
    (display
     (format #f "[ 'b, --]-->[ 'a, / ]~%"))
    (display
     (format #f "(7) (loop (list 'c 'd) "))
    (display
     (format #f "(list 'b 'a))~%"))
    (display
     (format #f "(8) temp = (list 'd)~%"))
    (display
     (format #f "(9) (set-cdr! x y), then temp "))
    (display
     (format #f "(from 5) becomes~%"))
    (display
     (format #f "[ 'c --]-->[ 'b, --]-->[ 'a, / ]~%"))
    (display
     (format #f "(10) (loop (list 'd) "))
    (display
     (format #f "(list 'c 'b 'a))~%"))
    (display
     (format #f "(11) temp = null~%"))
    (display
     (format #f "(12) (set-cdr! x y), then temp "))
    (display
     (format #f "(from 8) becomes~%"))
    (display
     (format #f "    (list 'd 'c 'b 'a)~%"))
    (display
     (format #f "(13) (loop null "))
    (display
     (format #f "(list 'd 'c 'b 'a))~%"))
    (display
     (format #f "(14) (list 'd 'c 'b 'a)~%"))
    (newline)
    (display
     (format #f "v = (list 'a)~%"))
    (display
     (format #f "w = (list 'd 'c 'b 'a)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.14 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
