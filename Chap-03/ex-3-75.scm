#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.75                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones
  (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x) (list (srfi-41:stream-car s) x))
       (srfi-41:stream-cdr t))
      (pairs
       (srfi-41:stream-cdr s) (srfi-41:stream-cdr t))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Unfortunately, Alyssa's zero-crossing "))
    (display
     (format #f "detector in exercise~%"))
    (display
     (format #f "3.74 proves to be insufficient, because "))
    (display
     (format #f "the noisy signal~%"))
    (display
     (format #f "from the sensor leads to spurious zero "))
    (display
     (format #f "crossings. Lem E.~%"))
    (display
     (format #f "Tweakit, a hardware specialist, suggests "))
    (display
     (format #f "that Alyssa smooth~%"))
    (display
     (format #f "the signal to filter out the noise "))
    (display
     (format #f "before extracting the~%"))
    (display
     (format #f "zero crossings.  Alyssa takes his "))
    (display
     (format #f "advice and decides to~%"))
    (display
     (format #f "extract the zero crossings from the "))
    (display
     (format #f "signal constructed~%"))
    (display
     (format #f "by averaging each value of the sense "))
    (display
     (format #f "data with the~%"))
    (display
     (format #f "previous value. She explains the problem "))
    (display
     (format #f "to her assistant,~%"))
    (display
     (format #f "Louis Reasoner, who attempts to implement "))
    (display
     (format #f "the idea,~%"))
    (display
     (format #f "altering Alyssa's program as "))
    (display
     (format #f "follows:~%"))
    (newline)
    (display
     (format #f "(define (make-zero-crossings "))
    (display
     (format #f "input-stream last-value)~%"))
    (display
     (format #f "  (let ((avpt (/ (+ "))
    (display
     (format #f "(stream-car input-stream)~%"))
    (display
     (format #f "           last-value) 2)))~%"))
    (display
     (format #f "    (cons-stream (sign-change-detector "))
    (display
     (format #f "avpt last-value)~%"))
    (display
     (format #f "                 (make-zero-crossings "))
    (display
     (format #f "(stream-cdr input-stream)~%"))
    (display
     (format #f "                                      "))
    (display
     (format #f "avpt))))~%"))
    (newline)
    (display
     (format #f "This does not correctly implement Alyssa's "))
    (display
     (format #f "plan. Find the~%"))
    (display
     (format #f "bug that Louis has installed and fix it "))
    (display
     (format #f "without changing~%"))
    (display
     (format #f "the structure of the program. (Hint: You "))
    (display
     (format #f "will need to~%"))
    (display
     (format #f "increase the number of arguments to "))
    (display
     (format #f "make-zero-crossings.)~%"))
    (newline)
    (display
     (format #f "(define (make-zero-crossings~%"))
    (display
     (format #f "    input-stream last-value last-avpt)~%"))
    (display
     (format #f "  (let ((avpt (/ (+ "))
    (display
     (format #f "(stream-car input-stream) "))
    (display
     (format #f "last-value) 2)))~%"))
    (display
     (format #f "    (cons-stream "))
    (display
     (format #f "(sign-change-detector avpt last-avpt)~%"))
    (display
     (format #f "                 (make-zero-crossings "))
    (display
     (format #f "(stream-cdr input-stream)~%"))
    (display
     (format #f "                                      "))
    (display
     (format #f "(stream-car input-stream) avpt))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.75 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
