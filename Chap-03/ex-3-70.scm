#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.70                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter
         pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (merge-weighted s1 s2 wfunc)
  (begin
    (cond
     ((srfi-41:stream-null? s1)
      (begin
        s2
        ))
     ((srfi-41:stream-null? s2)
      (begin
        s1
        ))
     (else
      (begin
        (let ((s1car (srfi-41:stream-car s1))
              (s2car (srfi-41:stream-car s2)))
          (let ((w1 (wfunc s1car))
                (w2 (wfunc s2car)))
            (begin
              (cond
               ((< w1 w2)
                (begin
                  (srfi-41:stream-cons
                   s1car
                   (merge-weighted
                    (srfi-41:stream-cdr s1) s2 wfunc))
                  ))
               ((> w1 w2)
                (begin
                  (srfi-41:stream-cons
                   s2car
                   (merge-weighted
                    s1 (srfi-41:stream-cdr s2) wfunc))
                  ))
               (else
                (begin
                  (srfi-41:stream-cons
                   s1car
                   (srfi-41:stream-cons
                    s2car
                    (merge-weighted
                     (srfi-41:stream-cdr s1)
                     (srfi-41:stream-cdr s2) wfunc)))
                  )))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-1 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ a0 a1)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-2 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ (* 2 a0) (* 3 a1) (* 5 a0 a1))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x) (list (srfi-41:stream-car s) x))
       (srfi-41:stream-cdr t))
      (pairs (srfi-41:stream-cdr s) (srfi-41:stream-cdr t))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (weighted-pairs s t wfunc)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (merge-weighted
      (srfi-41:stream-map
       (lambda (x) (list (srfi-41:stream-car s) x))
       (srfi-41:stream-cdr t))
      (weighted-pairs (srfi-41:stream-cdr s)
                      (srfi-41:stream-cdr t) wfunc)
      wfunc))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It would be nice to be able to generate "))
    (display
     (format #f "streams in which~%"))
    (display
     (format #f "the pairs appear in some useful order, "))
    (display
     (format #f "rather than in~%"))
    (display
     (format #f "the order that results from an ad hoc "))
    (display
     (format #f "interleaving process.~%"))
    (display
     (format #f "We can use a technique similar to the "))
    (display
     (format #f "merge procedure~%"))
    (display
     (format #f "of exercise 3.56, if we define a way "))
    (display
     (format #f "to say that one~%"))
    (display
     (format #f "pair of integers is \"less than\" "))
    (display
     (format #f "another. One way~%"))
    (display
     (format #f "to do this is to define a \"weighting "))
    (display
     (format #f "function\" W(i, j)~%"))
    (display
     (format #f "and stipulate that (i1,j1) is less "))
    (display
     (format #f "than (i2,j2) if~%"))
    (display
     (format #f "W(i1,j1) < W(i2,j2). Write a procedure "))
    (display
     (format #f "merge-weighted~%"))
    (display
     (format #f "that is like merge, except that "))
    (display
     (format #f "merge-weighted~%"))
    (display
     (format #f "takes an additional argument weight, "))
    (display
     (format #f "which is a~%"))
    (display
     (format #f "procedure that computes the weight of a "))
    (display
     (format #f "pair, and is~%"))
    (display
     (format #f "used to determine the order in which "))
    (display
     (format #f "elements should~%"))
    (display
     (format #f "appear in the resulting merged stream. "))
    (display
     (format #f "Using this,~%"))
    (display
     (format #f "generalize pairs to a procedure "))
    (display
     (format #f "weighted-pairs that~%"))
    (display
     (format #f "takes two streams, together with a "))
    (display
     (format #f "procedure that~%"))
    (display
     (format #f "computes a weighting function, and "))
    (display
     (format #f "generates the stream~%"))
    (display
     (format #f "of pairs, ordered according to weight. "))
    (display
     (format #f "Use your procedure~%"))
    (display
     (format #f "to generate:~%"))
    (newline)
    (display
     (format #f "a. the stream of all pairs of positive "))
    (display
     (format #f "integers (i,j)~%"))
    (display
     (format #f "with i < j ordered according to "))
    (display
     (format #f "the sum i + j~%"))
    (display
     (format #f "b. the stream of all pairs of "))
    (display
     (format #f "positive integers~%"))
    (display
     (format #f "(i,j) with i < j, where neither i nor "))
    (display
     (format #f "j is~%"))
    (display
     (format #f "divisible by 2, 3, or 5, and the pairs "))
    (display
     (format #f "are ordered~%"))
    (display
     (format #f "according to the sum 2 i + 3 j + 5 i j.~%"))
    (newline)
    (display
     (format #f "(define (merge-weighted s1 s2 wfunc)~%"))
    (display
     (format #f "  (cond~%"))
    (display
     (format #f "   ((srfi-41:stream-null? s1) s2)~%"))
    (display
     (format #f "   ((srfi-41:stream-null? s2) s1)~%"))
    (display
     (format #f "   (else~%"))
    (display
     (format #f "    (let ((s1car (srfi-41:stream-car s1))~%"))
    (display
     (format #f "          (s2car (srfi-41:stream-car s2)))~%"))
    (display
     (format #f "      (let ((w1 (wfunc s1car))~%"))
    (display
     (format #f "            (w2 (wfunc s2car)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (cond~%"))
    (display
     (format #f "           ((< w1 w2)~%"))
    (display
     (format #f "            (srfi-41:stream-cons~%"))
    (display
     (format #f "             s1car (merge-weighted~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "    (srfi-41:stream-cdr s1)~%"))
    (display
     (format #f "                   s2 wfunc)))~%"))
    (display
     (format #f "           ((> w1 w2)~%"))
    (display
     (format #f "            (srfi-41:stream-cons~%"))
    (display
     (format #f "             s2car (merge-weighted~%"))
    (display
     (format #f "                      "))
    (display
     (format #f "s1 (srfi-41:stream-cdr s2)~%"))
    (display
     (format #f "               wfunc)))~%"))
    (display
     (format #f "           (else~%"))
    (display
     (format #f "            (srfi-41:stream-cons~%"))
    (display
     (format #f "             s1car~%"))
    (display
     (format #f "             (srfi-41:stream-cons~%"))
    (display
     (format #f "              s2car~%"))
    (display
     (format #f "              (merge-weighted~%"))
    (display
     (format #f "                 (srfi-41:stream-cdr s1)~%"))
    (display
     (format #f "                 (srfi-41:stream-cdr s2) "))
    (display
     (format #f "wfunc)))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "    )))~%"))
    (newline)
    (display
     (format #f "(define (weighted-pairs s t wfunc)~%"))
    (display
     (format #f "  (srfi-41:stream-cons~%"))
    (display
     (format #f "   (list (srfi-41:stream-car s)~%"))
    (display
     (format #f "         (srfi-41:stream-car t))~%"))
    (display
     (format #f "   (merge-weighted~%"))
    (display
     (format #f "    (srfi-41:stream-map~%"))
    (display
     (format #f "     (lambda (x)~%"))
    (display
     (format #f "       (list (srfi-41:stream-car s) x))~%"))
    (display
     (format #f "     (srfi-41:stream-cdr t))~%"))
    (display
     (format #f "    (weighted-pairs~%"))
    (display
     (format #f "      (srfi-41:stream-cdr s)~%"))
    (display
     (format #f "      (srfi-41:stream-cdr t) wfunc)~%"))
    (display
     (format #f "    wfunc)~%"))
    (display (format #f "   ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((nmax 20))
      (let ((t2-int-streams
             (weighted-pairs
              integers integers weight-func-1))
            (cmax 5)
            (ccount 1)
            (dup-htable (make-hash-table nmax)))
        (begin
          (display
           (format
            #f "(a) weight-function = (+ i j)~%"))
          (do ((ii 0 (1+ ii)))
              ((>= ii nmax))
            (begin
              (let ((alist (my-stream-ref t2-int-streams ii)))
                (begin
                  (display
                   (format
                    #f "(weighted-pairs(+ i j) ~a) " ii))
                  (display
                   (format #f "= ~a~%" alist))
                  (force-output)

                  (let ((a-exists
                         (hash-ref dup-htable alist #f)))
                    (begin
                      (if (equal? a-exists #f)
                          (begin
                            (hash-set! dup-htable alist ccount))
                          (begin
                            (display
                             (format
                              #f "duplicate entry ~a at "
                              alist))
                            (display
                             (format
                              #f "position ~a and ~a~%"
                              a-exists ccount))
                            (force-output)
                            ))
                      ))

                  (if (zero? (modulo ccount cmax))
                      (begin
                        (newline)
                        (force-output)
                        ))

                  (set! ccount (1+ ccount))
                  ))
              ))
          )))

    (let ((nmax 20))
      (let ((t2-int-streams
             (weighted-pairs
              integers integers weight-func-2))
            (cmax 5)
            (ccount 1)
            (dup-htable (make-hash-table nmax)))
        (begin
          (display
           (format
            #f "(b) weight-function = (+ 2i 3j 5ij)~%"))
          (do ((ii 0 (1+ ii)))
              ((>= ii nmax))
            (begin
              (let ((alist
                     (my-stream-ref t2-int-streams ii)))
                (begin
                  (display
                   (format
                    #f "(weighted-pairs(+ 2i 3j 5ij) "))
                  (display
                   (format #f "~a) = ~a~%"
                           ii alist))
                  (force-output)

                  (let ((a-exists
                         (hash-ref dup-htable alist #f)))
                    (begin
                      (if (equal? a-exists #f)
                          (begin
                            (hash-set!
                             dup-htable alist ccount))
                          (begin
                            (display
                             (format
                              #f "duplicate entry ~a at "
                              alist))
                            (display
                             (format
                              #f "position ~a and ~a~%"
                              a-exists ccount))
                            (force-output)
                            ))
                      ))

                  (if (zero? (modulo ccount cmax))
                      (begin
                        (newline)
                        (force-output)
                        ))

                  (set! ccount (1+ ccount))
                  ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.70 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
