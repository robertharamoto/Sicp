#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.19                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (has-cycle? x)
  (define (local-count-iter next-list)
    (begin
      (cond
       ((not (pair? next-list))
        (begin
          #f
          ))
       (else
        (begin
          (let ((next-cdr (cdr next-list)))
            (begin
              (cond
               ((not (pair? next-cdr))
                (begin
                  #f
                  ))
               ((equal? next-cdr x)
                (begin
                  #t
                  ))
               ((equal? next-cdr next-list)
                (begin
                  #t
                  ))
               (else
                (begin
                  (local-count-iter next-cdr)
                  )))
              ))
          )))
      ))
  (begin
    (local-count-iter x)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Redo exercise 3.18 using an algorithm "))
    (display
     (format #f "that takes only a~%"))
    (display
     (format #f "constant amount of space. (This "))
    (display
     (format #f "requires a very clever~%"))
    (display
     (format #f "idea.)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x (cons 'a (cons 'b (cons 'c (list))))))
      (begin
        (display
         (format #f "(define x (cons 'a (cons 'b "))
        (display
         (format #f "(cons 'c (list)))))~%"))
        (display
         (format #f "(has-cycle? x)~%"))
        (display
         (format #f "~a~%" (has-cycle? x)))
        ))

    (newline)
    (let ((x (cons 'a 'b)))
      (let ((y (cons x (cons x (list)))))
        (begin
          (display
           (format #f "(define x (cons 'a 'b))~%"))
          (display
           (format #f "(define y (cons x "))
          (display
           (format #f "(cons x (list))))~%"))
          (display
           (format #f "(has-cycle? y)~%"))
          (display
           (format #f "~a~%" (has-cycle? y)))
          )))

    (newline)
    (let ((x (cons 'a 'b)))
      (let ((y (cons x x)))
        (let ((z (cons y y)))
          (begin
            (display
             (format #f "(define x (cons 'a 'b))~%"))
            (display
             (format #f "(define y (cons x x))~%"))
            (display
             (format #f "(define z (cons y y))~%"))
            (display
             (format #f "(has-cycle? z)~%"))
            (display
             (format #f "~a~%" (has-cycle? z)))
            ))
        ))

    (newline)
    (let ((x (cons 'a 'b)))
      (let ((y (cons x x)))
        (let ((z (cons y y)))
          (begin
            (set-cdr! z z)
            (display
             (format #f "(define x (cons 'a 'b))~%"))
            (display
             (format #f "(define y (cons x x))~%"))
            (display
             (format #f "(define z (cons y y))~%"))
            (display
             (format #f "(set-cdr! z z)~%"))
            (display
             (format #f "(has-cycle? z)~%"))
            (display
             (format #f "~a~%" (has-cycle? z)))
            ))
        ))

    (newline)
    (let ((x (cons 'a 'b)))
      (let ((y (cons x x)))
        (let ((z (cons y y)))
          (begin
            (set-cdr! y y)
            (display
             (format #f "(define x (cons 'a 'b))~%"))
            (display
             (format #f "(define y (cons x x))~%"))
            (display
             (format #f "(define z (cons y y))~%"))
            (display
             (format #f "(set-cdr! y y)~%"))
            (display
             (format #f "(has-cycle? z)~%"))
            (display
             (format #f "~a~%" (has-cycle? z)))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.19 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
