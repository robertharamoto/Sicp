#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.59                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  last updated August 16, 2022                         ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         ))
     stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (integrate-series astream)
  (begin
    (srfi-41:stream-map / astream integers)
    ))

;;;#############################################################
;;;#############################################################
(define cos-stream
  (srfi-41:stream-cons
   1 (scale-stream (integrate-series sin-stream) -1)))

;;;#############################################################
;;;#############################################################
(define sin-stream
  (srfi-41:stream-cons
   0 (integrate-series cos-stream)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In section 2.5.3 we saw how to implement "))
    (display
     (format #f "a polynomial~%"))
    (display
     (format #f "arithmetic system representing polynomials "))
    (display
     (format #f "as lists of~%"))
    (display
     (format #f "terms. In a similar way, we can work "))
    (display
     (format #f "with power series,~%"))
    (display
     (format #f "such as:~%"))
    (newline)
    (display
     (format #f "e^x = 1+x+x^2/2+x^3/3!+x^4/4!+...~%"))
    (display
     (format #f "cos x = 1-x^2/2+x^4/4!+...~%"))
    (display
     (format #f "sin x = x-x^3/3!+x^5/5!+...~%"))
    (newline)
    (display
     (format #f "represented as infinite streams. We will "))
    (display
     (format #f "represent the~%"))
    (display
     (format #f "series a0 + a1*x + a2*x^2 + "))
    (display
     (format #f "a3*x^3 + ···~%"))
    (display
     (format #f "as the stream whose elements are the "))
    (display
     (format #f "coefficients~%"))
    (display
     (format #f "a0, a1, a2, a3, ....~%"))
    (newline)
    (display
     (format #f "a. The integral of the series~%"))
    (display
     (format #f "a0 + a1 x + a2 x2 + a3 x3 + ···~%"))
    (display
     (format #f "is the series~%"))
    (display
     (format #f "c+a0*x+1/2*a1*x^2+1/3*a2*a^3+1/4*a3*x^4"))
    (display
     (format #f "+...~%"))
    (display
     (format #f "where c is any constant. Define a "))
    (display
     (format #f "procedure~%"))
    (display
     (format #f "integrate-series that takes as input "))
    (display
     (format #f "a stream~%"))
    (display
     (format #f "a0, a1, a2, ... representing a power "))
    (display
     (format #f "series and returns~%"))
    (display
     (format #f "the stream a0, (1/2)a1, (1/3)a2, ... of "))
    (display
     (format #f "coefficients of~%"))
    (display
     (format #f "the non-constant terms of the integral "))
    (display
     (format #f "of the series.~%"))
    (display
     (format #f "(Since the result has no constant term, "))
    (display
     (format #f "it doesn't~%"))
    (display
     (format #f "represent a power series; when we use "))
    (display
     (format #f "integrate-series,~%"))
    (display
     (format #f "we will cons on the appropriate "))
    (display
     (format #f "constant.)~%"))
    (newline)
    (display
     (format #f "b. The function x -> e^x is its own "))
    (display
     (format #f "derivative. This~%"))
    (display
     (format #f "implies that e^x and the integral of "))
    (display
     (format #f "e^x are the~%"))
    (display
     (format #f "same series, except for the constant "))
    (display
     (format #f "term, which~%"))
    (display
     (format #f "is e^0 = 1. Accordingly, we can generate "))
    (display
     (format #f "the series for~%"))
    (display
     (format #f "e^x as~%"))
    (display
     (format #f "(define exp-series~%"))
    (display
     (format #f "  (cons-stream 1 "))
    (display
     (format #f "(integrate-series exp-series)))~%"))
    (display
     (format #f "Show how to generate the series for sine "))
    (display
     (format #f "and cosine,~%"))
    (display
     (format #f "starting from the facts that the "))
    (display
     (format #f "derivative of sine~%"))
    (display
     (format #f "is cosine and the derivative of cosine "))
    (display
     (format #f "is the negative~%"))
    (display
     (format #f "of sine:~%"))
    (newline)
    (display
     (format #f "(define cosine-series~%"))
    (display
     (format #f "  (cons-stream 1 <??>))~%"))
    (display
     (format #f "(define sine-series~%"))
    (display
     (format #f "  (cons-stream 0 <??>))~%"))
    (newline)
    (display
     (format #f "(a)~%"))
    (display
     (format #f "(define (integrate-series astream)~%"))
    (display
     (format #f "  (srfi-41:stream-map / "))
    (display
     (format #f "astream integers))~%"))
    (newline)
    (display
     (format #f "(b)~%"))
    (display
     (format #f "(define cos-series~%"))
    (display
     (format #f "  (cons-stream 1 (scale-stream "))
    (display
     (format #f "(integrate-series sin-series) -1)))~%"))
    (display
     (format #f "(define sin-series~%"))
    (display
     (format #f "  (cons-stream 0 "))
    (display
     (format #f "(integrate-series cos-series)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "(stream-ref cos-stream)~%"))
    (let ((c-stream cos-stream)
          (nmax 7))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (display
             (format
              #f "(stream-ref cos-stream ~a) = ~a~%"
              ii (my-stream-ref c-stream ii)))
            (force-output)
            ))
        ))

    (newline)
    (display
     (format #f "(stream-ref sin-stream)~%"))
    (let ((s-stream sin-stream)
          (nmax 7))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (display
             (format
              #f "(stream-ref sin-stream ~a) = ~a~%"
              ii (my-stream-ref s-stream ii)))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.59 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
