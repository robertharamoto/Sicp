#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.52                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  last updated August 16, 2022                         ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (memo-proc proc)
  (begin
    (let ((already-run? #f) (result #f))
      (begin
        (lambda ()
          (begin
            (if (not already-run?)
                (begin
                  (set! result (proc))
                  (set! already-run? #t)
                  result)
                (begin
                  result
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-delay lexpr)
  (begin
    (memo-proc
     (lambda ()
       (begin
         lexpr
         )))
    ))

;;;#############################################################
;;;#############################################################
(define (original-my-delay lexpr)
  (begin
    (lambda ()
      (begin
        lexpr
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-force delayed-object)
  (begin
    (delayed-object)
    ))

;;;#############################################################
;;;#############################################################
(define the-empty-stream srfi-41:stream-null)

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s)
  (begin
    (if (srfi-41:stream-null? s)
        (begin
          the-empty-stream)
        (begin
          (let ((aa (proc (srfi-41:stream-car s))))
            (begin
              (srfi-41:stream-cons
               aa (my-stream-map
                   proc (srfi-41:stream-cdr s)))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-for-each proc s)
  (begin
    (if (srfi-41:stream-null? s)
        (begin
          'done)
        (begin
          (proc (srfi-41:stream-car s))
          (stream-for-each
           proc (srfi-41:stream-cdr s))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-stream s)
  (begin
    (stream-for-each display-line s)
    ))

;;;#############################################################
;;;#############################################################
(define (display-line x)
  (begin
    (display x)
    (newline)
    ))

;;;#############################################################
;;;#############################################################
(define (stream-enumerate-interval low high)
  (begin
    (if (> low high)
        (begin
          the-empty-stream)
        (begin
          (srfi-41:stream-cons
           low
           (stream-enumerate-interval
            (+ low 1) high))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        the-empty-stream
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (my-stream-filter
          pred (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (my-stream-filter
         pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (show x)
  (begin
    (if (not (stream-null? x))
        (begin
          (display-line x)
          (force-output)
          x)
        (begin
          x
          ))
    ))

;;;#############################################################
;;;#############################################################
(define sum 0)

;;;#############################################################
;;;#############################################################
(define (accum x)
  (begin
    (if (number? x)
        (begin
          (set! sum (+ x sum))
          sum)
        (begin
          x
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the sequence of expressions:~%"))
    (newline)
    (display
     (format #f "(define sum 0)~%"))
    (display
     (format #f "(define (accum x)~%"))
    (display
     (format #f "  (set! sum (+ x sum))~%"))
    (display
     (format #f "  sum)~%"))
    (display
     (format #f "(define seq (stream-map accum "))
    (display
     (format #f "(stream-enumerate-interval 1 20)))~%"))
    (display
     (format #f "(define y (stream-filter even? seq))~%"))
    (display
     (format #f "(define z (stream-filter "))
    (display
     (format #f "(lambda (x) (= (remainder x 5) 0))~%"))
    (display
     (format #f "                         seq))~%"))
    (display
     (format #f "(stream-ref y 7)~%"))
    (display
     (format #f "(display-stream z)~%"))
    (newline)
    (display
     (format #f "What is the value of sum after each of "))
    (display
     (format #f "the above~%"))
    (display
     (format #f "expressions is evaluated? What is the "))
    (display
     (format #f "printed response~%"))
    (display
     (format #f "to evaluating the stream-ref and "))
    (display
     (format #f "display-stream~%"))
    (display
     (format #f "expressions? Would these responses differ "))
    (display
     (format #f "if we had~%"))
    (display
     (format #f "implemented (delay <exp>) simply as "))
    (display
     (format #f "(lambda () <exp>)~%"))
    (display
     (format #f "without using the optimization provided "))
    (display
     (format #f "by memo-proc?~%"))
    (display
     (format #f "Explain.~%"))
    (newline)
    (display
     (format #f "Consider what happens with the memo-proc "))
    (display
     (format #f "version of delay:~%"))
    (newline)
    (display
     (format #f "(define seq (stream-map accum "))
    (display
     (format #f "(stream-enumerate-interval 1 20)))~%"))
    (display
     (format #f "seq -> (cons (accum 1) (memo-proc "))
    (display
     (format #f "(stream-map accum~%"))
    (display
     (format #f "    (stream-enumerate-interval 2 20))))~%"))
    (display
     (format #f "seq -> (cons 1 result), sum = 1, where "))
    (display
     (format #f "result =~%"))
    (display
     (format #f "(cons 3 (memo-proc (stream-map accum~%"))
    (display
     (format #f "    (stream-enumerate-interval 3 20))))~%"))
    (newline)
    (display
     (format #f "and where the result is stored in an "))
    (display
     (format #f "environment variable~%"))
    (display
     (format #f "and sum = 3.~%"))
    (display
     (format #f "(define y (stream-filter even? seq))~%"))
    (display
     (format #f "the first element of the seq is 1, so "))
    (display
     (format #f "the stream-filter~%"))
    (display
     (format #f "examines the second element which is "))
    (display
     (format #f "(accum 2) = 3,~%"))
    (display
     (format #f "but it isn't even, so the third element "))
    (display
     (format #f "which is found~%"))
    (display
     (format #f "from (stream-cdr seq) which is "))
    (display
     (format #f "(accum 3) = 6.~%"))
    (display
     (format #f "Here the element is even, so we construct "))
    (display
     (format #f "the first~%"))
    (display
     (format #f "stream-cons, which is (stream-cons 6 "))
    (display
     (format #f "(stream-filter~%"))
    (display
     (format #f "even? ...).~%"))
    (display
     (format #f "(define z (stream-filter (lambda (x)~%"))
    (display
     (format #f "    (= (remainder x 5) 0)) seq))~%"))
    (newline)
    (display
     (format #f "since 1 is not divisible by 5, the "))
    (display
     (format #f "next term is~%"))
    (display
     (format #f "(accum 2) which is cached as (cons 3 "))
    (display
     (format #f "...), but it~%"))
    (display
     (format #f "isn't divisible by 5.  The 3rd term is "))
    (display
     (format #f "cached as~%"))
    (display
     (format #f "(cons 6 ...), and it isn't divisible "))
    (display
     (format #f "by 5.~%"))
    (display
     (format #f "The 4th term is (accum 4) = 10, and it "))
    (display
     (format #f "is cached as~%"))
    (display
     (format #f "(cons 10 (stream-filter...)), and the "))
    (display
     (format #f "remainder is~%"))
    (display
     (format #f "delayed.~%"))
    (newline)
    (display
     (format #f "Consider what happens with the "))
    (display
     (format #f "non-memo-proc version~%"))
    (display
     (format #f "of delay: The stream sequence will end "))
    (display
     (format #f "up to be~%"))
    (display
     (format #f "identical, since the values of the "))
    (display
     (format #f "sequence will be~%"))
    (display
     (format #f "stored in the sequence as you need "))
    (display
     (format #f "them.~%"))
    (display
     (format #f "(define seq (stream-map accum~%"))
    (display
     (format #f "    (stream-enumerate-interval 1 20)))~%"))
    (display
     (format #f "seq -> (cons 1 (stream-map accum~%"))
    (display
     (format #f "    (stream-enumerate-interval 2 20))),~%"))
    (display
     (format #f "sum = 1~%"))
    (display
     (format #f "(define y (stream-filter even? seq))~%"))
    (display
     (format #f "the first element of the seq is 1, so "))
    (display
     (format #f "the stream-filter~%"))
    (display
     (format #f "examines the second element which is "))
    (display
     (format #f "(accum 2) = 3,~%"))
    (display
     (format #f "but it isn't even, so the third element "))
    (display
     (format #f "which is found~%"))
    (display
     (format #f "from (stream-cdr seq) which is (accum 3) "))
    (display
     (format #f "= 6. Here~%"))
    (display
     (format #f "the element is even, so we construct the "))
    (display
     (format #f "first stream-cons,~%"))
    (display
     (format #f "which is "))
    (display
     (format #f "(stream-cons 6 (stream-filter even? ...),~%"))
    (display
     (format #f "sum = 6.~%"))
    (display
     (format #f "(define z (stream-filter~%"))
    (display
     (format #f "(lambda (x) (= (remainder x 5) 0))~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "seq))~%"))
    (display
     (format #f "Again, the stream-filter ignores the first "))
    (display
     (format #f "few terms, and~%"))
    (display
     (format #f "the fourth term yields a value which is "))
    (display
     (format #f "is divisible by 10.~%"))
    (newline)
    (display
     (format #f "Effectively, both version of delay produce "))
    (display
     (format #f "the same result,~%"))
    (display
     (format #f "except for the initial definition, which "))
    (display
     (format #f "does an extra~%"))
    (display
     (format #f "evaluation of the second term when it "))
    (display
     (format #f "puts the data~%"))
    (display
     (format #f "into a cache.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((seq
           (my-stream-map
            accum (stream-enumerate-interval 1 20))))
      (begin
        (display
         (format #f "seq = ~a, sum = ~a~%" seq sum))
        (force-output)
        (let ((y (my-stream-filter even? seq)))
          (begin
            (display
             (format
              #f "after (my-stream-filter even? seq) "))
            (display
             (format #f ": sum = ~a~%" sum))
            (force-output)
            (let ((z
                   (my-stream-filter
                    (lambda (x) (= (remainder x 5) 0)) seq)))
              (begin
                (display
                 (format
                  #f "after (my-stream-filter~%"))
                (display
                 (format
                  #f "(lambda (x) (= (remainder x 5) 0)) "))
                (display
                 (format #f "seq),~%"))
                (display
                 (format #f "sum = ~a~%" sum))
                (display
                 (format #f "after (my-stream-filter~%"))
                (display
                 (format
                  #f "(lambda (x) (= (remainder x 5)"))
                (display
                 (format #f "0)) seq)~%"))
                (display
                 (format #f "sum = ~a~%" sum))
                (newline)
                (display-stream z)
                (display
                 (format #f "after (display-stream z), "))
                (display
                 (format #f "sum = ~a~%" sum))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.52 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
