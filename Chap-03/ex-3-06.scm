#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.06                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-rand-uniform func-symbol)
  (begin
    (cond
     ((and (symbol? func-symbol)
           (eq? func-symbol 'generate))
      (begin
        (random:uniform)
        ))
     ((and (symbol? func-symbol)
           (eq? func-symbol 'reset))
      (begin
        (lambda (avalue)
          (begin
            (set! *random-state* avalue)
            ))
        ))
     (else
      (begin
        (display
         (format
          #f "my-rand-uniform error: undefined symbol ~a~%"
          func-symbol))
        (force-output)
        (quit)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (random-in-range low high)
  (begin
    (let ((range (- high low)))
      (+ low (* (my-rand-uniform 'generate) range)))
    ))

;;;#############################################################
;;;#############################################################
(define (unit-circle-pred xx yy)
  (begin
    (< (+ (* xx xx) (* yy yy)) 1)
    ))

;;;#############################################################
;;;#############################################################
(define (estimate-integral pred x1 x2 y1 y2 ntrials)
  (define (predicate-test)
    (begin
      (let ((xx (random-in-range x1 x2))
            (yy (random-in-range y1 y2)))
        (begin
          (pred xx yy)
          ))
      ))
  (begin
    (* (- x2 x1) (- y2 y1)
       (monte-carlo ntrials predicate-test))
    ))

;;;#############################################################
;;;#############################################################
(define (monte-carlo trials experiment)
  (define (local-iter trials-remaining trials-passed)
    (begin
      (cond
       ((= trials-remaining 0)
        (begin
          (/ trials-passed trials)
          ))
       ((experiment)
        (begin
          (local-iter
           (- trials-remaining 1) (+ trials-passed 1))
          ))
       (else
        (begin
          (local-iter
           (- trials-remaining 1) trials-passed)
          )))
      ))
  (begin
    (local-iter trials 0)
    ))

;;;#############################################################
;;;#############################################################
(define (pi-via-unit-circle-estimate ntrials)
  (begin
    ((my-rand-uniform 'reset)
     (random-state-from-platform))

    (let ((pi-est
           (estimate-integral
            unit-circle-pred -1 1 -1 1 ntrials)))
      (begin
        (exact->inexact pi-est)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It is useful to be able to reset a "))
    (display
     (format #f "random-number generator~%"))
    (display
     (format #f "to produce a sequence starting from a "))
    (display
     (format #f "given value. Design~%"))
    (display
     (format #f "a new rand procedure that is called "))
    (display
     (format #f "with an argument~%"))
    (display
     (format #f "that is either the symbol generate or the "))
    (display
     (format #f "symbol reset and~%"))
    (display
     (format #f "behaves as follows: (rand 'generate) produces "))
    (display
     (format #f "a new random~%"))
    (display
     (format #f "number; ((rand 'reset) <new-value>) resets "))
    (display
     (format #f "the internal~%"))
    (display
     (format #f "state variable to the designated <new-value>. "))
    (display
     (format #f "Thus, by~%"))
    (display
     (format #f "resetting the state, one can generate "))
    (display
     (format #f "repeatable sequences.~%"))
    (display
     (format #f "These are very handy to have when testing "))
    (display
     (format #f "and debugging~%"))
    (display
     (format #f "programs that use random numbers.~%"))
    (newline)
    (display
     (format #f "(define (my-rand-uniform func-symbol)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (cond~%"))
    (display
     (format #f "     ((and (symbol? func-symbol)~%"))
    (display
     (format #f "           (eq? func-symbol 'generate))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (random:uniform)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     ((and (symbol? func-symbol)~%"))
    (display
     (format #f "           (eq? func-symbol 'reset))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (lambda (avalue)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (set! *random-state* avalue)~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     (else~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (display~%"))
    (display
     (format #f "         (format~%"))
    (display
     (format #f "          #f \"my-rand-uniform error: "))
    (display
     (format #f "undefined symbol ~~a~~%\"~%"))
    (display
     (format #f "          func-symbol))~%"))
    (display
     (format #f "        (force-output)~%"))
    (display
     (format #f "        (quit)~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list 100 1000 10000 100000)))
      (begin
        (for-each
         (lambda (ntrials)
           (begin
             (let ((pi-est
                    (pi-via-unit-circle-estimate ntrials)))
               (begin
                 (display
                  (ice-9-format:format
                   #f "ntrials = ~:d  :  " ntrials))
                 (display
                  (format #f "pi = ~a~%" pi-est))
                 (force-output)
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.06 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
