#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.44                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the problem of transferring an "))
    (display
     (format #f "amount from one~%"))
    (display
     (format #f "account to another. Ben Bitdiddle claims "))
    (display
     (format #f "that this can~%"))
    (display
     (format #f "be accomplished with the following "))
    (display
     (format #f "procedure, even if there~%"))
    (display
     (format #f "are multiple people concurrently transferring "))
    (display
     (format #f "money among~%"))
    (display
     (format #f "multiple accounts, using any account "))
    (display
     (format #f "mechanism that serializes~%"))
    (display
     (format #f "deposit and withdrawal transaction, for "))
    (display
     (format #f "example, the version~%"))
    (display
     (format #f "of make-account in the text above.~%"))
    (newline)
    (display
     (format #f "(define (transfer from-account "))
    (display
     (format #f "to-account amount)~%"))
    (display
     (format #f "  ((from-account 'withdraw) amount)~%"))
    (display
     (format #f "  ((to-account 'deposit) amount))~%"))
    (newline)
    (display
     (format #f "Louis Reasoner claims that there is a "))
    (display
     (format #f "problem here, and~%"))
    (display
     (format #f "that we need to use a more sophisticated "))
    (display
     (format #f "method, such as~%"))
    (display
     (format #f "the one required for dealing with the "))
    (display
     (format #f "exchange problem. Is~%"))
    (display
     (format #f "Louis right? If not, what is the essential "))
    (display
     (format #f "difference between~%"))
    (display
     (format #f "the transfer problem and the exchange "))
    (display
     (format #f "problem? (You should~%"))
    (display
     (format #f "assume that the balance in from-account "))
    (display
     (format #f "is at least~%"))
    (display
     (format #f "amount.)~%"))
    (newline)
    (display
     (format #f "Louis is not right.  If you assume that "))
    (display
     (format #f "the from-account~%"))
    (display
     (format #f "always has a balance greater or equal to "))
    (display
     (format #f "the amount to~%"))
    (display
     (format #f "transfer, then it's not possible to run "))
    (display
     (format #f "into the account~%"))
    (display
     (format #f "transfer timing problem like "))
    (display
     (format #f "exercise 3.43.~%"))
    (newline)
    (display
     (format #f "The essential difference is that in the "))
    (display
     (format #f "unserialized~%"))
    (display
     (format #f "exchange function, both balances are "))
    (display
     (format #f "retrieved first, then~%"))
    (display
     (format #f "serialized withdraws and deposits occur. "))
    (display
     (format #f "This means that~%"))
    (display
     (format #f "two exchanges could compute the differences "))
    (display
     (format #f "to exchange~%"))
    (display
     (format #f "incorrectly.  This cannot happen in the "))
    (display
     (format #f "transfer code.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.44 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
