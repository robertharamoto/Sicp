#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.24                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-table same-key?)
  (begin
    (let ((local-table (list '*table*)))
      (begin
        (define (assoc key records)
          (begin
            (cond
             ((null? records)
              (begin
                #f
                ))
             ((same-key? key (caar records))
              (begin
                (car records)
                ))
             (else
              (begin
                (assoc key (cdr records))
                )))
            ))
        (define (lookup key-1 key-2)
          (begin
            (let ((subtable (assoc key-1 (cdr local-table))))
              (begin
                (if subtable
                    (begin
                      (let ((record (assoc key-2 (cdr subtable))))
                        (begin
                          (if record
                              (begin
                                (cdr record))
                              (begin
                                #f
                                ))
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (define (insert! key-1 key-2 value)
          (begin
            (let ((subtable (assoc key-1 (cdr local-table))))
              (begin
                (if subtable
                    (begin
                      (let ((record (assoc key-2 (cdr subtable))))
                        (begin
                          (if record
                              (begin
                                (set-cdr! record value))
                              (begin
                                (set-cdr! subtable
                                          (cons (cons key-2 value)
                                                (cdr subtable)))
                                ))
                          )))
                    (begin
                      (set-cdr! local-table
                                (cons (list key-1
                                            (cons key-2 value))
                                      (cdr local-table)))
                      ))
                ))
            'ok
            ))
        (define (dispatch m)
          (begin
            (cond
             ((eq? m 'lookup-proc)
              (begin
                lookup
                ))
             ((eq? m 'insert-proc!)
              (begin
                insert!
                ))
             (else
              (begin
                (error "Unknown operation -- TABLE" m)
                )))
            ))
        (begin
          dispatch
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (get z)
  (begin
    (z 'lookup-proc)
    ))

;;;#############################################################
;;;#############################################################
(define (put! z)
  (begin
    (z 'insert-proc!)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the table implementations above, the "))
    (display
     (format #f "keys are tested~%"))
    (display
     (format #f "for equality using equal? (called by "))
    (display
     (format #f "assoc). This is~%"))
    (display
     (format #f "not always the appropriate test. For "))
    (display
     (format #f "instance, we might~%"))
    (display
     (format #f "have a table with numeric keys in which "))
    (display
     (format #f "we don't need~%"))
    (display
     (format #f "an exact match to the number we're "))
    (display
     (format #f "looking up, but~%"))
    (display
     (format #f "only a number within some tolerance of "))
    (display
     (format #f "it. Design a~%"))
    (display
     (format #f "table constructor make-table that takes "))
    (display
     (format #f "as an argument~%"))
    (display
     (format #f "a same-key? procedure that will be used "))
    (display
     (format #f "to test \"equality\"~%"))
    (display
     (format #f "of keys. Make-table should return a "))
    (display
     (format #f "dispatch procedure~%"))
    (display
     (format #f "that can be used to access appropriate "))
    (display
     (format #f "lookup and insert!~%"))
    (display
     (format #f "procedures for a local table.~%"))
    (newline)
    (display
     (format #f "(define (make-table same-key?)~%"))
    (display
     (format #f "  (let ((local-table "))
    (display
     (format #f "(list '*table*)))~%"))
    (display
     (format #f "    (define (assoc key records)~%"))
    (display
     (format #f "      (cond ((null? records) #f)~%"))
    (display
     (format #f "            ((same-key? key "))
    (display
     (format #f "(caar records)) (car records))~%"))
    (display
     (format #f "            (else (assoc key "))
    (display
     (format #f "(cdr records)))))~%"))
    (display
     (format #f "    (define (lookup key-1 key-2)~%"))
    (display
     (format #f "      (let ((subtable (assoc "))
    (display
     (format #f "key-1 (cdr local-table))))~%"))
    (display
     (format #f "        (if subtable~%"))
    (display
     (format #f "            (let ((record (assoc "))
    (display
     (format #f "key-2 (cdr subtable))))~%"))
    (display
     (format #f "              (if record~%"))
    (display
     (format #f "                  (cdr record)~%"))
    (display
     (format #f "                  #f))~%"))
    (display
     (format #f "            #f)))~%"))
    (display
     (format #f "    (define (insert! "))
    (display
     (format #f "key-1 key-2 value)~%"))
    (display
     (format #f "      (let ((subtable (assoc "))
    (display
     (format #f "key-1 (cdr local-table))))~%"))
    (display
     (format #f "        (if subtable~%"))
    (display
     (format #f "            (let ((record (assoc "))
    (display
     (format #f "key-2 (cdr subtable))))~%"))
    (display
     (format #f "              (if record~%"))
    (display
     (format #f "                  (set-cdr! record value)~%"))
    (display
     (format #f "                  (set-cdr! subtable~%"))
    (display
     (format #f "                            (cons "))
    (display
     (format #f "(cons key-2 value)~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(cdr subtable)))))~%"))
    (display
     (format #f "            (set-cdr! local-table~%"))
    (display
     (format #f "                      (cons "))
    (display
     (format #f "(list key-1~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(cons key-2 value))~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "(cdr local-table)))))~%"))
    (display
     (format #f "      'ok)~%"))
    (display
     (format #f "    (define (dispatch m)~%"))
    (display
     (format #f "      (cond ((eq? m 'lookup-proc) "))
    (display
     (format #f "lookup)~%"))
    (display
     (format #f "            ((eq? m 'insert-proc!) "))
    (display
     (format #f "insert!)~%"))
    (display
     (format #f "            (else "))
    (display
     (format #f "(error~%"))
    (display
     (format #f "    \"Unknown operation -- "))
    (display
     (format #f "TABLE\" m))))~%"))
    (display
     (format #f "    dispatch))~%"))
    (newline)
    (display
     (format #f "(define (get z) (z 'lookup-proc))~%"))
    (display
     (format #f "(define (put z) (z 'insert-proc!))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((table-1 (make-table equal?)))
      (let ((test-list
             (list
              (list 'alphabet 'a 23)
              (list 'alphabet 'b 24)
              (list 'alphabet 'c 25)
              (list 'alphabet 'd 26)
              (list 'numbers '33 33)
              (list 'numbers '34 34)
              (list 'numbers '35 35)
              (list 'numbers '36 36)
              (list 'numbers '37 37))))
        (begin
          (display
           (format #f "setup table~%"))
          (display
           (format #f "(define table-1 (make-table equal?))~%"))
          (for-each
           (lambda (alist)
             (begin
               (let ((key-1 (list-ref alist 0))
                     (key-2 (list-ref alist 1))
                     (value (list-ref alist 2)))
                 (begin
                   ((put! table-1) key-1 key-2 value)
                   (display

                    (format #f "(put! table-1 '~a '~a ~a)~%" key-1 key-2 value))
                   (force-output)
                   ))
               )) test-list)

          (newline)
          (display
           (format #f "get data~%"))
          (for-each
           (lambda (alist)
             (begin
               (let ((key-1 (list-ref alist 0))
                     (key-2 (list-ref alist 1))
                     (value (list-ref alist 2)))
                 (begin
                   (let ((tvalue
                          ((get table-1) key-1 key-2)))
                     (begin
                       (if (equal? tvalue value)
                           (begin
                             (display
                              (format #f "(get table-1 '~a '~a)"
                                      key-1 key-2))
                             (display
                              (format #f " -> ~a~%"
                                      tvalue)))
                           (begin
                             (display
                              (format #f "(get table-1 '~a '~a), "
                                      key-1 key-2))
                             (display
                              (format #f "shouldbe=~a <> result=~a~%"
                                      value tvalue))
                             ))
                       (force-output)
                       ))
                   ))
               )) test-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.24 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
