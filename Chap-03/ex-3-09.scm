#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.09                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In section 1.2.1 we used the "))
    (display
     (format #f "substitution model to~%"))
    (display
     (format #f "analyze two procedures for computing "))
    (display
     (format #f "factorials, a~%"))
    (display
     (format #f "recursive version~%"))
    (display
     (format #f "(define (factorial n)~%"))
    (display
     (format #f "  (if (= n 1)~%"))
    (display
     (format #f "      1~%"))
    (display
     (format #f "      (* n (factorial (- n 1)))))~%"))
    (display
     (format #f "and an iterative version~%"))
    (display
     (format #f "(define (factorial n)~%"))
    (display
     (format #f "  (fact-iter 1 1 n))~%"))
    (display
     (format #f "(define (fact-iter product "))
    (display
     (format #f "counter max-count)~%"))
    (display
     (format #f "  (if (> counter max-count)~%"))
    (display
     (format #f "      product~%"))
    (display
     (format #f "      (fact-iter (* counter product)~%"))
    (display
     (format #f "                 (+ counter 1)~%"))
    (display
     (format #f "                 max-count)))~%"))
    (display
     (format #f "Show the environment structures created "))
    (display
     (format #f "by evaluating (factorial 6)~%"))
    (display
     (format #f "using each version~%"))
    (display
     (format #f "of the factorial procedure.~%"))
    (newline)
    (display
     (format #f "recursive version~%"))
    (display
     (format #f "(factorial 6) -> E1: n=6~%"))
    (display
     (format #f "(* 6 (factorial 5)) -> E2: n=5~%"))
    (display
     (format #f "(* 5 (factorial 4)) -> E3: n=4~%"))
    (display
     (format #f "(* 4 (factorial 3)) -> E4: n=3~%"))
    (display
     (format #f "(* 3 (factorial 2)) -> E5: n=2~%"))
    (display
     (format #f "(* 2 (factorial 1)) -> E6: n=1~%"))
    (display
     (format #f "1~%"))
    (newline)
    (display
     (format #f "iterative version~%"))
    (display
     (format #f "(factorial 6) -> E1: n=6~%"))
    (display
     (format #f "(fact-iter 1 1 6) -> E2: "))
    (display
     (format #f "product=1 counter=1 max-count=6~%"))
    (display
     (format #f "(fact-iter 1 2 6) -> E3: "))
    (display
     (format #f "product=1 counter=2 max-count=6~%"))
    (display
     (format #f "(fact-iter 2 3 6) -> E4: "))
    (display
     (format #f "product=2 counter=3 max-count=6~%"))
    (display
     (format #f "(fact-iter 6 4 6) -> E5: "))
    (display
     (format #f "product=6 counter=4 max-count=6~%"))
    (display
     (format #f "(fact-iter 24 5 6) -> E6: "))
    (display
     (format #f "product=24 counter=5 max-count=6~%"))
    (display
     (format #f "(fact-iter 120 6 6) -> E7: "))
    (display
     (format #f "product=120 counter=6 max-count=6~%"))
    (display
     (format #f "(fact-iter 720 7 6) -> E7: "))
    (display
     (format #f "product=720 counter=7 max-count=6~%"))
    (display
     (format #f "720~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.09 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
