#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.78                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones
  (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (integral delayed-integrand initial-value dt)
  (define local-int
    (begin
      (srfi-41:stream-cons
       initial-value
       (let ((integrand (force delayed-integrand)))
         (begin
           (add-streams
            (scale-stream integrand dt)
            local-int)
           )))
      ))
  (begin
    local-int
    ))

;;;#############################################################
;;;#############################################################
(define (solve f y0 dt)
  (define y (integral (delay dy) y0 dt))
  (define dy (srfi-41:stream-map f y))
  (begin
    y
    ))

;;;#############################################################
;;;#############################################################
(define (solve-2nd a b dt y0 dy0)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay d2y) dy0 dt))
  (define d2y
    (add-streams
     (scale-stream dy a)
     (scale-stream y b)))
  (begin
    y
    ))

;;;#############################################################
;;;#############################################################
;;; note: dt should be about equal to 1/n
(define (scheme-test-1)
  (begin
    (let ((solve-ref-list (list (list 1000 0.0010)))
          (a 0.0)
          (b -4.0)
          (y0 0.0)
          (dy0 1.0)
          (debug-flag #f))
      (begin
        (display
         (ice-9-format:format
          #f "d^2y/dt^2-~a*dy/dt-(~a)*y = 0~%"
          a b))
        (display
         (format
          #f "with boundary conditions y(0)=0, dy(0)=1.0.~%"))
        (display
         (format
          #f "solution is y=1/(sqrt b)*sin((sqrt b)*t)~%"))
        (force-output)
        (for-each
         (lambda (alist)
           (begin
             (let ((anum (list-ref alist 0))
                   (dt (list-ref alist 1)))
               (let ((exp-est
                      (my-stream-ref
                       (solve
                        (lambda (y)
                          (begin
                            y
                            )) 1 dt) anum))
                     (expr-2nd-est
                      (my-stream-ref
                       (solve-2nd a b dt y0 dy0) anum)))
                 (begin
                   (newline)
                   (if (equal? debug-flag #t)
                       (begin
                         (display
                          (ice-9-format:format
                           #f "exp(1) = ~a  : (n = ~:d)~%"
                           exp-est anum))
                         ))
                   (display
                    (ice-9-format:format
                     #f "d^2y/dt^2-~a*dy/dt-(~a)*y = 0~%"
                     a  b))
                   (display
                    (ice-9-format:format
                     #f "y = ~9,6f : (n = ~:d)~%"
                     expr-2nd-est anum))
                   (let ((time (* anum dt))
                         (b-sqrt (sqrt (* -1 b))))
                     (begin
                       (display
                        (ice-9-format:format
                         #f "check against y=1/(sqrt b)*"))
                       (display
                        (ice-9-format:format
                         #f "sin((sqrt b)*t)~%"))
                       (display
                        (ice-9-format:format
                         #f "= ~9,6f  : (t = ~a)~%"
                         (* (/ 1.0 b-sqrt) (sin (* b-sqrt time))) time))
                       (force-output)
                       ))
                   )))
             )) solve-ref-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-2)
  (begin
    (let ((solve-ref-list (list (list 1000 0.0010)))
          (a 0.0)
          (b 4.0)
          (y0 1.0)
          (dy0 0.0)
          (debug-flag #f))
      (begin
        (display
         (ice-9-format:format
          #f "d^2y/dt^2-~a*dy/dt-~a*y = 0~%" a b))
        (display
         (format
          #f "with boundary conditions y(0)=~a, dy(0)=~a.~%"
          y0 dy0))
        (display
         (format
          #f "solution is y=1/2*exp((sqrt b)*t)"))
        (display
         (format
          #f "+1/2*exp(-(sqrt b)*t)~%"))
        (force-output)
        (for-each
         (lambda (alist)
           (begin
             (let ((anum (list-ref alist 0))
                   (dt (list-ref alist 1)))
               (let ((exp-est
                      (my-stream-ref
                       (solve (lambda (y) y) 1 dt) anum))
                     (expr-2nd-est
                      (my-stream-ref
                       (solve-2nd a b dt y0 dy0) anum)))
                 (begin
                   (newline)
                   (if (equal? debug-flag #t)
                       (begin
                         (display
                          (ice-9-format:format
                           #f "exp(1) = ~a  : (n = ~:d)~%"
                           exp-est anum))
                         ))
                   (display
                    (ice-9-format:format
                     #f "d^2y/dt^2-~a*dy/dt-~a*y = 0~%"
                     a b))
                   (display
                    (ice-9-format:format
                     #f "y = ~9,6f : (n = ~:d)~%"
                     expr-2nd-est anum))
                   (let ((time (* anum dt))
                         (b-sqrt (sqrt b)))
                     (begin
                       (display
                        (ice-9-format:format
                         #f "check against y=1/2*exp((sqrt b)*t)"))
                       (display
                        (ice-9-format:format
                         #f "+1/2*exp(-(sqrt b)*t)~%"))
                       (display
                        (ice-9-format:format
                         #f "= ~9,6f  : (t = ~a)~%"
                         (+ (* (/ 1.0 b-sqrt)
                               (exp (* b-sqrt time)))
                            (* (/ 1.0 b-sqrt)
                               (exp (* -1.0 b-sqrt time))))
                         time))
                       (force-output)
                       ))
                   )))
             )) solve-ref-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the problem of designing a "))
    (display
     (format #f "signal-processing~%"))
    (display
     (format #f "system to study the homogeneous "))
    (display
     (format #f "second-order linear~%"))
    (display
     (format #f "differential equation.~%"))
    (newline)
    (display
     (format #f "d^2y/dt^2-a*dy/dt-b*y=0~%"))
    (newline)
    (display
     (format #f "The output stream, modeling y, is "))
    (display
     (format #f "generated by a~%"))
    (display
     (format #f "network that contains a loop. This is "))
    (display
     (format #f "because the~%"))
    (display
     (format #f "value of d^2y/dt^2 depends upon the "))
    (display
     (format #f "values of y and~%"))
    (display
     (format #f "dy/dt and both of these are determined "))
    (display
     (format #f "by integrating~%"))
    (display
     (format #f "d^2y/dt^2. The diagram we would like to "))
    (display
     (format #f "encode is shown~%"))
    (display
     (format #f "in figure 3.35. Write a procedure "))
    (display
     (format #f "solve-2nd that takes~%"))
    (display
     (format #f "as arguments the constants a, b, and "))
    (display
     (format #f "dt and the initial~%"))
    (display
     (format #f "values y0 and dy0 for y and dy/dt "))
    (display
     (format #f "and generates the~%"))
    (display
     (format #f "stream of successive values "))
    (display
     (format #f "of y.~%"))
    (newline)
    (display
     (format #f "(define (solve-2nd a b dt y0 dy0)~%"))
    (display
     (format #f "  (define y (integral "))
    (display
     (format #f "(delay dy) y0 dt))~%"))
    (display
     (format #f "  (define dy (integral "))
    (display
     (format #f "(delay d2y) dy0 dt))~%"))
    (display
     (format #f "  (define d2y~%"))
    (display
     (format #f "    (add-streams~%"))
    (display
     (format #f "     (scale-stream dy a)~%"))
    (display
     (format #f "     (scale-stream y b)))~%"))
    (display
     (format #f "  y)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (scheme-test-1)
    (newline)
    (scheme-test-2)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.78 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (display
              (format
               #f "solve 2nd order differential equation~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
