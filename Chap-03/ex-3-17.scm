#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.17                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-pairs x)
  (define (local-count-iter next-list seen-htable)
    (begin
      (if (not (pair? next-list))
          (begin
            0)
          (begin
            (let ((next-car (car next-list))
                  (next-cdr (cdr next-list)))
              (begin
                (if (and
                     (not (equal? next-car next-cdr))
                     (pair? next-cdr)
                     (not (equal? (cdr next-cdr) x)))
                    (begin
                      (let ((car-seen-flag
                             (hash-ref seen-htable next-car #f))
                            (next-car-count 0)
                            (next-cdr-count 0))
                        (begin
                          (if (equal? car-seen-flag #f)
                              (begin
                                (hash-set! seen-htable next-car 1)
                                (set!
                                 next-car-count
                                 (local-count-iter next-car seen-htable)))
                              (begin
                                (set! next-car-count 0)
                                ))
                          (let ((cdr-seen-flag
                                 (hash-ref seen-htable next-cdr #f)))
                            (begin
                              (if (equal? cdr-seen-flag #f)
                                  (begin
                                    (hash-set!
                                     seen-htable next-cdr 1)
                                    (set!
                                     next-cdr-count
                                     (local-count-iter next-cdr seen-htable)))
                                  (begin
                                    (set! next-cdr-count 0)
                                    ))
                              ))

                          (+ next-car-count next-cdr-count 1)
                          )))
                    (begin
                      ;;; (equal? next-car next-cdr)
                      (let ((car-seen-flag
                             (hash-ref seen-htable next-car #f))
                            (next-car-count 0))
                        (begin
                          (if (equal? car-seen-flag #f)
                              (begin
                                (hash-set! seen-htable next-car 1)
                                (set!
                                 next-car-count
                                 (local-count-iter next-car seen-htable)))
                              (begin
                                (set! next-car-count 0)
                                ))

                          (+ next-car-count 1)
                          ))
                      ))
                ))
            ))
      ))
  (begin
    (let ((seen-htable (make-hash-table)))
      (begin
        (local-count-iter x seen-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Devise a correct version of the "))
    (display
     (format #f "count-pairs procedure~%"))
    (display
     (format #f "of exercise 3.16 that returns the "))
    (display
     (format #f "number of distinct~%"))
    (display
     (format #f "pairs in any structure. (Hint: "))
    (display
     (format #f "Traverse the~%"))
    (display
     (format #f "structure, maintaining an auxiliary "))
    (display
     (format #f "data structure~%"))
    (display
     (format #f "that is used to keep track of which "))
    (display
     (format #f "pairs have already~%"))
    (display
     (format #f "been counted.)~%"))
    (newline)
    (display
     (format #f "A hash table was used to keep track "))
    (display
     (format #f "of already seen~%"))
    (display
     (format #f "entries. A special case was needed to "))
    (display
     (format #f "avoid infinite~%"))
    (display
     (format #f "recursion, if the (cdr next-cdr) = "))
    (display
     (format #f "original list,~%"))
    (display
     (format #f "then we have a recursive definition, "))
    (display
     (format #f "so we don't~%"))
    (display
     (format #f "need to count that path.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x (cons 'a (cons 'b (cons 'c (list))))))
      (begin
        (display
         (format
          #f "(define x (cons 'a (cons 'b (cons "))
        (display
         (format
          #f "'c (list)))))~%"))
        (display
         (format #f "(count-pairs x)~%"))
        (display
         (format #f "~a~%" (count-pairs x)))
        ))

    (newline)
    (let ((x (cons 'a 'b)))
      (let ((y (cons x (cons x (list)))))
        (begin
          (display
           (format #f "(define x (cons 'a 'b))~%"))
          (display
           (format #f "(define y (cons x "))
          (display
           (format #f "(cons x (list))))~%"))
          (display
           (format #f "(count-pairs y)~%"))
          (display
           (format #f "~a~%" (count-pairs y)))
          )))

    (newline)
    (let ((x (cons 'a 'b)))
      (let ((y (cons x x)))
        (let ((z (cons y y)))
          (begin
            (display
             (format #f "(define x (cons 'a 'b))~%"))
            (display
             (format #f "(define y (cons x x))~%"))
            (display
             (format #f "(define z (cons y y))~%"))
            (display
             (format #f "(count-pairs z)~%"))
            (display
             (format #f "~a~%" (count-pairs z)))
            ))
        ))

    (newline)
    (let ((x (cons 'a 'b)))
      (let ((y (cons x x)))
        (let ((z (cons y y)))
          (begin
            (set-cdr! z z)
            (display
             (format #f "(define x (cons 'a 'b))~%"))
            (display
             (format #f "(define y (cons x x))~%"))
            (display
             (format #f "(define z (cons y y))~%"))
            (display
             (format #f "(set-cdr! z z)~%"))
            (display
             (format #f "(count-pairs z)~%"))
            (display
             (format #f "~a~%" (count-pairs z)))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.17 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
