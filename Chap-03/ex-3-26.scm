#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.26                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-table less-key?)
  (begin
    (let ((local-table (list '*table*)))
      (begin
        (define (entry tree)
          (begin
            (car tree)
            ))
        (define (value tree)
          (begin
            (cadr tree)
            ))
        (define (left-branch tree)
          (begin
            (caddr tree)
            ))
        (define (right-branch tree)
          (begin
            (cadddr tree)
            ))
        (define (make-tree entry value left right)
          (begin
            (list entry value left right)
            ))
        (define (assoc key this-tree)
          (begin
            (cond
             ((null? this-tree)
              (begin
                #f
                ))
             (else
              (begin
                (let ((this-entry (entry this-tree))
                      (rlen (length this-tree)))
                  (begin
                    (cond
                     ((less-key? key this-entry)
                      (begin
                        (if (> rlen 2)
                            (begin
                              (assoc key (left-branch this-tree)))
                            (begin
                              #f
                              ))
                        ))
                     ((less-key? this-entry key)
                      (begin
                        (if (> rlen 3)
                            (begin
                              (assoc key (right-branch this-tree)))
                            (begin
                              #f
                              ))
                        ))
                     (else
                      (begin
                        this-tree
                        )))
                    ))
                )))
            ))
        (define (lookup key)
          (begin
            (let ((this-tree (cdr local-table)))
              (let ((subtable (assoc key this-tree)))
                (begin
                  (if subtable
                      (begin
                        (value subtable))
                      (begin
                        #f
                        ))
                  )))
            ))
        (define (insert! key value)
          (define (local-final-insert! a-key value a-tree)
            (begin
              (if (null? a-tree)
                  (begin
                    (set!
                     local-table
                     (cons (car local-table)
                           (make-tree key value (list) (list)))))
                  (begin
                    (let ((this-entry (entry a-tree))
                          (rlen (length a-tree)))
                      (begin
                        (cond
                         ((less-key? key this-entry)
                          (begin
                            (if (< rlen 1)
                                (begin
                                  (set!
                                   a-tree
                                   (cons a-tree
                                         (make-tree key value (list) (list)))
                                   ))
                                (begin
                                  (if (null? (left-branch a-tree))
                                      (begin
                                        (list-set!
                                         a-tree
                                         2 (make-tree key value (list) (list))))
                                      (begin
                                        (local-final-insert!
                                         a-key value (left-branch a-tree))
                                        ))
                                  ))
                            ))
                         ((less-key? this-entry key)
                          (begin
                            (if (< rlen 1)
                                (begin
                                  (set!
                                   a-tree
                                   (cons a-tree
                                         (make-tree
                                          key value (list) (list)))
                                   ))
                                (begin
                                  (if (null? (right-branch a-tree))
                                      (begin
                                        (list-set!
                                         a-tree
                                         3 (make-tree
                                            key value (list) (list))))
                                      (begin
                                        (local-final-insert!
                                         a-key value
                                         (right-branch a-tree))
                                        ))
                                  ))
                            ))
                         (else
                          (begin
                            (list-set!
                             a-tree 1 value)
                            )))
                        ))
                    ))
              ))
          (define (local-insert! a-key value a-tree)
            (begin
              (let ((subtable (assoc a-key a-tree)))
                (begin
                  (if subtable
                      (begin
                        (set-cdr! subtable value))
                      (begin
                        (local-final-insert! a-key value a-tree)
                        ))
                  ))
              ))
          (begin
            (local-insert! key value (cdr local-table))
            'ok
            ))
        (define (dispatch m)
          (begin
            (cond
             ((eq? m 'lookup-proc)
              (begin
                lookup
                ))
             ((eq? m 'insert-proc!)
              (begin
                insert!
                ))
             (else
              (begin
                (error "Unknown operation -- TABLE" m)
                )))
            ))
        (begin
          dispatch
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (get z)
  (begin
    (z 'lookup-proc)
    ))

;;;#############################################################
;;;#############################################################
(define (put! z)
  (begin
    (z 'insert-proc!)
    ))

;;;#############################################################
;;;#############################################################
(define (main-scheme-test test-list)
  (begin
    (let ((table-1 (make-table string-ci<?)))
      (begin
        (display
         (format #f "setup table~%"))
        (display
         (format #f "(define table-1 (make-table equal?))~%"))
        (for-each
         (lambda (alist)
           (begin
             (let ((key (list-ref alist 0))
                   (value (list-ref alist 1)))
               (begin
                 ((put! table-1) key value)
                 (display
                  (format #f "(put! table-1 ~a ~a)~%" key value))
                 (force-output)
                 ))
             )) test-list)

        (newline)
        (display (format #f "retrieve data~%"))
        (for-each
         (lambda (alist)
           (begin
             (let ((key (list-ref alist 0))
                   (value (list-ref alist 1)))
               (begin
                 (let ((tvalue
                        ((get table-1) key)))
                   (begin
                     (if (equal? tvalue value)
                         (begin
                           (display
                            (format #f "(get table-1 ~a) -> ~a~%"
                                    key tvalue)))
                         (begin
                           (let ((atmp
                                  (format
                                   #f "result=~a, shouldbe=~a"
                                   tvalue value)))
                             (begin
                               (display
                                (format
                                 #f
                                 "(get table-1 ~a), ~a~%"
                                 key atmp))
                               ))
                           ))
                     (force-output)
                     ))
                 ))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "To search a table as implemented above, "))
    (display
     (format #f "one needs to~%"))
    (display
     (format #f "scan through the list of records. This "))
    (display
     (format #f "is basically the~%"))
    (display
     (format #f "unordered list representation of section "))
    (display
     (format #f "2.3.3. For large~%"))
    (display
     (format #f "tables, it may be more efficient to "))
    (display
     (format #f "structure the table~%"))
    (display
     (format #f "in a different manner. Describe a where "))
    (display
     (format #f "implementation where~%"))
    (display
     (format #f "the (key, value) records are organized "))
    (display
     (format #f "using a binary~%"))
    (display
     (format #f "tree, assuming that keys can be ordered "))
    (display
     (format #f "in some way (e.g.,~%"))
    (display
     (format #f "numerically or alphabetically). (Compare "))
    (display
     (format #f "exercise 2.66~%"))
    (display
     (format #f "of chapter 2.)~%"))
    (newline)
    (display
     (format #f "(define (make-table less-key?)~%"))
    (display
     (format #f "  (let ((local-table (list '*table*)))~%"))
    (display
     (format #f "    (define (entry tree) (car tree))~%"))
    (display
     (format #f "    (define (value tree) (cadr tree))~%"))
    (display
     (format #f "    (define (left-branch tree) "))
    (display
     (format #f "(caddr tree))~%"))
    (display
     (format #f "    (define (right-branch tree) "))
    (display
     (format #f "(cadddr tree))~%"))
    (display
     (format #f "    (define (make-tree entry "))
    (display
     (format #f "value left right)~%"))
    (display
     (format #f "      (list entry value left right))~%"))
    (display
     (format #f "    (define (assoc key this-tree)~%"))
    (display
     (format #f "      (cond~%"))
    (display
     (format #f "       ((null? this-tree) #f)~%"))
    (display
     (format #f "       (else~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((this-entry "))
    (display
     (format #f "(entry this-tree))~%"))
    (display
     (format #f "                (rlen "))
    (display
     (format #f "(length this-tree)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (cond~%"))
    (display
     (format #f "               ((less-key? "))
    (display
     (format #f "key this-entry)~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (if (> rlen 2)~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (assoc key "))
    (display
     (format #f "(left-branch this-tree)))~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        #f~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                  ))~%"))
    (display
     (format #f "               ((less-key? "))
    (display
     (format #f "this-entry key)~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (if (> rlen 3)~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        (assoc key "))
    (display
     (format #f "(right-branch this-tree)))~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        #f~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                  ))~%"))
    (display
     (format #f "               (else~%"))
    (display
     (format #f "                this-tree~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "       ))~%"))
    (display
     (format #f "    (define (lookup key)~%"))
    (display
     (format #f "      (let ((this-tree "))
    (display
     (format #f "(cdr local-table)))~%"))
    (display
     (format #f "        (let ((subtable "))
    (display
     (format #f "(assoc key this-tree)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (if subtable~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (value subtable))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  #f~%"))
    (display
     (format #f "                  ))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    (define (insert! key value)~%"))
    (display
     (format #f "      (define (local-final-insert! "))
    (display
     (format #f "a-key value a-tree)~%"))
    (display
     (format #f "        (if (null? a-tree)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (set! local-table~%"))
    (display
     (format #f "                    (cons (car "))
    (display
     (format #f "local-table) (make-tree key value "))
    (display
     (format #f "(list) (list)))))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((this-entry "))
    (display
     (format #f "(entry a-tree))~%"))
    (display
     (format #f "                    (rlen "))
    (display
     (format #f "(length a-tree)))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (cond~%"))
    (display
     (format #f "                   ((less-key? "))
    (display
     (format #f "key this-entry)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (if (< rlen 1)~%"))
    (display
     (format #f "                          (begin~%"))
    (display
     (format #f "                            (set! a-tree~%"))
    (display
     (format #f "                                  (cons a-tree~%"))
    (display
     (format #f "                                        "))
    (display
     (format #f "(make-tree key value (list) (list)))~%"))
    (display
     (format #f "                                  ))~%"))
    (display
     (format #f "                          (begin~%"))
    (display
     (format #f "                            (if (null? "))
    (display
     (format #f "(left-branch a-tree))~%"))
    (display
     (format #f "                                (begin~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(list-set!~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "a-tree~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "2 (make-tree key value (list) (list))))~%"))
    (display
     (format #f "                                (begin~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(local-final-insert!~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "a-key value (left-branch a-tree))~%"))
    (display
     (format #f "                                  ))~%"))
    (display
     (format #f "                            ))~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "((less-key? this-entry key)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (if (< rlen 1)~%"))
    (display
     (format #f "                          (begin~%"))
    (display
     (format #f "                            (set! a-tree~%"))
    (display
     (format #f "                                  (cons a-tree~%"))
    (display
     (format #f "                                        "))
    (display
     (format #f "(make-tree key value (list) (list)))~%"))
    (display
     (format #f "                                  ))~%"))
    (display
     (format #f "                          (begin~%"))
    (display
     (format #f "                            (if (null? "))
    (display
     (format #f "(right-branch a-tree))~%"))
    (display
     (format #f "                                (begin~%"))
    (display
     (format #f "                                  (list-set!~%"))
    (display
     (format #f "                                   a-tree~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "3 (make-tree key value (list) (list))))~%"))
    (display
     (format #f "                                (begin~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(local-final-insert!~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "a-key value (right-branch a-tree))~%"))
    (display
     (format #f "                                  ))~%"))
    (display
     (format #f "                            ))~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                   (else~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (list-set!~%"))
    (display
     (format #f "                       a-tree 1 value)~%"))
    (display
     (format #f "                      )))~%"))
    (display
     (format #f "                  )))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "      (define (local-insert! "))
    (display
     (format #f "a-key value a-tree)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((subtable "))
    (display
     (format #f "(assoc a-key a-tree)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (if subtable~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (set-cdr! "))
    (display
     (format #f "subtable value))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (local-final-insert! "))
    (display
     (format #f "a-key value a-tree)~%"))
    (display
     (format #f "                    ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (local-insert! "))
    (display
     (format #f "key value (cdr local-table))~%"))
    (display
     (format #f "        'ok~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    (define (dispatch m)~%"))
    (display
     (format #f "      (cond ((eq? m "))
    (display
     (format #f "'lookup-proc) lookup)~%"))
    (display
     (format #f "            ((eq? m "))
    (display
     (format #f "'insert-proc!) insert!)~%"))
    (display
     (format #f "            (else~%"))
    (display
     (format #f "               "))
    (display
     (format #f "(error \"Unknown operation "))
    (display
     (format #f "-- TABLE\" m))))~%"))
    (display
     (format #f "    dispatch))~%"))
    (display
     (format #f "(define (get z) (z 'lookup-proc))~%"))
    (display
     (format #f "(define (put! z) (z 'insert-proc!))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list "alpha-3" 25)
            (list "alpha-4" 26)
            (list "alpha-2" 24)
            (list "alpha-1" 23)
            (list "num-3" 35)
            (list "num-5" 37)
            (list "num-4" 36)
            (list "num-1" 33)
            (list "num-2" 34)
            )))
      (begin
        (main-scheme-test test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.26 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
