#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.73                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons
   1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons
   1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm)
      (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter
         pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s)
           (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (pairs
       (srfi-41:stream-cdr s)
       (srfi-41:stream-cdr t))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (weighted-pairs s t wfunc)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (merge-weighted
      (srfi-41:stream-map
       (lambda (x) (list (srfi-41:stream-car s) x))
       (srfi-41:stream-cdr t))
      (weighted-pairs
       (srfi-41:stream-cdr s)
       (srfi-41:stream-cdr t) wfunc)
      wfunc))
    ))

;;;#############################################################
;;;#############################################################
(define (integral integrand initial-value dt)
  (define local-int
    (begin
      (srfi-41:stream-cons
       initial-value
       (add-streams
        (scale-stream integrand dt) local-int))
      ))
  (begin
    local-int
    ))

;;;#############################################################
;;;#############################################################
(define (rc rr cc dt)
  (define (local-int current-stream v0)
    (begin
      (let ((pstream (partial-sums current-stream))
            (v0-stream (scale-stream ones v0))
            (cc-factor (/ dt cc)))
        (begin
          (define a-stream
            (srfi-41:stream-cons
             (+ v0
                (* rr
                   (srfi-41:stream-car current-stream)))
             (add-streams
              v0-stream
              (add-streams
               (scale-stream current-stream rr)
               (scale-stream pstream cc-factor)
               ))
             ))
          a-stream
          ))
      ))
  (begin
    local-int
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "We can model electrical circuits using "))
    (display
     (format #f "streams to represent~%"))
    (display
     (format #f "the values of currents or voltages at a "))
    (display
     (format #f "sequence of times.~%"))
    (display
     (format #f "For instance, suppose we have an RC "))
    (display
     (format #f "circuit consisting of~%"))
    (display
     (format #f "a resistor of resistance R and a capacitor "))
    (display
     (format #f "of capacitance~%"))
    (display
     (format #f "C in series. The voltage response v of the "))
    (display
     (format #f "circuit to an~%"))
    (display
     (format #f "injected current i is determined by the "))
    (display
     (format #f "formula in~%"))
    (display
     (format #f "figure 3.33, whose structure is shown by "))
    (display
     (format #f "the accompanying~%"))
    (display
     (format #f "signal-flow diagram.~%"))
    (newline)
    (display
     (format #f "Write a procedure RC that models this "))
    (display
     (format #f "circuit. RC should~%"))
    (display
     (format #f "take as inputs the values of R, C, and "))
    (display
     (format #f "dt and should~%"))
    (display
     (format #f "return a procedure that takes as inputs "))
    (display
     (format #f "a stream~%"))
    (display
     (format #f "representing the current i and an initial "))
    (display
     (format #f "value for the~%"))
    (display
     (format #f "capacitor voltage v0 and produces a output "))
    (display
     (format #f "the stream of~%"))
    (display
     (format #f "voltages v. For example, you should be "))
    (display
     (format #f "able to use~%"))
    (display
     (format #f "RC to model an RC circuit with R = 5 ohms, "))
    (display
     (format #f "C = 1 farad,~%"))
    (display
     (format #f "and a 0.5-second time step by "))
    (display
     (format #f "evaluating:~%"))
    (newline)
    (display
     (format #f "(define RC (RC 5 1 0.5)).~%"))
    (newline)
    (display
     (format #f "This defines RC1 as a procedure that "))
    (display
     (format #f "takes a stream~%"))
    (display
     (format #f "representing the time sequence of "))
    (display
     (format #f "currents and an~%"))
    (display
     (format #f "initial capacitor voltage and produces "))
    (display
     (format #f "the output~%"))
    (display
     (format #f "stream of voltages.~%"))
    (newline)
    (display
     (format #f "(define (rc rr cc dt)~%"))
    (display
     (format #f "  (define (local-int "))
    (display
     (format #f "current-stream v0)~%"))
    (display
     (format #f "    (let ((pstream "))
    (display
     (format #f "(partial-sums current-stream))~%"))
    (display
     (format #f "          (v0-stream "))
    (display
     (format #f "(scale-stream ones v0))~%"))
    (display
     (format #f "          (cc-factor (/ dt cc)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (define a-stream~%"))
    (display
     (format #f "          (srfi-41:stream-cons~%"))
    (display
     (format #f "           (+ v0 (* rr "))
    (display
     (format #f "(srfi-41:stream-car current-stream)))~%"))
    (display
     (format #f "           (add-streams~%"))
    (display
     (format #f "            v0-stream~%"))
    (display
     (format #f "            (add-streams~%"))
    (display
     (format #f "             (scale-stream "))
    (display
     (format #f "current-stream rr)~%"))
    (display
     (format #f "             (scale-stream "))
    (display
     (format #f "pstream cc-factor)~%"))
    (display
     (format #f "             ))))~%"))
    (display
     (format #f "        a-stream~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    local-int~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((rr 5)
          (cc 1)
          (dt 0.5)
          (v0 1)
          (nmax 10)
          (debug-flag #f))
      (let ((rc-streams-func
             (rc rr cc dt)))
        (let ((rc-int-stream
               (rc-streams-func integers v0)))
          (begin
            (display (format #f "rc circuit test~%"))
            (display
             (format #f "stream results~%"))
            (do ((ii 0 (1+ ii)))
                ((>= ii nmax))
              (begin
                (let ((vv
                       (my-stream-ref rc-int-stream ii)))
                  (begin
                    (display
                     (format
                      #f "~:d : current = ~a, volt = ~a~%"
                      ii (my-stream-ref integers ii) vv))
                    (force-output)
                    ))
                ))

            (if (equal? debug-flag #t)
                (begin
                  (newline)
                  (display
                   (format #f "check calculations~%"))
                  (let ((a0 (+ v0 (* rr 1)))
                        (pstream (partial-sums integers))
                        (cfactor (/ dt cc)))
                    (let ((next-aa #f))
                      (begin
                        (display
                         (format
                          #f "~:d : current = ~a, volt = ~a,~%"
                          0 (my-stream-ref integers 0) a0))
                        (display
                         (format
                          #f "stream=~a, diff=~a~%"
                          (my-stream-ref rc-int-stream 0)
                          (- (my-stream-ref rc-int-stream 0) a0)))
                        (do ((ii 1 (1+ ii)))
                            ((>= ii nmax))
                          (begin
                            (let ((aresult
                                   (+
                                    v0
                                    (*
                                     (my-stream-ref
                                      pstream (1- ii)) cfactor)
                                    (* rr ii)))
                                  (vresult
                                   (my-stream-ref
                                    rc-int-stream ii)))
                              (begin
                                (display
                                 (format
                                  #f "~:d : current = ~a, volt = ~a,~%"
                                  ii (my-stream-ref integers ii) aresult))
                                (display
                                 (format
                                  #f "stream=~a, diff=~a~%"
                                  vresult (- vresult aresult)))
                                (force-output)
                                ))
                            ))
                        )))
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.73 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
