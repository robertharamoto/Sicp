#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.15                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Draw box-and-pointer diagrams to explain "))
    (display
     (format #f "the effect of~%"))
    (display
     (format #f "set-to-wow! on the structures z1 and "))
    (display
     (format #f "z2 above.~%"))
    (newline)
    (display
     (format #f "(define x (list 'a 'b))~%"))
    (display
     (format #f "(define z1 (cons x x))~%"))
    (display
     (format #f "(define (set-to-wow! x)~%"))
    (display
     (format #f "  (set-car! (car x) 'wow)~%"))
    (display
     (format #f "  x)~%"))
    (display
     (format #f "z1 -> [ --> x , --> x ]~%"))
    (display
     (format #f " x -> [ 'a, --]-->[ 'b, / ]~%"))
    (display
     (format #f "(set-to-wow! z1) -> "))
    (display
     (format #f "(set-to-wow! (cons x x))~%"))
    (display
     (format #f "(1) (set-car! x 'wow)~%"))
    (display
     (format #f " x -> [ 'wow, --]-->[ 'b, / ]~%"))
    (display
     (format #f "z1 -> [ --> x , --> x ]~%"))
    (display
     (format #f "z1~%"))
    (display
     (format #f "(list (list 'wow 'b) 'wow b) ~%"))
    (newline)
    (display
     (format #f "(define z2 (list (list 'a 'b) "))
    (display
     (format #f "(list 'a 'b)))~%"))
    (display
     (format #f "z2 -> [ --> y1, --]-->[ --> y2, /]~%"))
    (display
     (format #f " y1 -> [ 'a, --]-->[ 'b, / ]~%"))
    (display
     (format #f " y2 -> [ 'a, --]-->[ 'b, / ]~%"))
    (display
     (format #f "(set-to-wow! z2) -> "))
    (display
     (format #f "(set-to-wow! (list y1 y2))~%"))
    (display
     (format #f "(1) (set-car! y1 'wow)~%"))
    (display
     (format #f " y1 -> [ 'wow, --]-->[ 'b, / ]~%"))
    (display
     (format #f " y2 -> [ 'a, --]-->[ 'b, / ]~%"))
    (display
     (format #f "z2 -> [ --> y1 , --]--> "))
    (display
     (format #f "[ --> y2, / ]~%"))
    (display
     (format #f "z2~%"))
    (display
     (format #f "(list (list 'wow 'b) (list 'a 'b))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.15 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
