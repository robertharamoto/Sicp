#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.20                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Draw environment diagrams to illustrate "))
    (display
     (format #f "the evaluation of~%"))
    (display
     (format #f "the sequence of expressions~%"))
    (display
     (format #f "(define x (cons 1 2))~%"))
    (display
     (format #f "(define z (cons x x))~%"))
    (display
     (format #f "(set-car! (cdr z) 17)~%"))
    (display
     (format #f "(car x)~%"))
    (display
     (format #f "17~%"))
    (display
     (format #f "using the procedural implementation "))
    (display
     (format #f "of pairs given~%"))
    (display
     (format #f "above. (Compare exercise 3.11.)~%"))
    (newline)
    (display
     (format #f "(define (cons x y)~%"))
    (display
     (format #f "  (define (set-x! v) (set! x v))~%"))
    (display
     (format #f "  (define (set-y! v) (set! y v))~%"))
    (display
     (format #f "  (define (dispatch m)~%"))
    (display
     (format #f "    (cond ((eq? m 'car) x)~%"))
    (display
     (format #f "          ((eq? m 'cdr) y)~%"))
    (display
     (format #f "          ((eq? m 'set-car!) set-x!)~%"))
    (display
     (format #f "          ((eq? m 'set-cdr!) set-y!)~%"))
    (display
     (format #f "          (else (error "))
    (display
     (format #f "\"Undefined operation -- CONS\" m))))~%"))
    (display
     (format #f "  dispatch)~%"))
    (display
     (format #f "(define (car z) (z 'car))~%"))
    (display
     (format #f "(define (cdr z) (z 'cdr))~%"))
    (display
     (format #f "(define (set-car! z new-value)~%"))
    (display
     (format #f "  ((z 'set-car!) new-value)~%"))
    (display
     (format #f "  z)~%"))
    (display
     (format #f "(define (set-cdr! z new-value)~%"))
    (display
     (format #f "  ((z 'set-cdr!) new-value)~%"))
    (display
     (format #f "  z)~%"))
    (newline)

    (display
     (format #f "(define x (cons 1 2))~%"))
    (display
     (format #f "global: { x }, E1: { x=1, y=2 }~%"))
    (newline)
    (display
     (format #f "(define z (cons x x))~%"))
    (display
     (format #f "global: { x, z }, E1(x): { x=1, y=2 }, "))
    (display
     (format #f "E2(z):~%"))
    (display
     (format #f "    { x=x(global), y=x(global) }~%"))
    (newline)
    (display
     (format #f "(set-car! (cdr z) 17)~%"))
    (display
     (format #f "global: { x, z }, E1(x): { x=1, y=2 }, "))
    (display
     (format #f "E2(z):~%"))
    (display
     (format #f "    { x=x(global), y=x(global) }~%"))
    (display
     (format #f "E3(cdr z): { y=x(global) }~%"))
    (display
     (format #f "E4(set-car!) { param z=x(global), "))
    (display
     (format #f "param new-value=17 }~%"))
    (display
     (format #f "z=(( 17, 2) 17 2)~%"))
    (newline)
    (display
     (format #f "(car x)~%"))
    (display
     (format #f "global: { x, z }, E1(x): "))
    (display
     (format #f "{ x=17, y=2 },~%"))
    (display
     (format #f "E2(z): { x=x(global), y=x(global) }~%"))
    (display
     (format #f "17~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.20 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
