#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.72                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons
   1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons
   1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm)
      (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter
         pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (merge-weighted s1 s2 wfunc)
  (begin
    (cond
     ((srfi-41:stream-null? s1)
      (begin
        s2
        ))
     ((srfi-41:stream-null? s2)
      (begin
        s1
        ))
     (else
      (begin
        (let ((s1car (srfi-41:stream-car s1))
              (s2car (srfi-41:stream-car s2)))
          (let ((w1 (wfunc s1car))
                (w2 (wfunc s2car)))
            (begin
              (cond
               ((< w1 w2)
                (begin
                  (srfi-41:stream-cons
                   s1car
                   (merge-weighted
                    (srfi-41:stream-cdr s1) s2 wfunc))
                  ))
               ((> w1 w2)
                (begin
                  (srfi-41:stream-cons
                   s2car
                   (merge-weighted
                    s1 (srfi-41:stream-cdr s2) wfunc))
                  ))
               (else
                (begin
                  (srfi-41:stream-cons
                   s1car
                   (srfi-41:stream-cons
                    s2car
                    (merge-weighted
                     (srfi-41:stream-cdr s1)
                     (srfi-41:stream-cdr s2) wfunc)
                    ))
                  )))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-1 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ a0 a1)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-2 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ (* 2 a0) (* 3 a1) (* 5 a0 a1))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-3 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ (* a0 a0 a0) (* a1 a1 a1))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (square-weight-func alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ (* a0 a0) (* a1 a1))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (pairs
       (srfi-41:stream-cdr s)
       (srfi-41:stream-cdr t))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (weighted-pairs s t wfunc)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s)
           (srfi-41:stream-car t))
     (merge-weighted
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (weighted-pairs
       (srfi-41:stream-cdr s)
       (srfi-41:stream-cdr t) wfunc)
      wfunc))
    ))

;;;#############################################################
;;;#############################################################
(define (construct-3-sum-squares pair-stream)
  (define (local-iter
           a-stream data-m1 weight-m1 data-m2 weight-m2)
    (begin
      (let ((this-data (srfi-41:stream-car a-stream))
            (this-tail (srfi-41:stream-cdr a-stream)))
        (let ((this-weight
               (square-weight-func this-data)))
          (begin
            (cond
             ((and (not (equal? weight-m2 #f))
                   (equal? weight-m1 weight-m2)
                   (equal? this-weight weight-m1))
              (begin
                (srfi-41:stream-cons
                 (list data-m1 data-m2 this-data)
                 (local-iter
                  (srfi-41:stream-cdr a-stream)
                  #f #f #f #f))
                ))
             ((and (equal? weight-m2 #f)
                   (not (equal? weight-m1 #f))
                   (equal? this-weight weight-m1))
              (begin
                (local-iter
                 (srfi-41:stream-cdr a-stream)
                 this-data this-weight data-m1 weight-m1)
                ))
             (else
              (begin
                (local-iter
                 (srfi-41:stream-cdr a-stream)
                 this-data this-weight #f #f)
                )))
            )))
      ))
  (begin
    (local-iter
     pair-stream #f #f #f #f)
    ))

;;;#############################################################
;;;#############################################################
(define (scheme-test nmax)
  (begin
    (let ((t3-int-streams
           (construct-3-sum-squares
            (weighted-pairs
             integers integers square-weight-func))))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (let ((alist
                   (my-stream-ref t3-int-streams ii)))
              (let ((pair-1 (list-ref alist 0))
                    (pair-2 (list-ref alist 1))
                    (pair-3 (list-ref alist 2)))
                (let ((weight-1
                       (square-weight-func pair-1))
                      (weight-2
                       (square-weight-func pair-2))
                      (weight-3
                       (square-weight-func pair-3)))
                  (begin
                    (display
                     (format
                      #f "~:d : ~a = ~a = ~a~%"
                      ii pair-1 pair-2 pair-3))
                    (display
                     (ice-9-format:format
                      #f "    ~a : ~:d^2 + ~:d^2 = ~:d~%"
                      pair-1 (car pair-1)
                      (cadr pair-1) weight-1))
                    (display
                     (ice-9-format:format
                      #f "    ~a : ~:d^2 + ~:d^2 = ~:d~%"
                      pair-2 (car pair-2)
                      (cadr pair-2) weight-2))
                    (display
                     (ice-9-format:format
                      #f "    ~a : ~:d^2 + ~:d^2 = ~:d~%"
                      pair-3 (car pair-3)
                      (cadr pair-3) weight-3))
                    (force-output)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In a similar way to exercise 3.71 generate "))
    (display
     (format #f "a stream~%"))
    (display
     (format #f "of all numbers that can be written as the "))
    (display
     (format #f "sum of two~%"))
    (display
     (format #f "squares in three different ways (showing "))
    (display
     (format #f "how they can~%"))
    (display
     (format #f "be so written).~%"))
    (newline)
    (display
     (format #f "(define (construct-3-sum-squares "))
    (display
     (format #f "pair-stream)~%"))
    (display
     (format #f "  (define (local-iter~%"))
    (display
     (format #f "           a-stream data-m1 weight-m1~%"))
    (display
     (format #f "           data-m2 weight-m2)~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (let ((this-data "))
    (display
     (format #f "(srfi-41:stream-car a-stream))~%"))
    (display
     (format #f "            (this-tail "))
    (display
     (format #f "(srfi-41:stream-cdr a-stream)))~%"))
    (display
     (format #f "        (let ((this-weight~%"))
    (display
     (format #f "               (square-weight-func "))
    (display
     (format #f "this-data)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (cond~%"))
    (display
     (format #f "             ((and "))
    (display
     (format #f "(not (equal? weight-m2 #f))~%"))
    (display
     (format #f "                   (equal? "))
    (display
     (format #f "weight-m1 weight-m2)~%"))
    (display
     (format #f "                   (equal? "))
    (display
     (format #f "this-weight weight-m1))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (srfi-41:stream-cons~%"))
    (display
     (format #f "                 (list data-m1 data-m2 "))
    (display
     (format #f "this-data)~%"))
    (display
     (format #f "                 (local-iter~%"))
    (display
     (format #f "                  (srfi-41:stream-cdr "))
    (display
     (format #f "a-stream)~%"))
    (display
     (format #f "                  #f #f #f #f))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "             ((and (equal? "))
    (display
     (format #f "weight-m2 #f)~%"))
    (display
     (format #f "                   (not (equal? "))
    (display
     (format #f "weight-m1 #f))~%"))
    (display
     (format #f "                   (equal? "))
    (display
     (format #f "this-weight weight-m1))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (local-iter~%"))
    (display
     (format #f "                 (srfi-41:stream-cdr "))
    (display
     (format #f "a-stream)~%"))
    (display
     (format #f "                 this-data this-weight "))
    (display
     (format #f "data-m1 weight-m1)~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "             (else~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (local-iter~%"))
    (display
     (format #f "                 "))
    (display
     (format #f "(srfi-41:stream-cdr a-stream)~%"))
    (display
     (format #f "                 "))
    (display
     (format #f "this-data this-weight #f #f)~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (local-iter~%"))
    (display
     (format #f "     pair-stream #f #f #f #f)~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((nmax 6))
      (begin
        (scheme-test nmax)
        ))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.72 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (display
              (format #f "sum of 2 squares in three "))
             (display
              (format #f "different ways~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
