#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.63                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append
       %load-compiled-path
       (list "." "..")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons
   1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (average x y)
  (begin
    (* 0.50 (+ x y))
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-improve guess x)
  (begin
    (average guess (/ x guess))
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-stream x)
  (define guesses
    (begin
      (srfi-41:stream-cons
       1.0
       (srfi-41:stream-map
        (lambda (guess)
          (begin
            (display
             (ice-9-format:format
              #f "debug guess=~8,4f~%" guess))
            (force-output)
            (sqrt-improve guess x)
            ))
        guesses))
      ))
  (begin
    guesses
    ))

;;;#############################################################
;;;#############################################################
(define (lr-sqrt-stream x)
  (begin
    (srfi-41:stream-cons
     1.0
     (srfi-41:stream-map
      (lambda (guess)
        (begin
          (display
           (ice-9-format:format
            #f "debug lr guess=~8,4f~%" guess))
          (force-output)
          (sqrt-improve guess x)
          ))
      (lr-sqrt-stream x)))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner asks why the sqrt-stream "))
    (display
     (format #f "procedure was not~%"))
    (display
     (format #f "written in the following more "))
    (display
     (format #f "straightforward way,~%"))
    (display
     (format #f "without the local variable "))
    (display
     (format #f "guesses:~%"))
    (newline)
    (display
     (format #f "(define (sqrt-stream x)~%"))
    (display
     (format #f "  (cons-stream 1.0~%"))
    (display
     (format #f "               "))
    (display
     (format #f "(stream-map (lambda (guess)~%"))
    (display
     (format #f "                             "))
    (display
     (format #f "(sqrt-improve guess x))~%"))
    (display
     (format #f "                           "))
    (display
     (format #f "(sqrt-stream x))))~%"))
    (newline)
    (display
     (format #f "Alyssa P. Hacker replies that this "))
    (display
     (format #f "version of the~%"))
    (display
     (format #f "procedure is considerably less efficient "))
    (display
     (format #f "because it~%"))
    (display
     (format #f "performs redundant computation. Explain "))
    (display
     (format #f "Alyssa's answer.~%"))
    (display
     (format #f "Would the two versions still differ in "))
    (display
     (format #f "efficiency if our~%"))
    (display
     (format #f "implementation of delay used only "))
    (display
     (format #f "(lambda() <exp>)~%"))
    (display
     (format #f "without using the optimization provided "))
    (display
     (format #f "by memo-proc~%"))
    (display
     (format #f "(section 3.5.1)?~%"))
    (newline)
    (display
     (format #f "Louis Reasoner's version is inefficient "))
    (display
     (format #f "because of the~%"))
    (display
     (format #f "last term in stream-map, (sqrt-stream x). "))
    (display
     (format #f "That term is~%"))
    (display
     (format #f "a function call to sqrt-stream with "))
    (display
     (format #f "parameter x, which~%"))
    (display
     (format #f "creates a new stream and requires the "))
    (display
     (format #f "start of a new~%"))
    (display
     (format #f "environment and a new set of memoized "))
    (display
     (format #f "data. This~%"))
    (display
     (format #f "environment is lost once the stream-ref "))
    (display
     (format #f "function has~%"))
    (display
     (format #f "completed. And so Louis' version has to "))
    (display
     (format #f "be restarted from~%"))
    (display
     (format #f "1.0 everytime stream-ref is called, "))
    (display
     (format #f "which is the~%"))
    (display
     (format #f "redundant calculations referred to by "))
    (display
     (format #f "Alyssa P. Hacker.~%"))
    (newline)
    (display
     (format #f "The original version defines a "))
    (display
     (format #f "stream directly using~%"))
    (display
     (format #f "(define guesses "))
    (display
     (format #f "(stream-cons 1.0 (stream-map...)))~%"))
    (display
     (format #f "so guesses is a stream variable, so "))
    (display
     (format #f "subsequent calls to~%"))
    (display
     (format #f "(stream-ref guesses ii) can make use "))
    (display
     (format #f "of the cached values~%"))
    (display
     (format #f "of guesses.~%"))
    (newline)
    (display
     (format #f "If the delay procedure did not "))
    (display
     (format #f "memoize it's results,~%"))
    (display
     (format #f "then the efficiency of both versions "))
    (display
     (format #f "of sqrt-stream~%"))
    (display
     (format #f "would be the same.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x 2.0)
          (nmax 10))
      (let ((s-stream (sqrt-stream x))
            (lr-stream (lr-sqrt-stream x)))
        (begin
          (display (format #f "(stream-ref sqrt-stream)~%"))
          (do ((ii 0 (1+ ii)))
              ((>= ii nmax))
            (begin
              (display
               (ice-9-format:format
                #f "(stream-ref sqrt-stream ~a) = ~9,7f~%"
                ii (my-stream-ref s-stream ii)))
              (force-output)
              ))
          (newline)
          (display
           (format #f "Demonstration that Louis "))
          (display
           (format #f "Reasoner's "))
          (display
           (format #f "function is inefficient.~%"))
          (display
           (format #f "(stream-ref lr-sqrt-stream)~%"))
          (do ((ii 0 (1+ ii)))
              ((>= ii nmax))
            (begin
              (display
               (ice-9-format:format
                #f "(stream-ref lr-sqrt-stream ~a) "
                ii))
              (display
               (format #f "= ~9,7f~%"
                       (my-stream-ref lr-stream ii)))
              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.63 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
