#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.43                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose that the balances in three "))
    (display
     (format #f "accounts start out~%"))
    (display
     (format #f "as $10, $20, and $30, and that multiple "))
    (display
     (format #f "processes run,~%"))
    (display
     (format #f "exchanging the balances in the accounts. "))
    (display
     (format #f "Argue that if~%"))
    (display
     (format #f "the processes are run sequentially, after "))
    (display
     (format #f "any number of~%"))
    (display
     (format #f "concurrent exchanges, the account balances "))
    (display
     (format #f "should be $10,~%"))
    (display
     (format #f "$20, and $30 in some order. Draw a timing "))
    (display
     (format #f "diagram like the~%"))
    (display
     (format #f "one in figure 3.29 to show how this "))
    (display
     (format #f "condition can be~%"))
    (display
     (format #f "violated if the exchanges are implemented "))
    (display
     (format #f "using the first~%"))
    (display
     (format #f "version of the account-exchange program "))
    (display
     (format #f "in this section.~%"))
    (display
     (format #f "On the other hand, argue that even with "))
    (display
     (format #f "this exchange program,~%"))
    (display
     (format #f "the sum of the balances in the accounts "))
    (display
     (format #f "will be preserved.~%"))
    (display
     (format #f "Draw a timing diagram to show how even "))
    (display
     (format #f "this condition would~%"))
    (display
     (format #f "be violated if we did not serialize the "))
    (display
     (format #f "transactions on~%"))
    (display
     (format #f "individual accounts.~%"))
    (newline)
    (display
     (format #f "Suppose acc1=10, acc2=20, acc3=30.  Also "))
    (display
     (format #f "suppose that the~%"))
    (display
     (format #f "process p-a serially exchanges acc1 and "))
    (display
     (format #f "acc2, p-b~%"))
    (display
     (format #f "serially exchanges acc2 and acc3, and "))
    (display
     (format #f "p-c serially~%"))
    (display
     (format #f "exchanges acc1 and acc3. Then, since "))
    (display
     (format #f "all exchanges are~%"))
    (display
     (format #f "serialized, each exchange is safe from "))
    (display
     (format #f "unexpected balance~%"))
    (display
     (format #f "changes from other interleaved processes. "))
    (display
     (format #f "This means that~%"))
    (display
     (format #f "each process completes its balance exchange, "))
    (display
     (format #f "completely, before~%"))
    (display
     (format #f "the next processes exchanges it's balance "))
    (display
     (format #f "pair. So the~%"))
    (display
     (format #f "account balances are 10, 20, and 30, "))
    (display
     (format #f "in some order.~%"))
    (display
     (format #f "For this example:~%"))
    (display
     (format #f "                         account1       "))
    (display
     (format #f "account2      account3    bank~%"))
    (display
     (format #f "balance                    10            "))
    (display
     (format #f "20             30         60~%"))
    (display
     (format #f "p-a                        20            "))
    (display
     (format #f "10             30         60~%"))
    (display
     (format #f "p-b                        20            "))
    (display
     (format #f "30             10         60~%"))
    (display
     (format #f "p-c                        10            "))
    (display
     (format #f "30             20         60~%"))
    (newline)
    (display
     (format #f "Unserialized version of exchange and "))
    (display
     (format #f "serialized make-account:~%"))
    (display
     (format #f "The unprotected version of exchange is:~%"))
    (display
     (format #f "(define (exchange account1 account2)~%"))
    (display
     (format #f "  (let ((difference "))
    (display
     (format #f "(- (account1 'balance)~%"))
    (display
     (format #f "                       "))
    (display
     (format #f "(account2 'balance))))~%"))
    (display
     (format #f "    ((account1 'withdraw)"))
    (display
     (format #f "difference)~%"))
    (display
     (format #f "    ((account2 'deposit)"))
    (display
     (format #f "difference)))~%"))
    (display
     (format #f "Suppose there are two processes which "))
    (display
     (format #f "exchange account2~%"))
    (display
     (format #f "with account1 (p-a) and which exchange "))
    (display
     (format #f "account3 with~%"))
    (display
     (format #f "account2 (p-b).~%"))
    (display
     (format #f "                         account1       "))
    (display
     (format #f "account2      account3    bank~%"))
    (display
     (format #f "balance                    10            "))
    (display
     (format #f "20             30         60~%"))
    (display
     (format #f "access balance (p-a)       10            "))
    (display
     (format #f "20             30         60~%"))
    (display
     (format #f "access balance (p-b)       10            "))
    (display
     (format #f "20             30         60~%"))
    (display
     (format #f "withdraw 10 from account3"))
    (display
     (format #f "                               20~%"))
    (display
     (format #f "  (p-b)~%"))
    (display
     (format #f "deposit 10 to account2                   "))
    (display
     (format #f "30~%"))
    (display
     (format #f "  (p-b)~%"))
    (display
     (format #f "withdraw 10 from account2                "))
    (display
     (format #f "20~%"))
    (display
     (format #f "  (p-a)~%"))
    (display
     (format #f "deposit 10 to account1     "))
    (display
     (format #f "20~%"))
    (display
     (format #f "  (p-a)~%"))
    (display
     (format #f "balance                    20            "))
    (display
     (format #f "20             20         60~%"))
    (display
     (format #f "So even though the account balances are "))
    (display
     (format #f "no longer~%"))
    (display
     (format #f "10, 20, or 30 in some~%"))
    (display
     (format #f "order, the total amount of money is "))
    (display
     (format #f "still the same.~%"))
    (newline)
    (display
     (format #f "Unserialized version of exchange "))
    (display
     (format #f "and make-account:~%"))
    (display
     (format #f "Suppose there are two processes which "))
    (display
     (format #f "exchange account2~%"))
    (display
     (format #f "with account1 (p-a)~%"))
    (display
     (format #f "and which exchange account3 with "))
    (display
     (format #f "account2 (p-b).~%"))
    (display
     (format #f "                         account1       "))
    (display
     (format #f "account2      account3    bank~%"))
    (display
     (format #f "balance                    10            "))
    (display
     (format #f "20             30         60~%"))
    (display
     (format #f "access balance (p-a)       10            "))
    (display
     (format #f "20             30         60~%"))
    (display
     (format #f "difference (p-a)           10~%"))
    (display
     (format #f "access balance (p-b)       10            "))
    (display
     (format #f "20             30         60~%"))
    (display
     (format #f "withdraw 10 from account3"))
    (display
     (format #f "                               20~%"))
    (display
     (format #f "  (p-b)~%"))
    (display
     (format #f "deposit 10 to account2"))
    (display
     (format #f "                   30~%"))
    (display
     (format #f "  (p-b)~%"))
    (display
     (format #f "  but before p-a sets account2 from "))
    (display
     (format #f "the withdraw~%"))
    (display
     (format #f "function and after p-a calculates the "))
    (display
     (format #f "new value of~%"))
    (display
     (format #f "balance~%"))
    (display
     (format #f "withdraw 10 from account2"))
    (display
     (format #f "                10~%"))
    (display
     (format #f "  (p-a)~%"))
    (display
     (format #f "deposit 10 to account1     20~%"))
    (display
     (format #f "  (p-a)~%"))
    (display
     (format #f "balance                    20            "))
    (display
     (format #f "10             20         50~%"))
    (display
     (format #f "The sum of the balances changed from 60 "))
    (display
     (format #f "to 50, because~%"))
    (display
     (format #f "the individual withdraw/deposit "))
    (display
     (format #f "transactions were~%"))
    (display
     (format #f "not protected.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.43 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
