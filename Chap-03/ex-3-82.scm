#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.82                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (srfi-41:stream-null? s)
        (begin
          #f)
        (begin
          (if (<= n 0)
              (begin
                (srfi-41:stream-car s))
              (begin
                (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map
                proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones
  (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (random-in-range low high)
  (begin
    (let ((range (- high low)))
      (begin
        (define (rcalc xx)
          (begin
            (+ low (* (random:uniform) range))
            ))
        (define rr-in-rr
          (srfi-41:stream-cons
           (rcalc 1.0)
           (srfi-41:stream-map rcalc rr-in-rr)))
        (begin
          rr-in-rr
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (test-random-in-range)
  (begin
    (let ((rr-stream (random-in-range -1 1)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii 10))
          (begin
            (let ((xx (my-stream-ref rr-stream ii)))
              (begin
                (display (format #f "(~a) : ~a~%" ii xx))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (map-two-streams-pairs f strm-1 strm-2)
  (begin
    (srfi-41:stream-cons
     (f (srfi-41:stream-car strm-1) (srfi-41:stream-car strm-2))
     (map-two-streams-pairs
      f (srfi-41:stream-cdr strm-1) (srfi-41:stream-cdr strm-2)))
    ))

;;;#############################################################
;;;#############################################################
(define (unit-circle-pred xx yy)
  (begin
    (< (+ (* xx xx) (* yy yy)) 1)
    ))

;;;#############################################################
;;;#############################################################
(define (circle-pred xx-ll xx-hh yy-ll yy-hh)
  (begin
    (let ((half-width-xx (abs (* 0.50 (- xx-hh xx-ll))))
          (half-width-yy (abs (* 0.50 (- yy-hh yy-ll)))))
      (let ((cpt-xx (abs (+ half-width-xx xx-ll)))
            (cpt-yy (abs (+ half-width-yy yy-ll))))
        (let ((radius (min half-width-xx half-width-yy)))
          (begin
            (define (cpred-func this-xx this-yy)
              (begin
                (let ((diff-xx (- this-xx cpt-xx))
                      (diff-yy (- this-yy cpt-yy)))
                  (let ((rad
                         (+
                          (* diff-xx diff-xx)
                          (* diff-yy diff-yy))))
                    (begin
                      (<= rad radius)
                      )))
                ))
            (begin
              cpred-func
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-circle-pred-1 result-hash-table)
 (begin
   (let ((sub-name "test-circle-pred-1")
         (apred (circle-pred -1 1 -1 1)))
     (let ((test-list
            (list (list 0 0 #t)
                  (list 1 0 #t)
                  (list 1 1 #f)
                  (list -1 1 #f)
                  (list -1 1 #f)
                  (list -1 -1 #f)))
           (test-label-index 0))
       (begin
         (for-each
          (lambda (alist)
            (begin
              (let ((xx (list-ref alist 0))
                    (yy (list-ref alist 1))
                    (shouldbe (list-ref alist 2)))
                (let ((result (apred xx yy)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "xx=~a, yy=~a, " xx yy))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)
                      ))
                  ))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
(define (estimate-integral pred x1 x2 y1 y2)
  (begin
    (let ((xx-stream (random-in-range x1 x2))
          (yy-stream (random-in-range y1 y2)))
      (let ((experiment-stream
             (map-two-streams-pairs
              pred xx-stream yy-stream)))
        (begin
          experiment-stream
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (monte-carlo experiment-stream passed failed)
  (define (next passed failed)
    (begin
      (srfi-41:stream-cons
       (exact->inexact (/ passed (+ passed failed)))
       (monte-carlo
        (srfi-41:stream-cdr experiment-stream) passed failed))
      ))
  (begin
    (if (srfi-41:stream-car experiment-stream)
        (begin
          (next (+ passed 1) failed))
        (begin
          (next passed (+ failed 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pi-estimate x1 x2 y1 y2)
  (begin
    (let ((cpred-func (circle-pred x1 x2 y1 y2)))
      (begin
        (define pi-via-unit-circle-estimate
          (let ((experiment-stream
                 (estimate-integral cpred-func x1 x2 y1 y2)))
            (begin
              (scale-stream
               (monte-carlo experiment-stream 0 0)
               (* (- x2 x1) (- y2 y1)))
              )))
        (begin
          pi-via-unit-circle-estimate
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Redo exercise 3.5 on Monte Carlo "))
    (display
     (format #f "integration in terms of~%"))
    (display
     (format #f "streams. The stream version of "))
    (display
     (format #f "estimate-integral will not~%"))
    (display
     (format #f "have an argument telling how many trials "))
    (display
     (format #f "to perform.~%"))
    (display
     (format #f "Instead, it will produce a stream of "))
    (display
     (format #f "estimates based~%"))
    (display
     (format #f "on successively more trials.~%"))
    (newline)
    (display
     (format #f "(define (random-in-range low high)~%"))
    (display
     (format #f "  (let ((range (- high low)))~%"))
    (display
     (format #f "    (define (rcalc xx)~%"))
    (display
     (format #f "      (+ low (* (random:uniform) "))
    (display
     (format #f "range)))~%"))
    (display
     (format #f "    (define rr-in-rr~%"))
    (display
     (format #f "      (srfi-41:stream-cons~%"))
    (display
     (format #f "       (rcalc 1.0)~%"))
    (display
     (format #f "       (srfi-41:stream-map "))
    (display
     (format #f "rcalc rr-in-rr)))~%"))
    (display
     (format #f "    rr-in-rr~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (map-two-streams-pairs f "))
    (display
     (format #f "strm-1 strm-2)~%"))
    (display
     (format #f "  (srfi-41:stream-cons~%"))
    (display
     (format #f "   (f (srfi-41:stream-car strm-1) "))
    (display
     (format #f "(srfi-41:stream-car strm-2))~%"))
    (display
     (format #f "   (map-two-streams-pairs~%"))
    (display
     (format #f "    f (srfi-41:stream-cdr strm-1) "))
    (display
     (format #f "(srfi-41:stream-cdr strm-2))))~%"))
    (display
     (format #f "(define (circle-pred xx-ll "))
    (display
     (format #f "xx-hh yy-ll yy-hh)~%"))
    (display
     (format #f "  (let ((half-width-xx "))
    (display
     (format #f "(abs (* 0.50 (- xx-hh xx-ll))))~%"))
    (display
     (format #f "        (half-width-yy (abs "))
    (display
     (format #f "(* 0.50 (- yy-hh yy-ll)))))~%"))
    (display
     (format #f "    (let ((cpt-xx (abs "))
    (display
     (format #f "(+ half-width-xx xx-ll)))~%"))
    (display
     (format #f "          (cpt-yy (abs "))
    (display
     (format #f "(+ half-width-yy yy-ll))))~%"))
    (display
     (format #f "      (let ((radius (min "))
    (display
     (format #f "half-width-xx half-width-yy)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (define (cpred-func "))
    (display
     (format #f "this-xx this-yy)~%"))
    (display
     (format #f "            (let ((diff-xx "))
    (display
     (format #f "(- this-xx cpt-xx))~%"))
    (display
     (format #f "                  (diff-yy "))
    (display
     (format #f "(- this-yy cpt-yy)))~%"))
    (display
     (format #f "              (let ((rad (+ "))
    (display
     (format #f "(* diff-xx diff-xx) "))
    (display
     (format #f "(* diff-yy diff-yy))))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (<= rad radius)~%"))
    (display
     (format #f "                  ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          cpred-func~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (pi-estimate x1 x2 y1 y2)~%"))
    (display
     (format #f "  (let ((cpred-func "))
    (display
     (format #f "(circle-pred x1 x2 y1 y2)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (define pi-via-unit-circle-estimate~%"))
    (display
     (format #f "        (let ((experiment-stream~%"))
    (display
     (format #f "               (estimate-integral "))
    (display
     (format #f "cpred-func x1 x2 y1 y2)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (scale-stream~%"))
    (display
     (format #f "             (monte-carlo "))
    (display
     (format #f "experiment-stream 0 0)~%"))
    (display
     (format #f "             (* (- x2 x1) (- y2 y1)))~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      pi-via-unit-circle-estimate~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x1 -1)
          (x2 +1)
          (y1 -1)
          (y2 +1))
      (let ((test-list
             (list 100 1000 10000 100000 1000000))
            (pi-est-stream (pi-estimate x1 x2 y1 y2)))
        (begin
;;;          (test-circle-pred-1)
          (for-each
           (lambda (nn)
             (begin
               (let ((pi-est
                      (my-stream-ref pi-est-stream nn)))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "ntrials = ~:d  :  pi = ~10,8f~%"
                     nn pi-est))
                   (newline)
                   (force-output)
                   ))
               )) test-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.82 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (display (format #f "monte carlo calculation of pi~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
