#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.42                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle suggests that it's a waste "))
    (display
     (format #f "of time to~%"))
    (display
     (format #f "create a new serialized procedure in "))
    (display
     (format #f "response to every~%"))
    (display
     (format #f "withdraw and deposit message. He says "))
    (display
     (format #f "that make-account~%"))
    (display
     (format #f "could be changed so that the calls to "))
    (display
     (format #f "protected are done~%"))
    (display
     (format #f "outside the dispatch procedure. That is, "))
    (display
     (format #f "an account would~%"))
    (display
     (format #f "return the same serialized procedure "))
    (display
     (format #f "(which was created~%"))
    (display
     (format #f "at the same time as the account), each "))
    (display
     (format #f "time it is~%"))
    (display
     (format #f "asked for a withdrawal procedure.~%"))
    (newline)
    (display
     (format #f "(define (make-account balance)~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (if (>= balance amount)~%"))
    (display
     (format #f "        (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "               balance)~%"))
    (display
     (format #f "        \"Insufficient funds\"))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (set! balance "))
    (display
     (format #f "(+ balance amount))~%"))
    (display
     (format #f "    balance)~%"))
    (display
     (format #f "  (let ((protected "))
    (display
     (format #f "(make-serializer)))~%"))
    (display
     (format #f "    (let ((protected-withdraw "))
    (display
     (format #f "(protected withdraw))~%"))
    (display
     (format #f "          (protected-deposit "))
    (display
     (format #f "(protected deposit)))~%"))
    (display
     (format #f "      (define (dispatch m)~%"))
    (display
     (format #f "        (cond ((eq? m 'withdraw) "))
    (display
     (format #f "protected-withdraw)~%"))
    (display
     (format #f "              ((eq? m 'deposit) "))
    (display
     (format #f "protected-deposit)~%"))
    (display
     (format #f "              ((eq? m 'balance) "))
    (display
     (format #f "balance)~%"))
    (display
     (format #f "              (else~%"))
    (display
     (format #f "                "))
    (display
     (format #f "(error \"Unknown request "))
    (display
     (format #f "-- MAKE-ACCOUNT\"~%"))
    (display
     (format #f "                           m))))~%"))
    (display
     (format #f "      dispatch)))~%"))
    (display
     (format #f "Is this a safe change to make? In "))
    (display
     (format #f "particular, is there~%"))
    (display
     (format #f "any difference in what concurrency "))
    (display
     (format #f "is allowed by these~%"))
    (display
     (format #f "two versions of make-account?~%"))
    (newline)
    (display
     (format #f "It looks like it does exactly the same "))
    (display
     (format #f "thing as the~%"))
    (display
     (format #f "previous version of make-account, there's "))
    (display
     (format #f "no difference~%"))
    (display
     (format #f "in the concurrent behavior of these "))
    (display
     (format #f "two definitions.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.42 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
