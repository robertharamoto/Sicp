#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.48                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Explain in detail why the "))
    (display
     (format #f "deadlock-avoidance method~%"))
    (display
     (format #f "described above, (i.e., the accounts "))
    (display
     (format #f "are numbered,~%"))
    (display
     (format #f "and each process attempts to acquire "))
    (display
     (format #f "the smaller-numbered~%"))
    (display
     (format #f "account first) avoids deadlock in the "))
    (display
     (format #f "exchange problem.~%"))
    (display
     (format #f "Rewrite serialized-exchange to "))
    (display
     (format #f "incorporate this idea.~%"))
    (display
     (format #f "(You will also need to modify "))
    (display
     (format #f "make-account so that each~%"))
    (display
     (format #f "account is created with a number, which "))
    (display
     (format #f "can be accessed~%"))
    (display
     (format #f "by sending an appropriate message.)~%"))
    (newline)
    (display
     (format #f "Exchange deadlock problem: suppose peter "))
    (display
     (format #f "tries to exchange~%"))
    (display
     (format #f "acc1 with acc2, at the same time that "))
    (display
     (format #f "paul tries to~%"))
    (display
     (format #f "exchange acc2 with acc1.  If each account "))
    (display
     (format #f "has a unique~%"))
    (display
     (format #f "numeric identifier, then if you always "))
    (display
     (format #f "try to lock~%"))
    (display
     (format #f "the account with the lower number first, "))
    (display
     (format #f "both will try~%"))
    (display
     (format #f "to acquire a lock on the same account "))
    (display
     (format #f "and one of~%"))
    (display
     (format #f "them won't succeed, so will have "))
    (display
     (format #f "to wait until~%"))
    (display
     (format #f "it has been released.~%"))
    (newline)
    (display
     (format #f "(define *global-unique-id* 1)~%"))
    (display
     (format #f "(define (make-account-and-serializer "))
    (display
     (format #f "balance)~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (if (>= balance amount)~%"))
    (display
     (format #f "        (begin (set! balance "))
    (display
     (format #f "(- balance amount))~%"))
    (display
     (format #f "               balance)~%"))
    (display
     (format #f "        \"Insufficient funds\"))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (set! balance "))
    (display
     (format #f "(+ balance amount))~%"))
    (display
     (format #f "    balance)~%"))
    (display
     (format #f "  (let ((balance-serializer "))
    (display
     (format #f "(make-serializer))~%"))
    (display
     (format #f "        (gid (1+ *global-unique-id*)))~%"))
    (display
     (format #f "    (define (dispatch m)~%"))
    (display
     (format #f "      (cond ((eq? m 'withdraw) "))
    (display
     (format #f "withdraw)~%"))
    (display
     (format #f "            ((eq? m 'deposit) "))
    (display
     (format #f "deposit)~%"))
    (display
     (format #f "            ((eq? m 'balance) "))
    (display
     (format #f "balance)~%"))
    (display
     (format #f "            ((eq? m 'unique-id) gid)~%"))
    (display
     (format #f "            ((eq? m 'serializer) "))
    (display
     (format #f "balance-serializer)~%"))
    (display
     (format #f "            (else~%"))
    (display
     (format #f "      (error \"Unknown request "))
    (display
     (format #f "-- MAKE-ACCOUNT\"~%"))
    (display
     (format #f "                         m))))~%"))
    (display
     (format #f "    (set! *global-unique-id* "))
    (display
     (format #f "(1+ *global-unique-id*))~%"))
    (display
     (format #f "    dispatch))~%"))
    (display
     (format #f "(define (serialized-exchange "))
    (display
     (format #f "account1 account2)~%"))
    (display
     (format #f "  (let ((serializer1 "))
    (display
     (format #f "(account1 'serializer))~%"))
    (display
     (format #f "        (serializer2 "))
    (display
     (format #f "(account2 'serializer))~%"))
    (display
     (format #f "        (gid1 (account1 'unique-id))~%"))
    (display
     (format #f "        (gid2 (account2 'unique-id)))~%"))
    (display
     (format #f "    (if (> gid1 gid2)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          ((serializer1 "))
    (display
     (format #f "(serializer2 exchange))~%"))
    (display
     (format #f "             account1~%"))
    (display
     (format #f "             account2))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          ((serializer2 "))
    (display
     (format #f "(serializer1 exchange))~%"))
    (display
     (format #f "             account1~%"))
    (display
     (format #f "             account2)))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.48 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
