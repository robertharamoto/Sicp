#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.31                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The internal procedure "))
    (display
     (format #f "accept-action-procedure! defined~%"))
    (display
     (format #f "in make-wire specifies that when a "))
    (display
     (format #f "new action procedure~%"))
    (display
     (format #f "is added to a wire, the procedure is "))
    (display
     (format #f "immediately run.~%"))
    (display
     (format #f "Explain why this initialization is "))
    (display
     (format #f "necessary. In~%"))
    (display
     (format #f "particular, trace through the "))
    (display
     (format #f "half-adder example~%"))
    (display
     (format #f "in the paragraphs above and say "))
    (display
     (format #f "how the system's~%"))
    (display
     (format #f "response would differ if we had "))
    (display
     (format #f "defined~%"))
    (display
     (format #f "accept-action-procedure! as:~%"))
    (newline)
    (display
     (format #f "(define (accept-action-procedure! proc)~%"))
    (display
     (format #f "  (set! action-procedures "))
    (display
     (format #f "(cons proc action-procedures)))~%"))
    (newline)
    (display
     (format #f "Initially, the output wires, s and c are "))
    (display
     (format #f "in inconsistent~%"))
    (display
     (format #f "states with the input states.  By "))
    (display
     (format #f "running the action~%"))
    (display
     (format #f "procedures as soon as it's added, "))
    (display
     (format #f "the state of each~%"))
    (display
     (format #f "gate can be set to the "))
    (display
     (format #f "correct state.~%"))
    (newline)
    (display
     (format #f "(define (half-adder a b s c)~%"))
    (display
     (format #f "  (let ((d (make-wire)) "))
    (display
     (format #f "(e (make-wire)))~%"))
    (display
     (format #f "    (or-gate a b d)~%"))
    (display
     (format #f "    (and-gate a b c)~%"))
    (display
     (format #f "    (inverter c e)~%"))
    (display
     (format #f "    (and-gate d e s)~%"))
    (display
     (format #f "    'ok))~%"))
    (newline)
    (display
     (format #f "Assume that initially the wires a, b, "))
    (display
     (format #f "s, and c are~%"))
    (display
     (format #f "all 0. The d and e are 0, and the "))
    (display
     (format #f "or-gate sets d~%"))
    (display
     (format #f "to 0, and the and-gate sets c to 0. "))
    (display
     (format #f "The inverter~%"))
    (display
     (format #f "sets e to 1, and the final and-gate "))
    (display
     (format #f "sets s to 0.~%"))
    (display
     (format #f "So when the wires "))
    (display
     (format #f "accept-action-procedure! is called,~%"))
    (display
     (format #f "both s and c are what they should "))
    (display
     (format #f "be with a = b = 0.~%"))
    (newline)
    (display
     (format #f "If (proc) is not called in the "))
    (display
     (format #f "wire's~%"))
    (display
     (format #f "accept-action-procedure!, then "))
    (display
     (format #f "all gates are~%"))
    (display
     (format #f "set-up but not consistent. That is, "))
    (display
     (format #f "if a, b, s,~%"))
    (display
     (format #f "and c are initially zero, then "))
    (display
     (format #f "they remain so.~%"))
    (display
     (format #f "This may not be a problem in this "))
    (display
     (format #f "case, however,~%"))
    (display
     (format #f "if a different circuit should have "))
    (display
     (format #f "it's output is~%"))
    (display
     (format #f "equal to 1, but initially it was "))
    (display
     (format #f "0, this may~%"))
    (display
     (format #f "cause a problem when stringing "))
    (display
     (format #f "together elements~%"))
    (display
     (format #f "which depend on the intermediate "))
    (display
     (format #f "results.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.31 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
