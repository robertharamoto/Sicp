#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.69                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         ))
     stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-limit astream tolerance)
  (define (local-rec astream prev-val count tol)
    (begin
      (if (srfi-41:stream-null? astream)
          (begin
            (list prev-val count))
          (begin
            (let ((a0 (srfi-41:stream-car astream))
                  (tail-stream (srfi-41:stream-cdr astream)))
              (let ((diff (abs (- a0 prev-val))))
                (begin
                  (if (< diff tol)
                      (begin
                        (list a0 count))
                      (begin
                        (local-rec tail-stream a0 (1+ count) tol)
                        ))
                  )))
            ))
      ))
  (begin
    (local-rec (srfi-41:stream-cdr astream)
               (srfi-41:stream-car astream)
               0 tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s) (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (pairs (srfi-41:stream-cdr s) (srfi-41:stream-cdr t))
      ))
    ))

;;;#############################################################
;;;#############################################################
;;; for streams of lists with triple integers, requiring that
;;; each triple is a pythagorean triple (x^2+y^2=z^2)
;;; if its greater than some max return the null stream to eliminate
;;; a lot of unnecessary calculations
(define-syntax check-size-code
  (syntax-rules ()
    ((check-size-code a-list-stream stream-ok-flag)
     (begin
       (if (srfi-41:stream-null? a-list-stream)
           (begin
             (set! stream-ok-flag #f))
           (begin
             (let ((tfirst (srfi-41:stream-car a-list-stream))
                   (ttail (srfi-41:stream-cdr a-list-stream)))
               (let ((xx (car tfirst))
                     (yy (cadr tfirst))
                     (zz (caddr tfirst)))
                 (let ((t-rr2 (+ (* xx xx) (* yy yy))))
                   (let ((zz2 (* zz zz)))
                     (begin
                       (if (> zz2 t-rr2)
                           (set! stream-ok-flag #f)
                           (set! stream-ok-flag #t)
                           ))
                     ))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave-triple-lists s-list-stream t-list-stream)
  (begin
    (let ((s-ok-flag #f)
          (t-ok-flag #f))
      (begin
        (check-size-code s-list-stream s-ok-flag)
        (check-size-code t-list-stream t-ok-flag)

        (cond
         ((and (equal? s-ok-flag #f) (equal? t-ok-flag #f))
          (begin
            srfi-41:stream-null
            ))
         ((and (equal? s-ok-flag #f) (equal? t-ok-flag #t))
          (begin
            t-list-stream
            ))
         ((and (equal? s-ok-flag #t) (equal? t-ok-flag #f))
          (begin
            s-list-stream
            ))
         (else
          (begin
            (srfi-41:stream-cons
             (srfi-41:stream-car s-list-stream)
             (interleave-triple-lists
              t-list-stream (srfi-41:stream-cdr s-list-stream)))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs-list-int s-list u)
  (begin
    (let ((sfirst (srfi-41:stream-car s-list))
          (stail (srfi-41:stream-cdr s-list)))
      (begin
        (gc)
        (srfi-41:stream-cons
         (append
          sfirst
          (list (srfi-41:stream-car u)))
         (interleave-triple-lists
          (srfi-41:stream-map
           (lambda (x)
             (begin
               (append sfirst (list x))
               )) u)
          (pairs-list-int stail u)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (triples s t u)
  (begin
    (pairs-list-int
     (pairs s t)
     u)
    ))

;;;#############################################################
;;;#############################################################
(define pythagorean-stream
  (begin
    (stream-filter
     (lambda (atriple)
       (begin
         (let ((x (list-ref atriple 0))
               (y (list-ref atriple 1))
               (z (list-ref atriple 2)))
           (let ((r2 (+ (* x x) (* y y)))
                 (z2 (* z z)))
             (begin
               (equal? r2 z2)
               )))
         ))
     (triples integers integers integers))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Write a procedure triples that takes "))
    (display
     (format #f "three infinite~%"))
    (display
     (format #f "streams, S, T, and U, and produces the "))
    (display
     (format #f "stream of triples~%"))
    (display
     (format #f "(Si,Tj,Uk) such that i <= j <= k. "))
    (display
     (format #f "Use triples to~%"))
    (display
     (format #f "generate the stream of all Pythagorean "))
    (display
     (format #f "triples of~%"))
    (display
     (format #f "positive integers, i.e., the triples "))
    (display
     (format #f "(i,j,k) such~%"))
    (display
     (format #f "that i <= j and i^2 + j^2 = k^2.~%"))
    (newline)
    (display
     (format #f "(define pythagorean-stream~%"))
    (display
     (format #f "  (stream-filter~%"))
    (display
     (format #f "   (lambda (atriple)~%"))
    (display
     (format #f "     (let ((x (list-ref atriple 0))~%"))
    (display
     (format #f "           (y (list-ref atriple 1))~%"))
    (display
     (format #f "           (z (list-ref atriple 2)))~%"))
    (display
     (format #f "       (let ((r2 (+ (* x x) (* y y)))~%"))
    (display
     (format #f "             (z2 (* z z)))~%"))
    (display
     (format #f "       (begin~%"))
    (display
     (format #f "         (equal? r2 z2)~%"))
    (display
     (format #f "         )))~%"))
    (display
     (format #f "   (triples integers integers integers)~%"))
    (display
     (format #f "   ))~%"))
    (display
     (format #f "(define (triples s t u)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (pairs-list-int~%"))
    (display
     (format #f "     (pairs s t)~%"))
    (display
     (format #f "     u)~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (pairs-list-int s-list u)~%"))
    (display
     (format #f "  (let ((sfirst "))
    (display
     (format #f "(srfi-41:stream-car s-list))~%"))
    (display
     (format #f "        (stail "))
    (display
     (format #f "(srfi-41:stream-cdr s-list)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (gc)~%"))
    (display
     (format #f "      (srfi-41:stream-cons~%"))
    (display
     (format #f "       (append~%"))
    (display
     (format #f "        sfirst~%"))
    (display
     (format #f "        (list (srfi-41:stream-car u)))~%"))
    (display
     (format #f "       (interleave-triple-lists~%"))
    (display
     (format #f "        (srfi-41:stream-map~%"))
    (display
     (format #f "         (lambda (x) "))
    (display
     (format #f "(append sfirst (list x))) u)~%"))
    (display
     (format #f "        (pairs-list-int stail u)))~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f ";;; throw away streams that have no chance "))
    (display
     (format #f "of being pythagorean triples~%"))
    (display
     (format #f "(define-syntax check-size-code~%"))
    (display
     (format #f "  (syntax-rules ()~%"))
    (display
     (format #f "    ((check-size-code "))
    (display
     (format #f "a-list-stream stream-ok-flag)~%"))
    (display
     (format #f "     (begin~%"))
    (display
     (format #f "       (if (srfi-41:stream-null? "))
    (display
     (format #f "a-list-stream)~%"))
    (display
     (format #f "           (set! stream-ok-flag #f)~%"))
    (display
     (format #f "           (begin~%"))
    (display
     (format #f "             (let ((tfirst "))
    (display
     (format #f "(srfi-41:stream-car a-list-stream))~%"))
    (display
     (format #f "                   (ttail "))
    (display
     (format #f "(srfi-41:stream-cdr a-list-stream)))~%"))
    (display
     (format #f "               (let ((xx "))
    (display
     (format #f "(car tfirst))~%"))
    (display
     (format #f "                     (yy "))
    (display
     (format #f "(cadr tfirst))~%"))
    (display
     (format #f "                     (zz "))
    (display
     (format #f "(caddr tfirst)))~%"))
    (display
     (format #f "                 (let ((t-rr2 "))
    (display
     (format #f "(+ (* xx xx) (* yy yy))))~%"))
    (display
     (format #f "                   (let "))
    (display
     (format #f "((zz2 (* zz zz)))~%"))
    (display
     (format #f "                     (begin~%"))
    (display
     (format #f "                       (if (> zz2 t-rr2)~%"))
    (display
     (format #f "                           (set! "))
    (display
     (format #f "stream-ok-flag #f)~%"))
    (display
     (format #f "                           (set! "))
    (display
     (format #f "stream-ok-flag #t)~%"))
    (display
     (format #f "                           ))~%"))
    (display
     (format #f "                     ))~%"))
    (display
     (format #f "                 ))~%"))
    (display
     (format #f "             ))~%"))
    (display
     (format #f "       ))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(define (interleave-triple-lists "))
    (display
     (format #f "s-list-stream t-list-stream)~%"))
    (display
     (format #f "  (let ((s-ok-flag #f)~%"))
    (display
     (format #f "        (t-ok-flag #f))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (check-size-code "))
    (display
     (format #f "s-list-stream s-ok-flag)~%"))
    (display
     (format #f "      (check-size-code "))
    (display
     (format #f "t-list-stream t-ok-flag)~%"))
    (display
     (format #f "      (cond~%"))
    (display
     (format #f "       ((and (equal? s-ok-flag #f)~%"))
    (display
     (format #f "          (equal? t-ok-flag #f))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          srfi-41:stream-null~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "       ((and (equal? s-ok-flag #f)~%"))
    (display
     (format #f "             (equal? t-ok-flag #t))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          t-list-stream~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "       ((and (equal? s-ok-flag #t)~%"))
    (display
     (format #f "             (equal? t-ok-flag #f))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          s-list-stream~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "       (else~%"))
    (display
     (format #f "        (srfi-41:stream-cons~%"))
    (display
     (format #f "         (srfi-41:stream-car "))
    (display
     (format #f "s-list-stream)~%"))
    (display
     (format #f "         (interleave-triple-lists~%"))
    (display
     (format #f "          t-list-stream "))
    (display
     (format #f "(srfi-41:stream-cdr~%"))
    (display
     (format #f "             s-list-stream)))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "note: a new version of interleave "))
    (display
     (format #f "needed to be~%"))
    (display
     (format #f "defined as the current version would "))
    (display
     (format #f "examine too many~%"))
    (display
     (format #f "triple pairs that could not be "))
    (display
     (format #f "pythagorean triples,~%"))
    (display
     (format #f "(e.g. (1, 2, 9,999)).~%"))
    (newline)
    (display
     (format #f "First triple is (3, 4, 5),~%"))
    (display
     (format #f "the second is (6, 8, 10),~%"))
    (display
     (format #f "the third is (5, 12, 13).~%"))
    (display
     (format #f "The second triple is not a primative~%"))
    (display
     (format #f "Pythagorean triple, since it is "))
    (display
     (format #f "a multiple of the first.~%"))
    (display
     (format #f "For more see:~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Pythagorean_triple~%"))

    (display (format #f "Pythagorean triples~%"))
    (let ((nmax 5))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (let ((alist (my-stream-ref pythagorean-stream ii)))
              (let ((x (list-ref alist 0))
                    (y (list-ref alist 1))
                    (z (list-ref alist 2)))
                (begin
                  (if (equal? (+ (* x x) (* y y)) (* z z))
                      (begin
                        (display
                         (ice-9-format:format
                          #f "(~a) ~a : ~:d^2 + ~:d^2~%"
                          ii alist x y))
                        (display
                         (ice-9-format:format
                          #f "= ~:d + ~:d = ~:d = ~:d^2~%"
                          (* x x) (* y y) (* z z) z)))
                      (begin
                        (display
                         (ice-9-format:format
                          #f "(~a) error: ~:d^2 + ~:d^2~%"
                          ii x y))
                        (display
                         (ice-9-format:format
                          #f "= ~:d + ~:d = ~:d~%"
                          (* x x) (* y y) (+ (* x x) (* y y))))
                        (display
                         (ice-9-format:format
                          #f "!= ~:d = ~:d^2~%"
                          (* z z) z))
                        ))
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.69 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
