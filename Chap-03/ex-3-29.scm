#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.29                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Another way to construct an or-gate is "))
    (display
     (format #f "as a compound~%"))
    (display
     (format #f "digital logic device, built from "))
    (display
     (format #f "and-gates and~%"))
    (display
     (format #f "inverters. Define a procedure or-gate "))
    (display
     (format #f "that accomplishes~%"))
    (display
     (format #f "this. What is the delay time of the "))
    (display
     (format #f "or-gate in terms~%"))
    (display
     (format #f "of and-gate-delay and inverter-delay?~%"))
    (newline)
    (display
     (format #f "According to the nand logic article~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/NAND_logic~%"))
    (display
     (format #f "the or-gate can be constructed from "))
    (display
     (format #f "3 nand-gates:~%"))
    (display
     (format #f "wire a ==>nand -->nand --> output~%"))
    (display
     (format #f "wire b ==>nand ---^ ~%"))
    (newline)
    (display
     (format #f "(define (nand-gate a1 a2 output)~%"))
    (display
     (format #f "  (define (nand-action-procedure)~%"))
    (display
     (format #f "    (let ((new-value~%"))
    (display
     (format #f "           (logical-not~%"))
    (display
     (format #f "            (logical-and "))
    (display
     (format #f "(get-signal a1) (get-signal a2)))))~%"))
    (display
     (format #f "      (after-delay "))
    (display
     (format #f "(+ and-gate-delay inverter-delay) ~%"))
    (display
     (format #f "                  (lambda ()~%"))
    (display
     (format #f "                    (set-signal! "))
    (display
     (format #f "output new-value)))))~%"))
    (display
     (format #f "  (add-action! a1 "))
    (display
     (format #f "nand-action-procedure)~%"))
    (display
     (format #f "  (add-action! a2 "))
    (display
     (format #f "nand-action-procedure)~%"))
    (display
     (format #f "  'ok)~%"))
    (newline)
    (display
     (format #f "(define (or-gate o1 o2 output)~%"))
    (display
     (format #f "  (define (or-action-procedure)~%"))
    (display
     (format #f "    (let ((sig-1 (get-signal o1))~%"))
    (display
     (format #f "          (sig-2 (get-signal o2)))~%"))
    (display
     (format #f "     (let ((val-1 (logical-not "))
    (display
     (format #f "(logical-and sig-1 sig-1)))~%"))
    (display
     (format #f "           (val-2 (logical-not "))
    (display
     (format #f "(logical-and sig-2 sig-2))))~%"))
    (display
     (format #f "      (let ((new-value~%"))
    (display
     (format #f "             (logical-not "))
    (display
     (format #f "(logical-and val-1 val-2)))~%"))
    (display
     (format #f "            (total-delay "))
    (display
     (format #f "(* 3 (+ and-gate-delay inverter-delay))))~%"))
    (display
     (format #f "        (after-delay total-delay~%"))
    (display
     (format #f "                     (lambda ()~%"))
    (display
     (format #f "                       (set-signal! "))
    (display
     (format #f "output new-value)))~%"))
    (display
     (format #f "         ))))~%"))
    (display
     (format #f "  (add-action! o1 or-action-procedure)~%"))
    (display
     (format #f "  (add-action! o2 or-action-procedure)~%"))
    (display
     (format #f "  'ok)~%"))
    (newline)
    (display
     (format #f "The amount of delay is 3 times the sum "))
    (display
     (format #f "of the~%"))
    (display
     (format #f "and-gate-delay and the inverter-delay.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.29 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
