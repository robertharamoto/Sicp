#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.12                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The following procedure for appending "))
    (display
     (format #f "lists was introduced~%"))
    (display
     (format #f "in section 2.2.1:~%"))
    (display
     (format #f "(define (append x y)~%"))
    (display
     (format #f "  (if (null? x)~%"))
    (display
     (format #f "      y~%"))
    (display
     (format #f "      (cons (car x) "))
    (display
     (format #f "(append (cdr x) y))))~%"))
    (display
     (format #f "Append forms a new list by successively "))
    (display
     (format #f "consing the elements~%"))
    (display
     (format #f "of x onto y. The procedure append! is "))
    (display
     (format #f "similar to append,~%"))
    (display
     (format #f "but it is a mutator rather than a "))
    (display
     (format #f "constructor. It~%"))
    (display
     (format #f "appends the lists by splicing them "))
    (display
     (format #f "together, modifying~%"))
    (display
     (format #f "the final pair of x so that its cdr is "))
    (display
     (format #f "now y. (It is~%"))
    (display
     (format #f "an error to call append! with "))
    (display
     (format #f "an empty x.)~%"))
    (newline)
    (display
     (format #f "(define (append! x y)~%"))
    (display
     (format #f "  (set-cdr! (last-pair x) y)~%"))
    (display
     (format #f "  x)~%"))
    (display
     (format #f "Here last-pair is a procedure that "))
    (display
     (format #f "returns the last~%"))
    (display
     (format #f "pair in its argument:~%"))
    (newline)
    (display
     (format #f "(define (last-pair x)~%"))
    (display
     (format #f "  (if (null? (cdr x))~%"))
    (display
     (format #f "      x~%"))
    (display
     (format #f "      (last-pair (cdr x))))~%"))
    (display
     (format #f "Consider the interactions:~%"))
    (newline)
    (display
     (format #f "(define x (list 'a 'b))~%"))
    (display
     (format #f "(define y (list 'c 'd))~%"))
    (display
     (format #f "(define z (append x y))~%"))
    (display
     (format #f "z~%"))
    (display
     (format #f "(a b c d)~%"))
    (display
     (format #f "(cdr x)~%"))
    (display
     (format #f "<response>~%"))
    (display
     (format #f "(define w (append! x y))~%"))
    (display
     (format #f "w~%"))
    (display
     (format #f "(a b c d)~%"))
    (display
     (format #f "(cdr x)~%"))
    (display
     (format #f "<response>~%"))
    (newline)
    (display
     (format #f "What are the missing <response>s? Draw "))
    (display
     (format #f "box-and-pointer~%"))
    (display
     (format #f "diagrams to explain your "))
    (display
     (format #f "answer.~%"))
    (newline)
    (display
     (format #f "(cdr x) -> (list b)~%"))
    (display
     (format #f "x -> [ 'a, --]->[ 'b, / ]~%"))
    (display
     (format #f "(cdr x) -> [ 'b, / ]~%"))
    (newline)
    (display
     (format #f "(define w (append! x y))~%"))
    (display
     (format #f "(cdr x) -> (list 'b 'c 'd)~%"))
    (display
     (format #f "x -> [ 'a, --]->[ 'b, "))
    (display
     (format #f "--]->[ 'c, --]->[ 'd, / ]~%"))
    (display
     (format #f "(cdr x) -> [ 'b, "))
    (display
     (format #f "--]->[ 'c, --]->[ 'd, / ]~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.12 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
