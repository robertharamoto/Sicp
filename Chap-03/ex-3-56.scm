#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.56                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  last updated August 16, 2022                         ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (prime? nn)
  (define (smallest-divisor nn test-divisor max-divisor)
    (begin
      (cond
       ((> test-divisor max-divisor)
        (begin
          nn
          ))
       ((zero? (modulo nn test-divisor))
        (begin
          test-divisor
          ))
       (else
        (begin
          (smallest-divisor
           nn (+ test-divisor 2) max-divisor)
          )))
      ))
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((= nn 2)
      (begin
        #t
        ))
     ((even? nn)
      (begin
        #f
        ))
     (else
      (begin
        (let ((max-divisor
               (+ (exact-integer-sqrt nn) 1)))
          (begin
            (= nn (smallest-divisor nn 3 max-divisor))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (divide-all-factors this-num this-factor)
  (begin
    (let ((ll-num this-num)
          (acc-list (list)))
      (begin
        (while (and (zero? (modulo ll-num this-factor))
                    (> ll-num 1))
               (begin
                 (set! ll-num (euclidean/ ll-num this-factor))
                 (set! acc-list (cons this-factor acc-list))
                 ))

        (list ll-num acc-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax divide-macro-process
  (syntax-rules ()
    ((divide-macro-process
      ll-num div-factor constant-number result-list)
     (begin
       (if (prime? div-factor)
           (begin
             (let ((partial-list
                    (divide-all-factors ll-num div-factor)))
               (begin
                 (set! ll-num (car partial-list))
                 (set! result-list
                       (append result-list (cadr partial-list)))
                 ))
             ))

       (let ((this-divisor
              (euclidean/ constant-number div-factor)))
         (begin
           (if (and (> this-divisor div-factor)
                    (equal? (memq this-divisor result-list) #f)
                    (prime? this-divisor))
               (begin
                 (let ((partial-list
                        (divide-all-factors ll-num this-divisor)))
                   (begin
                     (set! ll-num (car partial-list))
                     (set! result-list
                           (append result-list (cadr partial-list)))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (prime-factor-list input-number)
  (begin
    (if (<= input-number 1)
        (begin
          (list))
        (begin
          (let ((result-list (list))
                (constant-number input-number)
                (ll-num input-number)
                (ll-max (+ 1 (exact-integer-sqrt input-number))))
            (begin
              (if (even? constant-number)
                  (begin
                    (divide-macro-process
                     ll-num 2 constant-number result-list)
                    ))

              (do ((ii 3 (+ ii 2)))
                  ((or (> ii ll-max) (<= ll-num 1)))
                (begin
                  (if (zero? (modulo constant-number ii))
                      (begin
                        (divide-macro-process
                         ll-num ii constant-number result-list)
                        ))
                  ))

              (if (< (length result-list) 1)
                  (begin
                    (list input-number))
                  (begin
                    (sort result-list <)
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (srfi-41:stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map (lambda (x) (* x factor)) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (merge s1 s2)
  (begin
    (cond
     ((srfi-41:stream-null? s1)
      (begin
        s2
        ))
     ((srfi-41:stream-null? s2)
      (begin
        s1
        ))
     (else
      (begin
        (let ((s1car (srfi-41:stream-car s1))
              (s2car (srfi-41:stream-car s2)))
          (begin
            (cond
             ((< s1car s2car)
              (begin
                (srfi-41:stream-cons
                 s1car
                 (merge (srfi-41:stream-cdr s1)
                        s2))
                ))
             ((> s1car s2car)
              (begin
                (srfi-41:stream-cons
                 s2car
                 (merge s1
                        (srfi-41:stream-cdr s2)))
                ))
             (else
              (begin
                (srfi-41:stream-cons
                 s1car
                 (merge (srfi-41:stream-cdr s1)
                        (srfi-41:stream-cdr s2)))
                )))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define hamming-stream
  (begin
    (srfi-41:stream-cons
     1
     (merge (scale-stream hamming-stream 2)
            (merge (scale-stream hamming-stream 3)
                   (scale-stream hamming-stream 5))
            ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A famous problem, first raised by "))
    (display
     (format #f "R. Hamming, is to~%"))
    (display
     (format #f "enumerate, in ascending order with "))
    (display
     (format #f "no repetitions,~%"))
    (display
     (format #f "all positive integers with no prime "))
    (display
     (format #f "factors other~%"))
    (display
     (format #f "than 2, 3, or 5. One obvious way to "))
    (display
     (format #f "do this is~%"))
    (display
     (format #f "to simply test each integer in turn "))
    (display
     (format #f "to see whether~%"))
    (display
     (format #f "it has any factors other than 2, "))
    (display
     (format #f "3, and 5. But~%"))
    (display
     (format #f "this is very inefficient, since, as "))
    (display
     (format #f "the integers get~%"))
    (display
     (format #f "larger, fewer and fewer of them fit "))
    (display
     (format #f "the requirement. As~%"))
    (display
     (format #f "an alternative, let us call the "))
    (display
     (format #f "required stream of~%"))
    (display
     (format #f "numbers S and notice the following "))
    (display
     (format #f "facts about it.~%"))
    (display
     (format #f "S begins with 1. The elements of "))
    (display
     (format #f "(scale-stream S 2)~%"))
    (display
     (format #f "are also elements of S. The same is "))
    (display
     (format #f "true for~%"))
    (display
     (format #f "(scale-stream S 3) and (scale-stream 5 S). "))
    (display
     (format #f "These are~%"))
    (display
     (format #f "all the elements of S. Now all we have "))
    (display
     (format #f "to do is combine~%"))
    (display
     (format #f "elements from these sources. For this "))
    (display
     (format #f "we define a~%"))
    (display
     (format #f "procedure merge that combines two "))
    (display
     (format #f "ordered streams into~%"))
    (display
     (format #f "one ordered result stream, eliminating "))
    (display
     (format #f "repetitions:~%"))
    (newline)
    (display
     (format #f "(define (merge s1 s2)~%"))
    (display
     (format #f "  (cond ((stream-null? s1) s2)~%"))
    (display
     (format #f "        ((stream-null? s2) s1)~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         (let ((s1car (stream-car s1))~%"))
    (display
     (format #f "               (s2car (stream-car s2)))~%"))
    (display
     (format #f "           (cond ((< s1car s2car)~%"))
    (display
     (format #f "                  (cons-stream s1car "))
    (display
     (format #f "(merge (stream-cdr s1) s2)))~%"))
    (display
     (format #f "                 ((> s1car s2car)~%"))
    (display
     (format #f "                  (cons-stream s2car "))
    (display
     (format #f "(merge s1 (stream-cdr s2))))~%"))
    (display
     (format #f "                 (else~%"))
    (display
     (format #f "                  (cons-stream s1car~%"))
    (display
     (format #f "                               "))
    (display
     (format #f "(merge (stream-cdr s1)~%"))
    (display
     (format #f "                                      "))
    (display
     (format #f "(stream-cdr s2)))))))))~%"))
    (display
     (format #f "Then the required stream may be constructed "))
    (display
     (format #f "with merge,~%"))
    (display
     (format #f "as follows:~%"))
    (display
     (format #f "(define S (cons-stream 1 "))
    (display
     (format #f "(merge <??> <??>)))~%"))
    (display
     (format #f "Fill in the missing expressions in the "))
    (display
     (format #f "places marked~%"))
    (display
     (format #f "<??> above.~%"))
    (newline)
    (display
     (format #f "(define hamming-stream~%"))
    (display
     (format #f "  (srfi-41:stream-cons~%"))
    (display
     (format #f "   1 (merge (scale-stream "))
    (display
     (format #f "hamming-stream 2)~%"))
    (display
     (format #f "            (merge (scale-stream "))
    (display
     (format #f "hamming-stream 3)~%"))
    (display
     (format #f "    (scale-stream hamming-stream 5))~%"))
    (display
     (format #f "            )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((h-stream hamming-stream)
          (nmax 20))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (let ((this-num
                   (my-stream-ref h-stream ii)))
              (let ((flist
                     (prime-factor-list this-num)))
                (begin
                  (display
                   (format
                    #f "(stream-ref hamming-stream ~a) = "
                    ii))
                  (display
                   (format
                    #f "~a  : factors = ~a~%"
                    this-num flist))
                  (force-output)
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.56 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
