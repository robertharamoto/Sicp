#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.11                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In section 3.2.3 we saw how the "))
    (display
     (format #f "environment model~%"))
    (display
     (format #f "described the behavior of procedures "))
    (display
     (format #f "with local state.~%"))
    (display
     (format #f "Now we have seen how internal "))
    (display
     (format #f "definitions work. A~%"))
    (display
     (format #f "typical message-passing procedure "))
    (display
     (format #f "contains both of~%"))
    (display
     (format #f "these aspects. Consider the bank "))
    (display
     (format #f "account procedure~%"))
    (display
     (format #f "of section 3.1.1:~%"))
    (newline)
    (display
     (format #f "(define (make-account balance)~%"))
    (display
     (format #f "  (define (withdraw amount)~%"))
    (display
     (format #f "    (if (>= balance amount)~%"))
    (display
     (format #f "        (begin (set! "))
    (display
     (format #f "balance (- balance amount))~%"))
    (display
     (format #f "               balance)~%"))
    (display
     (format #f "        \"Insufficient funds\"))~%"))
    (display
     (format #f "  (define (deposit amount)~%"))
    (display
     (format #f "    (set! balance "))
    (display
     (format #f "(+ balance amount))~%"))
    (display
     (format #f "    balance)~%"))
    (display
     (format #f "  (define (dispatch m)~%"))
    (display
     (format #f "    (cond ((eq? m 'withdraw) "))
    (display
     (format #f "withdraw)~%"))
    (display
     (format #f "          ((eq? m 'deposit) "))
    (display
     (format #f "deposit)~%"))
    (display
     (format #f "          (else (error "))
    (display
     (format #f "\"Unknown request -- MAKE-ACCOUNT\"~%"))
    (display
     (format #f "                       m))))~%"))
    (display
     (format #f "  dispatch)~%"))
    (newline)
    (display
     (format #f "Show the environment structure generated "))
    (display
     (format #f "by the sequence~%"))
    (display
     (format #f "of interactions~%"))
    (newline)
    (display
     (format #f "(define acc (make-account 50))~%"))
    (display
     (format #f "((acc 'deposit) 40)~%"))
    (display
     (format #f "90~%"))
    (display
     (format #f "((acc 'withdraw) 60)~%"))
    (display
     (format #f "30~%"))
    (newline)
    (display
     (format #f "Where is the local state for acc kept? "))
    (display
     (format #f "Suppose we~%"))
    (display
     (format #f "define another account~%"))
    (display
     (format #f "(define acc2 (make-account 100))~%"))
    (display
     (format #f "How are the local states for the "))
    (display
     (format #f "two accounts kept~%"))
    (display
     (format #f "distinct? Which parts of the "))
    (display
     (format #f "environment structure~%"))
    (display
     (format #f "are shared between acc and acc2?~%"))
    (newline)
    (display
     (format #f "Environments:~%"))
    (display
     (format #f "(define acc (make-account 50))~%"))
    (display
     (format #f "E1 { balance=50, withdraw, "))
    (display
     (format #f "deposit, acc=dispatch }~%"))
    (display
     (format #f "  -> global { make-account, acc }~%"))
    (display
     (format #f "((acc 'deposit) 40)~%"))
    (display
     (format #f "E2 { m = 'deposit } -> "))
    (display
     (format #f "E1 { balance, ... }~%"))
    (display
     (format #f "  -> global { ... }~%"))
    (display
     (format #f "(deposit 40) : E3 { amount=40 } "))
    (display
     (format #f "-> E2 { m }~%"))
    (display
     (format #f "  -> E1 { balance, ... } "))
    (display
     (format #f "-> global { ... }~%"))
    (display
     (format #f "90~%"))
    (display
     (format #f "((acc 'withdraw) 60)~%"))
    (display
     (format #f "E4 { m = 'withdraw } "))
    (display
     (format #f " -> E1 { balance, ... }~%"))
    (display
     (format #f "  -> global { ... }~%"))
    (display
     (format #f "(withdraw 60) : E5 { amount=60 } "))
    (display
     (format #f "-> E4 { m }~%"))
    (display
     (format #f "  -> E1 { balance, ... } "))
    (display
     (format #f "-> global { ... }~%"))
    (display
     (format #f "30~%"))
    (newline)
    (display
     (format #f "The local state for acc is kept "))
    (display
     (format #f "in environment labeled E1.~%"))
    (newline)
    (display
     (format #f "(define acc2 (make-account 100))~%"))
    (display
     (format #f "E6 { balance=100, withdraw, "))
    (display
     (format #f "deposit, acc=dispatch }~%"))
    (display
     (format #f "  -> global { make-account, "))
    (display
     (format #f "acc, acc2 }~%"))
    (display
     (format #f "Nothing is shared between acc "))
    (display
     (format #f "and acc2, all locally~%"))
    (display
     (format #f "defined functions are kept in "))
    (display
     (format #f "separate environments.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.11 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
