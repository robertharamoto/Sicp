#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.08                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-func)
  (define local-num 1)
  (begin
    (lambda (anum)
      (begin
        (set! local-num (* local-num anum))
        local-num
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "When we defined the evaluation model "))
    (display
     (format #f "in section 1.1.3,~%"))
    (display
     (format #f "we said that the first step in "))
    (display
     (format #f "evaluating an expression~%"))
    (display
     (format #f "is to evaluate its subexpressions. But "))
    (display
     (format #f "we never specified~%"))
    (display
     (format #f "the order in which the subexpressions "))
    (display
     (format #f "should be evaluated~%"))
    (display
     (format #f "(e.g., left to right or right to left). "))
    (display
     (format #f "When we introduce~%"))
    (display
     (format #f "assignment, the order in which the "))
    (display
     (format #f "arguments to a~%"))
    (display
     (format #f "procedure are evaluated can make a "))
    (display
     (format #f "difference to the~%"))
    (display
     (format #f "result. Define a simple procedure f such "))
    (display
     (format #f "that evaluating~%"))
    (display
     (format #f "(+ (f 0) (f 1)) will return 0 if the "))
    (display
     (format #f "arguments to + are~%"))
    (display
     (format #f "evaluated from left to right but will "))
    (display
     (format #f "return 1 if the~%"))
    (display
     (format #f "arguments are evaluated from right "))
    (display
     (format #f "to left.~%"))
    (newline)
    (display
     (format #f "A simple function with local state:~%"))
    (display
     (format #f "(define (make-func)~%"))
    (display
     (format #f "  (define local-num 1)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (lambda (anum)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (set! local-num "))
    (display
     (format #f "(* local-num anum))~%"))
    (display
     (format #f "        local-num~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display (format #f "right to left:~%"))
    (let ((f (make-func)))
      (begin
        (display
         (format #f "(define f (make-func))~%"))
        (display
         (format #f "(+ (f 0) (f 1)) = ~a~%"
                 (+ (f 0) (f 1))))
        ))
    (let ((f (make-func)))
      (begin
        (display
         (format #f "(define f (make-func))~%"))
        (display
         (format #f "(+ (f 1) (f 0)) = ~a~%"
                 (+ (f 1) (f 0))))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.08 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
