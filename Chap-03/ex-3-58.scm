#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.58                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  last updated August 16, 2022                         ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (display
               (format
                #f "my-stream-map: (stream-car s1) = ~a~%"
                a1))
              (display
               (format
                #f "(stream-car s2) = ~a~%"
                a2))
              (display
               (format
                #f "(+ (car s1) (car s2)) = ~a~%"
                (proc a1 a2)))
              (force-output)

              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         ))
     stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons
   1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons
   1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm)
      (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define fibonacci-stream
  (begin
    (srfi-41:stream-cons
     0 (srfi-41:stream-cons
        1 (add-streams
           (srfi-41:stream-cdr fibonacci-stream)
           fibonacci-stream)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (fib-explode n)
  (begin
    (cond
     ((<= n 0)
      (begin
        0
        ))
     ((= n 1)
      (begin
        1
        ))
     (else
      (begin
        (+ (fib-explode (- n 1))
           (fib-explode (- n 2)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (expand num den radix)
  (begin
    (srfi-41:stream-cons
     (quotient (* num radix) den)
     (expand
      (remainder (* num radix) den) den radix))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Give an interpretation of the stream "))
    (display
     (format #f "computed by the~%"))
    (display
     (format #f "following procedure:~%"))
    (display
     (format #f "(define (expand num den radix)~%"))
    (display
     (format #f "  (cons-stream~%"))
    (display
     (format #f "   (quotient (* num radix) den)~%"))
    (display
     (format #f "   (expand (remainder (* num radix)"))
    (display
     (format #f "den) den radix)))~%"))
    (newline)
    (display
     (format #f "(Quotient is a primitive that returns "))
    (display
     (format #f "the integer quotient~%"))
    (display
     (format #f "of two integers.) What are the successive "))
    (display
     (format #f "elements produced~%"))
    (display
     (format #f "by (expand 1 7 10)? What is "))
    (display
     (format #f "produced by~%"))
    (display
     (format #f "(expand 3 8 10)?~%"))
    (newline)
    (display
     (format #f "The expand stream gives each digit "))
    (display
     (format #f "of the division~%"))
    (display
     (format #f "of num by den, in a given radix. In "))
    (display
     (format #f "the example,~%"))
    (display
     (format #f "1/7 = 0.142857..., and the first number "))
    (display
     (format #f "in the stream~%"))
    (display
     (format #f "corresponds to the first decimal "))
    (display
     (format #f "digit in the~%"))
    (display
     (format #f "decimal expansion.  If the num/den > 1, "))
    (display
     (format #f "then the first~%"))
    (display
     (format #f "number is radix * (everything before and "))
    (display
     (format #f "including the~%"))
    (display
     (format #f "first decimal digit). So (stream-car (expand "))
    (display
     (format #f "10 7 10))~%"))
    (display
     (format #f "will give 14, since 10/7 = 1.42857...~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (display
     (format #f "(expand 1 7 10)~%"))
    (let ((estream (expand 1 7 10))
          (nmax 6))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (display
             (format
              #f "(stream-ref (expand 1 7 10) ~a) = ~a~%"
              ii (my-stream-ref estream ii)))
            (force-output)
            ))
        ))

    (newline)
    (display
     (format #f "(expand 3 8 10)~%"))
    (let ((estream (expand 3 8 10))
          (nmax 6))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nmax))
          (begin
            (display
             (format
              #f "(stream-ref (expand 3 8 10) ~a) = ~a~%"
              ii (my-stream-ref estream ii)))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.58 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
