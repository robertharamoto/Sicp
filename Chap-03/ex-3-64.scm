#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.64                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (average x y)
  (begin
    (* 0.50 (+ x y))
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-improve guess x)
  (begin
    (average guess (/ x guess))
    ))

;;;#############################################################
;;;#############################################################
(define (sqrt-stream x)
  (define guesses
    (begin
      (srfi-41:stream-cons
       1.0
       (srfi-41:stream-map
        (lambda (guess)
          (begin
            (display
             (ice-9-format:format
              #f "debug guess=~8,6f~%" guess))
            (force-output)
            (sqrt-improve guess x)))
        guesses))
      ))
  (begin
    guesses
    ))

;;;#############################################################
;;;#############################################################
(define (stream-limit astream tolerance)
  (define (local-rec astream prev-val tol)
    (begin
      (if (srfi-41:stream-null? astream)
          (begin
            prev-val)
          (begin
            (let ((a0 (srfi-41:stream-car astream))
                  (tail-stream (srfi-41:stream-cdr astream)))
              (let ((diff (abs (- a0 prev-val))))
                (begin
                  (if (< diff tol)
                      (begin
                        a0)
                      (begin
                        (local-rec tail-stream a0 tol)
                        ))
                  )))
            ))
      ))
  (begin
    (local-rec (srfi-41:stream-cdr astream)
               (srfi-41:stream-car astream)
               tolerance)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Write a procedure stream-limit that "))
    (display
     (format #f "takes as arguments~%"))
    (display
     (format #f "a stream and a number (the tolerance). "))
    (display
     (format #f "It should examine~%"))
    (display
     (format #f "the stream until it finds two successive "))
    (display
     (format #f "elements that~%"))
    (display
     (format #f "differ in absolute value by less than "))
    (display
     (format #f "the tolerance, and~%"))
    (display
     (format #f "return the second of the two elements. "))
    (display
     (format #f "Using this, we~%"))
    (display
     (format #f "could compute square roots up to a "))
    (display
     (format #f "given tolerance by:~%"))
    (newline)
    (display
     (format #f "(define (sqrt x tolerance)~%"))
    (display
     (format #f "  (stream-limit (sqrt-stream x) "))
    (display
     (format #f "tolerance))~%"))
    (newline)
    (display
     (format #f "(define (stream-limit astream "))
    (display
     (format #f "tolerance)~%"))
    (display
     (format #f "  (define (local-rec astream "))
    (display
     (format #f "prev-val tol)~%"))
    (display
     (format #f "    (if (srfi-41:stream-null? "))
    (display
     (format #f "astream)~%"))
    (display
     (format #f "        prev-val~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((a0 (srfi-41:stream-car "))
    (display
     (format #f "astream))~%"))
    (display
     (format #f "                (tail-stream "))
    (display
     (format #f "(srfi-41:stream-cdr astream)))~%"))
    (display
     (format #f "            (let ((diff (abs "))
    (display
     (format #f "(- a0 prev-val))))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (< diff tol)~%"))
    (display
     (format #f "                    a0~%"))
    (display
     (format #f "                    (local-rec "))
    (display
     (format #f "tail-stream a0 tol)~%"))
    (display
     (format #f "                    ))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (local-rec~%"))
    (display
     (format #f "      (srfi-41:stream-cdr astream)~%"))
    (display
     (format #f "      (srfi-41:stream-car astream)~%"))
    (display
     (format #f "      tolerance)~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x 2.0)
          (tol-list (list 1e-6 1e-9 1e-12 1e-15)))
      (let ((s-stream (sqrt-stream x)))
        (begin
          (for-each
           (lambda (tol)
             (begin
               (let ((sqrt-val
                      (stream-limit s-stream tol)))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "(stream-limit (sqrt-stream ~a) "
                     x))
                   (display
                    (ice-9-format:format
                     #f "~a) = ~12,9f~%"
                     tol sqrt-val))
                   (force-output)
                   ))
               )) tol-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.64 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
