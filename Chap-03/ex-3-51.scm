#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.51                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  last updated August 16, 2022                         ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define the-empty-stream srfi-41:stream-null)

;;;#############################################################
;;;#############################################################
(define (stream-null? astream)
  (begin
    (srfi-41:stream-null? astream)
    ))

;;;#############################################################
;;;#############################################################
(define (stream-car stream)
  (begin
    (srfi-41:stream-car stream)
    ))

;;;#############################################################
;;;#############################################################
(define (stream-cdr stream)
  (begin
    (srfi-41:stream-cdr stream)
    ))

;;;#############################################################
;;;#############################################################
(define (stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (stream-car s))
        (begin
          (stream-ref (stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-map proc s)
  (begin
    (display
     (format
      #f "debug stream-map (car s) = ~a~%"
      (stream-car s)))
    (force-output)
    (if (stream-null? s)
        (begin
          the-empty-stream)
        (begin
          (srfi-41:stream-cons
           (proc (stream-car s))
           (stream-map proc (stream-cdr s)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-for-each proc s)
  (begin
    (if (stream-null? s)
        (begin
          'done)
        (begin
          (proc (stream-car s))
          (stream-for-each proc (stream-cdr s))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-stream s)
  (begin
    (stream-for-each display-line s)
    ))

;;;#############################################################
;;;#############################################################
(define (display-line x)
  (begin
    (if (not (stream-null? x))
        (begin
          (display x)
          (newline)
          (force-output)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-enumerate-interval low high)
  (begin
    (if (> low high)
        (begin
          the-empty-stream)
        (begin
          (srfi-41:stream-cons
           low
           (stream-enumerate-interval
            (+ low 1) high))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((stream-null? stream)
      (begin
        the-empty-stream
        ))
     ((pred (stream-car stream))
      (begin
        (srfi-41:stream-cons
         (stream-car stream)
         (stream-filter pred
                        (stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (show x)
  (begin
    (if (not (stream-null? x))
        (begin
          (display (format #f "show: ~a~%" x))
          (force-output)
          x)
        (begin
          x
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In order to take a closer look "))
    (display
     (format #f "at delayed evaluation,~%"))
    (display
     (format #f "we will use the following procedure, "))
    (display
     (format #f "which simply returns~%"))
    (display
     (format #f "its argument after printing it:~%"))
    (display
     (format #f "(define (show x)~%"))
    (display
     (format #f "  (display-line x)~%"))
    (display
     (format #f "  x)~%"))
    (display
     (format #f "What does the interpreter print in "))
    (display
     (format #f "response to~%"))
    (display
     (format #f "evaluating each expression in the "))
    (display
     (format #f "following sequence?~%"))
    (display
     (format #f "(define x (stream-map show "))
    (display
     (format #f "(stream-enumerate-interval 0 10)))~%"))
    (display
     (format #f "(stream-ref x 5)~%"))
    (display
     (format #f "(stream-ref x 7)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((x (stream-map
              show (stream-enumerate-interval 0 10))))
      (begin
        (force-output)
        (display
         (format
          #f "(stream-ref x 5) = ~a~%" (stream-ref x 5)))
        (display
         (format
          #f "(stream-ref x 7) = ~a~%" (stream-ref x 7)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.51 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (display
              (format #f "(define x (stream-map show "))
             (display
              (format #f "(stream-enumerate-interval 0 10)))~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%" (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
