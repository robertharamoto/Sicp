#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.02                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-monitored func)
  (define count 0)
  (define (how-many-calls?)
    (begin
      count
      ))
  (define (reset-count)
    (begin
      (set! count 0)
      ))
  (define (dispatch avalue)
    (begin
      (cond
       ((and (symbol? avalue)
             (eq? avalue 'how-many-calls?))
        (begin
          (how-many-calls?)
          ))
       ((and (symbol? avalue)
             (eq? avalue 'reset-count))
        (begin
          (reset-count)
          ))
       (else
        (begin
          (set! count (1+ count))
          (func avalue)
          )))
      ))
  (begin
    dispatch
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In software-testing applications, it "))
    (display
     (format #f "is useful to be~%"))
    (display
     (format #f "able to count the number of times a "))
    (display
     (format #f "given procedure is~%"))
    (display
     (format #f "called during the course of a "))
    (display
     (format #f "computation. Write a~%"))
    (display
     (format #f "procedure make-monitored that takes as "))
    (display
     (format #f "input a procedure,~%"))
    (display
     (format #f "f, that itself takes one input. The "))
    (display
     (format #f "result returned by~%"))
    (display
     (format #f "make-monitored is a third procedure, say "))
    (display
     (format #f "mf, that keeps track~%"))
    (display
     (format #f "of the number of times it has been called "))
    (display
     (format #f "by maintaining an~%"))
    (display
     (format #f "internal counter. If the input to mf is "))
    (display
     (format #f "the special symbol~%"))
    (display
     (format #f "how-many-calls?, then mf returns the "))
    (display
     (format #f "value of the~%"))
    (display
     (format #f "counter. If the input is the special "))
    (display
     (format #f "symbol reset-count,~%"))
    (display
     (format #f "then mf resets the counter to zero. "))
    (display
     (format #f "For any other~%"))
    (display
     (format #f "input, mf returns the result of calling f "))
    (display
     (format #f "on that input~%"))
    (display
     (format #f "and increments the counter. For instance, "))
    (display
     (format #f "we could make~%"))
    (display
     (format #f "a monitored version of the "))
    (display
     (format #f "sqrt procedure:~%"))
    (display
     (format #f "(define s (make-monitored sqrt))~%"))
    (display
     (format #f "(s 100)~%"))
    (display
     (format #f "10~%"))
    (display
     (format #f "(s 'how-many-calls?)~%"))
    (display
     (format #f "1~%"))
    (newline)
    (display
     (format #f "(define (make-monitored func)~%"))
    (display
     (format #f "  (define count 0)~%"))
    (display
     (format #f "  (define (how-many-calls?)~%"))
    (display
     (format #f "    count)~%"))
    (display
     (format #f "  (define (reset-count)~%"))
    (display
     (format #f "    (set! count 0))~%"))
    (display
     (format #f "  (define (dispatch avalue)~%"))
    (display
     (format #f "    (cond~%"))
    (display
     (format #f "     ((and (symbol? avalue)~%"))
    (display
     (format #f "           (eq? avalue "))
    (display
     (format #f "'how-many-calls?))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (how-many-calls?)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     ((and (symbol? avalue)~%"))
    (display
     (format #f "           (eq? avalue 'reset-count))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (reset-count)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     (else~%"))
    (display
     (format #f "      (set! count (1+ count))~%"))
    (display
     (format #f "      (func avalue)~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f "  dispatch)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((ms (make-monitored sqrt)))
      (begin
        (display
         (format #f "(define ms (make-monitored sqrt))~%"))
        (display
         (format #f "(ms 100) = ~a~%" (ms 100)))
        (display
         (format #f "(ms 'how-many-calls?) = ~a~%"
                 (ms 'how-many-calls?)))
        (newline)
        (display
         (format #f "(ms 10000) = ~a~%" (ms 10000)))
        (display
         (format #f "(ms 'how-many-calls?) = ~a~%"
                 (ms 'how-many-calls?)))
        (newline)
        (display
         (format #f "(ms 'reset-count)~%"))
        (ms 'reset-count)
        (display
         (format #f "(ms 'how-many-calls?) = ~a~%"
                 (ms 'how-many-calls?)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.02 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
