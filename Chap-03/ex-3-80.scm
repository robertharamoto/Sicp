#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.80                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones
  (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (integral delayed-integrand initial-value dt)
  (define local-int
    (begin
      (srfi-41:stream-cons
       initial-value
       (let ((integrand (force delayed-integrand)))
         (begin
           (add-streams
            (scale-stream integrand dt)
            local-int)
           )))
      ))
  (begin
    local-int
    ))

;;;#############################################################
;;;#############################################################
(define (solve f y0 dt)
  (define y (integral (delay dy) y0 dt))
  (define dy (srfi-41:stream-map f y))
  (begin
    y
    ))

;;;#############################################################
;;;#############################################################
(define (solve-2nd a b dt y0 dy0)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay d2y) dy0 dt))
  (define d2y
    (add-streams
     (scale-stream dy a)
     (scale-stream y b)))
  (begin
    y
    ))

;;;#############################################################
;;;#############################################################
(define (gen-solve-2nd f dt y0 dy0)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay d2y) dy0 dt))
  (define d2y (srfi-41:stream-map f dy y))
  (begin
    y
    ))

;;;#############################################################
;;;#############################################################
(define (rlc rr ll cc dt)
  (begin
    (define (rlc-cons-stream-func vv-cc-0 ii-ll-0)
      (begin
        (let ((fact-1 (/ 1.0 ll))
              (fact-2 (/ -1.0 cc))
              (fact-3 (* -1.0 (/ rr ll))))
          (begin
            (define vv-cc (integral (delay dv-cc) vv-cc-0 dt))
            (define ii-ll
              (integral (delay di-ll) ii-ll-0 dt))
            (define dv-cc (scale-stream ii-ll fact-2))
            (define di-ll
              (add-streams
               (scale-stream vv-cc fact-1)
               (scale-stream ii-ll fact-3)))
            (cons vv-cc ii-ll)
            ))
        ))
    rlc-cons-stream-func
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A series RLC circuit consists of a "))
    (display
     (format #f "resistor, a capacitor,~%"))
    (display
     (format #f "and an inductor connected in series, "))
    (display
     (format #f "as shown in~%"))
    (display
     (format #f "figure 3.36. If R, L, and C are the "))
    (display
     (format #f "resistance,~%"))
    (display
     (format #f "inductance, and capacitance, then the "))
    (display
     (format #f "relations between~%"))
    (display
     (format #f "voltage (v) and current (i) for the "))
    (display
     (format #f "three components~%"))
    (display
     (format #f "are described by the equations~%"))
    (newline)
    (display
     (format #f "v_R = i_R*R, v_L = "))
    (display
     (format #f "L*di_L/dt, i_C = C*dv_C/dt~%"))
    (newline)
    (display
     (format #f "and the circuit connections dictate "))
    (display
     (format #f "the relations~%"))
    (newline)
    (display
     (format #f "i_R = i_L = -i_C~%"))
    (display
     (format #f "v_C = v_L + v_R~%"))
    (newline)
    (display
     (format #f "Combining these equations shows that the "))
    (display
     (format #f "state of the~%"))
    (display
     (format #f "circuit (summarized by vC, the voltage "))
    (display
     (format #f "across the capacitor,~%"))
    (display
     (format #f "and iL, the current in the inductor) is "))
    (display
     (format #f "described by the~%"))
    (display
     (format #f "pair of differential equations~%"))
    (newline)
    (display
     (format #f "dv_C/dt = -i_L/C~%"))
    (display
     (format #f "di_L/dt = v_C/L - (R/L)*i_L~%"))
    (newline)
    (display
     (format #f "The signal-flow diagram representing this "))
    (display
     (format #f "system of~%"))
    (display
     (format #f "differential equations is shown in "))
    (display
     (format #f "exercise 3.80.~%"))
    (display
     (format #f "https://sicp.sourceacademy.org/chapters/3.5.4.html~%"))
    (display
     (format #f "Write a procedure RLC that takes as "))
    (display
     (format #f "arguments the~%"))
    (display
     (format #f "parameters R, L, and C of the circuit "))
    (display
     (format #f "and the time~%"))
    (display
     (format #f "increment dt. In a manner similar to "))
    (display
     (format #f "that of the RC~%"))
    (display
     (format #f "procedure of exercise 3.73, RLC "))
    (display
     (format #f "should produce a~%"))
    (display
     (format #f "procedure that takes the initial "))
    (display
     (format #f "values of the~%"))
    (display
     (format #f "state variables, vC0 and iL0, and "))
    (display
     (format #f "produces a pair~%"))
    (display
     (format #f "(using cons) of the streams of states "))
    (display
     (format #f "vC and iL.~%"))
    (display
     (format #f "Using RLC, generate the pair of streams "))
    (display
     (format #f "that models~%"))
    (display
     (format #f "the behavior of a series RLC circuit with "))
    (display
     (format #f "R = 1 ohm,~%"))
    (display
     (format #f "C = 0.2 farad, L = 1 henry, dt = 0.1 "))
    (display
     (format #f "second, and~%"))
    (display
     (format #f "initial values iL0 = 0 amps and "))
    (display
     (format #f "vC0 = 10 volts.~%"))
    (newline)
    (display
     (format #f "(define (rlc rr ll cc dt)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (define (rlc-cons-stream-func "))
    (display
     (format #f "vv-cc-0 ii-ll-0)~%"))
    (display
     (format #f "      (let ((fact-1 (/ 1.0 ll))~%"))
    (display
     (format #f "            (fact-2 (/ -1.0 cc))~%"))
    (display
     (format #f "            (fact-3 (* -1.0 (/ rr ll))))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (define vv-cc (integral "))
    (display
     (format #f "(delay dv-cc) vv-cc-0 dt))~%"))
    (display
     (format #f "          (define ii-ll~%"))
    (display
     (format #f "            (integral "))
    (display
     (format #f "(delay di-ll) ii-ll-0 dt))~%"))
    (display
     (format #f "          (define dv-cc "))
    (display
     (format #f "(scale-stream ii-ll fact-2))~%"))
    (display
     (format #f "          (define di-ll~%"))
    (display
     (format #f "            (add-streams~%"))
    (display
     (format #f "             (scale-stream "))
    (display
     (format #f "vv-cc fact-1)~%"))
    (display
     (format #f "             (scale-stream "))
    (display
     (format #f "ii-ll fact-3)))~%"))
    (display
     (format #f "          (cons vv-cc ii-ll)~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "    rlc-cons-stream-func~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((solve-ref-list (list (list 1000 0.0010)))
          (rr 1.0)
          (cc 0.2)
          (ll 1.0)
          (vv-cc-0 10.0)
          (ii-ll-0 0.0)
          (nsteps 10)
          (debug-flag #f))
      (begin
        (display
         (ice-9-format:format
          #f "dv_C/dt = -i_L/C,  di_L/dt = "))
        (display
         (ice-9-format:format
          #f "v_C/L - (R/L)*i_L~%"))
        (display
         (format
          #f "with boundary conditions i_L(0)=0,"))
        (display
         (format
          #f "v_C(0)=10.0.~%"))
        (force-output)
        (for-each
         (lambda (alist)
           (begin
             (let ((anum (list-ref alist 0))
                   (dt (list-ref alist 1)))
               (let ((rlc-cons-func (rlc rr ll cc dt)))
                 (let ((dual-streams
                        (rlc-cons-func vv-cc-0 ii-ll-0)))
                   (let ((vv-stream (car dual-streams))
                         (ii-stream (cdr dual-streams))
                         (tsteps (/ anum nsteps)))
                     (begin
                       (do ((ii 0 (1+ ii)))
                           ((> ii nsteps))
                         (begin
                           (let ((this-tt (* ii tsteps)))
                             (let ((this-time
                                    (/ (truncate (* 10.0 this-tt dt)) 10.0))
                                   (this-vv
                                    (my-stream-ref vv-stream this-tt))
                                   (this-ii
                                    (my-stream-ref ii-stream this-tt)))
                               (begin
                                 (display
                                  (ice-9-format:format
                                   #f "(~:d) t=~a, v_C=~1,3f, "
                                   this-tt this-time this-vv))
                                 (display
                                  (ice-9-format:format
                                   #f "i_L=~1,3f~%" this-ii))
                                 (force-output)
                                 )))
                           ))
                       ))
                   )))
             )) solve-ref-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.80 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (display
              (format #f "solve rc differential equations~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
