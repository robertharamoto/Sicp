#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.81                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (srfi-41:stream-null? s)
        (begin
          #f)
        (begin
          (if (<= n 0)
              (begin
                (srfi-41:stream-car s))
              (begin
                (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map
                proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones
  (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
;;; input stream of symbols, generate or reset
;;; assume generate command has no arguments, reset command takes
;;; argument from the stream
(define (consume-command-stream com-stream)
  (define (com-stream-func a1 a2 astate)
    (begin
      (cond
       ((and (symbol? a1) (eq? a1 'reset) (number? a2))
        (begin
          (let ((new-state (seed->random-state a2)))
            (let ((result (random:uniform new-state)))
              (begin
                (list result new-state)
                )))
          ))
       ((and (symbol? a1) (eq? a1 'generate))
        (begin
          (if (not (equal? astate #f))
              (begin
                (let ((result (random:uniform astate)))
                  (let ((next-state (copy-random-state astate)))
                    (begin
                      (list result next-state)
                      ))
                  ))
              (begin
                (let ((result (random:uniform)))
                  (let ((next-state (copy-random-state)))
                    (begin
                      (list result next-state)
                      )))
                ))
          ))
       (else
        (begin
          #f
          )))
      ))
  (define (local-stream-process s1 s2 astate)
    (begin
      (if (srfi-41:stream-null? s1)
          (begin
            srfi-41:stream-null)
          (begin
            (let ((a1 (srfi-41:stream-car s1))
                  (tail-1 (srfi-41:stream-cdr s1))
                  (a2 (srfi-41:stream-car s2))
                  (tail-2 (srfi-41:stream-cdr s2)))
              (let ((aresult-list
                     (com-stream-func a1 a2 astate)))
                (begin
                  (if (not (equal? aresult-list #f))
                      (begin
                        (let ((anum
                               (list-ref aresult-list 0))
                              (next-state
                               (list-ref aresult-list 1)))
                          (begin
                            (srfi-41:stream-cons
                             anum
                             (local-stream-process
                              tail-1 tail-2 next-state))
                            )))
                      (begin
                        (local-stream-process
                         tail-1 tail-2 astate)
                        ))
                  )))
            ))
      ))
  (begin
    (local-stream-process
     com-stream
     (srfi-41:stream-cdr com-stream) #f)
    ))

;;;#############################################################
;;;#############################################################
(define (make-com-stream n-gen)
  (begin
    (if (<= n-gen 0)
        (begin
          srfi-41:stream-null)
        (begin
          (srfi-41:stream-cons
           'generate
           (make-com-stream (1- n-gen)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Exercise 3.6 discussed generalizing the "))
    (display
     (format #f "random-number~%"))
    (display
     (format #f "generator to allow one to reset the "))
    (display
     (format #f "random-number sequence~%"))
    (display
     (format #f "so as to produce repeatable sequences "))
    (display
     (format #f "of \"random\" numbers.~%"))
    (display
     (format #f "Produce a stream formulation of this "))
    (display
     (format #f "same generator~%"))
    (display
     (format #f "that operates on an input stream of "))
    (display
     (format #f "requests to generate~%"))
    (display
     (format #f "a new random number or to reset the "))
    (display
     (format #f "sequence to a~%"))
    (display
     (format #f "specified value and that produces the "))
    (display
     (format #f "desired stream~%"))
    (display
     (format #f "of random numbers. Don't use assignment "))
    (display
     (format #f "in your solution.~%"))
    (newline)
    (display
     (format #f "(define (consume-command-stream "))
    (display
     (format #f "com-stream)~%"))
    (display
     (format #f "  (define (com-stream-func "))
    (display
     (format #f "a1 a2 astate)~%"))
    (display
     (format #f "    (cond~%"))
    (display
     (format #f "     ((and (symbol? a1) "))
    (display
     (format #f "(eq? a1 'reset) (number? a2))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (let ((new-state "))
    (display
     (format #f "(seed->random-state a2)))~%"))
    (display
     (format #f "          (let ((result "))
    (display
     (format #f "(random:uniform new-state)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (list "))
    (display
     (format #f "result new-state)~%"))
    (display
     (format #f "              )))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     ((and (symbol? a1) "))
    (display
     (format #f "(eq? a1 'generate))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (if (not "))
    (display
     (format #f "(equal? astate #f))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((result "))
    (display
     (format #f "(random:uniform astate)))~%"))
    (display
     (format #f "                (let ((next-state "))
    (display
     (format #f "(copy-random-state astate)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (list "))
    (display
     (format #f "result next-state)~%"))
    (display
     (format #f "                    ))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((result "))
    (display
     (format #f "(random:uniform)))~%"))
    (display
     (format #f "                (let ((next-state "))
    (display
     (format #f "(copy-random-state)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (list "))
    (display
     (format #f "result next-state)~%"))
    (display
     (format #f "                    )))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     (else~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        #f~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     ))~%"))
    (display
     (format #f "  (define (local-stream-process "))
    (display
     (format #f "s1 s2 astate)~%"))
    (display
     (format #f "    (if (srfi-41:stream-null? s1)~%"))
    (display
     (format #f "        srfi-41:stream-null~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((a1 "))
    (display
     (format #f "(srfi-41:stream-car s1))~%"))
    (display
     (format #f "                (tail-1 "))
    (display
     (format #f "(srfi-41:stream-cdr s1))~%"))
    (display
     (format #f "                (a2 "))
    (display
     (format #f "(srfi-41:stream-car s2))~%"))
    (display
     (format #f "                (tail-2 "))
    (display
     (format #f "(srfi-41:stream-cdr s2)))~%"))
    (display
     (format #f "            (let ((aresult-list "))
    (display
     (format #f "(com-stream-func a1 a2 astate)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (not "))
    (display
     (format #f "(equal? aresult-list #f))~%"))
    (display
     (format #f "                    (let ((anum "))
    (display
     (format #f "(list-ref aresult-list 0))~%"))
    (display
     (format #f "                          (next-state "))
    (display
     (format #f "(list-ref aresult-list 1)))~%"))
    (display
     (format #f "                      (begin~%"))
    (display
     (format #f "                        "))
    (display
     (format #f "(srfi-41:stream-cons~%"))
    (display
     (format #f "                         anum~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(local-stream-process~%"))
    (display
     (format #f "    tail-1 tail-2 next-state))~%"))
    (display
     (format #f "                        ))~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "(local-stream-process~%"))
    (display
     (format #f "    tail-1 tail-2 astate))~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (local-stream-process~%"))
    (display
     (format #f "   com-stream "))
    (display
     (format #f "(srfi-41:stream-cdr com-stream) #f))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((n1 5)
          (n2 5)
          (rr-0 100))
      (let ((ll-1 (make-com-stream n1))
            (ll-2 (make-com-stream n2))
            (ntotal (+ n1 n2)))
        (let ((stream-1
               (srfi-41:stream-cons
                'reset (srfi-41:stream-cons rr-0 ll-1)))
              (stream-2
               (srfi-41:stream-cons
                'reset (srfi-41:stream-cons rr-0 ll-2))))
          (let ((total-stream
                 (srfi-41:stream-append
                  stream-1 stream-2
                  (srfi-41:stream-cons
                   srfi-41:stream-null srfi-41:stream-null)
                  )))
            (let ((rand-stream
                   (consume-command-stream total-stream)))
              (begin
                (display
                 (ice-9-format:format
                  #f "generate random numbers using a "))
                (display
                 (ice-9-format:format
                  #f "stream of commands~%"))
                (display
                 (format
                  #f "a reset ~a command, ~a generate "
                  rr-0 n1))
                (display
                 (format
                  #f "commands, followed by~%"))
                (display
                 (format
                  #f "another reset ~a comand, followed by ~a "
                  rr-0 n2))
                (display
                 (format
                  #f "generate commands.~%"))
                (display
                 (format
                  #f "state is the same each time, so the "))
                (display
                 (format
                  #f "two sets of random~%"))
                (display
                 (format
                  #f "numbers should be the same.~%"))
                (force-output)
                (do ((ii 0 (1+ ii)))
                    ((> ii n1))
                  (begin
                    (let ((anum (my-stream-ref rand-stream ii)))
                      (begin
                        (display
                         (ice-9-format:format
                          #f "(first ~:d) rand = ~1,4f~%"
                          ii anum))
                        (force-output)
                        ))
                    ))
                (do ((ii (1+ n1) (1+ ii)))
                    ((> ii (+ n1 n2 1)))
                  (begin
                    (let ((anum (my-stream-ref rand-stream ii)))
                      (begin
                        (display
                         (ice-9-format:format
                          #f "(second ~:d) rand = ~1,4f~%"
                          ii anum))
                        (force-output)
                        ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.81 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display
              (format #f "scheme test~%"))
             (display
              (format #f "random numbers from a command stream~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
