#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.21                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (front-ptr queue)
  (begin
    (car queue)
    ))

;;;#############################################################
;;;#############################################################
(define (rear-ptr queue)
  (begin
    (cdr queue)
    ))

;;;#############################################################
;;;#############################################################
(define (set-front-ptr! queue item)
  (begin
    (set-car! queue item)
    ))

;;;#############################################################
;;;#############################################################
(define (set-rear-ptr! queue item)
  (begin
    (set-cdr! queue item)
    ))

;;;#############################################################
;;;#############################################################
(define (empty-queue? queue)
  (begin
    (null? (front-ptr queue))
    ))

;;;#############################################################
;;;#############################################################
(define (make-queue)
  (begin
    (cons (list) (list))
    ))

;;;#############################################################
;;;#############################################################
(define (front-queue queue)
  (begin
    (if (empty-queue? queue)
        (begin
          (error "FRONT called with an empty queue" queue))
        (begin
          (car (front-ptr queue))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (insert-queue! queue item)
  (begin
    (let ((new-pair (cons item (list))))
      (begin
        (cond
         ((empty-queue? queue)
          (begin
            (set-front-ptr! queue new-pair)
            (set-rear-ptr! queue new-pair)
            queue
            ))
         (else
          (begin
            (set-cdr! (rear-ptr queue) new-pair)
            (set-rear-ptr! queue new-pair)
            queue
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (delete-queue! queue)
  (begin
    (cond
     ((empty-queue? queue)
      (begin
        (error "DELETE! called with an empty queue" queue)
        ))
     (else
      (begin
        (set-front-ptr! queue (cdr (front-ptr queue)))
        queue
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (print-queue q1)
  (begin
    (let ((q-data (car q1)))
      (begin
        (display (format #f "~a~%" q-data))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle decides to test the "))
    (display
     (format #f "queue implementation~%"))
    (display
     (format #f "described above. He types in the "))
    (display
     (format #f "procedures to the Lisp~%"))
    (display
     (format #f "interpreter and proceeds to "))
    (display
     (format #f "try them out:~%"))
    (display
     (format #f "(define q1 (make-queue))~%"))
    (display
     (format #f "(insert-queue! q1 'a)~%"))
    (display
     (format #f "((a) a)~%"))
    (display
     (format #f "(insert-queue! q1 'b)~%"))
    (display
     (format #f "((a b) b)~%"))
    (display
     (format #f "(delete-queue! q1)~%"))
    (display
     (format #f "((b) b)~%"))
    (display
     (format #f "(delete-queue! q1)~%"))
    (display
     (format #f "(() b)~%"))
    (display
     (format #f "\"It's all wrong!\" he complains. "))
    (display
     (format #f "\"The interpreter's~%"))
    (display
     (format #f "response shows that the last item is "))
    (display
     (format #f "inserted into the~%"))
    (display
     (format #f "queue twice. And when I delete both items, "))
    (display
     (format #f "the second b~%"))
    (display
     (format #f "is still there, so the queue isn't empty, "))
    (display
     (format #f "even though it's~%"))
    (display
     (format #f "supposed to be.\" Eva Lu Ator suggests "))
    (display
     (format #f "that Ben has~%"))
    (display
     (format #f "misunderstood what is happening. \"It's "))
    (display
     (format #f "not that the~%"))
    (display
     (format #f "items are going into the queue twice,\" "))
    (display
     (format #f "she explains. \"It's~%"))
    (display
     (format #f "just that the standard Lisp printer doesn't "))
    (display
     (format #f "know how to make~%"))
    (display
     (format #f "sense of the queue representation. If you "))
    (display
     (format #f "want to see the~%"))
    (display
     (format #f "queue printed correctly, you'll have to "))
    (display
     (format #f "define your own~%"))
    (display
     (format #f "print procedure for queues.\" Explain "))
    (display
     (format #f "what Eva Lu is~%"))
    (display
     (format #f "talking about. In particular, show why "))
    (display
     (format #f "Ben's examples~%"))
    (display
     (format #f "produce the printed results that they do. "))
    (display
     (format #f "Define a~%"))
    (display
     (format #f "procedure print-queue that takes a queue "))
    (display
     (format #f "as input and~%"))
    (display
     (format #f "prints the sequence of items in the "))
    (display
     (format #f "queue.~%"))
    (newline)
    (display
     (format #f "Eva Lu is talking about how the "))
    (display
     (format #f "interpreter evaluates~%"))
    (display
     (format #f "lists. Lists are expected to be of "))
    (display
     (format #f "the form:~%"))
    (display
     (format #f "x -> [ 'a, --]-->[ 'b, / ], "))
    (display
     (format #f "where '/' represents~%"))
    (display
     (format #f "the null object. The queue object is "))
    (display
     (format #f "a pair which~%"))
    (display
     (format #f "the interpreter assumes is of the "))
    (display
     (format #f "same form. So~%"))
    (display
     (format #f "q1 -> [ f-ptr, r-ptr ]~%"))
    (display
     (format #f "and f-ptr -> [ 'a, --]-->[ 'b, / ],~%"))
    (display
     (format #f "and r-ptr -> [ 'b, / ]~%"))
    (display
     (format #f "The interpreter then assumes the pair "))
    (display
     (format #f "is a cons of a~%"))
    (display
     (format #f "list with another list, and prints "))
    (display
     (format #f "(('a 'b) 'b),~%"))
    (display
     (format #f "the first element is the queue data, "))
    (display
     (format #f "the last rear~%"))
    (display
     (format #f "pointer is taken to mean the "))
    (display
     (format #f "continuation of~%"))
    (display
     (format #f "the list.~%"))
    (newline)
    (display
     (format #f "(define q1 (make-queue))~%"))
    (display
     (format #f "(insert-queue! q1 'a)~%"))
    (display
     (format #f "((a) a)~%"))
    (display
     (format #f "Initially, the make-queue function is "))
    (display
     (format #f "called and~%"))
    (display
     (format #f "constructs an empty queue, (cons (list) "))
    (display
     (format #f "(list)). Next~%"))
    (display
     (format #f "the insert-queue! function is called, "))
    (display
     (format #f "and a is inserted,~%"))
    (display
     (format #f "and environment is created, and new-pair "))
    (display
     (format #f "is formed, where~%"))
    (display
     (format #f "new-pair=(cons 'a (list)). Then the "))
    (display
     (format #f "queue object is~%"))
    (display
     (format #f "modified, the first and second element "))
    (display
     (format #f "of the pair is~%"))
    (display
     (format #f "set to new-pair.  Since q1 is returned, "))
    (display
     (format #f "and q1 equals:~%"))
    (display
     (format #f "q1 -> [ f-ptr, r-ptr ],~%"))
    (display
     (format #f "f-ptr -> [ 'a, / ],~%"))
    (display
     (format #f "r-ptr -> [ 'a, / ].~%"))
    (display
     (format #f "the interpreter prints out (('a) 'a) "))
    (display
     (format #f "(which is the cons~%"))
    (display
     (format #f "of (cons 'a (list)) with itself.~%"))
    (newline)
    (display
     (format #f "(insert-queue! q1 'b)~%"))
    (display
     (format #f "((a b) b)~%"))
    (display
     (format #f "another environment is created with "))
    (display
     (format #f "another new-pair~%"))
    (display
     (format #f " = (cons 'b (list)), and sets the "))
    (display
     (format #f "rear-pointer and~%"))
    (display
     (format #f "the cdr of (cons 'a (list)) to point "))
    (display
     (format #f "to the new~%"))
    (display
     (format #f "pair. The queue becomes:~%"))
    (display
     (format #f "q1 -> [ f-ptr, r-ptr ],~%"))
    (display
     (format #f "f-ptr -> [ 'a, --]-->[ 'b, / ],~%"))
    (display
     (format #f "r-ptr -> [ 'b, / ].~%"))
    (display
     (format #f "the interpreter prints out (('a 'b) 'b) "))
    (display
     (format #f "(which is the~%"))
    (display
     (format #f "cons of (cons 'a (cons 'b (list))) with "))
    (display
     (format #f "(cons 'b (list)).~%"))
    (newline)
    (display
     (format #f "(delete-queue! q1)~%"))
    (display
     (format #f "((b) b)~%"))
    (display
     (format #f "since delete-queue! only deletes from "))
    (display
     (format #f "the front, the~%"))
    (display
     (format #f "rear-pointer is not changed. This means "))
    (display
     (format #f "that the front~%"))
    (display
     (format #f "pointer is set to the cdr of the first "))
    (display
     (format #f "element, so:~%"))
    (display
     (format #f "q1 -> [ f-ptr, r-ptr ],~%"))
    (display
     (format #f "f-ptr -> [ 'b, / ],~%"))
    (display
     (format #f "r-ptr -> [ 'b, / ].~%"))
    (display
     (format #f "the interpreter prints out (('b) 'b) "))
    (display
     (format #f "(which is the~%"))
    (display
     (format #f "cons of (cons 'b (list)) with "))
    (display
     (format #f "(cons 'b (list)).~%"))
    (newline)
    (display
     (format #f "(delete-queue! q1)~%"))
    (display
     (format #f "(() b)~%"))
    (display
     (format #f "only front-pointer is changed, and "))
    (display
     (format #f "queue becomes:~%"))
    (display
     (format #f "q1 -> [ f-ptr, r-ptr ],~%"))
    (display
     (format #f "f-ptr -> (list),~%"))
    (display
     (format #f "r-ptr -> [ 'b, / ].~%"))
    (display
     (format #f "the interpreter prints out ( (list) 'b) "))
    (display
     (format #f "(which is~%"))
    (display
     (format #f "the cons of (list) with (cons "))
    (display
     (format #f "'b (list)).~%"))
    (newline)
    (display
     (format #f "(define (print-queue q1)~%"))
    (display
     (format #f "  (let ((q-data (car q1)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (ice-9-format:format
      #f "      (display (format #f "))
    (display
     (format #f "#\"~~a~~%#\" q-data))~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((q1 (make-queue)))
      (begin
        (display
         (format #f "(define q1 (make-queue))~%"))
        (insert-queue! q1 'a)
        (display
         (format #f "(insert-queue! q1 'a) -> "))
        (print-queue q1)

        (insert-queue! q1 'b)
        (display
         (format #f "(insert-queue! q1 'b) -> "))
        (print-queue q1)

        (delete-queue! q1)
        (display
         (format #f "(delete-queue! q1) -> "))
        (print-queue q1)

        (delete-queue! q1)
        (display
         (format #f "(delete-queue! q1) -> "))
        (print-queue q1)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.21 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
