#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.61                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons
   1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons
   1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm)
      (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
;;; turns a stream into another stream s0, (s0 s1),
;;; (s0 s1 s2), (s0 s1 s2 s3), ...
(define (partial-cons strm)
  (begin
    (let ((s0 (srfi-41:stream-car strm))
          (stail (srfi-41:stream-cdr strm)))
      (begin
        (srfi-41:stream-cons
         (list s0)
         (my-stream-map
          (lambda (x y) (cons x y))
          stail (partial-cons strm)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mult-then-add-lists a1-list a2-list)
  (begin
    (let ((llen (min (length a1-list) (length a2-list)))
          (result 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii llen))
          (begin
            (let ((t1 (list-ref a1-list ii))
                  (t2 (list-ref a2-list ii)))
              (begin
                (set! result (+ result (* t1 t2)))
                ))
            ))
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (integrate-series astream)
  (begin
    (srfi-41:stream-map / astream integers)
    ))

;;;#############################################################
;;;#############################################################
(define cos-stream
  (srfi-41:stream-cons
   1 (scale-stream
      (integrate-series sin-stream) -1)))

;;;#############################################################
;;;#############################################################
(define sin-stream
  (srfi-41:stream-cons
   0 (integrate-series cos-stream)))

;;;#############################################################
;;;#############################################################
(define (original-mul-series s1 s2)
  (begin
    (let ((par-sum-1 (partial-cons s1))
          (par-sum-2 (partial-cons s2)))
      (begin
        (my-stream-map
         (lambda (s1 s2)
           (begin
             (let ((result
                    (mult-then-add-lists s1 (reverse s2))))
               (begin
                 result
                 ))
             ))
         par-sum-1 par-sum-2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mul-series s1 s2)
  (begin
    (srfi-41:stream-cons
     (* (srfi-41:stream-car s1)
        (srfi-41:stream-car s2))
     (add-streams
      (scale-stream
       (srfi-41:stream-cdr s2)
       (srfi-41:stream-car s1))
      (mul-series
       s2 (srfi-41:stream-cdr s1))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (invert-unit-series strm)
  (begin
    (let ((first-term
           (srfi-41:stream-car strm))
          (msr
           (scale-stream
            (srfi-41:stream-cdr strm) -1)))
      (begin
        (if (= first-term 0)
            (begin
              (display
               (format
                #f "error unable to compute the inverse~%"))
              (display
               (format
                #f "with first-term = ~a~%"
                first-term))
              (force-output)
              (quit))
            (begin
              (srfi-41:stream-cons
               1
               (mul-series msr (invert-unit-series strm)))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let S be a power series (exercise 3.59) "))
    (display
     (format #f "whose constant~%"))
    (display
     (format #f "term is 1. Suppose we want to find "))
    (display
     (format #f "power series 1/S,~%"))
    (display
     (format #f "that is, the series X such that "))
    (display
     (format #f "such that S · X = 1.~%"))
    (display
     (format #f "Write S = 1 + SR where SR is the part "))
    (display
     (format #f "of S after the~%"))
    (display
     (format #f "constant term. Then we can solve for X "))
    (display
     (format #f "as follows:~%"))
    (newline)
    (display
     (format #f "       S * X = 1~%"))
    (display
     (format #f "(1 + Sr) * X = 1~%"))
    (display
     (format #f "  X + Sr * X = 1~%"))
    (display
     (format #f "           X = 1 - Sr * X~%"))
    (newline)
    (display
     (format #f "In other words, X is the power series "))
    (display
     (format #f "whose constant term~%"))
    (display
     (format #f "is 1 and whose higher-order terms are "))
    (display
     (format #f "given by the negative~%"))
    (display
     (format #f "of SR times X. Use this idea to write "))
    (display
     (format #f "a procedure~%"))
    (display
     (format #f "invert-unit-series that computes 1/S "))
    (display
     (format #f "for a power series~%"))
    (display
     (format #f "S with constant term 1. You will need "))
    (display
     (format #f "to use mul-series~%"))
    (display
     (format #f "from exercise 3.60.~%"))
    (newline)
    (display
     (format #f "(define (invert-unit-series strm)~%"))
    (display
     (format #f "  (let ((first-term "))
    (display
     (format #f "(srfi-41:stream-car strm))~%"))
    (display
     (format #f "        (msr (scale-stream "))
    (display
     (format #f "(srfi-41:stream-cdr strm) -1)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (if (= first-term 0)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (display~%"))
    (display
     (format #f "              (format #f \"error unable "))
    (display
     (format #f "to compute the~%"))
    (display
     (format #f "  inverse with first-term = ~~a~~%\"~%"))
    (display
     (format #f "                            "))
    (display
     (format #f "first-term))~%"))
    (display
     (format #f "            (force-output)~%"))
    (display
     (format #f "            (quit))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (srfi-41:stream-cons~%"))
    (display
     (format #f "             1~%"))
    (display
     (format #f "             (mul-series msr "))
    (display
     (format #f "(invert-unit-series strm)))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((c-stream cos-stream)
          (inv-c-stream (invert-unit-series cos-stream))
          (s-stream sin-stream)
          (nmax 11))
      (let ((c-inv-c-stream (mul-series c-stream inv-c-stream)))
        (begin
          (display
           (format #f "(stream-ref cos-stream)~%"))
          (do ((ii 0 (1+ ii)))
              ((>= ii nmax))
            (begin
              (display
               (format
                #f "(stream-ref cos-stream ~a) = ~a~%"
                ii (my-stream-ref c-stream ii)))
              (display
               (format
                #f "    (stream-ref "))
              (display
               (format
                #f "cos-inv-c-stream ~a) = ~a~%"
                ii (my-stream-ref c-inv-c-stream ii)))
              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.61 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
