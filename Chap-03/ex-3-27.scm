#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.27                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (fib n)
  (begin
    (cond
     ((= n 0)
      (begin
        0
        ))
     ((= n 1)
      (begin
        1
        ))
     (else
      (begin
        (+ (fib (- n 1))
           (fib (- n 2)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fib-1 result-hash-table)
 (begin
   (let ((sub-name "test-fib-1")
         (test-list
          (list
           (list 0 0)
           (list 1 1)
           (list 2 1)
           (list 3 2)
           (list 4 3)
           (list 5 5)
           (list 6 8)
           (list 7 13)
           (list 8 21)
           (list 9 34)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((anum (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (fib anum)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : anum=~a, "
                        sub-name test-label-index
                        anum))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (my-fib n)
  (define (local-iter count max fn fnm1)
    (begin
      (if (>= count max)
          (begin
            fn)
          (begin
            (local-iter
             (1+ count) max (+ fn fnm1) fn)
            ))
      ))
  (begin
    (cond
     ((= n 0)
      (begin
        0
        ))
     ((= n 1)
      (begin
        1
        ))
     (else
      (begin
        (local-iter 1 n 1 0)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-my-fib-1 result-hash-table)
 (begin
   (let ((sub-name "test-my-fib-1")
         (test-list
          (list
           (list 0 0)
           (list 1 1)
           (list 2 1)
           (list 3 2)
           (list 4 3)
           (list 5 5)
           (list 6 8)
           (list 7 13)
           (list 8 21)
           (list 9 34)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((anum (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (my-fib anum)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : anum=~a, "
                        sub-name test-label-index
                        anum))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (memoize f)
  (begin
    (let ((htable (make-hash-table)))
      (begin
        (lambda (x)
          (begin
            (let ((previously-computed-result
                   (hash-ref htable x #f)))
              (begin
                (or previously-computed-result
                    (let ((result (f x)))
                      (begin
                        (hash-set! htable x result)
                        result
                        )))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define memo-fib
  (begin
    (memoize
     (lambda (n)
       (begin
         (cond
          ((= n 0)
           (begin
             0
             ))
          ((= n 1)
           (begin
             1
             ))
          (else
           (begin
             (+ (memo-fib (- n 1))
                (memo-fib (- n 2)))
             )))
         )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-memo-fib-1 result-hash-table)
 (begin
   (let ((sub-name "test-memo-fib-1")
         (test-list
          (list
           (list 0 0)
           (list 1 1)
           (list 2 1)
           (list 3 2)
           (list 4 3)
           (list 5 5)
           (list 6 8)
           (list 7 13)
           (list 8 21)
           (list 9 34)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((anum (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (memo-fib anum)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : anum=~a, "
                        sub-name test-label-index
                        anum))
                      (err-msg-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-1 nn)
  (define (ttest fname-str func)
    (begin
      (timer-module:time-code-macro
       (begin
         (let ((fvalue (func nn)))
           (begin
             (display
              (ice-9-format:format
               #f "(~a ~:d) = ~:d~%"
               fname-str nn fvalue))
             ))
         ))
      (newline)
      (force-output)
      ))
  (begin
    (display
     (format #f "fibonacci timing~%"))

    (display
     (format #f "recursive fibonacci~%"))
    (ttest "fib" fib)

    (display
     (format #f "memoized recursive fibonacci~%"))
    (ttest "memo-fib" memo-fib)

    (display
     (format #f "fast fibonacci~%"))
    (ttest "my-fib" my-fib)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Memoization (also called tabulation) is "))
    (display
     (format #f "a technique that~%"))
    (display
     (format #f "enables a procedure to record, in a "))
    (display
     (format #f "local table, values~%"))
    (display
     (format #f "that have previously been computed. "))
    (display
     (format #f "This technique can~%"))
    (display
     (format #f "make a vast difference in the "))
    (display
     (format #f "performance of a program.~%"))
    (display
     (format #f "A memoized procedure maintains a table "))
    (display
     (format #f "in which values~%"))
    (display
     (format #f "of previous calls are stored using as "))
    (display
     (format #f "keys the arguments~%"))
    (display
     (format #f "that produced the values. When the "))
    (display
     (format #f "memoized procedure~%"))
    (display
     (format #f "is asked to compute a value, it first "))
    (display
     (format #f "checks the table~%"))
    (display
     (format #f "to see if the value is already there "))
    (display
     (format #f "and, if so,~%"))
    (display
     (format #f "just returns that value. Otherwise, it "))
    (display
     (format #f "computes the new~%"))
    (display
     (format #f "value in the ordinary way and stores "))
    (display
     (format #f "this in the table.~%"))
    (display
     (format #f "As an example of memoization, recall "))
    (display
     (format #f "from section 1.2.2~%"))
    (display
     (format #f "the exponential process for computing "))
    (display
     (format #f "Fibonacci numbers:~%"))
    (newline)
    (display
     (format #f "(define (fib n)~%"))
    (display
     (format #f "  (cond ((= n 0) 0)~%"))
    (display
     (format #f "        ((= n 1) 1)~%"))
    (display
     (format #f "        (else (+ (fib (- n 1))~%"))
    (display
     (format #f "                 (fib (- n 2))))))~%"))
    (display
     (format #f "The memoized version of the same "))
    (display
     (format #f "procedure is:~%"))
    (display
     (format #f "(define memo-fib~%"))
    (display
     (format #f "  (memoize (lambda (n)~%"))
    (display
     (format #f "             (cond ((= n 0) 0)~%"))
    (display
     (format #f "                   ((= n 1) 1)~%"))
    (display
     (format #f "                   (else (+ (memo-fib "))
    (display
     (format #f "(- n 1))~%"))
    (display
     (format #f "                            (memo-fib "))
    (display
     (format #f "(- n 2))))))))~%"))
    (display
     (format #f "where the memoizer is defined as:~%"))
    (display
     (format #f "(define (memoize f)~%"))
    (display
     (format #f "  (let ((table (make-table)))~%"))
    (display
     (format #f "    (lambda (x)~%"))
    (display
     (format #f "      (let ((previously-computed-result "))
    (display
     (format #f "(lookup x table)))~%"))
    (display
     (format #f "        (or previously-computed-result~%"))
    (display
     (format #f "            (let ((result (f x)))~%"))
    (display
     (format #f "              (insert! x result table)~%"))
    (display
     (format #f "              result))))))~%"))
    (display
     (format #f "Draw an environment diagram to analyze "))
    (display
     (format #f "the computation of~%"))
    (display
     (format #f "(memo-fib 3). Explain why memo-fib "))
    (display
     (format #f "computes the nth~%"))
    (display
     (format #f "Fibonacci number in a number of steps "))
    (display
     (format #f "proportional to n.~%"))
    (display
     (format #f "Would the scheme still work if we had "))
    (display
     (format #f "simply defined~%"))
    (display
     (format #f "memo-fib to be (memoize fib)?~%"))
    (newline)
    (display
     (format #f "(memo-fib 3)~%"))
    (display
     (format #f "  (memoize f) :~%"))
    (display
     (format #f "    E1 -> "))
    (display
     (format #f "{ table=(make-table), f=(lambda n) }~%"))
    (display
     (format #f "    (lambda x)...) :~%"))
    (display
     (format #f "    E2 -> "))
    (display
     (format #f "{ x=3, previously-computed-result=#f, "))
    (display
     (format #f "result=(f 3) } ~%"))
    (display
     (format #f "      (+ (memo-fib 2) (memo-fib 1)) :~%"))
    (display
     (format #f "    E3 -> { n=2 }, E4 -> { n=1 }~%"))
    (display
     (format #f "      (+ (lambda 2) (lambda 1)) :~%"))
    (display
     (format #f "    E5 -> { x=2, "))
    (display
     (format #f "previously-computed-result=#f, "))
    (display
     (format #f "result=(f 2) },~%"))
    (display
     (format #f "    E6 -> { x=1, result=(f 1)=1 }~%"))
    (display
     (format #f "      (+ (+ (memo-fib 1) "))
    (display
     (format #f "(memo-fib 0)) 1)~%"))
    (display
     (format #f "      (+ (+ (lambda 1) (lambda 0)) 1):~%"))
    (display
     (format #f "    E7 -> { x=1, previously-computed-result=#t, "))
    (display
     (format #f "result=1 },~%"))
    (display
     (format #f "    E7 : { x=0, result=0 }~%"))
    (display
     (format #f "      (+ 1 1) -> 2~%"))
    (newline)
    (display
     (format #f "Even though the algorithm constantly "))
    (display
     (format #f "evaluates individual~%"))
    (display
     (format #f "values of n-1, n-2, ..., once a value "))
    (display
     (format #f "is computed, stored~%"))
    (display
     (format #f "values can be looked up for later "))
    (display
     (format #f "computations, so the~%"))
    (display
     (format #f "number of calculations is only of "))
    (display
     (format #f "order n.~%"))
    (newline)
    (display
     (format #f "The look-up feature would not work the "))
    (display
     (format #f "same for~%"))
    (display
     (format #f "(memoize fib). Consider trying to "))
    (display
     (format #f "compute~%"))
    (display
     (format #f "((memoize fib) n). Then since n would "))
    (display
     (format #f "not be in the~%"))
    (display
     (format #f "look-up table, it would be calculated "))
    (display
     (format #f "by fib and the~%"))
    (display
     (format #f "results stored in the table. "))
    (display
     (format #f "Unfortunately this results~%"))
    (display
     (format #f "in no savings in work, as fib doesn't "))
    (display
     (format #f "consult the~%"))
    (display
     (format #f "look-up table during each step in the "))
    (display
     (format #f "recursion.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (scheme-test-1 20)
    (newline)
    (scheme-test-1 35)

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.27 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
