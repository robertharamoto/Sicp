#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.40                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Give all possible values of x that can "))
    (display
     (format #f "result from executing:~%"))
    (newline)
    (display
     (format #f "(define x 10)~%"))
    (display
     (format #f "(parallel-execute~%"))
    (display
     (format #f "    (lambda () (set! x (* x x)))~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(lambda () (set! x (* x x x))))~%"))
    (newline)
    (display
     (format #f "Which of these possibilities remain if "))
    (display
     (format #f "we instead use~%"))
    (display
     (format #f "serialized procedures:~%"))
    (newline)
    (display
     (format #f "(define x 10)~%"))
    (display
     (format #f "(define s (make-serializer))~%"))
    (display
     (format #f "(parallel-execute "))
    (display
     (format #f "(s (lambda () (set! x (* x x))))~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(s (lambda () (set! x (* x x x)))))~%"))
    (newline)
    (display
     (format #f "The seven possibilities are:~%"))
    (display
     (format #f "1,000,000 : P1 sets x to 100, then P2 "))
    (display
     (format #f "sets x to 10^6~%"))
    (display
     (format #f "1,000,000 : P2 sets x to 1000, then P1 "))
    (display
     (format #f "sets x to 10^6~%"))
    (display
     (format #f "10,000 : P2 sets x to 1000 between the "))
    (display
     (format #f "the times P1~%"))
    (display
     (format #f "accesses the first and second value "))
    (display
     (format #f "of x~%"))
    (display
     (format #f "1000 : P2 sets x to 1000 after P1 accesses "))
    (display
     (format #f "the second~%"))
    (display
     (format #f "value of x~%"))
    (display
     (format #f "100,000 : P1 sets x to 100 between the "))
    (display
     (format #f "first time P2~%"))
    (display
     (format #f "accesses x and the second~%"))
    (display
     (format #f "10,000 : P1 sets x to 100 between the "))
    (display
     (format #f "second time P2~%"))
    (display
     (format #f "accesses x and the third time~%"))
    (display
     (format #f "1000 : P1 sets x to 100 after P2 "))
    (display
     (format #f "accesses x~%"))
    (display
     (format #f "for the third time.~%"))
    (newline)
    (display
     (format #f "With the serialized procedures, each "))
    (display
     (format #f "time the value is~%"))
    (display
     (format #f "computed as a single atomic transaction. "))
    (display
     (format #f "So any~%"))
    (display
     (format #f "possibilities where the x value is "))
    (display
     (format #f "changed between~%"))
    (display
     (format #f "accesses of x are not possible. The only "))
    (display
     (format #f "two remaining~%"))
    (display
     (format #f "possibilities are:~%"))
    (display
     (format #f "1,000,000 : P1 sets x to 100, then P2 "))
    (display
     (format #f "sets x to 10^6~%"))
    (display
     (format #f "1,000,000 : P2 sets x to 1000, then P1 "))
    (display
     (format #f "sets x to 10^6~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.40 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
