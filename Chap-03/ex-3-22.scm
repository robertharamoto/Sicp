#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.22                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-queue)
  (begin
    (let ((front-ptr (list))
          (rear-ptr (list)))
      (begin
        (define (empty-queue?)
          (begin
            (null? front-ptr)
            ))
        (define (front-queue)
          (begin
            (if (empty-queue?)
                (begin
                  (display
                   (format
                    #f "front-queue error: call front-queue on empty queue~%"))
                  (force-output))
                (begin
                  (car front-ptr)
                  ))
            ))
        (define (insert-queue! item)
          (begin
            (if (empty-queue?)
                (begin
                  (let ((new-pair (cons item (list))))
                    (begin
                      (set! front-ptr new-pair)
                      (set! rear-ptr new-pair)
                      )))
                (begin
                  (let ((new-pair (cons item (list)))
                        (last-elem rear-ptr))
                    (begin
                      (set-cdr! last-elem new-pair)
                      (set! rear-ptr new-pair)
                      ))
                  ))
            ))
        (define (delete-queue!)
          (begin
            (if (empty-queue?)
                (begin
                  (display
                   (format
                    #f "delete-queue error: call delete-queue on empty queue~%"))
                  (force-output))
                (begin
                  (set! front-ptr (cdr front-ptr))
                  ))
            ))
        (define (print-queue)
          (begin
            (display (format #f "~a~%" front-ptr))
            (force-output)
            ))
        (define (dispatch m)
          (begin
            (cond
             ((eq? m 'empty-queue?)
              (begin
                empty-queue?
                ))
             ((eq? m 'front-queue)
              (begin
                front-queue
                ))
             ((eq? m 'insert-queue!)
              (begin
                insert-queue!
                ))
             ((eq? m 'delete-queue!)
              (begin
                delete-queue!
                ))
             ((eq? m 'print-queue)
              (begin
                print-queue
                ))
             (else
              (begin
                (error "Undefined operation -- make-queue" m)
                )))
            ))
        (begin
          dispatch
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (empty-queue? z)
  (begin
    ((z 'empty-queue?))
    ))

;;;#############################################################
;;;#############################################################
(define (front-queue z)
  (begin
    ((z 'front-queue))
    ))

;;;#############################################################
;;;#############################################################
(define (insert-queue! z item)
  (begin
    ((z 'insert-queue!) item)
    ))

;;;#############################################################
;;;#############################################################
(define (delete-queue! z)
  (begin
    ((z 'delete-queue!))
    ))

;;;#############################################################
;;;#############################################################
(define (print-queue z)
  (begin
    ((z 'print-queue))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Instead of representing a queue "))
    (display
     (format #f "as a pair of~%"))
    (display
     (format #f "pointers, we can build a queue as "))
    (display
     (format #f "a procedure with~%"))
    (display
     (format #f "local state. The local state will "))
    (display
     (format #f "consist of pointers~%"))
    (display
     (format #f "to the beginning and the end of an "))
    (display
     (format #f "ordinary list.~%"))
    (display
     (format #f "Thus, the make-queue procedure will "))
    (display
     (format #f "have the form:~%"))
    (newline)
    (display
     (format #f "(define (make-queue)~%"))
    (display
     (format #f "  (let ((front-ptr ...)~%"))
    (display
     (format #f "        (rear-ptr ...))~%"))
    (display
     (format #f "    <definitions of internal "))
    (display
     (format #f "procedures>~%"))
    (display
     (format #f "    (define (dispatch m) ...)~%"))
    (display
     (format #f "    dispatch))~%"))
    (newline)
    (display
     (format #f "Complete the definition of make-queue "))
    (display
     (format #f "and provide~%"))
    (display
     (format #f "implementations of the queue operations "))
    (display
     (format #f "using this~%"))
    (display
     (format #f "representation.~%"))
    (newline)
    (display
     (format #f "(define (make-queue)~%"))
    (display
     (format #f "  (let ((front-ptr (list))~%"))
    (display
     (format #f "        (rear-ptr (list)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (define (empty-queue?) "))
    (display
     (format #f "(null? front-ptr))~%"))
    (display
     (format #f "      (define (front-queue)~%"))
    (display
     (format #f "        (if (empty-queue?)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (display~%"))
    (display
     (format #f "                 (format #f~%"))
    (display
     (format #f "                   \"front-queue error: "))
    (display
     (format #f "call front-queue on empty queue~~%\"))~%"))
    (display (format #f "              (force-output))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (car front-ptr)~%"))
    (display
     (format #f "              )))~%"))
    (display
     (format #f "      (define (insert-queue! item)~%"))
    (display
     (format #f "        (if (empty-queue?)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((new-pair "))
    (display
     (format #f "(cons item (list))))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  (set! front-ptr "))
    (display
     (format #f "new-pair)~%"))
    (display
     (format #f "                  (set! rear-ptr "))
    (display
     (format #f "new-pair)~%"))
    (display
     (format #f "                  )))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "                  (set-cdr! "))
    (display
     (format #f "last-elem new-pair)~%"))
    (display
     (format #f "                  (set! "))
    (display
     (format #f "rear-ptr new-pair)~%"))
    (display
     (format #f "                  )))~%"))
    (display
     (format #f "      (define (delete-queue!)~%"))
    (display
     (format #f "        (if (empty-queue?)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (display~%"))
    (display
     (format #f "                (format~%"))
    (display
     (format #f "                  #f \"delete-queue error: "))
    (display
     (format #f "call delete-queue on empty queue~~%\"))~%"))
    (display
     (format #f "              (force-output))~%"))
    (display
     (format #f "           (begin~%"))
    (display
     (format #f "              (set! front-ptr "))
    (display
     (format #f "(cdr front-ptr))~%"))
    (display
     (format #f "              )))~%"))
    (display
     (format #f "      (define (print-queue)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (display~%"))
    (display
     (format #f "            (format #f "))
    (display
     (format #f "\"~~a~~%\" front-ptr))~%"))
    (display
     (format #f "          (force-output)~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      (define (dispatch m)~%"))
    (display
     (format #f "        (cond~%"))
    (display
     (format #f "         ((eq? m 'empty-queue?) empty-queue?)~%"))
    (display
     (format #f "         ((eq? m 'front-queue) front-queue)~%"))
    (display
     (format #f "         ((eq? m 'insert-queue!) insert-queue!)~%"))
    (display
     (format #f "         ((eq? m 'delete-queue!) delete-queue!)~%"))
    (display
     (format #f "         ((eq? m 'print-queue) print-queue)~%"))
    (display
     (format #f "         (else~%"))
    (display
     (format #f "          (error~%"))
    (display
     (format #f "             \"Undefined operation "))
    (display
     (format #f "-- make-queue\" m))))~%"))
    (display
     (format #f "      dispatch~%"))
    (display
     (format #f "      )))~%"))
    (newline)
    (display
     (format #f "(define (empty-queue? z) "))
    (display
     (format #f "((z 'empty-queue?)))~%"))
    (display
     (format #f "(define (front-queue z) "))
    (display
     (format #f "((z 'front-queue)))~%"))
    (display
     (format #f "(define (insert-queue! z item) "))
    (display
     (format #f "((z 'insert-queue!) item))~%"))
    (display
     (format #f "(define (delete-queue! z) "))
    (display
     (format #f "((z 'delete-queue!)))~%"))
    (display
     (format #f "(define (print-queue z) "))
    (display
     (format #f "((z 'print-queue)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((q1 (make-queue)))
      (begin
        (display
         (format #f "(define q1 (make-queue))~%"))
        (insert-queue! q1 'a)
        (display
         (format #f "(insert-queue! q1 'a) -> "))
        (print-queue q1)

        (insert-queue! q1 'b)
        (display
         (format #f "(insert-queue! q1 'b) -> "))
        (print-queue q1)

        (delete-queue! q1)
        (display
         (format #f "(delete-queue! q1) -> "))
        (print-queue q1)

        (delete-queue! q1)
        (display
         (format #f "(delete-queue! q1) -> "))
        (print-queue q1)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.22 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
