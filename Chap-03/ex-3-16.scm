#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.16                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 14, 2022                              ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle decides to write a "))
    (display
     (format #f "procedure to count~%"))
    (display
     (format #f "the number of pairs in any list "))
    (display
     (format #f "structure. \"It's~%"))
    (display
     (format #f "easy,\" he reasons. \"The number "))
    (display
     (format #f "of pairs in any~%"))
    (display
     (format #f "structure is the number in the car "))
    (display
     (format #f "plus the number~%"))
    (display
     (format #f "in the cdr plus one more to count "))
    (display
     (format #f "the current pair.\"~%"))
    (display
     (format #f "So Ben writes the following "))
    (display
     (format #f "procedure:~%"))
    (display
     (format #f "(define (count-pairs x)~%"))
    (display
     (format #f "  (if (not (pair? x))~%"))
    (display
     (format #f "      0~%"))
    (display
     (format #f "      (+ (count-pairs (car x))~%"))
    (display
     (format #f "         (count-pairs (cdr x))~%"))
    (display
     (format #f "         1)))~%"))
    (display
     (format #f "Show that this procedure is not correct. "))
    (display
     (format #f "In particular,~%"))
    (display
     (format #f "draw box-and-pointer diagrams representing "))
    (display
     (format #f "list~%"))
    (display
     (format #f "structures made up of exactly three pairs "))
    (display
     (format #f "for which~%"))
    (display
     (format #f "Ben's procedure would return 3; "))
    (display
     (format #f "return 4; return 7;~%"))
    (display
     (format #f "never return at all.~%"))
    (newline)
    (display
     (format #f "(define x (cons 'a "))
    (display
     (format #f "(cons 'b (cons 'c (list)))))~%"))
    (display
     (format #f "(count-pairs x)~%"))
    (display
     (format #f "3~%"))
    (newline)
    (display
     (format #f "(define x (cons 'a 'b))~%"))
    (display
     (format #f "(define y (cons x "))
    (display
     (format #f "(cons x (list))))~%"))
    (display
     (format #f "(count-pairs y)~%"))
    (display
     (format #f "4~%"))
    (newline)
    (display
     (format #f "(define x (cons 'a 'b))~%"))
    (display
     (format #f "(define y (cons x x))~%"))
    (display
     (format #f "(define z (cons y y))~%"))
    (display
     (format #f "(count-pairs z)~%"))
    (display
     (format #f "7~%"))
    (newline)
    (display
     (format #f "(define x (cons 'a 'b))~%"))
    (display
     (format #f "(define y (cons x x))~%"))
    (display
     (format #f "(define z (cons y y))~%"))
    (display
     (format #f "(set-cdr! z z)~%"))
    (display
     (format #f "(count-pairs z)~%"))
    (display
     (format #f "stack overflow...~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.16 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
