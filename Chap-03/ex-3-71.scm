#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.71                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 16, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-stream-map proc s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          srfi-41:stream-null)
        (begin
          (let ((a1 (srfi-41:stream-car s1))
                (tail-1 (srfi-41:stream-cdr s1))
                (a2 (srfi-41:stream-car s2))
                (tail-2 (srfi-41:stream-cdr s2)))
            (begin
              (srfi-41:stream-cons
               (proc a1 a2)
               (my-stream-map proc tail-1 tail-2))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (my-stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (div-streams s1 s2)
  (begin
    (srfi-41:stream-map / s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (scale-stream stream factor)
  (begin
    (srfi-41:stream-map
     (lambda (x)
       (begin
         (* x factor)
         )) stream)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (partial-sums strm)
  (begin
    (srfi-41:stream-cons
     (srfi-41:stream-car strm)
     (add-streams
      (srfi-41:stream-cdr strm) (partial-sums strm)))
    ))

;;;#############################################################
;;;#############################################################
(define (stream-filter pred stream)
  (begin
    (cond
     ((srfi-41:stream-null? stream)
      (begin
        srfi-41:stream-null
        ))
     ((pred (srfi-41:stream-car stream))
      (begin
        (srfi-41:stream-cons
         (srfi-41:stream-car stream)
         (stream-filter
          pred
          (srfi-41:stream-cdr stream)))
        ))
     (else
      (begin
        (stream-filter pred (srfi-41:stream-cdr stream))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (merge-weighted s1 s2 wfunc)
  (begin
    (cond
     ((srfi-41:stream-null? s1)
      (begin
        s2
        ))
     ((srfi-41:stream-null? s2)
      (begin
        s1
        ))
     (else
      (begin
        (let ((s1car (srfi-41:stream-car s1))
              (s2car (srfi-41:stream-car s2)))
          (let ((w1 (wfunc s1car))
                (w2 (wfunc s2car)))
            (begin
              (cond
               ((< w1 w2)
                (begin
                  (srfi-41:stream-cons
                   s1car
                   (merge-weighted
                    (srfi-41:stream-cdr s1) s2 wfunc))
                  ))
               ((> w1 w2)
                (begin
                  (srfi-41:stream-cons
                   s2car
                   (merge-weighted
                    s1 (srfi-41:stream-cdr s2) wfunc))
                  ))
               (else
                (begin
                  (srfi-41:stream-cons
                   s1car
                   (srfi-41:stream-cons
                    s2car
                    (merge-weighted
                     (srfi-41:stream-cdr s1)
                     (srfi-41:stream-cdr s2) wfunc)
                    ))
                  )))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-1 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ a0 a1)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-2 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ (* 2 a0) (* 3 a1) (* 5 a0 a1))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (weight-func-3 alist)
  (begin
    (let ((a0 (car alist))
          (a1 (cadr alist)))
      (begin
        (+ (* a0 a0 a0) (* a1 a1 a1))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (interleave s1 s2)
  (begin
    (if (srfi-41:stream-null? s1)
        (begin
          s2)
        (begin
          (srfi-41:stream-cons
           (srfi-41:stream-car s1)
           (interleave s2 (srfi-41:stream-cdr s1)))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pairs s t)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s)
           (srfi-41:stream-car t))
     (interleave
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (pairs (srfi-41:stream-cdr s)
             (srfi-41:stream-cdr t))
      ))
    ))

;;;#############################################################
;;;#############################################################
(define (weighted-pairs s t wfunc)
  (begin
    (srfi-41:stream-cons
     (list (srfi-41:stream-car s)
           (srfi-41:stream-car t))
     (merge-weighted
      (srfi-41:stream-map
       (lambda (x)
         (begin
           (list (srfi-41:stream-car s) x)
           ))
       (srfi-41:stream-cdr t))
      (weighted-pairs
       (srfi-41:stream-cdr s)
       (srfi-41:stream-cdr t) wfunc)
      wfunc))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-two-loop)
  (begin
    (let ((nmax 100000))
      (let ((t3-int-streams
             (weighted-pairs
              integers integers weight-func-3))
            (rmax 6)
            (rcount 1)
            (gc-max 10000)
            (prev-result #f)
            (prev-weight #f)
            (dup-htable (make-hash-table nmax)))
        (begin
          (display
           (format #f "top ~a sum of two cubes in "
                   (1- rmax)))
          (display
           (format #f "two different ways~%"))
          (force-output)
          (do ((ii 0 (1+ ii)))
              ((or (>= ii nmax)
                   (> rcount rmax)))
            (begin
              (let ((alist
                     (my-stream-ref t3-int-streams ii)))
                (let ((aweight (weight-func-3 alist)))
                  (begin
                    (cond
                     ((equal? prev-weight #f)
                      (begin
                        (set! prev-result alist)
                        (set! prev-weight aweight)
                        ))
                     ((= prev-weight aweight)
                      (begin
                        (let ((aprev (car prev-result))
                              (bprev (cadr prev-result))
                              (athis (car alist))
                              (bthis (cadr alist)))
                          (begin
                            (display
                             (ice-9-format:format
                              #f "(~:d) ~:d^3 + ~:d^3 "
                              rcount aprev bprev))
                            (display
                             (ice-9-format:format
                              #f "= ~:d^3 + ~:d^3 = ~:d~%"
                              athis bthis aweight))
                            (display
                             (ice-9-format:format
                              #f "    (stream position = ~:d)~%"
                              ii))
                            (force-output)
                            (set! rcount (1+ rcount))
                            ))
                        ))
                     (else
                      (set! prev-result alist)
                      (set! prev-weight aweight)
                      ))
                    )))
              (if (zero? (modulo ii gc-max))
                  (begin
                    (gc)
                    ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-three-loop)
  (begin
    (let ((nmax 200000000))
      (let ((t3-int-streams
             (weighted-pairs
              integers integers weight-func-3))
            (rmax 4)
            (rcount 1)
            (gc-max 10000)
            (prev-2-result #f)
            (prev-2-weight #f)
            (prev-1-result #f)
            (prev-1-weight #f)
            (dup-htable (make-hash-table nmax)))
        (begin
          (display
           (format #f "top ~a sum of two cubes in "
                   (1- rmax)))
          (display
           (format #f "three different ways~%"))
          (force-output)
          (do ((ii 0 (1+ ii)))
              ((or (>= ii nmax)
                   (>= rcount rmax)))
            (begin
              (let ((alist
                     (my-stream-ref t3-int-streams ii)))
                (let ((aweight (weight-func-3 alist)))
                  (begin
                    (cond
                     ((equal? prev-2-weight #f)
                      (begin
                        (set! prev-2-result alist)
                        (set! prev-2-weight aweight)
                        ))
                     ((equal? prev-1-weight #f)
                      (begin
                        (set! prev-1-result alist)
                        (set! prev-1-weight aweight)
                        ))
                     ((and (= prev-1-weight aweight)
                           (= prev-2-weight aweight))
                      (begin
                        (let ((a2prev (car prev-2-result))
                              (b2prev (cadr prev-2-result))
                              (a1prev (car prev-1-result))
                              (b1prev (cadr prev-1-result))
                              (athis (car alist))
                              (bthis (cadr alist)))
                          (begin
                            (display
                             (ice-9-format:format
                              #f "(~:d) ~:d^3 + ~:d^3 = "
                              rcount a2prev b2prev))
                            (display
                             (ice-9-format:format
                              #f "~:d^3 + ~:d^3 =  "
                              a1prev b1prev))
                            (display
                             (ice-9-format:format
                              #f "~:d^3 + ~:d^3 = ~:d~%"
                              athis bthis aweight))
                            (display
                             (ice-9-format:format
                              #f "    (stream position = ~:d)~%"
                              ii))
                            (force-output)
                            (set! rcount (1+ rcount))
                            ))
                        ))
                     (else
                      (set! prev-2-result prev-1-result)
                      (set! prev-2-weight prev-1-weight)
                      (set! prev-1-result alist)
                      (set! prev-1-weight aweight)
                      ))
                    )))
              (if (zero? (modulo ii gc-max))
                  (begin
                    (gc)
                    ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Numbers that can be expressed as the sum "))
    (display
     (format #f "of two cubes in~%"))
    (display
     (format #f "more than one way are sometimes called "))
    (display
     (format #f "Ramanujan numbers,~%"))
    (display
     (format #f "in honor of the mathematician Srinivasa "))
    (display
     (format #f "Ramanujan. Ordered~%"))
    (display
     (format #f "streams of pairs provide an elegant "))
    (display
     (format #f "solution to the~%"))
    (display
     (format #f "problem of computing these numbers. To "))
    (display
     (format #f "find a number~%"))
    (display
     (format #f "that can be written as the sum of two "))
    (display
     (format #f "cubes in two~%"))
    (display
     (format #f "different ways, we need only generate "))
    (display
     (format #f "the stream of~%"))
    (display
     (format #f "pairs of integers (i, j) weighted "))
    (display
     (format #f "according to the~%"))
    (display
     (format #f "sum i^3 + j^3 (see exercise 3.70), "))
    (display
     (format #f "then search the~%"))
    (display
     (format #f "stream for two consecutive pairs with "))
    (display
     (format #f "the same weight.~%"))
    (display
     (format #f "Write a procedure to generate the "))
    (display
     (format #f "Ramanujan numbers.~%"))
    (display
     (format #f "The first such number is 1,729.~%"))
    (display
     (format #f "What are the next five?~%"))
    (newline)
    (display
     (format #f "Interesting websites to see:~%"))
    (display
     (format #f "https://rosettacode.org/wiki/Taxicab_numbers~%"))
    (display
     (format #f "https://oeis.org/A011541~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (sub-main-two-loop)
    (newline)

    (let ((long-process-flag #f))
      (begin
        ;;; 35 minute routine
        (if (equal? long-process-flag #t)
            (begin
                 ;;; sum of two cubes in three different ways
              (sub-main-three-loop)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.71 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (display (format #f "sum of two cubes~%"))
             ;;; sum of two cubes in two different ways
             (main-loop)
             (newline)
             ))


          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
