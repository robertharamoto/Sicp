#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.54                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  last updated August 16, 2022                         ###
;;;###                                                       ###
;;;###  updated February 24, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### stream functions
(use-modules ((srfi srfi-41)
              :renamer (symbol-prefix-proc 'srfi-41:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (my-stream-ref s n)
  (begin
    (if (<= n 0)
        (begin
          (srfi-41:stream-car s))
        (begin
          (my-stream-ref (srfi-41:stream-cdr s) (- n 1))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-streams s1 s2)
  (begin
    (srfi-41:stream-map + s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define (mul-streams s1 s2)
  (begin
    (srfi-41:stream-map * s1 s2)
    ))

;;;#############################################################
;;;#############################################################
(define ones (srfi-41:stream-cons 1 ones))

;;;#############################################################
;;;#############################################################
(define integers
  (srfi-41:stream-cons 1 (add-streams ones integers)))

;;;#############################################################
;;;#############################################################
(define factorials
  (srfi-41:stream-cons 1 (mul-streams integers factorials)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Define a procedure mul-streams, analogous "))
    (display
     (format #f "to add-streams~%"))
    (display
     (format #f "that produces the elementwise product of "))
    (display
     (format #f "its two input~%"))
    (display
     (format #f "streams. Use this together with the "))
    (display
     (format #f "stream of integers~%"))
    (display
     (format #f "to complete the following definition "))
    (display
     (format #f "of the stream~%"))
    (display
     (format #f "whose nth element (counting from 0) is "))
    (display
     (format #f "(n + 1) factorial:~%"))
    (newline)
    (display
     (format #f "(define factorials "))
    (display
     (format #f "(cons-stream 1 "))
    (display
     (format #f "(mul-streams <??> <??>)))~%"))
    (newline)
    (display
     (format #f "(define (mul-streams s1 s2)~%"))
    (display
     (format #f "  (stream-map * s1 s2))~%"))
    (newline)
    (display
     (format #f "(define factorials~%"))
    (display
     (format #f "  (cons-stream 1 (mul-streams "))
    (display
     (format #f "integers factorials)))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list 0 1 2 3 4 5 6 7))
          (fac-stream factorials))
      (begin
        (for-each
         (lambda (anum)
           (begin
             (display
              (format
               #f "(stream-ref fac-stream ~a) "
               anum))
             (display
              (format
               #f "= ~a! = ~a~%"
               anum (my-stream-ref fac-stream anum)))
             (force-output)
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.54 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
