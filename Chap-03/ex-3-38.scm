#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 3.38                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 15, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose that Peter, Paul, and Mary share "))
    (display
     (format #f "a joint bank~%"))
    (display
     (format #f "account that initially contains $100. "))
    (display
     (format #f "Concurrently,~%"))
    (display
     (format #f "Peter deposits $10, Paul withdraws "))
    (display
     (format #f "$20, and Mary~%"))
    (display
     (format #f "withdraws half the money in the "))
    (display
     (format #f "account, by~%"))
    (display
     (format #f "executing the following commands:~%"))
    (newline)
    (display
     (format #f "Peter: (set! balance (+ balance 10))~%"))
    (display
     (format #f "Paul:  (set! balance (- balance 20))~%"))
    (display
     (format #f "Mary:  (set! balance "))
    (display
     (format #f "(- balance (/ balance 2)))~%"))
    (newline)
    (display
     (format #f "a. List all the different possible "))
    (display
     (format #f "values for balance~%"))
    (display
     (format #f "after these three transactions have "))
    (display
     (format #f "been completed,~%"))
    (display
     (format #f "assuming that the banking system forces "))
    (display
     (format #f "the three~%"))
    (display
     (format #f "processes to run sequentially in "))
    (display
     (format #f "some order.~%"))
    (display
     (format #f "b. What are some other values that "))
    (display
     (format #f "could be produced~%"))
    (display
     (format #f "if the system allows the processes to "))
    (display
     (format #f "be interleaved?~%"))
    (display
     (format #f "Draw timing diagrams like the one in "))
    (display
     (format #f "figure 3.29 to~%"))
    (display
     (format #f "explain how these values can occur.~%"))
    (newline)
    (display
     (format #f "(a) all possible sequential transactions~%"))
    (display
     (format #f "(1) peter, paul, mary: 100->110->90->45~%"))
    (display
     (format #f "(2) paul, peter, mary: 100->80->90->45~%"))
    (display
     (format #f "(3) peter, mary, paul: 100->110->55->35~%"))
    (display
     (format #f "(4) paul, mary, peter: 100->80->40->50~%"))
    (display
     (format #f "(5) mary, paul, peter: 100->50->30->40~%"))
    (display
     (format #f "(6) mary, peter, paul: 100->50->60->40~%"))
    (display
     (format #f "All possible unique values for the "))
    (display
     (format #f "final balance~%"))
    (display
     (format #f "are 50, 45, 40, and 35.~%"))
    (newline)
    (display
     (format #f "                 peter     paul     "))
    (display
     (format #f "mary     bank~%"))
    (display
     (format #f "access balance:  100       100      "))
    (display
     (format #f "100      100~%"))
    (display
     (format #f "new value:       110        80       "))
    (display
     (format #f "50      100~%"))
    (display
     (format #f "set! balance:    110        80       "))
    (display
     (format #f "50       ?~%"))
    (display
     (format #f "--------------------------------------------------~%"))
    (display
     (format #f "Other values that could be produced, "))
    (display
     (format #f "depending on interleaved~%"))
    (display
     (format #f "execution: 110, 80, 50.  Even more "))
    (display
     (format #f "values could be~%"))
    (display
     (format #f "produced if all of the balance accesses "))
    (display
     (format #f "were staggered in~%"))
    (display
     (format #f "time.~%"))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 3.38 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
