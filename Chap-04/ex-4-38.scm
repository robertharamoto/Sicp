#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.38                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Modify the multiple-dwelling procedure "))
    (display
     (format #f "to omit the~%"))
    (display
     (format #f "requirement that Smith and Fletcher "))
    (display
     (format #f "do not live on~%"))
    (display
     (format #f "adjacent floors. How many solutions "))
    (display
     (format #f "are there to this~%"))
    (display
     (format #f "modified puzzle?~%"))
    (newline)
    (display
     (format #f "(define (multiple-dwelling)~%"))
    (display
     (format #f "  (let ((baker (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (cooper (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (fletcher (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (miller (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (smith (amb 1 2 3 4 5)))~%"))
    (display
     (format #f "    (require~%"))
    (display
     (format #f "     (distinct?~%"))
    (display
     (format #f "(list baker cooper fletcher "))
    (display
     (format #f "miller smith)))~%"))
    (display
     (format #f "    (require (not (= baker 5)))~%"))
    (display
     (format #f "    (require (not (= cooper 1)))~%"))
    (display
     (format #f "    (require (not (= fletcher 5)))~%"))
    (display
     (format #f "    (require (not (= fletcher 1)))~%"))
    (display
     (format #f "    (require (> miller cooper))~%"))
    (display
     (format #f "    (require (not (= (abs "))
    (display
     (format #f "(- fletcher cooper)) 1)))~%"))
    (display
     (format #f "    (list (list 'baker baker)~%"))
    (display
     (format #f "          (list 'cooper cooper)~%"))
    (display
     (format #f "          (list 'fletcher fletcher)~%"))
    (display
     (format #f "          (list 'miller miller)~%"))
    (display
     (format #f "          (list 'smith smith))))~%"))
    (newline)
    (display
     (format #f "The result from the textbook is:~%"))
    (display
     (format #f "((baker 3) (cooper 2) (fletcher 4) (miller 5) (smith 1))~%"))
    (display
     (format #f "If Smith and Fletcher can live on "))
    (display
     (format #f "adjacent floors, then:~%"))
    (newline)
    (display
     (format #f "The solution method is similar to solving "))
    (display
     (format #f "a sudoku puzzle,~%"))
    (display
     (format #f "try several possibilities for Miller, "))
    (display
     (format #f "and see where the~%"))
    (display
     (format #f "requirements of the puzzle lead. Since "))
    (display
     (format #f "Miller must be on~%"))
    (display
     (format #f "a floor greater than Cooper, and Cooper "))
    (display
     (format #f "cannot be on~%"))
    (display
     (format #f "the first floor, this means that Miller "))
    (display
     (format #f "can only be on~%"))
    (display
     (format #f "floors 3, 4, and 5.~%"))
    (newline)
    (display
     (format #f "Trial 1: Let Miller be on floor 3. Then "))
    (display
     (format #f "Cooper can only~%"))
    (display
     (format #f "be on floor 2. Since Cooper and Fletcher "))
    (display
     (format #f "cannot be adjacent,~%"))
    (display
     (format #f "this implies Fletcher is on floor 4. The "))
    (display
     (format #f "only unoccupied~%"))
    (display
     (format #f "floors are 1 and 5, and Baker cannot be "))
    (display
     (format #f "on floor 5, so~%"))
    (display
     (format #f "Baker is on floor 1, and Smith is "))
    (display
     (format #f "on floor 5.~%"))
    (newline)
    (display
     (format #f "Trial 2: Let Miller be on floor 4. "))
    (display
     (format #f "Then Cooper can be~%"))
    (display
     (format #f "on floor 2 or 3. If Cooper is on floor 2, "))
    (display
     (format #f "then Fletcher~%"))
    (display
     (format #f "can only be on floor 4, which is taken, "))
    (display
     (format #f "(Fletcher cannot~%"))
    (display
     (format #f "be on floors 1 or 5). If Cooper is on "))
    (display
     (format #f "floor 3, then~%"))
    (display
     (format #f "Fletcher will be adjacent to Cooper. So "))
    (display
     (format #f "there is no~%"))
    (display
     (format #f "solution, and Miller is on floor 4.~%"))
    (newline)
    (display
     (format #f "Trial 3: Let Miller be on floor 5. "))
    (display
     (format #f "Then Cooper can be~%"))
    (display
     (format #f "on floors 2, 3, or 4. Since Fletcher "))
    (display
     (format #f "cannot be adjacent~%"))
    (display
     (format #f "to Cooper, then the only solutions "))
    (display
     (format #f "occur when Cooper~%"))
    (display
     (format #f "is on 2 and Fletcher is on 4, or Cooper "))
    (display
     (format #f "is on 4 and~%"))
    (display
     (format #f "Fletcher is on 2. If Cooper is on 2, then "))
    (display
     (format #f "Fletcher is on~%"))
    (display
     (format #f "4, and Baker and Smith can be on floors "))
    (display
     (format #f "1 or 3. If Cooper~%"))
    (display
     (format #f "is on 4, then Fletcher is on 2, and "))
    (display
     (format #f "Baker and Smith~%"))
    (display
     (format #f "can be on floors 1 or 3. This means "))
    (display
     (format #f "that there are 4~%"))
    (display
     (format #f "solutions when Miller is on floor 5.~%"))
    (newline)
    (display
     (format #f "Altogether, there are 5 solutions "))
    (display
     (format #f "when the~%"))
    (display
     (format #f "Smith/Fletcher requirement is lifted.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.38 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
