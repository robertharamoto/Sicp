#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.35                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (an-integer-between low high)
  (begin
    (let ((diff (- high low)))
      (begin
        (if (< diff 0)
            (begin
              (set! diff (* -1 diff))
              (+ high (random diff)))
            (begin
              (+ low (random diff))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-an-integer-between result-hash-table)
 (begin
   (let ((sub-name "test-an-integer-between")
         (test-list
          (list
           (list 0 10) (list 1 100) (list 10 20) (list 50 100)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((low (list-ref alist 0))
                  (high (list-ref alist 1)))
              (let ((result (an-integer-between low high)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : low=~a, high=~a, "
                        sub-name test-label-index
                        low high))
                      (err-msg-2
                       (format #f "result=~a" result)))
                  (begin
                    (unittest2:assert?
                     (and (<= result high)
                          (>= result low))
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Write a procedure an-integer-between "))
    (display
     (format #f "that returns an~%"))
    (display
     (format #f "integer between two given bounds. This "))
    (display
     (format #f "can be used to~%"))
    (display
     (format #f "implement a procedure that finds "))
    (display
     (format #f "Pythagorean triples,~%"))
    (display
     (format #f "i.e., triples of integers (i,j,k) "))
    (display
     (format #f "between the given~%"))
    (display
     (format #f "bounds such that i <= j and "))
    (display
     (format #f "i2 + j2 = k2 "))
    (display
     (format #f "as follows:~%"))
    (newline)
    (display
     (format #f "(define (a-pythagorean-triple-between "))
    (display
     (format #f "low high)~%"))
    (display
     (format #f "  (let ((i (an-integer-between "))
    (display
     (format #f "low high)))~%"))
    (display
     (format #f "    (let ((j (an-integer-between "))
    (display
     (format #f "i high)))~%"))
    (display
     (format #f "      (let ((k (an-integer-between "))
    (display
     (format #f "j high)))~%"))
    (display
     (format #f "        (require (= (+ (* i i) (* j j))"))
    (display
     (format #f "(* k k)))~%"))
    (display
     (format #f "        (list i j k)))))~%"))
    (newline)
    (display
     (format #f "(define (an-integer-between low high)~%"))
    (display
     (format #f "  (let ((diff (- high low)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (if (< diff 0)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (set! diff (* -1 diff))~%"))
    (display
     (format #f "            (+ high (random diff)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (+ low (random diff))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.35 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
