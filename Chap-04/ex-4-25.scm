#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.25                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (unless condition usual-value exceptional-value)
  (begin
    (if condition
        exceptional-value
        usual-value)
    ))

;;;#############################################################
;;;#############################################################
(define (factorial n)
  (begin
    (if (> n 1)
        (begin
          (* n (factorial (- n 1))))
        (begin
          1
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial result-hash-table)
 (begin
   (let ((sub-name "test-factorial")
         (test-list
          (list
           (list 1 1) (list 2 2) (list 3 6) (list 4 24)
           (list 5 120) (list 6 720) (list 7 5040)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (factorial nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a : "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Suppose that (in ordinary applicative-order "))
    (display
     (format #f "Scheme), we~%"))
    (display
     (format #f "define unless as shown above and then "))
    (display
     (format #f "define factorial~%"))
    (display
     (format #f "in terms of unless as:~%"))
    (newline)
    (display
     (format #f "(define (factorial n)~%"))
    (display
     (format #f "  (unless (= n 1)~%"))
    (display
     (format #f "          (* n (factorial (- n 1)))~%"))
    (display
     (format #f "          1))~%"))
    (newline)
    (display
     (format #f "What happens if we attempt to evaluate "))
    (display
     (format #f "(factorial 5)?~%"))
    (display
     (format #f "Will our definitions work in a "))
    (display
     (format #f "normal-order language?~%"))
    (newline)
    (display
     (format #f "In ordinary scheme, because the arguments "))
    (display
     (format #f "to the unless~%"))
    (display
     (format #f "statement are evaluated first, the "))
    (display
     (format #f "(* n (factorial (- n 1)))~%"))
    (display
     (format #f "argument gets evaluated repeatedly. This "))
    (display
     (format #f "leads to an~%"))
    (display
     (format #f "infinite loop.~%"))
    (newline)
    (display
     (format #f "In a normal-order language, the "))
    (display
     (format #f "(* n (factorial (- n 1)))~%"))
    (display
     (format #f "argument is not evaluated until it's "))
    (display
     (format #f "needed, so this~%"))
    (display
     (format #f "definition will work.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.25 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
