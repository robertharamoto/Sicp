#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.42                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; for each pair in tf-pairs-list, one pair is true the other is false
(define (is-config-ok? a-list-list tf-pairs-list)
  (begin
    (let ((ok-flag #t))
      (begin
        (for-each
         (lambda (tf-pair)
           (begin
             (let ((tf-1 (list-ref tf-pair 0))
                   (tf-2 (list-ref tf-pair 1)))
               (let ((a1-flag (member tf-1 a-list-list))
                     (a2-flag (member tf-2 a-list-list)))
                 (begin
                   (if (or
                        (and (not (equal? a1-flag #f))
                             (not (equal? a2-flag #f)))
                        (and (equal? a1-flag #f)
                             (equal? a2-flag #f)))
                       (begin
                         (set! ok-flag #f)
                         ))
                   )))
             )) tf-pairs-list)

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-config-ok result-hash-table)
 (begin
   (let ((sub-name "test-is-config-ok")
         (test-list
          (list
           (list (list (list 'betty 1) (list 'ethel 1)
                       (list 'joan 3) (list 'kitty 2)
                       (list 'mary 5)) #t)
           (list (list (list 'betty 3) (list 'ethel 1)
                       (list 'joan 3) (list 'kitty 2)
                       (list 'mary 4)) #f)
           ))
         (tf-pairs-list
          (list
           (list (list 'kitty 2) (list 'betty 3))
           (list (list 'ethel 1) (list 'joan 2))
           (list (list 'joan 3) (list 'ethel 5))
           (list (list 'kitty 2) (list 'mary 4))
           (list (list 'mary 4) (list 'betty 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((score-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (is-config-ok? score-list tf-pairs-list)))
                (let ((err-msg-1
                       (format
                        #f "~a (~a) error : score list=~a : "
                        sub-name test-label-index score-list))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; for each pair in tf-pairs-list, one pair is true the other is false
(define (lookup-true-pairs a-list-list tf-pairs-list)
  (begin
    (let ((ok-flag #t)
          (true-pairs-list (list)))
      (begin
        (for-each
         (lambda (tf-pair)
           (begin
             (let ((tf-1 (list-ref tf-pair 0))
                   (tf-2 (list-ref tf-pair 1)))
               (let ((a1-flag (member tf-1 a-list-list))
                     (a2-flag (member tf-2 a-list-list)))
                 (begin
                   (cond
                    ((and (not (equal? a1-flag #f))
                          (equal? a2-flag #f))
                     (begin
                       (if (equal?
                            (member tf-1 true-pairs-list) #f)
                           (begin
                             (set!
                              true-pairs-list
                              (cons tf-1 true-pairs-list))
                             ))
                       ))
                    ((and (equal? a1-flag #f)
                          (not (equal? a2-flag #f)))
                     (begin
                       (if (equal?
                            (member tf-2 true-pairs-list) #f)
                           (begin
                             (set!
                              true-pairs-list
                              (cons tf-2 true-pairs-list))
                             ))
                       ))
                    (else
                     (begin
                       (set! ok-flag #f)
                       )))
                   )))
             )) tf-pairs-list)

        (if (equal? ok-flag #t)
            (begin
              (reverse true-pairs-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append
            err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append
                     err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lookup-true-pairs result-hash-table)
 (begin
   (let ((sub-name "test-lookup-true-pairs")
         (test-list
          (list
           (list (list (list 'betty 1) (list 'ethel 1)
                       (list 'joan 3) (list 'kitty 2)
                       (list 'mary 5))
                 (list (list 'kitty 2) (list 'ethel 1)
                       (list 'joan 3) (list 'betty 1)))
           (list (list (list 'betty 3) (list 'ethel 1)
                       (list 'joan 3) (list 'kitty 2)
                       (list 'mary 4)) (list))
           ))
         (tf-pairs-list
          (list
           (list (list 'kitty 2) (list 'betty 3))
           (list (list 'ethel 1) (list 'joan 2))
           (list (list 'joan 3) (list 'ethel 5))
           (list (list 'kitty 2) (list 'mary 4))
           (list (list 'mary 4) (list 'betty 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((place-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (lookup-true-pairs place-list tf-pairs-list)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : place list=~a : "
                        sub-name test-label-index
                        place-list)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (place-not-in-list? aplace a-list-list)
  (begin
    (let ((ok-flag #t)
          (llen (length a-list-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii llen)
                 (equal? ok-flag #f)))
          (begin
            (let ((apair (list-ref a-list-list ii)))
              (let ((this-name (car apair))
                    (this-place (cadr apair)))
                (begin
                  (if (equal? this-place aplace)
                      (begin
                        (set! ok-flag #f)
                        ))
                  )))
            ))
        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-place-not-in-list result-hash-table)
 (begin
   (let ((sub-name "test-place-not-in-list")
         (test-list
          (list
           (list 5 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #t)
           (list 1 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #t)
           (list 2 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #f)
           (list 3 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #f)
           (list 4 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #f)
           (list 6 (list (list 'baker 3) (list 'cooper 5)
                         (list 'fletcher 2) (list 'miller 4)
                         (list 'smith 1)) #t)
           (list 1 (list (list 'baker 3) (list 'cooper 5)
                         (list 'fletcher 2) (list 'miller 4)
                         (list 'smith 1)) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((aplace (list-ref alist 0))
                  (place-list-list (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (place-not-in-list?
                      aplace place-list-list)))
                (let ((err-msg-1
                       (format
                        #f "~a (~a) error : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "aplace ~a, place list=~a : "
                        aplace place-list-list))
                      (err-msg-3
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (enumerate-configurations a-list-list)
  (define (local-iter input-list-list curr-list acc-list-list)
    (begin
      (if (null? input-list-list)
          (begin
            (cons (reverse curr-list) acc-list-list))
          (begin
            (let ((this-trial (car input-list-list))
                  (tail-list (cdr input-list-list)))
              (let ((name (car this-trial))
                    (place-list (cdr this-trial)))
                (begin
                  (for-each
                   (lambda (aplace)
                     (begin
                       (if (place-not-in-list? aplace curr-list)
                           (begin
                             (let ((next-acc-list
                                    (local-iter
                                     tail-list
                                     (cons (list name aplace) curr-list)
                                     acc-list-list)))
                               (begin
                                 (set! acc-list-list next-acc-list)
                                 ))
                             ))
                       )) place-list)

                  acc-list-list
                  )))
            ))
      ))
  (begin
    (let ((result-list-list
           (reverse
            (local-iter a-list-list (list) (list)))))
      (begin
        result-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-enumerate-configurations result-hash-table)
 (begin
   (let ((sub-name "test-enumerate-configurations")
         (test-list
          (list
           (list
            (list (list 'baker 1))
            (list (list (list 'baker 1))))
           (list
            (list (list 'baker 1 2)
                  (list 'cooper 1 2))
            (list (list (list 'baker 1) (list 'cooper 2))
                  (list (list 'baker 2) (list 'cooper 1))))
           (list
            (list (list 'baker 1 2 3)
                  (list 'cooper 1 2 3)
                  (list 'fletcher 1 2 3))
            (list (list (list 'baker 1) (list 'cooper 2) (list 'fletcher 3))
                  (list (list 'baker 1) (list 'cooper 3) (list 'fletcher 2))
                  (list (list 'baker 2) (list 'cooper 1) (list 'fletcher 3))
                  (list (list 'baker 2) (list 'cooper 3) (list 'fletcher 1))
                  (list (list 'baker 3) (list 'cooper 1) (list 'fletcher 2))
                  (list (list 'baker 3) (list 'cooper 2) (list 'fletcher 1))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (enumerate-configurations input-list)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : input floor list=~a : "
                        sub-name test-label-index input-list)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Solve the following \"Liars\" puzzle "))
    (display
     (format #f "(from Phillips 1934):~%"))
    (display
     (format #f "Five schoolgirls sat for an examination. "))
    (display
     (format #f "Their parents~%"))
    (display
     (format #f "-- so they thought -- showed an undue "))
    (display
     (format #f "degree of interest~%"))
    (display
     (format #f "in the result. They therefore agreed "))
    (display
     (format #f "that, in writing~%"))
    (display
     (format #f "home about the examination, each girl "))
    (display
     (format #f "should make one~%"))
    (display
     (format #f "true statement and one untrue one. The "))
    (display
     (format #f "following are the~%"))
    (display
     (format #f "relevant passages from their letters:~%"))
    (newline)
    (display
     (format #f "Betty: \"Kitty was second in the "))
    (display
     (format #f "examination. I was~%"))
    (display
     (format #f "only third.\"~%"))
    (display
     (format #f "Ethel: \"You'll be glad to hear that "))
    (display
     (format #f "I was on top.~%"))
    (display
     (format #f "Joan was second.\"~%"))
    (display
     (format #f "Joan: \"I was third, and poor old "))
    (display
     (format #f "Ethel was bottom.\"~%"))
    (display
     (format #f "Kitty: \"I came out second. Mary was "))
    (display
     (format #f "only fourth.\"~%"))
    (display
     (format #f "Mary: \"I was fourth. Top place was "))
    (display
     (format #f "taken by Betty.\"~%"))
    (display
     (format #f "What in fact was the order in which "))
    (display
     (format #f "the five girls~%"))
    (display
     (format #f "were placed?~%"))
    (newline)
    (display
     (format #f "There are 5! = 120 possible distinct "))
    (display
     (format #f "test orderings,~%"))
    (display
     (format #f "so a straight-forward enumeration of "))
    (display
     (format #f "all possible~%"))
    (display
     (format #f "configurations was "))
    (display
     (format #f "performed.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((possible-list-list
           (list
            (list 'betty 1 2 3 4 5)
            (list 'ethel 1 2 3 4 5)
            (list 'joan 1 2 3 4 5)
            (list 'kitty 1 2 3 4 5)
            (list 'mary 1 2 3 4 5)))
          (tf-pairs-list
           (list
            (list (list 'kitty 2) (list 'betty 3))
            (list (list 'ethel 1) (list 'joan 2))
            (list (list 'joan 3) (list 'ethel 5))
            (list (list 'kitty 2) (list 'mary 4))
            (list (list 'mary 4) (list 'betty 1))
            )))
      (let ((exam-list-list
             (enumerate-configurations possible-list-list))
            (md-count 0))
        (begin
          (for-each
           (lambda (aconfig)
             (begin
               (if (is-config-ok? aconfig tf-pairs-list)
                   (begin
                     (let ((true-list
                            (lookup-true-pairs aconfig tf-pairs-list)))
                       (begin
                         (set! md-count (1+ md-count))
                         (display
                          (ice-9-format:format
                           #f "(~:d)  ~a~%"
                           md-count aconfig))
                         (display
                          (format
                           #f "     true statements = ~a~%"
                           (sort true-list
                                 (lambda (a b)
                                   (begin
                                     (string-ci<?
                                      (symbol->string (car a))
                                      (symbol->string (car b))))))
                           ))
                         (newline)
                         (force-output)
                         ))
                     ))
               )) exam-list-list)

          (display
           (ice-9-format:format
            #f "found a total of ~:d solutions~%" md-count))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.42 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
