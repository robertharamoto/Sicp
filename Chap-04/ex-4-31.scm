#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.31                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;### ex-4-31-module - upward-compatible lazy evaluation
(use-modules ((ex-4-31-module)
              :renamer (symbol-prefix-proc 'ex-4-31-module:)))

;;;#############################################################
;;;#############################################################
(define (scheme-test-version run-code fibonacci-list test-list)
  (begin
    (for-each
     (lambda (anum)
       (begin
         (let ((fib-result
                (run-code
                 (list 'begin fibonacci-list (list 'fib anum)))))
           (begin
             (display
              (ice-9-format:format
               #f "(fib ~a) = ~:d~%" anum fib-result))
             (force-output)
             ))
         )) test-list)
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-fib)
  (begin
    (let ((test-list (list 10 20 30 40 50))
          (fibonacci-list
           (list 'define (list 'fib 'nn)
                 (list
                  'define (list 'fib-iter 'b
                                (list 'a 'lazy) 'counter)
                  (list 'if (list '<= 'counter 0)
                        'b
                        (list 'fib-iter 'a
                              (list '+ 'a 'b)
                              (list '1- 'counter))
                        ))
                 (list 'fib-iter 0 1 'nn))))
      (begin
        (display
         (format
          #f "scheme test, specified lazy evaluator~%"))
        (force-output)
        (scheme-test-version
         ex-4-31-module:run-code
         fibonacci-list test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-prime-version run-code prime-list test-list)
  (begin
    (for-each
     (lambda (anum)
       (begin
         (let ((num-primes
                (run-code
                 (list prime-list (list 'count-primes anum)))))
           (begin
             (display
              (ice-9-format:format
               #f "(num-primes <= ~:d) = ~:d~%" anum num-primes))
             (force-output)
             ))
         )) test-list)
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-count-primes)
  (begin
    (let ((test-list (list 10 20 30 40 50 100))
          (prime-list
           (list
            (list 'define (list 'smallest-divisor 'nn)
                  (list
                   'define
                   (list 'find-divisor 'nn 'test-divisor 'max-divisor)
                   (list 'cond (list (list '> 'test-divisor 'max-divisor)
                                     'nn)
                         (list
                          (list 'zero?
                                (list 'modulo 'nn 'test-divisor))
                          'test-divisor)
                         (list
                          'else
                          (list 'find-divisor 'nn
                                (list '+ 'test-divisor 2)
                                'max-divisor))))
                  (list
                   'if (list 'zero? (list 'modulo 'nn 2))
                   2 (list 'find-divisor 'nn 3
                           (list
                            '1+
                            (list 'inexact->exact
                                  (list 'truncate (list 'sqrt 'nn)))
                            ))))
            (list 'define (list 'prime? 'nn)
                  (list
                   '= 'nn (list 'smallest-divisor 'nn)))
            (list
             'define
             (list 'count-primes-iter
                   (list 'nn 'lazy-memo) 'max-nn 'counter)
             (list
              'if (list '> 'nn 'max-nn)
              'counter
              (list 'if (list 'prime? 'nn)
                    (list 'count-primes-iter
                          (list '+ 'nn 2) 'max-nn (list '1+ 'counter))
                    (list 'count-primes-iter
                          (list '+ 'nn 2) 'max-nn 'counter))
              ))
            (list
             'define (list 'count-primes 'max-nn)
             (list 'count-primes-iter 3 'max-nn 1))
            )))
      (begin
        (display
         (format
          #f "scheme test, specified lazy evaluator~%"))
        (scheme-test-prime-version
         ex-4-31-module:run-code
         prime-list test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-eval-id-test)
  (begin
    (let ((memo-test-list
           (list
            (list 'define 'count 0)
            (list 'define (list 'id (list 'x 'lazy-memo))
                  (list 'set! 'count (list '+ 'count 1))
                  'x)
            (list 'define 'w (list 'id (list 'id 10)))
            (list 'begin
                  (list 'display 'count) (list 'newline)
                  (list 'display 'w) (list 'newline)
                  (list 'display 'count) (list 'newline)
                  #t)
            ))
          (lazy-test-list
           (list
            (list 'define 'count 0)
            (list 'define (list 'id (list 'x 'lazy))
                  (list 'set! 'count (list '+ 'count 1))
                  'x)
            (list 'define 'w (list 'id (list 'id 10)))
            (list 'begin
                  (list 'display 'count) (list 'newline)
                  (list 'display 'w) (list 'newline)
                  (list 'display 'count) (list 'newline)
                  #t)
            ))
          (applicative-test-list
           (list
            (list 'define 'count 0)
            (list 'define (list 'id 'x)
                  (list 'set! 'count (list '+ 'count 1))
                  'x)
            (list 'define 'w (list 'id (list 'id 10)))
            (list 'begin
                  (list 'display 'count) (list 'newline)
                  (list 'display 'w) (list 'newline)
                  (list 'display 'count) (list 'newline)
                  #t)
            )))
      (begin
        (display
         (format #f "testing the exercise 4.27 code~%"))
        (display (format #f "~a~%" memo-test-list))
        (newline)

        (display
         (format
          #f "scheme test, (define (id (x lazy-memo)))~%"))
        (force-output)
        (ex-4-31-module:run-code memo-test-list)
        (newline)

        (display
         (format
          #f "scheme test, (define (id (x lazy)))~%"))
        (ex-4-31-module:run-code lazy-test-list)
        (newline)

        (display
         (format
          #f "scheme test, (define (id x))~%"))
        (ex-4-31-module:run-code applicative-test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The approach taken in this section is "))
    (display
     (format #f "somewhat unpleasant,~%"))
    (display
     (format #f "because it makes an incompatible change "))
    (display
     (format #f "to Scheme. It~%"))
    (display
     (format #f "might be nicer to implement lazy "))
    (display
     (format #f "evaluation as an~%"))
    (display
     (format #f "upward-compatible extension, that is, "))
    (display
     (format #f "so that ordinary~%"))
    (display
     (format #f "Scheme programs will work as before. We "))
    (display
     (format #f "can do this~%"))
    (display
     (format #f "by extending the syntax of procedure "))
    (display
     (format #f "declarations to~%"))
    (display
     (format #f "let the user control whether or not "))
    (display
     (format #f "arguments are to~%"))
    (display
     (format #f "be delayed. While we're at it, we may "))
    (display
     (format #f "as well also give~%"))
    (display
     (format #f "the user the choice between delaying with "))
    (display
     (format #f "and without~%"))
    (display
     (format #f "memoization. For example, the "))
    (display
     (format #f "definition~%"))
    (newline)
    (display
     (format #f "(define (f a (b lazy) c "))
    (display
     (format #f "(d lazy-memo))~%"))
    (display
     (format #f "  ...)~%"))
    (newline)
    (display
     (format #f "would define f to be a procedure of four "))
    (display
     (format #f "arguments, where~%"))
    (display
     (format #f "the first and third arguments are "))
    (display
     (format #f "evaluated when the~%"))
    (display
     (format #f "procedure is called, the second argument "))
    (display
     (format #f "is delayed, and~%"))
    (display
     (format #f "the fourth argument is both delayed "))
    (display
     (format #f "and memoized.~%"))
    (display
     (format #f "Thus, ordinary procedure definitions will "))
    (display
     (format #f "produce the same~%"))
    (display
     (format #f "behavior as ordinary Scheme, while adding "))
    (display
     (format #f "the lazy-memo~%"))
    (display
     (format #f "declaration to each parameter of every "))
    (display
     (format #f "compound procedure~%"))
    (display
     (format #f "will produce the behavior of the lazy "))
    (display
     (format #f "evaluator defined~%"))
    (display
     (format #f "in this section. Design and implement the "))
    (display
     (format #f "changes required~%"))
    (display
     (format #f "to produce such an extension to Scheme. "))
    (display
     (format #f "You will have~%"))
    (display
     (format #f "to implement new syntax procedures to "))
    (display
     (format #f "handle the new~%"))
    (display
     (format #f "syntax for define. You must also arrange "))
    (display
     (format #f "for eval or~%"))
    (display
     (format #f "apply to determine when arguments are to "))
    (display
     (format #f "be delayed, and~%"))
    (display
     (format #f "to force or delay arguments accordingly, "))
    (display
     (format #f "and you must~%"))
    (display
     (format #f "arrange for forcing to memoize or not, "))
    (display
     (format #f "as appropriate.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (experiment-fib)
    (newline)
    (experiment-count-primes)
    (newline)
    (experiment-eval-id-test)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.31 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
