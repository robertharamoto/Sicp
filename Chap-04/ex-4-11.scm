#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.11                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (enclosing-environment env)
  (begin
    (cdr env)
    ))

;;;#############################################################
;;;#############################################################
(define (first-frame env)
  (begin
    (car env)
    ))

;;;#############################################################
;;;#############################################################
(define the-empty-environment (list))

;;;#############################################################
;;;#############################################################
(define (make-frame vars-list vals-list)
  (begin
    (if (equal? (length vars-list) (length vals-list))
        (begin
          (let ((vlen (length vars-list))
                (frame-list (list)))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii vlen))
                (begin
                  (let ((avar (list-ref vars-list ii))
                        (avalue (list-ref vals-list ii)))
                    (begin
                      (set!
                       frame-list
                       (cons (list avar avalue) frame-list))
                      ))
                  ))

              frame-list
              )))
        (begin
          (if (<
               (length vars-list)
               (length vals-list))
              (begin
                (error
                 "Too many arguments supplied"
                 vars-list vals-list))
              (begin
                (error
                 "Too few arguments supplied"
                 vars-list vals-list)
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append
            err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append
                     err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-frame result-hash-table)
 (begin
   (let ((sub-name "test-make-frame")
         (test-list
          (list
           (list (list 'a) (list 10) (list (list 'a 10)))
           (list (list 'a 'b) (list 10 5) (list (list 'a 10) (list 'b 5)))
           (list (list 'a 'b 'c) (list 5 10 20)
                 (list (list 'a 5) (list 'b 10) (list 'c 20)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((vars (list-ref alist 0))
                  (vals (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((frame-list
                     (make-frame vars vals)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "vars=~a, vals=~a : "
                        vars vals)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list frame-list
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index  (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (add-binding-to-frame var val frame)
  (define (local-iter var val frame acc-list)
    (begin
      (if (null? frame)
          (begin
            (if (equal? (member (list var val) acc-list) #f)
                (begin
                  (cons (list var val) acc-list))
                (begin
                  acc-list
                  )))
          (begin
            (if (equal? (car (car frame)) var)
                (begin
                  (set! acc-list
                        (cons (list var val) acc-list)))
                (begin
                  (set! acc-list
                        (cons (car frame) acc-list))
                  ))

            (local-iter
             var val (cdr frame) acc-list)
            ))
      ))
  (begin
    (let ((result-frame
           (local-iter var val frame (list))))
      (begin
        result-frame
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-binding-to-frame result-hash-table)
 (begin
   (let ((sub-name "test-add-binding-to-frame")
         (test-list
          (list
           (list 'a 10 (list)
                 (list (list 'a 10)))
           (list 'a 10 (list (list 'b 5))
                 (list (list 'a 10) (list 'b 5)))
           (list 'a 20 (list (list 'a 10) (list 'b 5))
                 (list (list 'a 20) (list 'b 5)))
           (list 'b 20 (list (list 'a 10) (list 'b 5))
                 (list (list 'a 10) (list 'b 20)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (val (list-ref alist 1))
                  (init-frame-list (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((frame-list
                     (add-binding-to-frame
                      var val init-frame-list))
                    (err-msg-1
                     (format
                      #f "~a : error (~a) : "
                      sub-name test-label-index))
                    (err-msg-2
                     (format
                      #f "var=~a, val=~a, init-frame-list=~a : "
                      var val init-frame-list)))
                (begin
                  (assert-lists-are-equal?
                   shouldbe-list frame-list
                   sub-name
                   (string-append
                    err-msg-1 err-msg-2)
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (extend-environment vars vals base-env)
  (begin
    (if (= (length vars) (length vals))
        (begin
          (cons (make-frame vars vals) base-env))
        (begin
          (if (< (length vars) (length vals))
              (begin
                (error
                 "Too many arguments supplied" vars vals))
              (begin
                (error
                 "Too few arguments supplied" vars vals)
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lookup-variable-value var env)
  (define (env-loop env)
    (define (scan frame)
      (begin
        (cond
         ((null? frame)
          (begin
            (env-loop (enclosing-environment env))
            ))
         ((eq? var (car (car frame)))
          (begin
            (cadr (car frame))
            ))
         (else
          (begin
            (scan (cdr frame))
            )))
        ))
    (begin
      (if (eq? env the-empty-environment)
          (begin
            (error "Unbound variable" var))
          (begin
            (let ((frame (first-frame env)))
              (begin
                (scan frame)
                ))
            ))
      ))
  (begin
    (env-loop env)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lookup-variable-value result-hash-table)
 (begin
   (let ((sub-name "test-lookup-variable-value")
         (test-list
          (list
           (list 'a (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     the-empty-environment) 10)
           (list 'b (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     the-empty-environment) 11)
           (list 'c (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     the-empty-environment) 12)
           (list 'd (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     (extend-environment
                      (list 'd 'e 'f) (list 22 23 24)
                      the-empty-environment)) 22)
           (list 'e (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     (extend-environment
                      (list 'd 'e 'f) (list 22 23 24)
                      the-empty-environment)) 23)
           (list 'f (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     (extend-environment
                      (list 'd 'e 'f) (list 22 23 24)
                      the-empty-environment)) 24)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (env (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (lookup-variable-value var env)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : var=~a, env=~a : "
                        sub-name test-label-index var env))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (set-variable-value! var val env)
  (define (env-loop env)
    (define (scan frame)
      (begin
        (cond
         ((null? frame)
          (begin
            (env-loop (enclosing-environment env))
            ))
         ((eq? var (car (car frame)))
          (begin
            (set-car! frame (list var val))
            ))
         (else
          (begin
            (scan (cdr frame))
            )))
        ))
    (begin
      (if (eq? env the-empty-environment)
          (begin
            (error "Unbound variable -- SET!" var))
          (begin
            (let ((frame (first-frame env)))
              (begin
                (scan frame)
                ))
            ))
      ))
  (begin
    (env-loop env)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-set-variable-value result-hash-table)
 (begin
   (let ((sub-name "test-set-variable-value")
         (test-list
          (list
           (list 'a 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 20 11 12)
                  the-empty-environment))
           (list 'b 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 20 12)
                  the-empty-environment))
           (list 'c 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 20)
                  the-empty-environment))
           (list 'd 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 50 23 24)
                   the-empty-environment)))
           (list 'e 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 50 24)
                   the-empty-environment)))
           (list 'f 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 50)
                   the-empty-environment)))

           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (value (list-ref alist 1))
                  (env (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3))
                  (slen (length (list-ref alist 3))))
              (begin
                (set-variable-value! var value env)
                (let ((err-start
                       (format
                        #f "~a : error (~a) : var=~a, value=~a : "
                        sub-name test-label-index var value)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list env
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (define-variable var val env)
  (define (scan frame acc-list)
    (begin
      (cond
       ((or (null? frame) (null? (car frame)))
        (begin
          (if (equal? (member (list var val) acc-list) #f)
              (begin
                (cons (list var val) acc-list))
              (begin
                acc-list
                ))
          ))
       ((eq? var (car (car frame)))
        (begin
          (scan (cdr frame) (cons (list var val) acc-list))
          ))
       (else
        (begin
          (scan (cdr frame) (cons (car frame) acc-list))
          )))
      ))
  (begin
    (if (null? env)
        (begin
          (let ((frame
                 (make-frame (list var) (list val))))
            (let ((new-env (cons frame env)))
              (begin
                new-env
                ))
            ))
        (begin
          (let ((frame (first-frame env)))
            (let ((new-frame (scan frame (list))))
              (begin
                (set-car! env new-frame)
                env
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-environments-are-equal?
         shouldbe-environment result-environment
         sub-name err-start
         result-hash-table)
  (begin
    (let ((shouldbe-list
           (car shouldbe-environment))
          (result-list
           (car result-environment)))
      (let ((slen (length shouldbe-list))
            (rlen (length result-list)))
        (let ((err-msg-2
               (format
                #f "shouldbe=~a, result=~a, "
                shouldbe-list result-list))
              (err-msg-3
               (format
                #f "shouldbe length=~a, result length=~a"
                slen rlen)))
          (begin
            (unittest2:assert?
             (equal? slen rlen)
             sub-name
             (string-append
              err-start err-msg-2 err-msg-3)
             result-hash-table)

            (for-each
             (lambda (selem)
               (begin
                 (let ((sflag
                        (member selem result-list))
                       (err-msg-4
                        (format
                         #f "missing element ~a" selem)))
                   (begin
                     (unittest2:assert?
                      (not (equal? sflag #f))
                      sub-name
                      (string-append
                       err-start err-msg-2 err-msg-4)
                      result-hash-table)
                     ))
                 )) shouldbe-list)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-define-variable result-hash-table)
 (begin
   (let ((sub-name "test-define-variable")
         (test-list
          (list
           (list 'd 20 the-empty-environment
                 (extend-environment
                  (list 'd) (list 20)
                  the-empty-environment))
           (list 'd 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c 'd) (list 10 11 12 20)
                  the-empty-environment))
           (list 'a 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 20 11 12)
                  the-empty-environment))
           (list 'b 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 20 12)
                  the-empty-environment))
           (list 'c 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 20)
                  the-empty-environment))
           (list 'd 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c 'd)
                  (list 10 11 12 50)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment)))
           (list 'e 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c 'e) (list 10 11 12 50)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment)))
           (list 'f 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c 'f) (list 10 11 12 50)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (value (list-ref alist 1))
                  (env (list-ref alist 2))
                  (shouldbe-env (list-ref alist 3))
                  (slen (length (car (list-ref alist 3)))))
              (let ((result-env (define-variable var value env)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "var=~a, val=~a, env=~a : "
                        var value env)))
                  (begin
                    (assert-environments-are-equal?
                     shouldbe-env result-env
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Instead of representing a frame as a pair "))
    (display
     (format #f "of lists, we~%"))
    (display
     (format #f "can represent a frame as a list of "))
    (display
     (format #f "bindings, where each~%"))
    (display
     (format #f "binding is a name-value pair. Rewrite "))
    (display
     (format #f "the environment~%"))
    (display
     (format #f "operations to use this alternative "))
    (display
     (format #f "representation.~%"))
    (newline)
    (display
     (format #f "The environment operations were re-written, "))
    (display
     (format #f "with the~%"))
    (display
     (format #f "add-binding-to-frame and the define-variable "))
    (display
     (format #f "functions~%"))
    (display
     (format #f "changed so that the return values are a "))
    (display
     (format #f "new frame or~%"))
    (display
     (format #f "environment, instead of modifying the "))
    (display
     (format #f "original copy.~%"))
    (display
     (format #f "This was necessary to account for "))
    (display
     (format #f "special cases where~%"))
    (display
     (format #f "the environment was empty. Guile will "))
    (display
     (format #f "send a copy of~%"))
    (display
     (format #f "(list), and not a reference to the "))
    (display
     (format #f "(list), so certain~%"))
    (display
     (format #f "test-cases would not pass.~%"))
    (newline)
    (display
     (format #f "(define (make-frame "))
    (display
     (format #f "vars-list vals-list)~%"))
    (display
     (format #f "  (if (equal? "))
    (display
     (format #f "(length vars-list) (length vals-list))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (let ((vlen "))
    (display
     (format #f "(length vars-list))~%"))
    (display
     (format #f "              (frame-list (list)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "                ((>= ii vlen))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (let ((avar "))
    (display
     (format #f "(list-ref vars-list ii))~%"))
    (display
     (format #f "                      (avalue "))
    (display
     (format #f "(list-ref vals-list ii)))~%"))
    (display
     (format #f "                  (begin~%"))
    (display
     (format #f "                    (set! frame-list~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(cons (list avar avalue) frame-list))~%"))
    (display
     (format #f "                    ))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            frame-list~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (if (< (length vars-list) "))
    (display
     (format #f "(length vals-list))~%"))
    (display
     (format #f "            "))
    (display
     (format #f "(error \"Too many arguments supplied\"~%"))
    (display
     (format #f "    vars-list vals-list)~%"))
    (display
     (format #f "            "))
    (display
     (format #f "(error \"Too few arguments supplied\"~%"))
    (display
     (format #f "    vars-list vals-list))~%"))
    (display
     (format #f "        )))~%"))
    (newline)
    (display
     (format #f "(define (add-binding-to-frame "))
    (display
     (format #f "var val frame)~%"))
    (display
     (format #f "  (define (local-iter "))
    (display
     (format #f "var val frame acc-list)~%"))
    (display
     (format #f "    (if (null? frame)~%"))
    (display
     (format #f "        (if (equal? (member "))
    (display
     (format #f "(list var val) acc-list) #f)~%"))
    (display
     (format #f "            (cons "))
    (display
     (format #f "(list var val) acc-list)~%"))
    (display
     (format #f "            acc-list)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (if (equal? "))
    (display
     (format #f "(car (car frame)) var)~%"))
    (display
     (format #f "              (set! acc-list "))
    (display
     (format #f "(cons (list var val) acc-list))~%"))
    (display
     (format #f "              (set! acc-list "))
    (display
     (format #f "(cons (car frame) acc-list)))~%"))
    (display
     (format #f "          (local-iter var val "))
    (display
     (format #f "(cdr frame) acc-list)~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (let ((result-frame~%"))
    (display
     (format #f "         (local-iter var val "))
    (display
     (format #f "frame (list))))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      result-frame~%"))
    (display
     (format #f "      )))~%"))
    (newline)
    (display
     (format #f "(define (lookup-variable-value "))
    (display
     (format #f "var env)~%"))
    (display
     (format #f "  (define (env-loop env)~%"))
    (display
     (format #f "    (define (scan frame)~%"))
    (display
     (format #f "      (cond ((null? frame)~%"))
    (display
     (format #f "             (env-loop "))
    (display
     (format #f "(enclosing-environment env)))~%"))
    (display
     (format #f "            ((eq? var "))
    (display
     (format #f "(car (car frame)))~%"))
    (display
     (format #f "             (cadr (car frame)))~%"))
    (display
     (format #f "            (else "))
    (display
     (format #f "(scan (cdr frame)))))~%"))
    (display
     (format #f "    (if (eq? env "))
    (display
     (format #f "the-empty-environment)~%"))
    (display
     (format #f "        "))
    (display
     (format #f "(error \"Unbound variable\" var)~%"))
    (display
     (format #f "        (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "          (scan frame))))~%"))
    (display
     (format #f "  (env-loop env))~%"))
    (newline)
    (display
     (format #f "(define (set-variable-value! "))
    (display
     (format #f "var val env)~%"))
    (display
     (format #f "  (define (env-loop env)~%"))
    (display
     (format #f "    (define (scan frame)~%"))
    (display
     (format #f "      (cond ((null? frame)~%"))
    (display
     (format #f "             (env-loop "))
    (display
     (format #f "(enclosing-environment env)))~%"))
    (display
     (format #f "            ((eq? var "))
    (display
     (format #f "(car (car frame)))~%"))
    (display
     (format #f "             (set-car! "))
    (display
     (format #f "frame (list var val)))~%"))
    (display
     (format #f "            (else "))
    (display
     (format #f "(scan (cdr frame)))))~%"))
    (display
     (format #f "    (if (eq? env "))
    (display
     (format #f "the-empty-environment)~%"))
    (display
     (format #f "        "))
    (display
     (format #f "(error \"Unbound variable "))
    (display
     (format #f "-- SET!\" var)~%"))
    (display
     (format #f "        (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "          (scan frame))))~%"))
    (display
     (format #f "  (env-loop env))~%"))
    (newline)
    (display
     (format #f "(define (define-variable "))
    (display
     (format #f "var val env)~%"))
    (display
     (format #f "  (define (scan frame acc-list)~%"))
    (display
     (format #f "    (cond ((or (null? frame) "))
    (display
     (format #f "(null? (car frame)))~%"))
    (display
     (format #f "           (begin~%"))
    (display
     (format #f "             (if (equal? "))
    (display
     (format #f "(member (list var val) acc-list) #f)~%"))
    (display
     (format #f "                 (begin~%"))
    (display
     (format #f "                   (cons "))
    (display
     (format #f "(list var val) acc-list))~%"))
    (display
     (format #f "                 (begin~%"))
    (display
     (format #f "                   acc-list~%"))
    (display
     (format #f "                   ))~%"))
    (display
     (format #f "             ))~%"))
    (display
     (format #f "          ((eq? var "))
    (display
     (format #f "(car (car frame)))~%"))
    (display
     (format #f "           (begin~%"))
    (display
     (format #f "             (scan (cdr frame) "))
    (display
     (format #f "(cons (list var val) acc-list))~%"))
    (display
     (format #f "             ))~%"))
    (display
     (format #f "          (else~%"))
    (display
     (format #f "           (begin~%"))
    (display
     (format #f "             (scan (cdr frame) "))
    (display
     (format #f "(cons (car frame) acc-list))~%"))
    (display
     (format #f "             ))))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (if (null? env)~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((frame "))
    (display
     (format #f "(make-frame (list var) (list val))))~%"))
    (display
     (format #f "            (let ((new-env "))
    (display
     (format #f "(cons frame env)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                new-env~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "            (let ((new-frame "))
    (display
     (format #f "(scan frame (list))))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (set-car! "))
    (display
     (format #f "env new-frame)~%"))
    (display
     (format #f "                env~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "    ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.11 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
