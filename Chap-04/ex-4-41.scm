#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.41                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (is-config-ok? a-list-list)
  (begin
    (let ((baker-floor #f)
          (cooper-floor #f)
          (fletcher-floor #f)
          (miller-floor #f)
          (smith-floor #f)
          (ok-flag #t))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((asym (list-ref alist 0))
                   (afloor (list-ref alist 1)))
               (begin
                 (cond
                  ((eq? asym 'baker)
                   (begin
                     (set! baker-floor afloor)
                     ))
                  ((eq? asym 'cooper)
                   (begin
                     (set! cooper-floor afloor)
                     ))
                  ((eq? asym 'fletcher)
                   (begin
                     (set! fletcher-floor afloor)
                     ))
                  ((eq? asym 'miller)
                   (begin
                     (set! miller-floor afloor)
                     ))
                  ((eq? asym 'smith)
                   (begin
                     (set! smith-floor afloor)
                     ))
                  (else
                   (begin
                     (display
                      (format
                       #f
                       (string-append
                        "is-config-ok error: unknown "
                        "symbol = ~a, list=~a~%")
                       asym a-list-list))
                     (quit)
                     )))
                 ))
             )) a-list-list)

        (cond
         ((or (equal? baker-floor #f)
              (equal? cooper-floor #f)
              (equal? fletcher-floor #f)
              (equal? miller-floor #f)
              (equal? smith-floor #f))
          (begin
            (set! ok-flag #f)
            ))
         ((or (equal? fletcher-floor 1)
              (equal? fletcher-floor 5)
              (equal? cooper-floor 1)
              (<= miller-floor cooper-floor)
              (equal? baker-floor 5)
              (<= (abs (- fletcher-floor cooper-floor)) 1)
              (<= (abs (- smith-floor fletcher-floor)) 1))
          (begin
            (set! ok-flag #f)
            )))

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-config-ok result-hash-table)
 (begin
   (let ((sub-name "test-is-config-ok")
         (test-list
          (list
           (list (list (list 'baker 3) (list 'cooper 2)
                       (list 'fletcher 4) (list 'miller 5)
                       (list 'smith 1)) #t)
           (list (list (list 'baker 3) (list 'cooper 5)
                       (list 'fletcher 2) (list 'miller 4)
                       (list 'smith 1)) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((floor-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (is-config-ok? floor-list)))
                (let ((err-msg-1
                       (format
                        #f "~a (~a) error : floor list=~a : "
                        sub-name test-label-index floor-list))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (floor-not-in-list? afloor a-list-list)
  (begin
    (let ((ok-flag #t)
          (llen (length a-list-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii llen)
                 (equal? ok-flag #f)))
          (begin
            (let ((apair (list-ref a-list-list ii)))
              (let ((this-name (car apair))
                    (this-floor (cadr apair)))
                (begin
                  (if (equal? this-floor afloor)
                      (begin
                        (set! ok-flag #f)
                        ))
                  )))
            ))
        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-floor-not-in-list result-hash-table)
 (begin
   (let ((sub-name "test-floor-not-in-list")
         (test-list
          (list
           (list 5 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #t)
           (list 1 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #t)
           (list 2 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #f)
           (list 3 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #f)
           (list 4 (list (list 'baker 3) (list 'cooper 2)
                         (list 'fletcher 4)) #f)
           (list 6 (list (list 'baker 3) (list 'cooper 5)
                         (list 'fletcher 2) (list 'miller 4)
                         (list 'smith 1)) #t)
           (list 1 (list (list 'baker 3) (list 'cooper 5)
                         (list 'fletcher 2) (list 'miller 4)
                         (list 'smith 1)) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((afloor (list-ref alist 0))
                  (floor-list-list (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (floor-not-in-list? afloor floor-list-list)))
                (let ((err-msg-1
                       (format
                        #f "~a (~a) error : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "floor ~a, floor list=~a : "
                        afloor floor-list-list))
                      (err-msg-3
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2 err-msg-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (enumerate-configurations a-list-list)
  (define (local-iter input-list-list curr-list acc-list-list)
    (begin
      (if (null? input-list-list)
          (begin
            (cons (reverse curr-list) acc-list-list))
          (begin
            (let ((this-trial (car input-list-list))
                  (tail-list (cdr input-list-list)))
              (let ((name (car this-trial))
                    (floor-list (cdr this-trial)))
                (begin
                  (for-each
                   (lambda (afloor)
                     (begin
                       (if (floor-not-in-list? afloor curr-list)
                           (begin
                             (let ((next-acc-list
                                    (local-iter
                                     tail-list
                                     (cons (list name afloor) curr-list)
                                     acc-list-list)))
                               (begin
                                 (set! acc-list-list next-acc-list)
                                 ))
                             ))
                       )) floor-list)

                  acc-list-list
                  )))
            ))
      ))
  (begin
    (let ((result-list-list
           (reverse
            (local-iter a-list-list (list) (list)))))
      (begin
        result-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append
            err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append
                     err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-enumerate-configurations result-hash-table)
 (begin
   (let ((sub-name "test-enumerate-configurations")
         (test-list
          (list
           (list
            (list (list 'baker 1))
            (list (list (list 'baker 1))))
           (list
            (list (list 'baker 1 2)
                  (list 'cooper 1 2))
            (list (list (list 'baker 1) (list 'cooper 2))
                  (list (list 'baker 2) (list 'cooper 1))))
           (list
            (list (list 'baker 1 2 3)
                  (list 'cooper 1 2 3)
                  (list 'fletcher 1 2 3))
            (list (list (list 'baker 1)
                        (list 'cooper 2)
                        (list 'fletcher 3))
                  (list (list 'baker 1)
                        (list 'cooper 3)
                        (list 'fletcher 2))
                  (list (list 'baker 2)
                        (list 'cooper 1)
                        (list 'fletcher 3))
                  (list (list 'baker 2)
                        (list 'cooper 3)
                        (list 'fletcher 1))
                  (list (list 'baker 3)
                        (list 'cooper 1)
                        (list 'fletcher 2))
                  (list (list 'baker 3)
                        (list 'cooper 2)
                        (list 'fletcher 1))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (enumerate-configurations input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input floor list=~a : "
                        input-list)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list-list result-list-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Write an ordinary Scheme program to "))
    (display
     (format #f "solve the multiple~%"))
    (display
     (format #f "dwelling puzzle.~%"))
    (newline)
    (display
     (format #f "There are 5! = 120 possible distinct "))
    (display
     (format #f "floor configurations,~%"))
    (display
     (format #f "so a straight-forward enumeration of "))
    (display
     (format #f "all possible~%"))
    (display
     (format #f "configurations was "))
    (display
     (format #f "performed.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((possible-list-list
           (list
            (list 'baker 1 2 3 4 5)
            (list 'cooper 1 2 3 4 5)
            (list 'fletcher 1 2 3 4 5)
            (list 'miller 1 2 3 4 5)
            (list 'smith 1 2 3 4 5))))
      (let ((floor-list-list
             (enumerate-configurations possible-list-list))
            (md-count 0))
        (begin
          (for-each
           (lambda (aconfig)
             (begin
               (if (is-config-ok? aconfig)
                   (begin
                     (set! md-count (1+ md-count))
                     (display
                      (format
                       #f "(~:d)  ~a~%" md-count aconfig))
                     (force-output)
                     ))
               )) floor-list-list)

          (display
           (ice-9-format:format
            #f "found a total of ~:d solutions~%"
            md-count))
          (newline)
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.41 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
