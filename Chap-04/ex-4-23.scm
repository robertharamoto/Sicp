#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.23                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Alyssa P. Hacker doesn't understand why "))
    (display
     (format #f "analyze-sequence~%"))
    (display
     (format #f "needs to be so complicated. All the "))
    (display
     (format #f "other analysis~%"))
    (display
     (format #f "procedures are straightforward "))
    (display
     (format #f "transformations of the~%"))
    (display
     (format #f "corresponding evaluation procedures (or "))
    (display
     (format #f "eval clauses) in~%"))
    (display
     (format #f "in section 4.1.1. She expected "))
    (display
     (format #f "analyze-sequence to look~%"))
    (display
     (format #f "like this:~%"))
    (newline)
    (display
     (format #f "(define (analyze-sequence exps)~%"))
    (display
     (format #f "  (define (execute-sequence procs env)~%"))
    (display
     (format #f "    (cond ((null? (cdr procs)) "))
    (display
     (format #f "((car procs) env))~%"))
    (display
     (format #f "          (else ((car procs) env)~%"))
    (display
     (format #f "                (execute-sequence "))
    (display
     (format #f "(cdr procs) env))))~%"))
    (display
     (format #f "  (let ((procs (map analyze exps)))~%"))
    (display
     (format #f "    (if (null? procs)~%"))
    (display
     (format #f "        (error \"Empty sequence "))
    (display
     (format #f "-- ANALYZE\"))~%"))
    (display
     (format #f "    (lambda (env) "))
    (display
     (format #f "(execute-sequence procs env))))~%"))
    (newline)
    (display
     (format #f "Eva Lu Ator explains to Alyssa that the "))
    (display
     (format #f "version in the~%"))
    (display
     (format #f "text does more of the work of evaluating "))
    (display
     (format #f "a sequence at~%"))
    (display
     (format #f "analysis time. Alyssa's sequence-execution "))
    (display
     (format #f "procedure, rather~%"))
    (display
     (format #f "than having the calls to the individual "))
    (display
     (format #f "execution procedures~%"))
    (display
     (format #f "built in, loops through the procedures in "))
    (display
     (format #f "order to call~%"))
    (display
     (format #f "them: In effect, although the individual "))
    (display
     (format #f "expressions in the~%"))
    (display
     (format #f "sequence have been analyzed, the sequence "))
    (display
     (format #f "itself has not~%"))
    (display
     (format #f "been. Compare the two versions of "))
    (display
     (format #f "analyze-sequence. For~%"))
    (display
     (format #f "example, consider the common case "))
    (display
     (format #f "(typical of procedure~%"))
    (display
     (format #f "bodies), where the sequence has just one "))
    (display
     (format #f "expression. What~%"))
    (display
     (format #f "work will the execution procedure "))
    (display
     (format #f "by Allyssa's program~%"))
    (display
     (format #f "do? What about the execution procedure "))
    (display
     (format #f "produced by the~%"))
    (display
     (format #f "program in the text above? How do the "))
    (display
     (format #f "two versions compare~%"))
    (display
     (format #f "for a sequence with two expression?~%"))
    (newline)
    (display
     (format #f "In the case of Alyssa's version of "))
    (display
     (format #f "analyze sequence,~%"))
    (display
     (format #f "one-line of code will get analyzed, "))
    (display
     (format #f "and a function is~%"))
    (display
     (format #f "returned that will call execute-sequence "))
    (display
     (format #f "to run it.~%"))
    (display
     (format #f "Similary, the textbook's version of "))
    (display
     (format #f "the analyze-sequence~%"))
    (display
     (format #f "function will call analyze on the "))
    (display
     (format #f "one-line of code~%"))
    (display
     (format #f "before returning the result for "))
    (display
     (format #f "execution.~%"))
    (newline)
    (display
     (format #f "In the case of two lines of code, "))
    (display
     (format #f "Alyssa's version will~%"))
    (display
     (format #f "recursively loop through both lines and "))
    (display
     (format #f "evaluate each~%"))
    (display
     (format #f "sequentially when evaluated. The texbook's "))
    (display
     (format #f "version will~%"))
    (display
     (format #f "build up a single lambda function that can "))
    (display
     (format #f "execute each~%"))
    (display
     (format #f "line, in order, without the need for "))
    (display
     (format #f "recursion,~%"))
    (display
     (format #f "effectively unrolling the loop.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.23 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
