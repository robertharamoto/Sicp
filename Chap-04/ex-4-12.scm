#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.12                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (enclosing-environment env)
  (begin
    (cdr env)
    ))

;;;#############################################################
;;;#############################################################
(define (first-frame env)
  (begin
    (car env)
    ))

;;;#############################################################
;;;#############################################################
(define the-empty-environment (list))

;;;#############################################################
;;;#############################################################
(define (make-frame variables values)
  (begin
    (cons variables values)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-variables frame)
  (begin
    (car frame)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-values frame)
  (begin
    (cdr frame)
    ))

;;;#############################################################
;;;#############################################################
(define (add-binding-to-frame! var val frame)
  (begin
    (set-car! frame (cons var (car frame)))
    (set-cdr! frame (cons val (cdr frame)))
    ))

;;;#############################################################
;;;#############################################################
(define (extend-environment vars vals base-env)
  (begin
    (if (= (length vars) (length vals))
        (begin
          (cons (make-frame vars vals) base-env))
        (begin
          (if (< (length vars) (length vals))
              (begin
                (error
                 "Too many arguments supplied"
                 vars vals))
              (begin
                (error
                 "Too few arguments supplied"
                 vars vals)
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (scan
         var val variables-list values-list
         env null-func found-func)
  (begin
    (cond
     ((null? variables-list)
      (begin
        (null-func
         var val variables-list values-list env)
        ))
     ((eq? var (car variables-list))
      (begin
        (found-func
         var val variables-list values-list env)
        ))
     (else
      (begin
        (scan
         var val
         (cdr variables-list) (cdr values-list)
         env null-func found-func)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (env-loop
         var val env
         null-func found-func calling-string)
  (begin
    (if (eq? env the-empty-environment)
        (begin
          (error calling-string var))
        (begin
          (let ((frame (first-frame env)))
            (begin
              (scan
               var val
               (frame-variables frame)
               (frame-values frame)
               env null-func found-func)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lookup-variable-value var env)
  (define (a-found-func var val vars-list vals-list env)
    (begin
      (car vals-list)
      ))
  (define (a-null-func var val vars-list vals-list env)
    (begin
      (let ((enc-env
             (enclosing-environment env)))
        (begin
          (env-loop
           var val enc-env
           a-null-func a-found-func
           "Unbound variable -- lookup-variable-value")
          ))
      ))
  (begin
    (env-loop
     var #f env
     a-null-func a-found-func
     "Unbound variable -- lookup-variable-value")
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lookup-variable-value result-hash-table)
 (begin
   (let ((sub-name "test-lookup-variable-value")
         (test-list
          (list
           (list 'a (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     the-empty-environment) 10)
           (list 'b (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     the-empty-environment) 11)
           (list 'c (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     the-empty-environment) 12)
           (list 'd (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     (extend-environment
                      (list 'd 'e 'f) (list 22 23 24)
                      the-empty-environment)) 22)
           (list 'e (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     (extend-environment
                      (list 'd 'e 'f) (list 22 23 24)
                      the-empty-environment)) 23)
           (list 'f (extend-environment
                     (list 'a 'b 'c) (list 10 11 12)
                     (extend-environment
                      (list 'd 'e 'f) (list 22 23 24)
                      the-empty-environment)) 24)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (env (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result (lookup-variable-value var env)))
                (let ((err-msg-1
                       (format
                        #f "~a (~a) error : var=~a, env=~a : "
                        sub-name test-label-index var env))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, frame = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (set-variable-value! var val env)
  (define (a-found-func var val vars-list vals-list env)
    (begin
      (set-car! vals-list val)
      ))
  (define (a-null-func var val vars-list vals-list env)
    (begin
      (env-loop
       var val (enclosing-environment env)
       a-null-func a-found-func
       "Unbound variable -- set!")
      ))
  (begin
    (env-loop
     var val env
     a-null-func a-found-func
     "Unbound variable -- set!")
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append
            err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-set-variable-value result-hash-table)
 (begin
   (let ((sub-name "test-set-variable-value")
         (test-list
          (list
           (list 'a 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 20 11 12)
                  the-empty-environment))
           (list 'b 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 20 12)
                  the-empty-environment))
           (list 'c 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 20)
                  the-empty-environment))
           (list 'd 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 50 23 24)
                   the-empty-environment)))
           (list 'e 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 50 24)
                   the-empty-environment)))
           (list 'f 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 50)
                   the-empty-environment)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (value (list-ref alist 1))
                  (env (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3))
                  (slen (length (list-ref alist 3))))
              (begin
                (set-variable-value! var value env)
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "var=~a, val=~a : "
                        var value)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list env
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (define-variable! var val env)
  (begin
    (if (or (null? env) (null? (car env)))
        (begin
          (let ((frame
                 (make-frame (list var) (list val))))
            (begin
              (error
               "null environment for define-variable!" env)
              )))
        (begin
          (let ((frame (first-frame env)))
            (let ((vars-list (frame-variables frame))
                  (values-list (frame-values frame)))
              (begin
                (scan
                 var val
                 vars-list values-list env
                 (lambda (var val vars-list values-list env)
                   (begin
                     (let ((frame (first-frame env)))
                       (begin
                         (add-binding-to-frame! var val frame)
                         ))
                     ))
                 (lambda (var val vars-list values-list env)
                   (begin
                     (set-car! values-list val)
                     )))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-define-variable result-hash-table)
 (begin
   (let ((sub-name "test-define-variable")
         (test-list
          (list
           (list 'd 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'd 'a 'b 'c) (list 20 10 11 12)
                  the-empty-environment))
           (list 'a 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 20 11 12)
                  the-empty-environment))
           (list 'b 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 20 12)
                  the-empty-environment))
           (list 'c 20
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 20)
                  the-empty-environment))
           (list 'd 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'd 'a 'b 'c) (list 50 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment)))
           (list 'e 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'e 'a 'b 'c) (list 50 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment)))
           (list 'f 50
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'f 'a 'b 'c) (list 50 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (value (list-ref alist 1))
                  (env (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3))
                  (slen (length (list-ref alist 3))))
              (begin
                (define-variable! var value env)
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-msg-2
                       (format
                        #f "var=~a, val=~a, env=~a : "
                        var value env)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list env
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The procedures set-variable-value!, "))
    (display
     (format #f "define-variable!, and~%"))
    (display
     (format #f "lookup-variable-value can be expressed in "))
    (display
     (format #f "terms of more~%"))
    (display
     (format #f "abstract procedures for traversing the "))
    (display
     (format #f "environment~%"))
    (display
     (format #f "structure. Define abstractions that "))
    (display
     (format #f "capture the common~%"))
    (display
     (format #f "patterns and redefine the three procedures "))
    (display
     (format #f "in terms of~%"))
    (display
     (format #f "these abstractions.~%"))
    (newline)
    (display
     (format #f "(define (scan var val "))
    (display
     (format #f "variables-list values-list~%"))
    (display
     (format #f "              env null-func "))
    (display
     (format #f "found-func)~%"))
    (display
     (format #f "  (cond ((null? variables-list)~%"))
    (display
     (format #f "         (null-func var val "))
    (display
     (format #f "variables-list values-list env))~%"))
    (display
     (format #f "        ((eq? var "))
    (display
     (format #f "(car variables-list))~%"))
    (display
     (format #f "         (found-func var val "))
    (display
     (format #f "variables-list values-list env))~%"))
    (display
     (format #f "        (else (scan var val~%"))
    (display
     (format #f "                    (cdr variables-list) "))
    (display
     (format #f "(cdr values-list)~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "env null-func found-func))))~%"))
    (display
     (format #f "(define (env-loop var val env~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "null-func found-func calling-string)~%"))
    (display
     (format #f "  (if (eq? env "))
    (display
     (format #f "the-empty-environment)~%"))
    (display
     (format #f "      (error calling-string "))
    (display
     (format #f "var)~%"))
    (display
     (format #f "      (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "        (scan var val~%"))
    (display
     (format #f "              "))
    (display
     (format #f "(frame-variables frame)~%"))
    (display
     (format #f "              "))
    (display
     (format #f "(frame-values frame)~%"))
    (display
     (format #f "              "))
    (display
     (format #f "env null-func found-func))))~%"))
    (newline)
    (display
     (format #f "(define (lookup-variable-value "))
    (display
     (format #f "var env)~%"))
    (display
     (format #f "  (define (a-found-func "))
    (display
     (format #f "var val vars-list vals-list env)~%"))
    (display
     (format #f "    (car vals-list))~%"))
    (display
     (format #f "  (define (a-null-func "))
    (display
     (format #f "var val vars-list vals-list env)~%"))
    (display
     (format #f "    (let ((enc-env "))
    (display
     (format #f "(enclosing-environment env)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (env-loop~%"))
    (display
     (format #f "         var val enc-env~%"))
    (display
     (format #f "         a-null-func a-found-func~%"))
    (display
     (format #f "         \"Unbound variable -- "))
    (display
     (format #f "lookup-variable-value\")~%"))
    (display
     (format #f "        )))~%"))
    (display
     (format #f "  (env-loop~%"))
    (display
     (format #f "   var #f env~%"))
    (display
     (format #f "   a-null-func a-found-func~%"))
    (display
     (format #f "   \"Unbound variable -- "))
    (display
     (format #f "lookup-variable-value\"~%"))
    (display
     (format #f "   ))~%"))
    (newline)
    (display
     (format #f "(define (set-variable-value! "))
    (display
     (format #f "var val env)~%"))
    (display
     (format #f "  (define (a-found-func var val "))
    (display
     (format #f "vars-list vals-list env)~%"))
    (display
     (format #f "    (set-car! "))
    (display
     (format #f "vals-list val))~%"))
    (display
     (format #f "  (define (a-null-func var val "))
    (display
     (format #f "vars-list vals-list env)~%"))
    (display
     (format #f "    (env-loop var val "))
    (display
     (format #f "(enclosing-environment env)~%"))
    (display
     (format #f "              "))
    (display
     (format #f "a-null-func a-found-func~%"))
    (display
     (format #f "              \"Unbound "))
    (display
     (format #f "variable -- set!\"))~%"))
    (display
     (format #f "  (env-loop~%"))
    (display
     (format #f "   var val env~%"))
    (display
     (format #f "   a-null-func a-found-func~%"))
    (display
     (format #f "   \"Unbound variable "))
    (display
     (format #f "-- set!\"))~%"))
    (newline)
    (display
     (format #f "(define (define-variable! "))
    (display
     (format #f "var val env)~%"))
    (display
     (format #f "  (if (or (null? env) "))
    (display
     (format #f "(null? (car env)))~%"))
    (display
     (format #f "      (error \"null environment "))
    (display
     (format #f "for define-variable!\" env)~%"))
    (display
     (format #f "      (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "        (let ((vars-list "))
    (display
     (format #f "(frame-variables frame))~%"))
    (display
     (format #f "              (values-list "))
    (display
     (format #f "(frame-values frame)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (scan~%"))
    (display
     (format #f "             var val~%"))
    (display
     (format #f "             vars-list "))
    (display
     (format #f "values-list env~%"))
    (display
     (format #f "             "))
    (display
     (format #f "(lambda (var val "))
    (display
     (format #f "vars-list values-list env)~%"))
    (display
     (format #f "               (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "                 "))
    (display
     (format #f "(add-binding-to-frame! var val frame)))~%"))
    (display
     (format #f "             (lambda (var val "))
    (display
     (format #f "vars-list values-list env)~%"))
    (display
     (format #f "               "))
    (display
     (format #f "(set-car! values-list val)))~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.12 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
