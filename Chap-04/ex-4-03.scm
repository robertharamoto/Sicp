#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.03                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (eval exp env)
  (begin
    (cond
     ((self-evaluating? exp)
      (begin
        exp
        ))
     ((variable? exp)
      (begin
        (lookup-variable-value exp env)
        ))
     ((quoted? exp)
      (begin
        (text-of-quotation exp)
        ))
     ((assignment? exp)
      (begin
        (eval-assignment exp env)
        ))
     ((definition? exp)
      (begin
        (eval-definition exp env)
        ))
     ((if? exp)
      (begin
        (eval-if exp env)
        ))
     ((lambda? exp)
      (begin
        (make-procedure
         (lambda-parameters exp)
         (lambda-body exp)
         env)
        ))
     ((begin? exp)
      (begin
        (eval-sequence
         (begin-actions exp) env)
        ))
     ((cond? exp)
      (begin
        (eval (cond->if exp) env)
        ))
     ((application? exp)
      (begin
        (apply (eval (operator exp) env)
               (list-of-values (operands exp) env))
        ))
     (else
      (begin
        (error "Unknown expression type -- EVAL" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Rewrite eval so that the dispatch is "))
    (display
     (format #f "done in data-directed~%"))
    (display
     (format #f "style. Compare this with the "))
    (display
     (format #f "data-directed differentiation~%"))
    (display
     (format #f "procedure of exercise 2.73. (You may use "))
    (display
     (format #f "the car of a~%"))
    (display
     (format #f "compound expression as the type of the "))
    (display
     (format #f "expression, as~%"))
    (display
     (format #f "is appropriate for the syntax implemented "))
    (display
     (format #f "in this section.)~%"))
    (newline)
    (display
     (format #f "(define (make-lambda-procedure "))
    (display
     (format #f "exp env)~%"))
    (display
     (format #f "  (make-procedure "))
    (display
     (format #f "(lambda-parameters exp)~%"))
    (display
     (format #f "                  (lambda-body "))
    (display
     (format #f "exp)~%"))
    (display
     (format #f "                  env))~%"))
    (display
     (format #f "(define (make-begin exp env)~%"))
    (display
     (format #f "  (eval-sequence (begin-actions "))
    (display
     (format #f "exp) env))~%"))
    (display
     (format #f "(define (make-cond exp env)~%"))
    (display
     (format #f "  (eval (cond->if exp) env))~%"))
    (display
     (format #f "(define (make-apply exp env)~%"))
    (display
     (format #f "  (apply (eval "))
    (display
     (format #f "(operator exp) env)~%"))
    (display
     (format #f "       (list-of-values "))
    (display
     (format #f "(operands exp) env)))~%"))
    (newline)
    (display
     (format #f "(put 'lookup-variable-value "))
    (display
     (format #f "'(exp env) lookup-variable-value)~%"))
    (display
     (format #f "(put 'quoted '(exp) text-quotation)~%"))
    (display
     (format #f "(put 'assignment '(exp env) "))
    (display
     (format #f "eval-assignment)~%"))
    (display
     (format #f "(put 'definition '(exp env) "))
    (display
     (format #f "eval-definition)~%"))
    (display
     (format #f "(put 'if '(exp env) eval-if)~%"))
    (display
     (format #f "(put 'make-lambda '(exp env) "))
    (display
     (format #f "make-lambda-procedure)~%"))
    (display
     (format #f "(put 'make-begin '(exp env) "))
    (display
     (format #f "make-begin)~%"))
    (display
     (format #f "(put 'make-cond '(exp env) "))
    (display
     (format #f "make-cond)~%"))
    (display
     (format #f "(put 'make-apply '(exp env) "))
    (display
     (format #f "make-apply~%"))
    (newline)
    (display
     (format #f "(define (eval exp env)~%"))
    (display
     (format #f "  (cond ((self-evaluating? "))
    (display
     (format #f "exp) exp)~%"))
    (display
     (format #f "        ((variable? exp) "))
    (display
     (format #f "((get lookup-variable-value~%"))
    (display
     (format #f "            '(exp env)) exp env))~%"))
    (display
     (format #f "        ((quoted? exp) "))
    (display
     (format #f "((get 'quoted '(exp)) exp))~%"))
    (display
     (format #f "        ((assignment? exp) "))
    (display
     (format #f "((get 'assignment '(exp env)) "))
    (display
     (format #f "exp env))~%"))
    (display
     (format #f "        ((definition? exp) "))
    (display
     (format #f "((get 'definition '(exp env)) "))
    (display
     (format #f "exp env))~%"))
    (display
     (format #f "        ((if? exp) ((get "))
    (display
     (format #f "'if '(exp env)) exp env))~%"))
    (display
     (format #f "        ((lambda? exp) ((get "))
    (display
     (format #f "'make-lambda '(exp env)) exp env))~%"))
    (display
     (format #f "        ((begin? exp) ((get "))
    (display
     (format #f "'make-begin '(exp env)) exp env))~%"))
    (display
     (format #f "        ((cond? exp) ((get "))
    (display
     (format #f "'make-cond '(exp env)) exp env))~%"))
    (display
     (format #f "        ((application? exp) "))
    (display
     (format #f "((get 'make-apply '(exp env)) exp env))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         "))
    (display
     (format #f "(error \"Unknown expression type "))
    (display
     (format #f "-- EVAL\" exp))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.03 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
