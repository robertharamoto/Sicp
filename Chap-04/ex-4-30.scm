#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.30                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Cy D. Fect, a reformed C programmer, is "))
    (display
     (format #f "worried that some~%"))
    (display
     (format #f "side effects may never take place, "))
    (display
     (format #f "because the lazy~%"))
    (display
     (format #f "evaluator doesn't force the expressions "))
    (display
     (format #f "in a sequence.~%"))
    (display
     (format #f "Since the value of an expression in a "))
    (display
     (format #f "sequence other than~%"))
    (display
     (format #f "the last one is not used (the expression "))
    (display
     (format #f "is there only~%"))
    (display
     (format #f "for its effect, such as assigning to a "))
    (display
     (format #f "variable or~%"))
    (display
     (format #f "printing), there can be no subsequent use "))
    (display
     (format #f "of this value~%"))
    (display
     (format #f "(e.g., as an argument to a primitive "))
    (display
     (format #f "procedure), that will~%"))
    (display
     (format #f "cause it to be forced. Cy thus thinks that "))
    (display
     (format #f "when evaluating~%"))
    (display
     (format #f "sequences, we must force all expressions "))
    (display
     (format #f "in the sequence~%"))
    (display
     (format #f "except the final one. He proposes to "))
    (display
     (format #f "modify eval-sequence~%"))
    (display
     (format #f "from section 4.1.1 to use actual-value "))
    (display
     (format #f "rather than eval:~%"))
    (newline)
    (display
     (format #f "(define (eval-sequence exps env)~%"))
    (display
     (format #f "  (cond ((last-exp? exps) "))
    (display
     (format #f "(eval (first-exp exps) env))~%"))
    (display
     (format #f "        (else (actual-value "))
    (display
     (format #f "(first-exp exps) env)~%"))
    (display
     (format #f "              (eval-sequence "))
    (display
     (format #f "(rest-exps exps) env))))~%"))
    (newline)
    (display
     (format #f "a. Ben Bitdiddle thinks Cy is wrong. "))
    (display
     (format #f "He shows Cy the~%"))
    (display
     (format #f "for-each procedure described in exercise "))
    (display
     (format #f "2.23, which~%"))
    (display
     (format #f "gives an important example of a sequence "))
    (display
     (format #f "with side~%"))
    (display
     (format #f "effects:~%"))
    (newline)
    (display
     (format #f "(define (for-each proc items)~%"))
    (display
     (format #f "  (if (null? items)~%"))
    (display
     (format #f "      'done~%"))
    (display
     (format #f "      (begin (proc (car items))~%"))
    (display
     (format #f "             (for-each proc "))
    (display
     (format #f "(cdr items)))))~%"))
    (newline)
    (display
     (format #f "He claims that the evaluator in the "))
    (display
     (format #f "text (with the~%"))
    (display
     (format #f "original eval-sequence) handles this "))
    (display
     (format #f "correctly:~%"))
    (newline)
    (display
     (format #f ";;; L-Eval input:~%"))
    (display
     (format #f "(for-each (lambda (x) "))
    (display
     (format #f "(newline)~%"))
    (display
     (format #f "  (display x))~%"))
    (display
     (format #f "          (list 57 321 88))~%"))
    (display
     (format #f "57~%"))
    (display
     (format #f "321~%"))
    (display
     (format #f "88~%"))
    (display
     (format #f ";;; L-Eval value:~%"))
    (display
     (format #f "done~%"))
    (newline)
    (display
     (format #f "Explain why Ben is right about the "))
    (display
     (format #f "behavior of for-each.~%"))
    (display
     (format #f "b. Cy agrees that Ben is right about "))
    (display
     (format #f "the for-each~%"))
    (display
     (format #f "example, but says that that's not the "))
    (display
     (format #f "kind of program~%"))
    (display
     (format #f "he was thinking about when he proposed "))
    (display
     (format #f "his change to~%"))
    (display
     (format #f "eval-sequence. He defines the following "))
    (display
     (format #f "two procedures~%"))
    (display
     (format #f "in the lazy evaluator:~%"))
    (newline)
    (display
     (format #f "(define (p1 x)~%"))
    (display
     (format #f "  (set! x (cons x '(2)))~%"))
    (display
     (format #f "  x)~%"))
    (display
     (format #f "(define (p2 x)~%"))
    (display
     (format #f "  (define (p e)~%"))
    (display
     (format #f "    e~%"))
    (display
     (format #f "    x)~%"))
    (display
     (format #f "  (p (set! x (cons x '(2)))))~%"))
    (newline)
    (display
     (format #f "What are the values of (p1 1) and "))
    (display
     (format #f "(p2 1) with the~%"))
    (display
     (format #f "original eval-sequence? What would the "))
    (display
     (format #f "values be with~%"))
    (display
     (format #f "Cy's proposed change to "))
    (display
     (format #f "eval-sequence?~%"))
    (display
     (format #f "c. Cy also points out that changing "))
    (display
     (format #f "eval-sequence as~%"))
    (display
     (format #f "he proposes does not affect the behavior "))
    (display
     (format #f "of the example~%"))
    (display
     (format #f "in part a. Explain why this is true.~%"))
    (display
     (format #f "d. How do you think sequences ought to "))
    (display
     (format #f "be treated in~%"))
    (display
     (format #f "the lazy evaluator? Do you like Cy's "))
    (display
     (format #f "approach, the~%"))
    (display
     (format #f "approach in the text, or some other "))
    (display
     (format #f "approach?~%"))
    (newline)
    (display
     (format #f "(a)~%"))
    (display
     (format #f "Ben Bitdiddle is right, since in the "))
    (display
     (format #f "for-each procedure,~%"))
    (display
     (format #f "we have a proc which is applied to each "))
    (display
     (format #f "element of a~%"))
    (display
     (format #f "delayed list. However, once eval function "))
    (display
     (format #f "starts evaluating~%"))
    (display
     (format #f "the for-each procedure and considers "))
    (display
     (format #f "the (null? items)~%"))
    (display
     (format #f "expression, this is when the delayed "))
    (display
     (format #f "list (items),~%"))
    (display
     (format #f "gets forced into a list, since null? is "))
    (display
     (format #f "a primitive~%"))
    (display
     (format #f "procedure and the arguments are retrieved "))
    (display
     (format #f "using actual-value.~%"))
    (newline)
    (display
     (format #f "(b)~%"))
    (display
     (format #f "Using the original eval-sequence, (p1 1) "))
    (display
     (format #f "-> (1 2),~%"))
    (display
     (format #f "(p2 1)-> 1, since the expression e, was "))
    (display
     (format #f "turned into a~%"))
    (display
     (format #f "thunk, (delayed). With Cy's method, "))
    (display
     (format #f "(p1 1) -> (1 2),~%"))
    (display
     (format #f "(p2 1) -> (1 2).~%"))
    (newline)
    (display
     (format #f "(c)~%"))
    (display
     (format #f "Cy's method doesn't affect the for-each "))
    (display
     (format #f "example because the~%"))
    (display
     (format #f "arguments to primitive functions are not "))
    (display
     (format #f "delayed anyway.~%"))
    (newline)
    (display
     (format #f "(d)~%"))
    (display
     (format #f "I like the approach in the text, since "))
    (display
     (format #f "the result~%"))
    (display
     (format #f "(p2 1) -> 1 yields an expected result, "))
    (display
     (format #f "in a language~%"))
    (display
     (format #f "with normal order. Arguments should be "))
    (display
     (format #f "assumed to be~%"))
    (display
     (format #f "delayed, therefore it's the 'natural' "))
    (display
     (format #f "choice to make.~%"))
    (display
     (format #f "Cy's method seems inconsistent with "))
    (display
     (format #f "normal order.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.30 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
