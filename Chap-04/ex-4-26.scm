#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.26                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (unless condition usual-value exceptional-value)
  (begin
    (if condition
        exceptional-value
        usual-value)
    ))

;;;#############################################################
;;;#############################################################
(define (factorial n)
  (begin
    (if (> n 1)
        (begin
          (* n (factorial (- n 1))))
        (begin
          1
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial result-hash-table)
 (begin
   (let ((sub-name "test-factorial")
         (test-list
          (list
           (list 1 1) (list 2 2) (list 3 6) (list 4 24)
           (list 5 120) (list 6 720) (list 7 5040)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (factorial nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a : "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle and Alyssa P. Hacker disagree "))
    (display
     (format #f "over the~%"))
    (display
     (format #f "importance of lazy evaluation for implementing "))
    (display
     (format #f "things~%"))
    (display
     (format #f "such as unless. Ben points out that it's "))
    (display
     (format #f "possible to~%"))
    (display
     (format #f "implement unless in applicative order as a "))
    (display
     (format #f "special form.~%"))
    (display
     (format #f "Alyssa counters that, if one did that, "))
    (display
     (format #f "unless would be~%"))
    (display
     (format #f "merely syntax, not a procedure that could "))
    (display
     (format #f "be used in~%"))
    (display
     (format #f "conjunction with higher-order procedures. "))
    (display
     (format #f "Fill in the~%"))
    (display
     (format #f "details on both sides of the argument. "))
    (display
     (format #f "Show how to~%"))
    (display
     (format #f "implement unless as a derived expression "))
    (display
     (format #f "(like cond~%"))
    (display
     (format #f "or let), and give an example of a "))
    (display
     (format #f "situation where it~%"))
    (display
     (format #f "might be useful to have unless available "))
    (display
     (format #f "as a procedure,~%"))
    (display
     (format #f "rather than as a special form.~%"))
    (newline)
    (display
     (format #f "Ben is right in that if is a special form, "))
    (display
     (format #f "so unless~%"))
    (display
     (format #f "should be defined in the same way. Alyssa "))
    (display
     (format #f "is right, you~%"))
    (display
     (format #f "want to use the unless function to operate "))
    (display
     (format #f "with~%"))
    (display
     (format #f "higher-order procedures, for example, "))
    (display
     (format #f "mapping the~%"))
    (display
     (format #f "unless function over a stream.~%"))
    (newline)
    (display
     (format #f "(define (unless predicate "))
    (display
     (format #f "delayed-usual-expr delayed-except-expr)~%"))
    (display
     (format #f "  (if (predicate)~%"))
    (display
     (format #f "      (force delayed-except-expr)~%"))
    (display
     (format #f "      (force delayed-usual-expr)))~%"))
    (newline)
    (display
     (format #f "(stream-filter~%"))
    (display
     (format #f "  (lambda (an-int) (unless (prime? "))
    (display
     (format #f "an-int) #f #t)) integers-stream)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.26 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
