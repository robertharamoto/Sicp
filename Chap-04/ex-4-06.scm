#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.06                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (let->combination let-list)
  (define (local-iter var-list acc-var-list acc-exp-list)
    (begin
      (if (null? var-list)
          (begin
            (list acc-var-list acc-exp-list))
          (begin
            (let ((this-pair (car var-list))
                  (tail-list (cdr var-list)))
              (let ((var-elem (car this-pair))
                    (exp-elem (cadr this-pair)))
                (begin
                  (local-iter
                   tail-list
                   (append acc-var-list (list var-elem))
                   (append acc-exp-list (list exp-elem)))
                  )))
            ))
      ))
  (begin
    (let ((var-list (cadr let-list))
          (body-list (caddr let-list)))
      (let ((acc-list-list
             (local-iter var-list (list) (list))))
        (let ((new-var-list (car acc-list-list))
              (new-expr-list (cadr acc-list-list)))
          (let ((lambda-expr
                 (cons
                  (list 'lambda new-var-list
                        body-list)
                  new-expr-list)))
            (begin
              lambda-expr
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append
                     err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-let-to-combination result-hash-table)
 (begin
   (let ((sub-name "test-let-to-combination")
         (test-list
          (list
           (list
            (list
             'let (list (list 'z 1))
             (list
              'begin (list newline)))
            (list
             (list
              'lambda (list 'z)
              (list 'begin (list newline))) 1))
           (list
            (list
             'let (list (list 'x 3) (list 'y 4))
             (list 'begin (list newline)))
            (list
             (list
              'lambda (list 'x 'y)
              (list 'begin (list newline))) 3 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((sexp (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list (let->combination sexp)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : sexp=~a, "
                        sub-name test-label-index sexp)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list result-list
                     sub-name err-msg-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (let-form? let-list)
  (begin
    (eq? (car let-list) 'let)
    ))

;;;#############################################################
;;;#############################################################
(define (eval exp env)
  (begin
    (cond
     ((self-evaluating? exp)
      (begin
        exp
        ))
     ((variable? exp)
      (begin
        (lookup-variable-value exp env)
        ))
     ((quoted? exp)
      (begin
        (text-of-quotation exp)
        ))
     ((assignment? exp)
      (begin
        (eval-assignment exp env)
        ))
     ((definition? exp)
      (begin
        (eval-definition exp env)
        ))
     ((if? exp)
      (begin
        (eval-if exp env)
        ))
     ((let-form? exp)
      (begin
        (eval (let->combination exp env))
        ))
     ((lambda? exp)
      (begin
        (make-procedure
         (lambda-parameters exp)
         (lambda-body exp)
         env)
        ))
     ((begin? exp)
      (begin
        (eval-sequence (begin-actions exp) env)
        ))
     ((cond? exp)
      (begin
        (eval (cond->if exp) env)
        ))
     ((application? exp)
      (begin
        (apply (eval (operator exp) env)
               (list-of-values (operands exp) env))
        ))
     (else
      (begin
        (error "Unknown expression type -- EVAL" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (cond? exp)
  (begin
    (tagged-list? exp 'cond)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-clauses exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-else-clause? clause)
  (begin
    (eq? (cond-predicate clause) 'else)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-predicate clause)
  (begin
    (car clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-actions clause)
  (begin
    (cdr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-recipient-clause? clause)
  (begin
    (eq? (car (cond-actions clause)) '=>)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-recipient-action clause)
  (begin
    (cddr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond->if exp)
  (begin
    (expand-clauses (cond-clauses exp))
    ))

;;;#############################################################
;;;#############################################################
(define (expand-clauses clauses)
  (begin
    (if (null? clauses)
        (begin
          'false)                          ; no else clause
        (begin
          (let ((first (car clauses))
                (rest (cdr clauses)))
            (begin
              (if (cond-else-clause? first)
                  (begin
                    (if (null? rest)
                        (begin
                          (sequence->exp (cond-actions first)))
                        (begin
                          (error
                           "ELSE clause isn't last -- COND->IF"
                           clauses)
                          )))
                  (begin
                    (if (and (cond-recipient-clause? first)
                             (procedure? (cond-recipient-action first)))
                        (begin
                          (make-if (cond-predicate first)
                                   (list cond-recipient-action
                                         (list
                                          (cond-predicate first)
                                          cond-recipient-action))
                                   (expand-clauses rest)))
                        (begin
                          (make-if (cond-predicate first)
                                   (sequence->exp (cond-actions first))
                                   (expand-clauses rest))
                          ))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let expressions are derived expressions, "))
    (display
     (format #f "because:~%"))
    (newline)
    (display
     (format #f "(let ((<var1> <exp1>) ... "))
    (display
     (format #f "(<varn> <expn>))~%"))
    (display
     (format #f "  <body>)~%"))
    (newline)
    (display
     (format #f "is equivalent to:~%"))
    (newline)
    (display
     (format #f "((lambda (<var1> ... <varn>)~%"))
    (display
     (format #f "   <body>)~%"))
    (display
     (format #f " <exp1> ... <expn>)~%"))
    (newline)
    (display
     (format #f "Implement a syntactic transformation "))
    (display
     (format #f "let->combination that~%"))
    (display
     (format #f "that reduces evaluating let expressions "))
    (display
     (format #f "to evaluating~%"))
    (display
     (format #f "combinations of the type shown above, and "))
    (display
     (format #f "add the~%"))
    (display
     (format #f "appropriate clause to eval to handle "))
    (display
     (format #f "let expressions.~%"))
    (newline)
    (display
     (format #f "(define (let->combination let-list)~%"))
    (display
     (format #f "  (define (local-iter var-list "))
    (display
     (format #f "acc-var-list acc-exp-list)~%"))
    (display
     (format #f "    (if (null? var-list)~%"))
    (display
     (format #f "        (list acc-var-list "))
    (display
     (format #f "acc-exp-list)~%"))
    (display
     (format #f "        (let ((this-pair "))
    (display
     (format #f "(car var-list))~%"))
    (display
     (format #f "              (tail-list "))
    (display
     (format #f "(cdr var-list)))~%"))
    (display
     (format #f "          (let ((var-elem "))
    (display
     (format #f "(car this-pair))~%"))
    (display
     (format #f "                (exp-elem "))
    (display
     (format #f "(cadr this-pair)))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (local-iter~%"))
    (display
     (format #f "               tail-list~%"))
    (display
     (format #f "               (append "))
    (display
     (format #f "acc-var-list (list var-elem))~%"))
    (display
     (format #f "               (append "))
    (display
     (format #f "acc-exp-list (list exp-elem)))~%"))
    (display
     (format #f "              )))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "  (let ((var-list (cadr let-list))~%"))
    (display
     (format #f "        (body-list (caddr let-list)))~%"))
    (display
     (format #f "    (let ((acc-list-list (local-iter "))
    (display
     (format #f "var-list (list) (list))))~%"))
    (display
     (format #f "      (let ((new-var-list "))
    (display
     (format #f "(car acc-list-list))~%"))
    (display
     (format #f "            (new-expr-list "))
    (display
     (format #f "(cadr acc-list-list)))~%"))
    (display
     (format #f "        (let ((lambda-expr~%"))
    (display
     (format #f "               (cons~%"))
    (display
     (format #f "                (list 'lambda "))
    (display
     (format #f "new-var-list~%"))
    (display
     (format #f "                      body-list)~%"))
    (display
     (format #f "                new-expr-list)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            lambda-expr~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "(define (let-form? let-list)~%"))
    (display
     (format #f "  (eq? (car let-list) 'let))~%"))
    (display
     (format #f "(define (eval exp env)~%"))
    (display
     (format #f "  (cond ((self-evaluating? exp) "))
    (display
     (format #f "exp)~%"))
    (display
     (format #f "        ((variable? exp) "))
    (display
     (format #f "(lookup-variable-value exp env))~%"))
    (display
     (format #f "        ((quoted? exp) "))
    (display
     (format #f "(text-of-quotation exp))~%"))
    (display
     (format #f "        ((assignment? exp) "))
    (display
     (format #f "(eval-assignment exp env))~%"))
    (display
     (format #f "        ((definition? exp) "))
    (display
     (format #f "(eval-definition exp env))~%"))
    (display
     (format #f "        ((if? exp) "))
    (display
     (format #f "(eval-if exp env))~%"))
    (display
     (format #f "        ((let-form? exp) "))
    (display
     (format #f "(eval (let->combination exp env)))~%"))
    (display
     (format #f "        ((lambda? exp)~%"))
    (display
     (format #f "         (make-procedure "))
    (display
     (format #f "(lambda-parameters exp)~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(lambda-body exp)~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "env))~%"))
    (display
     (format #f "        ((begin? exp)~%"))
    (display
     (format #f "         (eval-sequence "))
    (display
     (format #f "(begin-actions exp) env))~%"))
    (display
     (format #f "        ((cond? exp) "))
    (display
     (format #f "(eval (cond->if exp) env))~%"))
    (display
     (format #f "        ((application? "))
    (display
     (format #f "exp)~%"))
    (display
     (format #f "         (apply (eval "))
    (display
     (format #f "(operator exp) env)~%"))
    (display
     (format #f "                "))
    (display
     (format #f "(list-of-values (operands exp) env)))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         "))
    (display
     (format #f "(error \"Unknown expression "))
    (display
     (format #f "type -- EVAL\" exp))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.06 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
