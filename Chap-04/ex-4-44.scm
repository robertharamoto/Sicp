#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.44                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Exercise 2.42 described the "))
    (display
     (format #f "\"eight-queens puzzle\" of~%"))
    (display
     (format #f "placing queens on a chessboard so that "))
    (display
     (format #f "no two attack each~%"))
    (display
     (format #f "other. Write a nondeterministic program "))
    (display
     (format #f "to solve this~%"))
    (display
     (format #f "puzzle.~%"))
    (newline)
    (display
     (format #f "(define (eight-queens)~%"))
    (display
     (format #f "  (let ((col-1 (amb 1 2 3 4 5 6 7 8))~%"))
    (display
     (format #f "        (col-2 (amb 1 2 3 4 5 6 7 8)))~%"))
    (display
     (format #f "    (require (distinct? col-1 col-2))~%"))
    (display
     (format #f "    (require (not (equal? "))
    (display
     (format #f "(abs (- col-1 col-2) 1))))~%"))
    (display
     (format #f "    (let ((col-3 "))
    (display
     (format #f "(amb 1 2 3 4 5 6 7 8)))~%"))
    (display
     (format #f "      (require (distinct? "))
    (display
     (format #f "col-1 col-2 col-3))~%"))
    (display
     (format #f "      (require (not (equal? "))
    (display
     (format #f "(abs (- col-1 col-3) 2))))~%"))
    (display
     (format #f "      (require (not (equal? "))
    (display
     (format #f "(abs (- col-2 col-3) 1))))~%"))
    (display
     (format #f "      (let ((col-4 "))
    (display
     (format #f "(amb 1 2 3 4 5 6 7 8)))~%"))
    (display
     (format #f "        (require (distinct? "))
    (display
     (format #f "col-1 col-2 col-3 col-4))~%"))
    (display
     (format #f "        (require (not (equal? "))
    (display
     (format #f "(abs (- col-1 col-4) 3))))~%"))
    (display
     (format #f "        (require (not (equal? "))
    (display
     (format #f "(abs (- col-2 col-4) 2))))~%"))
    (display
     (format #f "        (require (not (equal? "))
    (display
     (format #f "(abs (- col-3 col-4) 1))))~%"))
    (display
     (format #f "        (let ((col-5 "))
    (display
     (format #f "(amb 1 2 3 4 5 6 7 8)))~%"))
    (display
     (format #f "          (require (distinct? "))
    (display
     (format #f "col-1 col-2 col-3 col-4 col-5))~%"))
    (display
     (format #f "          (require (not (equal? "))
    (display
     (format #f "(abs (- col-1 col-5) 4))))~%"))
    (display
     (format #f "          (require (not (equal? "))
    (display
     (format #f "(abs (- col-2 col-5) 3))))~%"))
    (display
     (format #f "          (require (not (equal? "))
    (display
     (format #f "(abs (- col-3 col-5) 2))))~%"))
    (display
     (format #f "          (require (not (equal? "))
    (display
     (format #f "(abs (- col-4 col-5) 1))))~%"))
    (display
     (format #f "          (let ((col-6 "))
    (display
     (format #f "(amb 1 2 3 4 5 6 7 8)))~%"))
    (display
     (format #f "            (require (distinct? "))
    (display
     (format #f "col-1 col-2 col-3 col-4 col-5 col-6))~%"))
    (display
     (format #f "            (require (not (equal? "))
    (display
     (format #f "(abs (- col-1 col-6) 5))))~%"))
    (display
     (format #f "            (require (not (equal? "))
    (display
     (format #f "(abs (- col-2 col-6) 4))))~%"))
    (display
     (format #f "            (require (not (equal? "))
    (display
     (format #f "(abs (- col-3 col-6) 3))))~%"))
    (display
     (format #f "            (require (not (equal? "))
    (display
     (format #f "(abs (- col-4 col-6) 2))))~%"))
    (display
     (format #f "            (require (not (equal? "))
    (display
     (format #f "(abs (- col-5 col-6) 1))))~%"))
    (display
     (format #f "            (let ((col-7 "))
    (display
     (format #f "(amb 1 2 3 4 5 6 7 8)))~%"))
    (display
     (format #f "              (require (distinct? "))
    (display
     (format #f "col-1 col-2 col-3 col-4 col-5 col-6 col-7))~%"))
    (display
     (format #f "              (require (not (equal? "))
    (display
     (format #f "(abs (- col-1 col-7) 6))))~%"))
    (display
     (format #f "              (require (not (equal? "))
    (display
     (format #f "(abs (- col-2 col-7) 5))))~%"))
    (display
     (format #f "              (require (not (equal? "))
    (display
     (format #f "(abs (- col-3 col-7) 4))))~%"))
    (display
     (format #f "              (require (not (equal? "))
    (display
     (format #f "(abs (- col-4 col-7) 3))))~%"))
    (display
     (format #f "              (require (not (equal? "))
    (display
     (format #f "(abs (- col-5 col-7) 2))))~%"))
    (display
     (format #f "              (require (not (equal? "))
    (display
     (format #f "(abs (- col-6 col-7) 1))))~%"))
    (display
     (format #f "              (let ((col-8 "))
    (display
     (format #f "(amb 1 2 3 4 5 6 7 8)))~%"))
    (display
     (format #f "                (require (distinct? "))
    (display
     (format #f "col-1 col-2 col-3 col-4~%"))
    (display
     (format #f "                           "))
    (display
     (format #f "col-5 col-6 col-7 col-8))~%"))
    (display
     (format #f "                (require (not (equal? "))
    (display
     (format #f "(abs (- col-1 col-8) 7))))~%"))
    (display
     (format #f "                (require (not (equal? "))
    (display
     (format #f "(abs (- col-2 col-8) 6))))~%"))
    (display
     (format #f "                (require (not (equal? "))
    (display
     (format #f "(abs (- col-3 col-8) 5))))~%"))
    (display
     (format #f "                (require (not (equal? "))
    (display
     (format #f "(abs (- col-4 col-8) 4))))~%"))
    (display
     (format #f "                (require (not (equal? "))
    (display
     (format #f "(abs (- col-5 col-8) 3))))~%"))
    (display
     (format #f "                (require (not (equal? "))
    (display
     (format #f "(abs (- col-6 col-8) 2))))~%"))
    (display
     (format #f "                (require (not (equal? "))
    (display
     (format #f "(abs (- col-7 col-8) 1))))~%"))
    (display
     (format #f "                (list~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-1 \", col 1\")~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-2 \", col 2\")~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-3 \", col 3\")~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-4 \", col 4\")~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-5 \", col 5\")~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-6 \", col 6\")~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-7 \", col 7\")~%"))
    (display
     (format #f "                  (list \"queen, row \" "))
    (display
     (format #f "col-8 \", col 8\"))~%"))
    (display
     (format #f "               ))))~%"))
    (display
     (format #f "         ))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.44 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
