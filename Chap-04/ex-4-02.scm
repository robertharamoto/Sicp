#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.02                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (list-of-values-l-to-r exps env)
  (begin
    (if (no-operands? exps)
        (begin
          (list))
        (begin
          (let ((result (list))
                (llen (length exps)))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii llen))
                (begin
                  (let ((a1 (eval (list-ref exps ii) env)))
                    (begin
                      (set! result (append result (list a1)))
                      ))
                  ))
              result
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (list-of-values-r-to-l exps env)
  (begin
    (if (no-operands? exps)
        (begin
          (list))
        (begin
          (let ((result (list))
                (llen (length exps)))
            (begin
              (do ((ii (1- llen) (1- ii)))
                  ((< ii 0))
                (begin
                  (let ((a1 (eval (list-ref exps ii) env)))
                    (begin
                      (set! result (cons a1 result))
                      ))
                  ))
              result
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Louis Reasoner plans to reorder the cond "))
    (display
     (format #f "clauses in eval~%"))
    (display
     (format #f "so that the clause for procedure "))
    (display
     (format #f "applications appears~%"))
    (display
     (format #f "before the clause for assignments. "))
    (display
     (format #f "He argues that~%"))
    (display
     (format #f "this will make the interpreter more "))
    (display
     (format #f "efficient. Since~%"))
    (display
     (format #f "programs usually contain more "))
    (display
     (format #f "applications than~%"))
    (display
     (format #f "assignments, definitions, and so on, "))
    (display
     (format #f "his modified eval~%"))
    (display
     (format #f "will usually check fewer clauses than "))
    (display
     (format #f "the original~%"))
    (display
     (format #f "eval before identifying the type of "))
    (display
     (format #f "an expression.~%"))
    (newline)
    (display
     (format #f "a. What is wrong with Louis's plan? "))
    (display
     (format #f "(Hint: What~%"))
    (display
     (format #f "will Louis's evaluator do with the "))
    (display
     (format #f "expression~%"))
    (display
     (format #f "    (define x 3)?)~%"))
    (display
     (format #f "b. Louis is upset that his plan didn't "))
    (display
     (format #f "work. He is~%"))
    (display
     (format #f "willing to go to any lengths to make "))
    (display
     (format #f "his evaluator~%"))
    (display
     (format #f "recognize procedure applications before "))
    (display
     (format #f "it checks for~%"))
    (display
     (format #f "most other kinds of expressions. Help "))
    (display
     (format #f "him by changing~%"))
    (display
     (format #f "the syntax of the evaluated language so "))
    (display
     (format #f "that procedure~%"))
    (display
     (format #f "applications start with call. For example, "))
    (display
     (format #f "instead of~%"))
    (display
     (format #f "(factorial 3) we will now have to write "))
    (display
     (format #f "(call factorial 3)~%"))
    (display
     (format #f "and instead of (+ 1 2) we will have to "))
    (display
     (format #f "write (call + 1 2).~%"))
    (newline)
    (display
     (format #f "(a) When the evaluator encounters "))
    (display
     (format #f "(define x 3)~%"))
    (display
     (format #f "and the application clause is before "))
    (display
     (format #f "the assignments~%"))
    (display
     (format #f "clause, then the statement will be "))
    (display
     (format #f "treated as~%"))
    (display
     (format #f "an application.  The evaluator will "))
    (display
     (format #f "try to apply~%"))
    (display
     (format #f "the function define, which will result "))
    (display
     (format #f "in an error.~%"))
    (newline)
    (display
     (format #f "(b) If you change to syntax of the "))
    (display
     (format #f "language to~%"))
    (display
     (format #f "require that call is needed before "))
    (display
     (format #f "applying a~%"))
    (display
     (format #f "function to it's parameters, then "))
    (display
     (format #f "you can~%"))
    (display
     (format #f "change the order of the applications "))
    (display
     (format #f "clause, since you~%"))
    (display
     (format #f "will be able to differentiate function "))
    (display
     (format #f "application~%"))
    (display
     (format #f "from function definition.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.02 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
