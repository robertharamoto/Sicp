#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.09                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (do->recursive-function do-sexp)
  (begin
    (let ((ii-var-list (car (list-ref do-sexp 1)))
          (end-pred (car (list-ref do-sexp 2)))
          (body-list (list-ref do-sexp 3)))
      (let ((ii-var (list-ref ii-var-list 0))
            (ii-init (list-ref ii-var-list 1))
            (ii-increment (list-ref ii-var-list 2)))
        (let ((result-sexp
               (list 'define (list 'tmp-loop-fname ii-var)
                     (list 'if end-pred #f
                           (append
                            body-list
                            (list (list 'tmp-loop-fname ii-increment))
                            )))))
          (begin
            result-sexp
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-do-to-recursive-function result-hash-table)
 (begin
   (let ((sub-name "test-do-to-recursive-function")
         (test-list
          (list
           (list (list 'do (list (list 'ii 0 (list '1+ 'ii)))
                       (list (list '>= 'ii 'n))
                       (list 'begin (list 'newline)))
                 (list 'define (list 'tmp-loop-fname 'ii)
                       (list
                        'if (list '>= 'ii 'n) #f
                        (list
                         'begin (list 'newline)
                         (list 'tmp-loop-fname
                               (list '1+ 'ii))))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((sexp (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list (do->recursive-function sexp)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : sexp=~a, "
                        sub-name test-label-index sexp)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list result-list
                     sub-name err-msg-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Many languages support a variety of "))
    (display
     (format #f "iteration constructs,~%"))
    (display
     (format #f "such as do, for, while, and until. In "))
    (display
     (format #f "Scheme, iterative~%"))
    (display
     (format #f "processes can be expressed in terms of "))
    (display
     (format #f "ordinary procedure~%"))
    (display
     (format #f "calls, so special iteration constructs "))
    (display
     (format #f "provide no essential~%"))
    (display
     (format #f "gain in computational power. On the other "))
    (display
     (format #f "hand, such~%"))
    (display
     (format #f "constructs are often convenient. Design "))
    (display
     (format #f "some iteration~%"))
    (display
     (format #f "constructs, give examples of their use, "))
    (display
     (format #f "and show how to~%"))
    (display
     (format #f "implement them as derived expressions.~%"))
    (newline)
    (display
     (format #f "Here are some iteration constructs:~%"))
    (display
     (format #f "(do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "    ((>= ii max-len))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "   <body>~%"))
    (display
     (format #f "   ))~%"))
    (display
     (format #f "(for ((ii 0)) ((ii (1+ ii)))~%"))
    (display
     (format #f "     ((>= ii max-len))~%"))
    (display
     (format #f "    <body>)~%"))
    (newline)
    (display
     (format #f "(do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "    ((>= ii 10))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (display (format #f \"~~a \" ii))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(newline)~%"))
    (display
     (format #f "produces: 0 1 2 3 4 5 6 7 8 9 ~%"))
    (newline)
    (display
     (format #f "(for ((ii 0)) ((ii (1+ ii)))~%"))
    (display
     (format #f "     ((>= ii 5))~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (display (format #f \"~~a \" ii))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "(newline)~%"))
    (display
     (format #f "produces: 0 1 2 3 4 ~%"))
    (newline)
    (display
     (format #f "Here's an sample of how to convert a "))
    (display
     (format #f "do-loop into~%"))
    (display
     (format #f "a recursive function. This step has to "))
    (display
     (format #f "be done on~%"))
    (display
     (format #f "a separate pass, so that loops within "))
    (display
     (format #f "the code can~%"))
    (display
     (format #f "be relocated into local functions at "))
    (display
     (format #f "the top of a~%"))
    (display
     (format #f "function definition.~%"))
    (newline)
    (display
     (format #f "(define (do->recursive-function do-sexp)~%"))
    (display
     (format #f "  (let ((ii-var-list "))
    (display
     (format #f "(car (list-ref do-sexp 1)))~%"))
    (display
     (format #f "        (end-pred "))
    (display
     (format #f "(car (list-ref do-sexp 2)))~%"))
    (display
     (format #f "        (body-list "))
    (display
     (format #f "(list-ref do-sexp 3)))~%"))
    (display
     (format #f "    (let ((ii-var "))
    (display
     (format #f "(list-ref ii-var-list 0))~%"))
    (display
     (format #f "          (ii-init "))
    (display
     (format #f "(list-ref ii-var-list 1))~%"))
    (display
     (format #f "          (ii-increment "))
    (display
     (format #f "(list-ref ii-var-list 2)))~%"))
    (display
     (format #f "      (let ((result-sexp~%"))
    (display
     (format #f "             (list~%"))
    (display
     (format #f "              (list 'define "))
    (display
     (format #f "(list 'tmp-loop-fname ii-var)~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "(list 'if end-pred #f~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(append body-list~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(list~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "  (list 'tmp-loop-fname "))
    (display
     (format #f "ii-increment))~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "))))))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          result-sexp~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.09 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
