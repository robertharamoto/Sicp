#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.15                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Given a one-argument procedure p and an "))
    (display
     (format #f "object a, p is~%"))
    (display
     (format #f "said to \"halt\" on a if evaluating the "))
    (display
     (format #f "expression (p a)~%"))
    (display
     (format #f "returns a value, (as opposed to "))
    (display
     (format #f "terminating with an error~%"))
    (display
     (format #f "message or running forever). Show that "))
    (display
     (format #f "it is impossible~%"))
    (display
     (format #f "to write a procedure halts? that correctly "))
    (display
     (format #f "determines where~%"))
    (display
     (format #f "p halts on a for any procedure p and "))
    (display
     (format #f "object a. Use the~%"))
    (display
     (format #f "following reasoning:~%"))
    (newline)
    (display
     (format #f "If you had such a procedure halts?, you "))
    (display
     (format #f "could implement~%"))
    (display
     (format #f "the following program:~%"))
    (newline)
    (display
     (format #f "(define (run-forever) (run-forever))~%"))
    (display
     (format #f "(define (try p)~%"))
    (display
     (format #f "  (if (halts? p p)~%"))
    (display
     (format #f "      (run-forever)~%"))
    (display
     (format #f "      'halted))~%"))
    (newline)
    (display
     (format #f "Now consider evaluating the expression "))
    (display
     (format #f "(try try)~%"))
    (display
     (format #f "and show that any possible outcome "))
    (display
     (format #f "(either halting or~%"))
    (display
     (format #f "running forever) violates the intended "))
    (display
     (format #f "behavior of halts?.~%"))
    (newline)
    (display
     (format #f "Assume (halts? p a) returns true if p "))
    (display
     (format #f "halts on a.~%"))
    (newline)
    (display
     (format #f "Suppose (try try) runs forever, then "))
    (display
     (format #f "halts? returns a~%"))
    (display
     (format #f "false, and (try try) prints 'halted. If "))
    (display
     (format #f "(try try) halts,~%"))
    (display
     (format #f "then halts? returns a true, and it runs "))
    (display
     (format #f "forever. This~%"))
    (display
     (format #f "is exactly opposite of what was intended.~%"))
    (newline)
    (display
     (format #f "Since it's possible to construct a "))
    (display
     (format #f "procedure p that~%"))
    (display
     (format #f "doesn't allow us to determine whether "))
    (display
     (format #f "p halts on a,~%"))
    (display
     (format #f "then it's impossible to write halts? "))
    (display
     (format #f "for every~%"))
    (display
     (format #f "procedure p and object a.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.15 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
