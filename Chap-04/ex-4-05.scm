#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.05                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (eval-expression exp env)
  (begin
    (apply
     (eval (operator exp) env)
     (list-of-values (operands exp) env))
    ))

;;;#############################################################
;;;#############################################################
(define (and-form-1 expr-list env)
  (begin
    (if (null? expr-list)
        (begin
          #t)
        (begin
          (let ((this-expr (car expr-list))
                (tail-list (cdr expr-list)))
            (let ((aresult (eval-expression this-expr env)))
              (begin
                (if (equal? aresult #f)
                    (begin
                      #f)
                    (begin
                      (and aresult (and-form-1 tail-list env))
                      ))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (or-form-1 expr-list env)
  (begin
    (if (null? expr-list)
        (begin
          #f)
        (begin
          (let ((this-expr (car expr-list))
                (tail-list (cdr expr-list)))
            (let ((aresult
                   (eval-expression this-expr env)))
              (begin
                (if (equal? aresult #t)
                    (begin
                      #t)
                    (begin
                      (or aresult (or-form-1 tail-list env))
                      ))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (eval exp env)
  (begin
    (cond
     ((self-evaluating? exp)
      (begin
        exp
        ))
     ((variable? exp)
      (begin
        (lookup-variable-value exp env)
        ))
     ((quoted? exp)
      (begin
        (text-of-quotation exp)
        ))
     ((assignment? exp)
      (begin
        (eval-assignment exp env)
        ))
     ((definition? exp)
      (begin
        (eval-definition exp env)
        ))
     ((if? exp)
      (begin
        (eval-if exp env)
        ))
     ((and-form? exp)
      (begin
        (and-form-1 exp env)
        ))
     ((or-form? exp)
      (begin
        (or-form-1 exp env)
        ))
     ((lambda? exp)
      (begin
        (make-procedure
         (lambda-parameters exp)
         (lambda-body exp)
         env)
        ))
     ((begin? exp)
      (begin
        (eval-sequence (begin-actions exp) env)
        ))
     ((cond? exp)
      (begin
        (eval (cond->if exp) env)
        ))
     ((application? exp)
      (begin
        (apply (eval (operator exp) env)
               (list-of-values (operands exp) env))
        ))
     (else
      (begin
        (error "Unknown expression type -- EVAL" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (cond? exp)
  (begin
    (tagged-list? exp 'cond)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-clauses exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-else-clause? clause)
  (begin
    (eq? (cond-predicate clause) 'else)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-predicate clause)
  (begin
    (car clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-actions clause)
  (begin
    (cdr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-recipient-clause? clause)
  (begin
    (eq? (car (cond-actions clause)) '=>)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-recipient-action clause)
  (begin
    (cddr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond->if exp)
  (begin
    (expand-clauses (cond-clauses exp))
    ))

;;;#############################################################
;;;#############################################################
(define (expand-clauses clauses)
  (begin
    (if (null? clauses)
        (begin
          'false)                          ; no else clause
        (begin
          (let ((first (car clauses))
                (rest (cdr clauses)))
            (begin
              (if (cond-else-clause? first)
                  (begin
                    (if (null? rest)
                        (begin
                          (sequence->exp (cond-actions first)))
                        (begin
                          (error
                           "ELSE clause isn't last -- COND->IF"
                           clauses)
                          )))
                  (begin
                    (if (and (cond-recipient-clause? first)
                             (procedure?
                              (cond-recipient-action first)))
                        (begin
                          (make-if
                           (cond-predicate first)
                           (list
                            cond-recipient-action
                            (list
                             (cond-predicate first)
                             cond-recipient-action))
                           (expand-clauses rest)))
                        (begin
                          (make-if
                           (cond-predicate first)
                           (sequence->exp
                            (cond-actions first))
                           (expand-clauses rest))
                          ))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Scheme allows an additional syntax for "))
    (display
     (format #f "cond clauses, (<test>~%"))
    (display
     (format #f "=> <recipient>). If <test> evaluates to "))
    (display
     (format #f "a true value, then~%"))
    (display
     (format #f "<recipient> is evaluated. Its value must "))
    (display
     (format #f "be a procedure~%"))
    (display
     (format #f "of one argument; this procedure is then "))
    (display
     (format #f "invoked on the~%"))
    (display
     (format #f "value of the <test>, and the result is "))
    (display
     (format #f "returned as the~%"))
    (display
     (format #f "value of the cond expression. For "))
    (display
     (format #f "example:~%"))
    (newline)
    (display
     (format #f "(cond ((assoc 'b '((a 1) (b 2))) "))
    (display
     (format #f "=> cadr)~%"))
    (display
     (format #f "      (else false))~%"))
    (newline)
    (display
     (format #f "returns 2. Modify the handling of cond so "))
    (display
     (format #f "that it supports~%"))
    (display
     (format #f "this extended syntax.~%"))
    (newline)
    (display
     (format #f "(define (cond-recipient-clause? "))
    (display
     (format #f "clause)~%"))
    (display
     (format #f "  (eq? (car (cond-actions "))
    (display
     (format #f "clause)) '=>))~%"))
    (display
     (format #f "(define (cond-recipient-action "))
    (display
     (format #f "clause) (cddr clause))~%"))
    (newline)
    (display
     (format #f "(define (expand-clauses clauses)~%"))
    (display
     (format #f "  (if (null? clauses)~%"))
    (display
     (format #f "      #f~%"))
    (display
     (format #f "      (let ((first (car clauses))~%"))
    (display
     (format #f "            (rest (cdr clauses)))~%"))
    (display
     (format #f "        (if (cond-else-clause? first)~%"))
    (display
     (format #f "            (if (null? rest)~%"))
    (display
     (format #f "                (sequence->exp "))
    (display
     (format #f "(cond-actions first))~%"))
    (display
     (format #f "                "))
    (display
     (format #f "(error \"ELSE clause isn't last "))
    (display
     (format #f "-- COND->IF\"~%"))
    (display
     (format #f "                       clauses))~%"))
    (display
     (format #f "            (if (and "))
    (display
     (format #f "(cond-recipient-clause? first)~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(procedure? (cond-recipient-action "))
    (display
     (format #f "first)))~%"))
    (display
     (format #f "                "))
    (display
     (format #f "(make-if (cond-predicate first)~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(list cond-recipient-action~%"))
    (display
     (format #f "                               "))
    (display
     (format #f "(list (cond-predicate first)~%"))
    (display
     (format #f "                               "))
    (display
     (format #f "    cond-recipient-action))~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(expand-clauses rest))~%"))
    (display
     (format #f "                "))
    (display
     (format #f "(make-if (cond-predicate first)~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(sequence->exp (cond-actions first))~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(expand-clauses rest)))))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.05 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
