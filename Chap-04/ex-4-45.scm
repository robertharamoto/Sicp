#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.45                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "With the grammar given above, the "))
    (display
     (format #f "following sentence~%"))
    (display
     (format #f "can be parsed in five different ways: "))
    (display
     (format #f "``The professor~%"))
    (display
     (format #f "lectures to the student in the class "))
    (display
     (format #f "with the cat.''~%"))
    (display
     (format #f "Give the five parses and explain the "))
    (display
     (format #f "differences in~%"))
    (display
     (format #f "shades of meaning among them.~%"))
    (newline)
    (display
     (format #f "(1) The professor lectures (to the "))
    (display
     (format #f "student) (in~%"))
    (display
     (format #f "the class) (with the cat). (sentence~%"))
    (display
     (format #f "  (simple-noun-phrase (article the) "))
    (display
     (format #f "(noun professor))~%"))
    (display
     (format #f "  (verb-phrase~%"))
    (display
     (format #f "   (verb-phrase~%"))
    (display
     (format #f "     (verb lectures)~%"))
    (display
     (format #f "     (prep-phrase (prep to)~%"))
    (display
     (format #f "                  (simple-noun-phrase~%"))
    (display
     (format #f "                   (article the) "))
    (display
     (format #f "(noun student))))~%"))
    (display
     (format #f "   (prep-phrase (prep in)~%"))
    (display
     (format #f "                (simple-noun-phrase~%"))
    (display
     (format #f "                 (article the) "))
    (display
     (format #f "(noun class)))~%"))
    (display
     (format #f "   (prep-phrase (prep with)~%"))
    (display
     (format #f "                (simple-noun-phrase~%"))
    (display
     (format #f "                 (article the) "))
    (display
     (format #f "(noun cat)))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "The professor lectures in the class, "))
    (display
     (format #f "with the cat,~%"))
    (display
     (format #f "to the student.~%"))
    (newline)
    (display
     (format #f "(2) The professor lectures (to the "))
    (display
     (format #f "student) ((in the~%"))
    (display
     (format #f "class) (with the cat)).~%"))
    (display
     (format #f "(sentence~%"))
    (display
     (format #f "  (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun professor))~%"))
    (display
     (format #f "  (verb-phrase~%"))
    (display
     (format #f "   (verb-phrase~%"))
    (display
     (format #f "     (verb lectures)~%"))
    (display
     (format #f "     (prep-phrase (prep to)~%"))
    (display
     (format #f "                  (simple-noun-phrase~%"))
    (display
     (format #f "                   (article the) "))
    (display
     (format #f "(noun student)))~%"))
    (display
     (format #f "     (prep-phrase (prep in)~%"))
    (display
     (format #f "                  (simple-noun-phrase~%"))
    (display
     (format #f "                   (article the) "))
    (display
     (format #f "(noun class))~%"))
    (display
     (format #f "                  (prep-phrase "))
    (display
     (format #f "(prep with)~%"))
    (display
     (format #f "                               "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                                "))
    (display
     (format #f "(article the) (noun cat))))~%"))
    (display
     (format #f "    ))~%"))
    (display
     (format #f "The professor lectures (in the class "))
    (display
     (format #f "with the cat),~%"))
    (display
     (format #f "to the student.~%"))
    (newline)
    (display
     (format #f "(3) The professor lectures ((to the "))
    (display
     (format #f "student)~%"))
    (display
     (format #f "(in the class)) (with the cat). (sentence~%"))
    (display
     (format #f "  (noun-phrase~%"))
    (display
     (format #f "    (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun professor))~%"))
    (display
     (format #f "    (verb-phrase~%"))
    (display
     (format #f "     (verb-phrase~%"))
    (display
     (format #f "       (verb lectures)~%"))
    (display
     (format #f "       (prep-phrase (prep to)~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "(noun-phrase~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                       "))
    (display
     (format #f "(article the) (noun student))~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(prep-phrase (prep in)~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "(article the) (noun class))))))~%"))
    (display
     (format #f "     (prep-phrase (prep with)~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(article the) (noun cat)))~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f "The professor lectures (to the "))
    (display
     (format #f "student in the~%"))
    (display
     (format #f "class), with the cat~%"))
    (newline)
    (display
     (format #f "(4) The professor lectures (to the "))
    (display
     (format #f "student in the~%"))
    (display
     (format #f "class with the cat).~%"))
    (display
     (format #f "(sentence~%"))
    (display
     (format #f "  (noun-phrase~%"))
    (display
     (format #f "    (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun professor))~%"))
    (display
     (format #f "    (verb-phrase~%"))
    (display
     (format #f "     (verb-phrase~%"))
    (display
     (format #f "       (verb lectures)~%"))
    (display
     (format #f "       (prep-phrase (prep to)~%"))
    (display
     (format #f "                    (noun-phrase~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                       "))
    (display
     (format #f "(article the) (noun student))~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(prep-phrase (prep in)~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "(article the) (noun class)))~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(prep-phrase (prep with)~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                                   "))
    (display
     (format #f "(article the) (noun cat))))))~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f "The professor lectures to (the student "))
    (display
     (format #f "in the~%"))
    (display
     (format #f "class with the cat).~%"))
    (newline)
    (display
     (format #f "(5) The professor lectures (to the "))
    (display
     (format #f "student in the~%"))
    (display
     (format #f "class) (with the cat).~%"))
    (display
     (format #f "(sentence~%"))
    (display
     (format #f "  (noun-phrase~%"))
    (display
     (format #f "    (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun professor))~%"))
    (display
     (format #f "    (verb-phrase~%"))
    (display
     (format #f "     (verb-phrase~%"))
    (display
     (format #f "       (verb lectures)~%"))
    (display
     (format #f "       (prep-phrase (prep to)~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(noun-phrase~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                       "))
    (display
     (format #f "(article the) (noun student))~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(prep-phrase (prep in)~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(noun-phrase~%"))
    (display
     (format #f "                                    "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                                      "))
    (display
     (format #f "(article the) (noun class)))~%"))
    (display
     (format #f "                                    "))
    (display
     (format #f "(prep-phrase (prep with)~%"))
    (display
     (format #f "                                                 "))
    (display
     (format #f "(simple-noun-phrase~%"))
    (display
     (format #f "                                                  "))
    (display
     (format #f "(article the) (noun cat))))))~%"))
    (display
     (format #f "      ))))~%"))
    (display
     (format #f "The professor lectures to (the student "))
    (display
     (format #f "in (the class~%"))
    (display
     (format #f "with the cat)).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.45 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
