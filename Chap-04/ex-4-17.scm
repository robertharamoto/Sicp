#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.17                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Draw diagrams of the environment in "))
    (display
     (format #f "effect when evaluating~%"))
    (display
     (format #f "the expression <e3> in the procedure in "))
    (display
     (format #f "the text, comparing~%"))
    (display
     (format #f "how this will be structured when "))
    (display
     (format #f "definitions are~%"))
    (display
     (format #f "interpreted sequentially with how it "))
    (display
     (format #f "will be structured~%"))
    (display
     (format #f "if definitions are scanned out as "))
    (display
     (format #f "described. Why is~%"))
    (display
     (format #f "there an extra frame in the transformed "))
    (display
     (format #f "program? Explain~%"))
    (display
     (format #f "why this difference in enviroment "))
    (display
     (format #f "structure can never make~%"))
    (display
     (format #f "a difference in the behavior of a correct "))
    (display
     (format #f "program. Design a~%"))
    (display
     (format #f "way to make the interpreter implement the "))
    (display
     (format #f "\"simultaneous\"~%"))
    (display
     (format #f "scope rule for internal definitions without "))
    (display
     (format #f "constructing~%"))
    (display
     (format #f "the extra frame.~%"))
    (newline)
    (display
     (format #f "(lambda <vars> (define "))
    (display
     (format #f "u <e1>) (define v <e2>) <e3>)~%"))
    (display
     (format #f "(lambda <vars> ...)~%"))
    (display
     (format #f "  -> environment E1 { <vars>, "))
    (display
     (format #f "u, v, global-environment }~%"))
    (newline)
    (display
     (format #f "(lambda <vars>~%"))
    (display
     (format #f "  (let ((u '*unassigned*)~%"))
    (display
     (format #f "        (v '*unassigned*))~%"))
    (display
     (format #f "    (set! u <e1>)~%"))
    (display
     (format #f "    (set! v <e2>)~%"))
    (display
     (format #f "    <e3>))~%"))
    (display
     (format #f "Since let is syntactic sugar for a "))
    (display
     (format #f "lambda expression~%"))
    (display
     (format #f "(see exercise 4.6), two environments are "))
    (display
     (format #f "created,~%"))
    (newline)
    (display
     (format #f "E1 { <vars>, global-environment} "))
    (display
     (format #f "and~%"))
    (display
     (format #f "E2 { u, v, E1 },~%"))
    (display
     (format #f "where <e3> is evaluated in the "))
    (display
     (format #f "E2 environment.~%"))
    (newline)
    (display
     (format #f "The environment structure won't make "))
    (display
     (format #f "a difference in~%"))
    (display
     (format #f "the behavior of a program since the "))
    (display
     (format #f "variables in the~%"))
    (display
     (format #f "2 environment case are encapsulated. "))
    (display
     (format #f "Any variables~%"))
    (display
     (format #f "needed at the lowest level can be "))
    (display
     (format #f "accessed by going~%"))
    (display
     (format #f "up one level in the environment, resulting "))
    (display
     (format #f "in identical~%"))
    (display
     (format #f "behavior.~%"))
    (newline)
    (display
     (format #f "In order to construct the simultaneous "))
    (display
     (format #f "scope rule for~%"))
    (display
     (format #f "internal definitions, one could modify "))
    (display
     (format #f "apply, so that~%"))
    (display
     (format #f "let expressions within a function use the "))
    (display
     (format #f "current~%"))
    (display
     (format #f "environment, instead of calling "))
    (display
     (format #f "extend-environment.~%"))
    (display
     (format #f "One could do this by defining a new type "))
    (display
     (format #f "'let-procedure,~%"))
    (display
     (format #f "and modify apply as follows:~%"))
    (newline)
    (display
     (format #f "(define (apply procedure arguments)~%"))
    (display
     (format #f "  (cond ((primitive-procedure? "))
    (display
     (format #f "procedure)~%"))
    (display
     (format #f "         (apply-primitive-procedure "))
    (display
     (format #f "procedure arguments))~%"))
    (display
     (format #f "        ((let-procedure? procedure)~%"))
    (display
     (format #f "         (begin~%"))
    (display
     (format #f "           (add-binding-list-to-frame!~%"))
    (display
     (format #f "             (procedure-parameters "))
    (display
     (format #f "procedure)~%"))
    (display
     (format #f "             arguments (first-frame "))
    (display
     (format #f "(procedure-environment procedure)))~%"))
    (display
     (format #f "           (eval-sequence~%"))
    (display
     (format #f "             (procedure-body "))
    (display
     (format #f "procedure)~%"))
    (display
     (format #f "             (procedure-environment "))
    (display
     (format #f "procedure))))~%"))
    (display
     (format #f "        ((compound-procedure? "))
    (display
     (format #f "procedure)~%"))
    (display
     (format #f "         (eval-sequence~%"))
    (display
     (format #f "           (procedure-body "))
    (display
     (format #f "procedure)~%"))
    (display
     (format #f "           (extend-environment~%"))
    (display
     (format #f "             (procedure-parameters "))
    (display
     (format #f "procedure)~%"))
    (display
     (format #f "             arguments~%"))
    (display
     (format #f "             (procedure-environment "))
    (display
     (format #f "procedure))))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         (error~%"))
    (display
     (format #f "          "))
    (display
     (format #f "\"Unknown procedure type "))
    (display
     (format #f "-- APPLY\" procedure))))~%"))
    (display
     (format #f "where the new function "))
    (display
     (format #f "add-binding-list-to-frame!~%"))
    (display
     (format #f "is calls add-binding-to-frame! "))
    (display
     (format #f "(defined in the text).~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.17 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
