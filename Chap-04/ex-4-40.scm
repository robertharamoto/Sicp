#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.40                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the multiple dwelling problem, how "))
    (display
     (format #f "many sets of~%"))
    (display
     (format #f "assignments are there of people to floors, "))
    (display
     (format #f "both before and~%"))
    (display
     (format #f "after the requirement that floor "))
    (display
     (format #f "assignments be distinct?~%"))
    (display
     (format #f "It is very inefficient to generate all "))
    (display
     (format #f "assignments of people~%"))
    (display
     (format #f "to floors and then leave it to "))
    (display
     (format #f "backtracking to eliminate~%"))
    (display
     (format #f "possibilities. For example, most of the "))
    (display
     (format #f "restrictions depend~%"))
    (display
     (format #f "on only one or two of the person-floor "))
    (display
     (format #f "variables, and can~%"))
    (display
     (format #f "thus be imposed before floors have been "))
    (display
     (format #f "selected for all~%"))
    (display
     (format #f "the people. Write and demonstrate a much "))
    (display
     (format #f "more efficient~%"))
    (display
     (format #f "nondeterministic procedure that solves "))
    (display
     (format #f "this problem based~%"))
    (display
     (format #f "upon generating only those possibilities "))
    (display
     (format #f "that are not~%"))
    (display
     (format #f "already ruled out by previous restrictions. "))
    (display
     (format #f "(Hint: this~%"))
    (display
     (format #f "will require a nest of let "))
    (display
     (format #f "expressions.)~%"))
    (newline)

    (display
     (format #f "(define (multiple-dwelling)~%"))
    (display
     (format #f "  (let ((fletcher (amb 1 2 3 4 5)))~%"))
    (display
     (format #f "    (require (not (= fletcher 5)))~%"))
    (display
     (format #f "    (require (not (= fletcher 1)))~%"))
    (display
     (format #f "    (let ((cooper (amb 1 2 3 4 5)))~%"))
    (display
     (format #f "      (require (not (= cooper 1)))~%"))
    (display
     (format #f "      (require (not (= (abs "))
    (display
     (format #f "(- fletcher cooper)) 1)))~%"))
    (display
     (format #f "      (let ((miller (amb 1 2 3 4 5)))~%"))
    (display
     (format #f "        (require (> miller cooper))~%"))
    (display
     (format #f "        (let ((baker (amb 1 2 3 4 5)))~%"))
    (display
     (format #f "          (require (not (= baker 5)))~%"))
    (display
     (format #f "          (let ((smith (amb 1 2 3 4 5)))~%"))
    (display
     (format #f "            (require~%"))
    (display
     (format #f "               (distinct?~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(list baker cooper fletcher "))
    (display
     (format #f "miller smith)))~%"))
    (display
     (format #f "            (list "))
    (display
     (format #f "(list 'baker baker)~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(list 'cooper cooper)~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(list 'fletcher fletcher)~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(list 'miller miller)~%"))
    (display
     (format #f "                  "))
    (display
     (format #f "(list 'smith smith))))~%"))
    (display
     (format #f "        ))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.40 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
