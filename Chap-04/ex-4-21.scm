#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.21                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (fib nn)
  (begin
    ((lambda (fibo)
       (begin
         (fibo fibo 0 1 nn)
         ))
     (lambda (func b a counter)
       (begin
         (if (<= counter 0)
             (begin
               b)
             (begin
               (func
                func a (+ a b) (1- counter))
               ))
         )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fib result-hash-table)
 (begin
   (let ((sub-name "test-fib")
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1)
           (list 3 2) (list 4 3) (list 5 5)
           (list 6 8) (list 7 13) (list 8 21)
           (list 9 34) (list 10 55) (list 11 89)
           (list 12 144) (list 13 233) (list 14 377)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (fib nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : for n=~a : "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (ff-example x)
  (begin
    ((lambda (even? odd?)
       (begin
         (even? even? odd? x)
         ))
     (lambda (ev? od? n)
       (begin
         (if (= n 0)
             (begin
               #t)
             (begin
               (od? ev? od? (1- n))
               ))
         ))
     (lambda (ev? od? n)
       (begin
         (if (= n 0)
             (begin
               #f)
             (begin
               (ev? ev? od? (1- n))
               ))
         )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-ff-example result-hash-table)
 (begin
   (let ((sub-name "test-ff-example")
         (test-list
          (list
           (list 0 #t) (list 1 #f) (list 2 #t) (list 3 #f)
           (list 4 #t) (list 5 #f) (list 6 #t) (list 7 #f)
           (list 8 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (ff-example nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : for n=~a : "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Amazingly, Louis's intuition in exercise "))
    (display
     (format #f "4.20 is correct.~%"))
    (display
     (format #f "It is indeed possible to specify recursive "))
    (display
     (format #f "procedures~%"))
    (display
     (format #f "without using letrec (or even define), "))
    (display
     (format #f "although the method~%"))
    (display
     (format #f "for accomplishing this is much more subtle "))
    (display
     (format #f "than Louis~%"))
    (display
     (format #f "imagined. The following expression computes "))
    (display
     (format #f "10 factorial~%"))
    (display
     (format #f "by applying a recursive factorial "))
    (display
     (format #f "procedure:~%"))
    (newline)
    (display
     (format #f "((lambda (n)~%"))
    (display
     (format #f "   ((lambda (fact)~%"))
    (display
     (format #f "      (fact fact n))~%"))
    (display
     (format #f "    (lambda (ft k)~%"))
    (display
     (format #f "      (if (= k 1)~%"))
    (display
     (format #f "          1~%"))
    (display
     (format #f "          (* k (ft ft (- k 1)))))))~%"))
    (display
     (format #f " 10)~%"))
    (newline)
    (display
     (format #f "a. Check (by evaluating the expression) "))
    (display
     (format #f "that this really~%"))
    (display
     (format #f "does compute factorials. Devise an "))
    (display
     (format #f "analogous expression~%"))
    (display
     (format #f "for computing Fibonacci numbers.~%"))
    (display
     (format #f "b. Consider the following procedure, which "))
    (display
     (format #f "includes~%"))
    (display
     (format #f "mutually recursive internal definitions:~%"))
    (newline)
    (display
     (format #f "(define (f x)~%"))
    (display
     (format #f "  (define (even? n)~%"))
    (display
     (format #f "    (if (= n 0)~%"))
    (display
     (format #f "        true~%"))
    (display
     (format #f "        (odd? (- n 1))))~%"))
    (display
     (format #f "  (define (odd? n)~%"))
    (display
     (format #f "    (if (= n 0)~%"))
    (display
     (format #f "        false~%"))
    (display
     (format #f "        (even? (- n 1))))~%"))
    (display
     (format #f "  (even? x))~%"))
    (newline)
    (display
     (format #f "Fill in the missing expressions to "))
    (display
     (format #f "complete an alternative~%"))
    (display
     (format #f "definition of f, which uses neither "))
    (display
     (format #f "internal definitions~%"))
    (display
     (format #f "nor letrec:~%"))
    (newline)
    (display
     (format #f "(define (f x)~%"))
    (display
     (format #f "  ((lambda (even? odd?)~%"))
    (display
     (format #f "     (even? even? odd? x))~%"))
    (display
     (format #f "   (lambda (ev? od? n)~%"))
    (display
     (format #f "     (if (= n 0) true "))
    (display
     (format #f "(od? <??> <??> <??>)))~%"))
    (display
     (format #f "   (lambda (ev? od? n)~%"))
    (display
     (format #f "     (if (= n 0) false "))
    (display
     (format #f "(ev? <??> <??> <??>)))))~%"))
    (newline)
    (display
     (format #f "(a)~%"))
    (display
     (format #f "(define (fib nn)~%"))
    (display
     (format #f "  ((lambda (fibo)~%"))
    (display
     (format #f "     (fibo fibo 0 1 nn))~%"))
    (display
     (format #f "   (lambda (func b a counter)~%"))
    (display
     (format #f "     (if (<= counter 0)~%"))
    (display
     (format #f "         b~%"))
    (display
     (format #f "         (func func a "))
    (display
     (format #f "(+ a b) (1- counter))~%"))
    (display
     (format #f "         ))))~%"))
    (display
     (format #f "See test below.~%"))
    (newline)
    (display
     (format #f "(b)~%"))
    (display
     (format #f "(define (ff-example x)~%"))
    (display
     (format #f "  ((lambda (even? odd?)~%"))
    (display
     (format #f "     (even? even? odd? x))~%"))
    (display
     (format #f "   (lambda (ev? od? n)~%"))
    (display
     (format #f "     (if (= n 0) #t "))
    (display
     (format #f "(od? ev? od? (1- n))))~%"))
    (display
     (format #f "   (lambda (ev? od? n)~%"))
    (display
     (format #f "     (if (= n 0) #f "))
    (display
     (format #f "(ev? ev? od? (1- n))))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((n 10))
      (let ((f-result
             ((lambda (n)
                ((lambda (fact)
                   (fact fact n))
                 (lambda (ft k)
                   (begin
                     (if (= k 1)
                         (begin
                           1)
                         (begin
                           (* k (ft ft (- k 1)))
                           ))
                     ))))
              n)))
        (let ((fib-result (fib n)))
          (begin
            (display
             (ice-9-format:format
              #f "(fact ~a) = ~:d~%" n f-result))
            (newline)
            (display
             (ice-9-format:format
              #f "(fib ~a) = ~:d~%" n fib-result))
            (newline)
            (display
             (ice-9-format:format
              #f "(ff-example ~a) = ~a~%"
              n (if (ff-example n) "true" "false")))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.21 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
