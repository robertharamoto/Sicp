#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.36                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (an-integer-between low high)
  (begin
    (let ((diff (- high low)))
      (begin
        (if (< diff 0)
            (begin
              (set! diff (* -1 diff))
              (+ high (random diff)))
            (begin
              (+ low (random diff))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-an-integer-between result-hash-table)
 (begin
   (let ((sub-name "test-an-integer-between")
         (test-list
          (list
           (list 0 10) (list 1 100) (list 10 20) (list 50 100)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((low (list-ref alist 0))
                  (high (list-ref alist 1)))
              (let ((result (an-integer-between low high)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : low=~a, high=~a, "
                        sub-name test-label-index
                        low high))
                      (err-msg-2
                       (format #f "result=~a" result)))
                  (begin
                    (unittest2:assert?
                     (and (<= result high)
                          (>= result low))
                     sub-name
                     (string-append
                      err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Exercise 3.69 discussed how to generate "))
    (display
     (format #f "the stream of~%"))
    (display
     (format #f "all Pythagorean triples, with no upper "))
    (display
     (format #f "bound on the~%"))
    (display
     (format #f "size of the integers to be searched. "))
    (display
     (format #f "Explain why simply~%"))
    (display
     (format #f "replacing an-integer-between by "))
    (display
     (format #f "an-integer-starting-from~%"))
    (display
     (format #f "in the procedure in exercise 4.35 "))
    (display
     (format #f "is not an adequate~%"))
    (display
     (format #f "way to generate arbitrary Pythagorean "))
    (display
     (format #f "triples. Write a~%"))
    (display
     (format #f "procedure that actually will accomplish "))
    (display
     (format #f "this. (That is,~%"))
    (display
     (format #f "write a procedure for which repreatedly "))
    (display
     (format #f "typing try-again~%"))
    (display
     (format #f "would in principle eventually generate "))
    (display
     (format #f "all Pythagorean~%"))
    (display
     (format #f "triples.)~%"))
    (newline)
    (display
     (format #f "The function an-integer-between is not "))
    (display
     (format #f "adequate for~%"))
    (display
     (format #f "generating arbitrary Pythagorean triples "))
    (display
     (format #f "because~%"))
    (display
     (format #f "an-integer-between needs a high-bound, "))
    (display
     (format #f "which limits~%"))
    (display
     (format #f "the size of the triples that can be "))
    (display
     (format #f "generated.~%"))
    (newline)
    (display
     (format #f "(define (make-try-again low high)~%"))
    (display
     (format #f "  (let ((local-low low)~%"))
    (display
     (format #f "        (local-high high))~%"))
    (display
     (format #f "    (define (local-try-again)~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (set! local-high "))
    (display
     (format #f "(* local-high 10))~%"))
    (display
     (format #f "        (a-pythagorean-triple-between "))
    (display
     (format #f "local-low local-high)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "     local-try-again))~%"))
    (newline)
    (display
     (format #f "(define try-again "))
    (display
     (format #f "(make-try-again 1 10))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.36 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
