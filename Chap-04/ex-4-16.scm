#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.16                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (lookup-variable-value var env)
  (define (env-loop env)
    (define (scan vars vals)
      (begin
        (cond
         ((null? vars)
          (begin
            (env-loop (enclosing-environment env))
            ))
         ((eq? var (car vars))
          (begin
            (let ((val (car vals)))
              (begin
                (if (eq? val '*unassigned*)
                    (begin
                      (error "Unassigned variable" var))
                    (begin
                      val
                      ))
                ))
            ))
         (else
          (begin
            (scan (cdr vars) (cdr vals))
            )))
        ))
    (begin
      (if (eq? env the-empty-environment)
          (begin
            (error "Unbound variable" var))
          (begin
            (let ((frame (first-frame env)))
              (begin
                (scan
                 (frame-variables frame)
                 (frame-values frame))
                ))
            ))
      ))
  (begin
    (env-loop env)
    ))

;;;#############################################################
;;;#############################################################
(define-syntax list-in-define-macro
  (syntax-rules ()
    ((list-in-define-macro
      a-name proc-list
      header-list param-list
      first-item rest-of-proc
      local-extract-defines-at-one-level)
     (begin
       ;;; then (define func-name (lambda (param-list) expr-block))
       ;;; or (define local-var-name expr-block)
       (let ((dname a-name)
             (expr-block (cddr proc-list)))
         (begin
           (if (and
                (list? expr-block)
                (list? (car expr-block)))
               (begin
                 (let ((next-list (caddr proc-list)))
                   (begin
                     (if (and
                          (list? next-list)
                          (eq? (car next-list) 'lambda))
                         (begin
                           (let ((param-list (cadr next-list)))
                             (begin
                               (set!
                                header-list
                                (list first-item dname))
                               (set!
                                rest-of-proc
                                (cons
                                 (caddr next-list)
                                 (cdddr (caddr proc-list))))

                               (let ((revised-body
                                      (local-extract-defines-at-one-level
                                       rest-of-proc)))
                                 (let ((final-list
                                        (append
                                         header-list
                                         (append
                                          (list
                                           (list
                                            'lambda
                                            param-list
                                            revised-body))))))
                                   (begin
                                     final-list
                                     )))
                               )))
                         (begin
                           (set!
                            header-list
                            (list first-item dname))
                           (set!
                            rest-of-proc (cddr proc-list))

                           (let ((revised-body
                                  (local-extract-defines-at-one-level
                                   rest-of-proc)))
                             (let ((final-list
                                    (append
                                     header-list
                                     (append
                                      (list
                                       (list
                                        'lambda
                                        param-list
                                        revised-body))))))
                               (begin
                                 final-list
                                 )))
                           ))
                     )))
               (begin
                 ;;; or (define local-var-name expr)
                 (set!
                  header-list
                  (list first-item dname))
                 (set!
                  rest-of-proc (cddr proc-list))

                 (let ((revised-body
                        (local-extract-defines-at-one-level
                         rest-of-proc)))
                   (let ((final-list
                          (append
                           header-list
                           revised-body)))
                     (begin
                       final-list
                       )))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (scan-out-defines a-procedure)
  (define (local-initial-header-extract proc-list)
    (begin
      (if (null? proc-list)
          (begin
            proc-list)
          (begin
            (let ((header-list (list))
                  (params-list (list))
                  (rest-of-proc (list)))
              (begin
                (let ((first-item (car proc-list)))
                  (begin
                    (cond
                     ((eq? first-item 'define)
                      (begin
                        (let ((a-name (cadr proc-list)))
                          (begin
                            (if (list? a-name)
                                (begin
                                  ;;; then (define (func-name param-list) expr-block))
                                  (let ((dname (car a-name)))
                                    (begin
                                      (set!
                                       header-list
                                       (list first-item a-name))
                                      (set!
                                       rest-of-proc
                                       (cddr proc-list))

                                      (let ((revised-body
                                             (local-extract-defines-at-one-level
                                              rest-of-proc)))
                                        (let ((final-list
                                               (append
                                                header-list
                                                (append
                                                 (list revised-body)))))
                                          (begin
                                            final-list
                                            )))
                                      )))
                                (begin
                                  (list-in-define-macro
                                   a-name proc-list
                                   header-list param-list
                                   first-item rest-of-proc
                                   local-extract-defines-at-one-level)
                                  ))
                            ))
                        ))
                     ((eq? first-item 'lambda)
                      (begin
                        (let ((params-list (cadr proc-list))
                              (next-list (cddr proc-list)))
                          (begin
                            (set! rest-of-proc next-list)

                            (let ((revised-body
                                   (local-extract-defines-at-one-level
                                    rest-of-proc)))
                              (let ((final-list
                                     (append
                                      header-list
                                      (list 'lambda params-list revised-body))))
                                (begin
                                  final-list
                                  )))
                            ))
                        ))
                     (else
                      (begin
                        (let ((revised-body
                               (local-extract-defines-at-one-level
                                rest-of-proc)))
                          (begin
                            (append header-list revised-body)
                            ))
                        )))
                    ))
                ))
            ))
      ))
  (define (local-define-list this-term)
    (begin
    ;;; then (define (func-name param-list) expr-block)
      (let ((second-item (cadr this-term))
            (next-params (caddr this-term)))
        (let ((dname (car second-item))
              (param-list (cdr second-item))
              (next-expr next-params))
          (let ((revised-body
                 (local-extract-defines-at-one-level next-expr)))
            (let ((this-name-list (list (list dname '*unassigned*)))
                  (this-set-list
                   (list
                    'set! dname
                    (list 'lambda param-list revised-body))))
              (begin
                (list this-name-list this-set-list)
                )))
          ))
      ))
  (define (local-func-lambda this-term)
    (begin
    ;;; then (define func-name (lambda (param-list) expr-block))
      (let ((dname (cadr this-term))
            (second-item (cadr this-term))
            (next-params (caddr this-term))
            (next-expr (caddr this-term)))
        (let ((params-list (cadr next-expr))
              (expr-block (caddr next-expr)))
          (let ((this-name-list
                 (list (list dname '*unassigned*))))
            (let ((revised-body
                   (local-extract-defines-at-one-level
                    expr-block)))
              (let ((this-set-list
                     (list
                      'set! dname
                      (list 'lambda params-list
                            revised-body))))
                (begin
                  (list this-name-list this-set-list)
                  ))
              ))
          ))
      ))
  (define (local-func-expr this-term)
    (begin
    ;;; then (define func-name expr-block)
      (let ((dname (cadr this-term))
            (next-expr (caddr this-term)))
        (let ((this-name-list
               (list (list dname '*unassigned*))))
          (let ((revised-body
                 (local-extract-defines-at-one-level next-expr)))
            (let ((this-set-list
                   (list 'set! dname revised-body)))
              (begin
                (list this-name-list this-set-list)
                )))
          ))
      ))
  (define (local-process-single-term this-term)
    (begin
      (if (list? this-term)
          (begin
            (let ((first-item (car this-term)))
              (begin
                (if (eq? first-item 'define)
                    (begin
                      (let ((second-item (cadr this-term)))
                        (begin
                          (if (list? second-item)
                              (begin
                              ;;; then (define (func-name param-list) expr-block)
                                (let ((name-set-list
                                       (local-define-list this-term)))
                                  (let ((this-name-list
                                         (list-ref name-set-list 0))
                                        (this-set-list
                                         (list-ref name-set-list 1)))
                                    (begin
                                      (list this-name-list this-set-list #f)
                                      ))
                                  ))
                              (begin
                              ;;; then (define func-name (lambda (param-list) expr-block))
                              ;;; or (define local-var-name expr)
                                (let ((next-expr
                                       (car (cddr this-term))))
                                  (begin
                                    (if (and
                                         (list? next-expr)
                                         (eq? (car next-expr) 'lambda))
                                        (begin
                                        ;;; then (define func-name (lambda (param-list) expr-block))
                                          (let ((name-set-list
                                                 (local-func-lambda this-term)))
                                            (let ((this-name-list
                                                   (list-ref name-set-list 0))
                                                  (this-set-list
                                                   (list-ref name-set-list 1)))
                                              (begin
                                                (list this-name-list this-set-list #f)
                                                ))
                                            ))
                                        (begin
                                        ;;; then (define local-var-name expr)
                                          (let ((name-set-list
                                                 (local-func-expr this-term)))
                                            (let ((this-name-list
                                                   (list-ref name-set-list 0))
                                                  (this-set-list
                                                   (list-ref name-set-list 1)))
                                              (begin
                                                (list
                                                 this-name-list this-set-list #f)
                                                )))
                                          ))
                                    ))
                                ))
                          )))
                    (begin
                      (if (list? this-term)
                          (list #f #f (list this-term))
                          (list #f #f this-term))
                      ))
                )))
          (begin
            (list #f #f this-term)
            ))
      ))
  (define (local-extract-defines-at-one-level proc-list)
    (begin
      (if (null? proc-list)
          (begin
            (list))
          (begin
            (let ((name-list (list))
                  (set-list (list))
                  (body-list (list))
                  (llen
                   (if (list? proc-list)
                       (begin
                         (length proc-list))
                       (begin
                         0
                         ))))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((>= ii llen))
                  (begin
                    (let ((this-term (list-ref proc-list ii)))
                      (let ((name-set-body-list
                             (local-process-single-term this-term)))
                        (let ((this-name-list
                               (list-ref name-set-body-list 0))
                              (this-set-list
                               (list-ref name-set-body-list 1))
                              (this-body-list
                               (list-ref name-set-body-list 2)))
                          (begin
                            (if (list? this-name-list)
                                (begin
                                  (set!
                                   name-list
                                   (append name-list this-name-list))
                                  ))
                            (if (list? this-set-list)
                                (begin
                                  (set!
                                   set-list
                                   (cons this-set-list set-list))
                                  ))
                            (if (list? this-body-list)
                                (begin
                                  (set!
                                   body-list
                                   (append body-list this-body-list)))
                                (begin
                                  (if (not (equal? this-body-list #f))
                                      (begin
                                        (set!
                                         body-list
                                         (append body-list (list this-body-list)))
                                        ))
                                  ))
                            ))
                        ))
                    ))

                (if (null? name-list)
                    (begin
                      (if (null? body-list)
                          (begin
                            proc-list)
                          (begin
                            body-list
                            )))
                    (begin
                      (let ((final-list
                             (cons
                              'let
                              (append
                               (list name-list)
                               (reverse set-list)
                               body-list))))
                        (begin
                          final-list
                          ))
                      ))
                ))
            ))
      ))
  (begin
    (let ((result-list
           (local-initial-header-extract a-procedure)))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-scan-out-defines result-hash-table)
 (begin
   (let ((sub-name "test-scan-out-defines")
         (test-list
          (list
           (list
            (list 'define 'avar 33) (list 'define 'avar 33))
           (list
            (list 'define 'aproc
                  (list 'lambda (list 'p1 'p2)
                        (list 'define 'u
                              (list 'lambda
                                    (list 'astring)
                                    (list 'display 'astring)))
                        (list 'begin
                              (list 'newline)
                              (list 'display "hello world!")
                              (list 'u "hello again!")
                              (list 'newline))))
            (list 'define 'aproc
                  (list 'lambda (list 'p1 'p2)
                        (list 'let (list (list 'u '*unassigned*))
                              (list 'set! 'u
                                    (list 'lambda (list 'astring)
                                          (list 'display 'astring)))
                              (list 'begin
                                    (list 'newline)
                                    (list 'display "hello world!")
                                    (list 'u "hello again!")
                                    (list 'newline))))))
           (list
            (list 'define (list 'fib 'n)
                  (list 'define (list 'fib-iter 'a 'b 'count)
                        (list 'if (list '= 'count 0) 'b
                              (list 'fib-iter
                                    (list '+ 'a 'b)
                                    'a (list '- 'count 1))))
                  (list 'fib-iter 0 1 'n))
            (list 'define (list 'fib 'n)
                  (list 'let (list (list 'fib-iter '*unassigned*))
                        (list 'set! 'fib-iter
                              (list 'lambda (list 'a 'b 'count)
                                    (list 'if (list '= 'count 0) 'b
                                          (list 'fib-iter
                                                (list '+ 'a 'b)
                                                'a (list '- 'count 1)))))
                        (list 'fib-iter 0 1 'n))))
           (list
            (list 'lambda (list 'p1 'p2)
                  (list 'define 'uu (list 'expr1))
                  (list 'define 'vv (list 'expr2))
                  (list 'expr3))
            (list 'lambda (list 'p1 'p2)
                  (list 'let (list (list 'uu '*unassigned*)
                                   (list 'vv '*unassigned*))
                        (list 'set! 'uu (list 'expr1))
                        (list 'set! 'vv (list 'expr2))
                        (list 'expr3))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((aproc-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list (scan-out-defines aproc-list)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : aproc-list=~a : "
                        sub-name test-label-index aproc-list)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-procedure parameters body env)
  (begin
    (list 'procedure parameters body env)
    ))

;;;#############################################################
;;;#############################################################
(define (procedure-body p)
  (begin
    (caddr p)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In this exercise we implement the method "))
    (display
     (format #f "just described~%"))
    (display
     (format #f "for interpreting internal definitions. We "))
    (display
     (format #f "assume that the~%"))
    (display
     (format #f "evaluator supports let (see exericse "))
    (display
     (format #f "4.6).~%"))
    (newline)
    (display
     (format #f "a.  Change lookup-variable-value (section "))
    (display
     (format #f "4.1.3) to signal~%"))
    (display
     (format #f "an error if the value it finds is the "))
    (display
     (format #f "symbol *unassigned*.~%"))
    (display
     (format #f "b.  Write a procedure scan-out-defines "))
    (display
     (format #f "that takes a~%"))
    (display
     (format #f "procedure body and returns an equivalent "))
    (display
     (format #f "one that has no~%"))
    (display
     (format #f "internal definitions, by making the "))
    (display
     (format #f "transformation~%"))
    (display
     (format #f "described above.~%"))
    (display
     (format #f "c.  Install scan-out-defines in the "))
    (display
     (format #f "interpreter, either in~%"))
    (display
     (format #f "make-procedure or in procedure-body "))
    (display
     (format #f "(see section 4.1.3).~%"))
    (display
     (format #f "Which place is better? Why?~%"))
    (newline)
    (display
     (format #f "(a)~%"))
    (display
     (format #f "(define (lookup-variable-value "))
    (display
     (format #f "var env)~%"))
    (display
     (format #f "  (define (env-loop env)~%"))
    (display
     (format #f "    (define (scan vars vals)~%"))
    (display
     (format #f "      (cond ((null? vars)~%"))
    (display
     (format #f "             (env-loop "))
    (display
     (format #f "(enclosing-environment env)))~%"))
    (display
     (format #f "            ((eq? var (car vars))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "               (let ((val "))
    (display
     (format #f "(car vals)))~%"))
    (display
     (format #f "                 (if (eq? val "))
    (display
     (format #f "'*unassigned*)~%"))
    (display
     (format #f "                     (begin~%"))
    (display
     (format #f "                       "))
    (display
     (format #f "(error \"Unassigned variable\" var))~%"))
    (display
     (format #f "                     (begin~%"))
    (display
     (format #f "                       val~%"))
    (display
     (format #f "                       )))~%"))
    (display
     (format #f "               ))~%"))
    (display
     (format #f "            (else~%"))
    (display
     (format #f "              (scan (cdr vars) "))
    (display
     (format #f "(cdr vals)))))~%"))
    (display
     (format #f "    (if (eq? env "))
    (display
     (format #f "the-empty-environment)~%"))
    (display
     (format #f "        "))
    (display
     (format #f "(error \"Unbound variable\" var)~%"))
    (display
     (format #f "        (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "          (scan "))
    (display
     (format #f "(frame-variables frame)~%"))
    (display
     (format #f "                "))
    (display
     (format #f "(frame-values frame)))))~%"))
    (display
     (format #f "  (env-loop env))~%"))
    (newline)
    (display
     (format #f "(b)~%"))
    (display
     (format #f "There are a lot of possible definitions "))
    (display
     (format #f "to scan out,~%"))
    (display
     (format #f "so the program is not displayed, just "))
    (display
     (format #f "described (there is~%"))
    (display
     (format #f "about 300 lines of code).~%"))
    (display
     (format #f "(1) The first step is to strip out "))
    (display
     (format #f "the initial~%"))
    (display
     (format #f "definition, so local-initial-header-extract "))
    (display
     (format #f "is called,~%"))
    (display
     (format #f "and returns the transformed expression.~%"))
    (display
     (format #f "local-initial-header-extract handles "))
    (display
     (format #f "define~%"))
    (display
     (format #f "expressions of the form:~%"))
    (newline)
    (display
     (format #f "(define (func-name param-list) expr-block)~%"))
    (display
     (format #f "(define func-name (lambda "))
    (display
     (format #f "(param-list) expr-block))~%"))
    (display
     (format #f "(define local-var-name expr)~%"))
    (display
     (format #f "(lambda <vars> expr-block)~%"))
    (newline)
    (display
     (format #f "(2) once the header information has been "))
    (display
     (format #f "extracted, the~%"))
    (display
     (format #f "rest of the body is scanned for defines, "))
    (display
     (format #f "using the local~%"))
    (display
     (format #f "function~%"))
    (display
     (format #f "local-extract-defines-at-one-level.~%"))
    (display
     (format #f "local-extract-defines-at-one-level loops "))
    (display
     (format #f "through~%"))
    (display
     (format #f "each list to see if it is a definition, "))
    (display
     (format #f "if so it~%"))
    (display
     (format #f "stores the name and set information "))
    (display
     (format #f "needed. These~%"))
    (display
     (format #f "are obtained from local-process-single-term, "))
    (display
     (format #f "which calls~%"))
    (display
     (format #f "local-define-list, local-func-lambda, "))
    (display
     (format #f "local-func-expr,~%"))
    (display
     (format #f "depending on the type of term. These "))
    (display
     (format #f "functions in~%"))
    (display
     (format #f "turn call local-extract-defines-at-one-level~%"))
    (display
     (format #f "to process the body of an internal "))
    (display
     (format #f "definition, if~%"))
    (display
     (format #f "found.~%"))
    (newline)
    (display
     (format #f "(c)~%"))
    (display
     (format #f "It is best to install scan-out-defines in "))
    (display
     (format #f "make-procedure~%"))
    (display
     (format #f "if you are concerned with efficiency and "))
    (display
     (format #f "the procedure-body~%"))
    (display
     (format #f "is called more than once. On the other "))
    (display
     (format #f "hand, you should~%"))
    (display
     (format #f "install scan-out-defines in procedure-body "))
    (display
     (format #f "if you are~%"))
    (display
     (format #f "presenting debugging information, only at "))
    (display
     (format #f "the last moment~%"))
    (display
     (format #f "should you transform the code so that "))
    (display
     (format #f "line numbers~%"))
    (display
     (format #f "associated with the source text can be "))
    (display
     (format #f "accessed.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((an-expr
           (list 'lambda '<vars>
                 (list 'define 'uu '<expr1>)
                 (list 'define 'vv '<expr2>)
                 '<expr3>)))
      (let ((transformed-expr (scan-out-defines an-expr)))
        (begin
          (display
           (format #f "initial expression = ~a~%" an-expr))
          (newline)
          (display
           (format
            #f "scan-out-defines expression = ~a~%"
            transformed-expr))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.16 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
