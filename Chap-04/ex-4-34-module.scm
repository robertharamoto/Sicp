;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.34                                   ###
;;;###                                                       ###
;;;###  last updated July 10, 2024                           ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;;### start exercise 4-34 module
(define-module (ex-4-34-module)
  #:export (run-code
            my-eval
            my-apply
            driver-loop
            ))

;;;#############################################################
;;;#############################################################
(define true #t)
(define false #f)
(define null (list))

;;;#############################################################
;;;#############################################################
(define (my-eval exp env)
  (begin
    (cond
     ((my-self-evaluating? exp)
      (begin
        exp
        ))
     ((my-variable? exp)
      (begin
        (lookup-variable-value exp env)
        ))
     ((quoted? exp)
      (begin
        (text-of-quotation exp env)
        ))
     ((assignment? exp)
      (begin
        (eval-assignment exp env)
        ))
     ((definition? exp)
      (begin
        (eval-definition exp env)
        ))
     ((if? exp)
      (begin
        (eval-if exp env)
        ))
     ((lambda? exp)
      (begin
        (make-procedure
         (lambda-parameters exp)
         (lambda-body exp)
         env)
        ))
     ((begin? exp)
      (begin
        (eval-sequence (begin-actions exp) env)
        ))
     ((cond? exp)
      (begin
        (my-eval (cond->if exp) env)
        ))
     ((application? exp)
      (begin
        (let ((atmp (actual-value (operator exp) env)))
          (let ((result (my-apply atmp (operands exp) env)))
            (begin
              result
              )))
        ))
     ((and (symbol? exp) (eq? exp 'ok))
      (begin
        #t
        ))
     ((equal? exp #t)
      (begin
        #t
        ))
     (else
      (begin
        (display
         (format #f "error exp = ~a~%" exp))
        (force-output)
        (error "Unknown expression type -- MY-EVAL" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define apply-in-underlying-scheme apply)

;;;#############################################################
;;;#############################################################
(define (actual-value exp env)
  (begin
    (force-it (my-eval exp env))
    ))

;;;#############################################################
;;;#############################################################
(define (my-apply procedure arguments env)
  (begin
    (cond
     ((primitive-procedure? procedure)
      (begin
        (apply-primitive-procedure
         procedure
         (list-of-arg-values arguments env))
        ))
     ((compound-procedure? procedure)
      (begin
        (let ((result
               (eval-sequence
                (procedure-body procedure)
                (extend-environment
                 (procedure-parameters procedure)
                 (list-of-delayed-args arguments env)
                 (procedure-environment procedure)))))
          (begin
            (if (and (list? result) (thunk? result))
                (begin
                  (force-it result))
                (begin
                  result
                  ))
            ))
        ))
     ((lambda? procedure)
      (begin
        (let ((result
               (eval-sequence
                (lambda-body procedure)
                (extend-environment
                 (lambda-parameters procedure)
                 (list-of-values arguments env)
                 (lambda-environment procedure)))))
          (begin
            (if (and (list? result) (thunk? result))
                (begin
                  (force-it result))
                (begin
                  result
                  ))
            ))
        ))
     ((and (symbol? procedure)
           (eq? procedure 'ok))
      (begin
        (if (null? arguments)
            (begin
              'ok)
            (begin
              (let ((expr (car arguments)))
                (begin
                  (my-eval arguments env)
                  ))
              ))
        ))
     ((number? procedure)
      (begin
        procedure
        ))
     ((equal? procedure #t)
      (begin
        arguments
        ))
     (else
      (begin
        (error
         "Unknown procedure type -- MY-APPLY"
         procedure)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (list-of-arg-values exps env)
  (begin
    (if (no-operands? exps)
        (begin
          (list))
        (begin
          (cons
           (actual-value (first-operand exps) env)
           (list-of-arg-values
            (rest-operands exps) env))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (list-of-delayed-args exps env)
  (begin
    (cond
     ((no-operands? exps)
      (begin
        (list)
        ))
     (else
      (begin
        (let ((delayed-operand (delay-it (car exps) env))
              (tail-list (cdr exps)))
          (begin
            (cons
             delayed-operand
             (list-of-delayed-args
              tail-list env))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (list-of-values exps env)
  (begin
    (if (no-operands? exps)
        (begin
          (list))
        (begin
          (cons
           (my-eval (car exps) env)
           (list-of-values (cdr exps) env))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (eval-if exp env)
  (begin
    (if (true? (actual-value (if-predicate exp) env))
        (begin
          (my-eval (if-consequent exp) env))
        (begin
          (my-eval (if-alternative exp) env)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (eval-sequence exps env)
  (begin
    (cond
     ((last-exp? exps)
      (begin
        (my-eval (first-exp exps) env)
        ))
     (else
      (begin
        (my-eval (first-exp exps) env)
        (eval-sequence (rest-exps exps) env)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (eval-assignment exp env)
  (begin
    (set-variable-value!
     (assignment-variable exp)
     (my-eval (assignment-value exp) env)
     env)
    'ok
    ))

;;;#############################################################
;;;#############################################################
(define (eval-definition exp env)
  (begin
    (define-variable! (definition-variable exp)
      (my-eval (definition-value exp) env)
      env)
    'ok
    ))

;;;#############################################################
;;;#############################################################
(define (delay-it exp env)
  (begin
    (if (thunk? exp)
        (begin
          exp)
        (begin
          (list 'thunk exp env)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (thunk? obj)
  (begin
    (tagged-list? obj 'thunk)
    ))

;;;#############################################################
;;;#############################################################
(define (thunk-exp thunk)
  (begin
    (cadr thunk)
    ))

;;;#############################################################
;;;#############################################################
(define (thunk-env thunk)
  (begin
    (caddr thunk)
    ))

;;;#############################################################
;;;#############################################################
(define (evaluated-thunk? obj)
  (begin
    (tagged-list? obj 'evaluated-thunk)
    ))

;;;#############################################################
;;;#############################################################
(define (thunk-value evaluated-thunk)
  (begin
    (cadr evaluated-thunk)
    ))

;;;#############################################################
;;;#############################################################
(define (force-it obj)
  (begin
    (cond
     ((thunk? obj)
      (begin
        (let ((result (actual-value
                       (thunk-exp obj)
                       (thunk-env obj))))
          (begin
            (set-car! obj 'evaluated-thunk)
            (set-car! (cdr obj) result)     ; replace exp with its value
            (set-cdr! (cdr obj) (list))     ; forget unneeded env
            result
            ))
        ))
     ((evaluated-thunk? obj)
      (begin
        (thunk-value obj)
        ))
     (else
      (begin
        obj
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (my-self-evaluating? exp)
  (begin
    (cond
     ((number? exp)
      (begin
        true
        ))
     ((string? exp)
      (begin
        true
        ))
     ((or (equal? exp #t) (equal? exp #f))
      (begin
        true
        ))
     (else
      (begin
        false
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (my-variable? exp)
  (begin
    (symbol? exp)
    ))

;;;#############################################################
;;;#############################################################
(define (quoted? exp)
  (begin
    (tagged-list? exp 'quote)
    ))

;;;#############################################################
;;;#############################################################
(define (my-cons x y)
  (begin
    (lambda (m) (m x y))
    ))

;;;#############################################################
;;;#############################################################
(define (my-car z)
  (begin
    (z (lambda (p q) p))
    ))

;;;#############################################################
;;;#############################################################
(define (my-cdr z)
  (begin
    (z (lambda (p q) q))
    ))

;;;#############################################################
;;;#############################################################
(define (my-fold-scheme-to-lazy-list func alist init env)
  (begin
    (if (null? alist)
        (begin
          init)
        (begin
          (func
           (delay-it (car alist) env)
           (my-fold-scheme-to-lazy-list
            func (cdr alist) init env))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (text-of-quotation exp env)
  (begin
    (let ((raw-text (cadr exp)))
      (begin
        (if (list? raw-text)
            (begin
              (let ((result
                     (my-fold-scheme-to-lazy-list
                      my-cons raw-text (list) env)))
                (begin
                  result
                  )))
            (begin
              raw-text
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (tagged-list? exp tag)
  (begin
    (if (pair? exp)
        (begin
          (eq? (car exp) tag))
        (begin
          false
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (assignment? exp)
  (begin
    (tagged-list? exp 'set!)
    ))

;;;#############################################################
;;;#############################################################
(define (assignment-variable exp)
  (begin
    (cadr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (assignment-value exp)
  (begin
    (caddr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (definition? exp)
  (begin
    (tagged-list? exp 'define)
    ))

;;;#############################################################
;;;#############################################################
(define (definition-variable exp)
  (begin
    (if (symbol? (cadr exp))
        (begin
          (cadr exp))
        (begin
          (caadr exp)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (definition-value exp)
  (begin
    (if (symbol? (cadr exp))
        (begin
          (caddr exp))
        (begin
          (make-lambda
           (cdadr exp)   ; formal parameters
           (cddr exp))   ; body
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lambda? exp)
  (begin
    (tagged-list? exp 'lambda)
    ))

;;;#############################################################
;;;#############################################################
(define (lambda-parameters exp)
  (begin
    (cadr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (lambda-body exp)
  (begin
    (cddr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (lambda-environment p)
  (begin
    (cdddr p)
    ))

;;;#############################################################
;;;#############################################################
(define (make-lambda parameters body)
  (begin
    (cons 'lambda (cons parameters body))
    ))

;;;#############################################################
;;;#############################################################
(define (if? exp)
  (begin
    (tagged-list? exp 'if)
    ))

;;;#############################################################
;;;#############################################################
(define (if-predicate exp)
  (begin
    (cadr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (if-consequent exp)
  (begin
    (caddr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (if-alternative exp)
  (begin
    (if (not (null? (cdddr exp)))
        (begin
          (cadddr exp))
        (begin
          'false
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-if predicate consequent alternative)
  (begin
    (list 'if predicate consequent alternative)
    ))

;;;#############################################################
;;;#############################################################
(define (begin? exp)
  (begin
    (tagged-list? exp 'begin)
    ))

;;;#############################################################
;;;#############################################################
(define (begin-actions exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (last-exp? seq)
  (begin
    (null? (cdr seq))
    ))

;;;#############################################################
;;;#############################################################
(define (first-exp seq)
  (begin
    (car seq)
    ))

;;;#############################################################
;;;#############################################################
(define (rest-exps seq)
  (begin
    (cdr seq)
    ))

;;;#############################################################
;;;#############################################################
(define (sequence->exp seq)
  (begin
    (cond
     ((null? seq)
      (begin
        seq
        ))
     ((last-exp? seq)
      (begin
        (first-exp seq)
        ))
     (else
      (begin
        (make-begin seq)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (make-begin seq)
  (begin
    (cons 'begin seq)
    ))

;;;#############################################################
;;;#############################################################
(define (application? exp)
  (begin
    (pair? exp)
    ))

;;;#############################################################
;;;#############################################################
(define (operator exp)
  (begin
    (car exp)
    ))

;;;#############################################################
;;;#############################################################
(define (operands exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (no-operands? ops)
  (begin
    (null? ops)
    ))

;;;#############################################################
;;;#############################################################
(define (first-operand ops)
  (begin
    (car ops)
    ))

;;;#############################################################
;;;#############################################################
(define (rest-operands ops)
  (begin
    (cdr ops)
    ))

;;;#############################################################
;;;#############################################################
(define (cond? exp)
  (begin
    (tagged-list? exp 'cond)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-clauses exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-else-clause? clause)
  (begin
    (eq? (cond-predicate clause) 'else)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-predicate clause)
  (begin
    (car clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-actions clause)
  (begin
    (cdr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond->if exp)
  (begin
    (expand-clauses (cond-clauses exp))
    ))

;;;#############################################################
;;;#############################################################
(define (expand-clauses clauses)
  (begin
    (if (null? clauses)
        (begin
          'false)                          ; no else clause
        (begin
          (let ((first (car clauses))
                (rest (cdr clauses)))
            (begin
              (if (cond-else-clause? first)
                  (begin
                    (if (null? rest)
                        (begin
                          (sequence->exp (cond-actions first)))
                        (begin
                          (error
                           "ELSE clause isn't last -- COND->IF"
                           clauses)
                          )))
                  (begin
                    (make-if
                     (cond-predicate first)
                     (sequence->exp (cond-actions first))
                     (expand-clauses rest))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (true? x)
  (begin
    (not (eq? x #f))
    ))

;;;#############################################################
;;;#############################################################
(define (false? x)
  (begin
    (eq? x #f)
    ))

;;;#############################################################
;;;#############################################################
(define (make-procedure parameters body env)
  (begin
    (list 'procedure parameters body env)
    ))

;;;#############################################################
;;;#############################################################
(define (compound-procedure? p)
  (begin
    (tagged-list? p 'procedure)
    ))

;;;#############################################################
;;;#############################################################
(define (procedure-parameters p)
  (begin
    (cadr p)
    ))

;;;#############################################################
;;;#############################################################
(define (procedure-body p)
  (begin
    (caddr p)
    ))

;;;#############################################################
;;;#############################################################
(define (procedure-environment p)
  (begin
    (cadddr p)
    ))

;;;#############################################################
;;;#############################################################
(define (enclosing-environment env)
  (begin
    (cdr env)
    ))

;;;#############################################################
;;;#############################################################
(define (first-frame env)
  (begin
    (car env)
    ))

;;;#############################################################
;;;#############################################################
(define the-empty-environment (list))

;;;#############################################################
;;;#############################################################
(define (make-frame variables values)
  (begin
    (cons variables values)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-variables frame)
  (begin
    (car frame)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-values frame)
  (begin
    (cdr frame)
    ))

;;;#############################################################
;;;#############################################################
(define (add-binding-to-frame! var val frame)
  (begin
    (set-car! frame (cons var (car frame)))
    (set-cdr! frame (cons val (cdr frame)))
    ))

;;;#############################################################
;;;#############################################################
(define (extend-environment vars vals base-env)
  (begin
    (if (= (length vars) (length vals))
        (begin
          (cons (make-frame vars vals) base-env))
        (begin
          (if (< (length vars) (length vals))
              (begin
                (error
                 "Too many arguments supplied"
                 vars vals))
              (begin
                (error
                 "Too few arguments supplied"
                 vars vals)
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lookup-variable-value var env)
  (define (env-loop env)
    (define (scan vars vals)
      (begin
        (cond
         ((null? vars)
          (begin
            (env-loop (enclosing-environment env))
            ))
         ((eq? var (car vars))
          (begin
            (car vals)
            ))
         (else
          (begin
            (scan (cdr vars) (cdr vals))
            )))
        ))
    (begin
      (if (eq? env the-empty-environment)
          (begin
            (error "Unbound variable" var))
          (begin
            (let ((frame (first-frame env)))
              (begin
                (scan
                 (frame-variables frame)
                 (frame-values frame))
                ))
            ))
      ))
  (begin
    (env-loop env)
    ))

;;;#############################################################
;;;#############################################################
(define (set-variable-value! var val env)
  (define (env-loop env)
    (define (scan vars vals)
      (begin
        (cond
         ((null? vars)
          (begin
            (env-loop (enclosing-environment env))
            ))
         ((eq? var (car vars))
          (begin
            (set-car! vals val)
            ))
         (else
          (begin
            (scan (cdr vars) (cdr vals))
            )))
        ))
    (begin
      (if (eq? env the-empty-environment)
          (begin
            (error
             "Unbound variable -- SET!" var))
          (begin
            (let ((frame (first-frame env)))
              (begin
                (scan
                 (frame-variables frame)
                 (frame-values frame))
                ))
            ))
      ))
  (begin
    (env-loop env)
    ))

;;;#############################################################
;;;#############################################################
(define (define-variable! var val env)
  (begin
    (let ((frame (first-frame env)))
      (begin
        (define (scan vars vals)
          (begin
            (cond
             ((null? vars)
              (begin
                (add-binding-to-frame! var val frame)
                ))
             ((eq? var (car vars))
              (begin
                (set-car! vals val)
                ))
             (else
              (begin
                (scan (cdr vars) (cdr vals))
                )))
            ))
        (scan
         (frame-variables frame)
         (frame-values frame))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (primitive-procedure? proc)
  (begin
    (tagged-list? proc 'primitive)
    ))

;;;#############################################################
;;;#############################################################
(define (primitive-implementation proc)
  (begin
    (cadr proc)
    ))

;;;#############################################################
;;;#############################################################
(define primitive-procedures
  (list (list 'car car)
        (list 'cdr cdr)
        (list 'cons cons)
        (list 'list list)
        (list 'list-ref list-ref)
        (list 'null? null?)
        (list 'null (list))
        (list 'display display)
        (list 'newline newline)
        (list 'format format)
        (list 'force-output force-output)
        (list '+ +)
        (list '- -)
        (list '/ /)
        (list '* *)
        (list '< <)
        (list '<= <=)
        (list '= =)
        (list '> >)
        (list '>= >=)
        (list '1+ 1+)
        (list '1- 1-)
        (list 'zero? zero?)
        (list 'modulo modulo)
        (list 'inexact->exact inexact->exact)
        (list 'truncate truncate)
        (list 'sqrt sqrt)
        (list 'my-cons my-cons)
        (list 'my-car my-car)
        (list 'my-cdr my-cdr)
        (list 'delay-it delay-it)
        ))

;;;#############################################################
;;;#############################################################
(define (primitive-procedure-names)
  (begin
    (map
     car
     primitive-procedures)
    ))

;;;#############################################################
;;;#############################################################
(define (primitive-procedure-objects)
  (begin
    (map
     (lambda (proc)
       (begin
         (list 'primitive (cadr proc))
         )) primitive-procedures)
    ))

;;;#############################################################
;;;#############################################################
(define (apply-primitive-procedure proc args)
  (begin
    (apply-in-underlying-scheme
     (primitive-implementation proc) args)
    ))

;;;#############################################################
;;;#############################################################
(define (setup-environment)
  (begin
    (let ((initial-env
           (extend-environment
            (primitive-procedure-names)
            (primitive-procedure-objects)
            the-empty-environment)))
      (begin
        (define-variable! 'true #t initial-env)
        (define-variable! 'false #f initial-env)
        initial-env
        ))
    ))

;;;#############################################################
;;;#############################################################
(define the-global-environment (setup-environment))

;;;#############################################################
;;;#############################################################
(define (lazy-list-to-string a-lazy-list)
  (begin
    (let ((first-elem
           (force-it
            (my-car a-lazy-list)))
          (second-elem
           (force-it
            (my-car (force-it (my-cdr a-lazy-list))))))
      (begin
        (format
         #f "(~a ~a ,...)" first-elem second-elem)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define input-prompt ";;; L-Eval input:")

;;;#############################################################
;;;#############################################################
(define output-prompt ";;; L-Eval value:")

;;;#############################################################
;;;#############################################################
(define (driver-loop)
  (begin
    (prompt-for-input input-prompt)
    (let ((input (read)))
      (begin
        (if (my-variable? input)
            (begin
              (let ((value
                     (lookup-variable-value
                      input the-global-environment)))
                (begin
                  (if (procedure? value)
                      (begin
                        (display
                         (format
                          #f "~a = ~a~%"
                          input (lazy-list-to-string value)))
                        (force-output))
                      (begin
                        value
                        ))
                  )))
            (begin
              (let ((output
                     (actual-value
                      input the-global-environment)))
                (begin
                  (announce-output output-prompt)
                  (user-print output)
                  ))
              ))
        (driver-loop)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (prompt-for-input string)
  (begin
    (newline)
    (newline)
    (display string)
    (newline)
    ))

;;;#############################################################
;;;#############################################################
(define (announce-output string)
  (begin
    (newline)
    (display string)
    (newline)
    ))

;;;#############################################################
;;;#############################################################
(define (user-print object)
  (begin
    (if (compound-procedure? object)
        (begin
          (display
           (list
            'compound-procedure
            (procedure-parameters object)
            (procedure-body object)
            '<procedure-env>)))
        (begin
          (display object)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define the-global-environment (setup-environment))

;;;#############################################################
;;;#############################################################
(define (run-code sexp)
  (begin
    (my-eval sexp the-global-environment)
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
