#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.04                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (eval-expression exp env)
  (begin
    (apply
     (eval (operator exp) env)
     (list-of-values (operands exp) env))
    ))

;;;#############################################################
;;;#############################################################
(define (and-form-1 expr-list env)
  (begin
    (if (null? expr-list)
        (begin
          #t)
        (begin
          (let ((this-expr (car expr-list))
                (tail-list (cdr expr-list)))
            (let ((aresult (eval-expression this-expr env)))
              (begin
                (if (equal? aresult #f)
                    (begin
                      #f)
                    (begin
                      (and aresult (and-form-1 tail-list env))
                      ))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (or-form-1 expr-list env)
  (begin
    (if (null? expr-list)
        (begin
          #f)
        (begin
          (let ((this-expr (car expr-list))
                (tail-list (cdr expr-list)))
            (let ((aresult
                   (eval-expression this-expr env)))
              (begin
                (if (equal? aresult #t)
                    (begin
                      #t)
                    (begin
                      (or aresult (or-form-1 tail-list env))
                      ))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (eval exp env)
  (begin
    (cond
     ((self-evaluating? exp)
      (begin
        exp
        ))
     ((variable? exp)
      (begin
        (lookup-variable-value exp env)
        ))
     ((quoted? exp)
      (begin
        (text-of-quotation exp)
        ))
     ((assignment? exp)
      (begin
        (eval-assignment exp env)
        ))
     ((definition? exp)
      (begin
        (eval-definition exp env)
        ))
     ((if? exp)
      (begin
        (eval-if exp env)
        ))
     ((and-form? exp)
      (begin
        (and-form-1 exp env)
        ))
     ((or-form? exp)
      (begin
        (or-form-1 exp env)
        ))
     ((lambda? exp)
      (begin
        (make-procedure
         (lambda-parameters exp)
         (lambda-body exp)
         env)
        ))
     ((begin? exp)
      (begin
        (eval-sequence (begin-actions exp) env)
        ))
     ((cond? exp)
      (begin
        (eval (cond->if exp) env)
        ))
     ((application? exp)
      (begin
        (apply (eval (operator exp) env)
               (list-of-values (operands exp) env))
        ))
     (else
      (begin
        (error "Unknown expression type -- EVAL" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Recall the definitions of the special "))
    (display
     (format #f "forms and and~%"))
    (display
     (format #f "or from chapter 1.~%"))
    (newline)
    (display
     (format #f "  and: The expressions are evaluated "))
    (display
     (format #f "from left to~%"))
    (display
     (format #f "right. If any expression evaluates "))
    (display
     (format #f "to false, false~%"))
    (display
     (format #f "is returned; any remaining expressions "))
    (display
     (format #f "are not evaluated.~%"))
    (display
     (format #f "If all the expressions evaluate to true "))
    (display
     (format #f "values, the value~%"))
    (display
     (format #f "of the last expression is returned. If "))
    (display
     (format #f "there are no~%"))
    (display
     (format #f "expressions then true is "))
    (display
     (format #f "returned.~%"))
    (newline)
    (display
     (format #f "  or: The expressions are evaluated from "))
    (display
     (format #f "left to right.~%"))
    (display
     (format #f "If any expression evaluates to a true "))
    (display
     (format #f "value, that value~%"))
    (display
     (format #f "is returned; any remaining expressions "))
    (display
     (format #f "are not evaluated.~%"))
    (display
     (format #f "If all expressions evaluate to false, or "))
    (display
     (format #f "if there are no~%"))
    (display
     (format #f "expressions, then false is returned. "))
    (display
     (format #f "Install and and~%"))
    (display
     (format #f "or as new special forms for the evaluator "))
    (display
     (format #f "by defining~%"))
    (display
     (format #f "appropriate syntax procedures and "))
    (display
     (format #f "evaluation procedures~%"))
    (display
     (format #f "eval-and and eval-or. Alternatively, show "))
    (display
     (format #f "how to~%"))
    (display
     (format #f "implement and and or as derived "))
    (display
     (format #f "expressions.~%"))
    (newline)
    (display
     (format #f "(define (eval-expression exp env)~%"))
    (display
     (format #f "  (apply~%"))
    (display
     (format #f "   (eval (operator exp) env)~%"))
    (display
     (format #f "   (list-of-values (operands exp) "))
    (display
     (format #f "env)~%"))
    (display
     (format #f "   ))~%"))
    (display
     (format #f "(define (and-form-1 "))
    (display
     (format #f "expr-list env)~%"))
    (display
     (format #f "  (if (null? expr-list)~%"))
    (display
     (format #f "      #t~%"))
    (display
     (format #f "      (let ((this-expr "))
    (display
     (format #f "(car expr-list))~%"))
    (display
     (format #f "            (tail-list "))
    (display
     (format #f "(cdr expr-list)))~%"))
    (display
     (format #f "        (let ((aresult "))
    (display
     (format #f "(eval-expression this-expr env)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (if (equal? aresult #f)~%"))
    (display
     (format #f "                #f~%"))
    (display
     (format #f "                (and aresult "))
    (display
     (format #f "(and-form-1 tail-list env)))~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "(define (or-form-1 expr-list env)~%"))
    (display
     (format #f "  (if (null? expr-list)~%"))
    (display
     (format #f "      #f~%"))
    (display
     (format #f "      (let ((this-expr "))
    (display
     (format #f "(car expr-list))~%"))
    (display
     (format #f "            (tail-list "))
    (display
     (format #f "(cdr expr-list)))~%"))
    (display
     (format #f "        (let ((aresult "))
    (display
     (format #f "(eval-expression this-expr env)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (if (equal? "))
    (display
     (format #f "aresult #t)~%"))
    (display
     (format #f "                #t~%"))
    (display
     (format #f "                (or aresult "))
    (display
     (format #f "(or-form-1 tail-list env)))~%"))
    (display
     (format #f "            )))~%"))
    (display
     (format #f "      ))~%"))
    (display
     (format #f "(define (eval exp env)~%"))
    (display
     (format #f "  (cond ((self-evaluating? exp)"))
    (display
     (format #f "exp)~%"))
    (display
     (format #f "        ((variable? exp) "))
    (display
     (format #f "(lookup-variable-value exp env))~%"))
    (display
     (format #f "        ((quoted? exp) "))
    (display
     (format #f "(text-of-quotation exp))~%"))
    (display
     (format #f "        ((assignment? exp) "))
    (display
     (format #f "(eval-assignment exp env))~%"))
    (display
     (format #f "        ((definition? exp) "))
    (display
     (format #f "(eval-definition exp env))~%"))
    (display
     (format #f "        ((if? exp) "))
    (display
     (format #f "(eval-if exp env))~%"))
    (display
     (format #f "        ((and-form? exp) "))
    (display
     (format #f "(and-form-1 exp env))~%"))
    (display
     (format #f "        ((or-form? exp) "))
    (display
     (format #f "(or-form-1 exp env))~%"))
    (display
     (format #f "        ((lambda? exp)~%"))
    (display
     (format #f "         (make-procedure "))
    (display
     (format #f "(lambda-parameters exp)~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(lambda-body exp)~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "env))~%"))
    (display
     (format #f "        ((begin? exp)~%"))
    (display
     (format #f "         (eval-sequence "))
    (display
     (format #f "(begin-actions exp) env))~%"))
    (display
     (format #f "        ((cond? exp) "))
    (display
     (format #f "(eval (cond->if exp) env))~%"))
    (display
     (format #f "        ((application? exp)~%"))
    (display
     (format #f "         (apply (eval "))
    (display
     (format #f "(operator exp) env)~%"))
    (display
     (format #f "                (list-of-values "))
    (display
     (format #f "(operands exp) env)))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         "))
    (display
     (format #f "(error \"Unknown expression "))
    (display
     (format #f "type -- EVAL\" exp))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.04 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
