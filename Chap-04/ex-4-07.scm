#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.07                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (let*->nested-lets let-list)
  (define (local-iter var-list acc-list)
    (begin
      (if (null? var-list)
          (begin
            acc-list)
          (begin
            (let ((this-pair (car var-list))
                  (tail-list (cdr var-list)))
              (begin
                (local-iter
                 tail-list
                 (list 'let (list this-pair) acc-list))
                ))
            ))
      ))
  (begin
    (let ((rev-var-list (reverse (cadr let-list)))
          (body-list (caddr let-list)))
      (let ((acc-list (local-iter rev-var-list body-list)))
        (begin
          acc-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-let*-to-nested-lets result-hash-table)
 (begin
   (let ((sub-name "test-let*-to-nested-lets")
         (test-list
          (list
           (list
            (list 'let* (list (list 'z 1)) (list 'begin (list newline)))
            (list 'let (list (list 'z 1)) (list 'begin (list newline))))
           (list
            (list 'let* (list (list 'x 3) (list 'y 4)) (list 'begin (list newline)))
            (list 'let (list (list 'x 3))
                  (list 'let (list (list 'y 4))
                        (list 'begin (list newline)))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((sexp (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list (let*->nested-lets sexp)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : sexp=~a, "
                        sub-name test-label-index sexp)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list result-list
                     sub-name err-msg-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (let*-form? let-list)
  (begin
    (eq? (car let-list) 'let*)
    ))

;;;#############################################################
;;;#############################################################
(define (eval exp env)
  (begin
    (cond
     ((self-evaluating? exp)
      (begin
        exp
        ))
     ((variable? exp)
      (begin
        (lookup-variable-value exp env)
        ))
     ((quoted? exp)
      (begin
        (text-of-quotation exp)
        ))
     ((assignment? exp)
      (begin
        (eval-assignment exp env)
        ))
     ((definition? exp)
      (begin
        (eval-definition exp env)
        ))
     ((if? exp)
      (begin
        (eval-if exp env)
        ))
     ((let*-form? exp)
      (begin
        (eval (let*->combination exp env))
        ))
     ((lambda? exp)
      (begin
        (make-procedure
         (lambda-parameters exp)
         (lambda-body exp)
         env)
        ))
     ((begin? exp)
      (begin
        (eval-sequence (begin-actions exp) env)
        ))
     ((cond? exp)
      (begin
        (eval (cond->if exp) env)
        ))
     ((application? exp)
      (begin
        (apply (eval (operator exp) env)
               (list-of-values (operands exp) env))
        ))
     (else
      (begin
        (error "Unknown expression type -- EVAL" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (cond? exp)
  (begin
    (tagged-list? exp 'cond)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-clauses exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-else-clause? clause)
  (begin
    (eq? (cond-predicate clause) 'else)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-predicate clause)
  (begin
    (car clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-actions clause)
  (begin
    (cdr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-recipient-clause? clause)
  (begin
    (eq? (car (cond-actions clause)) '=>)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-recipient-action clause)
  (begin
    (cddr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond->if exp)
  (begin
    (expand-clauses (cond-clauses exp))
    ))

;;;#############################################################
;;;#############################################################
(define (expand-clauses clauses)
  (begin
    (if (null? clauses)
        (begin
          'false)                          ; no else clause
        (begin
          (let ((first (car clauses))
                (rest (cdr clauses)))
            (begin
              (if (cond-else-clause? first)
                  (begin
                    (if (null? rest)
                        (begin
                          (sequence->exp (cond-actions first)))
                        (begin
                          (error
                           "ELSE clause isn't last -- COND->IF"
                           clauses)
                          )))
                  (begin
                    (if (and
                         (cond-recipient-clause? first)
                         (procedure? (cond-recipient-action first)))
                        (begin
                          (make-if
                           (cond-predicate first)
                           (list
                            cond-recipient-action
                            (list
                             (cond-predicate first)
                             cond-recipient-action))
                           (expand-clauses rest)))
                        (begin
                          (make-if
                           (cond-predicate first)
                           (sequence->exp
                            (cond-actions first))
                           (expand-clauses rest))
                          ))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let* is similar to let, except that the "))
    (display
     (format #f "bindings of the~%"))
    (display
     (format #f "let variables are performed sequentially "))
    (display
     (format #f "from left to~%"))
    (display
     (format #f "right, and each binding is made in an "))
    (display
     (format #f "environment in which~%"))
    (display
     (format #f "all of the preceding bindings are visible. "))
    (display
     (format #f "For example:~%"))
    (newline)
    (display
     (format #f "(let* ((x 3)~%"))
    (display
     (format #f "       (y (+ x 2))~%"))
    (display
     (format #f "       (z (+ x y 5)))~%"))
    (display
     (format #f "  (* x z))~%"))
    (newline)
    (display
     (format #f "returns 39. Explain how a let* expression "))
    (display
     (format #f "can be rewritten~%"))
    (display
     (format #f "as a set of nested let expressions, and "))
    (display
     (format #f "write a procedure~%"))
    (display
     (format #f "let*->nested-lets that performs this "))
    (display
     (format #f "transformation. If~%"))
    (display
     (format #f "we have already implemented let (exercise "))
    (display
     (format #f "4.6) and we want~%"))
    (display
     (format #f "to extend the evaluator to handle let*, "))
    (display
     (format #f "is it sufficient~%"))
    (display
     (format #f "to add a clause to eval whose action is~%"))
    (display
     (format #f "(eval (let*->nested-lets exp) env)~%"))
    (display
     (format #f "or must we explicitly expand let* in "))
    (display
     (format #f "terms of non-derived~%"))
    (display
     (format #f "expressions?~%"))
    (newline)
    (display
     (format #f "(let* ((<var-1> <exp-1>) ... "))
    (display
     (format #f "(<var-n> <exp-n>)) <body>)~%"))
    (display
     (format #f "is equivalent to:~%"))
    (display
     (format #f "(let ((<var-1> <exp-1>))~%"))
    (display
     (format #f "  (let ((<var-2> <exp-2))~%"))
    (display
     (format #f " ... (let ((<var-n> <exp-n>))~%"))
    (display
     (format #f "         <body>) ... ))~%"))
    (newline)
    (display
     (format #f "since each variable can be used "))
    (display
     (format #f "by subsequent variables.~%"))
    (newline)
    (display
     (format #f "(define (let*->nested-lets let-list)~%"))
    (display
     (format #f "  (define (local-iter "))
    (display
     (format #f "var-list acc-list)~%"))
    (display
     (format #f "    (if (null? var-list)~%"))
    (display
     (format #f "        acc-list~%"))
    (display
     (format #f "        (let ((this-pair "))
    (display
     (format #f "(car var-list))~%"))
    (display
     (format #f "              (tail-list "))
    (display
     (format #f "(cdr var-list)))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (local-iter~%"))
    (display
     (format #f "             tail-list~%"))
    (display
     (format #f "             (list 'let "))
    (display
     (format #f "(list this-pair) acc-list))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "  (let ((rev-var-list "))
    (display
     (format #f "(reverse (cadr let-list)))~%"))
    (display
     (format #f "        (body-list "))
    (display
     (format #f "(caddr let-list)))~%"))
    (display
     (format #f "    (let ((acc-list "))
    (display
     (format #f "(local-iter rev-var-list "))
    (display
     (format #f "body-list)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        acc-list~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "In the function eval, it is sufficient "))
    (display
     (format #f "to use the~%"))
    (display
     (format #f "expression (eval (let*->nested-lets exp) "))
    (display
     (format #f "env), since~%"))
    (display
     (format #f "let*->nested-lets turns the expression "))
    (display
     (format #f "exp into~%"))
    (display
     (format #f "a nested let expression, and eval can "))
    (display
     (format #f "then process the~%"))
    (display
     (format #f "nested let expression "))
    (display
     (format #f "recursively.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.07 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
