#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.34                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;### ex-4-34-module - lazy-pairs evaluation
(use-modules ((ex-4-34-module)
              :renamer (symbol-prefix-proc 'ex-4-34-module:)))

;;;#############################################################
;;;#############################################################
(define (experiment-list-ref-integers)
  (begin
    (let ((list-ref-list
           (list
            (list 'define (list 'my-cons-2 'x 'y)
                  (list 'lambda (list 'm) (list 'm 'x 'y)))
            (list 'define (list 'my-car-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'p)))
            (list 'define (list 'my-cdr-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'q)))
            (list 'define (list 'my-list-ref 'items 'n)
                  (list 'if (list '<= 'n 0)
                        (list 'my-car-2 'items)
                        (list 'begin
                              (list 'my-list-ref
                                    (list 'my-cdr-2 'items)
                                    (list '- 'n 1)))))
            (list 'define (list 'my-map 'proc 'items)
                  (list 'if (list 'null? 'items)
                        (list)
                        (list
                         'my-cons-2
                         (list 'proc (list 'my-car-2 'items))
                         (list 'my-map 'proc
                               (list 'my-cdr-2 'items)))))
            (list 'define
                  (list 'scale-list 'items 'factor)
                  (list
                   'my-map
                   (list 'lambda (list 'x)
                         (list '* 'x 'factor))
                   'items))
            (list 'define
                  (list 'add-lists 'list1 'list2)
                  (list 'cond
                        (list (list 'null? 'list1) 'list2)
                        (list (list 'null? 'list2) 'list1)
                        (list
                         'else
                         (list 'my-cons-2
                               (list '+ (list 'my-car-2 'list1)
                                     (list 'my-car-2 'list2))
                               (list 'add-lists (list 'my-cdr-2 'list1)
                                     (list 'my-cdr-2 'list2))))))
            (list 'define 'ones (list 'my-cons-2 1 'ones))
            (list 'define 'my-integers
                  (list 'my-cons-2
                        1 (list 'add-lists 'ones 'my-integers)))
            (list 'define 'yy (list 'my-list-ref 'my-integers 17))
            (list 'begin
                  (list 'display
                        (list
                         'format #f "(list-ref integers 17) = ~a~%" 'yy))
                  (list 'force-output)
                  #t)
            )))
      (begin
        (display (format #f "list-ref test~%"))
        (ex-4-34-module:run-code list-ref-list)
        (gc)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-integral)
  (begin
    (let ((int-list
           (list
            (list 'define (list 'my-cons-2 'x 'y)
                  (list 'lambda (list 'm) (list 'm 'x 'y)))
            (list 'define (list 'my-car-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'p)))
            (list 'define (list 'my-cdr-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'q)))
            (list 'define (list 'my-list-ref 'items 'n)
                  (list 'if (list '= 'n 0)
                        (list 'my-car-2 'items)
                        (list 'begin
                              (list
                               'my-list-ref
                               (list 'my-cdr-2 'items)
                               (list '- 'n 1)))))
            (list 'define (list 'my-map 'proc 'items)
                  (list 'if (list 'null? 'items)
                        (list)
                        (list 'my-cons-2
                              (list 'proc (list 'my-car-2 'items))
                              (list 'my-map 'proc
                                    (list 'my-cdr-2 'items)))))
            (list 'define
                  (list 'scale-list 'items 'factor)
                  (list 'my-map
                        (list 'lambda
                              (list 'x) (list '* 'x 'factor))
                        'items))
            (list 'define (list 'add-lists 'list1 'list2)
                  (list 'cond (list (list 'null? 'list1) 'list2)
                        (list (list 'null? 'list2) 'list1)
                        (list
                         'else
                         (list 'my-cons-2
                               (list '+ (list 'my-car-2 'list1)
                                     (list 'my-car-2 'list2))
                               (list 'add-lists (list 'my-cdr-2 'list1)
                                     (list 'my-cdr-2 'list2))))))
            (list 'define (list 'integral 'integrand 'initial-value 'dt)
                  (list 'define 'int
                        (list 'my-cons-2 'initial-value
                              (list 'add-lists
                                    (list 'scale-list 'integrand 'dt)
                                    'int)))
                  'int)
            (list 'define (list 'solve 'f 'y0 'dt)
                  (list 'define 'y
                        (list 'integral 'dy 'y0 'dt))
                  (list 'define 'dy (list 'my-map 'f 'y))
                  'y)
            (list 'define 'result
                  (list 'my-list-ref
                        (list 'solve (list 'lambda (list 'x) 'x)
                              1 0.010) 100))
            (list 'begin
                  (list
                   'display
                   (list
                    'format #f
                    (string-append
                     "(my-list-ref (solve (lambda (x) x)"
                     " 1 0.0010) 100) = ~a~%")
                    'result))
                  (list 'force-output)
                  #t)
            )))
      (begin
        (display (format #f "integral test~%"))
        (force-output)
        (ex-4-34-module:run-code int-list)
        (gc)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-car-list)
  (begin
    (let ((car-list
           (list
            (list 'define 'alist ''('a 'b 'c))
            (list 'begin
                  (list 'display
                        (list
                         'format #f "alist = '(a b c)~%"))
                  (list 'display
                        (list
                         'format #f "(car alist) = ~a~%"
                         (list 'my-car 'alist)))
                  (list 'display
                        (list
                         'format #f "(car (cdr alist)) = ~a~%"
                         (list 'my-car (list 'my-cdr 'alist))))
                  (list 'display
                        (list
                         'format #f "(car (cdr (cdr alist))) = ~a~%"
                         (list 'my-car
                               (list 'my-cdr
                                     (list 'my-cdr 'alist)))))
                  (list 'force-output)
                  #t
                  ))))
      (begin
        (display (format #f "car list test~%"))
        (force-output)
        (ex-4-34-module:run-code car-list)
        (gc)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Modify the driver loop for the evaluator "))
    (display
     (format #f "so that lazy~%"))
    (display
     (format #f "pairs and lists will print in some "))
    (display
     (format #f "reasonable way. (What~%"))
    (display
     (format #f "are you going to do about infinite lists?) "))
    (display
     (format #f "You may also~%"))
    (display
     (format #f "need to modify the representation of lazy "))
    (display
     (format #f "pairs so that~%"))
    (display
     (format #f "the evaluator can identify them "))
    (display
     (format #f "order to print them.~%"))
    (newline)
    (display
     (format #f "guile --no-auto-compile~%"))
    (display
     (format #f "(assumes guile 3.0)~%"))
    (display
     (format #f "scheme@(guile-user)> "))
    (display
     (format #f "(add-to-load-path \".\")~%"))
    (display
     (format #f "scheme@(guile-user)> (use-modules "))
    (display
     (format #f "((ex-4-34-module)))~%"))
    (display
     (format #f "scheme@(guile-user)> (driver-loop)~%"))
    (display
     (format #f ";;; L-Eval input:~%"))
    (display
     (format #f "(define alist '('a 'b 'c))~%"))
    (display
     (format #f ";;; L-Eval input:~%"))
    (display
     (format #f "alist~%"))
    (display
     (format #f "alist = (a b ,...)~%"))
    (display
     (format #f ";;; L-Eval input:~%"))
    (newline)
    (display
     (format #f "(define (lazy-list-to-string "))
    (display
     (format #f "a-lazy-list)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (let ((first-elem "))
    (display
     (format #f "(force-it (my-car a-lazy-list)))~%"))
    (display
     (format #f "          (second-elem "))
    (display
     (format #f "(force-it (my-car (force-it "))
    (display
     (format #f "(my-cdr a-lazy-list))))))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        "))
    (display
     (format #f "(format #f \"(~~a ~~a ,...)\"~%"))
    (display
     (format #f "        "))
    (display
     (format #f "  first-elem second-elem)~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "(define (driver-loop)~%"))
    (display
     (format #f "  (prompt-for-input input-prompt)~%"))
    (display
     (format #f "  (let ((input (read)))~%"))
    (display
     (format #f "    (begin~%"))
    (display
     (format #f "      (if (my-variable? input)~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((value~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(lookup-variable-value~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "input the-global-environment)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (if (procedure? value)~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      (display~%"))
    (display
     (format #f "                         "))
    (display
     (format #f "(format #f \"~~a = ~~a~%\"~%"))
    (display
     (format #f "                             "))
    (display
     (format #f "input (lazy-list-to-string value)))~%"))
    (display
     (format #f "                      "))
    (display
     (format #f "(force-output))~%"))
    (display
     (format #f "                    (begin~%"))
    (display
     (format #f "                      value~%"))
    (display
     (format #f "                      ))~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "          (begin~%"))
    (display
     (format #f "            (let ((output~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(actual-value input "))
    (display
     (format #f "the-global-environment)))~%"))
    (display
     (format #f "              (announce-output "))
    (display
     (format #f "output-prompt)~%"))
    (display
     (format #f "              (user-print output))~%"))
    (display
     (format #f "            ))~%"))
    (display
     (format #f "      (driver-loop)~%"))
    (display
     (format #f "      )))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (experiment-list-ref-integers)
    (experiment-integral)
    (newline)

    (experiment-car-list)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.34 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
