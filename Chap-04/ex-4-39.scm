#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.39                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Does the order of the restrictions in "))
    (display
     (format #f "the~%"))
    (display
     (format #f "multiple-dwelling procedure affect the "))
    (display
     (format #f "answer? Does~%"))
    (display
     (format #f "it affect the time to find an answer? "))
    (display
     (format #f "If you think~%"))
    (display
     (format #f "it demonstrates a faster program "))
    (display
     (format #f "obtained from the~%"))
    (display
     (format #f "given one by reordering the restrictions. "))
    (display
     (format #f "If you~%"))
    (display
     (format #f "think it does not matter, argue "))
    (display
     (format #f "your case.~%"))
    (newline)

    (display
     (format #f "Here is what I believe is a faster "))
    (display
     (format #f "arrangement to~%"))
    (display
     (format #f "solve the puzzle:~%"))
    (newline)
    (display
     (format #f "(define (multiple-dwelling)~%"))
    (display
     (format #f "  (let ((baker (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (cooper (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (fletcher (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (miller (amb 1 2 3 4 5))~%"))
    (display
     (format #f "        (smith (amb 1 2 3 4 5)))~%"))
    (display
     (format #f "    (require~%"))
    (display
     (format #f "     (distinct?~%"))
    (display
     (format #f "       (list baker cooper "))
    (display
     (format #f "fletcher miller smith)))~%"))
    (display
     (format #f "    (require (not (= fletcher 5)))~%"))
    (display
     (format #f "    (require (not (= fletcher 1)))~%"))
    (display
     (format #f "    (require (not (= cooper 1)))~%"))
    (display
     (format #f "    (require (not (= (abs "))
    (display
     (format #f "(- fletcher cooper)) 1)))~%"))
    (display
     (format #f "    (require (> miller cooper))~%"))
    (display
     (format #f "    (require (not (= baker 5)))~%"))
    (display
     (format #f "    (list (list 'baker baker)~%"))
    (display
     (format #f "          (list 'cooper cooper)~%"))
    (display
     (format #f "          (list 'fletcher fletcher)~%"))
    (display
     (format #f "          (list 'miller miller)~%"))
    (display
     (format #f "          (list 'smith smith))))~%"))
    (newline)
    (display
     (format #f "This order is preferred, it's best to "))
    (display
     (format #f "start with fletcher,~%"))
    (display
     (format #f "cooper, and miller, since they have the "))
    (display
     (format #f "most restrictions~%"))
    (display
     (format #f "of all. Once fletcher is fixed, then "))
    (display
     (format #f "cooper has just 1~%"))
    (display
     (format #f "or 2 places to be, and considering "))
    (display
     (format #f "miller's restriction,~%"))
    (display
     (format #f "means that cooper can't be on floor 5, "))
    (display
     (format #f "and miller can't~%"))
    (display
     (format #f "be on floor 2.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.39 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
