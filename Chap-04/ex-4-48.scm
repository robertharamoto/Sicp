#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.48                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Extend the grammar given above to handle "))
    (display
     (format #f "more complex~%"))
    (display
     (format #f "sentences. For example, you could extend "))
    (display
     (format #f "noun phrases~%"))
    (display
     (format #f "and verb phrases to include adjectives "))
    (display
     (format #f "and adverbs,~%"))
    (display
     (format #f "or you could handle compound "))
    (display
     (format #f "sentences.~%"))
    (newline)
    (display
     (format #f "(define adjectives '(adjectives "))
    (display
     (format #f "strong weak happy sad))~%"))
    (display
     (format #f "(define adverbs '(adverbs fast "))
    (display
     (format #f "slowly softly loudly quietly)~%"))
    (newline)
    (display
     (format #f "(define (parse-simple-noun-phrase)~%"))
    (display
     (format #f "  (list 'simple-noun-phrase~%"))
    (display
     (format #f "        (parse-word articles)~%"))
    (display
     (format #f "        (parse-word adjectives)~%"))
    (display
     (format #f "        (parse-word nouns)))~%"))
    (display
     (format #f "(define (parse-noun-phrase)~%"))
    (display
     (format #f "  (define (maybe-extend noun-phrase)~%"))
    (display
     (format #f "    (amb noun-phrase~%"))
    (display
     (format #f "         (maybe-extend (list 'noun-phrase~%"))
    (display
     (format #f "                             "))
    (display
     (format #f "noun-phrase~%"))
    (display
     (format #f "                             "))
    (display
     (format #f "(parse-prepositional-phrase)))))~%"))
    (display
     (format #f "  (maybe-extend "))
    (display
     (format #f "(parse-simple-noun-phrase)))~%"))
    (newline)
    (display
     (format #f "(define (parse-simple-verb-phrase)~%"))
    (display
     (format #f "  (list 'simple-verb-phrase~%"))
    (display
     (format #f "        (parse-word verbs)~%"))
    (display
     (format #f "        (parse-word adverbs)))~%"))
    (display
     (format #f "(define (parse-verb-phrase)~%"))
    (display
     (format #f "  (define (maybe-extend verb-phrase)~%"))
    (display
     (format #f "    (amb verb-phrase~%"))
    (display
     (format #f "         (maybe-extend "))
    (display
     (format #f "(list 'verb-phrase~%"))
    (display
     (format #f "                              "))
    (display
     (format #f "verb-phrase~%"))
    (display
     (format #f "                             "))
    (display
     (format #f "(parse-prepositional-phrase)))))~%"))
    (display
     (format #f "  (maybe-extend "))
    (display
     (format #f "(parse-simple-verb-phrase)))~%"))
    (newline)
    (display
     (format #f "The key is in the parse-word function, "))
    (display
     (format #f "which require's~%"))
    (display
     (format #f "that (memq (car *unparsed*) (cdr word-list)), "))
    (display
     (format #f "so that if~%"))
    (display
     (format #f "a word is not in the word list, the "))
    (display
     (format #f "global *unparsed*~%"))
    (display
     (format #f "variable will remain unaffected.~%"))
    (newline)
    (display
     (format #f "Such a parser can handle the sentence:~%"))
    (display
     (format #f "'the happy cat sleeps quietly'~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.48 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
