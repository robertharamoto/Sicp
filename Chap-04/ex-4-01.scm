#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.01                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (list-of-values-l-to-r exps env)
  (begin
    (if (no-operands? exps)
        (begin
          (list))
        (begin
          (let ((result (list))
                (llen (length exps)))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii llen))
                (begin
                  (let ((a1 (eval (list-ref exps ii) env)))
                    (begin
                      (set! result (append result (list a1)))
                      ))
                  ))
              result
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (list-of-values-r-to-l exps env)
  (begin
    (if (no-operands? exps)
        (begin
          (list))
        (begin
          (let ((result (list))
                (llen (length exps)))
            (begin
              (do ((ii (1- llen) (1- ii)))
                  ((< ii 0))
                (begin
                  (let ((a1 (eval (list-ref exps ii) env)))
                    (begin
                      (set! result (cons a1 result))
                      ))
                  ))
              result
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Notice that we cannot tell whether "))
    (display
     (format #f "the metacircular~%"))
    (display
     (format #f "evaluator evaluates operands from left "))
    (display
     (format #f "to right or from~%"))
    (display
     (format #f "right to left. Its evaluation order "))
    (display
     (format #f "is inherited from the~%"))
    (display
     (format #f "underlying Lisp: If the arguments "))
    (display
     (format #f "to cons in~%"))
    (display
     (format #f "list-of-values are evaluated from left "))
    (display
     (format #f "to right, then~%"))
    (display
     (format #f "list-of-values will evaluate operands "))
    (display
     (format #f "from left to~%"))
    (display
     (format #f "right; and if the arguments to cons "))
    (display
     (format #f "are evaluated from~%"))
    (display
     (format #f "right to left, then list-of-values "))
    (display
     (format #f "will evaluate operands~%"))
    (display
     (format #f "from right to left.~%"))
    (newline)
    (display
     (format #f "Write a version of list-of-values that "))
    (display
     (format #f "evaluates operands~%"))
    (display
     (format #f "from left to right regardless of the "))
    (display
     (format #f "order of evaluation~%"))
    (display
     (format #f "in the underlying Lisp. Also write a "))
    (display
     (format #f "version of~%"))
    (display
     (format #f "list-of-values that evaluates "))
    (display
     (format #f "operands from right~%"))
    (display
     (format #f "to left.~%"))
    (newline)
    (display
     (format #f "(define (list-of-values-l-to-r "))
    (display
     (format #f "exps env)~%"))
    (display
     (format #f "  (if (no-operands? exps)~%"))
    (display
     (format #f "      (list)~%"))
    (display
     (format #f "      (let ((result (list))~%"))
    (display
     (format #f "            (llen (length exps)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (do ((ii 0 (1+ ii)))~%"))
    (display
     (format #f "              ((>= ii llen))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((a1 (eval "))
    (display
     (format #f "(list-ref exps ii) env)))~%"))
    (display
     (format #f "                (set! result "))
    (display
     (format #f "(append result (list a1))))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          result~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      ))~%"))
    (newline)
    (display
     (format #f "(define (list-of-values-r-to-l "))
    (display
     (format #f "exps env)~%"))
    (display
     (format #f "  (if (no-operands? exps)~%"))
    (display
     (format #f "      (list)~%"))
    (display
     (format #f "      (let ((result (list))~%"))
    (display
     (format #f "            (llen (length exps)))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (do ((ii (1- llen) "))
    (display
     (format #f "(1- ii)))~%"))
    (display
     (format #f "              ((< ii 0))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((a1 (eval "))
    (display
     (format #f "(list-ref exps ii) env)))~%"))
    (display
     (format #f "                (set! result "))
    (display
     (format #f "(cons a1 result)))~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "          result~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.01 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
