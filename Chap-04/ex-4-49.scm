#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.49                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Alyssa P. Hacker is more interested in "))
    (display
     (format #f "generating~%"))
    (display
     (format #f "interesting sentences than in parsing "))
    (display
     (format #f "them. She reasons~%"))
    (display
     (format #f "that by simply changing the procedure "))
    (display
     (format #f "parse-word so~%"))
    (display
     (format #f "that it ignores the \"input sentence\" "))
    (display
     (format #f "and instead always~%"))
    (display
     (format #f "succeeds and generates an appropriate "))
    (display
     (format #f "word, we can~%"))
    (display
     (format #f "use the programs we had built for "))
    (display
     (format #f "parsing to do~%"))
    (display
     (format #f "generation instead. Implement Alyssa's "))
    (display
     (format #f "idea, and show~%"))
    (display
     (format #f "the first half-dozen or so sentences "))
    (display
     (format #f "generated.~%"))
    (newline)
    (display
     (format #f "(define (gen-word word-list)~%"))
    (display
     (format #f "  (let ((wlen (length "))
    (display
     (format #f "(cdr word-list)))~%"))
    (display
     (format #f "        (wtype "))
    (display
     (format #f "(car word-list))~%"))
    (display
     (format #f "        (words "))
    (display
     (format #f "(cdr word-list)))~%"))
    (display
     (format #f "    (let ((an-expr "))
    (display
     (format #f "(append (list amb) words)))~%"))
    (display
     (format #f "      (eval an-expr)~%"))
    (display
     (format #f "      )))~%"))
    (display
     (format #f "(define nouns '(noun student "))
    (display
     (format #f "professor cat class))~%"))
    (display
     (format #f "(define verbs '(verb studies "))
    (display
     (format #f "lectures eats sleeps))~%"))
    (display
     (format #f "(define articles '(article the a))~%"))
    (display
     (format #f "(define prepositions '(prep for "))
    (display
     (format #f "to in by with))~%"))
    (newline)
    (display
     (format #f "(1) (sentence (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun student))~%"))
    (display
     (format #f "      (verbs studies))~%"))
    (display
     (format #f "(2) (sentence (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun student))~%"))
    (display
     (format #f "      (verb lectures))~%"))
    (display
     (format #f "(3) (sentence (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun student))~%"))
    (display
     (format #f "      (verb eats))~%"))
    (display
     (format #f "(4) (sentence (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun student))~%"))
    (display
     (format #f "      (verb sleeps))~%"))
    (display
     (format #f "(5) (sentence (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun professor))~%"))
    (display
     (format #f "      (verb studies))~%"))
    (display
     (format #f "(6) (sentence (simple-noun-phrase "))
    (display
     (format #f "(article the) (noun professor))~%"))
    (display
     (format #f "      (verb lectures))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.49 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
