#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.32                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;### ex-4-32-module - lazy-pairs evaluation
(use-modules ((ex-4-32-module)
              :renamer (symbol-prefix-proc 'ex-4-32-module:)))

;;;#############################################################
;;;#############################################################
(define (experiment-list-ref-integers)
  (begin
    (let ((list-ref-list
           (list
            (list 'define (list 'my-cons 'x 'y)
                  (list 'lambda (list 'm) (list 'm 'x 'y)))
            (list 'define (list 'my-car 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'p)))
            (list 'define (list 'my-cdr 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'q)))
            (list 'define (list 'my-list-ref 'items 'n)
                  (list 'if (list '<= 'n 0)
                        (list 'my-car 'items)
                        (list
                         'begin
                         (list 'my-list-ref (list 'my-cdr 'items)
                               (list '- 'n 1)))))
            (list 'define (list 'my-map 'proc 'items)
                  (list 'if (list 'null? 'items)
                        (list)
                        (list
                         'my-cons
                         (list 'proc (list 'my-car 'items))
                         (list 'my-map 'proc
                               (list 'my-cdr 'items)))))
            (list
             'define (list 'scale-list 'items 'factor)
             (list 'my-map
                   (list 'lambda (list 'x) (list '* 'x 'factor))
                   'items))
            (list
             'define (list 'add-lists 'list1 'list2)
             (list 'cond (list (list 'null? 'list1) 'list2)
                   (list (list 'null? 'list2) 'list1)
                   (list
                    'else
                    (list 'my-cons
                          (list '+ (list 'my-car 'list1)
                                (list 'my-car 'list2))
                          (list 'add-lists (list 'my-cdr 'list1)
                                (list 'my-cdr 'list2))))))
            (list 'define 'ones
                  (list 'my-cons 1 'ones))
            (list 'define 'my-integers
                  (list 'my-cons 1
                        (list 'add-lists 'ones 'my-integers)))
            (list
             'define 'yy (list 'my-list-ref 'my-integers 17))
            (list 'begin
                  (list
                   'display
                   (list
                    'format
                    #f "(list-ref integers 17) = ~a~%" 'yy))
                  (list 'force-output)
                  #t)
            )))
      (begin
        (display
         (format #f "list-ref test~%"))
        (ex-4-32-module:run-code list-ref-list)
        (gc)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-integral)
  (begin
    (let ((int-list
           (list
            (list 'define (list 'my-cons 'x 'y)
                  (list 'lambda (list 'm) (list 'm 'x 'y)))
            (list 'define (list 'my-car 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'p)))
            (list 'define (list 'my-cdr 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'q)))
            (list 'define (list 'my-list-ref 'items 'n)
                  (list 'if (list '= 'n 0)
                        (list 'my-car 'items)
                        (list 'begin
                              (list 'my-list-ref
                                    (list 'my-cdr 'items)
                                    (list '- 'n 1)))))
            (list 'define (list 'my-map 'proc 'items)
                  (list 'if (list 'null? 'items)
                        (list)
                        (list 'my-cons
                              (list 'proc (list 'my-car 'items))
                              (list 'my-map 'proc
                                    (list 'my-cdr 'items)))))
            (list 'define (list 'scale-list 'items 'factor)
                  (list 'my-map
                        (list 'lambda (list 'x)
                              (list '* 'x 'factor))
                        'items))
            (list 'define (list 'add-lists 'list1 'list2)
                  (list
                   'cond (list (list 'null? 'list1) 'list2)
                   (list (list 'null? 'list2) 'list1)
                   (list 'else
                         (list 'my-cons
                               (list '+ (list 'my-car 'list1)
                                     (list 'my-car 'list2))
                               (list 'add-lists (list 'my-cdr 'list1)
                                     (list 'my-cdr 'list2))))))
            (list
             'define (list 'integral 'integrand
                           'initial-value 'dt)
             (list 'define 'int
                   (list 'my-cons 'initial-value
                         (list 'add-lists
                               (list 'scale-list 'integrand 'dt)
                               'int)))
             'int)
            (list 'define (list 'solve 'f 'y0 'dt)
                  (list 'define 'y (list 'integral 'dy 'y0 'dt))
                  (list 'define 'dy (list 'my-map 'f 'y))
                  'y)
            (list 'define 'result
                  (list 'my-list-ref
                        (list 'solve (list 'lambda (list 'x) 'x)
                              1 0.010) 100))
            (list 'begin
                  (list 'display
                        (list
                         'format #f
                         (string-append
                          "(my-list-ref (solve (lambda (x) x) "
                          "1 0.0010) 1000) = ~a~%")
                         'result))
                  (list 'force-output)
                  #t)
            )))
      (begin
        (display (format #f "integral test~%"))
        (force-output)
        (ex-4-32-module:run-code int-list)
        (gc)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Give some examples that illustrate the "))
    (display
     (format #f "difference between~%"))
    (display
     (format #f "the streams of chapter 3 and the "))
    (display
     (format #f "\"lazier\" lazy lists~%"))
    (display
     (format #f "described in this section. How can you "))
    (display
     (format #f "take advantage of~%"))
    (display
     (format #f "this extra laziness?~%"))
    (newline)
    (display
     (format #f "An example is the integral function, "))
    (display
     (format #f "which requires a~%"))
    (display
     (format #f "delayed-integrand in chapter 3. This "))
    (display
     (format #f "delayed integrand is~%"))
    (display
     (format #f "then forced, before it's scaled by "))
    (display
     (format #f "dt. The lazy lists~%"))
    (display
     (format #f "method does not need the integrand "))
    (display
     (format #f "to be forced, it~%"))
    (display
     (format #f "directly scales the lazy lists.~%"))
    (newline)
    (display
     (format #f "To take advantage of the extra laziness, "))
    (display
     (format #f "consider the eval~%"))
    (display
     (format #f "function, and the "))
    (display
     (format #f "list-of-delayed-arguments. You no longer~%"))
    (display
     (format #f "need to delay each (delay-it "))
    (display
     (format #f "(car (operand arg-list)))~%"))
    (display
     (format #f "since the car would automatically be "))
    (display
     (format #f "delayed. Another example~%"))
    (display
     (format #f "is the solve function which calls the "))
    (display
     (format #f "integral function. The~%"))
    (display
     (format #f "streams version needs to call integral "))
    (display
     (format #f "with an explicit call~%"))
    (display
     (format #f "to (delay dy), since the arguments to "))
    (display
     (format #f "integral are not~%"))
    (display
     (format #f "lazily evaluated. With lazy pairs, one "))
    (display
     (format #f "doesn't have to call~%"))
    (display
     (format #f "delay at all.~%"))
    (newline)
    (display
     (format #f "streams:~%"))
    (display
     (format #f "(define (solve f y0 dt)~%"))
    (display
     (format #f "  (define y (integral "))
    (display
     (format #f "(delay dy) y0 dt))~%"))
    (display
     (format #f "  (define dy (stream-map f y))~%"))
    (display
     (format #f "  y)~%"))
    (newline)
    (display
     (format #f "lazy pairs:~%"))
    (display
     (format #f "(define (solve f y0 dt)~%"))
    (display
     (format #f "  (define y (integral dy y0 dt))~%"))
    (display
     (format #f "  (define dy (map f y))~%"))
    (display
     (format #f "  y)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (experiment-list-ref-integers)
    (newline)
    (experiment-integral)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.32 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
