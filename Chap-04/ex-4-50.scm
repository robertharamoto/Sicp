#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.50                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Implement a new special form ramb that "))
    (display
     (format #f "is like amb~%"))
    (display
     (format #f "except that it searches alternatives in "))
    (display
     (format #f "a random order,~%"))
    (display
     (format #f "rather than from left to right. Show "))
    (display
     (format #f "how this can~%"))
    (display
     (format #f "help with Alyssa's problem in "))
    (display
     (format #f "exercise 4.49.~%"))
    (newline)
    (display
     (format #f "(define (ramb? exp) (tagged-list? exp 'ramb))~%"))
    (display
     (format #f "(define (ramb-choices exp) (cdr exp))~%"))
    (display
     (format #f "((ramb? exp) (analyze-ramb exp))~%"))
    (display
     (format #f "(define (rambeval exp env succeed fail)~%"))
    (display
     (format #f "  ((analyze exp) env succeed fail))~%"))
    (display
     (format #f "(define (analyze-ramb exp)~%"))
    (display
     (format #f "  (let ((cprocs (map analyze "))
    (display
     (format #f "(ramb-choices exp))))~%"))
    (display
     (format #f "    (lambda (env succeed fail)~%"))
    (display
     (format #f "      (define (try-next choices)~%"))
    (display
     (format #f "        (if (null? choices)~%"))
    (display
     (format #f "            (fail)~%"))
    (display
     (format #f "            (let ((clen "))
    (display
     (format #f "(length choices)))~%"))
    (display
     (format #f "              (let ((nn (random clen)))~%"))
    (display
     (format #f "                (let ((this-elem "))
    (display
     (format #f "(list-ref choices nn)))~%"))
    (display
     (format #f "                  (let ((next-choices "))
    (display
     (format #f "(delete this-elem choices)))~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(this-elem env~%"))
    (display
     (format #f "                                "))
    (display
     (format #f "succeed~%"))
    (display
     (format #f "                                "))
    (display
     (format #f "(lambda ()~%"))
    (display
     (format #f "                                  "))
    (display
     (format #f "(try-next next-choices)))~%"))
    (display
     (format #f "                    ))))))~%"))
    (display
     (format #f "      (try-next cprocs))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.50 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
