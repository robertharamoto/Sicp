#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.19                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle, Alyssa P. Hacker, and Eva "))
    (display
     (format #f "Lu Ator are~%"))
    (display
     (format #f "arguing about the desired result of "))
    (display
     (format #f "evaluating the~%"))
    (display
     (format #f "expression:~%"))
    (newline)
    (display
     (format #f "(let ((a 1))~%"))
    (display
     (format #f "  (define (f x)~%"))
    (display
     (format #f "    (define b (+ a x))~%"))
    (display
     (format #f "    (define a 5)~%"))
    (display
     (format #f "    (+ a b))~%"))
    (display
     (format #f "  (f 10))~%"))
    (newline)
    (display
     (format #f "Ben asserts that the result should be "))
    (display
     (format #f "obtained using the~%"))
    (display
     (format #f "sequential rule for define: b is defined "))
    (display
     (format #f "to be 11, then~%"))
    (display
     (format #f "a is defined to be 5, so the result is "))
    (display
     (format #f "16. Alyssa objects~%"))
    (display
     (format #f "that mutual recursion requires the "))
    (display
     (format #f "simultaneous scope~%"))
    (display
     (format #f "rule for internal procedure definitions, "))
    (display
     (format #f "and that it~%"))
    (display
     (format #f "is unreasonable to treat procedure names "))
    (display
     (format #f "differently from~%"))
    (display
     (format #f "other names. Thus, she argues for the "))
    (display
     (format #f "mechanism implemented~%"))
    (display
     (format #f "in exercise 4.16. This would lead to 'a "))
    (display
     (format #f "being unassigned~%"))
    (display
     (format #f "at the time that the value for b is to "))
    (display
     (format #f "be computed. Hence,~%"))
    (display
     (format #f "in Alyssa's view the procedure should "))
    (display
     (format #f "produce an error.~%"))
    (display
     (format #f "Eva has a third opinion. She says that if "))
    (display
     (format #f "the definitions~%"))
    (display
     (format #f "of a and b are truly meant to be "))
    (display
     (format #f "simultaneous, then the~%"))
    (display
     (format #f "value 5 for a should be used in evaluating b. "))
    (display
     (format #f "Hence, in~%"))
    (display
     (format #f "Eva's view a should be 5, b should be 15, "))
    (display
     (format #f "and the result~%"))
    (display
     (format #f "should be 20. Which (if any), of these "))
    (display
     (format #f "viewpoints do~%"))
    (display
     (format #f "you support? Can you devise a way "))
    (display
     (format #f "to implement internal~%"))
    (display
     (format #f "definitions so that they behave as Eva "))
    (display
     (format #f "prefers?~%"))
    (newline)
    (display
     (format #f "Similar to the comment in footnote 26, "))
    (display
     (format #f "I support~%"))
    (display
     (format #f "Alyssa's viewpoint, that the mechanism "))
    (display
     (format #f "of exercise 4.16~%"))
    (display
     (format #f "leads to 'a being used when it's "))
    (display
     (format #f "'*unassigned*, and~%"))
    (display
     (format #f "so should signal an error.~%"))
    (newline)
    (display
     (format #f "To implement Eva's truly simultaneous "))
    (display
     (format #f "method would~%"))
    (display
     (format #f "involve evaluating set!'s in a manner "))
    (display
     (format #f "similar to solving~%"))
    (display
     (format #f "sudoku puzzles. That is, to have a way "))
    (display
     (format #f "to try a~%"))
    (display
     (format #f "combination, then back out of it if "))
    (display
     (format #f "it produces an~%"))
    (display
     (format #f "error. For example, try to evaluate "))
    (display
     (format #f "'b first (order~%"))
    (display
     (format #f "and 'a '*unassigned*, which gives an "))
    (display
     (format #f "error. Next try to~%"))
    (display
     (format #f "evaluate 'a (5), and then evaluating "))
    (display
     (format #f "'b (15), with a~%"))
    (display
     (format #f "return value of 20. However, this "))
    (display
     (format #f "method is complex~%"))
    (display
     (format #f "and may be difficult for the programmer "))
    (display
     (format #f "to understand,~%"))
    (display
     (format #f "especially if there are three or more "))
    (display
     (format #f "recursive~%"))
    (display
     (format #f "definitions.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.19 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
