#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.33                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;### ex-4-33-module - lazy-pairs evaluation
(use-modules ((ex-4-33-module)
              :renamer (symbol-prefix-proc 'ex-4-33-module:)))

;;;#############################################################
;;;#############################################################
(define (experiment-list-ref-integers)
  (begin
    (let ((list-ref-list
           (list
            (list 'define (list 'my-cons-2 'x 'y)
                  (list 'lambda (list 'm) (list 'm 'x 'y)))
            (list 'define (list 'my-car-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'p)))
            (list 'define (list 'my-cdr-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'q)))
            (list 'define (list 'my-list-ref 'items 'n)
                  (list 'if (list '<= 'n 0)
                        (list 'my-car-2 'items)
                        (list 'begin
                              (list 'my-list-ref
                                    (list 'my-cdr-2 'items)
                                    (list '- 'n 1)))))
            (list 'define (list 'my-map 'proc 'items)
                  (list 'if (list 'null? 'items)
                        (list)
                        (list 'my-cons-2
                              (list 'proc
                                    (list 'my-car-2 'items))
                              (list 'my-map 'proc
                                    (list 'my-cdr-2 'items)))))
            (list 'define (list 'scale-list 'items 'factor)
                  (list 'my-map
                        (list 'lambda (list 'x)
                              (list '* 'x 'factor))
                        'items))
            (list 'define (list 'add-lists 'list1 'list2)
                  (list 'cond (list (list 'null? 'list1) 'list2)
                        (list (list 'null? 'list2) 'list1)
                        (list 'else
                              (list
                               'my-cons-2
                               (list '+ (list 'my-car-2 'list1)
                                     (list 'my-car-2 'list2))
                               (list 'add-lists (list 'my-cdr-2 'list1)
                                     (list 'my-cdr-2 'list2))))))
            (list 'define 'ones (list 'my-cons-2 1 'ones))
            (list 'define 'my-integers
                  (list 'my-cons-2
                        1 (list 'add-lists 'ones 'my-integers)))
            (list 'define
                  'yy (list 'my-list-ref 'my-integers 17))
            (list 'begin
                  (list
                   'display
                   (list
                    'format #f "(list-ref integers 17) = ~a~%" 'yy))
                  (list 'force-output)
                  #t)
            )))
      (begin
        (display (format #f "list-ref test~%"))
        (ex-4-33-module:run-code list-ref-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-integral)
  (begin
    (let ((int-list
           (list
            (list 'define (list 'my-cons-2 'x 'y)
                  (list 'lambda (list 'm) (list 'm 'x 'y)))
            (list 'define (list 'my-car-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'p)))
            (list 'define (list 'my-cdr-2 'z)
                  (list 'z (list 'lambda (list 'p 'q) 'q)))
            (list 'define (list 'my-list-ref 'items 'n)
                  (list 'if (list '= 'n 0)
                        (list 'my-car-2 'items)
                        (list 'begin
                              (list 'my-list-ref
                                    (list 'my-cdr-2 'items)
                                    (list '- 'n 1)))))
            (list 'define (list 'my-map 'proc 'items)
                  (list 'if (list 'null? 'items)
                        (list)
                        (list 'my-cons-2
                              (list 'proc (list 'my-car-2 'items))
                              (list 'my-map 'proc
                                    (list 'my-cdr-2 'items)))))
            (list 'define (list 'scale-list 'items 'factor)
                  (list 'my-map
                        (list 'lambda
                              (list 'x) (list '* 'x 'factor))
                        'items))
            (list 'define (list 'add-lists 'list1 'list2)
                  (list
                   'cond (list (list 'null? 'list1) 'list2)
                   (list (list 'null? 'list2) 'list1)
                   (list
                    'else
                    (list 'my-cons-2
                          (list '+ (list 'my-car-2 'list1)
                                (list 'my-car-2 'list2))
                          (list 'add-lists (list 'my-cdr-2 'list1)
                                (list 'my-cdr-2 'list2))))))
            (list 'define (list 'integral 'integrand 'initial-value 'dt)
                  (list 'define 'int
                        (list 'my-cons-2 'initial-value
                              (list
                               'add-lists
                               (list 'scale-list 'integrand 'dt)
                               'int)))
                  'int)
            (list 'define (list 'solve 'f 'y0 'dt)
                  (list 'define 'y (list 'integral 'dy 'y0 'dt))
                  (list 'define 'dy (list 'my-map 'f 'y))
                  'y)
            (list 'define 'result
                  (list 'my-list-ref
                        (list 'solve (list 'lambda (list 'x) 'x)
                              1 0.010) 100))
            (list 'begin
                  (list
                   'display
                   (list
                    'format #f
                    (string-append
                     "(my-list-ref (solve (lambda (x) x) "
                     "1 0.0010) 100) = ~a~%")
                    'result))
                  (list 'force-output)
                  #t)
            )))
      (begin
        (display (format #f "integral test~%"))
        (force-output)
        (gc)
        (ex-4-33-module:run-code int-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-car-list)
  (begin
    (let ((car-list
           (list
            (list 'define 'alist ''('a 'b 'c))
            (list 'begin
                  (list 'display
                        (list 'format #f "alist = '(a b c)~%"))
                  (list 'display
                        (list 'format #f "(car alist) = ~a~%"
                              (list 'my-car 'alist)))
                  (list 'display
                        (list 'format #f "(car (cdr alist)) = ~a~%"
                              (list 'my-car (list 'my-cdr 'alist))))
                  (list 'display
                        (list 'format
                              #f "(car (cdr (cdr alist))) = ~a~%"
                              (list 'my-car
                                    (list 'my-cdr
                                          (list 'my-cdr 'alist)))))
                  (list 'force-output)
                  #t
                  ))
           ))
      (begin
        (display (format #f "car list test~%"))
        (force-output)
        (gc)
        (ex-4-33-module:run-code car-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Ben Bitdiddle tests the lazy list "))
    (display
     (format #f "implementation given~%"))
    (display
     (format #f "above by evaluating the expression "))
    (display
     (format #f "(car '(a b c))~%"))
    (display
     (format #f "To his surprise, this produces an "))
    (display
     (format #f "error. After some~%"))
    (display
     (format #f "thought, he realizes that the "))
    (display
     (format #f "\"lists\" obtained by reading~%"))
    (display
     (format #f "in quoted expressions are different "))
    (display
     (format #f "from the lists~%"))
    (display
     (format #f "manipulated by the new definitions of "))
    (display
     (format #f "cons, car, and cdr.~%"))
    (display
     (format #f "Modify the evaluator's treatment of "))
    (display
     (format #f "quoted expressions~%"))
    (display
     (format #f "so that quoted lists typed at the "))
    (display
     (format #f "driver loop will~%"))
    (display
     (format #f "produce true lazy lists.~%"))
    (newline)
    (display
     (format #f "Note: there are 3 versions of "))
    (display
     (format #f "cons/car/cdr used in this~%"))
    (display
     (format #f "program. Guile's cons/car/cdr is used "))
    (display
     (format #f "to process the input~%"))
    (display
     (format #f "list. The module's my-cons/my-car/my-cdr "))
    (display
     (format #f "is used to put~%"))
    (display
     (format #f "the raw text into a text list, and uses "))
    (display
     (format #f "applicative order.~%"))
    (display
     (format #f "The final version of my-cons-2/my-car-2/"))
    (display
     (format #f "my-cdr-2 are~%"))
    (display
     (format #f "used in the interpreted text, so that "))
    (display
     (format #f "those arguments will~%"))
    (display
     (format #f "be delayed.~%"))
    (newline)
    (display
     (format #f "(define (my-fold-scheme-to-lazy-list "))
    (display
     (format #f "func alist init env)~%"))
    (display
     (format #f "  (if (null? alist)~%"))
    (display
     (format #f "      init~%"))
    (display
     (format #f "      (func (delay-it "))
    (display
     (format #f "(car alist) env)~%"))
    (display
     (format #f "            "))
    (display
     (format #f "(my-fold-scheme-to-lazy-list~%"))
    (display
     (format #f "             "))
    (display
     (format #f "func (cdr alist) init env))~%"))
    (display
     (format #f "      ))~%"))
    (newline)
    (display
     (format #f "(define (text-of-quotation "))
    (display
     (format #f "exp env)~%"))
    (display
     (format #f "  (begin~%"))
    (display
     (format #f "    (let ((raw-text (cadr exp)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (if (list? raw-text)~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              (let ((result~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(my-fold-scheme-to-lazy-list~%"))
    (display
     (format #f "                      "))
    (display
     (format #f "my-cons raw-text (list) env)))~%"))
    (display
     (format #f "                (begin~%"))
    (display
     (format #f "                  result~%"))
    (display
     (format #f "                  )))~%"))
    (display
     (format #f "            (begin~%"))
    (display
     (format #f "              raw-text~%"))
    (display
     (format #f "              ))~%"))
    (display
     (format #f "        ))~%"))
    (display
     (format #f "    ))~%"))
    (newline)
    (display
     (format #f "In addition, in the apply function, there "))
    (display
     (format #f "needs to be~%"))
    (display
     (format #f "new code to handle the returned "))
    (display
     (format #f "function from cons~%"))
    (newline)
    (display
     (format #f "        ((lambda? procedure)~%"))
    (display
     (format #f "         (begin~%"))
    (display
     (format #f "           (let ((result~%"))
    (display
     (format #f "                  (eval-sequence~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(lambda-body procedure)~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "(extend-environment~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "(lambda-parameters procedure)~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "(list-of-values arguments env)~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "(lambda-environment procedure)))))~%"))
    (display
     (format #f "             (begin~%"))
    (display
     (format #f "               "))
    (display
     (format #f "(if (and (list? result) "))
    (display
     (format #f "(thunk? result))~%"))
    (display
     (format #f "                   (begin~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(force-it result))~%"))
    (display
     (format #f "                   (begin~%"))
    (display
     (format #f "                     result~%"))
    (display
     (format #f "                     ))~%"))
    (display
     (format #f "               ))~%"))
    (display
     (format #f "           ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (experiment-list-ref-integers)
    (newline)
    (experiment-integral)
    (newline)
    (experiment-car-list)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.33 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (display
           (format #f "scheme test~%"))
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
