#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.20                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (letrec->let proc-list)
  (define (local-transform-vars proc-list)
    (begin
      (let ((name-list (list))
            (set-list (list))
            (llen
             (if (list? proc-list)
                 (begin
                   (length proc-list))
                 (begin
                   0
                   ))))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii llen))
            (begin
              (let ((var-exp-pair (list-ref proc-list ii)))
                (let ((var-ii (car var-exp-pair))
                      (expr-ii (cadr var-exp-pair)))
                  (begin
                    (let ((this-name
                           (list var-ii '*unassigned*))
                          (revised-expr
                           (local-transform-body expr-ii (list))))
                      (let ((this-set
                             (list 'set! var-ii revised-expr)))
                        (begin
                          (set!
                           name-list
                           (append name-list (list this-name)))
                          (set!
                           set-list
                           (append set-list (list this-set)))
                          )))
                    )))
              ))
          (if (null? name-list)
              (begin
                proc-list)
              (begin
                (let ((final-list
                       (append
                        (list 'let name-list) set-list)))
                  (begin
                    final-list
                    ))
                ))
          ))
      ))
  (define (local-transform-body proc-list acc-list)
    (begin
      (if (null? proc-list)
          (begin
            (reverse acc-list))
          (begin
            (if (list? proc-list)
                (begin
                  (let ((this-elem (car proc-list))
                        (tail-list (cdr proc-list)))
                    (begin
                      (if (eq? this-elem 'letrec)
                          (begin
                            (let ((result-list
                                   (local-transform-top proc-list)))
                              (begin
                                result-list
                                )))
                          (begin
                            (local-transform-body
                             tail-list (cons this-elem acc-list))
                            ))
                      )))
                (begin
                  proc-list
                  ))
            ))
      ))
  (define (local-transform-top proc-list)
    (begin
      (if (eq? (car proc-list) 'letrec)
          (begin
            (let ((var-list (cadr proc-list))
                  (body-list (caddr proc-list)))
              (let ((result-list
                     (local-transform-vars var-list))
                    (revised-body-list
                     (local-transform-body body-list (list))))
                (begin
                  (append result-list (list revised-body-list))
                  ))
              ))
          (begin
            (local-tranform-body proc-list)
            ))
      ))
  (begin
    (let ((result-list
           (local-transform-top proc-list)))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append
                     err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-letrec->let result-hash-table)
 (begin
   (let ((sub-name "test-letrec->let")
         (test-list
          (list
           (list
            (list 'letrec (list (list 'var1 '<expr1>)) '<body>)
            (list 'let (list (list 'var1 '*unassigned*))
                  (list 'set! 'var1 '<expr1>) '<body>))
           (list
            (list 'letrec (list (list 'var1 '<expr1>)
                                (list 'var2 '<expr2>)) '<body>)
            (list 'let (list (list 'var1 '*unassigned*)
                             (list 'var2 '*unassigned*))
                  (list 'set! 'var1 '<expr1>)
                  (list 'set! 'var2 '<expr2>)
                  '<body>))
           (list
            (list 'letrec (list (list 'var1 '<expr1>)
                                (list 'var2 '<expr2>))
                  (list 'letrec (list (list 'var3 '<expr3>)) '<body>))
            (list 'let (list (list 'var1 '*unassigned*)
                             (list 'var2 '*unassigned*))
                  (list 'set! 'var1 '<expr1>)
                  (list 'set! 'var2 '<expr2>)
                  (list 'let (list (list 'var3 '*unassigned*))
                        (list 'set! 'var3 '<expr3>) '<body>)))
           (list
            (list 'letrec
                  (list
                   (list 'fact
                         (list 'lambda (list 'n)
                               (list
                                'if (list '= 'n 1) 1
                                (list
                                 '* 'n
                                 (list
                                  'fact (list '- 'n 1))
                                 ))
                               )))
                  (list 'fact 10))
            (list 'let (list (list 'fact '*unassigned*))
                  (list 'set! 'fact
                        (list 'lambda (list 'n)
                              (list
                               'if (list '= 'n 1) 1
                               (list
                                '* 'n
                                (list
                                 'fact (list '- 'n 1))
                                ))
                              ))
                  (list 'fact 10)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((aproc-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (letrec->let aproc-list)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : aproc-list=~a : "
                        sub-name test-label-index
                        aproc-list)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Because internal definitions look "))
    (display
     (format #f "sequential but are~%"))
    (display
     (format #f "actually simultaneous, some people prefer "))
    (display
     (format #f "to avoid them~%"))
    (display
     (format #f "entirely, and use the special form letrec "))
    (display
     (format #f "instead. Letrec~%"))
    (display
     (format #f "looks like let, so it is not surprising "))
    (display
     (format #f "that the variables~%"))
    (display
     (format #f "it binds are bound simultaneously and "))
    (display
     (format #f "have the same~%"))
    (display
     (format #f "scope as each other. The sample procedure "))
    (display
     (format #f "f above can~%"))
    (display
     (format #f "written without internal definitions, "))
    (display
     (format #f "but with exactly~%"))
    (display
     (format #f "the same meaning, as:~%"))
    (newline)
    (display
     (format #f "(define (f x)~%"))
    (display
     (format #f "  (letrec ((even?~%"))
    (display
     (format #f "            (lambda (n)~%"))
    (display
     (format #f "              (if (= n 0)~%"))
    (display
     (format #f "                  true~%"))
    (display
     (format #f "                  (odd? (- n 1)))))~%"))
    (display
     (format #f "           (odd?~%"))
    (display
     (format #f "            (lambda (n)~%"))
    (display
     (format #f "              (if (= n 0)~%"))
    (display
     (format #f "                  false~%"))
    (display
     (format #f "                  (even? (- n 1))))))~%"))
    (display
     (format #f "    <rest of body of f>))~%"))
    (newline)
    (display
     (format #f "Letrec expressions, which have "))
    (display
     (format #f "the form~%"))
    (display
     (format #f "(letrec ((<var1> <exp1>) ... "))
    (display
     (format #f "(<varn> <expn>))~%"))
    (display
     (format #f "  <body>)~%"))
    (newline)
    (display
     (format #f "are a variation on let in which the "))
    (display
     (format #f "expressions <exp_k>~%"))
    (display
     (format #f "that provide the initial values for the "))
    (display
     (format #f "variables <var_k>~%"))
    (display
     (format #f "are evaluated in an environment that "))
    (display
     (format #f "includes all the~%"))
    (display
     (format #f "letrec bindings. This permits recursion "))
    (display
     (format #f "in the bindings,~%"))
    (display
     (format #f "such as the mutual recursion of even? and "))
    (display
     (format #f "odd? in the~%"))
    (display
     (format #f "example above, or the evaluation of 10 "))
    (display
     (format #f "factorial with:~%"))
    (newline)
    (display
     (format #f "(letrec ((fact~%"))
    (display
     (format #f "          (lambda (n)~%"))
    (display
     (format #f "            (if (= n 1)~%"))
    (display
     (format #f "                1~%"))
    (display
     (format #f "                (* n (fact "))
    (display
     (format #f "(- n 1)))))))~%"))
    (display
     (format #f "  (fact 10))~%"))
    (newline)
    (display
     (format #f "a. Implement letrec as a derived "))
    (display
     (format #f "expression, by~%"))
    (display
     (format #f "transforming a letrec expression into "))
    (display
     (format #f "a let expression~%"))
    (display
     (format #f "as shown in the text above or in exercise "))
    (display
     (format #f "4.18. That is,~%"))
    (display
     (format #f "the letrec variables should be created "))
    (display
     (format #f "with a let~%"))
    (display
     (format #f "and then be assigned their values with "))
    (display
     (format #f "set!.~%"))
    (display
     (format #f "b. Louis Reasoner is confused by all "))
    (display
     (format #f "this fuss about~%"))
    (display
     (format #f "internal definitions. The way he sees "))
    (display
     (format #f "it, if you don't~%"))
    (display
     (format #f "like to use define inside a procedure, "))
    (display
     (format #f "you can just~%"))
    (display
     (format #f "use let. Illustrate what is loose about "))
    (display
     (format #f "his reasoning by~%"))
    (display
     (format #f "drawing an environment diagram that shows "))
    (display
     (format #f "the environment~%"))
    (display
     (format #f "in which the <rest of body of f> is "))
    (display
     (format #f "evaluated during~%"))
    (display
     (format #f "evaluation of the expression (f 5), "))
    (display
     (format #f "with f defined~%"))
    (display
     (format #f "as in this exercise. Draw an environment "))
    (display
     (format #f "diagram for the~%"))
    (display
     (format #f "same evaluation, but with let in place "))
    (display
     (format #f "letrec in the~%"))
    (display
     (format #f "definition of f.~%"))
    (newline)
    (display
     (format #f "(a) The program letrec->let calls "))
    (display
     (format #f "local-transform-top which~%"))
    (display
     (format #f "either calls local-transform-vars (if "))
    (display
     (format #f "the proc list~%"))
    (display
     (format #f "starts with 'letrec), or calls "))
    (display
     (format #f "local-transform-body otherwise.~%"))
    (display
     (format #f "local-transform-vars take the list of "))
    (display
     (format #f "var/expr pairs and~%"))
    (display
     (format #f "transforms it into the let/set combination. "))
    (display
     (format #f "local-transform-body~%"))
    (display
     (format #f "iterates through each expression until it "))
    (display
     (format #f "finds a letrec~%"))
    (display
     (format #f "to process. See scheme test below.~%"))
    (newline)
    (display
     (format #f "(b) The key is applicative order evaluation "))
    (display
     (format #f "for functions.~%"))
    (display
     (format #f "Consider what happens with the function "))
    (display
     (format #f "written as is.~%"))
    (display
     (format #f "It is equivalent to~%"))
    (newline)
    (display
     (format #f "(define (f x) (let ((even? "))
    (display
     (format #f "'*unassigned*)~%"))
    (display
     (format #f "(odd? '*unassigned*))~%"))
    (display
     (format #f "(set! even? ...) (set! odd? ...) "))
    (display
     (format #f "<rest of body of f>)~%"))
    (newline)
    (display
     (format #f "Since let is equivalent to a call to "))
    (display
     (format #f "lambda, and each~%"))
    (display
     (format #f "of the arguments are evaluated, the "))
    (display
     (format #f "'*unassigned* version~%"))
    (display
     (format #f "allows for mutually recursive "))
    (display
     (format #f "definitions.~%"))
    (newline)
    (display
     (format #f "(define (f x) -> E0: "))
    (display
     (format #f "{ global-environment }~%"))
    (display
     (format #f "(letrec ...) -> E1: "))
    (display
     (format #f "{ even?='*unassigned*,~%"))
    (display
     (format #f "odd?='*unassigned*, E0 }~%"))
    (display
     (format #f "later even? and odd? get assigned to "))
    (display
     (format #f "their correct~%"))
    (display
     (format #f "expressions.~%"))
    (newline)
    (display
     (format #f "Instead of letrec, we simply use let, "))
    (display
     (format #f "then both the~%"))
    (display
     (format #f "even? and odd? arguments would be fully "))
    (display
     (format #f "evaluated, but~%"))
    (display
     (format #f "would signal an error since odd? is not "))
    (display
     (format #f "defined (when~%"))
    (display
     (format #f "first evaluating even?).~%"))
    (newline)
    (display
     (format #f "(define (f x) -> E0: "))
    (display
     (format #f "{ global-environment }~%"))
    (display
     (format #f "(let ...) -> E1: { E0 }~%"))
    (display
     (format #f "With let, there is no way to evaluate "))
    (display
     (format #f "even? and odd?~%"))
    (display
     (format #f "at the same time.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((sexp
           (list
            'letrec
            (list
             (list 'fact
                   (list 'lambda (list 'n)
                         (list 'if (list '= 'n 1) 1
                               (list '*
                                     'n
                                     (list
                                      'fact
                                      (list '- 'n 1)))
                               ))))
            (list 'fact 10))))
      (let ((result-list (letrec->let sexp)))
        (begin
          (display
           (format #f "init: ~a~%" sexp))
          (newline)
          (display
           (format #f "result: ~a~%" result-list))
          (newline)
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.20 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
