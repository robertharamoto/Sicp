#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.13                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (enclosing-environment env)
  (begin
    (cdr env)
    ))

;;;#############################################################
;;;#############################################################
(define (first-frame env)
  (begin
    (car env)
    ))

;;;#############################################################
;;;#############################################################
(define the-empty-environment (list))

;;;#############################################################
;;;#############################################################
(define (make-frame variables values)
  (begin
    (cons variables values)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-variables frame)
  (begin
    (car frame)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-values frame)
  (begin
    (cdr frame)
    ))

;;;#############################################################
;;;#############################################################
(define (add-binding-to-frame! var val frame)
  (begin
    (set-car! frame (cons var (car frame)))
    (set-cdr! frame (cons val (cdr frame)))
    ))

;;;#############################################################
;;;#############################################################
(define (extend-environment vars vals base-env)
  (begin
    (if (= (length vars) (length vals))
        (begin
          (cons (make-frame vars vals) base-env))
        (begin
          (if (< (length vars) (length vals))
              (begin
                (error
                 "Too many arguments supplied"
                 vars vals))
              (begin
                (error
                 "Too few arguments supplied"
                 vars vals)
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (scan
         var val variables-list values-list
         env null-func found-func)
  (begin
    (cond
     ((null? variables-list)
      (begin
        (null-func
         var val variables-list values-list env)
        ))
     ((eq? var (car variables-list))
      (begin
        (found-func
         var val variables-list values-list env)
        ))
     (else
      (begin
        (scan
         var val
         (cdr variables-list) (cdr values-list)
         env null-func found-func)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (env-loop
         var val env
         null-func found-func calling-string)
  (begin
    (if (eq? env the-empty-environment)
        (begin
          (error calling-string var))
        (begin
          (let ((frame (first-frame env)))
            (begin
              (scan
               var val
               (frame-variables frame)
               (frame-values frame)
               env null-func found-func)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-unbound! var env)
  (define (local-iter
           var vars-list values-list
           acc-vars-list acc-values-list)
    (begin
      (if (null? vars-list)
          (begin
            (list (reverse acc-vars-list)
                  (reverse acc-values-list)))
          (begin
            (let ((this-var (car vars-list))
                  (this-value (car values-list))
                  (tail-vars (cdr vars-list))
                  (tail-values (cdr values-list)))
              (begin
                (if (eq? var this-var)
                    (begin
                      (local-iter
                       var tail-vars tail-values
                       acc-vars-list acc-values-list))
                    (begin
                      (local-iter
                       var tail-vars tail-values
                       (cons this-var acc-vars-list)
                       (cons this-value acc-values-list))
                      ))
                ))
            ))
      ))
  (begin
    (if (or (null? env) (null? (car env)))
        (begin
          (error "null environment for make-unbound!" env))
        (begin
          (let ((frame (first-frame env)))
            (let ((vars-list (frame-variables frame))
                  (values-list (frame-values frame)))
              (let ((rlist
                     (local-iter
                      var vars-list values-list (list) (list))))
                (let ((vars (list-ref rlist 0))
                      (vals (list-ref rlist 1)))
                  (begin
                    (set-car! frame vars)
                    (set-cdr! frame vals)
                    (set-car! env frame)
                    )))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-unbound result-hash-table)
 (begin
   (let ((sub-name "test-make-unbound")
         (test-list
          (list
           (list 'a
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'b 'c) (list 11 12)
                  the-empty-environment))
           (list 'b
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'c) (list 10 12)
                  the-empty-environment))
           (list 'c
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  the-empty-environment)
                 (extend-environment
                  (list 'a 'b) (list 10 11)
                  the-empty-environment))
           (list 'd
                 (extend-environment
                  (list 'a 'b 'c 'd) (list 10 11 12 13)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment))
                 (extend-environment
                  (list 'a 'b 'c) (list 10 11 12)
                  (extend-environment
                   (list 'd 'e 'f) (list 22 23 24)
                   the-empty-environment)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((var (list-ref alist 0))
                  (env (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2))
                  (slen (length (list-ref alist 2))))
              (begin
                (make-unbound! var env)

                (let ((err-start
                       (format
                        #f "~a : error (~a) : var=~a, env=~a : "
                        sub-name test-label-index var env)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list env
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Scheme allows us to create new bindings "))
    (display
     (format #f "for variables by~%"))
    (display
     (format #f "means of define, but provides no way to "))
    (display
     (format #f "get rid of~%"))
    (display
     (format #f "bindings. Implement for the evaluator "))
    (display
     (format #f "a special form~%"))
    (display
     (format #f "make-unbound! that removes the binding "))
    (display
     (format #f "of a given~%"))
    (display
     (format #f "symbol from from the environment in "))
    (display
     (format #f "which the~%"))
    (display
     (format #f "make-unbound! expression is evaluated. "))
    (display
     (format #f "This problem is~%"))
    (display
     (format #f "not completely specified. For example, "))
    (display
     (format #f "should we remove~%"))
    (display
     (format #f "only the binding in the first frame of "))
    (display
     (format #f "the environment?~%"))
    (display
     (format #f "Complete the specification and justify "))
    (display
     (format #f "any choices you~%"))
    (display
     (format #f "make.~%"))
    (newline)
    (display
     (format #f "The function make-unbound! should only "))
    (display
     (format #f "remove definitions~%"))
    (display
     (format #f "from the first frame of the environment. "))
    (display
     (format #f "This is because~%"))
    (display
     (format #f "enclosing environments should not have "))
    (display
     (format #f "variables removed~%"))
    (display
     (format #f "from local functions.~%"))
    (newline)
    (display
     (format #f "(define (make-unbound! var env)~%"))
    (display
     (format #f "  (define (local-iter~%"))
    (display
     (format #f "           var vars-list values-list~%"))
    (display
     (format #f "           acc-vars-list "))
    (display
     (format #f "acc-values-list)~%"))
    (display
     (format #f "    (if (null? vars-list)~%"))
    (display
     (format #f "        (list (reverse acc-vars-list) "))
    (display
     (format #f "(reverse acc-values-list))~%"))
    (display
     (format #f "        (begin~%"))
    (display
     (format #f "          (let ((this-var "))
    (display
     (format #f "(car vars-list))~%"))
    (display
     (format #f "                (this-value "))
    (display
     (format #f "(car values-list))~%"))
    (display
     (format #f "                (tail-vars "))
    (display
     (format #f "(cdr vars-list))~%"))
    (display
     (format #f "                (tail-values "))
    (display
     (format #f "(cdr values-list)))~%"))
    (display
     (format #f "            (if (eq? var this-var)~%"))
    (display
     (format #f "                (local-iter~%"))
    (display
     (format #f "                 var tail-vars "))
    (display
     (format #f "tail-values~%"))
    (display
     (format #f "                 acc-vars-list "))
    (display
     (format #f "acc-values-list)~%"))
    (display
     (format #f "                (local-iter~%"))
    (display
     (format #f "                 var tail-vars "))
    (display
     (format #f "tail-values~%"))
    (display
     (format #f "                 (cons this-var "))
    (display
     (format #f "acc-vars-list)~%"))
    (display
     (format #f "                 (cons this-value "))
    (display
     (format #f "acc-values-list))~%"))
    (display
     (format #f "                ))~%"))
    (display
     (format #f "          )))~%"))
    (display
     (format #f "  (if (or (null? env) "))
    (display
     (format #f "(null? (car env)))~%"))
    (display
     (format #f "      (begin~%"))
    (display
     (format #f "        (error \"null environment "))
    (display
     (format #f "for make-unbound!\" env))~%"))
    (display
     (format #f "      (let ((frame "))
    (display
     (format #f "(first-frame env)))~%"))
    (display
     (format #f "        (let ((vars-list "))
    (display
     (format #f "(frame-variables frame))~%"))
    (display
     (format #f "              (values-list "))
    (display
     (format #f "(frame-values frame)))~%"))
    (display
     (format #f "          (let ((rlist~%"))
    (display
     (format #f "                 (local-iter~%"))
    (display
     (format #f "                  var vars-list "))
    (display
     (format #f "values-list (list) (list))))~%"))
    (display
     (format #f "            (let ((vars "))
    (display
     (format #f "(list-ref rlist 0))~%"))
    (display
     (format #f "                  (vals "))
    (display
     (format #f "(list-ref rlist 1)))~%"))
    (display
     (format #f "              (begin~%"))
    (display
     (format #f "                (set-car! "))
    (display
     (format #f "frame vars)~%"))
    (display
     (format #f "                (set-cdr! "))
    (display
     (format #f "frame vals)~%"))
    (display
     (format #f "                (set-car! "))
    (display
     (format #f "env frame)~%"))
    (display
     (format #f "                )))~%"))
    (display
     (format #f "          ))~%"))
    (display
     (format #f "      ))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.13 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
