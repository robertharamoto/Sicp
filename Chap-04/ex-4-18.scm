#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.18                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider an alternative strategy for "))
    (display
     (format #f "scanning out~%"))
    (display
     (format #f "definitions that translates the example "))
    (display
     (format #f "in the text to:~%"))
    (newline)
    (display
     (format #f "(lambda <vars>~%"))
    (display
     (format #f "  (let ((u '*unassigned*)~%"))
    (display
     (format #f "        (v '*unassigned*))~%"))
    (display
     (format #f "    (let ((a <e1>)~%"))
    (display
     (format #f "          (b <e2>))~%"))
    (display
     (format #f "      (set! u a)~%"))
    (display
     (format #f "      (set! v b))~%"))
    (display
     (format #f "    <e3>))~%"))
    (newline)
    (display
     (format #f "Here a and b are meant to represent new "))
    (display
     (format #f "variable names,~%"))
    (display
     (format #f "created by the interpreter, that do not "))
    (display
     (format #f "appear in the~%"))
    (display
     (format #f "user's program. Consider the solve "))
    (display
     (format #f "solve procedure from~%"))
    (display
     (format #f "section 3.5.4:~%"))
    (newline)
    (display
     (format #f "(define (solve f y0 dt)~%"))
    (display
     (format #f "  (define y (integral "))
    (display
     (format #f "(delay dy) y0 dt))~%"))
    (display
     (format #f "  (define dy "))
    (display
     (format #f "(stream-map f y))~%"))
    (display
     (format #f "  y)~%"))
    (newline)
    (display
     (format #f "Will this procedure work if internal "))
    (display
     (format #f "definitions are scanned~%"))
    (display
     (format #f "out as shown in this exercise? What if "))
    (display
     (format #f "they are scanned out~%"))
    (display
     (format #f "as shown in the text? Explain.~%"))
    (newline)
    (display
     (format #f "If the solve function run through "))
    (display
     (format #f "scan-out-define,~%"))
    (display
     (format #f "then it gets transformed to:~%"))
    (newline)
    (display
     (format #f "(define (solve f y0 dt)~%"))
    (display
     (format #f "  (let ((y '*unassigned*)~%"))
    (display
     (format #f "        (dy '*unassigned*))~%"))
    (display
     (format #f "    (set! y (integral "))
    (display
     (format #f "(delay dy) y0 dt))~%"))
    (display
     (format #f "    (set! dy (stream-map f y))~%"))
    (display
     (format #f "    y))~%"))
    (newline)
    (display
     (format #f "So solve will work the same as before "))
    (display
     (format #f "the transformation.~%"))
    (display
     (format #f "The only difference is that this version "))
    (display
     (format #f "of solve will~%"))
    (display
     (format #f "have an extra environment "))
    (display
     (format #f "frame.~%"))
    (newline)
    (display
     (format #f "If the solve function is transformed "))
    (display
     (format #f "according to the~%"))
    (display
     (format #f "new procedure:~%"))
    (newline)
    (display
     (format #f "(define (solve f y0 dt)~%"))
    (display
     (format #f "  (let ((y '*unassigned*)~%"))
    (display
     (format #f "        (dy '*unassigned*))~%"))
    (display
     (format #f "    (let ((a (integral "))
    (display
     (format #f "(delay dy) y0 dt))~%"))
    (display
     (format #f "          (b (stream-map f y)))~%"))
    (display
     (format #f "      (set! y a)~%"))
    (display
     (format #f "      (set! dy b))~%"))
    (display
     (format #f "    y))~%"))
    (newline)
    (display
     (format #f "Solve behaves identically, since in this "))
    (display
     (format #f "version, a and~%"))
    (display
     (format #f "b points to some code, and then y and "))
    (display
     (format #f "dy get set to~%"))
    (display
     (format #f "point to the same code, so it behaves "))
    (display
     (format #f "identically. The~%"))
    (display
     (format #f "only difference are extra environment "))
    (display
     (format #f "frames, one for~%"))
    (display
     (format #f "each let.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.18 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
