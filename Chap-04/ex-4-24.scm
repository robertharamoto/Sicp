#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.24                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;### ex-4-24a-module - functions for original/inefficient evaluator
(use-modules ((ex-4-24a-module)
              :renamer (symbol-prefix-proc 'ex-4-24a-module:)))

;;;### ex-4-24b-module - functions for analyze/efficient evaluator
(use-modules ((ex-4-24b-module)
              :renamer (symbol-prefix-proc 'ex-4-24b-module:)))

;;;#############################################################
;;;#############################################################
(define (fib nn)
  (define (fib-iter b a counter)
    (begin
      (if (<= counter 0)
          (begin
            b)
          (begin
            (fib-iter a (+ a b) (1- counter))
            ))
      ))
  (begin
    (fib-iter 0 1 nn)
    ))

;;;#############################################################
;;;#############################################################
(define fib-list
  (list 'define (list 'fib 'nn)
        (list 'define (list 'fib-iter 'b 'a 'counter)
              (list 'if (list '<= 'counter 0)
                    'b
                    (list 'fib-iter 'a (list '+ 'a 'b) (list '1- 'counter))
                    ))
        (list 'fib-iter 0 1 'nn)))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fib result-hash-table)
 (begin
   (let ((sub-name "test-fib")
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1) (list 3 2)
           (list 4 3) (list 5 5) (list 6 8) (list 7 13)
           (list 8 21) (list 9 34) (list 10 55) (list 11 89)
           (list 12 144) (list 13 233) (list 14 377)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (fib nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a : "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-version run-code fibonacci-list test-list)
  (begin
    (for-each
     (lambda (anum)
       (begin
         (let ((fib-result
                (run-code
                 (list fibonacci-list (list 'fib anum)))))
           (begin
             (display
              (ice-9-format:format
               #f "(fib ~a) = ~:d~%"
               anum fib-result))
             (force-output)
             ))
         )) test-list)
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-fib)
  (begin
    (let ((test-list
           (list 10 20 30 40 50 60 70 80 90 100 200))
          (fibonacci-list
           (list
            'define (list 'fib 'nn)
            (list 'define (list 'fib-iter 'b 'a 'counter)
                  (list 'if (list '<= 'counter 0)
                        'b
                        (list
                         'fib-iter 'a
                         (list '+ 'a 'b)
                         (list '1- 'counter))
                        ))
            (list 'fib-iter 0 1 'nn))))
      (begin
        (display
         (format #f "scheme test original, inefficient method~%"))

        (timer-module:time-code-macro
         (begin
           (scheme-test-version
            ex-4-24a-module:run-code fibonacci-list test-list)
           ))

        (newline)
        (display
         (format #f "scheme test analyze, efficient method~%"))

        (timer-module:time-code-macro
         (begin
           (scheme-test-version
            ex-4-24b-module:run-code fibonacci-list test-list)
           ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (scheme-test-prime-version run-code prime-list test-list)
  (begin
    (for-each
     (lambda (anum)
       (begin
         (let ((num-primes
                (run-code
                 (list
                  prime-list (list 'count-primes anum)))))
           (begin
             (display
              (ice-9-format:format
               #f "(num-primes <= ~:d) = ~:d~%"
               anum num-primes))
             (force-output)
             ))
         )) test-list)
    ))

;;;#############################################################
;;;#############################################################
(define (experiment-count-primes)
  (begin
    (let ((test-list (list 10 20 100 1000 5000))
          (prime-list
           (list
            (list
             'define (list 'smallest-divisor 'nn)
             (list
              'define
              (list 'find-divisor 'nn 'test-divisor 'max-divisor)
              (list
               'cond
               (list (list '> 'test-divisor 'max-divisor) 'nn)
               (list
                (list
                 'zero? (list 'modulo 'nn 'test-divisor))
                'test-divisor)
               (list
                'else
                (list 'find-divisor 'nn
                      (list '+ 'test-divisor 2)
                      'max-divisor))
               ))
             (list 'if (list 'zero? (list 'modulo 'nn 2))
                   2
                   (list
                    'find-divisor 'nn 3
                    (list '1+
                          (list 'inexact->exact
                                (list 'truncate
                                      (list 'sqrt 'nn))
                                ))
                    )))
            (list
             'define (list 'prime? 'nn)
             (list '= 'nn (list 'smallest-divisor 'nn)))
            (list
             'define (list 'count-primes-iter 'nn 'max-nn 'counter)
             (list 'if (list '> 'nn 'max-nn)
                   'counter
                   (list
                    'if (list 'prime? 'nn)
                    (list 'count-primes-iter
                          (list '+ 'nn 2) 'max-nn (list '1+ 'counter))
                    (list 'count-primes-iter
                          (list '+ 'nn 2) 'max-nn 'counter))
                   ))
            (list
             'define (list 'count-primes 'max-nn)
             (list 'count-primes-iter 3 'max-nn 1))
            )))
      (begin
        (display
         (format #f "scheme test original, inefficient method~%"))

        (timer-module:time-code-macro
         (begin
           (scheme-test-prime-version
            ex-4-24a-module:run-code prime-list test-list)
           ))

        (newline)
        (display
         (format #f "scheme test analyze, efficient method~%"))
        (timer-module:time-code-macro
         (begin
           (scheme-test-prime-version
            ex-4-24b-module:run-code prime-list test-list)
           ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (smallest-divisor nn)
  (define (find-divisor nn test-divisor max-divisor)
    (begin
      (cond
       ((> test-divisor max-divisor)
        (begin
          nn
          ))
       ((zero? (modulo nn test-divisor))
        (begin
          test-divisor
          ))
       (else
        (begin
          (find-divisor nn (+ test-divisor 2) max-divisor)
          )))
      ))
  (begin
    (if (zero? (modulo nn 2))
        (begin
          2)
        (begin
          (find-divisor
           nn 3
           (1+
            (inexact->exact
             (truncate (sqrt nn)))))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (prime? nn)
  (begin
    (= nn (smallest-divisor nn))
    ))

;;;#############################################################
;;;#############################################################
(define (count-primes-iter nn max-nn counter)
  (begin
    (if (> nn max-nn)
        (begin
          counter)
        (begin
          (if (prime? nn)
              (begin
                (count-primes-iter
                 (+ nn 2) max-nn (1+ counter)))
              (begin
                (count-primes-iter
                 (+ nn 2) max-nn counter)
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (count-primes max-nn)
  (begin
    (if (< max-nn 2)
        (begin
          0)
        (begin
          (count-primes-iter 3 max-nn 1)
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-primes result-hash-table)
 (begin
   (let ((sub-name "test-count-primes")
         (test-list
          (list
           (list 0 0) (list 1 0) (list 2 1) (list 3 2)
           (list 4 2) (list 5 3) (list 6 3) (list 7 4)
           (list 8 4) (list 9 4) (list 10 4) (list 11 5)
           (list 12 5) (list 13 6) (list 14 6) (list 15 6)
           (list 16 6) (list 17 7) (list 18 7) (list 19 8)
           (list 20 8) (list 21 8) (list 22 8) (list 23 9)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (count-primes nn)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : nn=~a : "
                        sub-name test-label-index nn))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Design and carry out some experiments to "))
    (display
     (format #f "compare the speed~%"))
    (display
     (format #f "of the original metacircular evaluator "))
    (display
     (format #f "with the version~%"))
    (display
     (format #f "in this section. Use your results to "))
    (display
     (format #f "estimate the fraction~%"))
    (display
     (format #f "of time that is spent in analysis versus "))
    (display
     (format #f "execution for~%"))
    (display
     (format #f "various procedures.~%"))
    (newline)
    (display
     (format #f "We used guile 3.0 to compare the original "))
    (display
     (format #f "versus the more~%"))
    (display
     (format #f "efficient analyze version of the "))
    (display
     (format #f "evaluator.~%"))
    (newline)
    (display
     (format #f "             Fibonacci               Count Primes~%"))
    (display
     (format #f "Original     0.045 secs                2.42 secs~%"))
    (display
     (format #f "Analyze      0.037 secs                1.86 secs~%"))
    (display
     (format #f "fraction       18%                      23%~%"))
    (newline)
    (display
     (format #f "where the fraction assumes that there is "))
    (display
     (format #f "no time spent~%"))
    (display
     (format #f "in analysis in the analyze version, and "))
    (display
     (format #f "all the time~%"))
    (display
     (format #f "spent in analysis is found in the original "))
    (display
     (format #f "version. So~%"))
    (display
     (format #f "the table shows that about half the time "))
    (display
     (format #f "in the evaluator~%"))
    (display
     (format #f "is spent analyzing the code.~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (experiment-fib)
    (newline)
    (experiment-count-primes)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.24 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (display (format #f "scheme test~%"))
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
