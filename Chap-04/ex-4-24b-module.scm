;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.24b - analyze evaluator              ###
;;;###                                                       ###
;;;###  last updated July 10, 2024                           ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;;### start exercise 4-24 b module

(define-module (ex-4-24b-module)
  #:export (run-code
            my-eval
            driver-loop
            ))

;;;#############################################################
;;;#############################################################
(define true #t)

;;;#############################################################
;;;#############################################################
(define false #f)

;;;#############################################################
;;;#############################################################
(define null (list))

;;;#############################################################
;;;#############################################################
(define (my-eval exp env)
  (begin
    ((analyze exp) env)
    ))

;;;#############################################################
;;;#############################################################
(define (analyze exp)
  (begin
    (cond
     ((my-self-evaluating? exp)
      (begin
        (analyze-self-evaluating exp)
        ))
     ((quoted? exp)
      (begin
        (analyze-quoted exp)
        ))
     ((my-variable? exp)
      (begin
        (analyze-variable exp)
        ))
     ((assignment? exp)
      (begin
        (analyze-assignment exp)
        ))
     ((definition? exp)
      (begin
        (analyze-definition exp)
        ))
     ((if? exp)
      (begin
        (analyze-if exp)
        ))
     ((lambda? exp)
      (begin
        (analyze-lambda exp)
        ))
     ((begin? exp)
      (begin
        (analyze-sequence (begin-actions exp))
        ))
     ((cond? exp)
      (begin
        (analyze (cond->if exp))
        ))
     ((application? exp)
      (begin
        (analyze-application exp)
        ))
     ((or (equal? exp #t)
          (equal? exp #f))
      (begin
        exp
        ))
     (else
      (begin
        (error "Unknown expression type -- ANALYZE" exp)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-self-evaluating exp)
  (begin
    (lambda (env) exp)
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-quoted exp)
  (begin
    (let ((qval (text-of-quotation exp)))
      (begin
        (lambda (env) qval)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-variable exp)
  (begin
    (lambda (env)
      (begin
        (lookup-variable-value exp env)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-assignment exp)
  (begin
    (let ((var (assignment-variable exp))
          (vproc (analyze (assignment-value exp))))
      (begin
        (lambda (env)
          (begin
            (set-variable-value! var (vproc env) env)
            'ok))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-definition exp)
  (begin
    (let ((var (definition-variable exp))
          (vproc (analyze (definition-value exp))))
      (begin
        (lambda (env)
          (begin
            (define-variable! var (vproc env) env)
            'ok))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-if exp)
  (begin
    (let ((pproc (analyze (if-predicate exp)))
          (cproc (analyze (if-consequent exp)))
          (aproc (analyze (if-alternative exp))))
      (begin
        (lambda (env)
          (begin
            (if (true? (pproc env))
                (begin
                  (cproc env))
                (begin
                  (aproc env)
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-lambda exp)
  (begin
    (let ((vars (lambda-parameters exp))
          (bproc (analyze-sequence (lambda-body exp))))
      (begin
        (lambda (env)
          (begin
            (make-procedure vars bproc env)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-sequence exps)
  (define (sequentially proc1 proc2)
    (begin
      (lambda (env)
        (begin
          (proc1 env)
          (proc2 env)
          ))
      ))
  (define (loop first-proc rest-procs)
    (begin
      (if (null? rest-procs)
          (begin
            first-proc)
          (begin
            (loop
             (sequentially first-proc (car rest-procs))
             (cdr rest-procs))
            ))
      ))
  (begin
    (let ((procs (map analyze exps)))
      (begin
        (if (null? procs)
            (begin
              (error "Empty sequence -- ANALYZE"))
            (begin
              (loop
               (car procs) (cdr procs))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (analyze-application exp)
  (begin
    (let ((fproc (analyze (operator exp)))
          (aprocs (map analyze (operands exp))))
      (begin
        (lambda (env)
          (begin
            (execute-application
             (fproc env)
             (map
              (lambda (aproc)
                (begin
                  (aproc env)
                  ))
              aprocs))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (execute-application proc args)
  (begin
    (cond
     ((primitive-procedure? proc)
      (begin
        (apply-primitive-procedure proc args)
        ))
     ((compound-procedure? proc)
      (begin
        ((procedure-body proc)
         (extend-environment
          (procedure-parameters proc)
          args
          (procedure-environment proc)))
        ))
     ((eq? proc 'ok)
      (begin
        (car args)
        ))
     (else
      (begin
        (error
         "Unknown procedure type -- EXECUTE-APPLICATION"
         proc)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define apply-in-underlying-scheme apply)

;;;#############################################################
;;;#############################################################
(define (list-of-values exps env)
  (begin
    (if (no-operands? exps)
        (begin
          (list))
        (begin
          (cons
           (my-eval (first-operand exps) env)
           (list-of-values
            (rest-operands exps) env))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (eval-sequence exps env)
  (begin
    (cond
     ((last-exp? exps)
      (begin
        (my-eval (first-exp exps) env)
        ))
     (else
      (begin
        (my-eval (first-exp exps) env)
        (eval-sequence (rest-exps exps) env)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (my-self-evaluating? exp)
  (begin
    (cond
     ((number? exp)
      (begin
        true
        ))
     ((string? exp)
      (begin
        true
        ))
     (else
      (begin
        false
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (my-variable? exp)
  (begin
    (symbol? exp)
    ))

;;;#############################################################
;;;#############################################################
(define (quoted? exp)
  (begin
    (tagged-list? exp 'quote)
    ))

;;;#############################################################
;;;#############################################################
(define (text-of-quotation exp)
  (begin
    (cadr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (tagged-list? exp tag)
  (begin
    (if (pair? exp)
        (begin
          (eq? (car exp) tag))
        (begin
          false
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (assignment? exp)
  (begin
    (tagged-list? exp 'set!)
    ))

;;;#############################################################
;;;#############################################################
(define (assignment-variable exp)
  (begin
    (cadr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (assignment-value exp)
  (begin
    (caddr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (definition? exp)
  (begin
    (tagged-list? exp 'define)
    ))

;;;#############################################################
;;;#############################################################
(define (definition-variable exp)
  (begin
    (if (symbol? (cadr exp))
        (begin
          (cadr exp))
        (begin
          (caadr exp)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (definition-value exp)
  (begin
    (if (symbol? (cadr exp))
        (begin
          (caddr exp))
        (begin
          (make-lambda
           (cdadr exp)   ; formal parameters
           (cddr exp))   ; body
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lambda? exp)
  (begin
    (tagged-list? exp 'lambda)
    ))

;;;#############################################################
;;;#############################################################
(define (lambda-parameters exp)
  (begin
    (cadr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (lambda-body exp)
  (begin
    (cddr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (make-lambda parameters body)
  (begin
    (cons
     'lambda
     (cons parameters body))
    ))

;;;#############################################################
;;;#############################################################
(define (if? exp)
  (begin
    (tagged-list? exp 'if)
    ))

;;;#############################################################
;;;#############################################################
(define (if-predicate exp)
  (begin
    (cadr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (if-consequent exp)
  (begin
    (caddr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (if-alternative exp)
  (begin
    (if (not (null? (cdddr exp)))
        (begin
          (cadddr exp))
        (begin
          'false
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-if predicate consequent alternative)
  (begin
    (list
     'if predicate consequent alternative)
    ))

;;;#############################################################
;;;#############################################################
(define (begin? exp)
  (begin
    (tagged-list? exp 'begin)
    ))

;;;#############################################################
;;;#############################################################
(define (begin-actions exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (last-exp? seq)
  (begin
    (null? (cdr seq))
    ))

;;;#############################################################
;;;#############################################################
(define (first-exp seq)
  (begin
    (car seq)
    ))

;;;#############################################################
;;;#############################################################
(define (rest-exps seq)
  (begin
    (cdr seq)
    ))

;;;#############################################################
;;;#############################################################
(define (sequence->exp seq)
  (begin
    (cond
     ((null? seq)
      (begin
        seq
        ))
     ((last-exp? seq)
      (begin
        (first-exp seq)
        ))
     (else
      (begin
        (make-begin seq)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (make-begin seq)
  (begin
    (cons 'begin seq)
    ))

;;;#############################################################
;;;#############################################################
(define (application? exp)
  (begin
    (pair? exp)
    ))

;;;#############################################################
;;;#############################################################
(define (operator exp)
  (begin
    (car exp)
    ))

;;;#############################################################
;;;#############################################################
(define (operands exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (no-operands? ops)
  (begin
    (null? ops)
    ))

;;;#############################################################
;;;#############################################################
(define (first-operand ops)
  (begin
    (car ops)
    ))

;;;#############################################################
;;;#############################################################
(define (rest-operands ops)
  (begin
    (cdr ops)
    ))

;;;#############################################################
;;;#############################################################
(define (cond? exp)
  (begin
    (tagged-list? exp 'cond)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-clauses exp)
  (begin
    (cdr exp)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-else-clause? clause)
  (begin
    (eq?
     (cond-predicate clause)
     'else)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-predicate clause)
  (begin
    (car clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond-actions clause)
  (begin
    (cdr clause)
    ))

;;;#############################################################
;;;#############################################################
(define (cond->if exp)
  (begin
    (expand-clauses (cond-clauses exp))
    ))

;;;#############################################################
;;;#############################################################
(define (expand-clauses clauses)
  (begin
    (if (null? clauses)
        (begin
          'false)                          ; no else clause
        (begin
          (let ((first (car clauses))
                (rest (cdr clauses)))
            (begin
              (if (cond-else-clause? first)
                  (begin
                    (if (null? rest)
                        (begin
                          (sequence->exp (cond-actions first)))
                        (begin
                          (error
                           "ELSE clause isn't last -- COND->IF"
                           clauses)
                          )))
                  (begin
                    (make-if
                     (cond-predicate first)
                     (sequence->exp (cond-actions first))
                     (expand-clauses rest))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (true? x)
  (begin
    (not (eq? x #f))
    ))

;;;#############################################################
;;;#############################################################
(define (false? x)
  (begin
    (eq? x #f)
    ))

;;;#############################################################
;;;#############################################################
(define (make-procedure parameters body env)
  (begin
    (list
     'procedure
     parameters body env)
    ))

;;;#############################################################
;;;#############################################################
(define (compound-procedure? p)
  (begin
    (tagged-list? p 'procedure)
    ))

;;;#############################################################
;;;#############################################################
(define (procedure-parameters p)
  (begin
    (cadr p)
    ))

;;;#############################################################
;;;#############################################################
(define (procedure-body p)
  (begin
    (caddr p)
    ))

;;;#############################################################
;;;#############################################################
(define (procedure-environment p)
  (begin
    (cadddr p)
    ))

;;;#############################################################
;;;#############################################################
(define (enclosing-environment env)
  (begin
    (cdr env)
    ))

;;;#############################################################
;;;#############################################################
(define (first-frame env)
  (begin
    (car env)
    ))

;;;#############################################################
;;;#############################################################
(define the-empty-environment (list))

;;;#############################################################
;;;#############################################################
(define (make-frame variables values)
  (begin
    (cons variables values)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-variables frame)
  (begin
    (car frame)
    ))

;;;#############################################################
;;;#############################################################
(define (frame-values frame)
  (begin
    (cdr frame)
    ))

;;;#############################################################
;;;#############################################################
(define (add-binding-to-frame! var val frame)
  (begin
    (set-car! frame (cons var (car frame)))
    (set-cdr! frame (cons val (cdr frame)))
    ))

;;;#############################################################
;;;#############################################################
(define (extend-environment vars vals base-env)
  (begin
    (if (= (length vars) (length vals))
        (begin
          (cons (make-frame vars vals) base-env))
        (begin
          (if (< (length vars) (length vals))
              (begin
                (error
                 "Too many arguments supplied"
                 vars vals))
              (begin
                (error
                 "Too few arguments supplied"
                 vars vals)
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (lookup-variable-value var env)
  (define (env-loop env)
    (define (scan vars vals)
      (begin
        (cond
         ((null? vars)
          (begin
            (env-loop (enclosing-environment env))
            ))
         ((eq? var (car vars))
          (begin
            (car vals)
            ))
         (else
          (begin
            (scan (cdr vars) (cdr vals))
            )))
        ))
    (begin
      (if (eq? env the-empty-environment)
          (begin
            (error "Unbound variable" var))
          (begin
            (let ((frame (first-frame env)))
              (begin
                (scan
                 (frame-variables frame)
                 (frame-values frame))
                ))
            ))
      ))
  (begin
    (env-loop env)
    ))

;;;#############################################################
;;;#############################################################
(define (set-variable-value! var val env)
  (define (env-loop env)
    (define (scan vars vals)
      (begin
        (cond
         ((null? vars)
          (begin
            (env-loop (enclosing-environment env))
            ))
         ((eq? var (car vars))
          (begin
            (set-car! vals val)
            ))
         (else
          (begin
            (scan (cdr vars) (cdr vals))
            )))
        ))
    (begin
      (if (eq? env the-empty-environment)
          (begin
            (error "Unbound variable -- SET!" var))
          (begin
            (let ((frame (first-frame env)))
              (begin
                (scan
                 (frame-variables frame)
                 (frame-values frame))
                ))
            ))
      ))
  (begin
    (env-loop env)
    ))

;;;#############################################################
;;;#############################################################
(define (define-variable! var val env)
  (begin
    (let ((frame (first-frame env)))
      (begin
        (define (scan vars vals)
          (begin
            (cond
             ((null? vars)
              (begin
                (add-binding-to-frame! var val frame)
                ))
             ((eq? var (car vars))
              (begin
                (set-car! vals val)
                ))
             (else
              (begin
                (scan (cdr vars) (cdr vals))
                )))
            ))

        (scan
         (frame-variables frame)
         (frame-values frame))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (primitive-procedure? proc)
  (begin
    (tagged-list? proc 'primitive)
    ))

;;;#############################################################
;;;#############################################################
(define (primitive-implementation proc)
  (begin
    (cadr proc)
    ))

;;;#############################################################
;;;#############################################################
(define primitive-procedures
  (list (list 'car car)
        (list 'cdr cdr)
        (list 'cons cons)
        (list 'list list)
        (list 'list-ref list-ref)
        (list 'null? null?)
        (list 'null (list))
        (list 'display display)
        (list 'format format)
        (list 'force-output force-output)
        (list '+ +)
        (list '- -)
        (list '/ /)
        (list '* *)
        (list '< <)
        (list '<= <=)
        (list '= =)
        (list '> >)
        (list '>= >=)
        (list '1+ 1+)
        (list '1- 1-)
        (list 'zero? zero?)
        (list 'modulo modulo)
        (list 'inexact->exact inexact->exact)
        (list 'truncate truncate)
        (list 'sqrt sqrt)
        ))

;;;#############################################################
;;;#############################################################
(define (primitive-procedure-names)
  (begin
    (map
     car
     primitive-procedures)
    ))

;;;#############################################################
;;;#############################################################
(define (primitive-procedure-objects)
  (begin
    (map
     (lambda (proc) (list 'primitive (cadr proc)))
     primitive-procedures)
    ))

;;;#############################################################
;;;#############################################################
(define (apply-primitive-procedure proc args)
  (begin
    (apply-in-underlying-scheme
     (primitive-implementation proc) args)
    ))

;;;#############################################################
;;;#############################################################
(define (setup-environment)
  (begin
    (let ((initial-env
           (extend-environment
            (primitive-procedure-names)
            (primitive-procedure-objects)
            the-empty-environment)))
      (begin
        (define-variable!
          'true #t initial-env)
        (define-variable!
          'false #f initial-env)
        initial-env
        ))
    ))

;;;#############################################################
;;;#############################################################
(define the-global-environment
  (begin
    (setup-environment)
    ))

;;;#############################################################
;;;#############################################################
(define input-prompt ";;; M-Eval input:")

;;;#############################################################
;;;#############################################################
(define output-prompt ";;; M-Eval value:")

;;;#############################################################
;;;#############################################################
(define (driver-loop)
  (begin
    (prompt-for-input input-prompt)

    (let ((input (read)))
      (let ((output
             (my-eval input the-global-environment)))
        (begin
          (announce-output output-prompt)
          (user-print output)
          )))
    (driver-loop)
    ))

;;;#############################################################
;;;#############################################################
(define (prompt-for-input string)
  (begin
    (newline)
    (newline)
    (display string)
    (newline)
    ))

;;;#############################################################
;;;#############################################################
(define (announce-output string)
  (begin
    (newline)
    (display string)
    (newline)
    ))

;;;#############################################################
;;;#############################################################
(define (user-print object)
  (begin
    (if (compound-procedure? object)
        (begin
          (display
           (list
            'compound-procedure
            (procedure-parameters object)
            (procedure-body object)
            '<procedure-env>)))
        (begin
          (display object)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define the-global-environment
  (begin
    (setup-environment)
    ))

;;;#############################################################
;;;#############################################################
(define (run-code sexp)
  (begin
    (my-eval
     sexp
     the-global-environment)
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of module                                        ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
