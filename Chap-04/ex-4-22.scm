#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.22                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (let->combination let-list)
  (define (local-iter
           var-list acc-var-list acc-exp-list)
    (begin
      (if (null? var-list)
          (begin
            (list acc-var-list acc-exp-list))
          (begin
            (let ((this-pair (car var-list))
                  (tail-list (cdr var-list)))
              (let ((var-elem (car this-pair))
                    (exp-elem (cadr this-pair)))
                (begin
                  (local-iter
                   tail-list
                   (append acc-var-list (list var-elem))
                   (append acc-exp-list (list exp-elem)))
                  )))
            ))
      ))
  (begin
    (let ((var-list (cadr let-list))
          (body-list (caddr let-list)))
      (let ((acc-list-list
             (local-iter var-list (list) (list))))
        (let ((new-var-list (car acc-list-list))
              (new-expr-list (cadr acc-list-list)))
          (let ((lambda-expr
                 (cons
                  (list 'lambda new-var-list
                        body-list)
                  new-expr-list)))
            (begin
              lambda-expr
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-lists-are-equal?
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-msg-2
             (format
              #f "shouldbe=~a, result=~a, "
              shouldbe-list result-list))
            (err-msg-3
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-start err-msg-2 err-msg-3)
           result-hash-table)

          (for-each
           (lambda (selem)
             (begin
               (let ((sflag (member selem result-list))
                     (err-msg-4
                      (format #f "missing element ~a" selem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? sflag #f))
                    sub-name
                    (string-append
                     err-start err-msg-2 err-msg-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-let-to-combination result-hash-table)
 (begin
   (let ((sub-name "test-let-to-combination")
         (test-list
          (list
           (list
            (list 'let (list (list 'z 1))
                  (list 'begin (list newline)))
            (list (list 'lambda (list 'z)
                        (list 'begin (list newline))) 1))
           (list
            (list 'let (list (list 'x 3) (list 'y 4))
                  (list 'begin (list newline)))
            (list (list 'lambda (list 'x 'y)
                        (list 'begin (list newline))) 3 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((sexp (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list (let->combination sexp)))
                (let ((err-start
                       (format
                        #f "~a : error (~a) : s-expression ~a : "
                        sub-name test-label-index sexp)))
                  (begin
                    (assert-lists-are-equal?
                     shouldbe-list result-list
                     sub-name err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Extend the evaluator in this section "))
    (display
     (format #f "to support the special~%"))
    (display
     (format #f "form let. (See exercise 4.6.)~%"))
    (newline)
    (display
     (format #f "The method used here is to use the "))
    (display
     (format #f "same function~%"))
    (display
     (format #f "let->combination from exercise 4.6, "))
    (display
     (format #f "in order to transform~%"))
    (display
     (format #f "the let function into a lambda expression "))
    (display
     (format #f "and then to~%"))
    (display
     (format #f "the analyze-lambda to prepare the "))
    (display
     (format #f "procedure function.~%"))
    (newline)
    (display
     (format #f "(define (let-form? let-list)~%"))
    (display
     (format #f "  (eq? (car let-list) 'let))~%"))
    (display
     (format #f "(define (analyze exp)~%"))
    (display
     (format #f "  (cond ((self-evaluating? exp)~%"))
    (display
     (format #f "         (analyze-self-evaluating exp))~%"))
    (display
     (format #f "        ((quoted? exp) "))
    (display
     (format #f "(analyze-quoted exp))~%"))
    (display
     (format #f "        ((variable? exp) "))
    (display
     (format #f "(analyze-variable exp))~%"))
    (display
     (format #f "        ((assignment? exp) "))
    (display
     (format #f "(analyze-assignment exp))~%"))
    (display
     (format #f "        ((definition? exp) "))
    (display
     (format #f "(analyze-definition exp))~%"))
    (display
     (format #f "        ((if? exp) (analyze-if exp))~%"))
    (display
     (format #f "        ((lambda? exp) "))
    (display
     (format #f "(analyze-lambda exp))~%"))
    (display
     (format #f "        ((begin? exp) "))
    (display
     (format #f "(analyze-sequence "))
    (display
     (format #f "(begin-actions exp)))~%"))
    (display
     (format #f "        ((let-form? exp) "))
    (display
     (format #f "(analyze (let->combination exp)))~%"))
    (display
     (format #f "        ((cond? exp) "))
    (display
     (format #f "(analyze (cond->if exp)))~%"))
    (display
     (format #f "        ((application? exp) "))
    (display
     (format #f "(analyze-application exp))~%"))
    (display
     (format #f "        (else~%"))
    (display
     (format #f "         "))
    (display
     (format #f "(error \"Unknown expression "))
    (display
     (format #f "type -- ANALYZE\" exp))))~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.22 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
