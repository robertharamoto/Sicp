#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sicp exercise 4.43                                   ###
;;;###                                                       ###
;;;###  last updated September 24, 2024                      ###
;;;###                                                       ###
;;;###  updated August 17, 2022                              ###
;;;###                                                       ###
;;;###  updated February 25, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro, and current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2 for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Use the amb evaluator to solve the "))
    (display
     (format #f "following puzzle:~%"))
    (display
     (format #f "Mary Ann Moore's father has a yacht and "))
    (display
     (format #f "so has each of~%"))
    (display
     (format #f "his four friends: Colonel Downing, Mr. "))
    (display
     (format #f "Hall, Sir Barnacle~%"))
    (display
     (format #f "Hood, and Dr. Parker. Each of the five "))
    (display
     (format #f "also has one daughter~%"))
    (display
     (format #f "and each has named his yacht after a "))
    (display
     (format #f "daughter of one of~%"))
    (display
     (format #f "the others. Sir Baracle's yacht is the "))
    (display
     (format #f "Gabrielle, Mr. Moore~%"))
    (display
     (format #f "owns the Lorna; Mr. Hall the Rosalind. "))
    (display
     (format #f "The Melissa, owned by~%"))
    (display
     (format #f "Colonel Downing, is named after Sir "))
    (display
     (format #f "Barnacle's daughter.~%"))
    (display
     (format #f "Gabrielle's father owns the yacht that "))
    (display
     (format #f "is named after~%"))
    (display
     (format #f "Dr. Parker's daughter. Who is Lorna's "))
    (display
     (format #f "father?~%"))
    (newline)
    (display
     (format #f "Try to write the program so that it runs "))
    (display
     (format #f "efficiently (see~%"))
    (display
     (format #f "exercise 4.40). Also determine how many "))
    (display
     (format #f "solutions there~%"))
    (display
     (format #f "are if we are not told that Mary Ann's "))
    (display
     (format #f "last name is Moore.~%"))
    (newline)
    (display
     (format #f "(define (lornas-father)~%"))
    (display
     (format #f "  (let ((colonel-downing-yacht "))
    (display
     (format #f "'melissa)~%"))
    (display
     (format #f "        (dr-parker-yacht "))
    (display
     (format #f "'mary-ann)~%"))
    (display
     (format #f "        (mr-hall-yacht "))
    (display
     (format #f "'rosalind)~%"))
    (display
     (format #f "        (mr-moore-yacht "))
    (display
     (format #f "'lorna)~%"))
    (display
     (format #f "        (sir-barnacle-yacht "))
    (display
     (format #f "'gabrielle)~%"))
    (display
     (format #f "        (mr-moore-daughter "))
    (display
     (format #f "'mary-ann)~%"))
    (display
     (format #f "        (sir-barnacle-daughter "))
    (display
     (format #f "'melissa)~%"))
    (display
     (format #f "        (mr-hall-daughter "))
    (display
     (format #f "(amb 'gabrielle 'lorna)))~%"))
    (display
     (format #f "    (let ((colonel-downing-daughter "))
    (display
     (format #f "(amb 'gabrielle 'lorna 'rosalind)))~%"))
    (display
     (format #f "      (let ((dr-parker-daughter "))
    (display
     (format #f "(amb 'gabrielle 'lorna 'rosalind)))~%"))
    (display
     (format #f "        (require~%"))
    (display
     (format #f "          (distinct?~%"))
    (display
     (format #f "             (list mr-hall-daughter~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "colonel-downing-daughter~%"))
    (display
     (format #f "                   "))
    (display
     (format #f "dr-parker-daughter)))~%"))
    (display
     (format #f "        (require (not (eq? "))
    (display
     (format #f "dr-parker-daughter 'gabrielle))~%"))
    (display
     (format #f "        (require (or (and (eq? "))
    (display
     (format #f "dr-parker-daughter 'lorna)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(eq? mr-moore-yacht 'lorna)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(eq? mr-moore-daughter 'gabrielle))~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(and (eq? dr-parker-daughter 'rosalind)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(eq? mr-hall-daughter 'gabrielle))))~%"))
    (display
     (format #f "        (list (list 'colonel-downing-daughter~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "colonel-downing-daughter)~%"))
    (display
     (format #f "              (list 'dr-parker-daughter~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "dr-parker-daughter)~%"))
    (display
     (format #f "              (list 'mr-hall-daughter~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "mr-hall-daughter))~%"))
    (display
     (format #f "       ))))~%"))
    (newline)
    (display
     (format #f "If mr-hall-daughter is gabrielle, then "))
    (display
     (format #f "dr-parker's daughter~%"))
    (display
     (format #f "is rosalind, and colonel-downing-daughter "))
    (display
     (format #f "is lorna. If~%"))
    (display
     (format #f "mr-hall-daughter is lorna, and if "))
    (display
     (format #f "dr-parker-daughter is~%"))
    (display
     (format #f "rosalind, then "))
    (display
     (format #f "colonel-downing-daughter is gabrielle,~%"))
    (display
     (format #f "but this contradicts Gabrielle's father's "))
    (display
     (format #f "yacht is named~%"))
    (display
     (format #f "after Dr. Parker's daughter. So there is "))
    (display
     (format #f "only one solution,~%"))
    (display
     (format #f "and lorna's father is Colonel "))
    (display
     (format #f "Downing.~%"))
    (newline)
    (display
     (format #f "If we are not told that Mary Ann's father "))
    (display
     (format #f "is Mr. Moore,~%"))
    (display
     (format #f "then:~%"))
    (display
     (format #f "(define (lornas-father)~%"))
    (display
     (format #f "  (let ((colonel-downing-yacht 'melissa)~%"))
    (display
     (format #f "        (dr-parker-yacht 'mary-ann)~%"))
    (display
     (format #f "        (mr-hall-yacht 'rosalind)~%"))
    (display
     (format #f "        (mr-moore-yacht 'lorna)~%"))
    (display
     (format #f "        (sir-barnacle-yacht 'gabrielle)~%"))
    (display
     (format #f "        (sir-barnacle-daughter 'melissa)~%"))
    (display
     (format #f "        (mr-hall-daughter "))
    (display
     (format #f "(amb 'gabrielle 'lorna 'mary-ann))~%"))
    (display
     (format #f "        (mr-moore-daughter "))
    (display
     (format #f "(amb 'gabrielle 'mary-ann 'rosalind)))~%"))
    (display
     (format #f "    (let ((colonel-downing-daughter~%"))
    (display
     (format #f "       (amb 'gabrielle 'lorna "))
    (display
     (format #f "'mary-ann 'rosalind)))~%"))
    (display
     (format #f "      (let ((dr-parker-daughter "))
    (display
     (format #f "(amb 'gabrielle~%"))
    (display
     (format #f "'lorna 'mary-ann 'rosalind)))~%"))
    (display
     (format #f "        (require~%"))
    (display
     (format #f "          (distinct?~%"))
    (display
     (format #f "             "))
    (display
     (format #f "(list colonel-downing-daughter "))
    (display
     (format #f "dr-parker-daughter~%"))
    (display
     (format #f "                           "))
    (display
     (format #f "mr-hall-daughter "))
    (display
     (format #f "mr-moore-daughter)))~%"))
    (display
     (format #f "        (require~%"))
    (display
     (format #f "           "))
    (display
     (format #f "(not (eq? dr-parker-daughter "))
    (display
     (format #f "'gabrielle))~%"))
    (display
     (format #f "        (require~%"))
    (display
     (format #f "           "))
    (display
     (format #f "(or (and (eq? "))
    (display
     (format #f "dr-parker-daughter 'lorna)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(eq? mr-moore-yacht 'lorna)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(eq? mr-moore-daughter 'gabrielle))~%"))
    (display
     (format #f "                     "))
    (display
     (format #f "(and (eq? dr-parker-daughter 'rosalind)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(eq? mr-hall-yacht 'rosalind)~%"))
    (display
     (format #f "                          "))
    (display
     (format #f "(eq? mr-hall-daughter 'gabrielle))))~%"))
    (display
     (format #f "        (list (list "))
    (display
     (format #f "'colonel-downing-daughter~%"))
    (display
     (format #f "                    "))
    (display
     (format #f "colonel-downing-daughter)~%"))
    (display
     (format #f "              "))
    (display
     (format #f "(list 'dr-parker-daughter~%"))
    (display
     (format #f "              "))
    (display
     (format #f "       dr-parker-daughter)~%"))
    (display
     (format #f "              "))
    (display
     (format #f "(list 'mr-hall-daughter~%"))
    (display
     (format #f "              "))
    (display
     (format #f "       mr-hall-daughter)~%"))
    (display
     (format #f "              "))
    (display
     (format #f "(list 'mr-moore-daughter~%"))
    (display
     (format #f "              "))
    (display
     (format #f "      mr-moore-daughter))~%"))
    (display
     (format #f "       ))))~%"))
    (newline)
    (display
     (format #f "Since Gabrielle's father's yacht is named "))
    (display
     (format #f "after Dr. Parker's~%"))
    (display
     (format #f "daughter, dr-parker-daughter cannot be "))
    (display
     (format #f "gabrielle, since~%"))
    (display
     (format #f "Gabrielle's father's yacht is named after "))
    (display
     (format #f "Dr. Parker's~%"))
    (display
     (format #f "daughter, (and dr-parker-yacht is "))
    (display
     (format #f "'mary-ann). So~%"))
    (display
     (format #f "dr-parker-daughter can only be 'lorna, "))
    (display
     (format #f "'mary-ann, or~%"))
    (display
     (format #f "'rosalind. dr-parker-daughter cannot be "))
    (display
     (format #f "'mary-ann because~%"))
    (display
     (format #f "his yacht is named 'mary-ann. So any "))
    (display
     (format #f "solution must have~%"))
    (display
     (format #f "dr-parker-daughter = 'lorna "))
    (display
     (format #f "or 'rosalind.~%"))
    (display
     (format #f "(1) (dp lorna, mh gabrielle, "))
    (display
     (format #f "cd mary-ann, mm rosalind)~%"))
    (display
     (format #f "(2) (dp lorna, mh gabrielle, "))
    (display
     (format #f "cd rosalind, mm mary-ann)~%"))
    (display
     (format #f "(3) (dp lorna, mh mary-ann, "))
    (display
     (format #f "cd gabrielle, mm rosalind)~%"))
    (display
     (format #f "(4) (dp lorna, mh mary-ann, "))
    (display
     (format #f "cd rosalind, mm gabrielle)~%"))
    (display
     (format #f "(5) (dp lorna, mh rosalind, "))
    (display
     (format #f "cd gabrielle, mm mary-ann)~%"))
    (display
     (format #f "(6) (dp lorna, mh rosalind, "))
    (display
     (format #f "cd mary-ann, mm gabrielle)~%"))
    (display
     (format #f "(7) (dp rosalind, mh gabrielle, "))
    (display
     (format #f "cd mary-ann, mm lorna)~%"))
    (display
     (format #f "(8) (dp rosalind, mh gabrielle, "))
    (display
     (format #f "cd lorna, mm mary-ann)~%"))
    (display
     (format #f "(9) (dp rosalind, mh mary-ann, "))
    (display
     (format #f "cd gabrielle, mm lorna)~%"))
    (display
     (format #f "(10) (dp rosalind, mh mary-ann, "))
    (display
     (format #f "cd lorna, mm gabrielle)~%"))
    (display
     (format #f "(11) (dp rosalind, mh lorna, "))
    (display
     (format #f "cd gabrielle, mm mary-ann)~%"))
    (display
     (format #f "(12) (dp rosalind, mh lorna, "))
    (display
     (format #f "cd mary-ann, mm gabrielle)~%"))
    (newline)
    (display
     (format #f "Let req-1 = 'Gabrielle's father owns the "))
    (display
     (format #f "yacht that is~%"))
    (display
     (format #f "named after Dr. Parker's "))
    (display
     (format #f "daughter.~%"))
    (display
     (format #f "(1) & (2) & (3) are inconsistent "))
    (display
     (format #f "with req-1~%"))
    (display
     (format #f "(4) is a solution!~%"))
    (display
     (format #f "(5) & (6) mr-hall-daughter cannot be "))
    (display
     (format #f "rosalind since his~%"))
    (display
     (format #f "yacht is named rosalind.~%"))
    (display
     (format #f "(7) & (9) mr-moore-daughter cannot be "))
    (display
     (format #f "lorna since~%"))
    (display
     (format #f "his yacht is named lorna~%"))
    (display
     (format #f "(8) is a solution!~%"))
    (display
     (format #f "(10) & (11) & (12) are inconsistent "))
    (display
     (format #f "with req-1~%"))
    (newline)
    (display
     (format #f "In total, there are 2 solutions:~%"))
    (display
     (format #f "(4) (dp lorna, mh mary-ann, "))
    (display
     (format #f "cd rosalind, mm gabrielle)~%"))
    (display
     (format #f "(8) (dp rosalind, mh gabrielle, "))
    (display
     (format #f "cd lorna, mm mary-ann)~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Sicp exercise 4.43 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display
           (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
